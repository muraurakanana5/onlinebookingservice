class CurrencyConversion():
    def ConvertUsdToKes(amount, conversion_rate):
        float_amount = float(amount)
        float_rate = float(conversion_rate)

        kes_amount = float_amount * float_rate

        return round(kes_amount, 2)

    def ConvertKesToUsd(amount, conversion_rate):
        float_amount = float(amount)
        float_rate = float(conversion_rate)

        usd_amount = float_amount/float_amount

        return round(usd_amount, 2)


# class Converter():
#     def ConversionHandler(currencyToConvertTo, currencyToConvertFrom, conversionAmount):
#         if currencyToConvertFrom == currencyToConvertTo:
#             return conversionAmount
#         elif currencyToConvertFrom != currencyToConvertTo:
#             pass