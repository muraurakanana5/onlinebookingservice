from flask import render_template, jsonify
from datetime import datetime, timedelta, date
from flask_mail import Mail, Message
from sendgrid.helpers.mail import *

from routes import app
from routes import db
from routes import db_cache, FromCache, memoise_cache

from database.booking_activity_log import BookingActivity
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_payments import BookingPayment
from database.credit_note import CreditNote
from database.credit_note_facilities import CreditNoteFacility
from database.credit_note_guests import CreditNoteGuest
from database.credit_note_inventory import CreditNoteInventory
from database.credit_note_vehicles import CreditNoteVehicle
from database.facility import Facility
from database.facility_pricing_type import FacilityPricing
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.salesforce import SalesforceData
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.destination import Destination
from database.payment_methods import PaymentMethod
from database.booking_tickets import Ticket
from database.partner import Partner
from database.pickup_location import Location

from variables import *
from functions.date_operators import *


import math
import sendgrid
import requests
import uuid
import sys
import traceback


def close(self):
	self.session.close()


#############
#### Ref ####
#############
def get_booking_status_id(status_name):
	status_id = db.session.query(BookingStatus)\
						  .filter(BookingStatus.deletion_marker == None)\
						  .filter(BookingStatus.booking_status_name == status_name)\
						  .all()

	for single in status_id:
		booking_status_id = single.booking_status_public_id

		return booking_status_id


def currencyHandler(currencyTo, currencyFrom, amount):
	if currencyTo == currencyFrom:
		return amount
	elif currencyTo != currencyFrom:
		if currencyFrom == "162fface-f5f1-41de-913b-d2bb784dda3a":
			get_rate = requests.get(get_buy_sell_rate.format(currencyTo))

			try:
				buying = get_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_rate.json()["data"][0]["currency_sell_amount"]

				value = float(amount) / float(buying)
				return float(value)
			except Exception:
				value = 0
				return float(value)

		elif currencyTo == "162fface-f5f1-41de-913b-d2bb784dda3a":
			get_rate = requests.get(get_buy_sell_rate.format(currencyFrom))

			try:
				buying = get_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_rate.json()["data"][0]["currency_sell_amount"]

				value = float(amount) * float(selling)
				return float(value)
			except Exception:
				value = 0
				return float(value)
		
		else:
			get_from_rate = requests.get(get_buy_sell_rate.format(currencyFrom))
			get_to_rate = requests.get(get_buy_sell_rate.format(currencyTo))

			try:
				from_buying = get_from_rate.json()["data"][0]["currency_buy_amount"]
				from_selling = get_from_rate.json()["data"][0]["currency_sell_amount"]

				to_buying = get_to_rate.json()["data"][0]["currency_buy_amount"]
				to_selling = get_to_rate.json()["data"][0]["currency_sell_amount"]

				temp_from = float(amount) * float(from_selling)
				temp_to = float(temp_from) / float(to_buying)
				value = temp_to
				return float(value)
			except Exception:
				value = 0
				return float(value)


def currency_manager(currencyTo, currencyFrom, amount, conversion_rate):
	if currencyTo == currencyFrom:
		return float(amount)

	elif currencyTo != currencyFrom:
		if currencyFrom == "162fface-f5f1-41de-913b-d2bb784dda3a":
			value = float(amount) / float(conversion_rate)

			return float(value)
		
		elif currencyTo == "162fface-f5f1-41de-913b-d2bb784dda3a":
			value = float(amount) * float(conversion_rate)

			return float(value)


@memoise_cache.memoize(timeout=84600)
def get_details_partner(partner_id):
	partner_details = requests.get(get_partner_details.format(partner_id))

	return partner_details.json()


@memoise_cache.memoize(timeout=84600)
def get_details_member(member_id):
	member_details = requests.get(get_member_by_id.format(member_id))

	if member_details.status_code == 412:
		member_details = requests.get(get_corporate_member.format(member_id))

	return member_details.json()


@memoise_cache.memoize(timeout=84600)
def get_details_facility(facility_id):
	return_facility = requests.get(get_facility_details.format(facility_id))

	return return_facility


@memoise_cache.memoize(timeout=84600)
def get_details_inventory(inventory_id):
	return_inventory = requests.get(get_inventory_details.format(inventory_id))

	return return_inventory


@memoise_cache.memoize(timeout=169200)
def get_details_currency(currency_id):
	currency = requests.get(get_currency.format(currency_id))

	return currency


@memoise_cache.memoize(timeout=169200)
def get_details_donation_cause(cause_id):
	cause = requests.get(get_donation_cause.format(cause_id))

	return cause


@memoise_cache.memoize(timeout=169200)
def get_details_user(user_id):
	user = requests.get(get_single_user.format(user_id))

	return user


### Email Functions ###
def facilityMailer(email_data):
	recipient = email_data["recipient"]
	sender = email_data["sender"]

	booking_details = {}
	booking_details["booking_ref_code"] = email_data["booking_ref_code"]
	booking_details["check_in_date"] = email_data["check_in_date"]
	booking_details["check_out_date"] = email_data["check_out_date"]
	booking_details["num_of_guests"] = email_data["num_of_guests"]
	booking_details["client"] = email_data["client"]
	booking_details["today"] = email_data["today"]
	booking_details["name"] = email_data["name"]
	booking_details["copyright_year"] = datetime.now().strftime("%Y")

	sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])

	from_email = Email(sender)
	to_email = Email(recipient)
	subject = email_data["subject"]
	content = Content("text/html", render_template("facility_reservation.html", data = booking_details))
	mail = Mail(from_email, subject, to_email, content)

	try:
		response = sg.client.mail.send.post(request_body=mail.get())

		booking_activity = BookingActivity(
			booking_activity_public_id = str(uuid.uuid4()),
			booking_id = email_data["booking_id"],
			booking_activity_description = "Accomodation notification email sent to " + recipient + " from " + sender,
			session_id = None,
			created_at = datetime.now()
		)

		db.session.add(booking_activity)

		db.session.commit()
		close(db)

		return jsonify({'message' : str(response)})
	except Exception as e:
		trace = traceback.format_exc()
		raise Exception("Error: " + str(e) + ". Trace: " + trace)


def inventoryMailer(email_data):
	recipient = email_data["recipient"]
	sender = email_data["sender"]

	booking_details = {}
	booking_details["booking_ref_code"] = email_data["booking_ref_code"]
	booking_details["booking_date"] = email_data["booking_date"]
	booking_details["num_of_guests"] = email_data["num_of_guests"]
	booking_details["client"] = email_data["client"]
	booking_details["today"] = email_data["today"]
	booking_details["name"] = email_data["name"]
	booking_details["copyright_year"] = datetime.now().strftime("%Y")

	sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])

	from_email = Email(sender)
	to_email = Email(recipient)
	subject = email_data["subject"]
	content = Content("text/html", render_template("inventory_reservation.html", data = booking_details))
	mail = Mail(from_email, subject, to_email, content)

	try:
		response = sg.client.mail.send.post(request_body=mail.get())

		booking_activity = BookingActivity(
			booking_activity_public_id = str(uuid.uuid4()),
			booking_id = email_data["booking_id"],
			booking_activity_description = "Activity notification email sent to " + recipient + " from " + sender,
			session_id = None,
			created_at = datetime.now()
		)

		db.session.add(booking_activity)

		db.session.commit()
		close(db)

		return jsonify({'message' : str(response)})
	except Exception as e:
		trace = traceback.format_exc()
		raise Exception("Error: " + str(e) + ". Trace: " + trace)


def inventoryBooking(request, booking_info, inventory_email_data, currency_id = None, partner_details = None, promo_code_details = None):
	if currency_id:
		currency_to_use = request["currency_id"]
	else:
		currency_to_use = request["currency"]
	
	for single_inventory in request["inventory"]:
		inventory_info = {}

		try:
			# inventory_data = requests.get(get_inventory_details.format(single_inventory["public_id"]))
			inventory_data = requests.post(get_inventory_with_pricing.format(single_inventory["public_id"]), json = {
				"public_id": single_inventory["public_id"],
				"date": single_inventory["date"]
			})
			
			if inventory_data.status_code != 200:
				raise Exception("The pricing of the selected activity is not available. Please try again later or contact customer care. " + str(inventory_data.json()["exc"]))
			
			if inventory_data.json()["data"][0]["pricing_status"] == "false":
				# issues.append("The pricing of the selected activity is not available. Please try again later or contact customer care.")
				# return jsonify({"message": issues}), 422
				raise Exception("The pricing of the selected activity is not available. Please try again later or contact customer care.")
			
			elif inventory_data.json()["data"][0]["pricing_status"] == "true":
				single_inventory_code = inventory_data.json()["data"][0]["code"]
				inventory_currency_type = inventory_data.json()["data"][0]["price"]["currency_type"]
				inventory_currency_type_id = inventory_data.json()["data"][0]["price"]["currency_type_id"]
				analysis_code = inventory_data.json()["data"][0]["analysis_code"]
				name = inventory_data.json()["data"][0]["name"]

				inventory_info["inventory_name"] = name

				try:
					adult = float(inventory_data.json()["data"][0]["price"]["adult_price"])
					child = float(inventory_data.json()["data"][0]["price"]["child_price"])
				except (ValueError, TypeError) as no_value:
					adult = 0
					child = 0
				
				adult_rate = currencyHandler(currency_to_use, inventory_currency_type_id, adult)
				child_rate = currencyHandler(currency_to_use, inventory_currency_type_id, child)
				
				if single_inventory["date"] < request["check_in"]:
					raise Exception("Your activity date cannot come before your booking check in date.")

				if single_inventory["date"] > request["check_out"]:
					raise Exception("Your activity date cannot come after your booking check out date.")

				inv_bookings_count = []
				
				get_all_inventory_bookings_on_day = db.session.query(Inventory)\
																.filter(Inventory.deletion_marker == None)\
																.filter(Inventory.inventory_id == single_inventory["public_id"])\
																.filter(Inventory.inventory_booking_date == single_inventory["date"])\
																.filter(Inventory.status != get_booking_status_id("Updated"))\
																.filter(Inventory.status != get_booking_status_id("Cancelled"))\
																.all()

				
				for each_inv_booking in get_all_inventory_bookings_on_day:
					inv_bookings_count.append(each_inv_booking.inventory_booking_adults)
					inv_bookings_count.append(each_inv_booking.inventory_booking_children)

				total_inv_count = sum(inv_bookings_count)
				try:
					max_guests = inventory_data.json()["data"][0]["maximum_guests"]
					total_inv_guests = int(single_inventory["adults"]) + int(single_inventory["children"])
				except KeyError as e:
					# return jsonify({"message": "Error", "error": str(e), "position": 2}), 422
					raise Exception(str(e))

				if total_inv_count > max_guests:
					raise Exception(inventory_data.json()["data"][0]["name"] + " has already been fully booked on " + single_inventory["date"] + ". \
									Please select another date.")

				if (total_inv_count + total_inv_guests) > max_guests:
					raise Exception(inventory_data.json()["data"][0]["name"] + " is not available on " + single_inventory["date"] + " for "\
									+ str(total_inv_guests) + " guest(s). Please reduce the number of guests or select another date.")
				
				get_rate = requests.get(get_buy_sell_rate.format(inventory_currency_type_id))

				try:
					buying = get_rate.json()["data"][0]["currency_buy_amount"]
					selling = get_rate.json()["data"][0]["currency_sell_amount"]

				except Exception:
					buying = 1
					selling = 1
				
				if partner_details:
					if inventory_data.json()["data"][0]["discount_status"] == "true":
						discounts = inventory_data.json()["data"][0]["discount"]
						inventory_discount = 0
						inventory_special_discount = 0

						for each_discount in discounts:
							if partner_details["type_public_id"] == each_discount["discount_type_id"]:
								inventory_discount = float(each_discount["discount"])
								inventory_special_discount = float(each_discount["commission"])
							else:
								pass
					
					else:
						inventory_discount = 0
						inventory_special_discount = 0
				
				else:
					inventory_discount = 0
					inventory_special_discount = 0
				
				try:
					extra_adults = int(single_inventory["extra_pax_adults"])
				except Exception:
					extra_adults = 0

				try:
					extra_children = int(single_inventory["extra_pax_children"])
				except Exception:
					extra_children = 0

				## This is the discount that is entered on the UI
				try:
					applied_discount = float(single_inventory["activity_discount"])
				except Exception:
					applied_discount = 0

				## Handling member discount
				try:
					if request["member"]:
						inventory_discount = float(request["member_activity_discount"])
				except Exception:
					pass
				
				## TODO: Work with fixed amount
				## Handling percentage off used by promo code
				if promo_code_details:
					if promo_code_details["activities"]:
						if single_inventory["public_id"] in promo_code_details["activities_array"]:
							if promo_code_details["percentage_discount"]:
								inventory_discount = float(promo_code_details["percentage_off"])
				
				try:
					timeslot = single_inventory["time_frame"]
				except Exception:
					timeslot = None

				try:
					pickup_location = single_inventory["pick_up_location"]
				except Exception:
					pickup_location = None
				
				inventory = Inventory(
					inventory_booking_public_id = str(uuid.uuid4()),
					booking_id = booking_info["booking_id"],
					inventory_id = single_inventory["public_id"],
					inventory_code = single_inventory_code,
					inventory_booking_date = GenerateDateFromString.generateDate(single_inventory["date"]),
					inventory_booking_adults = int(single_inventory["adults"]),
					inventory_booking_children = int(single_inventory["children"]),
					inventory_booking_extra_adults = extra_adults,
					inventory_booking_extra_children = extra_children,
					inventory_cost_per_adult = round(adult_rate),
					inventory_cost_per_child = round(child_rate),
					inventory_cost_currency = currency_to_use,
					inventory_discount_rate = inventory_discount + applied_discount,
					inventory_commission_rate = inventory_special_discount,
					inventory_analysis_code = analysis_code,
					inventory_adult_cost_at_time = round(adult),
					inventory_child_cost_at_time = round(child),
					inventory_currency_at_time = inventory_currency_type_id,
					inventory_rate_at_time = buying,
					inventory_timeslot = timeslot,
					inventory_pickup_location = pickup_location,
					created_at = datetime.now(),
					updated_at = datetime.now()
				)

				db.session.add(inventory)

				inventory_mailing_list = requests.get(get_inventory_contacts.format(single_inventory["public_id"]))

				booking_date = GenerateDateFromString.generateDate(single_inventory["date"])

				inventory_info["inventory_guest_count"] = int(single_inventory["adults"]) + int(single_inventory["children"]) + extra_adults + extra_children
				inventory_info["date"] = booking_date.strftime("%A %d %B %Y")

				inventory_email_data.append(inventory_info)
				
				try:
					if inventory_mailing_list.json()["data"]:
						for each_email in inventory_mailing_list.json()["data"]:
							email_data = {}
							email_data["recipient"] = each_email["email"]
							email_data["sender"] = "reservations@olpejetaconservancy.org"
							email_data["subject"] = "Activity Reservation (#" + booking_info["booking_ref"] + ")"

							email_data["booking_date"] = booking_date.strftime("%A %d %B %Y")
							email_data["name"] = name

							email_data["num_of_guests"] = int(single_inventory["adults"]) + int(single_inventory["children"])
							email_data["client"] = booking_info["first_name"] + " " + booking_info["last_name"]
							email_data["booking_ref_code"] = booking_info["booking_ref"]
							email_data["booking_id"] = booking_info["booking_id"]
							email_data["today"] = datetime.now().strftime("%B %Y")

							try:
								inventoryMailer(email_data)
							except Exception as e:
								# raise Exception(str(e))
								pass
					
					elif inventory_mailing_list.json()["message"]:
						pass
				except KeyError as e:
					# return jsonify({"message": "Error on mailing.", "error": str(e), "position": 1}), 422
					# raise Exception(str(e))
					pass

		except KeyError as inventory_data_key_error:
			raise Exception("There was an issue on our end reserving the activity. Please try again later or contact customer care.")
			# issues.append("There was an issue on our end reserving the activity. Please try again later or contact customer care.")
			# print("There was an error getting the following key: " + str(inventory_data_key_error))
			# return jsonify({"message": issues, "error": str(inventory_data_key_error)}), 422
		except requests.exceptions.ConnectionError as connection_error:
			raise Exception("The activity pricing service is not available at the moment. Please try again later.")
			# issues.append("The activity pricing service is not available at the moment. Please try again later.")
			# return jsonify({"message": issues, "error": str(connection_error)}), 422

def facilityBooking(request, booking_info, facility_email_data, currency_id = None, partner_details = None, promo_code_details = None):
	
	if currency_id:
		currency_to_use = request["currency_id"]
	else:
		currency_to_use = request["currency"]

	try:
		for single_facility in request["facilities"]:
			facility_info = {}

			facility_start_date = single_facility["start_date"]
			facility_end_date = single_facility["end_date"]
			booked_rooms = 0
			# try:
			# 	booked_rooms = single_facility["number_of_rooms"]
			# except ValueError:
			# 	booked_rooms = 0


			if facility_start_date < request["check_in"]:
				raise Exception("The accommodation check-in date cannot come before the booking check-in date.")

			if facility_end_date > request["check_out"]:
				raise Exception("The accommodation check-out date cannot come after the booking check-out date.")

			if facility_start_date > request["check_out"]:
				raise Exception("The accommodation check-in date cannot come after the booking check-out date.")

			if facility_end_date < request["check_in"]:
				raise Exception("The accommodation check-out date cannot come before the booking check-in date.")

			if facility_start_date == facility_end_date:
				raise Exception("The accommodation check-in and check-out dates cannot be the same.")

			try:
				facility_data = requests.post(get_facility_pricing_by_date.format(single_facility["public_id"]),
				   json = {"start_date": facility_start_date, "end_date": facility_end_date})

				
				if facility_data.json()["data"][0]["pricing_status"] == "false":
					raise Exception("The pricing of the selected accommodation/campsite has not been set yet. Please try again at a future date.")

				elif facility_data.json()["data"][0]["pricing_status"] == "true":
					single_facility_code = facility_data.json()["data"][0]["code"]

					analysis_code = facility_data.json()["data"][0]["analysis_code"]
					name = facility_data.json()["data"][0]["name"]

					facility_info["facility_name"] = name

					catering_type = None

					## For cases where there a multiple arrivals on a campsite
					## Necessary to handle an exception since not all facility objects will have the 'is_multi' key
					try:
						is_multi = single_facility["is_multi"]
					except Exception:
						is_multi = False

					if facility_data.json()["data"][0]["facility_type_id"] == "Accommodation"\
					or facility_data.json()["data"][0]["facility_type_id"] == "Accomodation"\
					or facility_data.json()["data"][0]["facility_type_id"] == "Acomodation":
						if facility_data.json()["data"][0]["accomodation_type_id"] != "Pelican":
							try:
								adult = float(facility_data.json()["data"][0]["price"][0]["adult_price"])
								child = float(facility_data.json()["data"][0]["price"][0]["child_price"])
								fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])
							except (ValueError, TypeError) as no_value:
								adult = 0
								child = 0
								fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])

							facility_currency_type = facility_data.json()["data"][0]["price"][0]["currency_type"]
							facility_currency_type_id = facility_data.json()["data"][0]["price"][0]["currency_type_id"]

							adult_count = single_facility["adults"]
							child_count = single_facility["children"]

							adult_rate = currencyHandler(currency_to_use, facility_currency_type_id, adult)
							child_rate = currencyHandler(currency_to_use, facility_currency_type_id, child)
							fixed_rate = currencyHandler(currency_to_use, facility_currency_type_id, fixed)

							## For handling special pricing
							special_price_rate = None
							special_price_unit_increase_rate = None
							special_rate = None
							special_increase_rate = None
							special = None
							special_increase = None

						elif facility_data.json()["data"][0]["accomodation_type_id"] == "Pelican":
							facility_total_guests = int(single_facility["adults"]) + int(single_facility["children"])

							get_max_guests = facility_data.json()["data"][0]["maximum_guests"]

							# if facility_total_guests > int(get_max_guests):
							# 	raise Exception("You cannot have more than " + get_max_guests + " guests staying at " + name + ".")

							catering_type = single_facility["catering_type"]

							if single_facility["catering_type"] == "9a9a77eb":
								limits = None
								for each_group in facility_data.json()["data"][0]["price"]:
									if len(each_group["lower_limit_list"]) > 0:
										limits = each_group["lower_limit_list"]
									else:
										pass

								if not limits:
									# message = []
									# message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
									# return jsonify({"message": message}), 422
									raise Exception("The pricing for the catering selected option has not been set. Please try another catering option.")

								# try:
								# 	limits = facility_data.json()["data"][0]["price"][1]["lower_limit_list"]
								# except Exception as e:
								# 	message = []
								# 	message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
								# 	return jsonify({"message": message, "error": str(e)}), 422

							elif single_facility["catering_type"] == "377788d4":
								limits = None
								for each_group in facility_data.json()["data"][0]["price"]:
									if len(each_group["middle_middle_limit_list"]) > 0:
										limits = each_group["middle_middle_limit_list"]
									else:
										pass

								if not limits:
									# message = []
									# message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
									# return jsonify({"message": message}), 422
									raise Exception("The pricing for the catering selected option has not been set. Please try another catering option.")

								# try:
								# 	limits = facility_data.json()["data"][0]["price"][1]["middle_middle_limit_list"]
								# except Exception as e:
								# 	message = []
								# 	message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
								# 	return jsonify({"message": message, "error": str(e)}), 422

							elif single_facility["catering_type"] == "266677c3":
								limits = None
								for each_group in facility_data.json()["data"][0]["price"]:
									if len(each_group["upper_upper_limit_list"]) > 0:
										limits = each_group["upper_upper_limit_list"]
									else:
										pass

								if not limits:
									# message = []
									# message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
									# return jsonify({"message": message}), 422
									raise Exception("The pricing for the catering selected option has not been set. Please try another catering option.")

								# try:
								# 	limits = facility_data.json()["data"][0]["price"][1]["upper_upper_limit_list"]
								# except Exception as e:
								# 	output = []
								# 	output.append("The pricing for the catering selected option has not been set. Please try another catering option.")
								# 	return jsonify({"message": output, "error": str(e)}), 422

							else:
								try:
									info = []
									limit_data = {}

									limit_data["lower_limit"] = int(facility_data.json()["data"][0]["group_price"][0]["from_number"])
									limit_data["upper_limit"] = int(facility_data.json()["data"][0]["group_price"][0]["to_number"])
									limit_data["max_guests"] = int(facility_data.json()["data"][0]["group_price"][0]["maximum_number"])
									limit_data["currency"] = facility_data.json()["data"][0]["group_price"][0]["currency_type_id"]
									limit_data["price"] = 0
									limit_data["child_price"] = 0
									limit_data["mandatory_price"] = 0
									limit_data["special_pricing"] = 0
									limit_data["special_pricing"] = facility_data.json()["data"][0]["group_price"][0]["mandatory_price"]
									limit_data["special_pricing_unit_increase"] = facility_data.json()["data"][0]["group_price"][0]["extra_price"]

									info.append(limit_data)

									limits = info
								except Exception as e:
									# output = []
									# output.append("The pricing for the catering option you have selected has not been set. Please try another catering option.")
									# return jsonify({"message": output, "error": str(e)}), 422

									raise Exception("The pricing for the selected catering option has not been set. Please try another catering option.")

							for cost_range in limits:
								if int(single_facility["adults"]) >= int(cost_range["lower_limit"]):
									if int(single_facility["adults"]) >= int(cost_range["lower_limit"]) & int(single_facility["adults"]) <= int(cost_range["upper_limit"]):
										facility_currency_type_id = cost_range["currency"]

										currency_details = requests.get(get_currency.format(facility_currency_type_id))

										facility_currency_type = currency_details.json()["data"][0]["currency_name"]

										try:
											adult = float(cost_range["price"])
											child = float(cost_range["child_price"])
											fixed = 0
										except Exception:
											adult = 0
											child = 0
											fixed = 0

										try:
											special = round(float(cost_range["special_pricing"]))
											special_increase = round(float(cost_range["special_pricing_unit_increase"]))
										except Exception:
											special = None
											special_increase = None

								elif facility_total_guests > int(cost_range["upper_limit"]):
									pass


							# if facility_data.json()["data"][0]["price"][0]["mandatory_price"]:
							# 	fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])
							# else:
							# 	fixed = 0

							adult_count = single_facility["adults"]
							child_count = single_facility["children"]

							adult_rate = currencyHandler(currency_to_use, facility_currency_type_id, adult)
							child_rate = currencyHandler(currency_to_use, facility_currency_type_id, child)
							fixed_rate = currencyHandler(currency_to_use, facility_currency_type_id, fixed)

							if special:
								special_rate = round(currencyHandler(currency_to_use, facility_currency_type_id, special))
							else:
								special_rate = None

							if special_increase:
								special_increase_rate = round(currencyHandler(currency_to_use, facility_currency_type_id, special_increase))
							else:
								special_increase_rate = None

					elif facility_data.json()["data"][0]["facility_type_id"] == "Camping Sites"\
					or facility_data.json()["data"][0]["facility_type_id"] == "Campingsites"\
					or facility_data.json()["data"][0]["facility_type_id"] == "CampingSites":
						facility_currency_type = facility_data.json()["data"][0]["price"][0]["currency_type"]
						facility_currency_type_id = facility_data.json()["data"][0]["price"][0]["currency_type_id"]

						## Handling multiple arrivals
						if is_multi:
							try:
								adult = 0
								child = 0
								fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])

								adult_multi = float(facility_data.json()["data"][0]["price"][0]["adult_price"])
								child_multi = float(facility_data.json()["data"][0]["price"][0]["child_price"])
								fixed_multi = 0

							except (ValueError, TypeError) as no_value:
								adult = 0
								child = 0
								fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])

								adult_multi = 0
								child_multi = 0
								fixed_multi = 0

							adult_count = 0
							child_count = 0

							adult_rate = currencyHandler(currency_to_use, facility_currency_type_id, adult)
							child_rate = currencyHandler(currency_to_use, facility_currency_type_id, child)
							fixed_rate = currencyHandler(currency_to_use, facility_currency_type_id, fixed)

							adult_multi_rate = currencyHandler(currency_to_use, facility_currency_type_id, adult_multi)
							child_multi_rate = currencyHandler(currency_to_use, facility_currency_type_id, child_multi)
							fixed_multi_rate = currencyHandler(currency_to_use, facility_currency_type_id, fixed_multi)

						else:
							try:
								adult = float(facility_data.json()["data"][0]["price"][0]["adult_price"])
								child = float(facility_data.json()["data"][0]["price"][0]["child_price"])
								fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])
							except (ValueError, TypeError) as no_value:
								adult = 0
								child = 0
								fixed = float(facility_data.json()["data"][0]["price"][0]["mandatory_price"])

							adult_count = single_facility["adults"]
							child_count = single_facility["children"]

							if (adult_count + child_count + single_facility["extra_pax_adults"] + single_facility["extra_pax_children"]) == 0:
								raise Exception("The campsite booking cannot have zero guests.")

							adult_rate = currencyHandler(currency_to_use, facility_currency_type_id, adult)
							child_rate = currencyHandler(currency_to_use, facility_currency_type_id, child)
							fixed_rate = currencyHandler(currency_to_use, facility_currency_type_id, fixed)

						## For handling special pricing
						special_price_rate = None
						special_price_unit_increase_rate = None
						special_rate = None
						special_increase_rate = None
						special = None
						special_increase = None

					facility_check_in_date = GenerateDateFromString.generateDate(facility_start_date)
					facility_check_out_date = GenerateDateFromString.generateDate(facility_end_date)

					facility_date_diff = DateOperations.returnDateDifferenceInDays(facility_check_out_date, facility_check_in_date)

					get_rate = requests.get(get_buy_sell_rate.format(facility_currency_type_id))

					try:
						buying = get_rate.json()["data"][0]["currency_buy_amount"]
						selling = get_rate.json()["data"][0]["currency_sell_amount"]

					except Exception:
						buying = 1
						selling = 1

					if partner_details:
						if facility_data.json()["data"][0]["discount_status"] == "true":
							discounts = facility_data.json()["data"][0]["discount"]
							facility_discount = 0
							facility_special_discount = 0

							for each_discount in discounts:
								if partner_details["type_public_id"] == each_discount["discount_type_id"]:
									facility_discount = float(each_discount["discount"])
									facility_special_discount = float(each_discount["commission"])
								else:
									pass

						else:
							facility_discount = 0
							facility_special_discount = 0

					else:
						facility_discount = 0
						facility_special_discount = 0

					try:
						extra_adults = int(single_facility["extra_pax_adults"])
					except Exception:
						extra_adults = 0

					try:
						extra_children = int(single_facility["extra_pax_children"])
					except Exception:
						extra_children = 0

					## This is the discount that is entered on the UI
					try:
						applied_discount = float(single_facility["accommodation_discount"])
					except Exception:
						applied_discount = 0

					## Handling member discount
					try:
						if request["member"]:
							facility_discount = float(request["member_accommodation_discount"])
					except Exception:
						pass

					## TODO: Work with fixed amount
					## Handling percentage off used by promo code
					if promo_code_details:
						if promo_code_details["accommodation"]:
							if single_facility["public_id"] in promo_code_details["accommodation_array"]:
								if promo_code_details["percentage_discount"]:
									facility_discount = float(promo_code_details["percentage_off"])

					facility = Facility(
					 facility_booking_public_id = str(uuid.uuid4()),
					 booking_id = booking_info["booking_id"],
					 facility_id = single_facility["public_id"],
					 facility_code = single_facility_code,
					 facility_booking_check_in_date = facility_check_in_date,
					 facility_booking_check_out_date = facility_check_out_date,
					 facility_booking_adults = adult_count,
					 facility_booking_children = child_count,
					 facility_booking_extra_adults = extra_adults,
					 facility_booking_extra_children = extra_children,
					 facility_cost_per_adult = round(adult_rate),
					 facility_cost_per_child = round(child_rate),
					 facility_fixed_cost = round(fixed_rate),
					 facility_special_price = special_rate,
					 facility_special_price_unit_increase = special_increase_rate,
					 facility_cost_currency = currency_to_use,
					 facility_no_of_nights = facility_date_diff,
					 facility_discount_rate = facility_discount + applied_discount,
					 facility_commission_rate = facility_special_discount,
					 facility_analysis_code = analysis_code,
					 facility_adult_cost_at_time = round(adult),
					 facility_child_cost_at_time = round(child),
					 facility_fixed_cost_at_time = round(fixed),
					 facility_special_price_at_time = special,
					 facility_special_price_unit_increase_at_time = special_increase,
					 facility_currency_at_time = facility_currency_type_id,
					 facility_rate_at_time = buying,
					 facility_catering_type = catering_type,
					 facility_booked_rooms = booked_rooms,
					 created_at = datetime.now(),
					 updated_at = datetime.now()
					)

					db.session.add(facility)

					try:
						multi_array = request["multi"]
					except Exception:
						multi_array = []

					if is_multi:
						if multi_array:
							for single_multi_arrival in multi_array:
								if single_multi_arrival["public_id"] == single_facility["public_id"]:
									if single_multi_arrival["expected_to_pay"]:
										try:
											multi_discount = float(single_multi_arrival["discount"])
										except Exception:
											multi_discount = 0

									elif not single_multi_arrival["expected_to_pay"]:
										multi_discount = 100

									arrival_date = GenerateDateFromString.generateDate(single_multi_arrival["start_date"])
									departure_date = GenerateDateFromString.generateDate(single_multi_arrival["end_date"])

									extra_multi_adults = 0
									extra_multi_children = 0

									multi_date_diff = DateOperations.returnDateDifferenceInDays(departure_date, arrival_date)

									facility_multi_arrival = Facility(
										facility_booking_public_id=str(
											uuid.uuid4()),
										booking_id=booking_info["booking_id"],
										facility_id=single_facility[
											"public_id"],
										facility_code=single_facility_code,
										facility_booking_check_in_date=
										arrival_date,
										facility_booking_check_out_date=
										departure_date,
										facility_booking_adults=
										single_multi_arrival["adults"],
										facility_booking_children=
										single_multi_arrival["children"],
										facility_booking_extra_adults=
										extra_multi_adults,
										facility_booking_extra_children=
										extra_multi_children,
										facility_cost_per_adult=round(
											adult_multi_rate),
										facility_cost_per_child=round(
											child_multi_rate),
										facility_fixed_cost=round(
											fixed_multi_rate),
										facility_special_price=special_rate,
										facility_special_price_unit_increase=
										special_increase_rate,
										facility_cost_currency=currency_to_use,
										facility_no_of_nights=multi_date_diff,
										facility_discount_rate=multi_discount,
										facility_commission_rate=
										facility_special_discount,
										facility_analysis_code=analysis_code,
										facility_adult_cost_at_time=round(
											adult_multi),
										facility_child_cost_at_time=round(
											child_multi),
										facility_fixed_cost_at_time=round(
											fixed_multi),
										facility_special_price_at_time=special,
										facility_special_price_unit_increase_at_time
										=special_increase,
										facility_currency_at_time=
										facility_currency_type_id,
										facility_rate_at_time=buying,
										facility_catering_type=catering_type,
										facility_booked_rooms=booked_rooms,
										created_at=datetime.now(),
										updated_at=datetime.now())

									db.session.add(facility_multi_arrival)

					facility_mailing_list = requests.get(get_facility_contacts.format(single_facility["public_id"]))
					facility_info["facility_guest_count"] = int(single_facility["adults"]) + int(single_facility["children"]) + extra_adults + extra_children
					facility_info["start_date"] = facility_check_in_date.strftime("%A %d %B %Y")
					facility_info["end_date"] = facility_check_out_date.strftime("%A %d %B %Y")

					facility_email_data.append(facility_info)

					try:
						if facility_mailing_list.json()["data"]:
							for each_email in facility_mailing_list.json()["data"]:
								email_data = {}
								email_data["recipient"] = each_email["contact_email"]
								email_data["sender"] = "reservations@olpejetaconservancy.org"
								email_data["subject"] = "Facility Reservation (#" + booking_info["booking_ref"] + ")"
								email_data["check_in_date"] = facility_check_in_date.strftime("%A %d %B %Y")
								email_data["check_out_date"] = facility_check_out_date.strftime("%A %d %B %Y")
								email_data["name"] = name
								email_data["num_of_guests"] = int(single_facility["adults"]) + int(single_facility["children"])
								email_data["client"] = booking_info["first_name"] + " " + booking_info["last_name"]
								email_data["booking_ref_code"] = booking_info["booking_ref"]
								email_data["booking_id"] = booking_info["booking_id"]
								email_data["today"] = datetime.now().strftime("%B %Y")

								try:
									facilityMailer(email_data)
								except Exception as e:
									# raise Exception(str(e))
									pass

						elif facility_mailing_list.json()["data"]:
							pass
					except KeyError:
						pass

			except KeyError as facility_data:
				error_tuple = sys.exc_info()
				trace = traceback.format_exc()
				raise Exception("There was an error reserving the accommodation. Please try again later. Issue: " + str(facility_data) + ". Trace: " + trace)
				# message = []
				# message.append("There was an error reserving the facility. Please try again later.")
				# return jsonify({"message": message, "error": str(facility_data)}), 422
			except requests.exceptions.ConnectionError as connection_error:
				error_tuple = sys.exc_info()
				trace = traceback.format_exc()
				raise Exception("The accommodation pricing is not available at this time. Issue " + str(connection_error))
				# message = []
				# message.append("The facility pricing is not available at this time.")
				# return jsonify({"message": message, "error": str(connection_error)}), 422

	except KeyError:
		raise Exception("There was an error getting the booking's accommodation.")
		# message = []
		# message.append("There was an error getting the booking's facilities.")
		# return jsonify({"message": message}), 422


# postpone booking
def postponeFacilityBooking(request,
							booking_info,
							facility_email_data,
							currency_id=None,
							partner_details=None,
							promo_code_details=None):

	
	try:
		for single_facility in request["facilities"]:
				
			facility_info = {}
			facility_start_date = single_facility["facility_booking_check_in_date"]
			facility_check_in_date = single_facility["facility_booking_check_in_date"]
			facility_end_date = single_facility["facility_booking_check_out_date"]
			facility_check_out_date = single_facility["facility_booking_check_out_date"]
			name  = single_facility["facility_name"]

			if facility_start_date < request["check_in"]:
				raise Exception("The accommodation check-in date cannot come before the booking check-in date.")

			if facility_end_date > request["check_out"]:
				raise Exception("The accommodation check-out date cannot come after the booking check-out date.")

			if facility_start_date > request["check_out"]:
				raise Exception("The accommodation check-in date cannot come after the booking check-out date.")

			if facility_end_date < request["check_in"]:
				raise Exception("The accommodation check-out date cannot come before the booking check-in date.")

			if facility_start_date == facility_end_date:
				raise Exception("The accommodation check-in and check-out dates cannot be the same.")

			try:
				postpone_facility = db.session.query(Facility)\
					.filter(Facility.facility_booking_public_id == single_facility["facility_booking_public_id"])\
					.filter(Facility.deletion_marker == None)\
					.filter(Facility.status != get_booking_status_id("Updated"))\
					.filter(Facility.status != get_booking_status_id("Cancelled"))\
					.first()
				
				postpone_facility.facility_booking_check_in_date_before_postpone = postpone_facility.facility_booking_check_in_date
				postpone_facility.facility_booking_check_out_date_before_postpone = postpone_facility.facility_booking_check_out_date
				postpone_facility.facility_booking_check_in_date = facility_start_date
				postpone_facility.facility_booking_check_out_date = facility_end_date
				updated_at = datetime.now()
				db.session.commit()

			except KeyError:
				error_tuple = sys.exc_info()
				trace = traceback.format_exc()
				print("There was an error reserving the accommodation. Please try again later.")
				raise Exception("There was an error reserving the accommodation. Please try again later.")

			except requests.exceptions.ConnectionError as connection_error:
				error_tuple = sys.exc_info()
				trace = traceback.format_exc()
				print("The accommodation pricing is not available at this time. Issue " + str(connection_error))
				raise Exception("The accommodation pricing is not available at this time. Issue " + str(connection_error))

	except KeyError:
		raise Exception("There was an error getting the booking's accommodation.")
	# except Exception as e:
	# 	print("this is error")
	# 	print(e)


def postponeInventoryBooking(request, booking_info, inventory_email_data, currency_id = None, partner_details = None, promo_code_details = None):
	try:
		inventory= request["inventory"]
	except:
		inventory= []
	if inventory:
		for single_inventory in request["inventory"]:
			inventory_info = {}

			try:
				# inventory_data = requests.get(get_inventory_details.format(single_inventory["public_id"]))
				inventory_data = requests.post(get_inventory_with_pricing.format(single_inventory["inventory_id"]), json = {
				"public_id": single_inventory["inventory_id"],
				"date": single_inventory["activity_date"]
				})
				
				if inventory_data.status_code != 200:
					raise Exception("The pricing of the selected activity is not available. Please try again later or contact customer care. " + str(inventory_data.json()["exc"]))

				if inventory_data.json()["data"][0]["pricing_status"] == "false":
					# issues.append("The pricing of the selected activity is not available. Please try again later or contact customer care.")
					# return jsonify({"message": issues}), 422
					raise Exception("The pricing of the selected activity is not available. Please try again later or contact customer care.")

				elif inventory_data.json()["data"][0]["pricing_status"] == "true":
					single_inventory_code = inventory_data.json()["data"][0]["code"]
					inventory_currency_type = inventory_data.json()["data"][0]["price"]["currency_type"]
					inventory_currency_type_id = inventory_data.json()["data"][0]["price"]["currency_type_id"]
					analysis_code = inventory_data.json()["data"][0]["analysis_code"]
					name = inventory_data.json()["data"][0]["name"]

					inventory_info["inventory_name"] = name

					try:
						adult = float(inventory_data.json()["data"][0]["price"]["adult_price"])
						child = float(inventory_data.json()["data"][0]["price"]["child_price"])
					except (ValueError, TypeError) as no_value:
						adult = 0
						child = 0

					if single_inventory["activity_date"] < request["check_in"]:
						raise Exception("Your activity date cannot come before your booking check in date.")

					if single_inventory["activity_date"] > request["check_out"]:
						raise Exception("Your activity date cannot come after your booking check out date.")

					inv_bookings_count = []
				
					get_all_inventory_bookings_on_day = db.session.query(Inventory)\
					.filter(Inventory.deletion_marker == None)\
					.filter(Inventory.inventory_id == single_inventory["inventory_id"])\
					.filter(Inventory.inventory_booking_date == single_inventory["activity_date"])\
					.filter(Inventory.status != get_booking_status_id("Updated"))\
					.filter(Inventory.status != get_booking_status_id("Cancelled"))\
					.all()

					postpone_inventory = db.session.query(Inventory)\
					.filter(Inventory.deletion_marker == None)\
					.filter(Inventory.inventory_booking_public_id == single_inventory["inventory_booking_public_id"])\
					.filter(Inventory.status != get_booking_status_id("Updated"))\
					.filter(Inventory.status != get_booking_status_id("Cancelled"))\
					.first()

					for each_inv_booking in get_all_inventory_bookings_on_day:
						inv_bookings_count.append(each_inv_booking.inventory_booking_adults)
						inv_bookings_count.append(each_inv_booking.inventory_booking_children)

					total_inv_count = sum(inv_bookings_count)
					try:
						max_guests = inventory_data.json()["data"][0]["maximum_guests"]
						total_inv_guests = int(postpone_inventory.inventory_booking_adults) + int(postpone_inventory.inventory_booking_children)
					except KeyError as e:
						# return jsonify({"message": "Error", "error": str(e), "position": 2}), 422
						raise Exception(str(e))

					if total_inv_count > max_guests:
						raise Exception(inventory_data.json()["data"][0]["name"] + " has already been fully booked on " + single_inventory["activity_date"] + ". \
										Please select another date."                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       )

					if (total_inv_count + total_inv_guests) > max_guests:
						raise Exception(inventory_data.json()["data"][0]["name"] + " is not available on " + single_inventory["activity_date"] + " for "\
						+ str(total_inv_guests) + " guest(s). Please reduce the number of guests or select another date.")


					postpone_inventory.inventory_booking_date_before_postpone =  GenerateDateFromString.generateDate(single_inventory["old_activity_date"])
					postpone_inventory.inventory_booking_date = GenerateDateFromString.generateDate(single_inventory["activity_date"])
					updated_at = datetime.now()
					db.session.commit()

					
					inventory_mailing_list = requests.get(get_inventory_contacts.format(single_inventory["inventory_id"]))

					booking_date = GenerateDateFromString.generateDate(single_inventory["activity_date"])

					# inventory_info["inventory_guest_count"] = int(single_inventory["adults"]) + int(single_inventory["children"]) + extra_adults + extra_children
					inventory_info["date"] = booking_date.strftime("%A %d %B %Y")

					inventory_email_data.append(inventory_info)

					try:
						if inventory_mailing_list.json()["data"]:
							for each_email in inventory_mailing_list.json()["data"]:
								email_data = {}
								email_data["recipient"] = "patricia.kanana@olpejetaconservancy.org"
								email_data["sender"] = "reservations@olpejetaconservancy.org"
								email_data["subject"] = "Activity Reservation (#" + booking_info["booking_ref"] + ")"

								email_data["booking_date"] = booking_date.strftime("%A %d %B %Y")
								email_data["name"] = name

								email_data["num_of_guests"] = int(single_inventory["adults"]) + int(single_inventory["children"])
								email_data["client"] = booking_info["first_name"] + " " + booking_info["last_name"]
								email_data["booking_ref_code"] = booking_info["booking_ref"]
								email_data["booking_id"] = booking_info["booking_id"]
								email_data["today"] = datetime.now().strftime("%B %Y")

								try:
									inventoryMailer(email_data)
								except Exception as e:
									# raise Exception(str(e))
									pass

						elif inventory_mailing_list.json()["message"]:
							pass
					except KeyError as e:
						# return jsonify({"message": "Error on mailing.", "error": str(e), "position": 1}), 422
						# raise Exception(str(e))
						pass

			except KeyError as inventory_data_key_error:
				raise Exception("There was an issue on our end reserving the activity. Please try again later or contact customer care.")
				# issues.append("There was an issue on our end reserving the activity. Please try again later or contact customer care.")
				# print("There was an error getting the following key: " + str(inventory_data_key_error))
				# return jsonify({"message": issues, "error": str(inventory_data_key_error)}), 422
			except requests.exceptions.ConnectionError as connection_error:
				raise Exception("The activity pricing service is not available at the moment. Please try again later.")
				# issues.append("The activity pricing service is not available at the moment. Please try again later.")
				# return jsonify({"message": issues, "error": str(connection_error)}), 422

	else:
		pass


def bookingTotal(return_data_dict, booking_id, batchfile = None):
	get_booking = db.session.query(Booking)\
							.filter(Booking.booking_public_id == booking_id)\
							.options(FromCache(db_cache))\
							.first()
	
	# Booking currency
	# currency = requests.get(get_currency.format(get_booking.currency))
	currency = get_details_currency(get_booking.currency)
	# Get VAT rate
	# get_latest_vat = requests.get(get_latest_vat_rate)
	# Done to work with dynamic VAT values based on the date that the booking was created
	# this was changed and decided to get check in date  after request on 18th nov 20202 instead by Ester 
	# get_latest_vat = requests.post(get_vat_for_date, json = {
	# 	"query_date": get_booking.created_at.strftime("%Y-%m-%d")
	# })
	get_latest_vat = requests.post(get_vat_for_date, json = {
	 "query_date": get_booking.booking_check_in_date.strftime("%Y-%m-%d")
	})
	# Get catering levy rate
	get_latest_catering = requests.get(get_latest_cater_levy_rate)

	vat_rate = float(get_latest_vat.json()["data"][0]["vat_rate"])
	vat_calculation_amount = 1 + (vat_rate / 100)
	return_data_dict["_vat_rate"] = int(vat_rate)

	levy_rate = float(get_latest_catering.json()["data"][0]["levy_rate"])
	levy_calculation_amount = 100 + vat_rate + levy_rate
	return_data_dict["_catering_levy_rate"] = int(levy_rate)

	try:
		return_data_dict["booking_currency_name"] = currency.json()["data"][0]["currency_name"]
	except Exception:
		return_data_dict["booking_currency_name"] = ""
	
	booking_total_without_discount = []
	booking_total_with_discount = []

	vat_total = []
	catering_levy_total = []
	
	# Getting the guest types and conservancy fee totals
	get_all_gatepass_guests = db.session.query(GatepassGuest)\
										.join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
										.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
										.add_columns(Mandatory.payment_person, Mandatory.payment_public_id, Mandatory.payment_person_income_code,\
													 Mandatory.payment_person_dept_code,\
													 GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													 GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_discount_reason, GatepassGuest.gatepass_guest_rate_at_time,\
													 GatepassGuest.gatepass_guest_cost_at_time, GatepassGuest.gatepass_guest_currency_at_time, GatepassGuest.gatepass_currency,\
													 GatepassGuest.gatepass_payment_schedule, GatepassGuest.gatepass_guest_public_id,\
													 Gatepass.destination)\
										.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
										.filter(GatepassGuest.deletion_marker == None)\
										.filter(Gatepass.booking_id == booking_id)\
										.filter(Gatepass.status != get_booking_status_id("Updated"))\
										.options(FromCache(db_cache))\
										.all()

	guest_array = []
	guest_sum = []
	guest_total_without_discount = []
	guest_total_with_discount = []
	guest_discount = []
	for each_guest in get_all_gatepass_guests:
		guest_data = {}
		
		cost_per_person = currency_manager(get_booking.currency, each_guest.gatepass_currency, float(each_guest.gatepass_cost_per_pp), float(each_guest.gatepass_guest_rate_at_time))
		
		guest_data["payment_public_id"] = each_guest.payment_public_id
		guest_data["gatepass_guest_public_id"] = each_guest.gatepass_guest_public_id
		guest_data["payment_person"] = each_guest.payment_person
		guest_data["payment_guests"] = each_guest.gatepass_guest_count
		guest_data["payment_person_discount"] = float(each_guest.gatepass_discount_rate)
		guest_data["payment_person_discount_reason"] = each_guest.gatepass_discount_reason

		# While the details are being returned in 'payment_person' and 'payment_guests',
		# this has been included here in order to replace the sperate query of guest data
		# in bookings/view, searches etc
		guest_data["guest_type"] = each_guest.payment_person
		guest_data["no_of_guests"] = each_guest.gatepass_guest_count
		
		if float(each_guest.gatepass_discount_rate) < 0:
			guest_data["payment_person_amount"] = round(float(each_guest.gatepass_cost_per_pp) * ((100 - float(each_guest.gatepass_discount_rate)) / 100))
		else:
			guest_data["payment_person_amount"] = float(each_guest.gatepass_cost_per_pp)
		
		try:
			guest_data["payment_person_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			guest_data["payment_person_currency"] = ""
		guest_data["no_of_nights"] = each_guest.gatepass_no_of_nights
		guest_data["gatepass_payment_schedule"] = each_guest.gatepass_payment_schedule

		if batchfile:
			# guest_currency = requests.get(get_currency.format(each_guest.gatepass_guest_currency_at_time))
			guest_currency = get_details_currency(each_guest.gatepass_guest_currency_at_time)

			guest_data["destination"] = each_guest.destination
			
			## TODO: Get the UI to send destination if not set
			try:
				if each_guest.destination:
					guest_data["destination_outlet_code"], = db.session.query(Destination.code)\
																	.filter(Destination.gatepass_destination_public_id == each_guest.destination)\
																	.first()
				else:
					guest_data["destination_outlet_code"] = "O16"
			except Exception:
				guest_data["destination_outlet_code"] = "O16"
			
			guest_data["payment_person_income_code"] = each_guest.payment_person_income_code
			guest_data["payment_person_dept_code"] = each_guest.payment_person_dept_code
			guest_data["gatepass_guest_rate_at_time"] = float(each_guest.gatepass_guest_rate_at_time)
			guest_data["gatepass_guest_currency_at_time"] = each_guest.gatepass_guest_currency_at_time
			guest_data["currency"] = guest_currency.json()["data"][0]["currency_name"]
			
			price_minus_vat = round((float(each_guest.gatepass_guest_cost_at_time) / vat_calculation_amount), 2)
			vat = round((float(each_guest.gatepass_guest_cost_at_time) - price_minus_vat), 2)
			guest_data["price_minus_vat"] = price_minus_vat
			guest_data["vat"] = vat

			base_with_discount = float(1 - (float(each_guest.gatepass_discount_rate) / 100)) * (float(each_guest.gatepass_cost_per_pp) *\
								 float(each_guest.gatepass_no_of_nights)) * float(each_guest.gatepass_guest_count)
			rounded_base = math.ceil(base_with_discount)

			base_amount_minus_vat = round((rounded_base / vat_calculation_amount), 2)
			base_vat = round((rounded_base - base_amount_minus_vat), 2)
			guest_data["base_amount_minus_vat"] = base_amount_minus_vat
			guest_data["base_vat"] = base_vat

		total_without_discount = float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights) * float(each_guest.gatepass_guest_count)
		total_with_discount = math.ceil(float(1 - (float(each_guest.gatepass_discount_rate) / 100)) * total_without_discount)
		
		guest_data["payment_person_cost_before_discount"] = total_without_discount
		guest_data["payment_person_cost_after_discount"] = total_with_discount
		guest_discount.append(total_without_discount - total_with_discount)

		guest_total_without_discount.append(total_without_discount)
		guest_total_with_discount.append(total_with_discount)

		booking_total_without_discount.append(total_without_discount)
		booking_total_with_discount.append(total_with_discount)

		vat_total.append(total_with_discount)

		guest_array.append(guest_data)
		guest_sum.append(int(each_guest.gatepass_guest_count))

	return_data_dict["guests"] = guest_array
	return_data_dict["guest_total"] = sum(guest_sum)
	return_data_dict["guest_cost_before_discount"] = sum(guest_total_without_discount)
	return_data_dict["guest_cost_after_discount"] = sum(guest_total_with_discount)
	return_data_dict["guest_total_discount"] = sum(guest_discount)

	return_data_dict["gate_entry_total_cost"] = sum(guest_total_with_discount)

	# Getting the vehicle types and vehicle fee totals
	get_all_gatepass_vehicles = db.session.query(GatepassVehicle)\
										  .join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
										  .join(Vehicle, GatepassVehicle.gatepass_vehicle_type == Vehicle.vehicle_charge_public_id)\
										  .add_columns(GatepassVehicle.gatepass_vehicle_count, GatepassVehicle.gatepass_cost_per_vehicle, GatepassVehicle.gatepass_vehicle_public_id,\
													   GatepassVehicle.gatepass_vehicle_no_of_nights, GatepassVehicle.gatepass_vehicle_type, GatepassVehicle.gatepass_vehicle_discount_reason,\
													   GatepassVehicle.gatepass_vehicle_currency, GatepassVehicle.gatepass_vehicle_discount_rate, GatepassVehicle.gatepass_vehicle_currency_at_time,\
													   GatepassVehicle.gatepass_vehicle_cost_at_time, GatepassVehicle.gatepass_vehicle_rate_at_time, GatepassVehicle.gatepass_vehicle_currency,\
													   Vehicle.vehicle_charge_category, Vehicle.vehicle_charge_income_code, Vehicle.vehicle_charge_outlet_code,\
													   Vehicle.vehicle_charge_dept_code)\
										  .filter(GatepassVehicle.deletion_marker == None)\
										  .filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
										  .filter(Gatepass.deletion_marker == None)\
										  .filter(Gatepass.booking_id == booking_id)\
										  .filter(Gatepass.status != get_booking_status_id("Updated"))\
										  .options(FromCache(db_cache))\
										  .all()
	
	vehicle_array = []
	vehicle_sum = []
	vehicle_total_without_discount = []
	vehicle_total_with_discount = []
	vehicle_discount = []
	for each_vehicle in get_all_gatepass_vehicles:
		return_vehicle_data = {}
		
		cost_per_vehicle = currency_manager(get_booking.currency, each_vehicle.gatepass_vehicle_currency, float(each_vehicle.gatepass_cost_per_vehicle), float(each_vehicle.gatepass_vehicle_rate_at_time))
		
		return_vehicle_data["vehicle_charge_public_id"] = each_vehicle.gatepass_vehicle_type
		return_vehicle_data["gatepass_vehicle_public_id"] = each_vehicle.gatepass_vehicle_public_id
		return_vehicle_data["vehicle_charge_category"] = each_vehicle.vehicle_charge_category
		return_vehicle_data["vehicles"] = each_vehicle.gatepass_vehicle_count
		return_vehicle_data["vehicle_no_of_nights"] = each_vehicle.gatepass_vehicle_no_of_nights
		return_vehicle_data["discount"] = float(each_vehicle.gatepass_vehicle_discount_rate)
		return_vehicle_data["discount_reason"] = each_vehicle.gatepass_vehicle_discount_reason

		if float(each_vehicle.gatepass_vehicle_discount_rate) < 0:
			return_vehicle_data["vehicle_charge_category_cost"] = round(float(each_vehicle.gatepass_cost_per_vehicle) * ((100 - float(each_vehicle.gatepass_vehicle_discount_rate)) / 100))
		else:
			return_vehicle_data["vehicle_charge_category_cost"] = float(each_vehicle.gatepass_cost_per_vehicle)

		# currency = requests.get(get_currency.format(each_vehicle.gatepass_vehicle_currency))
		currency = get_details_currency(each_vehicle.gatepass_vehicle_currency)
		try:
			return_vehicle_data["vehicle_charge_cost_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			return_vehicle_data["vehicle_charge_cost_currency"] = ""

		if batchfile:
			# vehicle_currency = requests.get(get_currency.format(each_vehicle.gatepass_vehicle_currency_at_time))
			vehicle_currency = get_details_currency(each_vehicle.gatepass_vehicle_currency_at_time)

			return_vehicle_data["vehicle_charge_income_code"] = each_vehicle.vehicle_charge_income_code
			return_vehicle_data["vehicle_charge_outlet_code"] = each_vehicle.vehicle_charge_outlet_code
			return_vehicle_data["vehicle_charge_dept_code"] = each_vehicle.vehicle_charge_dept_code
			return_vehicle_data["gatepass_vehicle_rate_at_time"] = float(each_vehicle.gatepass_vehicle_rate_at_time)
			return_vehicle_data["gatepass_vehicle_currency_at_time"] = each_vehicle.gatepass_vehicle_currency_at_time
			try:
				return_vehicle_data["currency"] = vehicle_currency.json()["data"][0]["currency_name"]
			except Exception:
				return_vehicle_data["currency"] = ""

			price_minus_vat = round((float(each_vehicle.gatepass_vehicle_cost_at_time) / vat_calculation_amount), 2)
			vat = round((float(each_vehicle.gatepass_vehicle_cost_at_time) - price_minus_vat), 2)
			return_vehicle_data["price_minus_vat"] = price_minus_vat
			return_vehicle_data["vat"] = vat

			base_with_discount = float(1 - (float(each_vehicle.gatepass_vehicle_discount_rate) / 100)) * (float(each_vehicle.gatepass_cost_per_vehicle) *\
								 float(each_vehicle.gatepass_vehicle_no_of_nights)) * float(each_vehicle.gatepass_vehicle_count)
			rounded_base = math.ceil(base_with_discount)

			base_amount_minus_vat = round((rounded_base / vat_calculation_amount), 2)
			base_vat = round((rounded_base - base_amount_minus_vat), 2)
			return_vehicle_data["base_amount_minus_vat"] = base_amount_minus_vat
			return_vehicle_data["base_vat"] = base_vat

		total_without_discount = float(each_vehicle.gatepass_cost_per_vehicle) * float(each_vehicle.gatepass_vehicle_no_of_nights) * float(each_vehicle.gatepass_vehicle_count)
		total_with_discount = math.ceil(float(1 - (float(each_vehicle.gatepass_vehicle_discount_rate) / 100)) * total_without_discount)

		return_vehicle_data["vehicle_cost_before_discount"] = total_without_discount
		return_vehicle_data["vehicle_cost_after_discount"] = total_with_discount
		vehicle_discount.append(total_without_discount - total_with_discount)

		vehicle_total_without_discount.append(total_without_discount)
		vehicle_total_with_discount.append(total_with_discount)

		booking_total_without_discount.append(total_without_discount)
		booking_total_with_discount.append(total_with_discount)

		vat_total.append(total_with_discount)

		vehicle_array.append(return_vehicle_data)
		vehicle_sum.append(each_vehicle.gatepass_vehicle_count)

	return_data_dict["vehicles"] = vehicle_array
	return_data_dict["vehicle_total"] = sum(vehicle_sum)
	return_data_dict["vehicle_cost_before_discount"] = sum(vehicle_total_without_discount)
	return_data_dict["vehicle_cost_after_discount"] = sum(vehicle_total_with_discount)
	return_data_dict["vehicle_total_discount"] = sum(vehicle_discount)

	## TODO: Handle negative discount amount display for the invoice
	# Getting inventory bookings
	inventory_bookings = db.session.query(Inventory)\
								   .filter(Inventory.deletion_marker == None)\
								   .filter(Inventory.status != get_booking_status_id("Updated"))\
								   .filter(Inventory.booking_id == booking_id)\
								   .options(FromCache(db_cache))\
								   .all()

	inventory_array = []
	inventory_sum = []
	inventory_total_without_discount = []
	inventory_total_with_discount = []
	inventory_discount = []
	for single_inventory in inventory_bookings:
		inventory_data = {}

		return_inventory = requests.get(get_inventory_details.format(single_inventory.inventory_id))
		# return_inventory = get_details_inventory(single_inventory.inventory_id)

		try:
			# return_inventory_pricing = requests.get(get_inventory_current_pricing.format(single_inventory.inventory_id))
			
			inventory_data["inventory_name"] = return_inventory.json()["data"][0]["name"]
			inventory_data["inventory_ranger_marker"] = return_inventory.json()["data"][0]["ranger_marker"]
		except (KeyError, ValueError, TypeError) as no_data:
			inventory_data["inventory_name"] = "The record cannot be found."
			inventory_data["inventory_ranger_marker"] = False
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			inventory_data["inventory_name"] = "Network error."
			inventory_data["inventory_ranger_marker"] = False

		adult_total_without_discount = float(single_inventory.inventory_cost_per_adult) * float(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults)
		adult_total_with_discount_rate = (1 - (float(single_inventory.inventory_discount_rate) / 100)) * adult_total_without_discount
		adult_total_with_special_discount = (1 - (float(single_inventory.inventory_commission_rate) / 100)) * adult_total_with_discount_rate
		if single_inventory.inventory_commission_rate > 0:
			adult_total = adult_total_with_discount_rate 
		else:
			adult_total = adult_total_with_special_discount
		adult_with_commission = (1 - (float(single_inventory.inventory_commission_rate) / 100)) * adult_total_without_discount

		child_total_without_discount = float(single_inventory.inventory_cost_per_child) * float(single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children)
		child_total_with_discount_rate = (1 - (float(single_inventory.inventory_discount_rate) / 100)) * child_total_without_discount
		child_total_with_special_discount = (1 - (float(single_inventory.inventory_commission_rate) / 100)) * child_total_with_discount_rate
		if single_inventory.inventory_commission_rate > 0:
			child_total = child_total_with_discount_rate
		else:
			child_total = child_total_with_special_discount
		child_with_commission = (1 - (float(single_inventory.inventory_commission_rate) / 100)) * child_total_without_discount
		
		inventory_data["inventory_id"] = single_inventory.inventory_id
		inventory_data["inventory_booking_public_id"] = single_inventory.inventory_booking_public_id
		inventory_data["inventory_booking_date"] = single_inventory.inventory_booking_date
		inventory_data["inventory_booking_guests"] = single_inventory.inventory_booking_adults + single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_adults + single_inventory.inventory_booking_extra_children
		inventory_data["inventory_booking_adults"] = single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults
		inventory_data["inventory_booking_children"] = single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children
		inventory_data["inventory_booking_extra_adults"] = single_inventory.inventory_booking_extra_adults
		inventory_data["inventory_booking_extra_children"] = single_inventory.inventory_booking_extra_children
		inventory_data["inventory_booking_adult_cost"] = round(adult_total)
		inventory_data["inventory_booking_cost_per_adult"] = float(single_inventory.inventory_cost_per_adult)
		inventory_data["inventory_booking_adult_cost_before_discounts"] = round(adult_total_without_discount)
		inventory_data["inventory_booking_adult_cost_after_discounts"] = adult_total
		inventory_data["inventory_booking_child_cost"] = round(child_total)
		inventory_data["inventory_booking_cost_per_child"] = float(single_inventory.inventory_cost_per_child)
		inventory_data["inventory_booking_child_cost_before_discounts"] = round(child_total_without_discount)
		inventory_data["inventory_booking_child_cost_after_discounts"] = round(child_total)
		inventory_data["inventory_booking_total_cost"] = round(adult_total + child_total)
		inventory_data["inventory_booking_total_cost_before_discounts"] = round(adult_total_without_discount + child_total_without_discount)
		inventory_data["inventory_booking_total_discount"] = round(adult_total_without_discount + child_total_without_discount) - round(adult_total + child_total)
		inventory_data["inventory_booking_discount_percentage"] = float(single_inventory.inventory_discount_rate)
		inventory_data["inventory_booking_commission_percentage"] = float(single_inventory.inventory_commission_rate)
		inventory_data["adult_with_commission"] = adult_with_commission
		inventory_data["child_with_commission"] = child_with_commission

		try:
			inventory_data["time_frame"] = single_inventory.inventory_timeslot

			if single_inventory.inventory_pickup_location:
				get_pickup_location = db.session.query(Location)\
												.filter(Location.pickup_location_public_id == single_inventory.inventory_pickup_location)\
												.first()
				
				inventory_data["pick_up_location"] = get_pickup_location.pickup_location_name
			
			else:
				inventory_data["pick_up_location"] = None

		except Exception:
			pass
		
		# currency = requests.get(get_currency.format(single_inventory.inventory_cost_currency))
		currency = get_details_currency(single_inventory.inventory_cost_currency)
		try:
			inventory_data["inventory_booking_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			inventory_data["inventory_booking_currency"] = ""

		if batchfile:
			# inventory_currency = requests.get(get_currency.format(single_inventory.inventory_currency_at_time))
			inventory_currency = get_details_currency(single_inventory.inventory_currency_at_time)
			
			try:
				inventory_data["inventory_income_code"] = return_inventory.json()["data"][0]["code"]
			except Exception:
				inventory_data["inventory_income_code"] = None

			try:
				inventory_data["inventory_outlet_code"] = return_inventory.json()["data"][0]["sub_code"]
			except Exception:
				inventory_data["inventory_outlet_code"] = None
			
			inventory_data["inventory_dept_analysis_code"] = single_inventory.inventory_analysis_code
			inventory_data["inventory_rate_at_time"] = float(single_inventory.inventory_rate_at_time)
			inventory_data["inventory_currency_at_time"] = single_inventory.inventory_currency_at_time
			inventory_data["currency"] = inventory_currency.json()["data"][0]["currency_name"]
			
			inventory_data["inventory_adult_cost_at_time"] = float(single_inventory.inventory_adult_cost_at_time)
			inventory_data["inventory_child_cost_at_time"] = float(single_inventory.inventory_child_cost_at_time)

			if return_inventory.json()["data"][0]["vat"]:
				adult_price_minus_vat = round((float(single_inventory.inventory_adult_cost_at_time) / vat_calculation_amount), 2)
				adult_vat = round((float(single_inventory.inventory_adult_cost_at_time) - adult_price_minus_vat), 2)
				child_price_minus_vat = round((float(single_inventory.inventory_child_cost_at_time) / vat_calculation_amount), 2)
				child_vat = round((float(single_inventory.inventory_child_cost_at_time) - child_price_minus_vat), 2)
				inventory_data["adult_price_minus_vat"] = adult_price_minus_vat
				inventory_data["adult_vat"] = adult_vat
				inventory_data["child_price_minus_vat"] = child_price_minus_vat
				inventory_data["child_vat"] = child_vat
				
				## Rounding off rather than rounding up
				base_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_adult))
				base_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_child))
				
				base_adult_price_minus_vat = round((base_adult_with_discount / vat_calculation_amount), 2)
				base_adult_vat = round((float(base_adult_with_discount) - base_adult_price_minus_vat), 2)
				base_child_price_minus_vat = round((base_child_with_discount / vat_calculation_amount), 2)
				base_child_vat = round((float(base_child_with_discount) - base_child_price_minus_vat), 2)
				inventory_data["base_adult_price_minus_vat"] = base_adult_price_minus_vat
				inventory_data["base_adult_vat"] = base_adult_vat
				inventory_data["base_child_price_minus_vat"] = base_child_price_minus_vat
				inventory_data["base_child_vat"] = base_child_vat
				
				try:
					report_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * (float(single_inventory.inventory_cost_per_adult) *\
													(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults)))
					report_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * (float(single_inventory.inventory_cost_per_child) *\
													(single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children)))
					
					report_adult_price_minus_vat = round((report_adult_with_discount / vat_calculation_amount), 2)
					report_adult_vat = round((float(report_adult_with_discount) - report_adult_price_minus_vat), 2)
					report_child_price_minus_vat = round((report_child_with_discount / vat_calculation_amount), 2)
					report_child_vat = round((float(report_child_with_discount) - report_child_price_minus_vat), 2)
					inventory_data["report_adult_price_minus_vat"] = report_adult_price_minus_vat
					inventory_data["report_adult_vat"] = report_adult_vat
					inventory_data["report_child_price_minus_vat"] = report_child_price_minus_vat
					inventory_data["report_child_vat"] = report_child_vat

				except Exception:
					pass

				inventory_data["vat"] = True
			else:
				## Rounding off rather than rounding up
				base_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_adult))
				base_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_child))
				
				inventory_data["adult_price"] = float(single_inventory.inventory_adult_cost_at_time)
				inventory_data["child_price"] = float(single_inventory.inventory_child_cost_at_time)

				inventory_data["base_adult_price"] = base_adult_with_discount
				inventory_data["base_child_price"] = base_child_with_discount

				inventory_data["vat"] = False
			
			if return_inventory.json()["data"][0]["categing_levy"]:
				adult_cater = round(((float(single_inventory.inventory_adult_cost_at_time) * 2) / levy_calculation_amount), 2)
				adult_price_minus_cater = round((float(single_inventory.inventory_adult_cost_at_time) - adult_cater), 2)
				child_cater = round((float(single_inventory.inventory_child_cost_at_time) / levy_calculation_amount), 2)
				child_price_minus_cater = round(((float(single_inventory.inventory_child_cost_at_time) * 2) - child_cater), 2)
				inventory_data["adult_price_minus_cater"] = adult_price_minus_cater
				inventory_data["adult_cater"] = adult_cater
				inventory_data["child_price_minus_cater"] = child_price_minus_cater
				inventory_data["child_cater"] = child_cater
				
				## Rounding off rather than rounding up
				base_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_adult))
				base_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_child))
				
				base_adult_cater = round(((base_adult_with_discount * 2) / levy_calculation_amount), 2)
				base_adult_price_minus_cater = round((base_adult_with_discount - base_adult_cater), 2)
				base_child_cater = round((base_child_with_discount / levy_calculation_amount), 2)
				base_child_price_minus_cater = round(((base_child_with_discount * 2) - base_child_cater), 2)
				inventory_data["base_adult_price_minus_cater"] = base_adult_price_minus_cater
				inventory_data["base_adult_cater"] = base_adult_cater
				inventory_data["base_child_price_minus_cater"] = base_child_price_minus_cater
				inventory_data["base_child_cater"] = base_child_cater

				try:
					report_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * (float(single_inventory.inventory_cost_per_adult) *\
													(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults)))
					report_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * (float(single_inventory.inventory_cost_per_child) *\
													(single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children)))

					report_adult_cater = round(((report_adult_with_discount * 2) / levy_calculation_amount), 2)
					report_adult_price_minus_cater = round((report_adult_with_discount - report_adult_cater), 2)
					report_child_cater = round((report_child_with_discount / levy_calculation_amount), 2)
					report_child_price_minus_cater = round((report_child_with_discount - report_child_cater), 2)
					inventory_data["report_adult_price_minus_cater"] = report_adult_price_minus_cater
					inventory_data["report_adult_cater"] = report_adult_cater
					inventory_data["report_child_price_minus_cater"] = report_child_price_minus_cater
					inventory_data["report_child_cater"] = report_child_cater

				except Exception:
					pass
				
				inventory_data["catering_levy"] = True
			else:
				# base_adult_with_discount = (1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_adult)
				# base_child_with_discount = (1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_child)
				
				# inventory_data["adult_price"] = float(single_inventory.inventory_adult_cost_at_time)
				# inventory_data["child_price"] = float(single_inventory.inventory_child_cost_at_time)
				
				# inventory_data["base_adult_price"] = base_adult_with_discount
				# inventory_data["base_child_price"] = base_child_with_discount
				
				inventory_data["catering_levy"] = False

			if return_inventory.json()["data"][0]["vat"] & return_inventory.json()["data"][0]["categing_levy"]:
				adult_price_minus_vat = round((float(single_inventory.inventory_adult_cost_at_time) / vat_calculation_amount), 2)
				adult_vat = round((float(single_inventory.inventory_adult_cost_at_time) - adult_price_minus_vat), 2)
				adult_cater = round(((float(single_inventory.inventory_adult_cost_at_time) * 2) / levy_calculation_amount), 2)
				child_price_minus_vat = round((float(single_inventory.inventory_child_cost_at_time) / vat_calculation_amount), 2)
				child_vat = round((float(single_inventory.inventory_child_cost_at_time) - child_price_minus_vat), 2)
				child_cater = round((float(single_inventory.inventory_child_cost_at_time) / levy_calculation_amount), 2)
				inventory_data["adult_cater"] = adult_cater
				inventory_data["adult_vat"] = adult_vat
				inventory_data["adult_minus_taxes"] = float(single_inventory.inventory_adult_cost_at_time) - (adult_cater + adult_vat)
				inventory_data["child_cater"] = child_cater
				inventory_data["child_vat"] = child_vat
				inventory_data["child_minus_taxes"] = float(single_inventory.inventory_child_cost_at_time) - (child_cater + child_vat)

				## Rounding off rather than rounding up
				base_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_adult))
				base_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * float(single_inventory.inventory_cost_per_child))
				
				base_adult_price_minus_vat = round((base_adult_with_discount / vat_calculation_amount), 2)
				base_adult_vat = round((base_adult_with_discount - base_adult_price_minus_vat), 2)
				base_adult_cater = round(((base_adult_with_discount * 2) / levy_calculation_amount), 2)
				base_child_price_minus_vat = round((base_child_with_discount / vat_calculation_amount), 2)
				base_child_vat = round((base_child_with_discount - base_child_price_minus_vat), 2)
				base_child_cater = round((base_child_with_discount / levy_calculation_amount), 2)
				inventory_data["base_adult_price_minus_taxes"] = float(single_inventory.inventory_cost_per_adult) - (base_adult_vat + base_adult_cater)
				inventory_data["base_adult_vat"] = base_adult_vat
				inventory_data["base_adult_cater"] = base_adult_cater
				inventory_data["base_child_price_minus_taxes"] = float(single_inventory.inventory_cost_per_child) - (base_child_vat + base_child_cater)
				inventory_data["base_child_vat"] = base_child_vat
				inventory_data["base_child_cater"] = base_child_cater

				try:
					report_adult_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * (float(single_inventory.inventory_cost_per_adult) *\
													(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults)))
					report_child_with_discount = round((1 - (float(single_inventory.inventory_discount_rate) / 100)) * (float(single_inventory.inventory_cost_per_child) *\
													(single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children)))
					
					report_adult_price_minus_vat = round((report_adult_with_discount / vat_calculation_amount), 2)
					report_adult_vat = round((float(report_adult_with_discount) - report_adult_price_minus_vat), 2)
					report_adult_cater = round(((report_adult_with_discount * 2) / levy_calculation_amount), 2)
					report_child_price_minus_vat = round((report_child_with_discount / vat_calculation_amount), 2)
					report_child_vat = round((float(report_child_with_discount) - report_child_price_minus_vat), 2)
					report_child_cater = round((report_child_with_discount / levy_calculation_amount), 2)
					inventory_data["report_adult_price_minus_taxes"] = report_adult_with_discount - (report_adult_vat + report_adult_cater)
					inventory_data["report_adult_vat"] = report_adult_vat
					inventory_data["report_adult_cater"] = report_adult_cater
					inventory_data["report_child_price_minus_taxes"] = report_child_with_discount - (report_child_vat + report_child_cater)
					inventory_data["report_child_vat"] = report_child_vat
					inventory_data["report_child_cater"] = report_child_cater
				
				except Exception:
					pass

				inventory_data["vat"] = True
				inventory_data["catering_levy"] = True

		inventory_total_without_discount.append(round(adult_total_without_discount + child_total_without_discount))
		inventory_total_with_discount.append(round(adult_total + child_total))
		
		without_discount = round(adult_total_without_discount + child_total_without_discount)
		with_discount = round(adult_total + child_total)
		inventory_discount.append(without_discount - with_discount)
		
		booking_total_without_discount.append(round(adult_total_without_discount + child_total_without_discount))
		booking_total_with_discount.append(round(adult_total + child_total))

		if return_inventory.json()["data"][0]["vat"]:
			vat_total.append(adult_total + child_total)

		if return_inventory.json()["data"][0]["categing_levy"]:
			catering_levy_total.append(adult_total + child_total)
		
		inventory_array.append(inventory_data)
		inventory_sum.append(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_adults + single_inventory.inventory_booking_extra_children)

	inventory_array = sorted(inventory_array, key = lambda order: order["inventory_booking_date"])
	
	return_data_dict["inventory_bookings"] = inventory_array
	return_data_dict["inventory_bookings_total"] = sum(inventory_sum)
	return_data_dict["inventory_bookings_cost_before_discount"] = sum(inventory_total_without_discount)
	return_data_dict["inventory_bookings_cost_after_discount"] = sum(inventory_total_with_discount)
	return_data_dict["inventory_total_discount"] = sum(inventory_discount)
	
	# Getting facility bookings
	facility_bookings = db.session.query(Facility)\
								  .filter(Facility.deletion_marker == None)\
								  .filter(Facility.status != get_booking_status_id("Updated"))\
								  .filter(Facility.booking_id == booking_id)\
								  .options(FromCache(db_cache))\
								  .all()

	facility_array = []
	facility_sum = []
	facility_total_without_discount = []
	facility_total_with_discount = []
	facility_discount = []
	for single_facility in facility_bookings:
		facility_data = {}

		# return_facility = requests.get(get_facility_details.format(single_facility.facility_id))
		return_facility = get_details_facility(single_facility.facility_id)

		try:
			facility_data["facility_name"] = return_facility.json()["data"][0]["name"]
		except (KeyError, ValueError, TypeError) as no_data:
			facility_data["facility_name"] = "The record cannot be found."
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			facility_data["facility_name"] = "N/A"

		try:
			facility_data["facility_type"] = return_facility.json()["data"][0]["facility_type_id"]
		except (KeyError, ValueError, TypeError) as no_data:
			facility_data["facility_type"] = "The record cannot be found."
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			facility_data["facility_type"] = "The information cannot be retrieved at this time."
		
		try:
			facility_data["accomodation_type"] = return_facility.json()["data"][0]["accomodation_type_id"]
		except (KeyError, ValueError, TypeError) as no_data:
			facility_data["accomodation_type"] = "The record cannot be found."
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			facility_data["accomodation_type"] = "The information cannot be retrieved at this time."

		try:
			facility_data["maximum_guests"] = return_facility.json()["data"][0]["maximum_guests"]
		except Exception:
			raise Exception(single_facility.facility_id + str(return_facility.json()))
		facility_data["facility_income_code"] = return_facility.json()["data"][0]["code"]
		facility_data["facility_outlet_code"] = return_facility.json()["data"][0]["outlet_analysis_code"]
		facility_data["facility_dept_analysis_code"] = return_facility.json()["data"][0]["analysis_code"]
		
		facility_data["facility_id"] = single_facility.facility_id
		facility_data["facility_booking_public_id"] = single_facility.facility_booking_public_id
		facility_data["facility_booking_check_in_date"] = single_facility.facility_booking_check_in_date
		facility_data["facility_booking_check_out_date"] = single_facility.facility_booking_check_out_date
		facility_data["facility_booking_guests"] = single_facility.facility_booking_adults + single_facility.facility_booking_children + single_facility.facility_booking_extra_adults + single_facility.facility_booking_extra_children
		facility_data["facility_booking_adults"] = single_facility.facility_booking_adults
		facility_data["facility_booking_children"] = single_facility.facility_booking_children
		facility_data["facility_booking_extra_adults"] = single_facility.facility_booking_extra_adults
		facility_data["facility_booking_extra_children"] = single_facility.facility_booking_extra_children
		facility_data["facility_booking_discount_percentage"] = float(single_facility.facility_discount_rate)
		facility_data["facility_booking_commission_percentage"] = float(single_facility.facility_commission_rate)
		facility_data["facility_catering_type"] = single_facility.facility_catering_type
		
		if single_facility.facility_catering_type:
			get_catering_type = db.session.query(FacilityPricing)\
										  .filter(FacilityPricing.facility_pricing_type_public_id == single_facility.facility_catering_type)\
										  .first()

			catering_type = get_catering_type.facility_pricing_name
			facility_data["facility_catering_type_name"] = catering_type

		else:
			facility_data["facility_catering_type_name"] = None

		# currency = requests.get(get_currency.format(single_facility.facility_cost_currency))
		currency = get_details_currency(single_facility.facility_cost_currency)
		try:
			facility_data["facility_booking_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			facility_data["facility_booking_currency"] = ""

		facility_array.append(facility_data)
		facility_sum.append(single_facility.facility_booking_adults + single_facility.facility_booking_children)

		facility_data["facility_no_of_nights"] = single_facility.facility_no_of_nights

		if single_facility.facility_no_of_nights == 1:
			facility_data["unit"] = "Night"
		elif single_facility.facility_no_of_nights > 1:
			facility_data["unit"] = "Nights"

		## TODO: Handle negative discount amount display for the invoice
		## Handling the various accommodation types
		if return_facility.json()["data"][0]["facility_type_id"] == "Accommodation"\
		or return_facility.json()["data"][0]["facility_type_id"] == "Accomodation"\
		or return_facility.json()["data"][0]["facility_type_id"] == "Acomodation":
			if return_facility.json()["data"][0]["accomodation_type_id"] == "Stables":
				max_capacity = return_facility.json()["data"][0]["maximum_guests"]
				
				if float(single_facility.facility_booking_adults + single_facility.facility_booking_children) <= float(max_capacity):
					fixed_rate = float(single_facility.facility_fixed_cost) * float(single_facility.facility_no_of_nights)\
								 + ((float(single_facility.facility_fixed_cost) / float(max_capacity)) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_adults))\
								 + (((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_children))
					if (single_facility.facility_booking_adults + single_facility.facility_booking_children) == 0:
						facility_fixed_cost_without_discount = ((float(single_facility.facility_fixed_cost) / float(max_capacity)) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_adults))\
															   + (((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_children))
						facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
						facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
						facility_fixed_cost = facility_fixed_cost_with_spec_discount
					else:
						facility_fixed_cost_without_discount = fixed_rate
						facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
						facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
						facility_fixed_cost = facility_fixed_cost_with_spec_discount

					no_of_rooms = 1

				elif float(single_facility.facility_booking_adults + single_facility.facility_booking_children) > float(max_capacity):
					divided_value = round(float(single_facility.facility_booking_adults + single_facility.facility_booking_children) / float(max_capacity))
					modulus = float(single_facility.facility_booking_adults + single_facility.facility_booking_children) % float(max_capacity)
					fixed_rate = (float(single_facility.facility_fixed_cost) * divided_value * float(single_facility.facility_no_of_nights)) + (float(single_facility.facility_fixed_cost) * modulus * float(single_facility.facility_no_of_nights))\
								 + ((float(single_facility.facility_fixed_cost) / float(max_capacity)) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_adults))\
								 + (((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_children))
					facility_fixed_cost_without_discount = fixed_rate
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					no_of_rooms = divided_value + modulus
				
				# fixed_with_adults = float(single_facility.facility_fixed_cost) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
				# fixed_with_children = float(single_facility.facility_fixed_cost) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
				# facility_fixed_cost_without_discount = fixed_with_adults + fixed_with_children
				# facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
				# facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
				# facility_fixed_cost = facility_fixed_cost_with_spec_discount
				
				adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
				adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
				adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
				adult_with_discount = adult_cost_with_spec_discount

				child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
				child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
				child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
				child_with_discount = child_cost_with_spec_discount

				## Using fixed cost rather than adult cost intentionally
				extra_adult_cost_without_discount = float(single_facility.facility_fixed_cost) * float(single_facility.facility_booking_extra_adults) * float(single_facility.facility_no_of_nights)
				extra_adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * extra_adult_cost_without_discount
				extra_adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * extra_adult_cost_with_discount_rate
				extra_adult_with_discount = extra_adult_cost_with_discount_rate

				## Using fixed cost rather than child cost intentionally
				extra_child_cost_without_discount = ((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_booking_extra_children) * float(single_facility.facility_no_of_nights)
				extra_child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * extra_child_cost_without_discount
				extra_child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * extra_child_cost_with_discount_rate
				extra_child_with_discount = extra_child_cost_with_discount_rate

				facility_data["facility_booking_extra_adult_cost"] = round(extra_adult_with_discount)
				facility_data["facility_booking_extra_child_cost"] = round(extra_child_with_discount, 2)
				
				facility_data["facility_booking_fixed_cost"] = float(facility_fixed_cost)
				facility_data["facility_booking_fixed_cost_before_discount"] = float(facility_fixed_cost_without_discount)
				facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
				facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
				facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
				facility_data["facility_booking_child_cost"] = round(child_with_discount)
				facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
				facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
				facility_data["facility_booking_discount"] = round(float(facility_fixed_cost_without_discount) - float(facility_fixed_cost), 2)
				facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
				facility_data["facility_booking_total_cost"] = facility_fixed_cost
				facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount
				
				facility_data["no_of_rooms"] = int(no_of_rooms)
				facility_data["cost_per_room"] = float(single_facility.facility_fixed_cost)
				facility_data["cost_per_extra_adult"] = (float(single_facility.facility_fixed_cost) / float(max_capacity))
				facility_data["cost_per_extra_child"] = round(((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2), 2)

				if "Chef" in return_facility.json()["data"][0]["name"]:
					facility_data["chef_room_marker"] = "Chef"
				else:
					facility_data["chef_room_marker"] = "Room"

				# if batchfile:
				# 	facility_currency = requests.get(get_currency.format(single_facility.facility_currency_at_time))
					
				# 	try:
				# 		facility_data["facility_income_code"] = return_facility.json()["data"][0]["code"]
				# 	except Exception:
				# 		facility_data["facility_income_code"] = None

				# 	try:
				# 		facility_data["facility_outlet_code"] = return_facility.json()["data"][0]["outlet_analysis_code"]
				# 	except Exception:
				# 		facility_data["facility_outlet_code"] = None

				# 	facility_data["facility_analysis_code"] = single_facility.facility_analysis_code
				# 	facility_data["facility_rate_at_time"] = float(single_facility.facility_rate_at_time)
				# 	facility_data["facility_currency_at_time"] = single_facility.facility_currency_at_time
				# 	facility_data["currency"] = facility_currency.json()["data"][0]["currency_name"]
					
				# 	facility_data["facility_adult_cost_at_time"] = float(single_facility.facility_adult_cost_at_time)
				# 	facility_data["facility_child_cost_at_time"] = float(single_facility.facility_child_cost_at_time)

				facility_total_without_discount.append(facility_fixed_cost_without_discount)
				facility_total_with_discount.append(facility_fixed_cost)

				without_discount = facility_fixed_cost_without_discount
				with_discount = facility_fixed_cost
				facility_discount.append(without_discount - with_discount)
				
				booking_total_without_discount.append(facility_fixed_cost_without_discount)
				booking_total_with_discount.append(facility_fixed_cost)

				base_adult_cost_without_discount = float(single_facility.facility_fixed_cost) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
				base_adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * base_adult_cost_without_discount
				base_adult_with_discount = base_adult_cost_with_discount_rate

				base_child_cost_without_discount = float(single_facility.facility_fixed_cost) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
				base_child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * base_child_cost_without_discount
				base_child_with_discount = base_child_cost_with_discount_rate

				base_extra_adult_cost_without_discount = (float(single_facility.facility_fixed_cost) / float(max_capacity)) * float(single_facility.facility_booking_extra_adults) * float(single_facility.facility_no_of_nights)
				base_extra_adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * base_extra_adult_cost_without_discount
				base_extra_adult_with_discount = base_extra_adult_cost_with_discount_rate

				base_extra_child_cost_without_discount = ((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_booking_extra_children) * float(single_facility.facility_no_of_nights)
				base_extra_child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * base_extra_child_cost_without_discount
				base_extra_child_with_discount = math.ceil(base_extra_child_cost_with_discount_rate)

				if float(single_facility.facility_booking_adults + single_facility.facility_booking_children) <= float(max_capacity):
					if (single_facility.facility_booking_adults + single_facility.facility_booking_children) == 0:
						base_fixed_rate = float(single_facility.facility_fixed_cost) * float(single_facility.facility_no_of_nights) * 0
					else:
						base_fixed_rate = float(single_facility.facility_fixed_cost) * float(single_facility.facility_no_of_nights)
					
					base_facility_fixed_cost_without_discount = base_fixed_rate
					base_facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * base_facility_fixed_cost_without_discount
					base_facility_fixed_cost = base_facility_fixed_cost_with_discount_rate

				elif float(single_facility.facility_booking_adults + single_facility.facility_booking_children) > float(max_capacity):
					divided_value = round(float(single_facility.facility_booking_adults + single_facility.facility_booking_children) / float(max_capacity))
					modulus = float(single_facility.facility_booking_adults + single_facility.facility_booking_children) % float(max_capacity)
					base_fixed_rate = (float(single_facility.facility_fixed_cost) * divided_value * float(single_facility.facility_no_of_nights)) + (float(single_facility.facility_fixed_cost) * modulus * float(single_facility.facility_no_of_nights))
					base_facility_fixed_cost_without_discount = base_fixed_rate
					base_facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * base_facility_fixed_cost_without_discount
					base_facility_fixed_cost = base_facility_fixed_cost_with_discount_rate

				if return_facility.json()["data"][0]["vat"]:
					vat_total.append(facility_fixed_cost + extra_adult_with_discount + extra_child_with_discount)
					facility_data["vat"] = True

					base_fixed_minus_vat = round((float(base_facility_fixed_cost) / vat_calculation_amount), 2)
					base_fixed_vat = round((float(base_facility_fixed_cost) - base_fixed_minus_vat), 2)
					
					base_adult_minus_vat = round((float(base_adult_with_discount) / vat_calculation_amount), 2)
					base_adult_vat = round((float(base_adult_with_discount) - base_adult_minus_vat), 2)
					base_child_minus_vat = round((float(base_child_with_discount) / vat_calculation_amount), 2)
					base_child_vat = round((float(base_child_with_discount) - base_child_minus_vat), 2)

					base_extra_adult_minus_vat = round((float(base_extra_adult_with_discount) / vat_calculation_amount), 2)
					base_extra_adult_vat = round((float(base_extra_adult_with_discount) - base_extra_adult_minus_vat), 2)
					base_extra_child_minus_vat = round((float(base_extra_child_with_discount) / vat_calculation_amount), 2)
					base_extra_child_vat = round((float(base_extra_child_with_discount) - base_extra_child_minus_vat), 2)

					facility_data["base_fixed_minus_vat"] = float(base_fixed_minus_vat)
					facility_data["base_fixed_vat"] = float(base_fixed_vat)
					
					facility_data["base_adult_minus_vat"] = float(base_adult_minus_vat)
					facility_data["base_adult_vat"] = float(base_adult_vat)
					facility_data["base_child_minus_vat"] = float(base_child_minus_vat)
					facility_data["base_child_vat"] = float(base_child_vat)

					facility_data["base_extra_adult_minus_vat"] = float(base_extra_adult_minus_vat)
					facility_data["base_extra_adult_vat"] = float(base_extra_adult_vat)
					facility_data["base_extra_child_minus_vat"] = float(base_extra_child_minus_vat)
					facility_data["base_extra_child_vat"] = float(base_extra_child_vat)
				
				else:
					facility_data["vat"] = False

					facility_data["base_fixed"] = base_facility_fixed_cost
					
					facility_data["base_adult"] = base_adult_with_discount
					facility_data["base_child"] = base_child_with_discount

					facility_data["base_extra_adult"] = base_extra_adult_with_discount
					facility_data["base_extra_child"] = base_extra_child_with_discount

				if return_facility.json()["data"][0]["catering_levy"]:
					catering_levy_total.append(facility_fixed_cost + extra_adult_with_discount + extra_child_with_discount)
					facility_data["catering_levy"] = True

					base_fixed_cater = round(((float(base_facility_fixed_cost) * 2) / levy_calculation_amount), 2)
					base_fixed_price_minus_cater = round((float(base_facility_fixed_cost) - base_fixed_cater), 2)
					
					base_adult_cater = round(((float(base_adult_with_discount) * 2) / levy_calculation_amount), 2)
					base_adult_price_minus_cater = round((float(base_adult_with_discount) - base_adult_cater), 2)
					base_child_cater = round((float(base_child_with_discount) / levy_calculation_amount), 2)
					base_child_price_minus_cater = round(((float(base_child_with_discount) * 2) - base_child_cater), 2)

					base_extra_adult_cater = round(((float(base_extra_adult_with_discount) * 2) / levy_calculation_amount), 2)
					base_extra_adult_price_minus_cater = round((float(base_extra_adult_with_discount) - base_extra_adult_cater), 2)
					base_extra_child_cater = round((float(base_extra_child_with_discount) / levy_calculation_amount), 2)
					base_extra_child_price_minus_cater = round(((float(base_extra_child_with_discount) * 2) - base_extra_child_cater), 2)

					facility_data["base_fixed_minus_cater"] = float(base_fixed_price_minus_cater)
					facility_data["base_fixed_cater"] = float(base_fixed_cater)

					facility_data["base_adult_minus_cater"] = float(base_adult_price_minus_cater)
					facility_data["base_adult_cater"] = float(base_adult_cater)
					facility_data["base_child_minus_cater"] = float(base_child_price_minus_cater)
					facility_data["base_child_cater"] = float(base_child_cater)

					facility_data["base_extra_adult_minus_cater"] = float(base_extra_adult_price_minus_cater)
					facility_data["base_extra_adult_cater"] = float(base_extra_adult_cater)
					facility_data["base_extra_child_minus_cater"] = float(base_extra_child_price_minus_cater)
					facility_data["base_extra_child_cater"] = float(base_extra_child_cater)
				
				else:
					facility_data["catering_levy"] = False

				if return_facility.json()["data"][0]["vat"] & return_facility.json()["data"][0]["catering_levy"]:
					facility_data["vat"] = True
					facility_data["catering_levy"] = True
					
					base_fixed_minus_vat = round((float(base_facility_fixed_cost) / vat_calculation_amount), 2)
					base_fixed_vat = round((float(base_facility_fixed_cost) - base_fixed_minus_vat), 2)
					base_fixed_cater = round(((float(base_facility_fixed_cost) * 2) / levy_calculation_amount), 2)
					
					base_adult_minus_vat = round((float(base_adult_with_discount) / vat_calculation_amount), 2)
					base_adult_vat = round((float(base_adult_with_discount) - base_adult_minus_vat), 2)
					base_adult_cater = round(((float(base_adult_with_discount) * 2) / levy_calculation_amount), 2)
					base_child_minus_vat = round((float(base_child_with_discount) / vat_calculation_amount), 2)
					base_child_vat = round((float(base_child_with_discount) - base_child_minus_vat), 2)
					base_child_cater = round(((float(base_child_with_discount) * 2) / levy_calculation_amount), 2)

					base_extra_adult_minus_vat = round((float(base_extra_adult_with_discount) / vat_calculation_amount), 2)
					base_extra_adult_vat = round((float(base_extra_adult_with_discount) - base_extra_adult_minus_vat), 2)
					base_extra_adult_cater = round(((float(base_extra_adult_with_discount) * 2) / levy_calculation_amount), 2)
					base_extra_child_minus_vat = round((float(base_extra_child_with_discount) / vat_calculation_amount), 2)
					base_extra_child_vat = round((float(base_extra_child_with_discount) - base_extra_child_minus_vat), 2)
					base_extra_child_cater = round(((float(base_extra_child_with_discount) * 2) / levy_calculation_amount), 2)
					
					facility_data["base_fixed_cater"] = float(base_fixed_cater)
					facility_data["base_fixed_vat"] = float(base_fixed_vat)
					facility_data["base_fixed_minus_taxes"] = float(base_facility_fixed_cost) - (base_fixed_cater + base_fixed_vat)
					
					facility_data["base_adult_cater"] = float(base_adult_cater)
					facility_data["base_adult_vat"] = float(base_adult_vat)
					facility_data["base_adult_minus_taxes"] = float(base_adult_with_discount) - (base_adult_cater + base_adult_vat)
					facility_data["base_child_cater"] = float(base_child_cater)
					facility_data["base_child_vat"] = float(base_child_vat)
					facility_data["base_child_minus_taxes"] = float(base_child_with_discount) - (base_child_cater + base_child_vat)

					facility_data["base_extra_adult_cater"] = float(base_extra_adult_cater)
					facility_data["base_extra_adult_vat"] = float(base_extra_adult_vat)
					facility_data["base_extra_adult_minus_taxes"] = float(base_extra_adult_with_discount) - (base_extra_adult_cater + base_extra_adult_vat)
					facility_data["base_extra_child_cater"] = float(base_extra_child_cater)
					facility_data["base_extra_child_vat"] = float(base_extra_child_vat)
					facility_data["base_extra_child_minus_taxes"] = float(base_extra_child_with_discount) - (base_extra_child_cater + base_extra_child_vat)
			
			elif return_facility.json()["data"][0]["accomodation_type_id"] == "Pelican":
				# Commented out on 7-11-19 since the key 'price' doesn't appear to be used anywhere
				# facility_data["price"] = return_facility.json()["data"][0]["price"]
				
				if single_facility.facility_special_price:
					special_price = float(single_facility.facility_special_price) * float(single_facility.facility_no_of_nights)
					over_limit = (float(single_facility.facility_booking_adults) + float(single_facility.facility_booking_children)) - float(return_facility.json()["data"][0]["group_price"][0]["maximum_number"])
					special_increase_cost = over_limit * float(single_facility.facility_special_price_unit_increase) * float(single_facility.facility_no_of_nights)
					
					facility_fixed_cost_without_discount = special_price + special_increase_cost
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
					adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
					adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
					adult_with_discount = adult_cost_with_spec_discount

					child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
					child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
					child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
					child_with_discount = child_cost_with_spec_discount

					facility_data["facility_booking_fixed_cost"] = float(facility_fixed_cost)
					facility_data["facility_booking_fixed_cost_before_discount"] = float(facility_fixed_cost_without_discount)
					facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
					facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
					facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
					facility_data["facility_booking_child_cost"] = round(child_with_discount)
					facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
					facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
					facility_data["facility_booking_discount"] = round(float(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount) - float(facility_fixed_cost + adult_with_discount + child_with_discount), 2)
					facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
					facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
					facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount

					facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					without_discount = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount
					with_discount = facility_fixed_cost + adult_with_discount + child_with_discount
					facility_discount.append(without_discount - with_discount)
					
					booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					if return_facility.json()["data"][0]["vat"]:
						vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)
						facility_data["vat"] = True
					else:
						facility_data["vat"] = False

					if return_facility.json()["data"][0]["catering_levy"]:
						catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)
						facility_data["catering_levy"] = True
					else:
						facility_data["catering_levy"] = False

				else:
					facility_fixed_cost_without_discount = float(single_facility.facility_fixed_cost)
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
					adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
					adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
					adult_with_discount = adult_cost_with_spec_discount

					child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
					child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
					child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
					child_with_discount = child_cost_with_spec_discount
					
					facility_data["facility_booking_fixed_cost"] = float(facility_fixed_cost)
					facility_data["facility_booking_fixed_cost_before_discount"] = float(facility_fixed_cost_without_discount)
					facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
					facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
					facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
					facility_data["facility_booking_child_cost"] = round(child_with_discount)
					facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
					facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
					facility_data["facility_booking_discount"] = round(float(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount) - float(facility_fixed_cost + adult_with_discount + child_with_discount), 2)
					facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
					facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
					facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount

					facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					without_discount = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount
					with_discount = facility_fixed_cost + adult_with_discount + child_with_discount
					facility_discount.append(without_discount - with_discount)
					
					booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					if return_facility.json()["data"][0]["vat"]:
						vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)
						# facility_data["vat"] = True
					else:
						# facility_data["vat"] = False
						pass

					if return_facility.json()["data"][0]["catering_levy"]:
						catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)
						# facility_data["catering_levy"] = True
					else:
						# facility_data["catering_levy"] = False
						pass

					if batchfile:
						base_adult = adult_cost_with_discount_rate
						base_child = child_cost_with_discount_rate
						
						if return_facility.json()["data"][0]["vat"]:
							base_adult_minus_vat = round((float(base_adult) / vat_calculation_amount), 2)
							base_adult_vat = base_adult - base_adult_minus_vat

							base_child_minus_vat = round((float(base_child) / vat_calculation_amount), 2)
							base_child_vat = base_child - base_child_minus_vat
							
							facility_data["vat"] = True

							facility_data["base_adult_minus_vat"] = base_adult_minus_vat
							facility_data["base_adult_vat"] = base_adult_vat
							facility_data["base_child_minus_vat"] = base_child_minus_vat
							facility_data["base_child_vat"] = base_child_vat
						
						else:
							facility_data["vat"] = False

							facility_data["base_adult"] = base_adult
							facility_data["base_child"] = base_child

						if return_facility.json()["data"][0]["catering_levy"]:
							base_adult_cater = round(((float(base_adult) * 2) / levy_calculation_amount), 2)
							base_adult_minus_cater = round((base_adult - base_adult_cater), 2)
							
							base_child_cater = round(((float(base_child) * 2) / levy_calculation_amount), 2)
							base_child_minus_cater = round((base_child - base_child_cater), 2)
							
							facility_data["catering_levy"] = True

							facility_data["base_adult_minus_cater"] = base_adult_minus_cater
							facility_data["base_adult_cater"] = base_adult_cater
							facility_data["base_child_minus_cater"] = base_child_minus_cater
							facility_data["base_child_cater"] = base_child_cater

						else:
							facility_data["catering_levy"] = False
						
						if return_facility.json()["data"][0]["vat"] & return_facility.json()["data"][0]["catering_levy"]:
							base_adult_minus_vat = round((float(base_adult) / vat_calculation_amount), 2)
							base_adult_vat = base_adult - base_adult_minus_vat
							base_adult_cater = round(((float(base_adult) * 2) / levy_calculation_amount), 2)

							base_child_minus_vat = round((float(base_child) / vat_calculation_amount), 2)
							base_child_vat = base_child - base_child_minus_vat
							base_child_cater = round(((float(base_child) * 2) / levy_calculation_amount), 2)
							
							facility_data["vat"] = True
							facility_data["catering_levy"] = True
							
							facility_data["base_adult_minus_taxes"] = base_adult - (base_adult_vat + base_adult_cater)
							facility_data["base_adult_cater"] = base_adult_cater
							facility_data["base_adult_vat"] = base_adult_vat
							facility_data["base_child_minus_taxes"] = base_child - (base_child_vat + base_child_cater)
							facility_data["base_child_cater"] = base_child_cater
							facility_data["base_child_vat"] = base_child_vat
		
		## Handling camping
		elif return_facility.json()["data"][0]["facility_type_id"] == "Camping Sites"\
		or return_facility.json()["data"][0]["facility_type_id"] == "Campingsites"\
		or return_facility.json()["data"][0]["facility_type_id"] == "CampingSites":
			if single_facility.facility_no_of_nights/7 <= 1:
				facility_fixed_cost_without_discount = float(single_facility.facility_fixed_cost)

				facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
				facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
				facility_fixed_cost = facility_fixed_cost_with_spec_discount
			
			elif single_facility.facility_no_of_nights/7 > 1:
				weeks_count = math.floor(single_facility.facility_no_of_nights/7) + 1
				
				facility_fixed_cost_without_discount = float(single_facility.facility_fixed_cost) * weeks_count
				facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
				facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
				facility_fixed_cost = facility_fixed_cost_with_spec_discount

			adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults + single_facility.facility_booking_extra_adults) * float(single_facility.facility_no_of_nights)
			adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
			adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
			adult_with_discount = adult_cost_with_spec_discount

			child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children + single_facility.facility_booking_extra_children) * float(single_facility.facility_no_of_nights)
			child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
			child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
			child_with_discount = child_cost_with_spec_discount
			
			facility_data["facility_booking_fixed_cost"] = round(float(facility_fixed_cost))
			facility_data["facility_booking_fixed_cost_before_discount"] = round(float(facility_fixed_cost_without_discount))
			facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
			
			if float(single_facility.facility_discount_rate) < 0:
				facility_data["facility_booking_cost_per_adult"] = round(float(single_facility.facility_cost_per_adult) * ((100 - float(single_facility.facility_discount_rate)) / 100))
			else:
				facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
			
			facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
			facility_data["facility_booking_child_cost"] = round(child_with_discount)

			if float(single_facility.facility_discount_rate) < 0:
				facility_data["facility_booking_cost_per_child"] = round(float(single_facility.facility_cost_per_child) * ((100 - float(single_facility.facility_discount_rate)) / 100))
			else:
				facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
			
			facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
			facility_data["facility_booking_discount"] = round(float(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount) - float(facility_fixed_cost + adult_with_discount + child_with_discount), 2)
			facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
			facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
			facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount

			if batchfile:
				# facility_currency = requests.get(get_currency.format(single_facility.facility_currency_at_time))
				facility_currency = get_details_currency(single_facility.facility_currency_at_time)
				
				try:
					facility_data["facility_income_code"] = return_facility.json()["data"][0]["code"]
				except Exception:
					facility_data["facility_income_code"] = None

				try:
					facility_data["facility_outlet_code"] = return_facility.json()["data"][0]["outlet_analysis_code"]
				except Exception:
					facility_data["facility_outlet_code"] = None

				facility_data["facility_analysis_code"] = single_facility.facility_analysis_code
				facility_data["facility_rate_at_time"] = float(single_facility.facility_rate_at_time)
				facility_data["facility_currency_at_time"] = single_facility.facility_currency_at_time
				facility_data["currency"] = facility_currency.json()["data"][0]["currency_name"]
				
				facility_data["facility_fixed_cost_at_time"] = float(single_facility.facility_fixed_cost_at_time)
				facility_data["facility_adult_cost_at_time"] = float(single_facility.facility_adult_cost_at_time)
				facility_data["facility_child_cost_at_time"] = float(single_facility.facility_child_cost_at_time)

				if return_facility.json()["data"][0]["vat"]:
					fixed_price_minus_vat = round((float(single_facility.facility_fixed_cost) / vat_calculation_amount), 2)
					fixed_vat = round((float(single_facility.facility_fixed_cost) - fixed_price_minus_vat), 2)
					adult_price_minus_vat = round((float(single_facility.facility_cost_per_adult) / vat_calculation_amount), 2)
					adult_vat = round((float(single_facility.facility_cost_per_adult) - adult_price_minus_vat), 2)
					child_price_minus_vat = round((float(single_facility.facility_cost_per_child) / vat_calculation_amount), 2)
					child_vat = round((float(single_facility.facility_cost_per_child) - child_price_minus_vat), 2)
					facility_data["fixed_price_minus_vat"] = fixed_price_minus_vat
					facility_data["fixed_vat"] = fixed_vat
					facility_data["adult_price_minus_vat"] = adult_price_minus_vat
					facility_data["adult_vat"] = adult_vat
					facility_data["child_price_minus_vat"] = child_price_minus_vat
					facility_data["child_vat"] = child_vat
					
					days_to_weeks = single_facility.facility_no_of_nights % 7

					if days_to_weeks > 0:
						weeks = math.floor(single_facility.facility_no_of_nights / 7) + 1
					else:
						weeks = single_facility.facility_no_of_nights / 7
					
					base_fixed_cost = float(single_facility.facility_fixed_cost) * weeks
					base_fixed_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_fixed_cost
					base_fixed_cost_minus_vat = round((base_fixed_cost_with_discount / vat_calculation_amount), 2)
					base_fixed_cost_vat = round((base_fixed_cost_with_discount - base_fixed_cost_minus_vat), 2)

					base_adult_cost = float(single_facility.facility_cost_per_adult) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_adults + single_facility.facility_booking_extra_adults)
					base_adult_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_adult_cost
					base_adult_cost_minus_vat = round((base_adult_cost_with_discount / vat_calculation_amount), 2)
					base_adult_cost_vat = round((base_adult_cost_with_discount - base_adult_cost_minus_vat), 2)

					base_child_cost = float(single_facility.facility_cost_per_child) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_children + single_facility.facility_booking_extra_children)
					base_child_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_child_cost
					base_child_cost_minus_vat = round((base_child_cost_with_discount / vat_calculation_amount), 2)
					base_child_cost_vat = round((base_child_cost_with_discount - base_child_cost_minus_vat), 2)

					facility_data["base_fixed_cost_minus_vat"] = base_fixed_cost_minus_vat
					facility_data["base_fixed_cost_vat"] = base_fixed_cost_vat
					facility_data["base_adult_cost_minus_vat"] = base_adult_cost_minus_vat
					facility_data["base_adult_cost_vat"] = base_adult_cost_vat
					facility_data["base_child_cost_minus_vat"] = base_child_cost_minus_vat
					facility_data["base_child_cost_vat"] = base_child_cost_vat
					
					facility_data["vat"] = True
				else:
					days_to_weeks = single_facility.facility_no_of_nights % 7

					if days_to_weeks > 0:
						weeks = math.floor(single_facility.facility_no_of_nights / 7) + 1
					else:
						weeks = single_facility.facility_no_of_nights / 7
					
					base_fixed_cost = float(single_facility.facility_fixed_cost) * weeks
					base_fixed_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_fixed_cost

					base_adult_cost = float(single_facility.facility_cost_per_adult) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_adults + single_facility.facility_booking_extra_adults)
					base_adult_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_adult_cost

					base_child_cost = float(single_facility.facility_cost_per_child) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_children + single_facility.facility_booking_extra_children)
					base_child_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_child_cost

					facility_data["base_fixed_cost"] = base_fixed_cost_with_discount
					facility_data["base_adult_cost"] = base_adult_cost_with_discount
					facility_data["base_child_cost"] = base_child_cost_with_discount
					
					facility_data["vat"] = False

				if return_facility.json()["data"][0]["catering_levy"]:
					days_to_weeks = single_facility.facility_no_of_nights % 7

					if days_to_weeks > 0:
						weeks = math.floor(single_facility.facility_no_of_nights / 7) + 1
					else:
						weeks = single_facility.facility_no_of_nights / 7
					
					base_fixed_cost = float(single_facility.facility_fixed_cost) * weeks
					base_fixed_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_fixed_cost
					base_fixed_cost_cater = round(((float(base_fixed_cost_with_discount) * 2) / levy_calculation_amount), 2)
					base_fixed_cost_minus_cater = round((base_fixed_cost_with_discount - base_fixed_cost_cater), 2)

					base_adult_cost = float(single_facility.facility_cost_per_adult) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_adults + single_facility.facility_booking_extra_adults)
					base_adult_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_adult_cost
					base_adult_cost_cater = round(((float(base_adult_cost_with_discount) * 2) / levy_calculation_amount), 2)
					base_adult_cost_minus_cater = round((base_adult_cost_with_discount - base_adult_cost_cater), 2)

					base_child_cost = float(single_facility.facility_cost_per_child) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_children + single_facility.facility_booking_extra_children)
					base_child_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_child_cost
					base_child_cost_cater = round(((float(base_child_cost_with_discount) * 2) / levy_calculation_amount), 2)
					base_child_cost_minus_cater = round((base_child_cost_with_discount - base_child_cost_cater), 2)
					
					facility_data["catering_levy"] = True

					facility_data["base_fixed_cost_minus_cater"] = base_fixed_cost_minus_cater
					facility_data["base_fixed_cost_cater"] = base_fixed_cost_cater
					
					facility_data["base_adult_cost_minus_cater"] = base_adult_cost_minus_cater
					facility_data["base_adult_cost_cater"] = base_adult_cost_cater
					
					facility_data["base_child_cost_minus_cater"] = base_child_cost_minus_cater
					facility_data["base_child_cost_cater"] = base_child_cost_cater
				else:
					facility_data["catering_levy"] = False

				if return_facility.json()["data"][0]["vat"] & return_facility.json()["data"][0]["catering_levy"]:
					days_to_weeks = single_facility.facility_no_of_nights % 7

					if days_to_weeks > 0:
						weeks = math.floor(single_facility.facility_no_of_nights / 7) + 1
					else:
						weeks = single_facility.facility_no_of_nights / 7
					
					base_fixed_cost = float(single_facility.facility_fixed_cost) * weeks
					base_fixed_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_fixed_cost
					base_fixed_cost_minus_vat = round((base_fixed_cost_with_discount / vat_calculation_amount), 2)
					base_fixed_cost_vat = round((base_fixed_cost_with_discount - base_fixed_cost_minus_vat), 2)
					base_fixed_cost_cater = round(((float(base_fixed_cost_with_discount) * 2) / levy_calculation_amount), 2)

					base_adult_cost = float(single_facility.facility_cost_per_adult) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_adults + single_facility.facility_booking_extra_adults)
					base_adult_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_adult_cost
					base_adult_cost_minus_vat = round((base_adult_cost_with_discount / vat_calculation_amount), 2)
					base_adult_cost_vat = round((base_adult_cost_with_discount - base_adult_cost_minus_vat), 2)
					base_adult_cost_cater = round(((float(base_adult_cost_with_discount) * 2) / levy_calculation_amount), 2)

					base_child_cost = float(single_facility.facility_cost_per_child) * single_facility.facility_no_of_nights *\
									  float(single_facility.facility_booking_children + single_facility.facility_booking_extra_children)
					base_child_cost_with_discount = (1 - (float(single_facility.facility_discount_rate) / 100)) * base_child_cost
					base_child_cost_minus_vat = round((base_child_cost_with_discount / vat_calculation_amount), 2)
					base_child_cost_vat = round((base_child_cost_with_discount - base_child_cost_minus_vat), 2)
					base_child_cost_cater = round(((float(base_child_cost_with_discount) * 2) / levy_calculation_amount), 2)
					
					facility_data["vat"] = True
					facility_data["catering_levy"] = True

					facility_data["base_fixed_cost_minus_taxes"] = float(base_fixed_cost_with_discount) - (base_fixed_cost_vat + base_fixed_cost_cater)
					facility_data["base_fixed_cost_vat"] = base_fixed_cost_vat
					facility_data["base_fixed_cost_cater"] = base_fixed_cost_cater
					
					facility_data["base_adult_cost_minus_taxes"] = float(base_adult_cost_with_discount) - (base_adult_cost_vat + base_adult_cost_cater)
					facility_data["base_adult_cost_vat"] = base_adult_cost_vat
					facility_data["base_adult_cost_cater"] = base_adult_cost_cater
					
					facility_data["base_child_cost_minus_taxes"] = float(base_child_cost_with_discount) - (base_child_cost_vat + base_child_cost_cater)
					facility_data["base_child_cost_vat"] = base_child_cost_vat
					facility_data["base_child_cost_cater"] = base_child_cost_cater

			facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
			facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

			without_discount = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount
			with_discount = facility_fixed_cost + adult_with_discount + child_with_discount
			facility_discount.append(without_discount - with_discount)
			
			booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
			booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

			if return_facility.json()["data"][0]["vat"]:
				vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

			if return_facility.json()["data"][0]["catering_levy"]:
				catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

	facility_array = sorted(facility_array, key = lambda order: order["facility_booking_check_in_date"])
	
	return_data_dict["facility_bookings"] = facility_array
	return_data_dict["facility_bookings_total"] = sum(facility_sum)
	return_data_dict["facility_bookings_cost_before_discount"] = sum(facility_total_without_discount)
	return_data_dict["facility_bookings_cost_after_discount"] = sum(facility_total_with_discount)
	return_data_dict["facility_total_discount"] = sum(facility_discount)
	
	return_data_dict["booking_total_before_discount"] = sum(booking_total_without_discount)
	return_data_dict["total_cost"] = math.ceil(round(sum(booking_total_with_discount), 2))

	vat_summable = sum(vat_total)
	amount_minus_vat = vat_summable / vat_calculation_amount

	catering_levy_summable = sum(catering_levy_total)
	catering_levy_amount = (catering_levy_summable * 2) / levy_calculation_amount

	return_vat_data = {}
	return_vat_data["summable"] = round(vat_summable)
	return_vat_data["tax"] = round(vat_summable - amount_minus_vat)
	return_vat_data["after_tax"] = round(amount_minus_vat)
	
	return_catering_levy_data = {}
	return_catering_levy_data["summable"] = round(catering_levy_summable)
	return_catering_levy_data["tax"] = round(catering_levy_amount)
	return_catering_levy_data["after_tax"] = round(catering_levy_summable - catering_levy_amount)

	return_tax_data = {}
	return_tax_data["vat"] = round((vat_summable - amount_minus_vat), 2)
	return_tax_data["catering"] = round(catering_levy_amount, 2)
	return_tax_data["after_tax"] = round((sum(booking_total_with_discount) - ((vat_summable - amount_minus_vat) + catering_levy_amount)), 2)

	return_data_dict["vat"] = return_vat_data
	return_data_dict["catering_levy"] = return_catering_levy_data
	return_data_dict["tax_breakdown"] = return_tax_data

	return_data_dict["booking_type"] = get_booking.booking_type
	
	if batchfile:
		## Partner booking
		if get_booking.booking_type == "PB001A20":
			get_partner = db.session.query(Partner)\
									.filter(Partner.booking_id == booking_id)\
									.first()

			if get_partner:
				partner_info = requests.get(get_partner_details.format(get_partner.partner_id))

				return_data_dict["partner_booking"] = True
				## Booking in KES
				if get_booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
					return_data_dict["customer_code"] = partner_info.json()["customer_code"]
				## Booking in USD
				elif get_booking.currency == "6bf74e49-7ff6-4555-a080-15541d62204a":
					if "customer_code_usd" in partner_info.json():
						return_data_dict["customer_code"] = partner_info.json()["customer_code_usd"]
					else:
						return_data_dict["customer_code"] = partner_info.json()["customer_code"]

				return_data_dict["guest_booking"] = False
				return_data_dict["gate_booking"] = False
			else:
				return_data_dict["partner_booking"] = False

				return_data_dict["guest_booking"] = False
				return_data_dict["gate_booking"] = False
						
		## Guest booking via the public portal
		elif get_booking.booking_type == "GB601X10":
			return_data_dict["guest_booking"] = True
			## Booking in KES
			if get_booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
				return_data_dict["customer_code"] = "406B047"
			## Booking in USD
			elif get_booking.currency == "6bf74e49-7ff6-4555-a080-15541d62204a":
				return_data_dict["customer_code"] = "406B036"

			return_data_dict["partner_booking"] = False
			return_data_dict["gate_booking"] = False

		## Front office booking
		elif get_booking.booking_type == "FF9330A3":
			return_data_dict["gate_booking"] = True
			## Booking in KES
			if get_booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
				return_data_dict["customer_code"] = "406GA001"
			## Booking in USD
			elif get_booking.currency == "6bf74e49-7ff6-4555-a080-15541d62204a":
				return_data_dict["customer_code"] = "406GA002"

			return_data_dict["partner_booking"] = False
			return_data_dict["guest_booking"] = False
		
		## Member booking
		elif get_booking.booking_type == "7769748C":
			return_data_dict["gate_booking"] = True
			## Booking in KES
			if get_booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
				return_data_dict["customer_code"] = "406GA001"
			## Booking in USD
			elif get_booking.currency == "6bf74e49-7ff6-4555-a080-15541d62204a":
				return_data_dict["customer_code"] = "406GA002"

			return_data_dict["partner_booking"] = False
			return_data_dict["guest_booking"] = False

		## School booking
		elif get_booking.booking_type == "8D7E6504":
			return_data_dict["gate_booking"] = True
			## Booking in KES
			if get_booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
				return_data_dict["customer_code"] = "406GA001"
			## Booking in USD
			elif get_booking.currency == "6bf74e49-7ff6-4555-a080-15541d62204a":
				return_data_dict["customer_code"] = "406GA002"

			return_data_dict["partner_booking"] = False
			return_data_dict["guest_booking"] = False
		
		## Corporate booking
		elif get_booking.booking_type == "C3237489":
			return_data_dict["gate_booking"] = True
			## Booking in KES
			if get_booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
				return_data_dict["customer_code"] = "406GA001"
			## Booking in USD
			elif get_booking.currency == "6bf74e49-7ff6-4555-a080-15541d62204a":
				return_data_dict["customer_code"] = "406GA002"

			return_data_dict["partner_booking"] = False
			return_data_dict["guest_booking"] = False

		try:
			return_data_dict["currency_buying_rate_at_time"] = float(get_booking.currency_buying_rate_at_time)
		except Exception:
			return_data_dict["currency_buying_rate_at_time"] = 1
		
		try:
			return_data_dict["currency_selling_rate_at_time"] = float(get_booking.currency_selling_rate_at_time)
		except Exception:
			return_data_dict["currency_selling_rate_at_time"] = 1
		
	

def bookingTotalGraph(return_data_dict, booking_id, batchfile = None):
	get_booking = db.session.query(Booking)\
							.filter(Booking.booking_public_id == booking_id)\
							.options(FromCache(db_cache))\
							.first()
	
	# Booking currency
	currency = requests.get(get_currency.format(get_booking.currency))
	# Get VAT rate
	get_latest_vat = requests.get(get_latest_vat_rate)
	# Get catering levy rate
	get_latest_catering = requests.get(get_latest_cater_levy_rate)

	vat_rate = float(get_latest_vat.json()["data"][0]["vat_rate"])
	vat_calculation_amount = 1 + (vat_rate / 100)

	levy_rate = float(get_latest_catering.json()["data"][0]["levy_rate"])
	levy_calculation_amount = 100 + vat_rate + levy_rate
	
	booking_total_without_discount = []
	booking_total_with_discount = []

	vat_total = []
	catering_levy_total = []
	
	# Getting the guest types and conservancy fee totals
	get_all_gatepass_guests = db.session.query(GatepassGuest)\
										.join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
										.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
										.add_columns(Mandatory.payment_person, Mandatory.payment_public_id, Mandatory.payment_person_income_code,\
													 Mandatory.payment_person_dept_code,\
													 GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													 GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_discount_reason, GatepassGuest.gatepass_guest_rate_at_time,\
													 GatepassGuest.gatepass_guest_cost_at_time, GatepassGuest.gatepass_guest_currency_at_time, GatepassGuest.gatepass_currency,\
													 GatepassGuest.gatepass_payment_schedule,\
													 Gatepass.destination)\
										.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
										.filter(GatepassGuest.deletion_marker == None)\
										.filter(Gatepass.booking_id == booking_id)\
										.filter(Gatepass.status != get_booking_status_id("Updated"))\
										.options(FromCache(db_cache))\
										.all()

	guest_array = []
	guest_sum = []
	guest_total_without_discount = []
	guest_total_with_discount = []
	for each_guest in get_all_gatepass_guests:
		guest_data = {}
		
		cost_per_person = currency_manager(get_booking.currency, each_guest.gatepass_currency, float(each_guest.gatepass_cost_per_pp), float(each_guest.gatepass_guest_rate_at_time))
		
		guest_data["payment_public_id"] = each_guest.payment_public_id
		guest_data["payment_person"] = each_guest.payment_person
		guest_data["payment_guests"] = each_guest.gatepass_guest_count
		guest_data["payment_person_discount"] = float(each_guest.gatepass_discount_rate)
		guest_data["payment_person_discount_reason"] = each_guest.gatepass_discount_reason
		## Test and see
		guest_data["payment_person_amount"] = float(each_guest.gatepass_cost_per_pp)
		try:
			guest_data["payment_person_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			guest_data["payment_person_currency"] = ""
		guest_data["no_of_nights"] = each_guest.gatepass_no_of_nights
		guest_data["gatepass_payment_schedule"] = each_guest.gatepass_payment_schedule

		if batchfile:
			guest_currency = requests.get(get_currency.format(each_guest.gatepass_guest_currency_at_time))
			
			guest_data["destination"] = each_guest.destination
			
			## TODO: Get the UI to send destination if not set
			if each_guest.destination:
				guest_data["destination_outlet_code"], = db.session.query(Destination.code)\
																   .filter(Destination.gatepass_destination_public_id == each_guest.destination)\
																   .first()
			else:
				guest_data["destination_outlet_code"] = "To Fix"
			
			guest_data["payment_person_income_code"] = each_guest.payment_person_income_code
			guest_data["payment_person_dept_code"] = each_guest.payment_person_dept_code
			guest_data["gatepass_guest_rate_at_time"] = float(each_guest.gatepass_guest_rate_at_time)
			guest_data["gatepass_guest_currency_at_time"] = each_guest.gatepass_guest_currency_at_time
			guest_data["currency"] = guest_currency.json()["data"][0]["currency_name"]
			
			price_minus_vat = round((float(each_guest.gatepass_guest_cost_at_time) / vat_calculation_amount), 2)
			vat = round((float(each_guest.gatepass_guest_cost_at_time) - price_minus_vat), 2)
			guest_data["price_minus_vat"] = price_minus_vat
			guest_data["vat"] = vat

		## Test and see
		total_without_discount = float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights) * float(each_guest.gatepass_guest_count)
		total_with_discount = float(1 - (float(each_guest.gatepass_discount_rate) / 100)) * total_without_discount
		
		guest_data["payment_person_cost_before_discount"] = total_without_discount
		guest_data["payment_person_cost_after_discount"] = total_with_discount

		guest_total_without_discount.append(total_without_discount)
		guest_total_with_discount.append(total_with_discount)

		booking_total_without_discount.append(total_without_discount)
		booking_total_with_discount.append(total_with_discount)

		vat_total.append(total_with_discount)

		guest_array.append(guest_data)
		guest_sum.append(int(each_guest.gatepass_guest_count))

	return_data_dict["guests"] = guest_array
	return_data_dict["guest_total"] = sum(guest_sum)
	return_data_dict["guest_cost_before_discount"] = sum(guest_total_without_discount)
	return_data_dict["guest_cost_after_discount"] = sum(guest_total_with_discount)

	return_data_dict["gate_entry_total_cost"] = sum(guest_total_with_discount)

	# Getting the vehicle types and vehicle fee totals
	get_all_gatepass_vehicles = db.session.query(GatepassVehicle)\
										  .join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
										  .join(Vehicle, GatepassVehicle.gatepass_vehicle_type == Vehicle.vehicle_charge_public_id)\
										  .add_columns(GatepassVehicle.gatepass_vehicle_count, GatepassVehicle.gatepass_cost_per_vehicle,\
													   GatepassVehicle.gatepass_vehicle_no_of_nights, GatepassVehicle.gatepass_vehicle_type, GatepassVehicle.gatepass_vehicle_discount_reason,\
													   GatepassVehicle.gatepass_vehicle_currency, GatepassVehicle.gatepass_vehicle_discount_rate, GatepassVehicle.gatepass_vehicle_currency_at_time,\
													   GatepassVehicle.gatepass_vehicle_cost_at_time, GatepassVehicle.gatepass_vehicle_rate_at_time, GatepassVehicle.gatepass_vehicle_currency,\
													   Vehicle.vehicle_charge_category, Vehicle.vehicle_charge_income_code, Vehicle.vehicle_charge_outlet_code,\
													   Vehicle.vehicle_charge_dept_code)\
										  .filter(GatepassVehicle.deletion_marker == None)\
										  .filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
										  .filter(Gatepass.deletion_marker == None)\
										  .filter(Gatepass.booking_id == booking_id)\
										  .filter(Gatepass.status != get_booking_status_id("Updated"))\
										  .options(FromCache(db_cache))\
										  .all()
	
	vehicle_array = []
	vehicle_sum = []
	vehicle_total_without_discount = []
	vehicle_total_with_discount = []
	for each_vehicle in get_all_gatepass_vehicles:
		return_vehicle_data = {}
		
		cost_per_vehicle = currency_manager(get_booking.currency, each_vehicle.gatepass_vehicle_currency, float(each_vehicle.gatepass_cost_per_vehicle), float(each_vehicle.gatepass_vehicle_rate_at_time))
		
		return_vehicle_data["vehicle_charge_public_id"] = each_vehicle.gatepass_vehicle_type
		return_vehicle_data["vehicle_charge_category"] = each_vehicle.vehicle_charge_category
		return_vehicle_data["vehicles"] = each_vehicle.gatepass_vehicle_count
		## Test and see
		return_vehicle_data["vehicle_charge_category_cost"] = float(each_vehicle.gatepass_cost_per_vehicle)
		return_vehicle_data["vehicle_no_of_nights"] = each_vehicle.gatepass_vehicle_no_of_nights
		return_vehicle_data["discount"] = float(each_vehicle.gatepass_vehicle_discount_rate)
		return_vehicle_data["discount_reason"] = each_vehicle.gatepass_vehicle_discount_reason

		currency = requests.get(get_currency.format(each_vehicle.gatepass_vehicle_currency))
		try:
			return_vehicle_data["vehicle_charge_cost_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			return_vehicle_data["vehicle_charge_cost_currency"] = ""

		if batchfile:
			vehicle_currency = requests.get(get_currency.format(each_vehicle.gatepass_vehicle_currency_at_time))

			return_vehicle_data["vehicle_charge_income_code"] = each_vehicle.vehicle_charge_income_code
			return_vehicle_data["vehicle_charge_outlet_code"] = each_vehicle.vehicle_charge_outlet_code
			return_vehicle_data["vehicle_charge_dept_code"] = each_vehicle.vehicle_charge_dept_code
			return_vehicle_data["gatepass_vehicle_rate_at_time"] = float(each_vehicle.gatepass_vehicle_rate_at_time)
			return_vehicle_data["gatepass_vehicle_currency_at_time"] = each_vehicle.gatepass_vehicle_currency_at_time
			try:
				return_vehicle_data["currency"] = vehicle_currency.json()["data"][0]["currency_name"]
			except Exception:
				return_vehicle_data["currency"] = ""

			price_minus_vat = round((float(each_vehicle.gatepass_vehicle_cost_at_time) / vat_calculation_amount), 2)
			vat = round((float(each_vehicle.gatepass_vehicle_cost_at_time) - price_minus_vat), 2)
			return_vehicle_data["price_minus_vat"] = price_minus_vat
			return_vehicle_data["vat"] = vat

		## Test and see
		total_without_discount = float(each_vehicle.gatepass_cost_per_vehicle) * float(each_vehicle.gatepass_vehicle_no_of_nights) * float(each_vehicle.gatepass_vehicle_count)
		total_with_discount = float(1 - (float(each_vehicle.gatepass_vehicle_discount_rate) / 100)) * total_without_discount

		return_vehicle_data["vehicle_cost_before_discount"] = total_without_discount
		return_vehicle_data["vehicle_cost_after_discount"] = total_with_discount

		vehicle_total_without_discount.append(total_without_discount)
		vehicle_total_with_discount.append(total_with_discount)

		booking_total_without_discount.append(total_without_discount)
		booking_total_with_discount.append(total_with_discount)

		vat_total.append(total_with_discount)

		vehicle_array.append(return_vehicle_data)
		vehicle_sum.append(each_vehicle.gatepass_vehicle_count)

	return_data_dict["vehicles"] = vehicle_array
	return_data_dict["vehicle_total"] = sum(vehicle_sum)
	return_data_dict["vehicle_cost_before_discount"] = sum(vehicle_total_without_discount)
	return_data_dict["vehicle_cost_after_discount"] = sum(vehicle_total_with_discount)

	# Getting inventory bookings
	inventory_bookings = db.session.query(Inventory)\
								   .filter(Inventory.deletion_marker == None)\
								   .filter(Inventory.status != get_booking_status_id("Updated"))\
								   .filter(Inventory.booking_id == booking_id)\
								   .options(FromCache(db_cache))\
								   .all()

	inventory_array = []
	inventory_sum = []
	inventory_total_without_discount = []
	inventory_total_with_discount = []
	for single_inventory in inventory_bookings:
		inventory_data = {}

		adult_total_without_discount = float(single_inventory.inventory_cost_per_adult) * float(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults)
		adult_total_with_discount_rate = (1 - (float(single_inventory.inventory_discount_rate) / 100)) * adult_total_without_discount
		adult_total_with_special_discount = (1 - (float(single_inventory.inventory_commission_rate) / 100)) * adult_total_with_discount_rate
		adult_total = adult_total_with_special_discount

		child_total_without_discount = float(single_inventory.inventory_cost_per_child) * float(single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children)
		child_total_with_discount_rate = (1 - (float(single_inventory.inventory_discount_rate) / 100)) * child_total_without_discount
		child_total_with_special_discount = (1 - (float(single_inventory.inventory_commission_rate) / 100)) * child_total_with_discount_rate
		child_total = child_total_with_special_discount
		
		inventory_data["inventory_id"] = single_inventory.inventory_id
		inventory_data["inventory_booking_public_id"] = single_inventory.inventory_booking_public_id
		inventory_data["inventory_booking_date"] = single_inventory.inventory_booking_date
		inventory_data["inventory_booking_guests"] = single_inventory.inventory_booking_adults + single_inventory.inventory_booking_children
		inventory_data["inventory_booking_adults"] = single_inventory.inventory_booking_adults + single_inventory.inventory_booking_extra_adults
		inventory_data["inventory_booking_children"] = single_inventory.inventory_booking_children + single_inventory.inventory_booking_extra_children
		inventory_data["inventory_booking_extra_adults"] = single_inventory.inventory_booking_extra_adults
		inventory_data["inventory_booking_extra_children"] = single_inventory.inventory_booking_extra_children
		inventory_data["inventory_booking_adult_cost"] = round(adult_total)
		inventory_data["inventory_booking_cost_per_adult"] = float(single_inventory.inventory_cost_per_adult)
		inventory_data["inventory_booking_adult_cost_before_discounts"] = round(adult_total_without_discount)
		inventory_data["inventory_booking_adult_cost_after_discounts"] = adult_total
		inventory_data["inventory_booking_child_cost"] = round(child_total)
		inventory_data["inventory_booking_cost_per_child"] = float(single_inventory.inventory_cost_per_child)
		inventory_data["inventory_booking_child_cost_before_discounts"] = round(child_total_without_discount)
		inventory_data["inventory_booking_child_cost_after_discounts"] = round(child_total)
		inventory_data["inventory_booking_total_cost"] = round(adult_total + child_total)
		inventory_data["inventory_booking_total_cost_before_discounts"] = round(adult_total_without_discount + child_total_without_discount)
		inventory_data["inventory_booking_total_discount"] = round(adult_total_without_discount + child_total_without_discount) - round(adult_total + child_total)

		currency = requests.get(get_currency.format(single_inventory.inventory_cost_currency))
		try:
			inventory_data["inventory_booking_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			inventory_data["inventory_booking_currency"] = ""

		inventory_total_without_discount.append(round(adult_total_without_discount + child_total_without_discount))
		inventory_total_with_discount.append(round(adult_total + child_total))
		
		booking_total_without_discount.append(round(adult_total_without_discount + child_total_without_discount))
		booking_total_with_discount.append(round(adult_total + child_total))
		
		inventory_array.append(inventory_data)
		inventory_sum.append(single_inventory.inventory_booking_adults + single_inventory.inventory_booking_children)

	return_data_dict["inventory_bookings"] = inventory_array
	return_data_dict["inventory_bookings_total"] = sum(inventory_sum)
	return_data_dict["inventory_bookings_cost_before_discount"] = sum(inventory_total_without_discount)
	return_data_dict["inventory_bookings_cost_after_discount"] = sum(inventory_total_with_discount)
	
	# Getting facility bookings
	facility_bookings = db.session.query(Facility)\
								  .filter(Facility.deletion_marker == None)\
								  .filter(Facility.status != get_booking_status_id("Updated"))\
								  .filter(Facility.booking_id == booking_id)\
								  .options(FromCache(db_cache))\
								  .all()

	facility_array = []
	facility_sum = []
	facility_total_without_discount = []
	facility_total_with_discount = []
	for single_facility in facility_bookings:
		facility_data = {}

		return_facility = requests.get(get_facility_details.format(single_facility.facility_id))

		try:
			facility_data["facility_name"] = return_facility.json()["data"][0]["name"]
		except (KeyError, ValueError, TypeError) as no_data:
			facility_data["facility_name"] = "The record cannot be found."
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			facility_data["facility_name"] = "N/A"

		try:
			facility_data["facility_type"] = return_facility.json()["data"][0]["facility_type_id"]
		except (KeyError, ValueError, TypeError) as no_data:
			facility_data["facility_type"] = "The record cannot be found."
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			facility_data["facility_type"] = "The information cannot be retrieved at this time."
		
		try:
			facility_data["accomodation_type"] = return_facility.json()["data"][0]["accomodation_type_id"]
		except (KeyError, ValueError, TypeError) as no_data:
			facility_data["accomodation_type"] = "The record cannot be found."
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			facility_data["accomodation_type"] = "The information cannot be retrieved at this time."

		return_facility.json()["data"][0]["accomodation_type_id"]
		
		facility_data["facility_id"] = single_facility.facility_id
		facility_data["facility_booking_public_id"] = single_facility.facility_booking_public_id
		facility_data["facility_booking_check_in_date"] = single_facility.facility_booking_check_in_date
		facility_data["facility_booking_check_out_date"] = single_facility.facility_booking_check_out_date
		facility_data["facility_booking_guests"] = single_facility.facility_booking_adults + single_facility.facility_booking_children + single_facility.facility_booking_extra_adults + single_facility.facility_booking_extra_children
		facility_data["facility_booking_adults"] = single_facility.facility_booking_adults
		facility_data["facility_booking_children"] = single_facility.facility_booking_children
		facility_data["facility_booking_extra_adults"] = single_facility.facility_booking_extra_adults
		facility_data["facility_booking_extra_children"] = single_facility.facility_booking_extra_children

		currency = requests.get(get_currency.format(single_facility.facility_cost_currency))
		try:
			facility_data["facility_booking_currency"] = currency.json()["data"][0]["currency_name"]
		except Exception:
			facility_data["facility_booking_currency"] = ""

		facility_array.append(facility_data)
		facility_sum.append(single_facility.facility_booking_adults + single_facility.facility_booking_children)

		facility_data["facility_no_of_nights"] = single_facility.facility_no_of_nights

		if single_facility.facility_no_of_nights == 1:
			facility_data["unit"] = "Night"
		elif single_facility.facility_no_of_nights > 1:
			facility_data["unit"] = "Nights"

		## Handling the various accommodation types
		if return_facility.json()["data"][0]["facility_type_id"] == "Accommodation"\
		or return_facility.json()["data"][0]["facility_type_id"] == "Accomodation"\
		or return_facility.json()["data"][0]["facility_type_id"] == "Acomodation":
			if return_facility.json()["data"][0]["accomodation_type_id"] == "Stables":
				max_capacity = return_facility.json()["data"][0]["maximum_guests"]
				
				if float(single_facility.facility_booking_adults + single_facility.facility_booking_children) <= float(max_capacity):
					fixed_rate = float(single_facility.facility_fixed_cost) * float(single_facility.facility_no_of_nights)\
								 + ((float(single_facility.facility_fixed_cost) / float(max_capacity)) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_adults))\
								 + (((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_children))
					facility_fixed_cost_without_discount = fixed_rate
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					no_of_rooms = 1

				elif float(single_facility.facility_booking_adults + single_facility.facility_booking_children) > float(max_capacity):
					divided_value = round(float(single_facility.facility_booking_adults + single_facility.facility_booking_children) / float(max_capacity))
					modulus = float(single_facility.facility_booking_adults + single_facility.facility_booking_children) % float(max_capacity)
					fixed_rate = (float(single_facility.facility_fixed_cost) * divided_value * float(single_facility.facility_no_of_nights)) + (float(single_facility.facility_fixed_cost) * modulus * float(single_facility.facility_no_of_nights))\
								 + ((float(single_facility.facility_fixed_cost) / float(max_capacity)) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_adults))\
								 + (((float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2) * float(single_facility.facility_no_of_nights) * float(single_facility.facility_booking_extra_children))
					facility_fixed_cost_without_discount = fixed_rate
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					no_of_rooms = divided_value + modulus
				
				adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
				adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
				adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
				adult_with_discount = adult_cost_with_spec_discount

				child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
				child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
				child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
				child_with_discount = child_cost_with_spec_discount

				extra_adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_extra_adults) * float(single_facility.facility_no_of_nights)
				extra_adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * extra_adult_cost_without_discount
				extra_adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * extra_adult_cost_with_discount_rate
				extra_adult_with_discount = extra_adult_cost_with_spec_discount

				extra_child_cost_without_discount = float(single_facility.facility_cost_per_child) / 2 * float(single_facility.facility_booking_extra_children) * float(single_facility.facility_no_of_nights)
				extra_child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * extra_child_cost_without_discount
				extra_child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * extra_child_cost_with_discount_rate
				extra_child_with_discount = extra_child_cost_with_spec_discount

				facility_data["facility_booking_extra_adult_cost"] = round(extra_adult_with_discount)
				facility_data["facility_booking_extra_child_cost"] = round(extra_child_with_discount)
				
				facility_data["facility_booking_fixed_cost"] = float(facility_fixed_cost)
				facility_data["facility_booking_fixed_cost_before_discount"] = float(facility_fixed_cost_without_discount)
				facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
				facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
				facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
				facility_data["facility_booking_child_cost"] = round(child_with_discount)
				facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
				facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
				facility_data["facility_booking_discount"] = float(single_facility.facility_discount_rate)
				facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
				facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
				facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount
				
				facility_data["no_of_rooms"] = int(no_of_rooms)
				facility_data["cost_per_room"] = float(single_facility.facility_fixed_cost)
				facility_data["cost_per_extra_adult"] = (float(single_facility.facility_fixed_cost) / float(max_capacity))
				facility_data["cost_per_extra_child"] = (float(single_facility.facility_fixed_cost) / float(max_capacity)) / 2

				if "Chef" in return_facility.json()["data"][0]["name"]:
					facility_data["chef_room_marker"] = "Chef"
				else:
					facility_data["chef_room_marker"] = "Room"

				facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
				facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)
				
				booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
				booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

				if return_facility.json()["data"][0]["vat"]:
					vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

				if return_facility.json()["data"][0]["catering_levy"]:
					catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)
			
			elif return_facility.json()["data"][0]["accomodation_type_id"] == "Pelican":
				if single_facility.facility_special_price:
					special_price = float(single_facility.facility_special_price) * float(single_facility.facility_no_of_nights)
					over_limit = (float(single_facility.facility_booking_adults) + float(single_facility.facility_booking_children)) - float(return_facility.json()["data"][0]["group_price"][0]["maximum_number"])
					special_increase_cost = over_limit * float(single_facility.facility_special_price_unit_increase) * float(single_facility.facility_no_of_nights)
					
					facility_fixed_cost_without_discount = special_price + special_increase_cost
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
					adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
					adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
					adult_with_discount = adult_cost_with_spec_discount

					child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
					child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
					child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
					child_with_discount = child_cost_with_spec_discount

					facility_data["facility_booking_fixed_cost"] = float(facility_fixed_cost)
					facility_data["facility_booking_fixed_cost_before_discount"] = float(facility_fixed_cost_without_discount)
					facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
					facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
					facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
					facility_data["facility_booking_child_cost"] = round(child_with_discount)
					facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
					facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
					facility_data["facility_booking_discount"] = float(single_facility.facility_discount_rate)
					facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
					facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
					facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount

					facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)
					
					booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					if return_facility.json()["data"][0]["vat"]:
						vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					if return_facility.json()["data"][0]["catering_levy"]:
						catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

				else:
					facility_fixed_cost_without_discount = float(single_facility.facility_fixed_cost)
					facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
					facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
					facility_fixed_cost = facility_fixed_cost_with_spec_discount

					adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
					adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
					adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
					adult_with_discount = adult_cost_with_spec_discount

					child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
					child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
					child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
					child_with_discount = child_cost_with_spec_discount
					
					facility_data["facility_booking_fixed_cost"] = float(facility_fixed_cost)
					facility_data["facility_booking_fixed_cost_before_discount"] = float(facility_fixed_cost_without_discount)
					facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
					facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
					facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
					facility_data["facility_booking_child_cost"] = round(child_with_discount)
					facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
					facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
					facility_data["facility_booking_discount"] = float(single_facility.facility_discount_rate)
					facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
					facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
					facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount

					facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)
					
					booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
					booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					if return_facility.json()["data"][0]["vat"]:
						vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

					if return_facility.json()["data"][0]["catering_levy"]:
						catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)
		
		## Handling camping
		elif return_facility.json()["data"][0]["facility_type_id"] == "Camping Sites"\
		or return_facility.json()["data"][0]["facility_type_id"] == "Campingsites"\
		or return_facility.json()["data"][0]["facility_type_id"] == "CampingSites":
			if single_facility.facility_no_of_nights/7 <= 1:
				facility_fixed_cost_without_discount = float(single_facility.facility_fixed_cost)

				facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
				facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
				facility_fixed_cost = facility_fixed_cost_with_spec_discount
			
			elif single_facility.facility_no_of_nights/7 > 1:
				weeks_count = round(single_facility.facility_no_of_nights/7) + 1
				
				facility_fixed_cost_without_discount = float(single_facility.facility_fixed_cost) * weeks_count
				facility_fixed_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * facility_fixed_cost_without_discount
				facility_fixed_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * facility_fixed_cost_with_discount_rate
				facility_fixed_cost = facility_fixed_cost_with_spec_discount

			adult_cost_without_discount = float(single_facility.facility_cost_per_adult) * float(single_facility.facility_booking_adults) * float(single_facility.facility_no_of_nights)
			adult_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * adult_cost_without_discount
			adult_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * adult_cost_with_discount_rate
			adult_with_discount = adult_cost_with_spec_discount

			child_cost_without_discount = float(single_facility.facility_cost_per_child) * float(single_facility.facility_booking_children) * float(single_facility.facility_no_of_nights)
			child_cost_with_discount_rate = float(1 - (float(single_facility.facility_discount_rate) / 100)) * child_cost_without_discount
			child_cost_with_spec_discount = float(1 - (float(single_facility.facility_commission_rate) / 100)) * child_cost_with_discount_rate
			child_with_discount = child_cost_with_spec_discount
			
			facility_data["facility_booking_fixed_cost"] = round(float(facility_fixed_cost))
			facility_data["facility_booking_fixed_cost_before_discount"] = round(float(facility_fixed_cost_without_discount))
			facility_data["facility_booking_adult_cost"] = round(adult_with_discount)
			facility_data["facility_booking_cost_per_adult"] = float(single_facility.facility_cost_per_adult)
			facility_data["facility_booking_adult_cost_before_discount"] = round(adult_cost_without_discount)
			facility_data["facility_booking_child_cost"] = round(child_with_discount)
			facility_data["facility_booking_cost_per_child"] = float(single_facility.facility_cost_per_child)
			facility_data["facility_booking_child_cost_before_discount"] = round(child_cost_without_discount)
			facility_data["facility_booking_discount"] = float(single_facility.facility_discount_rate)
			facility_data["facility_booking_special_discount"] = float(single_facility.facility_commission_rate)
			facility_data["facility_booking_total_cost"] = facility_fixed_cost + adult_with_discount + child_with_discount
			facility_data["facility_booking_total_cost_before_discount"] = facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount

			facility_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
			facility_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)
			
			booking_total_without_discount.append(facility_fixed_cost_without_discount + adult_cost_without_discount + child_cost_without_discount)
			booking_total_with_discount.append(facility_fixed_cost + adult_with_discount + child_with_discount)

			if return_facility.json()["data"][0]["vat"]:
				vat_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

			if return_facility.json()["data"][0]["catering_levy"]:
				catering_levy_total.append(facility_fixed_cost + adult_with_discount + child_with_discount)

	return_data_dict["facility_bookings"] = facility_array
	return_data_dict["facility_bookings_total"] = sum(facility_sum)
	return_data_dict["facility_bookings_cost_before_discount"] = sum(facility_total_without_discount)
	return_data_dict["facility_bookings_cost_after_discount"] = sum(facility_total_with_discount)
	
	return_data_dict["booking_total_before_discount"] = sum(booking_total_without_discount)
	return_data_dict["total_cost"] = sum(booking_total_with_discount)

	vat_summable = sum(vat_total)
	amount_minus_vat = vat_summable / vat_calculation_amount

	catering_levy_summable = sum(catering_levy_total)
	catering_levy_amount = (catering_levy_summable * 2) / levy_calculation_amount

	return_vat_data = {}
	return_vat_data["summable"] = round(vat_summable)
	return_vat_data["tax"] = round(vat_summable - amount_minus_vat)
	return_vat_data["after_tax"] = round(amount_minus_vat)
	
	return_catering_levy_data = {}
	return_catering_levy_data["summable"] = round(catering_levy_summable)
	return_catering_levy_data["tax"] = round(catering_levy_amount)
	return_catering_levy_data["after_tax"] = round(catering_levy_summable - catering_levy_amount)

	return_tax_data = {}
	return_tax_data["vat"] = round(vat_summable - amount_minus_vat)
	return_tax_data["catering"] = round(catering_levy_amount)
	return_tax_data["after_tax"] = sum(booking_total_with_discount) - (round(vat_summable - amount_minus_vat) + round(catering_levy_amount))

	return_data_dict["vat"] = return_vat_data
	return_data_dict["catering_levy"] = return_catering_levy_data
	return_data_dict["tax_breakdown"] = return_tax_data
	
	if batchfile:
		get_partner = db.session.query(Partner)\
								.filter(Partner.booking_id == booking_id)\
								.first()

		if get_partner:
			partner_info = requests.get(get_partner_details.format(get_partner.partner_id))
			return_data_dict["partner_booking"] = True
			return_data_dict["partner_code_kes"] = partner_info.json()["customer_code"]
			## TODO: Get Tony to do this
			return_data_dict["partner_code_usd"] = None

			if get_booking.booking_type == "FF9330A3" & get_partner.partner_id:
				partner_info = requests.get(get_partner_details.format(get_partner.partner_id))
				return_data_dict["partner_booking"] = True
				return_data_dict["partner_code_kes"] = partner_info.json()["customer_code"]
				## TODO: Get Tony to do this
				return_data_dict["partner_code_usd"] = None
			else:
				return_data_dict["partner_booking"] = False
		
		else:
			return_data_dict["partner_booking"] = False

		if get_booking.booking_type == "GB601X10":
			return_data_dict["guest_booking"] = True
			return_data_dict["guest_code_kes"] = "406B047"
			return_data_dict["guest_code_usd"] = "406B036"
		else:
			return_data_dict["guest_booking"] = False

		if get_booking.booking_type == "FF9330A3":
			return_data_dict["gate_booking"] = True
			return_data_dict["gate_code_kes"] = "406GA001"
			return_data_dict["gate_code_usd"] = "406GA002"
		else:
			return_data_dict["gate_booking"] = False

		if get_booking.booking_type == "7769748C":
			return_data_dict["gate_booking"] = True
			return_data_dict["gate_code_kes"] = "406GA001"
			return_data_dict["gate_code_usd"] = "406GA002"
		else:
			return_data_dict["gate_booking"] = False


def creditNoteTotal(credit_note_id, booking_id, return_credit_note_details, batchfile = None):
	get_credit_note_guests = db.session.query(CreditNoteGuest)\
										.filter(CreditNoteGuest.deletion_marker == None)\
										.filter(CreditNoteGuest.credit_note_id == credit_note_id)\
										.all()

	get_credit_note_vehicles = db.session.query(CreditNoteVehicle)\
											.filter(CreditNoteVehicle.deletion_marker == None)\
											.filter(CreditNoteVehicle.credit_note_id == credit_note_id)\
											.all()

	get_credit_note_facilities = db.session.query(CreditNoteFacility)\
											.filter(CreditNoteFacility.deletion_marker == None)\
											.filter(CreditNoteFacility.credit_note_id == credit_note_id)\
											.all()

	get_credit_note_inventory = db.session.query(CreditNoteInventory)\
											.filter(CreditNoteInventory.deletion_marker == None)\
											.filter(CreditNoteInventory.credit_note_id == credit_note_id)\
											.all()

	# Get VAT rate
	get_latest_vat = requests.get(get_latest_vat_rate)
	# Get catering levy rate
	get_latest_catering = requests.get(get_latest_cater_levy_rate)

	vat_rate = float(get_latest_vat.json()["data"][0]["vat_rate"])
	vat_calculation_amount = 1 + (vat_rate / 100)

	levy_rate = float(get_latest_catering.json()["data"][0]["levy_rate"])
	levy_calculation_amount = 100 + vat_rate + levy_rate
	
	return_data = {}
	bookingTotal(return_data, booking_id, batchfile = True)

	credit_note_details = []
	credit_note_total = []

	# return_credit_note_details = {}

	credit_note_guests = []
	for single_credit_guest in get_credit_note_guests:
		for booking_guest in return_data["guests"]:
			if single_credit_guest.gatepass_guest_id == booking_guest["gatepass_guest_public_id"]:
				return_credit_guest = {}
				temp_date_diff = DateOperations.returnDateDifferenceInDays(single_credit_guest.credit_guest_check_out_date, single_credit_guest.credit_guest_check_in_date)

				if temp_date_diff == 0:
					no_of_days = 1
				elif temp_date_diff > 0:
					no_of_days = temp_date_diff

				discount = float(booking_guest["payment_person_discount"])
				cost = booking_guest["payment_person_amount"]

				guest_total = (1 - (discount / 100)) * cost * single_credit_guest.credit_number_of_guests * no_of_days
				
				if single_credit_guest.credit_guest_discount:
					discount_percentage = float(single_credit_guest.credit_guest_discount)
				else:
					discount_percentage = 0

				guest_total = round((guest_total * (1 - (discount_percentage / 100))), 2)
				
				credit_note_total.append(guest_total)

				return_credit_guest["credit_person"] = booking_guest["payment_person"]
				return_credit_guest["credit_no_of_nights"] = no_of_days
				return_credit_guest["credit_person_discount"] = discount
				return_credit_guest["credit_no_of_guests"] = single_credit_guest.credit_number_of_guests
				return_credit_guest["credit_person_check_in_date"] = single_credit_guest.credit_guest_check_in_date
				return_credit_guest["credit_person_check_out_date"] = single_credit_guest.credit_guest_check_out_date
				return_credit_guest["credit_person_check_in_date_formatted"] = single_credit_guest.credit_guest_check_in_date.strftime("%d %b %Y")
				return_credit_guest["credit_person_check_out_date_formatted"] = single_credit_guest.credit_guest_check_out_date.strftime("%d %b %Y")
				return_credit_guest["credit_person_total"] = float(guest_total)

				if batchfile:
					total_minus_vat = round((guest_total / vat_calculation_amount), 2)
					credit_vat = round((guest_total - total_minus_vat), 2)

					return_credit_guest["total_minus_vat"] = float(total_minus_vat)
					return_credit_guest["vat"] = credit_vat
					return_credit_guest["destination"] = booking_guest["destination"]
					return_credit_guest["destination_outlet_code"] = booking_guest["destination_outlet_code"]
					return_credit_guest["payment_person_income_code"] = booking_guest["payment_person_income_code"]
					return_credit_guest["payment_person_dept_code"] = booking_guest["payment_person_dept_code"]
			
				credit_note_guests.append(return_credit_guest)

			else:
				pass

	return_credit_note_details["guests"] = credit_note_guests

	credit_note_vehicles = []
	for single_credit_vehicle in get_credit_note_vehicles:
		for booking_vehicle in return_data["vehicles"]:
			if single_credit_vehicle.gatepass_vehicle_id == booking_vehicle["gatepass_vehicle_public_id"]:
				return_credit_vehicle = {}
				temp_date_diff = DateOperations.returnDateDifferenceInDays(single_credit_vehicle.credit_vehicle_check_out_date, single_credit_vehicle.credit_vehicle_check_in_date)

				if temp_date_diff == 0:
					no_of_days = 1
				elif temp_date_diff > 0:
					no_of_days = temp_date_diff

				discount = float(booking_vehicle["discount"])
				cost = booking_vehicle["vehicle_charge_category_cost"]

				vehicle_total = (1 - (discount / 100)) * cost * single_credit_vehicle.credit_number_of_vehicles * no_of_days
				
				if single_credit_vehicle.credit_vehicle_discount:
					discount_percentage = float(single_credit_vehicle.credit_vehicle_discount)
				else:
					discount_percentage = 0

				vehicle_total = round((vehicle_total * (1 - (discount_percentage / 100))), 2)
				
				credit_note_total.append(vehicle_total)

				return_credit_vehicle["credit_vehicle"] = booking_vehicle["vehicle_charge_category"]
				return_credit_vehicle["credit_no_of_nights"] = no_of_days
				return_credit_vehicle["credit_vehicle_discount"] = discount
				return_credit_vehicle["credit_no_of_vehicles"] = single_credit_vehicle.credit_number_of_vehicles
				return_credit_vehicle["credit_vehicle_check_in_date"] = single_credit_vehicle.credit_vehicle_check_in_date
				return_credit_vehicle["credit_vehicle_check_out_date"] = single_credit_vehicle.credit_vehicle_check_out_date
				return_credit_vehicle["credit_vehicle_check_in_date_formatted"] = single_credit_vehicle.credit_vehicle_check_in_date.strftime("%d %b %Y")
				return_credit_vehicle["credit_vehicle_check_out_date_formatted"] = single_credit_vehicle.credit_vehicle_check_out_date.strftime("%d %b %Y")
				return_credit_vehicle["credit_vehicle_total"] = float(vehicle_total)
			
				if batchfile:
					total_minus_vat = round((vehicle_total / vat_calculation_amount), 2)
					credit_vat = round((vehicle_total - total_minus_vat), 2)
					
					return_credit_vehicle["total_minus_vat"] = total_minus_vat
					return_credit_vehicle["vat"] = credit_vat
					return_credit_vehicle["vehicle_charge_income_code"] = booking_vehicle["vehicle_charge_income_code"]
					return_credit_vehicle["vehicle_charge_outlet_code"] = booking_vehicle["vehicle_charge_outlet_code"]
					return_credit_vehicle["vehicle_charge_dept_code"] = booking_vehicle["vehicle_charge_dept_code"]
				
				credit_note_vehicles.append(return_credit_vehicle)

			else:
				pass

	return_credit_note_details["vehicles"] = credit_note_vehicles

	credit_note_inventory = []
	for single_credit_inventory in get_credit_note_inventory:
		for booking_inventory in return_data["inventory_bookings"]:
			if single_credit_inventory.inventory_booking_id == booking_inventory["inventory_booking_public_id"]:
				return_credit_inventory = {}
				
				discount = float(booking_inventory["inventory_booking_discount_percentage"])
				adult_cost = booking_inventory["inventory_booking_cost_per_adult"]
				child_cost = booking_inventory["inventory_booking_cost_per_child"]

				adults_count = single_credit_inventory.credit_inventory_booking_adults + single_credit_inventory.credit_inventory_booking_extra_adults
				children_count = single_credit_inventory.credit_inventory_booking_children + single_credit_inventory.credit_inventory_booking_extra_children

				adult_total = (1 - (discount / 100)) * adult_cost * adults_count
				child_total = (1 - (discount / 100)) * child_cost * children_count
				
				inventory_total = adult_total + child_total
				credit_note_total.append(inventory_total)

				return_credit_inventory["credit_inventory"] = booking_inventory["inventory_name"]
				return_credit_inventory["inventory_ranger_marker"] = booking_inventory["inventory_ranger_marker"]
				return_credit_inventory["credit_inventory_discount"] = discount
				return_credit_inventory["credit_no_of_adults"] = adults_count
				return_credit_inventory["credit_no_of_children"] = children_count
				return_credit_inventory["credit_inventory_date"] = single_credit_inventory.credit_inventory_date
				return_credit_inventory["credit_inventory_date_formatted"] = single_credit_inventory.credit_inventory_date.strftime("%d %b %Y")
				return_credit_inventory["credit_inventory_total"] = float(inventory_total)

				if batchfile:
					return_credit_inventory["inventory_income_code"] = booking_inventory["inventory_income_code"]
					return_credit_inventory["inventory_outlet_code"] = booking_inventory["inventory_outlet_code"]
					return_credit_inventory["inventory_dept_analysis_code"] = booking_inventory["inventory_dept_analysis_code"]
					return_credit_inventory["vat"] = booking_inventory["vat"]
					return_credit_inventory["catering_levy"] = booking_inventory["catering_levy"]
					
					if booking_inventory["vat"] & booking_inventory["catering_levy"]:
						adult_total_minus_vat = round((adult_total / vat_calculation_amount), 2)
						adult_credit_vat = round((adult_total - adult_total_minus_vat), 2)
						adult_cater = round(((adult_total * 2) / levy_calculation_amount), 2)
						adult_minus_taxes = round((adult_total - (adult_credit_vat + adult_cater)), 2)
						child_total_minus_vat = round((child_total / vat_calculation_amount), 2)
						child_credit_vat = round((child_total - child_total_minus_vat), 2)
						child_cater = round(((child_total * 2) / levy_calculation_amount), 2)
						child_minus_taxes = round((child_total - (child_credit_vat + child_cater)), 2)

						return_credit_inventory["adult_cater"] = adult_cater
						return_credit_inventory["adult_vat"] = adult_credit_vat
						return_credit_inventory["adult_minus_taxes"] = adult_minus_taxes
						return_credit_inventory["child_cater"] = child_cater
						return_credit_inventory["child_vat"] = child_credit_vat
						return_credit_inventory["child_minus_taxes"] = child_minus_taxes
					else:
						if booking_inventory["vat"]:
							adult_total_minus_vat = round((adult_total / vat_calculation_amount), 2)
							adult_credit_vat = round((adult_total - adult_total_minus_vat), 2)
							child_total_minus_vat = round((child_total / vat_calculation_amount), 2)
							child_credit_vat = round((child_total - child_total_minus_vat), 2)

							return_credit_inventory["adult_total_minus_vat"] = adult_total_minus_vat
							return_credit_inventory["adult_vat"] = adult_credit_vat
							return_credit_inventory["child_total_minus_vat"] = child_total_minus_vat
							return_credit_inventory["child_vat"] = child_credit_vat

						elif booking_inventory["catering_levy"]:
							adult_cater = round(((adult_total * 2) / levy_calculation_amount), 2)
							adult_total_minus_cater = round((adult_total - adult_cater), 2)
							child_cater = round(((child_total * 2) / levy_calculation_amount), 2)
							child_total_minus_cater = round((child_total - child_cater), 2)

							return_credit_inventory["adult_total_minus_cater"] = adult_total_minus_cater
							return_credit_inventory["adult_cater"] = adult_cater
							return_credit_inventory["child_total_minus_cater"] = child_total_minus_cater
							return_credit_inventory["child_cater"] = child_cater
						
						else:
							return_credit_inventory["adult_total"] = adult_total
							return_credit_inventory["child_total"] = child_total
			
				credit_note_inventory.append(return_credit_inventory)

			else:
				pass

	return_credit_note_details["inventory"] = credit_note_inventory

	credit_note_facilities = []
	for single_credit_facility in get_credit_note_facilities:
		for booking_facility in return_data["facility_bookings"]:
			if single_credit_facility.facility_booking_id == booking_facility["facility_booking_public_id"]:
				return_credit_facility = {}

				facility_type = booking_facility["facility_type"]

				no_of_days = DateOperations.returnDateDifferenceInDays(single_credit_facility.credit_facility_check_out_date, single_credit_facility.credit_facility_check_in_date)

				fixed_total_cost = 0
				
				if facility_type == "Accomodation":
					if booking_facility["accomodation_type"] == "Stables":
						adult_cost = 0
						child_cost = 0
					elif booking_facility["accomodation_type"] == "Pelican":
						adult_cost = booking_facility["facility_booking_cost_per_adult"]
						child_cost = booking_facility["facility_booking_cost_per_child"]
				
				elif facility_type == "Camping Sites":
					days_to_weeks = no_of_days % 7

					if days_to_weeks > 0:
						weeks = math.floor(no_of_days / 7) + 1
					else:
						weeks = no_of_days / 7
					
					fixed_cost = booking_facility["facility_booking_fixed_cost"] * weeks
					adult_cost = booking_facility["facility_booking_cost_per_adult"]
					child_cost = booking_facility["facility_booking_cost_per_child"]
				
				discount = float(booking_facility["facility_booking_discount_percentage"])
				
				adults_count = single_credit_facility.credit_facility_booking_adults + single_credit_facility.credit_facility_booking_extra_adults
				children_count = single_credit_facility.credit_facility_booking_children + single_credit_facility.credit_facility_booking_extra_children

				if single_credit_facility.credit_note_facilities_public_id == "ab052905-e703-4f7c-a6b8-32b1aacf4a8d":
					discount = 0
				
				adult_total = (1 - (discount / 100)) * adult_cost * adults_count * no_of_days
				child_total = (1 - (discount / 100)) * child_cost * children_count * no_of_days
				if adults_count + children_count == 0:
					fixed_total = (1 - (discount / 100)) * fixed_total_cost
				else:
					fixed_total = 0
				
				facility_total = adult_total + child_total + fixed_total
				credit_note_total.append(facility_total)

				return_credit_facility["facility_type"] = facility_type
				return_credit_facility["accomodation_type"] = booking_facility["accomodation_type"]
				return_credit_facility["facility_catering_type_name"] = booking_facility["facility_catering_type_name"]
				return_credit_facility["credit_facility"] = booking_facility["facility_name"]
				return_credit_facility["credit_facility_discount"] = discount
				return_credit_facility["credit_no_of_nights"] = no_of_days
				return_credit_facility["credit_no_of_adults"] = adults_count
				return_credit_facility["credit_no_of_children"] = children_count
				return_credit_facility["credit_no_of_guests"] = adults_count + children_count
				return_credit_facility["credit_facility_check_in_date"] = single_credit_facility.credit_facility_check_in_date
				return_credit_facility["credit_facility_check_out_date"] = single_credit_facility.credit_facility_check_out_date
				return_credit_facility["credit_facility_total"] = float(facility_total)

				if batchfile:
					return_credit_facility["facility_income_code"] = booking_facility["facility_income_code"]
					return_credit_facility["facility_outlet_code"] = booking_facility["facility_outlet_code"]
					return_credit_facility["facility_analysis_code"] = booking_facility["facility_dept_analysis_code"]
					return_credit_facility["vat"] = booking_facility["vat"]
					return_credit_facility["catering_levy"] = booking_facility["catering_levy"]

					if booking_facility["vat"] & booking_facility["catering_levy"]:
						fixed_total_minus_vat = round((fixed_total / vat_calculation_amount), 2)
						fixed_credit_vat = round((fixed_total - fixed_total_minus_vat), 2)
						fixed_cater = round(((fixed_total * 2) / levy_calculation_amount), 2)
						fixed_minus_taxes = round((fixed_total - (fixed_credit_vat + fixed_cater)), 2)
						adult_total_minus_vat = round((adult_total / vat_calculation_amount), 2)
						adult_credit_vat = round((adult_total - adult_total_minus_vat), 2)
						adult_cater = round(((adult_total * 2) / levy_calculation_amount), 2)
						adult_minus_taxes = round((adult_total - (adult_credit_vat + adult_cater)), 2)
						child_total_minus_vat = round((child_total / vat_calculation_amount), 2)
						child_credit_vat = round((child_total - child_total_minus_vat), 2)
						child_cater = round(((child_total * 2) / levy_calculation_amount), 2)
						child_minus_taxes = round((child_total - (child_credit_vat + child_cater)), 2)

						return_credit_facility["fixed_minus_taxes"] = fixed_minus_taxes
						return_credit_facility["fixed_vat"] = fixed_credit_vat
						return_credit_facility["fixed_cater"] = fixed_cater
						return_credit_facility["adult_minus_taxes"] = adult_minus_taxes
						return_credit_facility["adult_vat"] = adult_credit_vat
						return_credit_facility["adult_cater"] = adult_cater
						return_credit_facility["child_minus_taxes"] = child_minus_taxes
						return_credit_facility["child_vat"] = child_credit_vat
						return_credit_facility["child_cater"] = child_cater

					else:
						if booking_facility["vat"]:
							fixed_total_minus_vat = round((fixed_total / vat_calculation_amount), 2)
							fixed_credit_vat = round((fixed_total - fixed_total_minus_vat), 2)
							adult_total_minus_vat = round((adult_total / vat_calculation_amount), 2)
							adult_credit_vat = round((adult_total - adult_total_minus_vat), 2)
							child_total_minus_vat = round((child_total / vat_calculation_amount), 2)
							child_credit_vat = round((child_total - child_total_minus_vat), 2)

							return_credit_facility["fixed_minus_vat"] = fixed_total_minus_vat
							return_credit_facility["fixed_vat"] = fixed_credit_vat
							return_credit_facility["adult_minus_vat"] = adult_total_minus_vat
							return_credit_facility["adult_vat"] = adult_credit_vat
							return_credit_facility["child_minus_vat"] = child_total_minus_vat
							return_credit_facility["child_vat"] = child_credit_vat
						
						elif booking_facility["catering_levy"]:
							fixed_cater = round(((fixed_total * 2) / levy_calculation_amount), 2)
							fixed_minus_cater = round((fixed_total - fixed_cater), 2)
							adult_cater = round(((adult_total * 2) / levy_calculation_amount), 2)
							adult_minus_cater = round((adult_total - adult_cater), 2)
							child_cater = round(((child_total * 2) / levy_calculation_amount), 2)
							child_minus_cater = round((child_total - child_cater), 2)

							return_credit_facility["fixed_minus_cater"] = fixed_minus_cater
							return_credit_facility["fixed_cater"] = fixed_cater
							return_credit_facility["adult_minus_cater"] = adult_minus_cater
							return_credit_facility["adult_cater"] = adult_cater
							return_credit_facility["child_minus_cater"] = child_minus_cater
							return_credit_facility["child_cater"] = child_cater
							
						else:
							return_credit_facility["fixed_total"] = fixed_total
							return_credit_facility["adult_total"] = adult_total
							return_credit_facility["child_total"] = child_total
			
				credit_note_facilities.append(return_credit_facility)

			else:
				pass

	return_credit_note_details["facilities"] = credit_note_facilities

	return_credit_note_details["total"] = sum(credit_note_total)

	if batchfile:
		return_credit_note_details["customer_code"] = return_data["customer_code"]