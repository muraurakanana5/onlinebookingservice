from flask import jsonify, make_response, request
from datetime import datetime
from functools import wraps
import sys
import traceback
import requests
import asyncio
import aiohttp
import socket

from routes import app


class Logger:
	def preRequest(func, *args, **kwargs):
		pass

	def postRequest(func, *args, **kwargs):
		pass

	
	def logWrapper(self):

		def logDecorator(func):

			@wraps(func)
			def execute(*args, **kwargs):
				# self.preRequest(func, *args, **kwargs)

				header_dict = dict(request.headers)

				try:
					user_agent = header_dict["User-Agent"]
				except Exception:
					user_agent = None

				try:
					language = header_dict["Accept-Language"]
				except Exception:
					language = None

				try:
					referer = header_dict["Referer"]
				except Exception:
					referer = None

				try:
					origin = header_dict["Origin"]
				except Exception:
					origin = None

				try:
					json_data = request.json
				except Exception:
					json_data = None

				try:
					platform = request.user_agent.platform.title()
				except Exception:
					platform = None

				try:
					browser = request.user_agent.browser.title()
				except Exception:
					browser = None

				try:
					bearer, auth_header_token = header_dict["Authorization"].split(" ")
				except Exception:
					bearer = None
					auth_header_token = None

				try:
					tracker_id = header_dict["Tracker-Id"]
				except Exception:
					tracker_id = None
				
				try:
					result = func(*args, **kwargs)

					result_response = make_response(result)

					post_data = {
						"error": None,
						"stack_trace": None,
						"method": request.method,
						"source_ip": request.remote_addr,
						"url": request.url,
						"status_code": result_response.status_code,
						"headers": str(header_dict),
						"user_agent": user_agent,
						"language": language,
						"platform": platform,
						"browser": browser,
						"referer": referer,
						"origin": origin,
						"auth_header": auth_header_token,
						"access_time": datetime.now().strftime("%A, %d %B %Y %H:%M:%S"),
						"logging_access_key": self.accessKey,
						"json": json_data,
						"request_params": str(dict(request.args)),
						"tracker_id": tracker_id
					}

					self.startPost(post_data)
					
				except Exception as e:
					error_tuple = sys.exc_info()
					trace = traceback.format_exc()

					kwargs = {
						"trace": trace,
						"exception": str(e)
					}
					
					# self.postRequest(func, *args, **kwargs)
					
					post_data = {
						"error": str(e),
						"stack_trace": trace,
						"method": request.method,
						"source_ip": request.remote_addr,
						"url": request.url,
						"status_code": 500,
						"headers": str(header_dict),
						"user_agent": user_agent,
						"language": language,
						"platform": platform,
						"browser": browser,
						"referer": referer,
						"origin": origin,
						"auth_header": auth_header_token,
						"access_time": datetime.now().strftime("%A, %d %B %Y %H:%M:%S"),
						"logging_access_key": self.accessKey,
						"json": json_data,
						"request_params": str(dict(request.args)),
						"tracker_id": tracker_id
					}

					self.startPost(post_data)

				# return jsonify(post_data)
				return result

			return execute

		return logDecorator

	
	def startPost(self, postData):
		loop = asyncio.new_event_loop()
		asyncio.set_event_loop(loop)
		loop.run_until_complete(self.postAsyncData(postData))

	
	async def postAsyncData(self, postData):
		# conn = aiohttp.TCPConnector(
		# 	verify_ssl = False,
		# 	family = socket.AF_INET
		# )
		
		logging_url = app.config["LOGGING_URL"]
		headers = {
			"X-Api-Key": app.config["LOG_AUTH_KEY"],
			"Content-Type": "application/json",
			"Origin": request.remote_addr
		}
		
		async with aiohttp.ClientSession() as session:
			await session.post(logging_url, json=postData, headers=headers)

		# request_session = requests.Session()
		# request_session.post(logging_url, json=postData, headers=headers)


	def __init__(self, accessKey = None):
		self.accessKey = accessKey