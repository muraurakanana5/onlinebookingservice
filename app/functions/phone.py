import requests

def validatePhoneNumber(phoneNumber):
	if phoneNumber == None:
		message = "The phone number is empty."
		return message

	prefix = None
	number = None

	strippedPhone = phoneNumber.strip("+")
	strippedPhone = strippedPhone.strip("-")

	if len(strippedPhone) > 15:
		message = "The phone number is too long. Please try again."
		return message

	if len(strippedPhone) < 7:
		message = "The phone number is too short. Please try again."
		return message
	
	try:
		prefix,number = phoneNumber.split("-")
	except Exception:
		message = "Please separate the phone number country code and the number with a hyphen eg +254-712345678"
		return message + str(prefix) + str(number)

	check_validity = requests.get("https://restcountries.eu/rest/v2/callingcode/{}".format(prefix.strip("+")))

	if check_validity.status_code == 404:
		message = "The provided country code is invalid."
		return message

	return 1