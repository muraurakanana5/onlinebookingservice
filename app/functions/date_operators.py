from datetime import datetime, timedelta, date


class GenerateDateFromString():
    def generateDate(stringDate):
        formatted_date = stringDate.replace("-", ",")

        year,month,day = formatted_date.split(",")

        new_date = date(int(year), int(month), int(day))

        return new_date


    def generateDateTime(stringDate):
        formatted_date = stringDate.replace("-", ",")

        year,month,day = formatted_date.split(",")

        new_date = datetime(int(year), int(month), int(day), 0, 0)

        return new_date


class DateOperations():
    def returnDateDifferenceInDays(end_date, start_date):
        return int(abs(end_date - start_date).days)