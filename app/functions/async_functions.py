import asyncio
import aiohttp


class AsyncRequests():
	def launchGetRequest(self, url = None, request_id = None):
		loop = asyncio.new_event_loop()
		asyncio.set_event_loop(loop)
		data = loop.run_until_complete(self.getRequest(url, request_id))

		return data

	
	async def getRequest(self, url = None, request_id = None):
		if request_id:
			async with aiohttp.ClientSession() as session:
				async with session.get(url.format(request_id)) as response:
					json_data = await response.json()

					return json_data
		
		else:
			async with aiohttp.ClientSession() as session:
				async with session.get(url) as response:
					json_data = await response.json()

					return json_data


	def launchPostRequest(self, url = None, json = None):
		loop = asyncio.new_event_loop()
		asyncio.set_event_loop(loop)
		data = loop.run_until_complete(self.postRequest(url, json))

		return data

	
	async def postRequest(self, url = None, json = None):
		async with aiohttp.ClientSession() as session:
			async with session.post(url, json = json) as response:
				json_data = await response.json()

				return json_data