from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from flask_sqlalchemy import SQLAlchemy, functools
from flask_cors import CORS, cross_origin
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid

import cherrypy
# File imports
# from variables import *

# Directory imports
from routes import app

if __name__ == '__main__':

    # Mount the application
    cherrypy.tree.graft(app, "/")

    # Unsubscribe the default server
    cherrypy.server.unsubscribe()

    # Instantiate a new server object
    server = cherrypy._cpserver.Server()

    # Configure the server object
    server.socket_host = "0.0.0.0"
    server.socket_port = 5005
    server.thread_pool = 40
    server.socket_timeout = 150

    if os.environ['ENVIRONMENT'] == "Development":
        pass
    elif os.environ['ENVIRONMENT'] == "Production":
     

    cherrypy.config.update({"response.stream": True})
    cherrypy.engine.start()
    cherrypy.engine.block()
# app.run(port=5005, host='0.0.0.0',debug=True)