import os


# if os.environ['ENVIRONMENT'] == "Development":
# 	ip = "http://" + os.environ['DEVELOPMENT_IP']
# elif os.environ['ENVIRONMENT'] == "Production":
# 	ip = "https://" + os.environ['PRODUCTION_IP']
# 	mpesa_ip = "http://" + os.environ['PRODUCTION_IP']
ip = "bookings.olpejetaconservancy.org"


#################################
##### Facility Service URLS #####
#################################
#* In use in bookings_facility_urls *#
get_filtered_not_active_accomodation = ip + ":5001/facilities/filtered/not/in/accomodation/active"
get_filtered_not_campsites = ip + ":5001/facilities/filtered/not/in/camping/active"

#* In use in bookings_urls *#
get_facility_details = ip + ":5001/facility/api/{}"
get_facility_contacts = ip + ":5001/facility/contact/{}"
get_facility_pricing = ip + ":5001/facility/pricing/{}"
get_facility_pricing_by_date = ip + ":5001/facility/dates/pricing/{}"

#* In use in calendar_urls *#
get_active_facilities = ip + ":5001/facilities/active"
get_active_facilities_with_pricing = ip + ":5001/facilities/active-with-pricing"


##################################
##### Inventory Service URLS #####
##################################
#* In use in calendar_urls *#
get_active_inventory = ip + ":5002/inventory/active"

#* In use in bookings_urls *#
get_inventory_details = ip + ":5002/inventory/api/{}"
get_inventory_current_pricing = ip + ":5002/inventory/current/pricing/{}"
get_inventory_contacts = ip + ":5002/inventory/contact/{}"
get_inventory_with_pricing = ip + ":5002/inventory/api/price-schedule/{}"
get_inventory_pricing = ip + ":5002/inventory/requested-pricing/{}"


######################
##### Aumra URLS #####
######################
get_user_from_aumra = ip + ":9000/v1/users/array"
get_country_details = ip + ":9000/v1/country/{}"
get_single_user = ip + ":9000/v1/user/{}"


###############################
##### Member Service URLS #####
###############################
get_member_details = ip + ":5004/v1/member/{}"
get_member_by_id = ip + ":5004/v1/member/id/{}"
get_corporate_member = ip + ":5004/v1/corporate/member/{}"


#################################
##### Currency Service URLS #####
#################################
get_latest_exchange_rate = ip + ":5006/currency/rates/view/latest"
get_currency = ip + ":5006/currency/view/{}"
get_buy_sell_rate = ip + ":5006/currency/bs/rate/view/latest/{}"
get_latest_vat_rate = ip + ":5006/vat/rates/view/latest"
get_latest_cater_levy_rate = ip + ":5006/levy/rates/view/latest"
get_all_currencies = ip + ":5006/currency/view"
get_currency_rate_at_time = ip + ":5006/currency/rates/view"
get_vat_for_date = ip + ":5006/vat/rates/view-for-date"


################################
##### Partner Service URLS #####
################################
get_partner_details = ip + ":5003/v1/partner/{}"


######################
##### Mpesa URLS #####
######################
mpesa_pay = ip + ":5019/checkout"


get_membership_data = ip + ":5004/v1/member-salesforce-id/{}"


get_country_details = "https://restcountries.eu/rest/v2/callingcode/{}"


promo_code_search = ip + ":5017/promo/code/search"
use_promo_code = ip + ":5017/promo/code/use/{}"


post_donation = ip + ":5008/v1/make/donation"
get_donation_cause = ip + ":5008/v1/payment/cause/{}"


get_sf_details = ip + ":5005/salesforce/access"