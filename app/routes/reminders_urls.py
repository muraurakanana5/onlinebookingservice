from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date
from flask_mail import Mail, Message
from sendgrid.helpers.mail import *

import pymysql, os, math, requests, uuid, sendgrid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from routes.bookings_urls import get_booking_status_id
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from variables import *

from functions.date_operators import *
from functions.currency_operators import *


def close(self):
	self.session.close()


###################
#### Reminders ####
###################
# @app.route("/reminders/24")
# def send_day_reminder_email():
# 	tomorrow = (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
	
# 	get_all_bookings = db.session.query(Booking)\
# 								 .join(Detail, Booking.booking_public_id == Detail.booking_id)\
# 								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
# 											  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
# 											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
# 											  Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status,\
# 											  Booking.currency,\
# 											  Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
# 											  Detail.address, Detail.additional_note)\
# 								 .filter(Booking.deletion_marker == None)\
# 								 .filter(Booking.booking_check_in_date == tomorrow)\
# 								 .all()

# 	if not get_all_bookings:
# 		message = []
# 		message.append("There are no bookings scheduled for tomorrow.")
		
# 		return jsonify({"message": message}), 412

# 	else:
# 		return_array = []
# 		error_array = []
# 		sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])

# 		sender = "reservations@olpejetaconservancy.org"

# 		for each_booking in get_all_bookings:
# 			reminder_data = {}
# 			reminder_data["today"] = datetime.now().strftime("%B %Y")
# 			reminder_data["booking_ref_code"] = each_booking.booking_ref_code
# 			reminder_data["check_in_date"] = each_booking.booking_check_in_date.strftime("%A %d %B %Y")
# 			reminder_data["check_out_date"] = each_booking.booking_check_out_date.strftime("%A %d %B %Y")
# 			reminder_data["client"] = " " + each_booking.first_name + " " + each_booking.last_name
# 			reminder_data["first_name"] = each_booking.first_name

# 			guest_sum = []
			
# 			get_all_guests = db.session.query(BookingGuest)\
# 									   .join(GuestType, BookingGuest.guest_type == GuestType.booking_guest_type_public_id)\
# 									   .add_columns(GuestType.booking_guest_type_name,\
# 									   				BookingGuest.guest_count)\
# 									   .filter(BookingGuest.deletion_marker == None)\
# 									   .filter(BookingGuest.booking_id == each_booking.booking_public_id)\
# 									   .all()

# 			for each_guest in get_all_guests:
# 				guest_sum.append(int(each_guest.guest_count))

# 			reminder_data["num_of_guests"] = sum(guest_sum)
			
# 			from_email = Email(sender)
# 			to_email = Email(each_booking.email_address)
# 			subject = "Booking Reminder"
# 			content = Content("text/html", render_template("bookingreminder.html", data = reminder_data))
# 			mail = Mail(from_email, subject, to_email, content)

# 			try:
# 				response = sg.client.mail.send.post(request_body=mail.get())

# 				return_array.append(response)

# 			except Exception as e:
# 				error_array.append(str(e))

# 		return jsonify({"data": str(return_array)})


## TODO: Include vehicle and guest breakdown
@app.route("/reminders/<booking_id>")
def send_reminder_email(booking_id):
	tomorrow = (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
	
	get_all_bookings = db.session.query(Booking)\
								 .join(Detail, Booking.booking_public_id == Detail.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
											  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status,\
											  Booking.currency,\
											  Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
											  Detail.address, Detail.additional_note)\
								 .filter(Booking.deletion_marker == None)\
								 .filter(Booking.booking_public_id == booking_id)\
								 .filter(Detail.status != get_booking_status_id("Updated"))\
								 .filter(Detail.status != get_booking_status_id("Cancelled"))\
								 .all()

	if not get_all_bookings:
		message = []
		message.append(".")
		
		return jsonify({"message": message}), 412

	else:
		return_array = []
		error_array = []
		sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])

		sender = "reservations@olpejetaconservancy.org"

		for each_booking in get_all_bookings:
			reminder_data = {}
			reminder_data["today"] = datetime.now().strftime("%B %Y")
			reminder_data["booking_ref_code"] = each_booking.booking_ref_code
			reminder_data["check_in_date"] = each_booking.booking_check_in_date.strftime("%A %d %B %Y")
			reminder_data["check_out_date"] = each_booking.booking_check_out_date.strftime("%A %d %B %Y")
			reminder_data["client"] = " " + each_booking.first_name + " " + each_booking.last_name
			reminder_data["first_name"] = each_booking.first_name
			reminder_data["booking_id"] = each_booking.booking_public_id
			reminder_data["copyright_year"] = datetime.now().strftime("%Y")

			guest_sum = []
			
			get_all_guests = db.session.query(BookingGuest)\
									   .join(GuestType, BookingGuest.guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name,\
									   				BookingGuest.guest_count)\
									   .filter(BookingGuest.deletion_marker == None)\
									   .filter(BookingGuest.booking_id == each_booking.booking_public_id)\
									   .all()

			for each_guest in get_all_guests:
				guest_sum.append(int(each_guest.guest_count))

			reminder_data["num_of_guests"] = sum(guest_sum)
			
			vehicle_sum = []

			reminder_data["num_of_vehicles"] = sum(vehicle_sum)
			
			from_email = Email(sender)
			to_email = Email(each_booking.email_address)
			subject = "Booking Reminder"
			content = Content("text/html", render_template("bookingreminder.html", data = reminder_data))
			mail = Mail(from_email, subject, to_email, content)

			try:
				response = sg.client.mail.send.post(request_body=mail.get())

				return_array.append("Reminder email sent to " + each_booking.email_address + ".")

			except Exception as e:
				error_array.append(str(e))

		return jsonify({"message": return_array})