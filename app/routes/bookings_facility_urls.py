import math
import os
import urllib.request
import uuid
from datetime import datetime, timedelta
from collections import defaultdict

import pymysql
import requests
import traceback
from flask import (Flask, json, jsonify, redirect, render_template, request,
	   url_for)

from database.booking_activity_log import BookingActivity
from database.booking_guest_types import GuestType
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_types import BookingType
from database.bookings import Booking
from database.facility import Facility
from database.facility_pricing_type import FacilityPricing
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.invoice import Invoice
from database.mandatory_payments import Mandatory
from functions.date_operators import GenerateDateFromString
# File imports
from routes import app, db
from routes.bookings_urls import get_booking_status_id, currencyHandler
from routes.seed_functions import BookingStatusSeed, BookingTypeSeed
from variables import *


def close(self):
	self.session.close()


##########################
#### Facility Booking ####
##########################

@app.route("/bookings/facility/available/date", methods = ["POST"])
@app.route("/bookings/public/facility/available/date", methods = ["POST"])
def check_available_facility_booking_date():
	messages = []

	try:
		request.json["check_in_date"].strip()
		if not request.json["check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		request.json["check_out_date"].strip()
		if not request.json["check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	if request.json["check_out_date"] < request.json["check_in_date"]:
		output = []
		output.append("Your check out date cannot come before your check in date.")
		return jsonify({"message": output}), 422

	check_in_date = GenerateDateFromString.generateDate(request.json["check_in_date"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out_date"])
	to_day = datetime.today().strftime('%Y-%m-%d')
	today = GenerateDateFromString.generateDate(to_day)

	output = []
	blank_array = []
	date_array = []

	while check_in_date <= check_out_date:
		date_array.append(check_in_date)

		check_in_date = check_in_date + timedelta(days = 1)
	to_check_date = GenerateDateFromString.generateDate(request.json["check_in_date"])

	get_all_active_accomodation = requests.post(get_active_facilities_with_pricing, json = {
	 "start_date": request.json["check_in_date"],
	 "end_date": request.json["check_out_date"]
	})

	available_facility = []
	available_campsite = []

	for single_facility in get_all_active_accomodation.json()["data"]:

		guest_total = []
		guest_total.append(0)

		if single_facility["pricing_status"] == "false":
			pass

		if "/public" in request.path and single_facility["thumbnail"]:

			try:
				facility_prices = single_facility["price"]
			except KeyError as e:
				pass

			for facility_price in facility_prices:
				facility_lead = facility_price["lead_time"]
			if facility_lead is None:
				facility_lead_time = 0
			else:
				facility_lead_time =int(facility_lead)
			modified_date = today + timedelta(days = facility_lead_time)
		else:

			modified_date = to_check_date

		if to_check_date < modified_date and single_facility["accomodation_type_id"] != "Pelican":
			pass
		else:
			for single_date in date_array:
				query_facilities = db.session.query(Facility)\
				.join(Booking, Facility.booking_id == Booking.booking_public_id)\
				.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
				Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
				.filter(Facility.deletion_marker == None)\
				.filter(Facility.facility_booking_check_in_date <= single_date)\
				.filter(Facility.facility_booking_check_out_date > single_date)\
				.filter(Facility.status != get_booking_status_id("Updated"))\
				.filter(Facility.status != get_booking_status_id("Cancelled"))\
				.filter(Facility.facility_id == single_facility["public_id"])\
				.filter(Booking.deletion_marker == None)\
				.filter(Booking.status != get_booking_status_id("Cancelled"))\
				.all()

				temp_total = []
				temp_total.append(0)

				for single_query_item in query_facilities:
					if single_query_item.facility_booking_check_in_date.strftime("%Y-%m-%d") == request.json["check_out_date"]:
						pass
					else:
						if single_query_item.facility_booking_check_out_date.strftime("%Y-%m-%d") == request.json["check_in_date"]:
							pass
						else:
							temp_total.append(single_query_item.facility_booking_adults + single_query_item.facility_booking_children)

				if single_facility["unavailability_schedule_set"]:
					for each_schedule in single_facility["unavailability_schedule"]:
						if (each_schedule["start_date_formatted"] <= single_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= single_date.strftime("%Y-%m-%d")):
							temp_total.append(each_schedule["unavailability_number"])
						else:
							pass

				guest_total.append(sum(temp_total))

			guests_over_duration = max(guest_total)

			if single_facility["facility_type_id"] == "Accomodation":
				if single_facility["accomodation_type_id"] == "Pelican":
					if guests_over_duration > 0:
						pass
					else:
						percentage_pricing_status = single_facility["percentage_pricing_set"]

						if percentage_pricing_status:
							percentage_pricing = {}

							for single_percentage in single_facility["percentage_pricing"]:
								if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
									percentage_pricing = single_percentage
								else:
									pass

							if percentage_pricing:
								pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

								for one_price in single_facility["price"]:
									lower_limit_list = []
									json_lower_limit = one_price["lower_limit_list"]

									if json_lower_limit:
										for one_lower_element in json_lower_limit:
											return_lower_elements = {}
											for each_element in one_lower_element:
												if "price" in each_element:
													return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
												else:
													return_lower_elements[each_element] = one_lower_element[each_element]

											lower_limit_list.append(return_lower_elements)

										one_price["lower_limit_list"] = lower_limit_list

									else:
										pass

									middle_limit_list = []
									json_middle_limit = one_price["middle_middle_limit_list"]

									if json_middle_limit:
										for one_middle_element in json_middle_limit:
											return_middle_elements = {}
											for each_element in one_middle_element:
												if "price" in each_element:
													return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
												else:
													return_middle_elements[each_element] = one_middle_element[each_element]

											middle_limit_list.append(return_middle_elements)

										one_price["middle_middle_limit_list"] = middle_limit_list

									upper_limit_list = []
									json_upper_limit = one_price["upper_upper_limit_list"]

									if json_upper_limit:
										for one_upper_element in json_upper_limit:
											return_upper_elements = {}
											for each_element in one_upper_element:
												if "price" in each_element:
													return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
												else:
													return_upper_elements[each_element] = one_upper_element[each_element]

											upper_limit_list.append(return_upper_elements)

										one_price["upper_upper_limit_list"] = upper_limit_list

									try:
										mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
									except Exception:
										mandatory_pricing = ""

									try:
										minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
									except Exception:
										minimum_price = ""

									try:
										extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
									except Exception:
										extra_price = ""

									try:
										child_price = str(round(float(one_price["child_price"]) * pricing_factor))
									except Exception:
										child_price = ""

									try:
										adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
									except Exception:
										adult_price = ""

									one_price["mandatory_price"] = mandatory_pricing
									one_price["minimum_price"] = minimum_price
									one_price["extra_price"] = extra_price
									one_price["child_price"] = child_price
									one_price["adult_price"] = adult_price

							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

						else:
							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

				elif single_facility["accomodation_type_id"] == "Stables":
					if guests_over_duration > 0:
						max_accomodation_guests = int(single_facility["maximum_guests"])
						quantity = int(single_facility["quantity"])

						total_space = max_accomodation_guests * quantity

						available = math.floor((total_space - guests_over_duration) / max_accomodation_guests)

						single_facility["quantity"] = str(available)
						single_facility["guests_over_duration"] = str(guests_over_duration)
						single_facility["total_space"] = str(total_space)
						single_facility["guest_total"] = guest_total

						percentage_pricing_status = single_facility["percentage_pricing_set"]

						if available > 0:
							if percentage_pricing_status:
								percentage_pricing = {}

								for single_percentage in single_facility["percentage_pricing"]:
									if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
										percentage_pricing = single_percentage

									else:
										pass

								if percentage_pricing:
									pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

									for one_price in single_facility["price"]:

										lower_limit_list = []
										json_lower_limit = one_price["lower_limit_list"]

										if json_lower_limit:
											for one_lower_element in json_lower_limit:
												return_lower_elements = {}
												for each_element in one_lower_element:
													if "price" in each_element:
														return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
													else:
														return_lower_elements[each_element] = one_lower_element[each_element]

												lower_limit_list.append(return_lower_elements)

											one_price["lower_limit_list"] = lower_limit_list

										else:
											pass

										middle_limit_list = []
										json_middle_limit = one_price["middle_middle_limit_list"]

										if json_middle_limit:
											for one_middle_element in json_middle_limit:
												return_middle_elements = {}
												for each_element in one_middle_element:
													if "price" in each_element:
														return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
													else:
														return_middle_elements[each_element] = one_middle_element[each_element]

												middle_limit_list.append(return_middle_elements)

											one_price["middle_middle_limit_list"] = middle_limit_list

										upper_limit_list = []
										json_upper_limit = one_price["upper_upper_limit_list"]

										if json_upper_limit:
											for one_upper_element in json_upper_limit:
												return_upper_elements = {}
												for each_element in one_upper_element:
													if "price" in each_element:
														return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
													else:
														return_upper_elements[each_element] = one_upper_element[each_element]

												upper_limit_list.append(return_upper_elements)

											one_price["upper_upper_limit_list"] = upper_limit_list

										try:
											mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
										except Exception:
											mandatory_pricing = ""

										try:
											minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
										except Exception:
											minimum_price = ""

										try:
											extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
										except Exception:
											extra_price = ""

										try:
											child_price = str(round(float(one_price["child_price"]) * pricing_factor))
										except Exception:
											child_price = ""

										try:
											adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
										except Exception:
											adult_price = ""

										one_price["mandatory_price"] = mandatory_pricing
										one_price["minimum_price"] = minimum_price
										one_price["extra_price"] = extra_price
										one_price["child_price"] = child_price
										one_price["adult_price"] = adult_price

								if "/public" in request.path:
									if single_facility["thumbnail"]:
										available_facility.append(single_facility)
									else:
										pass

								else:
									available_facility.append(single_facility)

							else:
								if "/public" in request.path:
									if single_facility["thumbnail"]:
										available_facility.append(single_facility)
									else:
										pass

								else:
									available_facility.append(single_facility)

						else:
							pass

					else:
						percentage_pricing_status = single_facility["percentage_pricing_set"]

						max_accomodation_guests = int(single_facility["maximum_guests"])
						quantity = int(single_facility["quantity"])

						total_space = max_accomodation_guests * quantity

						single_facility["guests_over_duration"] = str(guests_over_duration)
						single_facility["space"] = str(total_space)

						if percentage_pricing_status:
							percentage_pricing = {}

							for single_percentage in single_facility["percentage_pricing"]:
								if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
									percentage_pricing = single_percentage

								else:
									pass

							if percentage_pricing:
								pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

								for one_price in single_facility["price"]:
									lower_limit_list = []
									json_lower_limit = one_price["lower_limit_list"]

									if json_lower_limit:
										for one_lower_element in json_lower_limit:
											return_lower_elements = {}
											for each_element in one_lower_element:
												if "price" in each_element:
													return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
												else:
													return_lower_elements[each_element] = one_lower_element[each_element]

											lower_limit_list.append(return_lower_elements)

										one_price["lower_limit_list"] = lower_limit_list

									else:
										pass

									middle_limit_list = []
									json_middle_limit = one_price["middle_middle_limit_list"]

									if json_middle_limit:
										for one_middle_element in json_middle_limit:
											return_middle_elements = {}
											for each_element in one_middle_element:
												if "price" in each_element:
													return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
												else:
													return_middle_elements[each_element] = one_middle_element[each_element]

											middle_limit_list.append(return_middle_elements)

										one_price["middle_middle_limit_list"] = middle_limit_list

									upper_limit_list = []
									json_upper_limit = one_price["upper_upper_limit_list"]

									if json_upper_limit:
										for one_upper_element in json_upper_limit:
											return_upper_elements = {}
											for each_element in one_upper_element:
												if "price" in each_element:
													return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
												else:
													return_upper_elements[each_element] = one_upper_element[each_element]

											upper_limit_list.append(return_upper_elements)

										one_price["upper_upper_limit_list"] = upper_limit_list

									try:
										mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
									except Exception:
										mandatory_pricing = ""

									try:
										minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
									except Exception:
										minimum_price = ""

									try:
										extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
									except Exception:
										extra_price = ""

									try:
										child_price = str(round(float(one_price["child_price"]) * pricing_factor))
									except Exception:
										child_price = ""

									try:
										adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
									except Exception:
										adult_price = ""

									one_price["mandatory_price"] = mandatory_pricing
									one_price["minimum_price"] = minimum_price
									one_price["extra_price"] = extra_price
									one_price["child_price"] = child_price
									one_price["adult_price"] = adult_price

							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

						else:
							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

			else:
				if guests_over_duration > 0:
					pass
				else:
					single_facility["quantity"] = "1"

					percentage_pricing_status = single_facility["percentage_pricing_set"]

					if percentage_pricing_status:
						percentage_pricing = {}

						for single_percentage in single_facility["percentage_pricing"]:
							if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
								percentage_pricing = single_percentage

							else:
								pass

						if percentage_pricing:
							pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

							for one_price in single_facility["price"]:
								lower_limit_list = []
								json_lower_limit = one_price["lower_limit_list"]

								if json_lower_limit:
									for one_lower_element in json_lower_limit:
										return_lower_elements = {}
										for each_element in one_lower_element:
											if "price" in each_element:
												return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
											else:
												return_lower_elements[each_element] = one_lower_element[each_element]

										lower_limit_list.append(return_lower_elements)

									one_price["lower_limit_list"] = lower_limit_list

								else:
									pass

								middle_limit_list = []
								json_middle_limit = one_price["middle_middle_limit_list"]

								if json_middle_limit:
									for one_middle_element in json_middle_limit:
										return_middle_elements = {}
										for each_element in one_middle_element:
											if "price" in each_element:
												return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
											else:
												return_middle_elements[each_element] = one_middle_element[each_element]

										middle_limit_list.append(return_middle_elements)

									one_price["middle_middle_limit_list"] = middle_limit_list

								upper_limit_list = []
								json_upper_limit = one_price["upper_upper_limit_list"]

								if json_upper_limit:
									for one_upper_element in json_upper_limit:
										return_upper_elements = {}
										for each_element in one_upper_element:
											if "price" in each_element:
												return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
											else:
												return_upper_elements[each_element] = one_upper_element[each_element]

										upper_limit_list.append(return_upper_elements)

									one_price["upper_upper_limit_list"] = upper_limit_list

								try:
									mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
								except Exception:
									mandatory_pricing = ""

								try:
									minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
								except Exception:
									minimum_price = ""

								try:
									extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
								except Exception:
									extra_price = ""

								try:
									child_price = str(round(float(one_price["child_price"]) * pricing_factor))
								except Exception:
									child_price = ""

								try:
									adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
								except Exception:
									adult_price = ""

								one_price["mandatory_price"] = mandatory_pricing
								one_price["minimum_price"] = minimum_price
								one_price["extra_price"] = extra_price
								one_price["child_price"] = child_price
								one_price["adult_price"] = adult_price

						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_campsite.append(single_facility)
							else:
								pass

						else:
							available_campsite.append(single_facility)

					else:
						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_campsite.append(single_facility)
							else:
								pass

						else:
							available_campsite.append(single_facility)

	return_facilities = sorted(available_facility, key=lambda order: order["name"])
	return_campsites = sorted(available_campsite, key=lambda order: order["name"])

	return jsonify({"facility_data": return_facilities, "camping_data": return_campsites}), 200



@app.route("/bookings/single-facility/available/date", methods=["POST"])
def check_available_single_facility_booking_date():
	messages = []
	try:
		request.json["facility_booking_check_in_date"].strip()
		if not request.json["facility_booking_check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		request.json["facility_booking_check_out_date"].strip()
		if not request.json["facility_booking_check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	if request.json["facility_booking_check_out_date"] < request.json[
	  "facility_booking_check_in_date"]:
		output = []
		output.append(
		 "Your check out date cannot come before your check in date.")
		return jsonify({"message": output}), 422

	check_in_date = GenerateDateFromString.generateDate(
	 request.json["facility_booking_check_in_date"])
	check_out_date = GenerateDateFromString.generateDate(
	 request.json["facility_booking_check_out_date"])
	to_day = datetime.today().strftime('%Y-%m-%d')
	today = GenerateDateFromString.generateDate(to_day)

	facility_output = []
	inventory_output = []
	campsite_output = []
	accomodation_output = []
	summary_array = []
	date_array = []
	stuff_array = []

	starting = GenerateDateFromString.generateDate(
	 request.json["facility_booking_check_in_date"])
	ending = GenerateDateFromString.generateDate(
	 request.json["facility_booking_check_out_date"])
	add_day = timedelta(days=1)

	while starting <= ending:
		date_array.append(starting)

		starting += add_day

	###### Inventory Section ######

	# get_all_active_accomodation = asyncRequests.launchGetRequest(url = get_active_facilities)

	facility_id = request.json["facility_id"]
	# facility_type = request.json["facility_type"]
	
	###### Accomodation Section ######
	all_facility_bookings = []
	availability_array = []

	all_facility_bookings = [facility_id]

	for facility_booking in set(all_facility_bookings):
		return_facility_info = {}
		guest_total = []
		guest_total.append(0)
		
		try:
			return_facility = requests.get(
				get_facility_details.format(facility_booking))
				

			facility_type = return_facility.json(
			)["data"][0]["facility_type_id"]

			max_accomodation_guests = int(
				return_facility.json()["data"][0]["maximum_guests"])
			
			
			accomodation_details_array = []
			
			available_facility=[]

			for single_date in date_array:
				query_facilities = db.session.query(Facility)\
				.join(Booking, Facility.booking_id == Booking.booking_public_id)\
				.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
				Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
				.filter(Facility.deletion_marker == None)\
				.filter(Facility.facility_booking_check_in_date <= single_date)\
				.filter(Facility.facility_booking_check_out_date > single_date)\
				.filter(Facility.status != get_booking_status_id("Updated"))\
				.filter(Facility.status != get_booking_status_id("Cancelled"))\
				.filter(Facility.facility_id == facility_id)\
				.filter(Booking.deletion_marker == None)\
				.filter(Booking.status != get_booking_status_id("Cancelled"))\
				.all()

				temp_total = []
				temp_total.append(0)
				for single_query_item in query_facilities:
					if single_query_item.facility_booking_check_in_date.strftime("%Y-%m-%d") == request.json["check_out_date"]:
						pass
					else:
						if single_query_item.facility_booking_check_out_date.strftime("%Y-%m-%d") == request.json["check_in_date"]:
							pass
						else:
							temp_total.append(single_query_item.facility_booking_adults + single_query_item.facility_booking_children)
				if return_facility.json()["data"][0]["unavailability_schedule_set"]:
					for each_schedule in return_facility.json()["data"][0]["unavailability_schedule"]:
						if (each_schedule["start_date_formatted"] <= single_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= single_date.strftime("%Y-%m-%d")):
							temp_total.append(each_schedule["unavailability_number"])
						else:
							pass
	
				guest_total.append(sum(temp_total))

			guests_over_duration = max(guest_total)
			
			if return_facility.json()["data"][0]["facility_type_id"] == "Accomodation":
				if return_facility.json()["data"][0]["accomodation_type_id"] == "Pelican":
					if guests_over_duration > 0:
						available_facility.append(0)
					else:
						available_facility.append(1)
						
				elif return_facility.json()["data"][0]["accomodation_type_id"] == "Stables":
					
					facility_booking_guests = request.json["facility_booking_guests"]
					required_space = math.floor(int(facility_booking_guests)/max_accomodation_guests)
					if guests_over_duration > 0:
						max_accomodation_guests = int(return_facility.json()["data"][0]["maximum_guests"])
						quantity = int(return_facility.json()["data"][0]["quantity"])

						total_space = max_accomodation_guests * quantity
					   
						available = math.floor((total_space - guests_over_duration) / max_accomodation_guests)
						if available >= required_space:
							available_facility.append(1)
						else:
							available_facility.append(0)

					else:
						available_facility.append(1)
				
			else:
				if guests_over_duration > 0:
					available_facility.append(0)
				else:
					available_facility.append(1)
					

		except KeyError as e:
			print(e)

	
	if available_facility[0] == 1:
		message = []
		message.append("Facility is available.")
		return jsonify({"message": message}), 201
	else:
		message = []
		message.append("Facility is fully booked.")
		return jsonify({"message": message}), 422
			
			

@app.route("/bookings/facility/available/test", methods = ["POST"])
def check_available_facility_booking_test():
	messages = []

	try:
		request.json["check_in_date"].strip()
		if not request.json["check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		request.json["check_out_date"].strip()
		if not request.json["check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	if request.json["check_out_date"] < request.json["check_in_date"]:
		output = []
		output.append("Your check out date cannot come before your check in date.")
		return jsonify({"message": output}), 422

	check_in_date = GenerateDateFromString.generateDate(request.json["check_in_date"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out_date"])

	output = []
	blank_array = []
	date_array = []

	while check_in_date <= check_out_date:
		date_array.append(check_in_date)

		check_in_date = check_in_date + timedelta(days = 1)

	return_facility_details = requests.post(get_filtered_not_active_accomodation,\
			  json = {"facility_id": []})

	return_camping_details = requests.post(get_filtered_not_campsites,\
				json = {"facility_id": []})


	facility_booking_id = []

	return_unavailable_in = db.session.query(Facility)\
			.join(Booking, Facility.booking_id == Booking.booking_public_id)\
			.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
			   Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id,\
			   Facility.facility_booking_public_id)\
			.filter(Facility.deletion_marker == None)\
			.filter(Facility.facility_booking_check_in_date <= check_in_date)\
			.filter(Facility.facility_booking_check_out_date > check_in_date)\
			.filter(Facility.status != get_booking_status_id("Updated"))\
			.filter(Booking.deletion_marker == None)\
			.filter(Booking.status != get_booking_status_id("Cancelled"))\
			.all()

	for single_in in return_unavailable_in:
		facility_booking_id.append(single_in.facility_booking_public_id)

	return_unavailable_out = db.session.query(Facility)\
			.join(Booking, Facility.booking_id == Booking.booking_public_id)\
			.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
			   Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id,\
			   Facility.facility_booking_public_id)\
			.filter(Facility.deletion_marker == None)\
			.filter(Facility.facility_booking_check_out_date > check_out_date)\
			.filter(Facility.facility_booking_check_in_date < check_out_date)\
			.filter(Facility.status != get_booking_status_id("Updated"))\
			.filter(Booking.deletion_marker == None)\
			.filter(Booking.status != get_booking_status_id("Cancelled"))\
			.all()

	for single_out in return_unavailable_out:
		facility_booking_id.append(single_out.facility_booking_public_id)

	facility_array = []
	occupied_array = []
	x_array = []
	for single_date in date_array:
		query_facilities = db.session.query(Facility)\
				.join(Booking, Facility.booking_id == Booking.booking_public_id)\
				.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
					 Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
				.filter(Facility.facility_booking_public_id.in_(facility_booking_id))\
				.filter(Facility.deletion_marker == None)\
				.filter(Facility.facility_booking_check_in_date <= single_date)\
				.filter(Facility.facility_booking_check_out_date > single_date)\
				.filter(Facility.status != get_booking_status_id("Updated"))\
				.filter(Booking.deletion_marker == None)\
				.filter(Booking.status != get_booking_status_id("Cancelled"))\
				.all()

		# query_facilities_alt = db.session.query(Facility)\
		# 							 .join(Booking, Facility.booking_id == Booking.booking_public_id)\
		# 							 .add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
		# 								 		  Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
		# 							 .filter(Facility.facility_booking_public_id.in_(facility_booking_id))\
		# 							 .filter(Facility.deletion_marker == None)\
		# 							 .filter(Facility.facility_booking_check_in_date > single_date)\
		# 							 .filter(Facility.facility_booking_check_out_date < single_date)\
		# 							 .filter(Facility.status != get_booking_status_id("Updated"))\
		# 							 .filter(Booking.deletion_marker == None)\
		# 							 .filter(Booking.status != get_booking_status_id("Cancelled"))\
		# 							 .all()

		# if query_facilities_alt:
		# 	query_facilities = query_facilities + query_facilities_alt

		for single_facility in query_facilities:
			if (GenerateDateFromString.generateDate((single_facility.facility_booking_check_in_date).strftime("%Y-%m-%d")) <= single_date)\
			& (GenerateDateFromString.generateDate((single_facility.facility_booking_check_out_date).strftime("%Y-%m-%d")) > single_date):
				if GenerateDateFromString.generateDate((single_facility.facility_booking_check_out_date).strftime("%Y-%m-%d")) == single_date:
					pass
				else:
					guest_sum = single_facility.facility_booking_adults + single_facility.facility_booking_children

					facility_array.append((single_facility.facility_id, guest_sum))
					x_array.append((single_facility.facility_id, guest_sum, str(single_date)))
					occupied_array.append(single_facility.facility_id)

	facility_guest_dict = defaultdict(list)
	test_dict = defaultdict(list)

	for one_facility, guest_count in facility_array:
		facility_guest_dict[one_facility].append(guest_count)

	for a, b, c in x_array:
		test_dict[a].append((c, b))

	stuff = []

	for single_item, itemised in test_dict.items():
		return_info = {}

		xyz = []
		for each_itemised in itemised:
			xyz.append(each_itemised)

		text_dict = defaultdict(list)
		for x1, y1 in xyz:
			text_dict[x1].append(y1)

		res = [{alpha: sum(beta)} for alpha, beta in text_dict.items()]

		return_info[single_item] = res

		stuff.append(return_info)

	result_array = [{x: sum(y) for x, y in facility_guest_dict.items()}]
	y_array = [{x: y for x, y in facility_guest_dict.items()}]

	available_facility = []
	for single_facility in return_facility_details.json()["data"]:
		if single_facility["accomodation_type"] == "Pelican":
			if single_facility["public_id"] in occupied_array:
				pass

			else:
				if single_facility["unavailability_schedule_set"]:
					pass

				else:
					percentage_pricing_status = single_facility["percentage_pricing_set"]

					if percentage_pricing_status:
						percentage_pricing = {}

						for single_percentage in single_facility["percentage_pricing"]:
							if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
								percentage_pricing = single_percentage

							else:
								pass

						if percentage_pricing:
							pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

							for one_price in single_facility["price"]:
								lower_limit_list = []
								json_lower_limit = one_price["lower_limit_list"]

								if json_lower_limit:
									for one_lower_element in json_lower_limit:
										return_lower_elements = {}
										for each_element in one_lower_element:
											if "price" in each_element:
												return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
											else:
												return_lower_elements[each_element] = one_lower_element[each_element]

										lower_limit_list.append(return_lower_elements)

									one_price["lower_limit_list"] = lower_limit_list

								else:
									pass

								middle_limit_list = []
								json_middle_limit = one_price["middle_middle_limit_list"]

								if json_middle_limit:
									for one_middle_element in json_middle_limit:
										return_middle_elements = {}
										for each_element in one_middle_element:
											if "price" in each_element:
												return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
											else:
												return_middle_elements[each_element] = one_middle_element[each_element]

										middle_limit_list.append(return_middle_elements)

									one_price["middle_middle_limit_list"] = middle_limit_list

								upper_limit_list = []
								json_upper_limit = one_price["upper_upper_limit_list"]

								if json_upper_limit:
									for one_upper_element in json_upper_limit:
										return_upper_elements = {}
										for each_element in one_upper_element:
											if "price" in each_element:
												return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
											else:
												return_upper_elements[each_element] = one_upper_element[each_element]

										upper_limit_list.append(return_upper_elements)

									one_price["upper_upper_limit_list"] = upper_limit_list

								try:
									mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
								except Exception:
									mandatory_pricing = ""

								try:
									minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
								except Exception:
									minimum_price = ""

								try:
									extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
								except Exception:
									extra_price = ""

								try:
									child_price = str(round(float(one_price["child_price"]) * pricing_factor))
								except Exception:
									child_price = ""

								try:
									adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
								except Exception:
									adult_price = ""

								one_price["mandatory_price"] = mandatory_pricing
								one_price["minimum_price"] = minimum_price
								one_price["extra_price"] = extra_price
								one_price["child_price"] = child_price
								one_price["adult_price"] = adult_price

						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

					else:
						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

		elif single_facility["accomodation_type"] == "Stables":
			if single_facility["public_id"] in occupied_array:
				# available_facility.append(single_facility)

				for single_unavailable in stuff:
					for unavailable in single_unavailable:
						guests_over_time = []

						if unavailable == single_facility["public_id"]:
							for single_list in single_unavailable[unavailable]:
								for each_item in single_list:
									guests_over_time.append(single_list[each_item])

							highest_guest_count = max(guests_over_time)
							max_capacity = float(single_facility["maximum_guests"])
							quantity = float(single_facility["quantity"])

							# if (highest_guest_count > 0) & (highest_guest_count < (quantity * max_capacity)):
							# 	highest_guest_count = highest_guest_count + ((quantity * max_capacity) - highest_guest_count)

							maximum = max_capacity * quantity

							available_modulus = (maximum - highest_guest_count) % max_capacity

							if available_modulus > 0:
								highest_guest_count = highest_guest_count + (maximum - highest_guest_count)
								available = (maximum - highest_guest_count) / max_capacity
							else:
								available = (maximum - highest_guest_count) / max_capacity

							unavailability_array = []
							unavailability_array.append(0)
							if single_facility["unavailability_schedule_set"]:
								for unavailability_schedule in single_facility["unavailability_schedule"]:
									for each_un_date in date_array:
										if (unavailability_schedule["start_date_formatted"] < each_un_date.strftime("%Y-%m-%d")) & (unavailability_schedule["end_date_formatted"] >= each_un_date.strftime("%Y-%m-%d")):
											unavailability_array.append(unavailability_schedule["unavailability_number"])
										else:
											unavailability_array.append(0)

							# max_rooms_unavailable = max(unavailability_array) / max_capacity
							max_rooms_unavailable = max(unavailability_array)

							available = available - max_rooms_unavailable

							single_facility["max_avail"] = max(unavailability_array)
							single_facility["array"] = unavailability_array
							single_facility["quantity"] = str(available)

							single_facility["xyz"] = (maximum - highest_guest_count) / max_capacity
							single_facility["highest_guest_count"] = highest_guest_count
							single_facility["guests_over_time"] = guests_over_time

							if available > 0:
								single_facility["quantity"] = str(available)

								percentage_pricing_status = single_facility["percentage_pricing_set"]

								if percentage_pricing_status:
									percentage_pricing = {}

									for single_percentage in single_facility["percentage_pricing"]:
										if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
											percentage_pricing = single_percentage

										else:
											pass

									if percentage_pricing:
										pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

										for one_price in single_facility["price"]:
											lower_limit_list = []
											json_lower_limit = one_price["lower_limit_list"]

											if json_lower_limit:
												for one_lower_element in json_lower_limit:
													return_lower_elements = {}
													for each_element in one_lower_element:
														if "price" in each_element:
															return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
														else:
															return_lower_elements[each_element] = one_lower_element[each_element]

													lower_limit_list.append(return_lower_elements)

												one_price["lower_limit_list"] = lower_limit_list

											else:
												pass

											middle_limit_list = []
											json_middle_limit = one_price["middle_middle_limit_list"]

											if json_middle_limit:
												for one_middle_element in json_middle_limit:
													return_middle_elements = {}
													for each_element in one_middle_element:
														if "price" in each_element:
															return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
														else:
															return_middle_elements[each_element] = one_middle_element[each_element]

													middle_limit_list.append(return_middle_elements)

												one_price["middle_middle_limit_list"] = middle_limit_list

											upper_limit_list = []
											json_upper_limit = one_price["upper_upper_limit_list"]

											if json_upper_limit:
												for one_upper_element in json_upper_limit:
													return_upper_elements = {}
													for each_element in one_upper_element:
														if "price" in each_element:
															return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
														else:
															return_upper_elements[each_element] = one_upper_element[each_element]

													upper_limit_list.append(return_upper_elements)

												one_price["upper_upper_limit_list"] = upper_limit_list

											try:
												mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
											except Exception:
												mandatory_pricing = ""

											try:
												minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
											except Exception:
												minimum_price = ""

											try:
												extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
											except Exception:
												extra_price = ""

											try:
												child_price = str(round(float(one_price["child_price"]) * pricing_factor))
											except Exception:
												child_price = ""

											try:
												adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
											except Exception:
												adult_price = ""

											one_price["mandatory_price"] = mandatory_pricing
											one_price["minimum_price"] = minimum_price
											one_price["extra_price"] = extra_price
											one_price["child_price"] = child_price
											one_price["adult_price"] = adult_price

									available_facility.append(single_facility)

								else:
									available_facility.append(single_facility)

							else:
								pass
								# available_facility.append(single_facility)

							# single_facility["rooms_avail"] = available
							# available_facility.append(single_facility)

						else:
							pass

			else:
				percentage_pricing_status = single_facility["percentage_pricing_set"]

				if percentage_pricing_status:
					percentage_pricing = {}

					for single_percentage in single_facility["percentage_pricing"]:
						if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
							percentage_pricing = single_percentage

						else:
							pass

					if percentage_pricing:
						pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

						for one_price in single_facility["price"]:
							lower_limit_list = []
							json_lower_limit = one_price["lower_limit_list"]

							if json_lower_limit:
								for one_lower_element in json_lower_limit:
									return_lower_elements = {}
									for each_element in one_lower_element:
										if "price" in each_element:
											return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
										else:
											return_lower_elements[each_element] = one_lower_element[each_element]

									lower_limit_list.append(return_lower_elements)

								one_price["lower_limit_list"] = lower_limit_list

							else:
								pass

							middle_limit_list = []
							json_middle_limit = one_price["middle_middle_limit_list"]

							if json_middle_limit:
								for one_middle_element in json_middle_limit:
									return_middle_elements = {}
									for each_element in one_middle_element:
										if "price" in each_element:
											return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
										else:
											return_middle_elements[each_element] = one_middle_element[each_element]

									middle_limit_list.append(return_middle_elements)

								one_price["middle_middle_limit_list"] = middle_limit_list

							upper_limit_list = []
							json_upper_limit = one_price["upper_upper_limit_list"]

							if json_upper_limit:
								for one_upper_element in json_upper_limit:
									return_upper_elements = {}
									for each_element in one_upper_element:
										if "price" in each_element:
											return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
										else:
											return_upper_elements[each_element] = one_upper_element[each_element]

									upper_limit_list.append(return_upper_elements)

								one_price["upper_upper_limit_list"] = upper_limit_list

							try:
								mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
							except Exception:
								mandatory_pricing = ""

							try:
								minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
							except Exception:
								minimum_price = ""

							try:
								extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
							except Exception:
								extra_price = ""

							try:
								child_price = str(round(float(one_price["child_price"]) * pricing_factor))
							except Exception:
								child_price = ""

							try:
								adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
							except Exception:
								adult_price = ""

							one_price["mandatory_price"] = mandatory_pricing
							one_price["minimum_price"] = minimum_price
							one_price["extra_price"] = extra_price
							one_price["child_price"] = child_price
							one_price["adult_price"] = adult_price

					if "/public" in request.path:
						if single_facility["thumbnail"]:
							available_facility.append(single_facility)
						else:
							pass

					else:
						available_facility.append(single_facility)

				else:
					if "/public" in request.path:
						if single_facility["thumbnail"]:
							available_facility.append(single_facility)
						else:
							pass

					else:
						available_facility.append(single_facility)

	return_facilities = sorted(available_facility, key=lambda order: order["name"])

	available_campsite = []
	for single_campsite in return_camping_details.json()["data"]:
		if single_campsite["public_id"] in occupied_array:
			pass

		else:
			single_campsite["quantity"] = "1"

			percentage_pricing_status = single_campsite["percentage_pricing_set"]

			if percentage_pricing_status:
				percentage_pricing = {}

				for single_percentage in single_campsite["percentage_pricing"]:
					if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
						percentage_pricing = single_percentage

					else:
						pass

				if percentage_pricing:
					pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

					for one_price in single_campsite["price"]:
						lower_limit_list = []
						json_lower_limit = one_price["lower_limit_list"]

						if json_lower_limit:
							for one_lower_element in json_lower_limit:
								return_lower_elements = {}
								for each_element in one_lower_element:
									if "price" in each_element:
										return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
									else:
										return_lower_elements[each_element] = one_lower_element[each_element]

								lower_limit_list.append(return_lower_elements)

							one_price["lower_limit_list"] = lower_limit_list

						else:
							pass

						middle_limit_list = []
						json_middle_limit = one_price["middle_middle_limit_list"]

						if json_middle_limit:
							for one_middle_element in json_middle_limit:
								return_middle_elements = {}
								for each_element in one_middle_element:
									if "price" in each_element:
										return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
									else:
										return_middle_elements[each_element] = one_middle_element[each_element]

								middle_limit_list.append(return_middle_elements)

							one_price["middle_middle_limit_list"] = middle_limit_list

						upper_limit_list = []
						json_upper_limit = one_price["upper_upper_limit_list"]

						if json_upper_limit:
							for one_upper_element in json_upper_limit:
								return_upper_elements = {}
								for each_element in one_upper_element:
									if "price" in each_element:
										return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
									else:
										return_upper_elements[each_element] = one_upper_element[each_element]

								upper_limit_list.append(return_upper_elements)

							one_price["upper_upper_limit_list"] = upper_limit_list

						try:
							mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
						except Exception:
							mandatory_pricing = ""

						try:
							minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
						except Exception:
							minimum_price = ""

						try:
							extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
						except Exception:
							extra_price = ""

						try:
							child_price = str(round(float(one_price["child_price"]) * pricing_factor))
						except Exception:
							child_price = ""

						try:
							adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
						except Exception:
							adult_price = ""

						one_price["mandatory_price"] = mandatory_pricing
						one_price["minimum_price"] = minimum_price
						one_price["extra_price"] = extra_price
						one_price["child_price"] = child_price
						one_price["adult_price"] = adult_price

				if "/public" in request.path:
					if single_campsite["thumbnail"]:
						available_campsite.append(single_campsite)
					else:
						pass

				else:
					available_campsite.append(single_campsite)

			else:
				if "/public" in request.path:
					if single_campsite["thumbnail"]:
						available_campsite.append(single_campsite)
					else:
						pass

				else:
					available_campsite.append(single_campsite)

	return_campsites = sorted(available_campsite, key=lambda order: order["name"])

	# return jsonify({"facility_data": returned_data, "facility_message": returned_message, "camping_data": camping_data, "camping_message": camping_message}), 200
	# return jsonify({"dates": date_array, "facility_array": facility_array, "result_array": result_array, "facility_data": return_facilities}), 200
	return jsonify({"facility_data": return_facilities, "camping_data": return_campsites, "123": str(x_array), "facility_booking_id": facility_booking_id}), 200


@app.route("/facility-test", methods = ["POST"])
@app.route("/facility-test/public", methods = ["POST"])
def new_test():
	messages = []

	try:
		request.json["check_in_date"].strip()
		if not request.json["check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		request.json["check_out_date"].strip()
		if not request.json["check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	if request.json["check_out_date"] < request.json["check_in_date"]:
		output = []
		output.append("Your check out date cannot come before your check in date.")
		return jsonify({"message": output}), 422

	check_in_date = GenerateDateFromString.generateDate(request.json["check_in_date"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out_date"])

	output = []
	blank_array = []
	date_array = []

	while check_in_date <= check_out_date:
		date_array.append(check_in_date)

		check_in_date = check_in_date + timedelta(days = 1)

	get_all_active_accomodation = requests.get(get_active_facilities)

	available_facility = []
	available_campsite = []

	for single_facility in get_all_active_accomodation.json()["data"]:
		guest_total = []
		guest_total.append(0)

		for single_date in date_array:
			query_facilities = db.session.query(Facility)\
					.join(Booking, Facility.booking_id == Booking.booking_public_id)\
					.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
						Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
					.filter(Facility.deletion_marker == None)\
					.filter(Facility.facility_booking_check_in_date <= single_date)\
					.filter(Facility.facility_booking_check_out_date > single_date)\
					.filter(Facility.status != get_booking_status_id("Updated"))\
					.filter(Facility.status != get_booking_status_id("Cancelled"))\
					.filter(Facility.facility_id == single_facility["public_id"])\
					.filter(Booking.deletion_marker == None)\
					.filter(Booking.status != get_booking_status_id("Cancelled"))\
					.all()

			temp_total = []
			temp_total.append(0)

			for single_query_item in query_facilities:
				if single_query_item.facility_booking_check_in_date.strftime("%Y-%m-%d") == request.json["check_out_date"]:
					pass
				else:
					if single_query_item.facility_booking_check_out_date.strftime("%Y-%m-%d") == request.json["check_in_date"]:
						pass
					else:
						temp_total.append(single_query_item.facility_booking_adults + single_query_item.facility_booking_children)

			if single_facility["unavailability_schedule_set"]:
				for each_schedule in single_facility["unavailability_schedule"]:
					if (each_schedule["start_date_formatted"] <= single_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= single_date.strftime("%Y-%m-%d")):
						temp_total.append(each_schedule["unavailability_number"])
					else:
						pass

			guest_total.append(sum(temp_total))

		guests_over_duration = max(guest_total)

		if single_facility["facility_type_id"] == "Accomodation":
			if single_facility["accomodation_type_id"] == "Pelican":
				if guests_over_duration > 0:
					pass
				else:
					percentage_pricing_status = single_facility["percentage_pricing_set"]

					if percentage_pricing_status:
						percentage_pricing = {}

						for single_percentage in single_facility["percentage_pricing"]:
							if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
								percentage_pricing = single_percentage
							else:
								pass

						if percentage_pricing:
							pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

							for one_price in single_facility["price"]:
								lower_limit_list = []
								json_lower_limit = one_price["lower_limit_list"]

								if json_lower_limit:
									for one_lower_element in json_lower_limit:
										return_lower_elements = {}
										for each_element in one_lower_element:
											if "price" in each_element:
												return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
											else:
												return_lower_elements[each_element] = one_lower_element[each_element]

										lower_limit_list.append(return_lower_elements)

									one_price["lower_limit_list"] = lower_limit_list

								else:
									pass

								middle_limit_list = []
								json_middle_limit = one_price["middle_middle_limit_list"]

								if json_middle_limit:
									for one_middle_element in json_middle_limit:
										return_middle_elements = {}
										for each_element in one_middle_element:
											if "price" in each_element:
												return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
											else:
												return_middle_elements[each_element] = one_middle_element[each_element]

										middle_limit_list.append(return_middle_elements)

									one_price["middle_middle_limit_list"] = middle_limit_list

								upper_limit_list = []
								json_upper_limit = one_price["upper_upper_limit_list"]

								if json_upper_limit:
									for one_upper_element in json_upper_limit:
										return_upper_elements = {}
										for each_element in one_upper_element:
											if "price" in each_element:
												return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
											else:
												return_upper_elements[each_element] = one_upper_element[each_element]

										upper_limit_list.append(return_upper_elements)

									one_price["upper_upper_limit_list"] = upper_limit_list

								try:
									mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
								except Exception:
									mandatory_pricing = ""

								try:
									minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
								except Exception:
									minimum_price = ""

								try:
									extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
								except Exception:
									extra_price = ""

								try:
									child_price = str(round(float(one_price["child_price"]) * pricing_factor))
								except Exception:
									child_price = ""

								try:
									adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
								except Exception:
									adult_price = ""

								one_price["mandatory_price"] = mandatory_pricing
								one_price["minimum_price"] = minimum_price
								one_price["extra_price"] = extra_price
								one_price["child_price"] = child_price
								one_price["adult_price"] = adult_price

						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

					else:
						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

			elif single_facility["accomodation_type_id"] == "Stables":
				if guests_over_duration > 0:
					max_accomodation_guests = int(single_facility["maximum_guests"])
					quantity = int(single_facility["quantity"])

					total_space = max_accomodation_guests * quantity

					available = math.floor((total_space - guests_over_duration) / max_accomodation_guests)

					single_facility["quantity"] = str(available)
					single_facility["guests_over_duration"] = str(guests_over_duration)
					single_facility["total_space"] = str(total_space)
					single_facility["guest_total"] = guest_total

					percentage_pricing_status = single_facility["percentage_pricing_set"]

					if available > 0:
						if percentage_pricing_status:
							percentage_pricing = {}

							for single_percentage in single_facility["percentage_pricing"]:
								if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
									percentage_pricing = single_percentage

								else:
									pass

							if percentage_pricing:
								pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

								for one_price in single_facility["price"]:
									lower_limit_list = []
									json_lower_limit = one_price["lower_limit_list"]

									if json_lower_limit:
										for one_lower_element in json_lower_limit:
											return_lower_elements = {}
											for each_element in one_lower_element:
												if "price" in each_element:
													return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
												else:
													return_lower_elements[each_element] = one_lower_element[each_element]

											lower_limit_list.append(return_lower_elements)

										one_price["lower_limit_list"] = lower_limit_list

									else:
										pass

									middle_limit_list = []
									json_middle_limit = one_price["middle_middle_limit_list"]

									if json_middle_limit:
										for one_middle_element in json_middle_limit:
											return_middle_elements = {}
											for each_element in one_middle_element:
												if "price" in each_element:
													return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
												else:
													return_middle_elements[each_element] = one_middle_element[each_element]

											middle_limit_list.append(return_middle_elements)

										one_price["middle_middle_limit_list"] = middle_limit_list

									upper_limit_list = []
									json_upper_limit = one_price["upper_upper_limit_list"]

									if json_upper_limit:
										for one_upper_element in json_upper_limit:
											return_upper_elements = {}
											for each_element in one_upper_element:
												if "price" in each_element:
													return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
												else:
													return_upper_elements[each_element] = one_upper_element[each_element]

											upper_limit_list.append(return_upper_elements)

										one_price["upper_upper_limit_list"] = upper_limit_list

									try:
										mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
									except Exception:
										mandatory_pricing = ""

									try:
										minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
									except Exception:
										minimum_price = ""

									try:
										extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
									except Exception:
										extra_price = ""

									try:
										child_price = str(round(float(one_price["child_price"]) * pricing_factor))
									except Exception:
										child_price = ""

									try:
										adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
									except Exception:
										adult_price = ""

									one_price["mandatory_price"] = mandatory_pricing
									one_price["minimum_price"] = minimum_price
									one_price["extra_price"] = extra_price
									one_price["child_price"] = child_price
									one_price["adult_price"] = adult_price

							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

						else:
							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

					else:
						pass

				else:
					percentage_pricing_status = single_facility["percentage_pricing_set"]

					max_accomodation_guests = int(single_facility["maximum_guests"])
					quantity = int(single_facility["quantity"])

					total_space = max_accomodation_guests * quantity

					single_facility["guests_over_duration"] = str(guests_over_duration)
					single_facility["space"] = str(total_space)

					if percentage_pricing_status:
						percentage_pricing = {}

						for single_percentage in single_facility["percentage_pricing"]:
							if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
								percentage_pricing = single_percentage

							else:
								pass

						if percentage_pricing:
							pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

							for one_price in single_facility["price"]:
								lower_limit_list = []
								json_lower_limit = one_price["lower_limit_list"]

								if json_lower_limit:
									for one_lower_element in json_lower_limit:
										return_lower_elements = {}
										for each_element in one_lower_element:
											if "price" in each_element:
												return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
											else:
												return_lower_elements[each_element] = one_lower_element[each_element]

										lower_limit_list.append(return_lower_elements)

									one_price["lower_limit_list"] = lower_limit_list

								else:
									pass

								middle_limit_list = []
								json_middle_limit = one_price["middle_middle_limit_list"]

								if json_middle_limit:
									for one_middle_element in json_middle_limit:
										return_middle_elements = {}
										for each_element in one_middle_element:
											if "price" in each_element:
												return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
											else:
												return_middle_elements[each_element] = one_middle_element[each_element]

										middle_limit_list.append(return_middle_elements)

									one_price["middle_middle_limit_list"] = middle_limit_list

								upper_limit_list = []
								json_upper_limit = one_price["upper_upper_limit_list"]

								if json_upper_limit:
									for one_upper_element in json_upper_limit:
										return_upper_elements = {}
										for each_element in one_upper_element:
											if "price" in each_element:
												return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
											else:
												return_upper_elements[each_element] = one_upper_element[each_element]

										upper_limit_list.append(return_upper_elements)

									one_price["upper_upper_limit_list"] = upper_limit_list

								try:
									mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
								except Exception:
									mandatory_pricing = ""

								try:
									minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
								except Exception:
									minimum_price = ""

								try:
									extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
								except Exception:
									extra_price = ""

								try:
									child_price = str(round(float(one_price["child_price"]) * pricing_factor))
								except Exception:
									child_price = ""

								try:
									adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
								except Exception:
									adult_price = ""

								one_price["mandatory_price"] = mandatory_pricing
								one_price["minimum_price"] = minimum_price
								one_price["extra_price"] = extra_price
								one_price["child_price"] = child_price
								one_price["adult_price"] = adult_price

						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

					else:
						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

		else:
			if guests_over_duration > 0:
				pass
			else:
				single_facility["quantity"] = "1"

				percentage_pricing_status = single_facility["percentage_pricing_set"]

				if percentage_pricing_status:
					percentage_pricing = {}

					for single_percentage in single_facility["percentage_pricing"]:
						if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
							percentage_pricing = single_percentage

						else:
							pass

					if percentage_pricing:
						pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

						for one_price in single_facility["price"]:
							lower_limit_list = []
							json_lower_limit = one_price["lower_limit_list"]

							if json_lower_limit:
								for one_lower_element in json_lower_limit:
									return_lower_elements = {}
									for each_element in one_lower_element:
										if "price" in each_element:
											return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
										else:
											return_lower_elements[each_element] = one_lower_element[each_element]

									lower_limit_list.append(return_lower_elements)

								one_price["lower_limit_list"] = lower_limit_list

							else:
								pass

							middle_limit_list = []
							json_middle_limit = one_price["middle_middle_limit_list"]

							if json_middle_limit:
								for one_middle_element in json_middle_limit:
									return_middle_elements = {}
									for each_element in one_middle_element:
										if "price" in each_element:
											return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
										else:
											return_middle_elements[each_element] = one_middle_element[each_element]

									middle_limit_list.append(return_middle_elements)

								one_price["middle_middle_limit_list"] = middle_limit_list

							upper_limit_list = []
							json_upper_limit = one_price["upper_upper_limit_list"]

							if json_upper_limit:
								for one_upper_element in json_upper_limit:
									return_upper_elements = {}
									for each_element in one_upper_element:
										if "price" in each_element:
											return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
										else:
											return_upper_elements[each_element] = one_upper_element[each_element]

									upper_limit_list.append(return_upper_elements)

								one_price["upper_upper_limit_list"] = upper_limit_list

							try:
								mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
							except Exception:
								mandatory_pricing = ""

							try:
								minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
							except Exception:
								minimum_price = ""

							try:
								extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
							except Exception:
								extra_price = ""

							try:
								child_price = str(round(float(one_price["child_price"]) * pricing_factor))
							except Exception:
								child_price = ""

							try:
								adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
							except Exception:
								adult_price = ""

							one_price["mandatory_price"] = mandatory_pricing
							one_price["minimum_price"] = minimum_price
							one_price["extra_price"] = extra_price
							one_price["child_price"] = child_price
							one_price["adult_price"] = adult_price

					if "/public" in request.path:
						if single_facility["thumbnail"]:
							available_campsite.append(single_facility)
						else:
							pass

					else:
						available_campsite.append(single_facility)

				else:
					if "/public" in request.path:
						if single_facility["thumbnail"]:
							available_campsite.append(single_facility)
						else:
							pass

					else:
						available_campsite.append(single_facility)

	return_facilities = sorted(available_facility, key=lambda order: order["name"])
	return_campsites = sorted(available_campsite, key=lambda order: order["name"])

	return jsonify({"facility_data": return_facilities, "camping_data": return_campsites}), 200


# TODO: Remove after testing is done
@app.route("/bookings/facility/available/date/test", methods = ["POST"])
@app.route("/bookings/public/facility/available/date/test", methods = ["POST"])
def check_available_facility_booking_date_test():
	messages = []

	try:
		request.json["check_in_date"].strip()
		if not request.json["check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		request.json["check_out_date"].strip()
		if not request.json["check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	if request.json["check_out_date"] < request.json["check_in_date"]:
		output = []
		output.append("Your check out date cannot come before your check in date.")
		return jsonify({"message": output}), 422

	check_in_date = GenerateDateFromString.generateDate(request.json["check_in_date"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out_date"])

	output = []
	blank_array = []
	date_array = []

	while check_in_date <= check_out_date:
		date_array.append(check_in_date)

		check_in_date = check_in_date + timedelta(days = 1)

	# get_all_active_accomodation = requests.get(get_active_facilities)
	get_all_active_accomodation = requests.post(get_active_facilities_with_pricing, json = {
	 "start_date": request.json["check_in_date"],
	 "end_date": request.json["check_out_date"]
	})

	available_facility = []
	available_campsite = []

	for single_facility in get_all_active_accomodation.json()["data"]:
		guest_total = []
		guest_total.append(0)

		for single_date in date_array:
			query_facilities = db.session.query(Facility)\
					.join(Booking, Facility.booking_id == Booking.booking_public_id)\
					.add_columns(Facility.facility_id, Facility.facility_booking_check_out_date, Facility.facility_booking_check_in_date,\
						Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
					.filter(Facility.deletion_marker == None)\
					.filter(Facility.facility_booking_check_in_date <= single_date)\
					.filter(Facility.facility_booking_check_out_date > single_date)\
					.filter(Facility.status != get_booking_status_id("Updated"))\
					.filter(Facility.status != get_booking_status_id("Cancelled"))\
					.filter(Facility.facility_id == single_facility["public_id"])\
					.filter(Booking.deletion_marker == None)\
					.filter(Booking.status != get_booking_status_id("Cancelled"))\
					.all()

			temp_total = []
			temp_total.append(0)

			for single_query_item in query_facilities:
				if single_query_item.facility_booking_check_in_date.strftime("%Y-%m-%d") == request.json["check_out_date"]:
					pass
				else:
					if single_query_item.facility_booking_check_out_date.strftime("%Y-%m-%d") == request.json["check_in_date"]:
						pass
					else:
						temp_total.append(single_query_item.facility_booking_adults + single_query_item.facility_booking_children)

			if single_facility["unavailability_schedule_set"]:
				for each_schedule in single_facility["unavailability_schedule"]:
					if (each_schedule["start_date_formatted"] <= single_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= single_date.strftime("%Y-%m-%d")):
						temp_total.append(each_schedule["unavailability_number"])
					else:
						pass

			guest_total.append(sum(temp_total))

		guests_over_duration = max(guest_total)

		if single_facility["facility_type_id"] == "Accomodation":
			if single_facility["accomodation_type_id"] == "Pelican":
				if guests_over_duration > 0:
					pass
				else:
					percentage_pricing_status = single_facility["percentage_pricing_set"]

					if percentage_pricing_status:
						percentage_pricing = {}

						for single_percentage in single_facility["percentage_pricing"]:
							if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
								percentage_pricing = single_percentage
							else:
								pass

						if percentage_pricing:
							pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

							for one_price in single_facility["price"]:
								lower_limit_list = []
								json_lower_limit = one_price["lower_limit_list"]

								if json_lower_limit:
									for one_lower_element in json_lower_limit:
										return_lower_elements = {}
										for each_element in one_lower_element:
											if "price" in each_element:
												return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
											else:
												return_lower_elements[each_element] = one_lower_element[each_element]

										lower_limit_list.append(return_lower_elements)

									one_price["lower_limit_list"] = lower_limit_list

								else:
									pass

								middle_limit_list = []
								json_middle_limit = one_price["middle_middle_limit_list"]

								if json_middle_limit:
									for one_middle_element in json_middle_limit:
										return_middle_elements = {}
										for each_element in one_middle_element:
											if "price" in each_element:
												return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
											else:
												return_middle_elements[each_element] = one_middle_element[each_element]

										middle_limit_list.append(return_middle_elements)

									one_price["middle_middle_limit_list"] = middle_limit_list

								upper_limit_list = []
								json_upper_limit = one_price["upper_upper_limit_list"]

								if json_upper_limit:
									for one_upper_element in json_upper_limit:
										return_upper_elements = {}
										for each_element in one_upper_element:
											if "price" in each_element:
												return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
											else:
												return_upper_elements[each_element] = one_upper_element[each_element]

										upper_limit_list.append(return_upper_elements)

									one_price["upper_upper_limit_list"] = upper_limit_list

								try:
									mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
								except Exception:
									mandatory_pricing = ""

								try:
									minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
								except Exception:
									minimum_price = ""

								try:
									extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
								except Exception:
									extra_price = ""

								try:
									child_price = str(round(float(one_price["child_price"]) * pricing_factor))
								except Exception:
									child_price = ""

								try:
									adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
								except Exception:
									adult_price = ""

								one_price["mandatory_price"] = mandatory_pricing
								one_price["minimum_price"] = minimum_price
								one_price["extra_price"] = extra_price
								one_price["child_price"] = child_price
								one_price["adult_price"] = adult_price

						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

					else:
						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

			elif single_facility["accomodation_type_id"] == "Stables":
				if guests_over_duration > 0:
					max_accomodation_guests = int(single_facility["maximum_guests"])
					quantity = int(single_facility["quantity"])

					total_space = max_accomodation_guests * quantity

					available = math.floor((total_space - guests_over_duration) / max_accomodation_guests)

					single_facility["quantity"] = str(available)
					single_facility["guests_over_duration"] = str(guests_over_duration)
					single_facility["total_space"] = str(total_space)
					single_facility["guest_total"] = guest_total

					percentage_pricing_status = single_facility["percentage_pricing_set"]

					if available > 0:
						if percentage_pricing_status:
							percentage_pricing = {}

							for single_percentage in single_facility["percentage_pricing"]:
								if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
									percentage_pricing = single_percentage

								else:
									pass

							if percentage_pricing:
								pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

								for one_price in single_facility["price"]:
									lower_limit_list = []
									json_lower_limit = one_price["lower_limit_list"]

									if json_lower_limit:
										for one_lower_element in json_lower_limit:
											return_lower_elements = {}
											for each_element in one_lower_element:
												if "price" in each_element:
													return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
												else:
													return_lower_elements[each_element] = one_lower_element[each_element]

											lower_limit_list.append(return_lower_elements)

										one_price["lower_limit_list"] = lower_limit_list

									else:
										pass

									middle_limit_list = []
									json_middle_limit = one_price["middle_middle_limit_list"]

									if json_middle_limit:
										for one_middle_element in json_middle_limit:
											return_middle_elements = {}
											for each_element in one_middle_element:
												if "price" in each_element:
													return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
												else:
													return_middle_elements[each_element] = one_middle_element[each_element]

											middle_limit_list.append(return_middle_elements)

										one_price["middle_middle_limit_list"] = middle_limit_list

									upper_limit_list = []
									json_upper_limit = one_price["upper_upper_limit_list"]

									if json_upper_limit:
										for one_upper_element in json_upper_limit:
											return_upper_elements = {}
											for each_element in one_upper_element:
												if "price" in each_element:
													return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
												else:
													return_upper_elements[each_element] = one_upper_element[each_element]

											upper_limit_list.append(return_upper_elements)

										one_price["upper_upper_limit_list"] = upper_limit_list

									try:
										mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
									except Exception:
										mandatory_pricing = ""

									try:
										minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
									except Exception:
										minimum_price = ""

									try:
										extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
									except Exception:
										extra_price = ""

									try:
										child_price = str(round(float(one_price["child_price"]) * pricing_factor))
									except Exception:
										child_price = ""

									try:
										adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
									except Exception:
										adult_price = ""

									one_price["mandatory_price"] = mandatory_pricing
									one_price["minimum_price"] = minimum_price
									one_price["extra_price"] = extra_price
									one_price["child_price"] = child_price
									one_price["adult_price"] = adult_price

							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

						else:
							if "/public" in request.path:
								if single_facility["thumbnail"]:
									available_facility.append(single_facility)
								else:
									pass

							else:
								available_facility.append(single_facility)

					else:
						pass

				else:
					percentage_pricing_status = single_facility["percentage_pricing_set"]

					max_accomodation_guests = int(single_facility["maximum_guests"])
					quantity = int(single_facility["quantity"])

					total_space = max_accomodation_guests * quantity

					single_facility["guests_over_duration"] = str(guests_over_duration)
					single_facility["space"] = str(total_space)

					if percentage_pricing_status:
						percentage_pricing = {}

						for single_percentage in single_facility["percentage_pricing"]:
							if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
								percentage_pricing = single_percentage

							else:
								pass

						if percentage_pricing:
							pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

							for one_price in single_facility["price"]:
								lower_limit_list = []
								json_lower_limit = one_price["lower_limit_list"]

								if json_lower_limit:
									for one_lower_element in json_lower_limit:
										return_lower_elements = {}
										for each_element in one_lower_element:
											if "price" in each_element:
												return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
											else:
												return_lower_elements[each_element] = one_lower_element[each_element]

										lower_limit_list.append(return_lower_elements)

									one_price["lower_limit_list"] = lower_limit_list

								else:
									pass

								middle_limit_list = []
								json_middle_limit = one_price["middle_middle_limit_list"]

								if json_middle_limit:
									for one_middle_element in json_middle_limit:
										return_middle_elements = {}
										for each_element in one_middle_element:
											if "price" in each_element:
												return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
											else:
												return_middle_elements[each_element] = one_middle_element[each_element]

										middle_limit_list.append(return_middle_elements)

									one_price["middle_middle_limit_list"] = middle_limit_list

								upper_limit_list = []
								json_upper_limit = one_price["upper_upper_limit_list"]

								if json_upper_limit:
									for one_upper_element in json_upper_limit:
										return_upper_elements = {}
										for each_element in one_upper_element:
											if "price" in each_element:
												return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
											else:
												return_upper_elements[each_element] = one_upper_element[each_element]

										upper_limit_list.append(return_upper_elements)

									one_price["upper_upper_limit_list"] = upper_limit_list

								try:
									mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
								except Exception:
									mandatory_pricing = ""

								try:
									minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
								except Exception:
									minimum_price = ""

								try:
									extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
								except Exception:
									extra_price = ""

								try:
									child_price = str(round(float(one_price["child_price"]) * pricing_factor))
								except Exception:
									child_price = ""

								try:
									adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
								except Exception:
									adult_price = ""

								one_price["mandatory_price"] = mandatory_pricing
								one_price["minimum_price"] = minimum_price
								one_price["extra_price"] = extra_price
								one_price["child_price"] = child_price
								one_price["adult_price"] = adult_price

						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

					else:
						if "/public" in request.path:
							if single_facility["thumbnail"]:
								available_facility.append(single_facility)
							else:
								pass

						else:
							available_facility.append(single_facility)

		else:
			if guests_over_duration > 0:
				pass
			else:
				single_facility["quantity"] = "1"

				percentage_pricing_status = single_facility["percentage_pricing_set"]

				if percentage_pricing_status:
					percentage_pricing = {}

					for single_percentage in single_facility["percentage_pricing"]:
						if (single_percentage["start_date_formatted"] <= request.json["check_in_date"]) & (single_percentage["end_date_formatted"] >= request.json["check_in_date"]):
							percentage_pricing = single_percentage

						else:
							pass

					if percentage_pricing:
						pricing_factor = float((100 + percentage_pricing["pricing_percentage"]) / 100)

						for one_price in single_facility["price"]:
							lower_limit_list = []
							json_lower_limit = one_price["lower_limit_list"]

							if json_lower_limit:
								for one_lower_element in json_lower_limit:
									return_lower_elements = {}
									for each_element in one_lower_element:
										if "price" in each_element:
											return_lower_elements[each_element] = str(round(float(one_lower_element[each_element]) * pricing_factor))
										else:
											return_lower_elements[each_element] = one_lower_element[each_element]

									lower_limit_list.append(return_lower_elements)

								one_price["lower_limit_list"] = lower_limit_list

							else:
								pass

							middle_limit_list = []
							json_middle_limit = one_price["middle_middle_limit_list"]

							if json_middle_limit:
								for one_middle_element in json_middle_limit:
									return_middle_elements = {}
									for each_element in one_middle_element:
										if "price" in each_element:
											return_middle_elements[each_element] = str(round(float(one_middle_element[each_element]) * pricing_factor))
										else:
											return_middle_elements[each_element] = one_middle_element[each_element]

									middle_limit_list.append(return_middle_elements)

								one_price["middle_middle_limit_list"] = middle_limit_list

							upper_limit_list = []
							json_upper_limit = one_price["upper_upper_limit_list"]

							if json_upper_limit:
								for one_upper_element in json_upper_limit:
									return_upper_elements = {}
									for each_element in one_upper_element:
										if "price" in each_element:
											return_upper_elements[each_element] = str(round(float(one_upper_element[each_element]) * pricing_factor))
										else:
											return_upper_elements[each_element] = one_upper_element[each_element]

									upper_limit_list.append(return_upper_elements)

								one_price["upper_upper_limit_list"] = upper_limit_list

							try:
								mandatory_pricing = str(round(float(one_price["mandatory_price"]) * pricing_factor))
							except Exception:
								mandatory_pricing = ""

							try:
								minimum_price = str(round(float(one_price["minimum_price"]) * pricing_factor))
							except Exception:
								minimum_price = ""

							try:
								extra_price = str(round(float(one_price["extra_price"]) * pricing_factor))
							except Exception:
								extra_price = ""

							try:
								child_price = str(round(float(one_price["child_price"]) * pricing_factor))
							except Exception:
								child_price = ""

							try:
								adult_price = str(round(float(one_price["adult_price"]) * pricing_factor))
							except Exception:
								adult_price = ""

							one_price["mandatory_price"] = mandatory_pricing
							one_price["minimum_price"] = minimum_price
							one_price["extra_price"] = extra_price
							one_price["child_price"] = child_price
							one_price["adult_price"] = adult_price

					if "/public" in request.path:
						if single_facility["thumbnail"]:
							available_campsite.append(single_facility)
						else:
							pass

					else:
						available_campsite.append(single_facility)

				else:
					if "/public" in request.path:
						if single_facility["thumbnail"]:
							available_campsite.append(single_facility)
						else:
							pass

					else:
						available_campsite.append(single_facility)

	return_facilities = sorted(available_facility, key=lambda order: order["name"])
	return_campsites = sorted(available_campsite, key=lambda order: order["name"])

	return jsonify({"facility_data": return_facilities, "camping_data": return_campsites}), 200



@app.route("/bookings/facility/unavailable/facility", methods = ["POST"])
def check_unavailable_facility_booking_facility():
	messages = []

	try:
		request.json["facility_id"].strip()
		if not request.json["facility_id"]:
			messages.append("Facility ID is empty.")
	except KeyError as e:
		messages.append("Facility ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	return_unavailable = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .all()

	output = []

	for single in return_unavailable:
		return_data = {}
		return_data["check_in_date"] = single.facility_booking_check_in_date
		return_data["check_out_date"] = single.facility_booking_check_out_date

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/facility/list", methods = ["POST"])
def list_facility_bookings():
	messages = []

	try:
		request.json["facility_id"].strip()
		if not request.json["facility_id"]:
			messages.append("Facility ID is empty.")
	except KeyError as e:
		messages.append("Facility ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	return_bookings = db.session.query(Facility)\
		   .join(Booking, Facility.booking_id == Booking.booking_public_id)\
		   .add_columns(Booking.booking_public_id, Booking.booking_done_by, Booking.booking_check_in_date,\
			   Booking.booking_check_out_date, Booking.booking_ref_code, Booking.booking_type,\
			   Booking.status, Booking.created_at, Booking.updated_at,\
			   Booking.session_id, Booking.deletion_marker, Booking.checked_in,\
			   Booking.checked_out,\
			   Facility.booking_id, Facility.facility_booking_adults, Facility.facility_booking_children,\
			   Facility.facility_booking_check_in_date, Facility.facility_booking_check_out_date)\
		   .filter(Facility.deletion_marker == None)\
		   .filter(Facility.facility_id == request.json["facility_id"])\
		   .filter(Facility.facility_booking_check_in_date <= request.json["date"])\
		   .filter(Facility.facility_booking_check_out_date >= request.json["date"])\
		   .filter(Facility.status != get_booking_status_id("Cancelled"))\
		   .filter(Facility.status != get_booking_status_id("Updated"))\
		   .all()

	requested_date = GenerateDateFromString.generateDate(request.json["date"])

	get_facility = requests.get(get_facility_details.format(request.json["facility_id"]))
	facility_details = get_facility.json()

	active_unavailability = {}

	if facility_details["data"][0]["unavailability_schedule_set"]:
		for each_schedule in facility_details["data"][0]["unavailability_schedule"]:
			if (each_schedule["start_date_formatted"] <= request.json["date"]) & (each_schedule["end_date_formatted"] >= request.json["date"]):
				active_unavailability = each_schedule
			else:
				pass

	# if active_unavailability:
	# 	message = []
	# 	message.append("The accommodation/campsite is not avaliable for the date " + requested_date.strftime("%d %b %Y") + " due to the following reason: " + active_unavailability["unavailability_reason"])
	# 	return jsonify({"message": message, "data": []}), 412

	if not return_bookings:
		data = []

		if active_unavailability:
			return_unavailability = {}
			return_unavailability["booking_public_id"] = "N/A"
			return_unavailability["booking_type"] = "N/A"
			return_unavailability["booking_type_id"] = "N/A"
			return_unavailability["booking_check_in_date"] = active_unavailability["unavailability_start_date"]
			return_unavailability["booking_check_out_date"] = active_unavailability["unavailability_end_date"]
			return_unavailability["booking_done_by"] = "Unavailability (" + active_unavailability["unavailability_reason"] + ")"
			return_unavailability["booking_ref_code"] = "N/A"
			return_unavailability["inventory_booking_guests"] = active_unavailability["unavailability_number"]
			return_unavailability["guests"] = []
			return_unavailability["guest_total"] = active_unavailability["unavailability_number"]
			return_unavailability["booking_status"] = "N/A"
			return_unavailability["booking_status_id"] = "N/A"
			return_unavailability["created_at"] = active_unavailability["created_at"]
			return_unavailability["updated_at"] = active_unavailability["created_at"]
			return_unavailability["session_user"] = "N/A"
			return_unavailability["session_id"] = "N/A"

			data.append(return_unavailability)
			return jsonify({"data": data}), 200

		else:
			message = []
			message.append("There are no accommodation/camping bookings for the selected date: " + requested_date.strftime("%d %b %Y"))
			return jsonify({"message": message, "data": []}), 412

	else:
		output = []

		id_array = []
		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
				   json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		if active_unavailability:
			return_unavailability = {}
			return_unavailability["booking_public_id"] = "N/A"
			return_unavailability["booking_type"] = "N/A"
			return_unavailability["booking_type_id"] = "N/A"
			return_unavailability["booking_check_in_date"] = active_unavailability["unavailability_start_date"]
			return_unavailability["booking_check_out_date"] = active_unavailability["unavailability_end_date"]
			return_unavailability["booking_done_by"] = "Unavailability (" + active_unavailability["unavailability_reason"] + ")"
			return_unavailability["booking_ref_code"] = "N/A"
			return_unavailability["inventory_booking_guests"] = active_unavailability["unavailability_number"]
			return_unavailability["guests"] = []
			return_unavailability["guest_total"] = active_unavailability["unavailability_number"]
			return_unavailability["booking_status"] = "N/A"
			return_unavailability["booking_status_id"] = "N/A"
			return_unavailability["created_at"] = active_unavailability["created_at"]
			return_unavailability["updated_at"] = active_unavailability["created_at"]
			return_unavailability["session_user"] = "N/A"
			return_unavailability["session_id"] = "N/A"

			output.append(return_unavailability)

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id

			booking_type = db.session.query(BookingType)\
				   .filter(BookingType.deletion_marker == None)\
				   .filter(BookingType.booking_type_public_id == single.booking_type)\
				   .first()

			return_data["booking_type"] = booking_type.booking_type_name

			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			return_data["facility_booking_guests"] = int(single.facility_booking_adults) + int(single.facility_booking_children)

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
					 .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
					 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
					 .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
					  GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
					  GatepassGuest.gatepass_no_of_nights)\
					 .filter(GatepassGuest.deletion_marker == None)\
					 .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
					 .filter(GatepassGuest.status != get_booking_status_id("Cancelled"))\
					 .filter(Gatepass.booking_id == single.booking_public_id)\
					 .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			# return_data["guest_total"] = sum(guest_sum)
			return_data["guest_total"] = int(single.facility_booking_adults) + int(single.facility_booking_children)

			check_to_invoice = db.session.query(Invoice)\
				   .filter(Invoice.deletion_marker == None)\
				   .filter(Invoice.booking_id == single.booking_id)\
				   .first()

			booking_status = db.session.query(BookingStatus)\
					 .filter(BookingStatus.deletion_marker == None)\
					 .filter(BookingStatus.booking_status_public_id == single.status)\
					 .first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if booking_status.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif booking_status.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif booking_status.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif booking_status.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif booking_status.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif booking_status.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif booking_status.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = booking_status.booking_status_name
			return_data["booking_status_id"] = single.status

			if single.facility_booking_check_in_date.strftime("%Y-%m-%d") == request.json["date"]:
				return_data["in_house_status"] = "Checking In"
				return_data["in_house_status_color"] = "#3498db"
			elif single.facility_booking_check_in_date.strftime("%Y-%m-%d") < request.json["date"]:
				return_data["in_house_status"] = "In-House"
				return_data["in_house_status_color"] = "#2ecc71"

			if single.facility_booking_check_out_date.strftime("%Y-%m-%d") == request.json["date"]:
				if single.checked_out == 1:
					return_data["in_house_status"] = "Checked Out"
					return_data["in_house_status_color"] = "#e74c3c"
				else:
					return_data["in_house_status"] = "Due Out"
					return_data["in_house_status_color"] = "#9b59b6"

			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			if single.session_id:
				if single.session_id == "guest":
					return_data["session_user"] = "Public"
					return_data["session_id"] = single.session_id
				else:
					try:
						for user in return_user.json()["data"]:
							if user["public_id"] == single.session_id:
								return_data["session_user"] = user["full_name"]
								return_data["session_id"] = single.session_id

					except (IndexError, KeyError) as user_error:
						return_data["session_user"] = "N/A"
						return_data["session_id"] = single.session_id

					except (AttributeError, UnboundLocalError) as network_related_errors:
						return_data["session_user"] = "Network Error"
						return_data["session_id"] = single.session_id
			else:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id

			output.append(return_data)

		return jsonify({"data": output}), 200


## TODO: Fix it, Felix
@app.route("/bookings/default", methods = ["POST"])
def test_function_on_listing():
	messages = []

	try:
		request.json["facility_id"].strip()
		if not request.json["facility_id"]:
			messages.append("Facility ID is empty.")
	except KeyError as e:
		messages.append("Facility ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	return_bookings = db.session.query(Facility)\
		   .join(Booking, Facility.booking_id == Booking.booking_public_id)\
		   .add_columns(Booking.booking_public_id, Booking.booking_done_by, Booking.booking_check_in_date,\
			   Booking.booking_check_out_date, Booking.booking_ref_code, Booking.booking_type,\
			   Booking.status, Booking.created_at, Booking.updated_at,\
			   Booking.session_id, Booking.deletion_marker, Booking.checked_in,\
			   Booking.checked_out,\
			   Facility.booking_id, Facility.facility_booking_adults, Facility.facility_booking_children,\
			   Facility.facility_booking_check_in_date, Facility.facility_booking_check_out_date)\
		   .filter(Facility.deletion_marker == None)\
		   .filter(Facility.facility_id == request.json["facility_id"])\
		   .filter(Facility.facility_booking_check_in_date <= request.json["date"])\
		   .filter(Facility.facility_booking_check_out_date >= request.json["date"])\
		   .filter(Facility.status != get_booking_status_id("Cancelled"))\
		   .filter(Facility.status != get_booking_status_id("Updated"))\
		   .all()

	requested_date = GenerateDateFromString.generateDate(request.json["date"])

	if not return_bookings:
		message = []
		message.append("There are no accommodation/camping bookings for the selected date: " + requested_date.strftime("%d %b %Y"))
		return jsonify({"message": message}), 412

	else:
		output = []

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id

			booking_type = db.session.query(BookingType)\
				   .filter(BookingType.deletion_marker == None)\
				   .filter(BookingType.booking_type_public_id == single.booking_type)\
				   .first()

			return_data["booking_type"] = booking_type.booking_type_name

			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			return_data["facility_booking_guests"] = int(single.facility_booking_adults) + int(single.facility_booking_children)

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
					 .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
					 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
					 .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
					  GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
					  GatepassGuest.gatepass_no_of_nights)\
					 .filter(GatepassGuest.deletion_marker == None)\
					 .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
					 .filter(GatepassGuest.status != get_booking_status_id("Cancelled"))\
					 .filter(Gatepass.booking_id == single.booking_public_id)\
					 .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			# return_data["guest_total"] = sum(guest_sum)
			return_data["guest_total"] = int(single.facility_booking_adults) + int(single.facility_booking_children)

			booking_status = db.session.query(BookingStatus)\
					 .filter(BookingStatus.deletion_marker == None)\
					 .filter(BookingStatus.booking_status_public_id == single.status)\
					 .first()

			if single.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: #73a533; color: #fff"
			elif single.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color: #2196f3; color: #fff"
			elif single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: #e53935; color: #fff"
			else:
				if booking_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: #e67e22; color: #fff"
				elif booking_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: #9b59b6; color: #fff"
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = booking_status.booking_status_name
			return_data["booking_status_id"] = single.status

			if single.facility_booking_check_in_date.strftime("%Y-%m-%d") == request.json["date"]:
				return_data["in_house_status"] = "Checking In"
				return_data["in_house_status_color"] = "#3498db"
			elif single.facility_booking_check_in_date.strftime("%Y-%m-%d") < request.json["date"]:
				return_data["in_house_status"] = "In-House"
				return_data["in_house_status_color"] = "#2ecc71"

			if single.facility_booking_check_out_date.strftime("%Y-%m-%d") == request.json["date"]:
				if single.checked_out == 1:
					return_data["in_house_status"] = "Checked Out"
					return_data["in_house_status_color"] = "#e74c3c"
				else:
					return_data["in_house_status"] = "Due Out"
					return_data["in_house_status_color"] = "#9b59b6"

			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			id_array = []
			for each_id in return_bookings:
				id_array.append(each_id.session_id)

			try:
				return_user = requests.post(get_user_from_aumra,\
					   json = {"users_ids": id_array})
			except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
				pass

			if single.session_id:
				try:
					for user in return_user.json()["data"]:
						if user["public_id"] == single.session_id:
							return_data["session_user"] = user["full_name"]
							return_data["session_id"] = single.session_id

				except (IndexError, KeyError) as user_error:
					return_data["session_user"] = "N/A"
					return_data["session_id"] = single.session_id

				except (AttributeError, UnboundLocalError) as network_related_errors:
					return_data["session_user"] = "Network Error"
					return_data["session_id"] = single.session_id
			else:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/bookings/facility/view")
def get_all_facility_bookings():
	if db.session.query(Facility)\
		.filter(Facility.deletion_marker == None)\
		.count() == 0:
		output = []
		output.append("There are no facility bookings in the system.")
		return jsonify({"message": output}), 422

	return_bookings = db.session.query(Facility)\
		   .filter(Facility.deletion_marker == None)\
		   .all()

	output = []

	for single in return_bookings:
		return_data = {}
		return_data["facility_booking_public_id"] = single.facility_booking_public_id
		return_data["booking_id"] = single.booking_id
		return_data["facility_id"] = single.facility_id
		return_data["facility_booking_check_in_date"] = single.facility_booking_check_in_date
		return_data["facility_booking_check_out_date"] = single.facility_booking_check_out_date
		return_data["facility_booking_guests"] = single.facility_booking_guests
		return_data["session_id"] = single.session_id

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/facility/search", methods = ["POST"])
def search_all_facility_bookings_by_date():
	messages = []

	try:
		request.json["facility_id"].strip()
		if not request.json["facility_id"]:
			messages.append("Facility ID is empty.")
	except KeyError as e:
		messages.append("Facility ID is missing.")

	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	if db.session.query(Facility)\
		.filter(Facility.deletion_marker == None)\
		.count() == 0:
		output = []
		output.append("There are no facility bookings in the system.")
		return jsonify({"message": output}), 422

	return_bookings = db.session.query(Facility)\
		   .filter(Facility.deletion_marker == None)\
		   .filter(Facility.facility_id == request.json["facility_id"])\
		   .all()

	output = []

	for single in return_bookings:
		return_data = {}
		return_data["facility_booking_public_id"] = single.facility_booking_public_id
		return_data["booking_id"] = single.booking_id
		return_data["facility_id"] = single.facility_id
		return_data["facility_booking_check_in_date"] = single.facility_booking_check_in_date
		return_data["facility_booking_check_out_date"] = single.facility_booking_check_out_date
		return_data["facility_booking_guests"] = single.facility_booking_adults + single.facility_booking_children
		return_data["session_id"] = single.session_id

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/facility/view/schedule", methods=["POST"])
def get_facility_schedule():
	messages = []

	try:
		request.json["facility_id"].strip()
		if not request.json["facility_id"]:
			messages.append("Facility ID is empty.")
	except KeyError as e:
		messages.append("Facility ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	if not request.json["facility_id"].strip():
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": output}), 422

	weekday = datetime.today().weekday()
	start_of_week = datetime.now() - timedelta(days=(weekday + 1))
	end_of_week = datetime.now() + timedelta(days=(5 - weekday))

	next_weekday = datetime.today().weekday()
	start_of_next_week = start_of_week + timedelta(days=7)
	end_of_next_week = end_of_week + timedelta(days=7)

	today = datetime.now()
	tomorrow = datetime.now() + timedelta(days=1)

	count_bookings_today = db.session.query(Facility)\
			 .filter(Facility.deletion_marker == None)\
			 .filter(Facility.facility_id == request.json["facility_id"])\
			 .filter(Facility.facility_booking_check_in_date == today.strftime("%Y-%m-%d"))\
			 .count()

	count_bookings_tomorrow = db.session.query(Facility)\
			 .filter(Facility.deletion_marker == None)\
			 .filter(Facility.facility_id == request.json["facility_id"])\
			 .filter(Facility.facility_booking_check_in_date == tomorrow.strftime("%Y-%m-%d"))\
			 .count()

	count_bookings_this_week = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .filter(Facility.facility_booking_check_in_date >= start_of_week.strftime("%Y-%m-%d"))\
			  .filter(Facility.facility_booking_check_in_date <= end_of_week.strftime("%Y-%m-%d"))\
			  .count()

	count_bookings_next_week = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .filter(Facility.facility_booking_check_in_date >= start_of_next_week.strftime("%Y-%m-%d"))\
			  .filter(Facility.facility_booking_check_in_date <= end_of_next_week.strftime("%Y-%m-%d"))\
			  .count()


	get_bookings_today = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .filter(Facility.facility_booking_check_in_date == today.strftime("%Y-%m-%d"))\
			  .all()

	today_array = []
	for single_today in get_bookings_today:
		return_bookings_today = {}
		return_bookings_today["facility_booking_public_id"] = single_today.facility_booking_public_id
		return_bookings_today["booking_id"] = single_today.booking_id
		return_bookings_today["facility_id"] = single_today.facility_id
		return_bookings_today["facility_code"] = single_today.facility_code
		return_bookings_today["facility_booking_check_in_date"] = single_today.facility_booking_check_in_date
		return_bookings_today["facility_booking_adults"] = single_today.facility_booking_adults
		return_bookings_today["facility_booking_children"] = single_today.facility_booking_children

		today_array.append(return_bookings_today)

	get_bookings_tomorrow = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .filter(Facility.facility_booking_check_in_date == tomorrow.strftime("%Y-%m-%d"))\
			  .all()

	tomorrow_array = []
	for single_tomorrow in get_bookings_tomorrow:
		return_bookings_tomorrow = {}
		return_bookings_tomorrow["facility_booking_public_id"] = single_tomorrow.facility_booking_public_id
		return_bookings_tomorrow["booking_id"] = single_tomorrow.booking_id
		return_bookings_tomorrow["facility_id"] = single_tomorrow.facility_id
		return_bookings_tomorrow["facility_code"] = single_tomorrow.facility_code
		return_bookings_tomorrow["facility_booking_check_in_date"] = single_tomorrow.facility_booking_check_in_date
		return_bookings_tomorrow["facility_booking_adults"] = single_tomorrow.facility_booking_adults
		return_bookings_tomorrow["facility_booking_children"] = single_tomorrow.facility_booking_children

		tomorrow_array.append(return_bookings_tomorrow)

	get_bookings_this_week = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .filter(Facility.facility_booking_check_in_date >= start_of_week.strftime("%Y-%m-%d"))\
			  .filter(Facility.facility_booking_check_in_date <= end_of_week.strftime("%Y-%m-%d"))\
			  .all()

	this_week_array = []
	for single_this_week in get_bookings_this_week:
		return_bookings_this_week = {}
		return_bookings_this_week["facility_booking_public_id"] = single_this_week.facility_booking_public_id
		return_bookings_this_week["booking_id"] = single_this_week.booking_id
		return_bookings_this_week["facility_id"] = single_this_week.facility_id
		return_bookings_this_week["facility_code"] = single_this_week.facility_code
		return_bookings_this_week["facility_booking_check_in_date"] = single_this_week.facility_booking_check_in_date
		return_bookings_this_week["facility_booking_adults"] = single_this_week.facility_booking_adults
		return_bookings_this_week["facility_booking_children"] = single_this_week.facility_booking_children

		this_week_array.append(return_bookings_this_week)

	get_bookings_next_week = db.session.query(Facility)\
			  .filter(Facility.deletion_marker == None)\
			  .filter(Facility.facility_id == request.json["facility_id"])\
			  .filter(Facility.facility_booking_check_in_date >= start_of_next_week.strftime("%Y-%m-%d"))\
			  .filter(Facility.facility_booking_check_in_date <= end_of_next_week.strftime("%Y-%m-%d"))\
			  .all()

	next_week_array = []
	for single_next_week in get_bookings_next_week:
		return_bookings_next_week = {}
		return_bookings_next_week["facility_booking_public_id"] = single_next_week.facility_booking_public_id
		return_bookings_next_week["booking_id"] = single_next_week.booking_id
		return_bookings_next_week["facility_id"] = single_next_week.facility_id
		return_bookings_next_week["facility_code"] = single_next_week.facility_code
		return_bookings_next_week["facility_booking_check_in_date"] = single_next_week.facility_booking_check_in_date
		return_bookings_next_week["facility_booking_adults"] = single_next_week.facility_booking_adults
		return_bookings_next_week["facility_booking_children"] = single_next_week.facility_booking_children

		next_week_array.append(return_bookings_next_week)

	data = []

	booking_schedule = {}
	booking_schedule["today_count"] = count_bookings_today
	booking_schedule["today_bookings"] = today_array

	booking_schedule["tomorrow_count"] = count_bookings_tomorrow
	booking_schedule["tomorrow_bookings"] = tomorrow_array

	booking_schedule["this_week_count"] = count_bookings_this_week
	booking_schedule["this_week_bookings"] = this_week_array

	booking_schedule["next_week_count"] = count_bookings_next_week
	booking_schedule["next_week_bookings"] = next_week_array

	booking_schedule["date_start_of_week"] = start_of_week.strftime("%y-%m-%d")
	booking_schedule["date_end_of_week"] = end_of_week.strftime("%y-%m-%d")
	booking_schedule["date_start_of_next_week"] = start_of_next_week.strftime("%y-%m-%d")
	booking_schedule["date_end_of_next_week"] = end_of_next_week.strftime("%y-%m-%d")

	data.append(booking_schedule)

	return jsonify({"data": data}), 200


@app.route("/bookings/facility/view/<facility_id>")
def get_single_facility_bookings(facility_id):
	if db.session.query(Facility)\
		.filter(Facility.deletion_marker == None)\
		.filter(Facility.facility_booking_public_id == facility_id)\
		.count() == 0:
		output = []
		output.append("That facility booking does not appear to exist in the system.")
		return jsonify({"message": output}), 422

	return_bookings = db.session.query(Facility)\
		   .filter(Facility.deletion_marker == None)\
		   .filter(Facility.facility_booking_public_id == facility_id)\
		   .all()

	output = []

	for single in return_bookings:
		return_data = {}
		return_data["booking_id"] = single.booking_id
		return_data["facility_id"] = single.facility_id
		return_data["facility_check_in_date"] = single.facility_booking_check_in_date
		return_data["facility_check_out_date"] = single.facility_booking_check_out_date
		return_data["facility_booking_guests"] = single.facility_booking_guests
		return_data["session_id"] = single.session_id
		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/facility/view/from_facility/<facility_id>")
def get_single_facility_booking_from_facility(facility_id):
	# unit_quantity = requests.get("http://" + app.config['SERVER'] + ":5001/facility/{}".format(facility_id))
	# returned_data = unit_quantity.content
	# returned_data = unit_quantity.content

	# json_try = json.loads(unit_quantity)
	# return json_try

	request = urllib.request.Request("http://" + app.config['SERVER'] + ":5001/facility/pricing/{}".format(facility_id))
	response = urllib.request.urlopen(request)
	encoding = response.info().get_content_charset('utf8')
	data = json.loads(response.read().decode(encoding))

	# return jsonify(float(json.dumps(data["data"][0]["citizen_child_price"][0])))
	return jsonify(json.dumps(data["data"][0]))


@app.route("/bookings/facility/edit", methods = ["PATCH"])
@app.route("/bookings/campsite/edit", methods = ["PATCH"])
def edit_single_facility_booking():
	messages = []

	try:
		if not request.json["facility_booking_id"]:
			messages.append("Facility booking ID is empty.")
	except KeyError as e:
		messages.append("Facility booking ID is missing.")

	try:
		if not request.json["facility_booking_check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		if not request.json["facility_booking_check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	# try:
	# 	if not request.json["facility_booking_adults"]:
	# 		messages.append("No. of adults is empty.")
	# except KeyError as e:
	# 	messages.append("No. of adults is missing.")

	# try:
	# 	if not request.json["facility_booking_children"]:
	# 		messages.append("No. of children is empty.")
	# except KeyError as e:
	# 	messages.append("No. of children is missing.")

	# try:
	# 	if not request.json["facility_booking_extra_adults"]:
	# 		messages.append("No. of extra adults is empty.")
	# except KeyError as e:
	# 	messages.append("No. of extra adults is missing.")

	# try:
	# 	if not request.json["facility_booking_extra_children"]:
	# 		messages.append("No. of extra children is empty.")
	# except KeyError as e:
	# 	messages.append("No. of extra children is missing.")

	try:
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	return_booking = db.session.query(Facility)\
			 .filter(Facility.deletion_marker == None)\
			 .filter(Facility.facility_booking_public_id == request.json["facility_booking_id"])\
			 .first()

	if not return_booking:
		message = []
		message.append("The accommodation/campsite reservation doesn't appear to exist.")
		return jsonify({"message": message}), 412

	if "/campsite" in request.path:
		return_booking.facility_booking_check_in_date = request.json["facility_booking_check_in_date"]
		return_booking.facility_booking_check_out_date = request.json["facility_booking_check_out_date"]
		return_booking.facility_booking_adults = request.json["facility_booking_adults"]
		return_booking.facility_booking_children = request.json["facility_booking_children"]
		return_booking.facility_booking_extra_adults = request.json["facility_booking_extra_adults"]
		return_booking.facility_booking_extra_children = request.json["facility_booking_extra_children"]
		return_booking.updated_at = datetime.now()

		booking_activity = BookingActivity(
		 booking_activity_public_id = str(uuid.uuid4()),
		 booking_id = return_booking.booking_id,
		 booking_activity_description = "Campsite reservation updated",
		 session_id = request.json["session_id"],
		 created_at = datetime.now()
		)

		db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The accommodation/campsite reservation has been updated.")
		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while updating the accommodation/campsite reservation. Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/bookings/facility/modify/date", methods = ["PATCH"])
def edit_facility_booking_dates():
	messages = []

	try:
		if not request.json["facility_booking_id"]:
			messages.append("Facility booking ID is empty.")
	except KeyError as e:
		messages.append("Facility booking ID is missing.")

	try:
		if not request.json["check_in_date"]:
			messages.append("Check in date is empty.")
	except KeyError as e:
		messages.append("Check in date is missing.")

	try:
		if not request.json["check_out_date"]:
			messages.append("Check out date is empty.")
	except KeyError as e:
		messages.append("Check out date is missing.")

	try:
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	return_booking = db.session.query(Facility)\
			 .filter(Facility.deletion_marker == None)\
			 .filter(Facility.facility_booking_public_id == request.json["facility_booking_id"])\
			 .filter(Facility.status != get_booking_status_id("Updated"))\
			 .filter(Facility.status != get_booking_status_id("Cancelled"))\
			 .first()

	if not return_booking:
		message = []
		message.append("The accommodation/campsite reservation doesn't appear to exist.")
		return jsonify({"message": message}), 412

	return_booking.facility_booking_check_in_date = request.json["check_in_date"]
	return_booking.facility_booking_check_out_date = request.json["check_out_date"]

	log_description = request.json["facility_name"] + " reservation date changed from (" + request.json["old_check_in_date"] + " - " + request.json["old_check_out_date"] + ") to \
						("            + request.json["check_in_date"] + " - " + request.json["check_out_date"] + ")"

	booking_activity = BookingActivity(
	 booking_activity_public_id = str(uuid.uuid4()),
	 booking_id = return_booking.booking_id,
	 booking_activity_description = log_description,
	 session_id = request.json["session_id"],
	 created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The accommodation/campsite reservation has been updated.")
		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while updating the accommodation/campsite reservation. Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/bookings/pelican/modify/catering", methods = ["PATCH"])
def edit_pelican_catering_type():
	messages = []

	try:
		if not request.json["facility_booking_id"]:
			messages.append("Facility booking ID is empty.")
	except KeyError as e:
		messages.append("Facility booking ID is missing.")

	try:
		if not request.json["catering_type"]:
			messages.append("Catering type is empty.")
	except KeyError as e:
		messages.append("Catering type is missing.")

	try:
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	return_booking = db.session.query(Facility)\
			 .filter(Facility.deletion_marker == None)\
			 .filter(Facility.facility_booking_public_id == request.json["facility_booking_id"])\
			 .filter(Facility.status != get_booking_status_id("Updated"))\
			 .filter(Facility.status != get_booking_status_id("Cancelled"))\
			 .first()

	if not return_booking:
		message = []
		message.append("The accommodation/campsite reservation doesn't appear to exist.")
		return jsonify({"message": message}), 412

	get_adult_total = return_booking.facility_booking_adults

	facility_data = requests.post(get_facility_pricing_by_date.format(return_booking.facility_id),
			 json = {"start_date": return_booking.facility_booking_check_in_date.strftime("%Y-%m-%d"), "end_date": return_booking.facility_booking_check_out_date.strftime("%Y-%m-%d")})

	if facility_data.json()["data"][0]["pricing_status"] == "false":
		message = []
		message.append("The pricing has not been set.")
		return jsonify({"message": message}), 412

	elif facility_data.json()["data"][0]["pricing_status"] == "true":
		if request.json["catering_type"] == "9a9a77eb":
			limits = None
			for each_group in facility_data.json()["data"][0]["price"]:
				if len(each_group["lower_limit_list"]) > 0:
					limits = each_group["lower_limit_list"]
				else:
					pass

			if not limits:
				message = []
				message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
				return jsonify({"message": message}), 412

		elif request.json["catering_type"] == "377788d4":
			limits = None
			for each_group in facility_data.json()["data"][0]["price"]:
				if len(each_group["middle_middle_limit_list"]) > 0:
					limits = each_group["middle_middle_limit_list"]
				else:
					pass

			if not limits:
				message = []
				message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
				return jsonify({"message": message}), 412

		elif request.json["catering_type"] == "266677c3":
			limits = None
			for each_group in facility_data.json()["data"][0]["price"]:
				if len(each_group["upper_upper_limit_list"]) > 0:
					limits = each_group["upper_upper_limit_list"]
				else:
					pass

			if not limits:
				message = []
				message.append("The pricing for the catering selected option has not been set. Please try another catering option.")
				return jsonify({"message": message}), 412

		else:
			message = []
			message.append("Please check the selected catering type and try again.")
			return jsonify({"message": message}), 422

		for cost_range in limits:
			if int(get_adult_total) >= int(cost_range["lower_limit"]):
				if int(get_adult_total) >= int(cost_range["lower_limit"]) & int(get_adult_total) <= int(cost_range["upper_limit"]):
					facility_currency_type_id = cost_range["currency"]

					currency_details = requests.get(get_currency.format(facility_currency_type_id))

					facility_currency_type = currency_details.json()["data"][0]["currency_name"]

					try:
						adult = float(cost_range["price"])
						child = float(cost_range["child_price"])
						fixed = 0
					except Exception:
						adult = 0
						child = 0
						fixed = 0

					try:
						special = round(float(cost_range["special_pricing"]))
						special_increase = round(float(cost_range["special_pricing_unit_increase"]))
					except Exception:
						special = None
						special_increase = None

			elif get_adult_total > int(cost_range["upper_limit"]):
				pass

		adult_rate = currencyHandler(return_booking.facility_cost_currency, facility_currency_type_id, adult)
		child_rate = currencyHandler(return_booking.facility_cost_currency, facility_currency_type_id, child)
		fixed_rate = currencyHandler(return_booking.facility_cost_currency, facility_currency_type_id, fixed)

		return_booking.facility_cost_per_adult = adult_rate
		return_booking.facility_cost_per_child = child_rate
		return_booking.facility_fixed_cost = fixed_rate
		return_booking.facility_catering_type = request.json["catering_type"]

		booking_id = return_booking.booking_id

		get_old = db.session.query(FacilityPricing)\
			 .filter(FacilityPricing.facility_pricing_type_public_id == request.json["old_catering_type"])\
			 .first()

		get_new = db.session.query(FacilityPricing)\
			 .filter(FacilityPricing.facility_pricing_type_public_id == request.json["catering_type"])\
			 .first()

		log_description = facility_data.json()["data"][0]["name"] + " catering type changed from " + get_old.facility_pricing_name + " to " + get_new.facility_pricing_name + ""

		booking_activity = BookingActivity(
		 booking_activity_public_id = str(uuid.uuid4()),
		 booking_id = booking_id,
		 booking_activity_description = log_description,
		 session_id = request.json["session_id"],
		 created_at = datetime.now()
		)

		db.session.add(booking_activity)

		try:
			db.session.commit()
			close(db)

			output = []
			output.append("The accommodation/campsite reservation has been updated.")
			return jsonify({"message": output}), 200
		except Exception as e:
			print(e)
			db.session.rollback()
			close(db)

			output = []
			output.append("There was an error while updating the accommodation/campsite reservation. Please try again later.")
			return jsonify({"message": output}), 422


@app.route("/bookings/facility/delete", methods = ["PATCH"])
def delete_single_facility_booking():
	messages = []

	try:
		request.json["facility_booking_id"].strip()
		if not request.json["facility_booking_id"]:
			messages.append("Facility booking ID is empty.")
	except KeyError as e:
		messages.append("Facility booking ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	return_booking = db.session.query(Facility)\
			 .filter(Facility.deletion_marker == None)\
			 .filter(Facility.status != get_booking_status_id("Updated"))\
			 .filter(Facility.facility_booking_public_id == request.json["facility_booking_id"])\
			 .first()

	if not return_booking:
		message = []
		message.append("The selected accommodation booking doesn't appear to exist.")
		return jsonify({"message": message}), 422

	return_booking.status = get_booking_status_id("Updated")
	# return_booking.session_id = request.json["session_id"]
	return_booking.updated_at = datetime.now()

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The accommodation booking has been cancelled.")
		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while deleting the accommodation booking. Please try again later.")
		return jsonify({"message": output}), 422
