from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import VehicleChargesSeed
from database.vehicle import Vehicle
from variables import *


def close(self):
	self.session.close()


#########################
#### Vehicle Charges ####
#########################
@app.route("/vehicle/charges/new", methods = ["POST"])
def add_new_vehicle_charge():
	messages = []
	
	try:
		request.json["income_account_code"].strip()
		if not request.json["income_account_code"]:
			messages.append("Income Account Code is empty.")
	except KeyError as e:
		messages.append("Income Account Code is missing.")

	try:
		request.json["department_analysis_code"].strip()
		if not request.json["department_analysis_code"]:
			messages.append("Department Analysis Code is empty.")
	except KeyError as e:
		messages.append("Department Analysis Code is missing.")

	try:
		request.json["outlet_analysis_code"].strip()
		if not request.json["outlet_analysis_code"]:
			messages.append("Outlet Analysis Code is empty.")
	except KeyError as e:
		messages.append("Outlet Analysis Code is missing.")
	
	try:
		request.json["category"].strip()
		if not request.json["category"]:
			messages.append("Category is empty.")
	except KeyError as e:
		messages.append("Category is missing.")

	try:
		str(request.json["cost"]).strip()
		if not request.json["cost"]:
			messages.append("Cost is empty.")
	except KeyError as e:
		messages.append("Cost is missing.")

	try:
		request.json["currency"].strip()
		if not request.json["currency"]:
			messages.append("Currency is empty.")
	except KeyError as e:
		messages.append("Currency is missing.")

	try:
		str(request.json["max_number"]).strip()
		if not str(request.json["max_number"]):
			messages.append("Max number is empty.")
	except KeyError as e:
		messages.append("Max number is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	check_category = db.session.query(Vehicle)\
							   .filter(Vehicle.deletion_marker == None)\
							   .all()

	for each_category in check_category:
		if each_category.vehicle_charge_category.lower() == request.json["category"].lower():
			message = []
			message.append("That category already exists in the system.")
			return jsonify({"message": message}), 422
	
	vehicle = Vehicle(
		vehicle_charge_public_id = str(uuid.uuid4()),
		vehicle_charge_income_code = request.json["income_account_code"],
		vehicle_charge_outlet_code = request.json["outlet_analysis_code"],
		vehicle_charge_dept_code = request.json["department_analysis_code"],
		vehicle_charge_category = request.json["category"].title(),
		vehicle_charge_category_cost = request.json["cost"],
		vehicle_charge_cost_currency = request.json["currency"],
		vehicle_charge_description = request.json["description"],
		maximum_number = request.json["max_number"],
		session_id = request.json["session_id"]
	)

	db.session.add(vehicle)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully added a vehicle.")
		return jsonify({"message": message}), 200

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("There was an error adding that vehicle category. Try again later.")
		return jsonify({"message": message, "error": str(e)}), 422


@app.route("/vehicle/charges/view")
def view_all_vehicles_charges():
	if db.session.query(Vehicle)\
                    .filter(Vehicle.deletion_marker == None)\
                    .filter(Vehicle.vehicle_charge_public_id == "99330ddf-4bd6-4c5e-8844-66b6d7ba1134")\
                    .count() == 0:
		VehicleChargesSeed.seed_default_vehicle_charges()

	get_all_vehicle_charges = db.session.query(Vehicle)\
										.filter(Vehicle.deletion_marker == None)\
										.order_by(Vehicle.vehicle_charge_id.asc())\
										.all()

	if not get_all_vehicle_charges:
		output = []
		output.append("There are no vehicle categories present in the system at the moment.")
		return jsonify({"message": output})

	else:
		output = []
		
		for single in get_all_vehicle_charges:
			return_data = {}
			return_data["vehicle_charge_public_id"] = single.vehicle_charge_public_id
			return_data["vehicle_charge_category"] = single.vehicle_charge_category
			return_data["vehicle_charge_category_cost"] = float(single.vehicle_charge_category_cost)

			try:
				currency = requests.get(get_currency.format(single.vehicle_charge_cost_currency))
				return_data["vehicle_charge_cost_currency"] = currency.json()["data"][0]["currency_name"]
				return_data["vehicle_charge_cost_currency_id"] = single.vehicle_charge_cost_currency
			except Exception:
				return_data["vehicle_charge_cost_currency"] = "N/A"
				return_data["vehicle_charge_cost_currency_id"] = single.vehicle_charge_cost_currency

			return_data["discount"] = 0
			return_data["discount_reason"] = None
			return_data["vehicles"] = 0
			return_data["max_number"] = single.maximum_number

			return_data["vehicle_charge_income_code"] = single.vehicle_charge_income_code
			return_data["vehicle_charge_outlet_code"] = single.vehicle_charge_outlet_code
			return_data["vehicle_charge_dept_code"] = single.vehicle_charge_dept_code
			return_data["vehicle_charge_description"] = single.vehicle_charge_description

			output.append(return_data)

		return jsonify({"data": output})


@app.route("/vehicle/charges/view/<charge_id>")
def view_single_vehicle_charge(charge_id):
	if db.session.query(Vehicle)\
                    .filter(Vehicle.deletion_marker == None)\
                    .filter(Vehicle.vehicle_charge_public_id == "99330ddf-4bd6-4c5e-8844-66b6d7ba1134")\
                    .count() == 0:
		VehicleChargesSeed.seed_default_vehicle_charges()

	get_all_vehicle_charges = db.session.query(Vehicle)\
										.filter(Vehicle.deletion_marker == None)\
										.filter(Vehicle.vehicle_charge_public_id == charge_id)\
										.all()

	if not get_all_vehicle_charges:
		output = []
		output.append("That no vehicle category is not present in the system.")
		return jsonify({"message": output})

	else:
		output = []
		
		for single in get_all_vehicle_charges:
			return_data = {}
			return_data["vehicle_charge_public_id"] = single.vehicle_charge_public_id
			return_data["vehicle_charge_category"] = single.vehicle_charge_category
			return_data["vehicle_charge_category_cost"] = float(single.vehicle_charge_category_cost)
			
			try:
				currency = requests.get(get_currency.format(single.vehicle_charge_cost_currency))
				return_data["vehicle_charge_cost_currency"] = currency.json()["data"][0]["currency_name"]
				return_data["vehicle_charge_cost_currency_id"] = single.vehicle_charge_cost_currency
			except Exception:
				return_data["vehicle_charge_cost_currency"] = "N/A"
				return_data["vehicle_charge_cost_currency_id"] = single.vehicle_charge_cost_currency

			return_data["discount"] = 0
			return_data["discount_reason"] = None
			return_data["vehicles"] = 0
			return_data["max_number"] = single.maximum_number

			return_data["vehicle_charge_income_code"] = single.vehicle_charge_income_code
			return_data["vehicle_charge_outlet_code"] = single.vehicle_charge_outlet_code
			return_data["vehicle_charge_dept_code"] = single.vehicle_charge_dept_code
			return_data["vehicle_charge_description"] = single.vehicle_charge_description

			output.append(return_data)

		return jsonify({"data": output})


@app.route("/vehicle/charges/edit", methods = ["PATCH"])
def edit_vehicle_charge():
	messages = []
	
	try:
		request.json["charge_id"].strip()
		if not request.json["charge_id"]:
			messages.append("Charge ID is empty.")
	except KeyError as e:
		messages.append("Charge ID is missing.")

	try:
		request.json["income_account_code"].strip()
		if not request.json["income_account_code"]:
			messages.append("Income Account Code is empty.")
	except KeyError as e:
		messages.append("Income Account Code is missing.")

	try:
		request.json["department_analysis_code"].strip()
		if not request.json["department_analysis_code"]:
			messages.append("Department Analysis Code is empty.")
	except KeyError as e:
		messages.append("Department Analysis Code is missing.")

	try:
		request.json["outlet_analysis_code"].strip()
		if not request.json["outlet_analysis_code"]:
			messages.append("Outlet Analysis Code is empty.")
	except KeyError as e:
		messages.append("Outlet Analysis Code is missing.")
	
	try:
		request.json["category"].strip()
		if not request.json["category"]:
			messages.append("Category is empty.")
	except KeyError as e:
		messages.append("Category is missing.")

	try:
		str(request.json["cost"]).strip()
		if not request.json["cost"]:
			messages.append("Cost is empty.")
	except KeyError as e:
		messages.append("Cost is missing.")

	try:
		request.json["currency"].strip()
		if not request.json["currency"]:
			messages.append("Currency is empty.")
	except KeyError as e:
		messages.append("Currency is missing.")

	try:
		str(request.json["max_number"]).strip()
		if not str(request.json["max_number"]):
			messages.append("Max number is empty.")
	except KeyError as e:
		messages.append("Max number is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422
	
	get_vehicle = db.session.query(Vehicle)\
							.filter(Vehicle.deletion_marker == None)\
							.filter(Vehicle.vehicle_charge_public_id == request.json["charge_id"])\
							.first()

	if not get_vehicle:
		message = []
		message.append("That vehicle category does not exist.")
		return jsonify({"message": message}), 412

	else:
		get_vehicle.vehicle_charge_income_code = request.json["income_account_code"]
		get_vehicle.vehicle_charge_outlet_code = request.json["outlet_analysis_code"]
		get_vehicle.vehicle_charge_dept_code = request.json["department_analysis_code"]
		get_vehicle.vehicle_charge_category = request.json["category"].title()
		get_vehicle.vehicle_charge_category_cost = request.json["cost"]
		get_vehicle.vehicle_charge_cost_currency = request.json["currency"]
		get_vehicle.vehicle_charge_description = request.json["description"]
		get_vehicle.maximum_number = request.json["max_number"]
		get_vehicle.session_id = request.json["session_id"]
		get_vehicle.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("Successfully changed a vehicle category's details.")
			return jsonify({"message": message}), 200

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("There was an error adding that vehicle category. Try again later.")
			return jsonify({"message": message, "error": str(e)}), 422


@app.route("/vehicle/charges/delete", methods = ["PATCH"])
def delete_single_vehicle_charge():
	messages = []
	
	try:
		request.json["charge_id"].strip()
		if not request.json["charge_id"]:
			messages.append("Charge ID is empty.")
	except KeyError as e:
		messages.append("Charge ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422
	
	get_charge = db.session.query(Vehicle)\
						   .filter(Vehicle.deletion_marker == None)\
						   .filter(Vehicle.vehicle_charge_public_id == request.json["charge_id"])\
						   .first()

	if not get_charge:
		message = []
		message.append("That vehicle category does not exist in the system.")
		return jsonify({"message": message}), 412

	else:
		get_charge.deletion_marker = 1
		get_charge.session_id = request.json["session_id"]

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("Deletion successful. Please proceed.")
			return jsonify({"message": message}), 200

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("There was an error deleting that vehicle category. Try again later.")
			return jsonify({"message": message, "error": str(e)}), 422