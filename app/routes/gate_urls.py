from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import GatesSeed
from database.gate import Gate
from variables import *

from functions.date_operators import *
from functions.currency_operators import *


def close(self):
	self.session.close()


###############
#### Gates ####
###############
@app.route("/gates/new", methods = ["POST"])
def add_new_gate():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	gate = Gate(
		gate_public_id = str(uuid.uuid4()),
		gate_name = request.json["name"],
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(gate)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("You have added a new gate to the system.")

		return jsonify({"message": message}), 201

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("The gate could not be added at this moment. Try later.")

		return jsonify({"message": message, "error": str(e)}), 422


@app.route("/gates/view")
def view_all_gates():
	get_all_gates = db.session.query(Gate)\
							  .filter(Gate.deletion_marker == None)\
							  .order_by(Gate.gate_name.asc())\
							  .all()

	if not get_all_gates:
		message = []
		message.append("There are no gates in the system.")

		return jsonify({"message": message}), 412

	else:
		data = []

		for single in get_all_gates:
			data.append(single.return_json())

		return jsonify({"data": data}), 200


@app.route("/gates/view/<gate_id>")
def view_single_gate(gate_id):
	if db.session.query(Gate)\
				.filter(Gate.deletion_marker==None)\
				.filter(Gate.gate_public_id == "9ac19eb6")\
				.count() == 0:
		GatesSeed.seed_default_gates()

	get_all_gates = db.session.query(Gate)\
							  .filter(Gate.deletion_marker == None)\
							  .filter(Gate.gate_public_id == gate_id)\
							  .all()

	if not get_all_gates:
		message = []
		message.append("That gate does not exist in the system.")

		return jsonify({"message": message}), 412

	else:
		data = []

		for single in get_all_gates:
			data.append(single.return_json())

		return jsonify({"data": data}), 200


@app.route("/gates/modify", methods = ["PATCH"])
def modify_gate():
	messages = []
	
	try:
		request.json["gate_id"].strip()
		if not request.json["gate_id"]:
			messages.append("Gate ID is empty.")
	except KeyError as e:
		messages.append("Gate ID is missing.")
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	get_one_gate = db.session.query(Gate)\
							 .filter(Gate.deletion_marker == None)\
							 .filter(Gate.gate_public_id == request.json["gate_id"])\
							 .first()

	if not get_one_gate:
		message = []
		message.append("That gate does not exist in the system.")

		return jsonify({"message": message}), 412

	else:
		get_one_gate.gate_name = request.json["name"]
		get_one_gate.session_id = request.json["session_id"]
		get_one_gate.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("You have modified the gate details.")

			return jsonify({"message": message}), 201

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("The gate details could not be modified at this moment. Try later.")

			return jsonify({"message": message, "error": str(e)}), 422


@app.route("/gates/delete", methods = ["PATCH"])
def delete_gate():
	messages = []
	
	try:
		request.json["gate_id"].strip()
		if not request.json["gate_id"]:
			messages.append("Gate ID is empty.")
	except KeyError as e:
		messages.append("Gate ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422
	
	get_one_gate = db.session.query(Gate)\
							 .filter(Gate.deletion_marker == None)\
							 .filter(Gate.gate_public_id == request.json["gate_id"])\
							 .first()

	if not get_one_gate:
		message = []
		message.append("That gate does not exist in the system.")

		return jsonify({"message": message}), 412

	else:
		get_one_gate.deletion_marker = 1
		get_one_gate.session_id = request.json["session_id"]
		get_one_gate.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("The gate details have been deleted from the system.")

			return jsonify({"message": message}), 201

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("The gate details could not be deleted at this moment. Try later.")

			return jsonify({"message": message, "error": str(e)}), 422