from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

import pymysql, os, math, requests, uuid, calendar


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.salesforce import SalesforceData
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.destination import Destination
from database.group import Group
from database.partner import Partner
from variables import *

from functions.date_operators import *
from functions.currency_operators import *


def close(self):
	self.session.close()



#####################
#### Week Graphs ####
#####################
@app.route("/bookings/graphs/day/all")
def view_graph_of_all_bookings_day():
	return ""


@app.route("/bookings/graphs/week/all")
def view_graph_of_all_bookings_week():
	week_array = []
	data = []

	number_of_weeks = 4

	today = datetime.now()

	weekday = datetime.today().weekday()
	start_of_this_week = datetime.now() - timedelta(days = (weekday + 1))
	end_of_this_week = datetime.now() + timedelta(days = (5 - weekday))
	this_week = start_of_this_week.strftime("%Y-%m-%d") + " to " + end_of_this_week.strftime("%Y-%m-%d")

	week_array.append(this_week)

	i = 0
	while i < number_of_weeks - 1:
		i += 1

		no_of_days = 7 * i

		start_of_und_week = start_of_this_week - timedelta(days = no_of_days)
		end_of_und_week = end_of_this_week - timedelta(days = no_of_days)
		und_week = start_of_und_week.strftime("%Y-%m-%d") + " to " + end_of_und_week.strftime("%Y-%m-%d")

		week_array.append(und_week)

	for each_week in reversed(week_array):
		weeks = {}
		
		start,end = each_week.split(" to ")

		start_of_week = GenerateDateFromString.generateDate(start)
		end_of_week = GenerateDateFromString.generateDate(end)

		weeks["week"] = start_of_week.strftime("%d-%m-%Y") + " to " + end_of_week.strftime("%d-%m-%Y")

		check_in_booking_info = []
		check_out_booking_info = []

		bookings_details = {}
		bookings_details["check_in_bookings_count"] = db.session.query(Booking)\
													   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
													   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
													   .filter(Booking.booking_check_in_date >= start_of_week)\
													   .filter(Booking.booking_check_in_date <= end_of_week)\
													   .filter(Booking.deletion_marker == None)\
													   .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_in_date >= start_of_week)\
								 .filter(Booking.booking_check_in_date <= end_of_week)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_in_booking_info.append(booking_data)

		bookings_details["check_in_bookings_data"] = check_in_booking_info

		bookings_details["check_out_bookings_count"] = db.session.query(Booking)\
													   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
													   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
													   .filter(Booking.booking_check_out_date >= start_of_week)\
													   .filter(Booking.booking_check_out_date <= end_of_week)\
													   .filter(Booking.deletion_marker == None)\
													   .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_out_date >= start_of_week)\
								 .filter(Booking.booking_check_out_date <= end_of_week)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_out_booking_info.append(booking_data)

		bookings_details["check_out_bookings_data"] = check_out_booking_info

		weeks["week_booking_details"] = bookings_details

		data.append(weeks)

	return jsonify({"data": data}), 200


@app.route("/bookings/graphs/week/group")
def view_graph_of_group_bookings_week():
	week_array = []
	data = []

	number_of_weeks = 4

	today = datetime.now()

	weekday = datetime.today().weekday()
	start_of_this_week = datetime.now() - timedelta(days = (weekday + 1))
	end_of_this_week = datetime.now() + timedelta(days = (5 - weekday))
	this_week = start_of_this_week.strftime("%Y-%m-%d") + " to " + end_of_this_week.strftime("%Y-%m-%d")

	week_array.append(this_week)

	i = 0
	while i < number_of_weeks - 1:
		i += 1

		no_of_days = 7 * i

		start_of_und_week = start_of_this_week - timedelta(days = no_of_days)
		end_of_und_week = end_of_this_week - timedelta(days = no_of_days)
		und_week = start_of_und_week.strftime("%Y-%m-%d") + " to " + end_of_und_week.strftime("%Y-%m-%d")

		week_array.append(und_week)

	for each_week in reversed(week_array):
		weeks = {}
		
		start,end = each_week.split(" to ")

		start_of_week = GenerateDateFromString.generateDate(start)
		end_of_week = GenerateDateFromString.generateDate(end)

		weeks["week"] = start_of_week.strftime("%d-%m-%Y") + " to " + end_of_week.strftime("%d-%m-%Y")

		check_in_booking_info = []
		check_out_booking_info = []

		bookings_details = {}
		bookings_details["check_in_bookings_count"] = db.session.query(Booking)\
													   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
													   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
													   .join(Group, Booking.booking_public_id == Group.booking_id)\
													   .filter(Booking.booking_check_in_date >= start_of_week)\
													   .filter(Booking.booking_check_in_date <= end_of_week)\
													   .filter(Booking.deletion_marker == None)\
													   .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Group, Booking.booking_public_id == Group.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_in_date >= start_of_week)\
								 .filter(Booking.booking_check_in_date <= end_of_week)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_in_booking_info.append(booking_data)

		bookings_details["check_in_bookings_data"] = check_in_booking_info

		bookings_details["check_out_bookings_count"] = db.session.query(Booking)\
													   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
													   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
													   .join(Group, Booking.booking_public_id == Group.booking_id)\
													   .filter(Booking.booking_check_out_date >= start_of_week)\
													   .filter(Booking.booking_check_out_date <= end_of_week)\
													   .filter(Booking.deletion_marker == None)\
													   .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Group, Booking.booking_public_id == Group.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_out_date >= start_of_week)\
								 .filter(Booking.booking_check_out_date <= end_of_week)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_out_booking_info.append(booking_data)

		bookings_details["check_out_bookings_data"] = check_out_booking_info

		weeks["week_booking_details"] = bookings_details

		data.append(weeks)

	return jsonify({"data": data}), 200


@app.route("/bookings/graphs/week/partner")
def view_graph_of_partner_bookings_week():
	week_array = []
	data = []

	number_of_weeks = 4

	today = datetime.now()

	weekday = datetime.today().weekday()
	start_of_this_week = datetime.now() - timedelta(days = (weekday + 1))
	end_of_this_week = datetime.now() + timedelta(days = (5 - weekday))
	this_week = start_of_this_week.strftime("%Y-%m-%d") + " to " + end_of_this_week.strftime("%Y-%m-%d")

	week_array.append(this_week)

	i = 0
	while i < number_of_weeks - 1:
		i += 1

		no_of_days = 7 * i

		start_of_und_week = start_of_this_week - timedelta(days = no_of_days)
		end_of_und_week = end_of_this_week - timedelta(days = no_of_days)
		und_week = start_of_und_week.strftime("%Y-%m-%d") + " to " + end_of_und_week.strftime("%Y-%m-%d")

		week_array.append(und_week)

	for each_week in reversed(week_array):
		weeks = {}
		
		start,end = each_week.split(" to ")

		start_of_week = GenerateDateFromString.generateDate(start)
		end_of_week = GenerateDateFromString.generateDate(end)

		weeks["week"] = start_of_week.strftime("%d-%m-%Y") + " to " + end_of_week.strftime("%d-%m-%Y")

		check_in_booking_info = []
		check_out_booking_info = []

		bookings_details = {}
		bookings_details["check_in_bookings_count"] = db.session.query(Booking)\
													   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
													   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
													   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
													   .filter(Booking.booking_check_in_date >= start_of_week)\
													   .filter(Booking.booking_check_in_date <= end_of_week)\
													   .filter(Booking.deletion_marker == None)\
													   .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_in_date >= start_of_week)\
								 .filter(Booking.booking_check_in_date <= end_of_week)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_in_booking_info.append(booking_data)

		bookings_details["check_in_bookings_data"] = check_in_booking_info

		bookings_details["check_out_bookings_count"] = db.session.query(Booking)\
													   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
													   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
													   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
													   .filter(Booking.booking_check_out_date >= start_of_week)\
													   .filter(Booking.booking_check_out_date <= end_of_week)\
													   .filter(Booking.deletion_marker == None)\
													   .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_out_date >= start_of_week)\
								 .filter(Booking.booking_check_out_date <= end_of_week)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_out_booking_info.append(booking_data)

		bookings_details["check_out_bookings_data"] = check_out_booking_info

		weeks["week_booking_details"] = bookings_details

		data.append(weeks)

	return jsonify({"data": data}), 200


######################
#### Month Graphs ####
######################
@app.route("/bookings/graphs/month/all")
def view_graph_of_all_bookings_month():
	data = []
	month_array = []

	number_of_months = 4
	i = 0

	while i < number_of_months:
		month = datetime.now() - relativedelta(months = i)

		month_array.append(month)

		i += 1

	for each_month in reversed(month_array):
		return_month = {}
		last_day = calendar.monthrange(int(each_month.strftime("%Y")), int(each_month.strftime("%m")))[1]

		return_month["month"] = each_month.strftime("%B")
		
		date_range = date(int(each_month.strftime("%Y")), int(each_month.strftime("%m")), 1).strftime("%Y-%m-%d")\
					 + " to " +\
					 date(int(each_month.strftime("%Y")), int(each_month.strftime("%m")), last_day).strftime("%Y-%m-%d")
		
		start,end = date_range.split(" to ")

		start_of_month = GenerateDateFromString.generateDate(start)
		end_of_month = GenerateDateFromString.generateDate(end)

		check_in_booking_info = []
		check_out_booking_info = []

		return_month["month_date_range"] = start_of_month.strftime("%d-%m-%Y") + " to " + end_of_month.strftime("%d-%m-%Y")

		month_booking_details = {}
		month_booking_details["check_in_bookings_count"] = db.session.query(Booking)\
																	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
																	 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
																	 .filter(Booking.booking_check_in_date >= start_of_month)\
																	 .filter(Booking.booking_check_in_date <= end_of_month)\
																	 .filter(Booking.deletion_marker == None)\
																	 .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_in_date >= start_of_month)\
								 .filter(Booking.booking_check_in_date <= end_of_month)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_in_booking_info.append(booking_data)

		month_booking_details["check_in_bookings_data"] = check_in_booking_info

		month_booking_details["check_out_bookings_count"] = db.session.query(Booking)\
																	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
																	 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
																	 .filter(Booking.booking_check_out_date >= start_of_month)\
																	 .filter(Booking.booking_check_out_date <= end_of_month)\
																	 .filter(Booking.deletion_marker == None)\
																	 .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_out_date >= start_of_month)\
								 .filter(Booking.booking_check_out_date <= end_of_month)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_out_booking_info.append(booking_data)

		month_booking_details["check_out_bookings_data"] = check_out_booking_info
		
		return_month["month_booking_details"] = month_booking_details
		
		data.append(return_month)
	
	return jsonify({"data": data}), 200


@app.route("/bookings/graphs/month/group")
def view_graph_of_group_bookings_month():
	data = []
	month_array = []

	number_of_months = 4
	i = 0

	while i < number_of_months:
		month = datetime.now() - relativedelta(months = i)

		month_array.append(month)

		i += 1

	for each_month in reversed(month_array):
		return_month = {}
		last_day = calendar.monthrange(int(each_month.strftime("%Y")), int(each_month.strftime("%m")))[1]

		return_month["month"] = each_month.strftime("%B")
		
		date_range = date(int(each_month.strftime("%Y")), int(each_month.strftime("%m")), 1).strftime("%Y-%m-%d")\
					 + " to " +\
					 date(int(each_month.strftime("%Y")), int(each_month.strftime("%m")), last_day).strftime("%Y-%m-%d")
		
		start,end = date_range.split(" to ")

		start_of_month = GenerateDateFromString.generateDate(start)
		end_of_month = GenerateDateFromString.generateDate(end)

		check_in_booking_info = []
		check_out_booking_info = []

		return_month["month_date_range"] = start_of_month.strftime("%d-%m-%Y") + " to " + end_of_month.strftime("%d-%m-%Y")

		month_booking_details = {}
		month_booking_details["check_in_bookings_count"] = db.session.query(Booking)\
																	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
																	 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
																	 .join(Group, Booking.booking_public_id == Group.booking_id)\
																	 .filter(Booking.booking_check_in_date >= start_of_month)\
																	 .filter(Booking.booking_check_in_date <= end_of_month)\
																	 .filter(Booking.deletion_marker == None)\
																	 .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Group, Booking.booking_public_id == Group.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_in_date >= start_of_month)\
								 .filter(Booking.booking_check_in_date <= end_of_month)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_in_booking_info.append(booking_data)

		month_booking_details["check_in_bookings_data"] = check_in_booking_info

		month_booking_details["check_out_bookings_count"] = db.session.query(Booking)\
																	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
																	 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
																	 .join(Group, Booking.booking_public_id == Group.booking_id)\
																	 .filter(Booking.booking_check_out_date >= start_of_month)\
																	 .filter(Booking.booking_check_out_date <= end_of_month)\
																	 .filter(Booking.deletion_marker == None)\
																	 .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Group, Booking.booking_public_id == Group.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_out_date >= start_of_month)\
								 .filter(Booking.booking_check_out_date <= end_of_month)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_out_booking_info.append(booking_data)

		month_booking_details["check_out_bookings_data"] = check_out_booking_info
		
		return_month["month_booking_details"] = month_booking_details
		
		data.append(return_month)
	
	return jsonify({"data": data}), 200


@app.route("/bookings/graphs/month/partner")
def view_graph_of_partner_bookings_month():
	data = []
	month_array = []

	number_of_months = 4
	i = 0

	while i < number_of_months:
		month = datetime.now() - relativedelta(months = i)

		month_array.append(month)

		i += 1

	for each_month in reversed(month_array):
		return_month = {}
		last_day = calendar.monthrange(int(each_month.strftime("%Y")), int(each_month.strftime("%m")))[1]

		return_month["month"] = each_month.strftime("%B")
		
		date_range = date(int(each_month.strftime("%Y")), int(each_month.strftime("%m")), 1).strftime("%Y-%m-%d")\
					 + " to " +\
					 date(int(each_month.strftime("%Y")), int(each_month.strftime("%m")), last_day).strftime("%Y-%m-%d")
		
		start,end = date_range.split(" to ")

		start_of_month = GenerateDateFromString.generateDate(start)
		end_of_month = GenerateDateFromString.generateDate(end)

		check_in_booking_info = []
		check_out_booking_info = []

		return_month["month_date_range"] = start_of_month.strftime("%d-%m-%Y") + " to " + end_of_month.strftime("%d-%m-%Y")

		month_booking_details = {}
		month_booking_details["check_in_bookings_count"] = db.session.query(Booking)\
																	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
																	 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
																	 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
																	 .filter(Booking.booking_check_in_date >= start_of_month)\
																	 .filter(Booking.booking_check_in_date <= end_of_month)\
																	 .filter(Booking.deletion_marker == None)\
																	 .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_in_date >= start_of_month)\
								 .filter(Booking.booking_check_in_date <= end_of_month)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_in_booking_info.append(booking_data)

		month_booking_details["check_in_bookings_data"] = check_in_booking_info

		month_booking_details["check_out_bookings_count"] = db.session.query(Booking)\
																	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
																	 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
																	 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
																	 .filter(Booking.booking_check_out_date >= start_of_month)\
																	 .filter(Booking.booking_check_out_date <= end_of_month)\
																	 .filter(Booking.deletion_marker == None)\
																	 .count()

		get_bookings = db.session.query(Booking)\
								 .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								 .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
								 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											  Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											  Booking.currency,\
											  BookingType.booking_type_name,\
											  BookingStatus.booking_status_name)\
								 .filter(Booking.booking_check_out_date >= start_of_month)\
								 .filter(Booking.booking_check_out_date <= end_of_month)\
								 .filter(Booking.deletion_marker == None)\
								 .all()

		for each_booking in get_bookings:
			booking_data = {}
			booking_data["booking_public_id"] = each_booking.booking_public_id
			booking_data["booking_type"] = each_booking.booking_type_name
			booking_data["booking_type_id"] = each_booking.booking_type
			booking_data["booking_check_in_date"] = each_booking.booking_check_in_date
			booking_data["booking_check_out_date"] = each_booking.booking_check_out_date
			booking_data["booking_done_by"] = each_booking.booking_done_by
			booking_data["booking_ref_code"] = each_booking.booking_ref_code

			check_out_booking_info.append(booking_data)

		month_booking_details["check_out_bookings_data"] = check_out_booking_info
		
		return_month["month_booking_details"] = month_booking_details
		
		data.append(return_month)
	
	return jsonify({"data": data}), 200