from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import uuid


# File imports
from routes import app, bookings_logger
from routes import db
from database.bookings_notes import Note
from variables import *

from functions.date_operators import *
from functions.currency_operators import *
from functions.booking_snippets import *
from functions.validation import *


def close(self):
	self.session.close()


@app.route("/note/new", methods = ["POST"])
def addNewNote():
	validation_list = [
		{"field": "booking_id", "alias": "Booking ID"},
		{"field": "note"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	try:
		if request.json["show_on_invoice"]:
			show_on_invoice = bool(request.json["show_on_invoice"])
		else:
			show_on_invoice = None
	except Exception:
		show_on_invoice = None

	note = Note(
		booking_notes_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		note = request.json["note"],
		show_on_invoice = show_on_invoice,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(note)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("New note saved.")
		return jsonify({"message": message}), 201

	except Exception:
		db.session.rollback()
		close(db)

		message = []
		message.append("There was an error saving the note. Try again later."), 422



@app.route("/note/delete", methods = ["PATCH"])
def deleteNote():
	validation_list = [
		{"field": "note_id", "alias": "Note ID"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	get_note = db.session.query(Note)\
						 .filter(Note.deletion_marker == None)\
						 .filter(Note.booking_notes_public_id == request.json["note_id"])\
						 .first()

	if not get_note:
		message = []
		message.append("The note either does not exist or has already been deleted.")
		return jsonify({"message": message}), 412

	else:
		get_note.deletion_marker = 1
		get_note.session_id = request.json["session_id"]
		get_note.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("Successfully deleted the note.")
			return jsonify({"message": message}), 200

		except Exception:
			db.session.rollback()
			close(db)

			message = []
			message.append("There was an error deleting the note. Try again later.")
			return jsonify({"message": message}), 422