from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_payments import BookingPayment
from database.payment_methods import PaymentMethod
from database.payment_gateways import PaymentGateway
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.invoice import Invoice
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.partner import Partner
from database.salesforce import SalesforceData
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.destination import Destination
from database.school import School
from database.school_booking import SchoolBooking
from variables import *

from functions.date_operators import *
from functions.currency_operators import *
from functions.booking_snippets import bookingTotal, get_details_partner


import pandas as panda

from collections import OrderedDict

def close(self):
	self.session.close()

# @app.route('/filtered/report', methods=['POST'])
# def filteredReport():
# 	booking_ids = request.json['booking_ids']

# 	bookings_array = []
# 	mpesa_array=[]
# 	bank_transfer_array = []
# 	credit_card_array = []

# 	# all_bookings = db.session.query(Booking)\
# 	# 							.join(Detail, Booking.booking_public_id == Detail.booking_id)\
# 	# 							.join(BookingPayment, Booking.booking_public_id == BookingPayment.booking_id)\
# 	# 							.add_columns(
# 	# 								Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date, Booking.ticket,\
# 	# 								Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
# 	# 								Booking.booking_ref_code, Booking.session_id,\
# 	# 								Booking.created_at, Booking.status, Booking.payment_status,\
# 	# 								Booking.currency,\
# 	# 								Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.city, Detail.country,\
# 	# 								BookingPayment.booking_amount, BookingPayment.amount_paid, BookingPayment.payment_method
# 	# 							)\
# 	# 							.filter(Booking.deletion_marker == None)\
# 	# 							.filter(Booking.booking_public_id.in_(booking_ids))\
# 	# 							.all()

# 	all_bookings = db.session.query(Booking)\
# 								.join(Detail, Booking.booking_public_id == Detail.booking_id)\
# 								.add_columns(
# 									Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date, Booking.ticket,\
# 									Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
# 									Booking.booking_ref_code, Booking.session_id,\
# 									Booking.created_at, Booking.status, Booking.payment_status,\
# 									Booking.currency,\
# 									Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.city, Detail.country
# 								)\
# 								.filter(Booking.deletion_marker == None)\
# 								.filter(Booking.booking_public_id.in_(booking_ids))\
# 								.all()

# 	if all_bookings:
# 		for booking in all_bookings:
# 			response_data = OrderedDict()
# 			response_data["Check_In_Date"] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
# 			response_data["Check_Out_Date"] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
# 			response_data["Booking_Ref"] = booking.booking_ref_code
# 			response_data["Ticket"] = booking.ticket if booking.ticket else "N/A"
# 			response_data['Client_Name'] = "{} {}".format(booking.first_name, booking.last_name)
# 			response_data["Email"] = booking.email_address
# 			response_data["Phone_Number"] = booking.phone_number
# 			# response_data["city"] = booking.city
# 			# response_data["country"] = booking.country
# 			# __payment_method, = db.session.query(PaymentMethod.payment_method_name).filter_by(payment_method_public_id=booking.payment_method).first()
# 			# response_data["payment_method"] = __payment_method
# 			# currency = requests.get(get_currency.format(booking.currency))
# 			# response_data["currency"] = currency.json()["data"][0]["currency_name"]
# 			# response_data["vat_tax_amount"] = float(float(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
# 			# response_data["invoice_amount"] = float(booking.booking_amount) if booking.booking_amount != None else 0
# 			# response_data["amount_paid"] = float(booking.amount_paid) if booking.amount_paid != None else 0
# 			# response_data['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
# 			bookings_array.append(response_data)

# 			# if __payment_method.lower() == "mpesa":
# 			# 	mpesa_data = OrderedDict()
# 			# 	mpesa_data['Check_In'] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
# 			# 	mpesa_data['Check_Out'] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
# 			# 	mpesa_data['booking_ref_code'] = booking.booking_ref_code
# 			# 	m_currency = requests.get(get_currency.format(booking.currency))
# 			# 	mpesa_data['currency'] = m_currency.json()["data"][0]["currency_name"]
# 			# 	mpesa_data['vat_tax_amount'] = float(float(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
# 			# 	mpesa_data['invoice_amount'] = float(booking.booking_amount) if booking.booking_amount != None else 0
# 			# 	mpesa_data['amount_paid'] = float(booking.amount_paid) if booking.amount_paid != None else 0
# 			# 	mpesa_data['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
# 			# 	mpesa_array.append(mpesa_data)

# 			# elif __payment_method.lower() == "credit card":
# 			# 	credit_card = OrderedDict()
# 			# 	credit_card['Check_In'] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
# 			# 	credit_card['Check_Out'] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
# 			# 	credit_card['booking_ref_code'] = booking.booking_ref_code
# 			# 	c_currency = requests.get(get_currency.format(booking.currency))
# 			# 	credit_card['currency'] = c_currency.json()["data"][0]["currency_name"]
# 			# 	credit_card['vat_tax_amount'] = int(int(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
# 			# 	credit_card['invoice_amount'] = float(booking.booking_amount) if booking.booking_amount != None else 0
# 			# 	credit_card['amount_paid'] = float(booking.amount_paid) if booking.amount_paid != None else 0
# 			# 	credit_card['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
# 			# 	credit_card_array.append(credit_card)
# 			# else:
# 			# 	bank_transfer = OrderedDict()
# 			# 	bank_transfer['Check_In'] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
# 			# 	bank_transfer['Check_Out'] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
# 			# 	bank_transfer['booking_ref_code'] = booking.booking_ref_code
# 			# 	b_currency = requests.get(get_currency.format(booking.currency))
# 			# 	bank_transfer['currency'] = b_currency.json()["data"][0]["currency_name"]
# 			# 	bank_transfer['vat_tax_amount'] = int(int(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
# 			# 	bank_transfer['invoice_amount'] = float(booking.booking_amount) if booking.booking_amount != None else 0
# 			# 	bank_transfer['amount_paid'] = float(booking.amount_paid) if booking.amount_paid != None else 0
# 			# 	bank_transfer['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
# 			# 	bank_transfer_array.append(bank_transfer)

# 		writer = panda.ExcelWriter("reports/{}OPCFilteredReport.xlsx".format(datetime.now().strftime("%Y%m%d")))

# 		bookings_data_df = panda.DataFrame(bookings_array)
# 		bookings_data_df.to_excel(writer, "Bookings Data", index=False)

# 		# mpesa_data_df = panda.DataFrame(mpesa_array)
# 		# mpesa_data_df.to_excel(writer, "Mpesa Summary", index=False)

# 		# credit_card_df = panda.DataFrame(credit_card_array)
# 		# credit_card_df.to_excel(writer, "Credit Card Summary", index=False)

# 		# bank_transfer_df = panda.DataFrame(bank_transfer_array)
# 		# bank_transfer_df.to_excel(writer, "Bank Transfer Summary", index=False)

# 		writer.save()
# 		file_location_name = "https://{}/docs/reports/{}OPCFilteredReport.xlsx".format(app.config['SERVER'], datetime.now().strftime("%Y%m%d"))

# 		responseObject = {
# 			'url' : file_location_name
# 		}

# 		return make_response(jsonify(responseObject)), 200

# 	else:
# 		responseObject = {
# 			'message' : 'No data found'
# 		}
# 		return make_response(jsonify(responseObject)), 422


@app.route('/filtered/report', methods=['POST'])
def filteredReport():
	booking_ids = request.json['booking_ids']

	bookings_array = []
	mpesa_array=[]
	bank_transfer_array = []
	credit_card_array = []

	all_bookings = db.session.query(Booking)\
	 .filter(Booking.booking_public_id.in_(booking_ids))\
	 .order_by(Booking.booking_check_in_date.asc())\
	 .all()

	if all_bookings:
		for booking in all_bookings:
			response_data = OrderedDict()

			booking_data = {}
			bookingTotal(booking_data, booking.booking_public_id, True)

			get_booking_details = db.session.query(Detail)\
			  .filter(Detail.booking_id == booking.booking_public_id)\
			  .first()

			response_data["Created_At"] = (booking.created_at).strftime("%Y-%m-%d")
			response_data["Check_In_Date"] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
			response_data["Check_Out_Date"] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
			response_data["Booking_Ref"] = booking.booking_ref_code
			response_data["Customer_Code"] = booking_data["customer_code"]
			response_data["Booking_Type"] = booking.return_booking_type()
			response_data["Ticket"] = booking.ticket if booking.ticket else None

			if booking.return_booking_type() == "School Booking":
				check_school_booking = db.session.query(SchoolBooking)\
				   .join(School, SchoolBooking.school_id == School.school_public_id)\
				   .add_columns(School.school_name)\
				   .filter(SchoolBooking.booking_id == booking.booking_public_id)\
				   .first()

				if check_school_booking:
					response_data['Client_Name'] = check_school_booking.school_name
				else:
					response_data['Client_Name'] = booking.booking_done_by

			else:
				response_data['Client_Name'] = booking.booking_done_by

			try:
				response_data["Email"] = get_booking_details.email_address
			except Exception:
				response_data["Email"] = None

			try:
				response_data["Phone_Number"] = get_booking_details.phone_number
			except Exception:
				response_data["Phone_Number"] = None

			response_data["Conservancy_Entry_Fees"] = booking_data["guest_total"]
			response_data["Vehicle_Fees"] = booking_data["vehicle_total"]

			entry_pax = ""

			for single_person in booking_data["guests"]:
				if single_person["payment_guests"] > 0:
					entry_pax = entry_pax + single_person["payment_person"] + " (V), "

			response_data["Entry_Pax"] = entry_pax

			vehicles = ""

			for single_vehicle in booking_data["vehicles"]:
				if single_vehicle["vehicles"] > 0:
					vehicles = vehicles + single_vehicle["vehicle_charge_category"] + " (V), "

			response_data["vehicles"] = vehicles

			inventory_items = ""

			for single_inventory in booking_data["inventory_bookings"]:
				if single_inventory["vat"]:
					vat = "V"
				else:
					vat = ""

				inventory_items = inventory_items + single_inventory["inventory_name"] + " (" + vat + "), "

			response_data["Activities"] = inventory_items

			facilities = ""

			for single_facility in booking_data["facility_bookings"]:
				if single_facility["vat"]:
					vat = "V"
				else:
					vat = ""

				if single_facility["catering_levy"]:
					cater = "C"
				else:
					cater = ""

				facilities = facilities + single_facility["facility_name"] + " (" + vat + "," + cater + "), "

			response_data["Accommodation/Camping"] = facilities

			check_to_invoice = db.session.query(Invoice)\
			   .filter(Invoice.deletion_marker == None)\
			   .filter(Invoice.booking_id == booking.booking_public_id)\
			   .first()

			if check_to_invoice:
				if booking.deletion_marker == 1:
					response_data["Status"] = "Cancelled"
				else:
					response_data["Status"] = "To Invoice"

			else:
				if booking.checked_out == 1:
					response_data["Status"] = "Checked Out"
				elif booking.checked_in == 1:
					response_data["Status"] = "Checked In"
				else:
					response_data["Status"] = booking.b_status.booking_status_name

			if booking.deletion_marker == 1:
				response_data["Payment_Status"] = "Cancelled"
			else:
				if booking.payment_status == 1:
					response_data["Payment_Status"] = "Paid"
				if booking.payment_status == 2:
					response_data["Payment_Status"] = "Incomplete Payment"
				if booking.payment_status == 3:
					response_data["Payment_Status"] = "Complimentary"
				if booking.payment_status == 4:
					response_data["Payment_Status"] = "To Invoice"
				elif not booking.payment_status:
					response_data["Payment_Status"] = "Not Paid"

			payment_methods = ""

			get_transactions = db.session.query(Transaction)\
			  .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
			  .add_columns(Transaction.transaction_payment_gateway,\
			  PaymentMethod.payment_method_name)\
			  .filter(Transaction.booking_id == booking.booking_public_id)\
			  .filter(Transaction.deletion_marker == None)\
			  .all()

			for single_transaction in get_transactions:
				if single_transaction.transaction_payment_gateway:
					get_gateway = db.session.query(PaymentGateway)\
					   .filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
					   .first()

					transaction_method = get_gateway.payment_gateway_name

				else:
					transaction_method = single_transaction.payment_method_name

				payment_methods = payment_methods + transaction_method + ","

			response_data["Payment_Methods"] = payment_methods

			response_data["Currency"] = booking_data["booking_currency_name"]
			response_data["Booking_Total"] = booking_data["total_cost"]
			response_data["Booking_VAT"] = booking_data["tax_breakdown"]["vat"]
			response_data["Booking_Catering_Levy"] = booking_data["tax_breakdown"]["catering"]
			response_data["Booking_Net"] = booking_data["tax_breakdown"]["after_tax"]

			bookings_array.append(response_data)

		filename = "reports/{}OPCFilteredReport.xlsx".format(datetime.now().strftime("%d-%m-%Y_%H:%M:%S"))

		writer = panda.ExcelWriter(filename)

		bookings_data_df = panda.DataFrame(bookings_array)
		bookings_data_df.to_excel(writer, "Bookings Data", index=False)

		writer.save()
		file_location_name = "https://{}/docs/{}".format(app.config['SERVER'], filename)

		responseObject = {
		 'url' : file_location_name
		}

		return make_response(jsonify(responseObject)), 200

	else:
		responseObject = {
		 'message' : 'No data found'
		}
		return make_response(jsonify(responseObject)), 422


@app.route('/filtered/report/partner', methods=['POST'])
def partnerFilteredReport():
	booking_ids = request.json['booking_ids']
	partner_id = request.json["partner_id"]

	bookings_array = []
	mpesa_array=[]
	bank_transfer_array = []
	credit_card_array = []

	partner_details = get_details_partner(partner_id)
	partner_name = partner_details["name"]

	all_bookings = db.session.query(Booking)\
	 .filter(Booking.booking_public_id.in_(booking_ids))\
	 .order_by(Booking.booking_check_in_date.asc())\
	 .all()

	if all_bookings:
		for booking in all_bookings:
			response_data = OrderedDict()

			booking_data = {}
			bookingTotal(booking_data, booking.booking_public_id, True)

			get_booking_details = db.session.query(Detail)\
			  .filter(Detail.booking_id == booking.booking_public_id)\
			  .first()

			get_partner_booking_details = db.session.query(Partner)\
			 .filter(Partner.booking_id == booking.booking_public_id)\
			 .first()

			if get_partner_booking_details:
				if get_partner_booking_details.partner_booking_ref:
					partner_booking_ref = get_partner_booking_details.partner_booking_ref
				else:
					partner_booking_ref = "N/A"

			else:
				partner_booking_ref = "N/A"

			response_data["Check_In_Date"] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
			response_data["Check_Out_Date"] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
			response_data["Booking_Ref"] = booking.booking_ref_code
			response_data["In-House_Ref"] = partner_booking_ref
			response_data['Client_Name'] = booking.booking_done_by
			# response_data["Email"] = get_booking_details.email_address
			x = hasattr(get_booking_details, 'email_address')

			if x:
				response_data["Email"] = get_booking_details.email_address
			else:
				pass

			y = hasattr(get_booking_details, 'phone_number')
			if y:
				response_data["Email"] = get_booking_details.phone_number
			else:
				pass
			# response_data["Phone_Number"] = get_booking_details.phone_number

			response_data["Conservancy_Entry_Fees"] = booking_data["guest_total"]
			response_data["Vehicle_Fees"] = booking_data["vehicle_total"]

			inventory_items = ""

			for single_inventory in booking_data["inventory_bookings"]:
				inventory_items = inventory_items + single_inventory["inventory_name"] + ", "

			response_data["Activities"] = inventory_items

			facilities = ""

			for single_facility in booking_data["facility_bookings"]:
				facilities = facilities + single_facility["facility_name"] + ", "

			response_data["Accommodation/Camping"] = facilities

			# check_to_invoice = db.session.query(Invoice)\
			# 						 	 .filter(Invoice.deletion_marker == None)\
			# 						 	 .filter(Invoice.booking_id == booking.booking_public_id)\
			# 						 	 .first()

			# if check_to_invoice:
			# 	if booking.deletion_marker == 1:
			# 		response_data["Status"] = "Cancelled"
			# 	else:
			# 		response_data["Status"] = "To Invoice"

			# else:
			# 	if booking.checked_out == 1:
			# 		response_data["Status"] = "Checked Out"
			# 	elif booking.checked_in == 1:
			# 		response_data["Status"] = "Checked In"
			# 	else:
			# 		response_data["Status"] = booking.b_status.booking_status_name

			if booking.payment_status == 1:
				response_data["Payment_Status"] = "Paid"
			if booking.payment_status == 2:
				response_data["Payment_Status"] = "Incomplete Payment"
			if booking.payment_status == 3:
				response_data["Payment_Status"] = "Complimentary"
			if booking.payment_status == 4:
				response_data["Payment_Status"] = "To Invoice"
			if not booking.payment_status:
				response_data["Payment_Status"] = "Not Paid"

			response_data["Currency"] = booking_data["booking_currency_name"]
			response_data["Booking_Total"] = booking_data["total_cost"]

			bookings_array.append(response_data)

		filename = "reports/{}_Report_{}.xlsx".format(partner_name, datetime.now().strftime("%d-%m-%Y_%H:%M:%S"))

		writer = panda.ExcelWriter(filename)

		bookings_data_df = panda.DataFrame(bookings_array)
		bookings_data_df.to_excel(writer, "Bookings Data", index=False)

		writer.save()
		file_location_name = "https://{}/docs/{}".format(app.config['SERVER'], filename)

		responseObject = {
		 'url' : file_location_name
		}

		return make_response(jsonify(responseObject)), 200

	else:
		responseObject = {
		 'message' : 'No data found'
		}
		return make_response(jsonify(responseObject)), 422
