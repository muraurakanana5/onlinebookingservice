from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.inventory import Inventory
from variables import *


def close(self):
	self.session.close()


##################
#### Calendar ####
##################
# @app.route("/calendar/view", methods = ["POST"])
# def calendar_view():
# 	if not request.json["start_date"].strip()\
# 	or not request.json["end_date"].strip():
# 		output = []
# 		output.append("You appear to be missing some data. Please try again.")
# 		return jsonify({"message": output}), 422

# 	output = []
# 	facility_output = []
# 	inventory_output = []

# 	stripped_start_year,stripped_start_month,stripped_start_day = request.json["start_date"].split("-")
# 	stripped_end_year,stripped_end_month,stripped_end_day = request.json["end_date"].split("-")

# 	int_start_year = int(stripped_start_year)
# 	int_start_month = int(stripped_start_month)
# 	int_start_day = int(stripped_start_day)
# 	int_end_year = int(stripped_end_year)
# 	int_end_month = int(stripped_end_month)
# 	int_end_day = int(stripped_end_day)

# 	starting = date(int_start_year,int_start_month,int_start_day)
# 	ending = date(int_end_year,int_end_month,int_end_day)
# 	add_day = timedelta(days=1)

# 	while starting <= ending:
# 		return_facility_listings = {}
# 		return_facility_listings["date"] = starting
		
# 		return_inventory_bookings = db.session.query(Inventory)\
# 											  .filter(Inventory.deletion_marker == None)\
# 											  .filter(Inventory.inventory_booking_date == starting)\
# 											  .all()

# 		for single_inventory in return_inventory_bookings:
# 			if single_inventory.inventory_booking_date == starting:
# 				return_inventory_booking = {}
# 				return_inventory_booking["inventory_booking_public_id"] = single_inventory.inventory_booking_public_id
# 				return_inventory_booking["booking_id"] = single_inventory.booking_id
# 				return_inventory_booking["inventory_id"] = single_inventory.inventory_id
# 				return_inventory_booking["inventory_booking_date"] = single_inventory.inventory_booking_date
# 				return_inventory_booking["inventory_booking_guests"] = single_inventory.inventory_booking_guests
# 				return_inventory_booking["session_id"] = single_inventory.session_id
# 				inventory_output.append(return_inventory_booking)

# 			return_facility_listings["inventory_bookings"] = inventory_output

# 		output.append(return_facility_listings)

# 		starting = starting + add_day
	
# 	return jsonify({"inventory": output, "facility": []})

	# return_inventory = requests.get(get_active_inventory)
	# inventory_json = return_inventory.json()["data"]

	# return jsonify({"date": inventory_json, "count": len(inventory_json)})


@app.route("/calendar/old/view", methods = ["POST"])
def old_calendar_view():
	if not request.json["start_date"].strip()\
	or not request.json["end_date"].strip():
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": output}), 422

	output = []
	facility_output = []
	inventory_output = []
	dates = []

	while_loop_array = []
	date_array = []

	stripped_start_year,stripped_start_month,stripped_start_day = request.json["start_date"].split("-")
	stripped_end_year,stripped_end_month,stripped_end_day = request.json["end_date"].split("-")

	int_start_year = int(stripped_start_year)
	int_start_month = int(stripped_start_month)
	int_start_day = int(stripped_start_day)
	int_end_year = int(stripped_end_year)
	int_end_month = int(stripped_end_month)
	int_end_day = int(stripped_end_day)

	starting = date(int_start_year,int_start_month,int_start_day)
	ending = date(int_end_year,int_end_month,int_end_day)
	add_day = timedelta(days=1)

	return_inventory_bookings = db.session.query(Inventory.inventory_id)\
								.filter(Inventory.deletion_marker == None)\
								.group_by(Inventory.inventory_id)\
								.filter(Inventory.inventory_booking_date >= request.json["start_date"])\
								.filter(Inventory.inventory_booking_date <= request.json["end_date"])\
								.all()

	# while starting <= ending:
	# 	date_array.append(starting)

	# 	starting = starting + add_day

	for single in return_inventory_bookings:
		return_bookings_by_institution = db.session.query(Inventory.inventory_id)\
												   .filter(Inventory.deletion_marker == None)\
												   .filter(Inventory.inventory_id == single.inventory_id)\
												   .first()
		
		return_data = {}
		return_data["inventory_id"] = single.inventory_id
		return_data["bookings_count"] = db.session.query(Inventory.inventory_id)\
												  .filter(Inventory.deletion_marker == None)\
												  .filter(Inventory.inventory_id == single.inventory_id)\
												  .filter(Inventory.inventory_booking_date >= request.json["start_date"])\
												  .filter(Inventory.inventory_booking_date <= request.json["end_date"])\
												  .count()

		while starting <= ending:
			date_array.append(starting)

			starting = starting + add_day
		
		# while starting <= ending:
		# 	while_loop = {}
		# 	while_loop["date"] = starting
		# 	while_loop["inventory_id"] = single.inventory_id
		# 	print("Date : " + starting.strftime("%Y-%m-%d"))
			# date_array.append(starting)

			# starting = starting + add_day
		
		for one_date in date_array:
			while_loop = {}
			while_loop["date"] = one_date
			while_loop["bookings_count"] = db.session.query(Inventory.inventory_id)\
												  .filter(Inventory.deletion_marker == None)\
												  .filter(Inventory.inventory_id == single.inventory_id)\
												  .filter(Inventory.inventory_booking_date == one_date)\
												  .count()
			while_loop["inventory_id"] = single.inventory_id

			# starting = starting + add_day

			while_loop_array.append(while_loop)

		return_data["while_loop_info"] = while_loop_array

		output.append(return_data)

		# return_bookings = db.session.query(Inventory.inventory_id)\
		# 							.filter(Inventory.deletion_marker == None)\
		# 							.filter(Inventory.inventory_id == single.inventory_id)\
		# 							.filter(Inventory.inventory_booking_date >= request.json["start_date"])\
		# 							.filter(Inventory.inventory_booking_date <= request.json["end_date"])\
		# 							.all()
		
		# returned_bookings = db.session.query(Inventory)\
		# 							  .filter(Inventory.inventory_id == single[0])\
		# 							  .filter(Inventory.deletion_marker == None)\
		# 							  .all()
		
		# for single in returned_bookings:
		# 	return_inventory_booking = {}
		# 	return_inventory_booking["info"] = single.inventory_booking_public_id
		# 	inventory_output.append(return_inventory_booking)
		
	# 	while starting <= ending:
	# 		dates.append(starting)
	# 		starting = starting + add_day

	# 	return_data["dates"] = dates

	# 	for one_date in dates:
	# 		inventory_dict = {}
	# 		# return_bookings_count = db.session.query(Inventory)\
	# 		# 							.filter(Inventory.inventory_id == single.inventory_id)\
	# 		# 							.filter(Inventory.inventory_booking_date == one_date)\
	# 		# 							.filter(Inventory.deletion_marker == None)\
	# 		# 							.count()

	# 		inventory_dict["date"] = one_date
	# 		# inventory_dict["count"] = return_bookings_count
	# 		inventory_output.append(inventory_dict)

	# 	return_data["temp"] = inventory_output
		
		# return_data["data"] = inventory_output
		
		# output.append(return_data)
	
	return jsonify({"inventory": output})

	# return_inventory = requests.get(get_active_inventory)
	# inventory_json = return_inventory.json()["data"]

	# return jsonify({"date": inventory_json, "count": len(inventory_json)})