from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


from routes import app, bookings_logger
from database.bookings import Booking
from routes import db
from routes.bookings_urls import get_booking_status_id


@app.route("/bookings/no_show", methods = ["POST"])
def getNoShows():
	# get_bookings = db.session.query(Booking)\
	# 						 .filter(Booking.deletion_marker == None)\
	# 						 .filter(Booking.actual_booking_check_in_date == None)\
	# 						 .filter(Booking.status != get_booking_status_id("No-Show"))\
	# 						 .filter(Booking.status != get_booking_status_id("Updated"))\
	# 						 .all()

	get_bookings = db.session.query(Booking)\
							 .filter(Booking.deletion_marker == None)\
							 .filter(Booking.actual_booking_check_in_date == None)\
							 .filter(Booking.status == get_booking_status_id("Unconfirmed"))\
							 .all()
	
	yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
	
	if not get_bookings:
		message = []
		message.append("There are no bookings to mark as no-shows.")
		return jsonify({"message": message}), 412

	else:
		for single in get_bookings:
			if single.booking_check_in_date.strftime("%Y-%m-%d") == yesterday:
				single.status = get_booking_status_id("No-Show")

			else:
				pass

		try:
			db.session.commit()
			db.session.close()

		except Exception:
			db.session.rollback()
			db.session.close()

			return jsonify({"message": "There was an error marking the booking as a no-show."}), 200

		return jsonify({"message": "Ok"}), 200