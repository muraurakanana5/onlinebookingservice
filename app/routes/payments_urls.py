from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.payment_methods import PaymentMethod
from variables import *


def close(self):
	self.session.close()


@app.route("/payments/new", methods = ["POST"])
def make_new_payment():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		request.json["payment_type"].strip()
		if not request.json["payment_type"]:
			messages.append("Payment type is empty.")
	except KeyError as e:
		messages.append("Payment type is missing.")

	try:
		str(request.json["amount"]).strip()
		if not request.json["amount"]:
			messages.append("Amount is empty.")
	except KeyError as e:
		messages.append("Amount is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	today = datetime.now().strftime("%Y-%m-%d")
	amount_paid = float(request.json["amount"])

	output = []
	message = []

	return_bookings = db.session.query(Booking)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.booking_public_id == request.json["booking_id"])\
								.all()

	if not return_bookings:
		output = []
		output.append("That booking does not appear to exist in the system.")
		return jsonify({"message": output}), 200
	else:
		id_array = []
		inventory_total_cost_array = []
		facility_total_cost_array = []
		gatepass_total_cost_array = []
		vehicle_total_cost_array = []
		total_cost_array = []

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["currency"] = single.currency

			## Facility Costing
			facility_bookings = db.session.query(Facility)\
										  .filter(Facility.deletion_marker == None)\
										  .filter(Facility.booking_id == single.booking_public_id)\
										  .all()

			for single_facility in facility_bookings:
				facility_adult_cost = float(single_facility.facility_booking_adults) * float(single_facility.facility_cost_per_adult) * \
									  float(single_facility.facility_no_of_nights)
				facility_child_cost = float(single_facility.facility_booking_children) * float(single_facility.facility_cost_per_child) * \
									  float(single_facility.facility_no_of_nights)

				if single_facility.facility_no_of_nights%7 != 0:
					weeks_count = round(single_facility.facility_no_of_nights/7) + 1
					
					facility_fixed_cost = float(single_facility.facility_fixed_cost) * weeks_count

				elif single_facility.facility_no_of_nights%7 == 0:
					weeks_count = round(single_facility.facility_no_of_nights/7)
					
					facility_fixed_cost = float(single_facility.facility_fixed_cost) * weeks_count
					

				facility_total_cost_array.append(facility_adult_cost + facility_child_cost + facility_fixed_cost)
				total_cost_array.append(facility_adult_cost + facility_child_cost + facility_fixed_cost)

			## Inventory Costing
			inventory_bookings = db.session.query(Inventory)\
										   .filter(Inventory.deletion_marker == None)\
										   .filter(Inventory.booking_id == single.booking_public_id)\
										   .all()

			for single_inventory in inventory_bookings:
				adult_cost = float(single_inventory.inventory_booking_adults) * float(single_inventory.inventory_cost_per_adult)
				child_cost = float(single_inventory.inventory_booking_children) * float(single_inventory.inventory_cost_per_child)
				
				
				inventory_total_cost_array.append(adult_cost + child_cost)
				total_cost_array.append(adult_cost + child_cost)

			## Gatepass Costing
			gatepass_details = db.session.query(GatepassGuest)\
										 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
										 .add_columns(GatepassGuest.gatepass_guest_type, GatepassGuest.gatepass_guest_count,\
										 			  GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													  GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_currency)\
										 .filter(GatepassGuest.deletion_marker == None)\
										 .filter(Gatepass.deletion_marker == None)\
										 .filter(Gatepass.booking_id == single.booking_public_id)\
										 .all()

			for each_pass in gatepass_details:
				try:
					gatepass_sub_cost = round(float(each_pass.gatepass_guest_count) * ((100 - float(each_pass.gatepass_discount_rate))/100) * \
											  float(each_pass.gatepass_cost_per_pp) * float(each_pass.gatepass_no_of_nights), 2)
					
					gatepass_total_cost_array.append(gatepass_sub_cost)
					total_cost_array.append(gatepass_sub_cost)
				except (NameError, ValueError, TypeError) as no_value:
					pass

			## Vehicle Costing
			vehicle_charges = db.session.query(GatepassVehicle)\
										.join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
										.add_columns(GatepassVehicle.gatepass_vehicle_count, GatepassVehicle.gatepass_cost_per_vehicle)\
										.filter(GatepassVehicle.deletion_marker == None)\
										.filter(Gatepass.deletion_marker == None)\
										.filter(Gatepass.booking_id == single.booking_public_id)\
										.all()

			for each_vehicle in vehicle_charges:
				try:
					vehicle_sub_cost = round(float(each_vehicle.gatepass_vehicle_count) * float(each_vehicle.gatepass_cost_per_vehicle))

					vehicle_total_cost_array.append(vehicle_sub_cost)
					total_cost_array.append(vehicle_sub_cost)
				except (NameError, ValueError, TypeError) as no_value:
					pass

			total_amount = sum(total_cost_array)

	payment_method = db.session.query(PaymentMethod)\
							   .filter(PaymentMethod.deletion_marker == None)\
							   .filter(PaymentMethod.payment_method_public_id == request.json["payment_type"])\
							   .first()

	if payment_method:
		pass
	else:
		message.append("That payment method does not exist in the system.")
		return jsonify({"message": message}), 200
	
	count_past_payments = db.session.query(Transaction)\
									.filter(Transaction.deletion_marker == None)\
									.filter(Transaction.booking_id == request.json["booking_id"])\
									.count()

	if count_past_payments == 0:
		balance = total_amount - amount_paid
		
		transaction = Transaction(
			transaction_booking_public_id = str(uuid.uuid4()),
			booking_id = request.json["booking_id"],
			transaction_original_cost = total_amount,
			transaction_total = amount_paid,
			transaction_balance = balance,
			transaction_payment_method = request.json["payment_type"],
			transaction_date = today,
			transaction_total_currency = single.currency,
			session_id = request.json["session_id"]
		)

		db.session.add(transaction)

		return_data["past_payment_amount"] = 0
		return_data["past_payment_balance"] = 0
		return_data["total_cost"] = total_amount
		return_data["amount_paid"] = amount_paid
		return_data["balance"] = balance

		try:
			db.session.commit()
			close(db)

			output.append(return_data)
			message.append("You have successfully made a payment.")

			return jsonify({"message": message, "output": output})
		except Exception as e:
			print(e)

			db.session.rollback()
			close(db)

			message.append("There was an error on our side making that payment. Please try again later.")
			return jsonify({"message": message}), 422
	
	elif count_past_payments > 0:
		if payment_method.payment_method_name == "Split (Two Different Payment Methods)":
			if count_past_payments == 2:
				message.append("You cannot make more than two payments for that booking.")
				return jsonify({"message": message}), 200
			
			else:
				get_past_payment = db.session.query(Transaction)\
											.filter(Transaction.deletion_marker == None)\
											.filter(Transaction.booking_id == request.json["booking_id"])\
											.first()

				if get_past_payment:
					past_paid_amount = float(get_past_payment.transaction_total)
					past_paid_balance = float(get_past_payment.transaction_balance)
					balance = past_paid_balance - amount_paid

					transaction = Transaction(
						transaction_booking_public_id = str(uuid.uuid4()),
						booking_id = request.json["booking_id"],
						transaction_original_cost = total_amount,
						transaction_total = amount_paid,
						transaction_balance = balance,
						transaction_payment_method = request.json["payment_type"],
						transaction_date = today,
						transaction_total_currency = single.currency,
						session_id = request.json["session_id"]
					)

					db.session.add(transaction)

					return_data["past_payment_amount"] = past_paid_amount
					return_data["past_payment_balance"] = past_paid_balance
					return_data["total_cost"] = total_amount
					return_data["amount_paid"] = amount_paid
					return_data["balance"] = balance

					try:
						db.session.commit()
						close(db)

						output.append(return_data)
						message.append("You have successfully made a payment.")

						return jsonify({"message": message, "output": output})
					except Exception as e:
						print(e)

						db.session.rollback()
						close(db)

						message.append("There was an error on our side making that payment. Please try again later.")
						return jsonify({"message": message}), 422
		
		else:
			message.append("You cannot make another payment for that booking.")
			return jsonify({"message": message}), 200


@app.route("/payments/view")
def view_all_payments():
	get_all_payments = db.session.query(Transaction)\
								 .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
								 .add_columns(Transaction.transaction_booking_public_id, Transaction.booking_id, Transaction.transaction_original_cost,\
								 			  Transaction.transaction_total, Transaction.transaction_total_currency, Transaction.transaction_balance,\
											  Transaction.transaction_payment_method, Transaction.transaction_date, Transaction.session_id,\
											  PaymentMethod.payment_method_name)\
								 .filter(Transaction.deletion_marker == None)\
								 .order_by(Transaction.transaction_booking_id.desc())\
								 .all()

	output = []
	message = []
	id_array = []
	
	if not get_all_payments:
		message.append("There are no payments that have been made yet.")

		return jsonify({"message": message})
	
	else:
		for each_id in get_all_payments:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass
		
		for single in get_all_payments:
			return_data = {}
			return_data["payment_public_id"] = single.transaction_booking_public_id
			return_data["booking_id"] = single.booking_id
			return_data["transaction_original_cost"] = float(single.transaction_original_cost)
			return_data["transaction_total"] = float(single.transaction_total)
			return_data["transaction_total_currency"] = single.transaction_total_currency
			return_data["transaction_balance"] = float(single.transaction_balance)
			return_data["transaction_payment_method_id"] = single.transaction_payment_method
			return_data["transaction_payment_method_name"] = single.payment_method_name
			return_data["transaction_date"] = single.transaction_date
			return_data["session_id"] = single.session_id

			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						return_data["done_by"] = user["first_name"] + " " + user["last_name"]
					else:
						pass
			except (IndexError) as user_error:
				return_data["added_by"] = "N/A"

			output.append(return_data)

		return jsonify({"data": output})