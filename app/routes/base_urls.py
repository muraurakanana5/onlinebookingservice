from flask import jsonify, request, json, make_response
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid

# import cherrypy


from routes import app, bookings_logger
from database.bookings import Booking
from routes import db, memoise_cache
from routes.bookings_urls import get_booking_status_id

# def clearCache():
#     #clear the cache data
#     with app.app_context():
#         cache.clear()


def clearMemoCache():
	with app.app_context():
		memoise_cache.clear()


# @app.route("/exit-app")
# def exit_application():
# 	cherrypy.engine.stop()
# 	cherrypy.engine.start()

# 	return "Ok"


##############
#### Base ####
##############
@app.route("/")
@app.route("/index")
def index():
	output = []
	output.append("Hello world. Welcome to the booking service. We hope you enjoy your stay.")
	return jsonify(output)


@app.route("/request")
def get_request_info():
	header_dict = dict(request.headers)
	try:
		platform = request.user_agent.platform
	except Exception:
		platform = None
	
	try:
		browser = request.user_agent.browser.title()
	except Exception:
		browser = None
	
	return jsonify({"headers": header_dict, "platform": platform, "browser": browser}), 200

	# data = {
	# 	"headers": header_dict,
	# 	"platform": platform,
	# 	"browser": browser
	# }

	# data = json.dumps(data, separators = None, sort_keys = False, indent = None)

	# headers = {
	# 	"Content-Type": "application/json",
	# 	"Server": "90.90.90.90"
	# }
	
	# return Response(status = 418, response = data, headers = headers)


@app.route("/test-before")
def test_before_request():
	return "Ok"


@app.route("/memo-cache/clear")
def clear_memo_cache():
	clearMemoCache()

	return jsonify({"message": "Memo cache cleared."}), 200


# @app.route("/clear/cache")
# def clear_cache():
# 	clearCache()
# 	message = []
# 	message.append("Cache cleared.")

# 	return jsonify({"message": message}), 200



################
#### Errors ####
################
@app.errorhandler(404)
def page_not_found(e):
	output = []
	output.append(str(e))
	# print(e)
	# return jsonify({"message": output}), 404

	data = requests.get("https://http.cat/404")

	resp = make_response(data.content)
	resp.headers["Content-Type"] = "image/jpeg"

	return resp