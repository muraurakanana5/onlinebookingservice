from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_payments import BookingPayment
from database.payment_methods import PaymentMethod
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.salesforce import SalesforceData
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.destination import Destination
from variables import *

from functions.date_operators import *
from functions.currency_operators import *

import pandas as panda

from collections import OrderedDict

def close(self):
	self.session.close()


# To Note: This endpoint is referenced in the UI, but the UI code that would execute it is commented out
@app.route('/generate/reports/csv', methods=['POST'])
def generateCSVreports():
	startdate = request.json['start_date']
	enddate = request.json['end_date']


	bookings_array=[]
	inventory_array=[]
	gatspass_array=[]
	facility_array=[]
	mpesa_array=[]
	bank_transfer_array = []
	credit_card_array = []
	members_array = []
	volunteers_array = []
	donations_array = []


	if startdate == " " or enddate == "":
		messages = []
		if startdate == " ":
			messages.append("Start Date is missing")
		
		if enddate == " ":
			messages.append("End Date is missing")
		
		responseObject = {
			'messages' : messages
		}
		return make_response(jsonify(responseObject)), 422		
	
	all_bookings = db.session.query(Booking)\
								.join(Detail, Booking.booking_public_id == Detail.booking_id)\
								.join(BookingPayment, Booking.booking_public_id == BookingPayment.booking_id)\
								.add_columns(
									Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date, Booking.ticket,\
									Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
									Booking.booking_ref_code, Booking.session_id,\
									Booking.created_at, Booking.status, Booking.payment_status,\
									Booking.currency,\
									Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.city, Detail.country,\
									BookingPayment.booking_amount, BookingPayment.amount_paid, BookingPayment.payment_method
								)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.booking_check_in_date >= startdate)\
								.filter(Booking.booking_check_out_date <= enddate)\
								.all()

	for booking in all_bookings:
		response_data = OrderedDict()
		response_data["check_in"] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
		response_data["check_out"] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
		response_data["booking_ref_code"] = booking.booking_ref_code
		response_data["Ticket"] = booking.ticket if booking.ticket else "N/A"
		response_data['full_name'] = "{} {}".format(booking.first_name, booking.last_name)
		response_data["email"] = booking.email_address
		response_data["phone_number"] = booking.phone_number
		response_data["city"] = booking.city
		response_data["country"] = booking.country
		__payment_method, = db.session.query(PaymentMethod.payment_method_name).filter_by(payment_method_public_id=booking.payment_method).first()
		response_data["payment_method"] = __payment_method
		currency = requests.get(get_currency.format(booking.currency))
		response_data["currency"] = currency.json()["data"][0]["currency_name"]
		response_data["vat_tax_amount"] = float(float(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
		response_data["invoice_amount"] = float(booking.booking_amount) if booking.booking_amount != None else 0
		response_data["amount_paid"] = float(booking.amount_paid) if booking.amount_paid != None else 0
		response_data['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
		bookings_array.append(response_data)

		if __payment_method.lower() == "mpesa":
			mpesa_data = OrderedDict()
			mpesa_data['Check_In'] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
			mpesa_data['Check_Out'] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
			mpesa_data['booking_ref_code'] = booking.booking_ref_code
			m_currency = requests.get(get_currency.format(booking.currency))
			mpesa_data['currency'] = m_currency.json()["data"][0]["currency_name"]
			mpesa_data['vat_tax_amount'] = float(float(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
			mpesa_data['invoice_amount'] = float(booking.booking_amount) if booking.booking_amount != None else 0
			mpesa_data['amount_paid'] = float(booking.amount_paid) if booking.amount_paid != None else 0
			mpesa_data['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
			mpesa_array.append(mpesa_data)

		elif __payment_method.lower() == "credit card":
			credit_card = OrderedDict()
			credit_card['Check_In'] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
			credit_card['Check_Out'] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
			credit_card['booking_ref_code'] = booking.booking_ref_code
			c_currency = requests.get(get_currency.format(booking.currency))
			credit_card['currency'] = c_currency.json()["data"][0]["currency_name"]
			credit_card['vat_tax_amount'] = int(int(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
			credit_card['invoice_amount'] = float(booking.booking_amount) if booking.booking_amount != None else 0
			credit_card['amount_paid'] = float(booking.amount_paid) if booking.amount_paid != None else 0
			credit_card['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
			credit_card_array.append(credit_card)
		else:
			bank_transfer = OrderedDict()
			bank_transfer['Check_In'] = (booking.booking_check_in_date).strftime("%Y-%m-%d")
			bank_transfer['Check_Out'] = (booking.booking_check_out_date).strftime("%Y-%m-%d")
			bank_transfer['booking_ref_code'] = booking.booking_ref_code
			b_currency = requests.get(get_currency.format(booking.currency))
			bank_transfer['currency'] = b_currency.json()["data"][0]["currency_name"]
			bank_transfer['vat_tax_amount'] = int(int(booking.amount_paid) * 0.16) if booking.amount_paid != None else 0
			bank_transfer['invoice_amount'] = float(booking.booking_amount) if booking.booking_amount != None else 0
			bank_transfer['amount_paid'] = float(booking.amount_paid) if booking.amount_paid != None else 0
			bank_transfer['net'] = float(float(booking.amount_paid) - float(float(booking.amount_paid) * 0.16)) if booking.amount_paid != None else 0
			bank_transfer_array.append(bank_transfer)
	
	inventory_bookings = db.session.query(Inventory).filter(Inventory.inventory_booking_date >= startdate).filter(Inventory.inventory_booking_date <= enddate).filter(Inventory.deletion_marker == None).all()

	for inventory in inventory_bookings:
		inventory_booking = OrderedDict()
		inventory_name = requests.get(get_inventory_details.format(inventory.inventory_id))
		
		try:
			inventory_booking["Date"] = (inventory.inventory_booking_date).strftime("%Y-%m-%d")
			inventory_booking["inventory_name"] = inventory_name.json()["data"][0]["name"]
			inventory_booking["code"] = inventory.inventory_code
			inventory_booking["adults"] = inventory.inventory_booking_adults
			inventory_booking["children"] = inventory.inventory_booking_children
			currency = requests.get(get_currency.format(inventory.inventory_cost_currency))
			inventory_booking["Mode_of_Payment"] = inventory.inventory_payment_method 
			inventory_booking["purchase_currency"] = currency.json()["data"][0]["currency_name"]
			inventory_booking["currency"] = 'KES'
			inventory_booking["rate"] = float(inventory.inventory_rate_at_time)
			inventory_booking["vat_tax_amount"] =  float(inventory.inventory_rate_at_time) * float(0.16 * float(float(inventory.inventory_cost_per_child) * int(inventory.inventory_booking_children) + int(inventory.inventory_booking_adults) * float(inventory.inventory_cost_per_adult)))
			inventory_booking["total_revenue"] = float(inventory.inventory_rate_at_time) * float(float(inventory.inventory_cost_per_child) * int(inventory.inventory_booking_children) + int(inventory.inventory_booking_adults) * int(inventory.inventory_cost_per_adult))
			inventory_booking["net"] = float(inventory.inventory_rate_at_time) * float(float(inventory.inventory_cost_per_child) * int(inventory.inventory_booking_children) + int(inventory.inventory_booking_adults) * int(inventory.inventory_cost_per_adult)) - float(float(inventory.inventory_cost_per_child) * int(inventory.inventory_booking_children) + int(inventory.inventory_booking_adults) * int(inventory.inventory_cost_per_adult))
			inventory_array.append(inventory_booking)
		
		except Exception:
			pass
	
	gatepasses = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail,Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.join(GatepassVehicle, Gatepass.gatepass_public_id ==  GatepassVehicle.gatepass_id)\
								.add_columns(
									Gatepass.gatepass_done_by, Gatepass.gatepass_ref_code, Gatepass.gatepass_currency, Gatepass.destination, Gatepass.gatepass_date,\
									GatepassDetail.first_name, GatepassDetail.last_name,\
									GatepassVehicle.gatepass_vehicle_count, GatepassVehicle.gatepass_cost_per_vehicle,\
									GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp, GatepassGuest.gatepass_no_of_nights
								)\
								.filter(Gatepass.start_date >= startdate)\
								.filter(Gatepass.end_date <= enddate)\
								.filter(Gatepass.deletion_marker == None)\
								.all()


	for gatepass in gatepasses:
		gatepass_data = OrderedDict()
		gatepass_data["Check_In_Date"] = (gatepass.gatepass_date).strftime("%Y-%m-%d")
		gatepass_data["Gatepass_Ref_Code"] = gatepass.gatepass_ref_code
		gatepass_data["No_Of_Nights"] = gatepass.gatepass_no_of_nights
		gatepass_data["Bed_Nights"] = gatepass.gatepass_no_of_nights if int(gatepass.gatepass_guest_count) == 0 else int(gatepass.gatepass_guest_count) * gatepass.gatepass_no_of_nights
		gatepass_data["Discount"] = float(gatepass.gatepass_discount_rate)
		gatepass_data["Vehicle_Count"] = gatepass.gatepass_vehicle_count
		gatepass_data["Vehicle_Cost"] = float(gatepass.gatepass_cost_per_vehicle)
		if gatepass.destination:
			try:
				destination, = db.session.query(Destination.gatepass_destination_name).filter(Destination.gatepass_destination_public_id == gatepass.destination).first()
				gatepass_data["Destination"] = destination
			except Exception:
				gatepass_data["Destination"] = None
		else:
			gatepass_data["Destination"] = None
			
		currency = requests.get(get_currency.format(gatepass.gatepass_currency))
		gatepass_data["purchase_currency"] = currency.json()["data"][0]["currency_name"]
		
		totals = float(\
						float\
						(
						float(gatepass.gatepass_discount_rate) * \
							(\
								int(gatepass.gatepass_guest_count) * float(gatepass.gatepass_cost_per_pp)\
							)\
								+ \
							int(gatepass.gatepass_vehicle_count) * float(gatepass.gatepass_cost_per_vehicle)\
						) * int(gatepass.gatepass_no_of_nights)\
					) #(discount * (num_of_guests * ppc) + vechile_count * vprice)*no_of_night
		gatepass_data["Total_Revenue"] = totals

		vat = (float(float(float(gatepass.gatepass_discount_rate) * (int(gatepass.gatepass_guest_count) * float(gatepass.gatepass_cost_per_pp)) + int(gatepass.gatepass_vehicle_count) * float(gatepass.gatepass_cost_per_vehicle)\
						) * int(gatepass.gatepass_no_of_nights))) / 1.16 ### Get the VAT AMOUNT

		gatepass_data["VAT"] = vat
		gatepass_data["Net"] = float(totals - vat)

		gatspass_array.append(gatepass_data)
	
	facility_bookings = db.session.query(Facility).filter(Facility.facility_booking_check_in_date >= startdate, Facility.facility_booking_check_out_date <= enddate).filter(Facility.deletion_marker == None).order_by(Facility.facility_booking_check_in_date.desc()).all()

	for facility in facility_bookings:
		facility_data = OrderedDict()
		facility_details = requests.get(get_facility_details.format(facility.facility_id))
		
		try:
			facility_data['Booking_Id'] = facility.booking_id
			facility_data['Facility_Booking_Check_In_Date'] = (facility.facility_booking_check_in_date).strftime("%Y-%m-%d")
			facility_data['Facility_Booking_Check_Out_Date'] = (facility.facility_booking_check_out_date).strftime("%Y-%m-%d")
			facility_data['Facility_Code'] = facility.facility_code
			facility_data['Facility_Name'] = facility_details.json()['data'][0]['name'] if 'data' in facility_details.json() else "Not Available"
			facility_data['Facility_Analysis_Code'] = facility.facility_analysis_code
			facility_data['No_Of_Nights'] = int(facility.facility_no_of_nights)
			facility_data['Facility_Booking_Adults'] = float(facility.facility_booking_adults)
			facility_data['Facility_Cost_Per_Adult'] = float(facility.facility_cost_per_adult)
			facility_data['Facility_Booking_Children'] = float(facility.facility_booking_children)
			facility_data['Facility_Cost_Per_Child'] = float(facility.facility_cost_per_child)
			facility_data['Facility_Fixed_Cost'] = float(facility.facility_fixed_cost)
			facility_data['Facility_Discount_Rate'] = float(facility.facility_discount_rate)
			facility_data['Facility_Commission_Rate'] = float(facility.facility_commission_rate)
			currency = requests.get(get_currency.format(gatepass.gatepass_currency))
			facility_data['Facility_Purchase_Currency'] = currency.json()["data"][0]["currency_name"]
			facility_data['Analysis_Currency'] = 'KES'
			facility_data['Rate'] = float(facility.facility_rate_at_time)

			if (facility.facility_booking_check_out_date - facility.facility_booking_check_in_date).days <= 7:
				if int(facility.facility_cost_per_adult) == 0 or int(facility.facility_cost_per_child) == 0:
					#use the fixed cost
					# formula (Num_0f_Adults * Fixed Cost) + (Facility_Booking_Children * Fixed Cost)
					facility_data['Total_Amount'] = float(int(facility.facility_booking_adults) * float(facility.facility_fixed_cost) + int(facility.facility_booking_children) * float(facility.facility_fixed_cost)) * float(facility.facility_rate_at_time)
				else:
					# use the defined fee
					# formula (Num_0f_Adults * facility_cost_per_adult) + (Facility_Booking_Children * facility_cost_per_child)
					# get the num_of_weeks
					facility_data['Total_Amount'] = float(int(facility.facility_booking_adults) * float(facility.facility_cost_per_adult) + int(facility.facility_booking_children) * float(facility.facility_cost_per_child)) * float(facility.facility_rate_at_time)
			else:
				num_of_weeks = (facility.facility_booking_check_out_date - facility.facility_booking_check_in_date).days/7
				if type(num_of_weeks) == float:
					num_of_weeks = round(num_of_weeks)
				facility_data['Total_Amount'] = float(int(facility.facility_booking_adults) * float(facility.facility_cost_per_adult) * num_of_weeks + int(facility.facility_booking_children) * float(facility.facility_cost_per_child)* num_of_weeks) * float(facility.facility_rate_at_time)

				# get the number of weeks
			facility_array.append(facility_data)
		
		except Exception:
			pass

	
	## Make an API call to the Membershipship service

	payload = {
		'start_date' : startdate,
		'end_date' : enddate
 	}

	paid_members = requests.get(app.config['MEMBERSHIP_SERVICE'])

	if paid_members.status_code == 200:
		for member in paid_members.json():
			member_data = OrderedDict()
			member_data['Date'] = member["Date"]
			member_data['MemberID'] = member["MemberID"]
			member_data['FullName'] = member["FullName"]
			member_data['Gender'] = member["Gender"]
			member_data['dob'] = member["dob"]
			member_data['IDNumber'] = member["IDNumber"]
			member_data['Email'] = member["Email"]
			member_data['PhoneNumber'] = member["PhoneNumber"]
			member_data['PaymentMethod'] = member["PaymentMethod"]
			member_data['Price'] = member["Price"]
			members_array.append(member_data)
	else:
		members_array = []

	paid_volunteers = requests.post(app.config['VOLUNTEER_SERVICE'], json=payload)

	if paid_volunteers.status_code == 200:
		for volunteers in paid_volunteers.json():
			v_data = OrderedDict()
			v_data['Date'] = volunteers["Date"]
			# v_data['payment_status'] = volunteers["payment_status"]
			v_data['program'] = volunteers["program"]
			v_data['id_alien_number'] = volunteers["id_alien_number"]
			v_data['currency'] = volunteers["currency"]
			v_data['cost'] = volunteers["cost"]
			volunteers_array.append(v_data)
	else:
		volunteers_array = []

	paid_donations = requests.post(app.config['DONATION_SERVICE'], json=payload)

	if paid_donations.status_code == 200:
		for donation in paid_donations.json():
			d_data = OrderedDict()
			d_data['Date'] = donation["Date"]
			d_data['Donation_Id'] = donation["Donation_Id"]
			d_data['PaymentMode'] = donation["payment_method"]
			d_data['Cause'] = donation["payment_options"]
			d_data['Country'] = donation["country"]
			d_data['Currency'] = donation["currency"]
			d_data['Amount'] = donation["amount"]
			donations_array.append(d_data)
	else:
		donations_array = []



	if 'csv' in request.json:
		writer = panda.ExcelWriter("reports/OPCSalesReport"+str(startdate)+"to"+str(enddate)+".xlsx")

		bookings_data_df = panda.DataFrame(bookings_array)
		bookings_data_df.to_excel(writer, "Bookings Data", index=False)
		
		gatespass_data_df = panda.DataFrame(gatspass_array)
		gatespass_data_df.to_excel(writer, "Gatepass Data", index=False)

		inventory_data_df = panda.DataFrame(inventory_array)
		inventory_data_df.to_excel(writer, "Inventory Data", index=False)

		facilty_data_df = panda.DataFrame(facility_array)
		facilty_data_df.to_excel(writer, "Facility Data", index=False)

		mpesa_data_df = panda.DataFrame(mpesa_array)
		mpesa_data_df.to_excel(writer, "Mpesa Summary", index=False)

		credit_card_df = panda.DataFrame(credit_card_array)
		credit_card_df.to_excel(writer, "Credit Card Summary", index=False)

		bank_transfer_df = panda.DataFrame(bank_transfer_array)
		bank_transfer_df.to_excel(writer, "Bank Transfer Summary", index=False)

		memberships_df = panda.DataFrame(members_array)
		memberships_df.to_excel(writer, "Membership Summary", index=False)

		volunteers_df = panda.DataFrame(volunteers_array)
		volunteers_df.to_excel(writer, "Volunteer Program Summary", index=False)

		donations_df = panda.DataFrame(donations_array)
		donations_df.to_excel(writer, "Donations Summary", index=False)

		writer.save()

		file_location_name = ("https://" + app.config['SERVER'] + "/docs/reports/OPCSalesReport"+str(startdate)+"to"+str(enddate)+".xlsx").format(app.config['SERVER'])

		reponseObject = {
			'all_bookings' : bookings_array,
			'inventory_bookings' : inventory_array,
			'gatepass_bookings' : gatspass_array,
			'facility_bookings' : facility_array,
			'mpesa_summary' : mpesa_array,
			'credit_card_array' : credit_card_array,
			'url' : file_location_name
		}

		return make_response(jsonify(reponseObject)), 200
	else: 
		reponseObject = {
			'all_bookings' : bookings_array,
			'inventory_bookings' : inventory_array,
			'gatepass_bookings' : gatspass_array,
			'mpesa_summary' : mpesa_array,
			'facility_bookings' : facility_array,
			'credit_card_array' : credit_card_array
		}

		return make_response(jsonify(reponseObject)), 200




