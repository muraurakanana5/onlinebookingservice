from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.bookings_urls import get_booking_status_id, currencyPostProcessor, convertAmount
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_payments import BookingPayment
from database.booking_status import BookingStatus
from database.credit_note import CreditNote
from database.credit_note_facilities import CreditNoteFacility
from database.credit_note_guests import CreditNoteGuest
from database.credit_note_inventory import CreditNoteInventory
from database.credit_note_vehicles import CreditNoteVehicle
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.payment_gateways import PaymentGateway
from database.vehicle import Vehicle
from database.school import School
from database.school_booking import SchoolBooking
from database.destination import Destination
from functions.booking_snippets import bookingTotal, creditNoteTotal, get_details_currency
from functions.date_operators import GenerateDateFromString
from variables import *


def close(self):
	self.session.close()


## TODO Remove the /old and _
@app.route("/batchfile/generate/data/old")
def generate_full_batchfile_data_():
	select_bookings = db.session.query(Booking)\
								.join(Gatepass, Booking.booking_public_id ==  Gatepass.booking_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_ref_code, Booking.ticket,\
											 Booking.currency, Booking.booking_done_by, Booking.created_at,\
											 Booking.booking_check_in_date,\
											 Gatepass.gatepass_public_id, Gatepass.gatepass_date)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.created_at != None)\
								.filter(Booking.status != get_booking_status_id("Deleted"))\
								.filter(Booking.status != get_booking_status_id("Cancelled"))\
								.filter(Booking.status != get_booking_status_id("Abandoned"))\
								.filter(Gatepass.deletion_marker == None)\
								.all()

	yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
	
	existing_bookings = []
	
	data = []
	for single_booking in select_bookings:
		# if single_booking.created_at.strftime("%Y-%m-%d") == datetime.now().strftime("%Y-%m-%d"):
		if single_booking.booking_public_id in existing_bookings:
			pass
		else:
			if single_booking.booking_check_in_date.strftime("%Y-%m-%d") == "2019-04-03":
				return_data = {}

				booking_currency = requests.get(get_currency.format(single_booking.currency))
				bookingTotal(return_data, single_booking.booking_public_id, True)

				customer_ref = single_booking.booking_ref_code + " " + single_booking.booking_done_by

				if len(customer_ref) >= 30:
					customer_reference = customer_ref[:29]
				else:
					customer_reference = customer_ref

				for single_gatepass_guest in return_data["guests"]:
					if single_gatepass_guest["payment_guests"] > 0:
						## Income (Guests) ##
						income_gatepass_data = {}
						income_gatepass_data["income_account"] = single_gatepass_guest["payment_person_income_code"]
						income_gatepass_data["customer_reference"] = customer_reference
						
						# income_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
						# income_gatepass_data["currency"] = single_gatepass_guest["currency"]
						income_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
						
						income_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# income_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
						income_gatepass_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of people is already done
						income_gatepass_data["unit_price"] = round(single_gatepass_guest["price_minus_vat"], 2)
						income_gatepass_data["vat"] = "V"
						income_gatepass_data["dc_marker"] = "C"
						# income_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
						income_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						income_gatepass_data["journal_type"] = "VI"
						income_gatepass_data["account_description"] = single_booking.ticket
						income_gatepass_data["department"] = single_gatepass_guest["payment_person_dept_code"]
						income_gatepass_data["outlet"] = single_gatepass_guest["destination_outlet_code"]
						income_gatepass_data["base_amount"] = round(single_gatepass_guest["base_amount_minus_vat"], 2)

						data.append(income_gatepass_data)

						## VAT (Guests) ##
						vat_gatepass_data = {}
						vat_gatepass_data["income_account"] = "510001"
						vat_gatepass_data["customer_reference"] = customer_reference
						
						# vat_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
						# vat_gatepass_data["currency"] = single_gatepass_guest["currency"]
						vat_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
						
						vat_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# vat_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
						vat_gatepass_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of people is already done
						vat_gatepass_data["unit_price"] = round(single_gatepass_guest["vat"], 2)
						vat_gatepass_data["vat"] = "V"
						vat_gatepass_data["dc_marker"] = "C"
						# vat_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
						vat_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						vat_gatepass_data["journal_type"] = "VI"
						vat_gatepass_data["account_description"] = single_booking.ticket
						vat_gatepass_data["outlet"] = "#"
						vat_gatepass_data["department"] = "#"
						vat_gatepass_data["base_amount"] = round(single_gatepass_guest["base_vat"], 2)

						data.append(vat_gatepass_data)

				for single_gatepass_vehicle in return_data["vehicles"]:
					if single_gatepass_vehicle["vehicles"] > 0:
						## Income (Vehicles) ##
						income_gatepass_vehicle_data = {}
						income_gatepass_vehicle_data["income_account"] = single_gatepass_vehicle["vehicle_charge_income_code"]
						income_gatepass_vehicle_data["customer_reference"] = customer_reference
						
						# income_gatepass_vehicle_data["currency_code"] = single_gatepass_vehicle["gatepass_vehicle_currency_at_time"]
						# income_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["currency"]
						income_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["vehicle_charge_cost_currency"]
						
						income_gatepass_vehicle_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# income_gatepass_vehicle_data["tran_quantity"] = single_gatepass_vehicle["vehicles"]
						income_gatepass_vehicle_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of vehicles is already done
						income_gatepass_vehicle_data["unit_price"] = round(single_gatepass_vehicle["price_minus_vat"], 2)
						income_gatepass_vehicle_data["vat"] = "V"
						income_gatepass_vehicle_data["dc_marker"] = "C"
						# income_gatepass_vehicle_data["conversion_rate"] = single_gatepass_vehicle["gatepass_vehicle_rate_at_time"]
						income_gatepass_vehicle_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						income_gatepass_vehicle_data["journal_type"] = "VI"
						income_gatepass_vehicle_data["account_description"] = single_booking.ticket
						income_gatepass_vehicle_data["outlet"] = single_gatepass_vehicle["vehicle_charge_outlet_code"]
						income_gatepass_vehicle_data["department"] = single_gatepass_vehicle["vehicle_charge_dept_code"]
						income_gatepass_vehicle_data["base_amount"] = round(single_gatepass_vehicle["base_amount_minus_vat"], 2)

						data.append(income_gatepass_vehicle_data)

						## VAT (Vehicles) ##
						vat_gatepass_vehicle_data = {}
						vat_gatepass_vehicle_data["income_account"] = "510001"
						vat_gatepass_vehicle_data["customer_reference"] = customer_reference
						
						# vat_gatepass_vehicle_data["currency_code"] = single_gatepass_vehicle["gatepass_vehicle_currency_at_time"]
						# vat_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["currency"]
						vat_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["vehicle_charge_cost_currency"]
						
						vat_gatepass_vehicle_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# vat_gatepass_vehicle_data["tran_quantity"] = single_gatepass_vehicle["vehicles"]
						vat_gatepass_vehicle_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of vehicles is already done
						vat_gatepass_vehicle_data["unit_price"] = round(single_gatepass_vehicle["vat"], 2)
						vat_gatepass_vehicle_data["vat"] = "V"
						vat_gatepass_vehicle_data["dc_marker"] = "C"
						# vat_gatepass_vehicle_data["conversion_rate"] = single_gatepass_vehicle["gatepass_vehicle_rate_at_time"]
						vat_gatepass_vehicle_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						vat_gatepass_vehicle_data["journal_type"] = "VI"
						vat_gatepass_vehicle_data["account_description"] = single_booking.ticket
						vat_gatepass_vehicle_data["outlet"] = "#"
						vat_gatepass_vehicle_data["department"] = "#"
						vat_gatepass_vehicle_data["base_amount"] = round(single_gatepass_vehicle["base_vat"], 2)

						data.append(vat_gatepass_vehicle_data)

				# data.append(return_data["inventory_bookings"])
				
				if return_data["inventory_bookings"]:
					for single_inventory in return_data["inventory_bookings"]:
						if single_inventory["inventory_booking_adults"] > 0:
							if single_inventory["vat"] & single_inventory["catering_levy"]:
								## Income (Inventory Adult Bookings) ##
								income_adult_inventory_data = {}
								income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_adult_inventory_data["customer_reference"] = customer_reference
								
								# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_adult_inventory_data["currency"] = single_inventory["currency"]
								income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
								income_adult_inventory_data["unit_price"] = round(single_inventory["adult_minus_taxes"], 2)
								income_adult_inventory_data["vat"] = "V"
								income_adult_inventory_data["dc_marker"] = "C"
								# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_adult_inventory_data["journal_type"] = "VI"
								income_adult_inventory_data["account_description"] = single_booking.ticket
								income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_taxes"], 2)

								data.append(income_adult_inventory_data)

								## VAT (Inventory Adult Bookings) ##
								vat_adult_inventory_data = {}
								vat_adult_inventory_data["income_account"] = "510001"
								vat_adult_inventory_data["customer_reference"] = customer_reference
								
								# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# vat_adult_inventory_data["currency"] = single_inventory["currency"]
								vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
								vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
								vat_adult_inventory_data["vat"] = "V"
								vat_adult_inventory_data["dc_marker"] = "C"
								# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								vat_adult_inventory_data["journal_type"] = "VI"
								vat_adult_inventory_data["account_description"] = single_booking.ticket
								vat_adult_inventory_data["outlet"] = "#"
								vat_adult_inventory_data["department"] = "#"
								vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

								data.append(vat_adult_inventory_data)

								## Catering Levy (Inventory Adult Bookings) ##
								cater_adult_inventory_data = {}
								cater_adult_inventory_data["income_account"] = "511006"
								cater_adult_inventory_data["customer_reference"] = customer_reference
								
								# cater_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# cater_adult_inventory_data["currency"] = single_inventory["currency"]
								cater_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								cater_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
								cater_adult_inventory_data["unit_price"] = round(single_inventory["adult_cater"], 2)
								cater_adult_inventory_data["vat"] = None
								cater_adult_inventory_data["dc_marker"] = "C"
								# cater_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								cater_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_adult_inventory_data["journal_type"] = "VI"
								cater_adult_inventory_data["account_description"] = single_booking.ticket
								cater_adult_inventory_data["outlet"] = "#"
								cater_adult_inventory_data["department"] = "#"
								cater_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_cater"], 2)

								data.append(cater_adult_inventory_data)

							else:						
								if single_inventory["vat"]:
									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_vat"], 2)
									income_adult_inventory_data["vat"] = "V"
									income_adult_inventory_data["dc_marker"] = "C"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_vat"], 2)

									data.append(income_adult_inventory_data)

									## VAT (Inventory Adult Bookings) ##
									vat_adult_inventory_data = {}
									vat_adult_inventory_data["income_account"] = "510001"
									vat_adult_inventory_data["customer_reference"] = customer_reference
									
									# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_adult_inventory_data["currency"] = single_inventory["currency"]
									vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
									vat_adult_inventory_data["vat"] = "V"
									vat_adult_inventory_data["dc_marker"] = "C"
									# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_inventory_data["journal_type"] = "VI"
									vat_adult_inventory_data["account_description"] = single_booking.ticket
									vat_adult_inventory_data["outlet"] = "#"
									vat_adult_inventory_data["department"] = "#"
									vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

									data.append(vat_adult_inventory_data)

								else:
									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price"], 2)
									income_adult_inventory_data["vat"] = "V"
									income_adult_inventory_data["dc_marker"] = "C"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price"], 2)

									data.append(income_adult_inventory_data)

								if single_inventory["catering_levy"]:
									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_cater"], 2)
									income_adult_inventory_data["vat"] = None
									income_adult_inventory_data["dc_marker"] = "C"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_cater"], 2)

									data.append(income_adult_inventory_data)

									## Catering Levy (Inventory Adult Bookings) ##
									cater_adult_inventory_data = {}
									cater_adult_inventory_data["income_account"] = "511006"
									cater_adult_inventory_data["customer_reference"] = customer_reference
									
									# cater_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# cater_adult_inventory_data["currency"] = single_inventory["currency"]
									cater_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									cater_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									cater_adult_inventory_data["unit_price"] = round(single_inventory["adult_cater"], 2)
									cater_adult_inventory_data["vat"] = None
									cater_adult_inventory_data["dc_marker"] = "C"
									# cater_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									cater_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_adult_inventory_data["journal_type"] = "VI"
									cater_adult_inventory_data["account_description"] = single_booking.ticket
									cater_adult_inventory_data["outlet"] = "#"
									cater_adult_inventory_data["department"] = "#"
									cater_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_cater"], 2)

									data.append(cater_adult_inventory_data)
				
						if single_inventory["inventory_booking_children"] > 0:
							if single_inventory["vat"] & single_inventory["catering_levy"]:
								## Income (Inventory Child Bookings) ##
								income_child_inventory_data = {}
								income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_child_inventory_data["customer_reference"] = customer_reference
								
								# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_child_inventory_data["currency"] = single_inventory["currency"]
								income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								income_child_inventory_data["unit_price"] = round(single_inventory["child_minus_taxes"], 2)
								income_child_inventory_data["vat"] = "V"
								income_child_inventory_data["dc_marker"] = "C"
								# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_child_inventory_data["journal_type"] = "VI"
								income_child_inventory_data["account_description"] = single_booking.ticket
								income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_taxes"], 2)

								data.append(income_child_inventory_data)

								## VAT (VAT Child Bookings) ##
								vat_child_inventory_data = {}
								vat_child_inventory_data["income_account"] = "510001"
								vat_child_inventory_data["customer_reference"] = customer_reference
								
								# vat_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# vat_child_inventory_data["currency"] = single_inventory["currency"]
								vat_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								vat_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								vat_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								vat_child_inventory_data["unit_price"] = round(single_inventory["child_vat"], 2)
								vat_child_inventory_data["vat"] = "V"
								vat_child_inventory_data["dc_marker"] = "C"
								# vat_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								vat_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								vat_child_inventory_data["journal_type"] = "VI"
								vat_child_inventory_data["account_description"] = single_booking.ticket
								vat_child_inventory_data["outlet"] = "#"
								vat_child_inventory_data["department"] = "#"
								vat_child_inventory_data["base_amount"] = round(single_inventory["base_child_vat"], 2)

								data.append(vat_child_inventory_data)

								## Catering Levy (Inventory Child Bookings) ##
								cater_child_inventory_data = {}
								cater_child_inventory_data["income_account"] = "511006"
								cater_child_inventory_data["customer_reference"] = customer_reference
								
								# cater_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# cater_child_inventory_data["currency"] = single_inventory["currency"]
								cater_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								cater_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								cater_child_inventory_data["unit_price"] = round(single_inventory["child_cater"], 2)
								cater_child_inventory_data["vat"] = None
								cater_child_inventory_data["dc_marker"] = "C"
								# cater_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								cater_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_child_inventory_data["journal_type"] = "VI"
								cater_child_inventory_data["account_description"] = single_booking.ticket
								cater_child_inventory_data["outlet"] = "#"
								cater_child_inventory_data["department"] = "#"
								cater_child_inventory_data["base_amount"] = round(single_inventory["base_child_cater"], 2)

								data.append(cater_child_inventory_data)
							
							else:
								if single_inventory["vat"]:
									## Income (Inventory Child Bookings) ##
									income_child_inventory_data = {}
									income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_child_inventory_data["customer_reference"] = customer_reference
									
									# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_child_inventory_data["currency"] = single_inventory["currency"]
									income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									income_child_inventory_data["unit_price"] = round(single_inventory["child_price_minus_vat"], 2)
									income_child_inventory_data["vat"] = "V"
									income_child_inventory_data["dc_marker"] = "C"
									# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_inventory_data["journal_type"] = "VI"
									income_child_inventory_data["account_description"] = single_booking.ticket
									income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_vat"], 2)

									data.append(income_child_inventory_data)

									## VAT (VAT Child Bookings) ##
									vat_child_inventory_data = {}
									vat_child_inventory_data["income_account"] = "510001"
									vat_child_inventory_data["customer_reference"] = customer_reference
									
									# vat_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_child_inventory_data["currency"] = single_inventory["currency"]
									vat_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									vat_child_inventory_data["unit_price"] = round(single_inventory["child_vat"], 2)
									vat_child_inventory_data["vat"] = "V"
									vat_child_inventory_data["dc_marker"] = "C"
									# vat_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_child_inventory_data["journal_type"] = "VI"
									vat_child_inventory_data["account_description"] = single_booking.ticket
									vat_child_inventory_data["outlet"] = "#"
									vat_child_inventory_data["department"] = "#"
									vat_child_inventory_data["base_amount"] = round(single_inventory["base_child_vat"], 2)

									data.append(vat_child_inventory_data)

								else:
									## Income (Inventory Child Bookings) ##
									income_child_inventory_data = {}
									income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_child_inventory_data["customer_reference"] = customer_reference
									
									# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_child_inventory_data["currency"] = single_inventory["currency"]
									income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									income_child_inventory_data["unit_price"] = round(single_inventory["child_price"], 2)
									income_child_inventory_data["vat"] = "V"
									income_child_inventory_data["dc_marker"] = "C"
									# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_inventory_data["journal_type"] = "VI"
									income_child_inventory_data["account_description"] = single_booking.ticket
									income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price"], 2)

									data.append(income_child_inventory_data)
								
								if single_inventory["catering_levy"]:
									## Income (Inventory Child Bookings) ##
									income_child_inventory_data = {}
									income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_child_inventory_data["customer_reference"] = customer_reference
									
									# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_child_inventory_data["currency"] = single_inventory["currency"]
									income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									income_child_inventory_data["unit_price"] = round(single_inventory["child_price_minus_cater"], 2)
									income_child_inventory_data["vat"] = None
									income_child_inventory_data["dc_marker"] = "C"
									# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_inventory_data["journal_type"] = "VI"
									income_child_inventory_data["account_description"] = single_booking.ticket
									income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_cater"], 2)

									data.append(income_child_inventory_data)

									## Catering Levy (Inventory Child Bookings) ##
									cater_child_inventory_data = {}
									cater_child_inventory_data["income_account"] = "511006"
									cater_child_inventory_data["customer_reference"] = customer_reference
									
									# cater_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# cater_child_inventory_data["currency"] = single_inventory["currency"]
									cater_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									cater_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									cater_child_inventory_data["unit_price"] = round(single_inventory["child_cater"], 2)
									cater_child_inventory_data["vat"] = None
									cater_child_inventory_data["dc_marker"] = "C"
									# cater_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									cater_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_child_inventory_data["journal_type"] = "VI"
									cater_child_inventory_data["account_description"] = single_booking.ticket
									cater_child_inventory_data["outlet"] = "#"
									cater_child_inventory_data["department"] = "#"
									cater_child_inventory_data["base_amount"] = round(single_inventory["base_child_cater"], 2)

									data.append(cater_child_inventory_data)
				
				if return_data["facility_bookings"]:
					for single_facility in return_data["facility_bookings"]:
						if single_facility["facility_type"] == "Accomodation":
							if single_facility["accomodation_type"] == "Stables":
								if single_facility["vat"] & single_facility["catering_levy"]:
									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
										## Income (Facility (Stables) Fixed Rate) ##
										income_fixed_stables_data = {}
										income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
										income_fixed_stables_data["customer_reference"] = customer_reference
										income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_fixed_stables_data["vat"] = "V"
										income_fixed_stables_data["dc_marker"] = "C"
										income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_fixed_stables_data["journal_type"] = "VI"
										income_fixed_stables_data["account_description"] = single_booking.ticket
										income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_taxes"], 2)

										data.append(income_fixed_stables_data)

										## VAT (Facility (Stables) Fixed Rate) ##
										vat_fixed_stables_data = {}
										vat_fixed_stables_data["income_account"] = "510001"
										vat_fixed_stables_data["customer_reference"] = customer_reference
										vat_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										vat_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										vat_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_fixed_stables_data["vat"] = "V"
										vat_fixed_stables_data["dc_marker"] = "C"
										vat_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_fixed_stables_data["journal_type"] = "VI"
										vat_fixed_stables_data["account_description"] = single_booking.ticket
										vat_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										vat_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_vat"], 2)

										data.append(vat_fixed_stables_data)

										## Catering Levy (Facility (Stables) Fixed Rate) ##
										cater_fixed_stables_data = {}
										cater_fixed_stables_data["income_account"] = "511006"
										cater_fixed_stables_data["customer_reference"] = customer_reference
										cater_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										cater_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										cater_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_fixed_stables_data["vat"] = "V"
										cater_fixed_stables_data["dc_marker"] = "C"
										cater_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_fixed_stables_data["journal_type"] = "VI"
										cater_fixed_stables_data["account_description"] = single_booking.ticket
										cater_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										cater_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_cater"], 2)

										data.append(cater_fixed_stables_data)
									
									# if single_facility["facility_booking_adults"] > 0:
									# 	## Income (Facility (Stables) Adult Bookings) ##
									# 	income_adult_stables_data = {}
									# 	income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
									# 	income_adult_stables_data["customer_reference"] = customer_reference
									# 	income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
									# 	income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									# 	income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
									# 	income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									# 	income_adult_stables_data["vat"] = "V"
									# 	income_adult_stables_data["dc_marker"] = "C"
									# 	income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									# 	income_adult_stables_data["journal_type"] = "VI"
									# 	income_adult_stables_data["account_description"] = single_booking.ticket
									# 	income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
									# 	income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									# 	income_adult_stables_data["base_amount"] = round(single_facility["base_adult_minus_taxes"], 2)

									# 	data.append(income_adult_stables_data)

									# 	## VAT (Facility (Stables) Adult Bookings) ##
									# 	vat_adult_stables_data = {}
									# 	vat_adult_stables_data["income_account"] = "510001"
									# 	vat_adult_stables_data["customer_reference"] = customer_reference
									# 	vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
									# 	vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									# 	vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
									# 	vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									# 	vat_adult_stables_data["vat"] = "V"
									# 	vat_adult_stables_data["dc_marker"] = "C"
									# 	vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									# 	vat_adult_stables_data["journal_type"] = "VI"
									# 	vat_adult_stables_data["account_description"] = single_booking.ticket
									# 	vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
									# 	vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									# 	vat_adult_stables_data["base_amount"] = round(single_facility["base_adult_vat"], 2)

									# 	data.append(vat_adult_stables_data)

									# 	## Catering Levy (Facility (Stables) Adult Bookings) ##
									# 	cater_adult_stables_data = {}
									# 	cater_adult_stables_data["income_account"] = "511006"
									# 	cater_adult_stables_data["customer_reference"] = customer_reference
									# 	cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
									# 	cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									# 	cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
									# 	cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									# 	cater_adult_stables_data["vat"] = "V"
									# 	cater_adult_stables_data["dc_marker"] = "C"
									# 	cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									# 	cater_adult_stables_data["journal_type"] = "VI"
									# 	cater_adult_stables_data["account_description"] = single_booking.ticket
									# 	cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
									# 	cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									# 	cater_adult_stables_data["base_amount"] = round(single_facility["base_adult_cater"], 2)

									# 	data.append(cater_adult_stables_data)

									# if single_facility["facility_booking_children"] > 0:
									# 	## Income (Facility (Stables) Children Bookings) ##
									# 	income_child_stables_data = {}
									# 	income_child_stables_data["income_account"] = single_facility["facility_income_code"]
									# 	income_child_stables_data["customer_reference"] = customer_reference
									# 	income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
									# 	income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									# 	income_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
									# 	income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									# 	income_child_stables_data["vat"] = "V"
									# 	income_child_stables_data["dc_marker"] = "C"
									# 	income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									# 	income_child_stables_data["journal_type"] = "VI"
									# 	income_child_stables_data["account_description"] = single_booking.ticket
									# 	income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
									# 	income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									# 	income_child_stables_data["base_amount"] = round(single_facility["base_child_minus_taxes"], 2)

									# 	data.append(income_child_stables_data)

									# 	## VAT (Facility (Stables) Children Bookings) ##
									# 	vat_child_stables_data = {}
									# 	vat_child_stables_data["income_account"] = "510001"
									# 	vat_child_stables_data["customer_reference"] = customer_reference
									# 	vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
									# 	vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									# 	vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
									# 	vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									# 	vat_child_stables_data["vat"] = "V"
									# 	vat_child_stables_data["dc_marker"] = "C"
									# 	vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									# 	vat_child_stables_data["journal_type"] = "VI"
									# 	vat_child_stables_data["account_description"] = single_booking.ticket
									# 	vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
									# 	vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									# 	vat_child_stables_data["base_amount"] = round(single_facility["base_child_vat"], 2)

									# 	data.append(vat_child_stables_data)

									# 	## Catering Levy (Facility (Stables) Children Bookings) ##
									# 	cater_child_stables_data = {}
									# 	cater_child_stables_data["income_account"] = "511006"
									# 	cater_child_stables_data["customer_reference"] = customer_reference
									# 	cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
									# 	cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									# 	cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
									# 	cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									# 	cater_child_stables_data["vat"] = "V"
									# 	cater_child_stables_data["dc_marker"] = "C"
									# 	cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									# 	cater_child_stables_data["journal_type"] = "VI"
									# 	cater_child_stables_data["account_description"] = single_booking.ticket
									# 	cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
									# 	cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									# 	cater_child_stables_data["base_amount"] = round(single_facility["base_child_cater"], 2)

									# 	data.append(cater_child_stables_data)

									if single_facility["facility_booking_extra_adults"] > 0:
										## Income (Facility (Stables) Extra Adult Bookings) ##
										income_adult_stables_data = {}
										income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										income_adult_stables_data["customer_reference"] = customer_reference
										income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_adult_stables_data["vat"] = "V"
										income_adult_stables_data["dc_marker"] = "C"
										income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_stables_data["journal_type"] = "VI"
										income_adult_stables_data["account_description"] = single_booking.ticket
										income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_taxes"], 2)

										data.append(income_adult_stables_data)

										## VAT (Facility (Stables) Extra Adult Bookings) ##
										vat_adult_stables_data = {}
										vat_adult_stables_data["income_account"] = "510001"
										vat_adult_stables_data["customer_reference"] = customer_reference
										vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_adult_stables_data["vat"] = "V"
										vat_adult_stables_data["dc_marker"] = "C"
										vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_adult_stables_data["journal_type"] = "VI"
										vat_adult_stables_data["account_description"] = single_booking.ticket
										vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_vat"], 2)

										data.append(vat_adult_stables_data)

										## Catering Levy (Facility (Stables) Extra Adult Bookings) ##
										cater_adult_stables_data = {}
										cater_adult_stables_data["income_account"] = "511006"
										cater_adult_stables_data["customer_reference"] = customer_reference
										cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_adult_stables_data["vat"] = "V"
										cater_adult_stables_data["dc_marker"] = "C"
										cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_adult_stables_data["journal_type"] = "VI"
										cater_adult_stables_data["account_description"] = single_booking.ticket
										cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_cater"], 2)

										data.append(cater_adult_stables_data)

									if single_facility["facility_booking_extra_children"] > 0:
										## Income (Facility (Stables) Extra Children Bookings) ##
										income_child_stables_data = {}
										income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										income_child_stables_data["customer_reference"] = customer_reference
										income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_child_stables_data["vat"] = "V"
										income_child_stables_data["dc_marker"] = "C"
										income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_stables_data["journal_type"] = "VI"
										income_child_stables_data["account_description"] = single_booking.ticket
										income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_taxes"], 2)

										data.append(income_child_stables_data)

										## VAT (Facility (Stables) Extra Children Bookings) ##
										vat_child_stables_data = {}
										vat_child_stables_data["income_account"] = "510001"
										vat_child_stables_data["customer_reference"] = customer_reference
										vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_child_stables_data["vat"] = "V"
										vat_child_stables_data["dc_marker"] = "C"
										vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_child_stables_data["journal_type"] = "VI"
										vat_child_stables_data["account_description"] = single_booking.ticket
										vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_child_stables_data["base_amount"] = round(single_facility["base_extra_child_vat"], 2)

										data.append(vat_child_stables_data)

										## Catering Levy (Facility (Stables) Extra Children Bookings) ##
										cater_child_stables_data = {}
										cater_child_stables_data["income_account"] = "511006"
										cater_child_stables_data["customer_reference"] = customer_reference
										cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_child_stables_data["vat"] = "V"
										cater_child_stables_data["dc_marker"] = "C"
										cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_child_stables_data["journal_type"] = "VI"
										cater_child_stables_data["account_description"] = single_booking.ticket
										cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_child_stables_data["base_amount"] = round(single_facility["base_extra_child_cater"], 2)

										data.append(cater_child_stables_data)
								
								else:
									if single_facility["vat"]:
										if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
											## Income (Facility (Stables) Fixed Rate) ##
											income_fixed_stables_data = {}
											income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
											income_fixed_stables_data["customer_reference"] = customer_reference
											income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_fixed_stables_data["vat"] = "V"
											income_fixed_stables_data["dc_marker"] = "C"
											income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_fixed_stables_data["journal_type"] = "VI"
											income_fixed_stables_data["account_description"] = single_booking.ticket
											income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_vat"], 2)

											data.append(income_fixed_stables_data)

											## VAT (Facility (Stables) Fixed Rate) ##
											vat_fixed_stables_data = {}
											vat_fixed_stables_data["income_account"] = "510001"
											vat_fixed_stables_data["customer_reference"] = customer_reference
											vat_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											vat_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											vat_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_fixed_stables_data["vat"] = "V"
											vat_fixed_stables_data["dc_marker"] = "C"
											vat_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_fixed_stables_data["journal_type"] = "VI"
											vat_fixed_stables_data["account_description"] = single_booking.ticket
											vat_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											vat_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_vat"], 2)

											data.append(vat_fixed_stables_data)
										
										# if single_facility["facility_booking_adults"] > 0:
										# 	## Income (Facility (Stables) Adult Bookings) ##
										# 	income_adult_stables_data = {}
										# 	income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										# 	income_adult_stables_data["customer_reference"] = customer_reference
										# 	income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# 	income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	income_adult_stables_data["vat"] = "V"
										# 	income_adult_stables_data["dc_marker"] = "C"
										# 	income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	income_adult_stables_data["journal_type"] = "VI"
										# 	income_adult_stables_data["account_description"] = single_booking.ticket
										# 	income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	income_adult_stables_data["base_amount"] = round(single_facility["base_adult_minus_vat"], 2)

										# 	data.append(income_adult_stables_data)

										# 	## VAT (Facility (Stables) Adult Bookings) ##
										# 	vat_adult_stables_data = {}
										# 	vat_adult_stables_data["income_account"] = "510001"
										# 	vat_adult_stables_data["customer_reference"] = customer_reference
										# 	vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# 	vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	vat_adult_stables_data["vat"] = "V"
										# 	vat_adult_stables_data["dc_marker"] = "C"
										# 	vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	vat_adult_stables_data["journal_type"] = "VI"
										# 	vat_adult_stables_data["account_description"] = single_booking.ticket
										# 	vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	vat_adult_stables_data["base_amount"] = round(single_facility["base_adult_vat"], 2)

										# 	data.append(vat_adult_stables_data)

										# if single_facility["facility_booking_children"] > 0:
										# 	## Income (Facility (Stables) Children Bookings) ##
										# 	income_child_stables_data = {}
										# 	income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										# 	income_child_stables_data["customer_reference"] = customer_reference
										# 	income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	income_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# 	income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	income_child_stables_data["vat"] = "V"
										# 	income_child_stables_data["dc_marker"] = "C"
										# 	income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	income_child_stables_data["journal_type"] = "VI"
										# 	income_child_stables_data["account_description"] = single_booking.ticket
										# 	income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	income_child_stables_data["base_amount"] = round(single_facility["base_child_minus_vat"], 2)

										# 	data.append(income_child_stables_data)

										# 	## VAT (Facility (Stables) Children Bookings) ##
										# 	vat_child_stables_data = {}
										# 	vat_child_stables_data["income_account"] = "510001"
										# 	vat_child_stables_data["customer_reference"] = customer_reference
										# 	vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# 	vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	vat_child_stables_data["vat"] = "V"
										# 	vat_child_stables_data["dc_marker"] = "C"
										# 	vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	vat_child_stables_data["journal_type"] = "VI"
										# 	vat_child_stables_data["account_description"] = single_booking.ticket
										# 	vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	vat_child_stables_data["base_amount"] = round(single_facility["base_child_vat"], 2)

										# 	data.append(vat_child_stables_data)

										if single_facility["facility_booking_extra_adults"] > 0:
											## Income (Facility (Stables) Extra Adult Bookings) ##
											income_adult_stables_data = {}
											income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
											income_adult_stables_data["customer_reference"] = customer_reference
											income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_adult_stables_data["vat"] = "V"
											income_adult_stables_data["dc_marker"] = "C"
											income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_stables_data["journal_type"] = "VI"
											income_adult_stables_data["account_description"] = single_booking.ticket
											income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_vat"], 2)

											data.append(income_adult_stables_data)

											## VAT (Facility (Stables) Extra Adult Bookings) ##
											vat_adult_stables_data = {}
											vat_adult_stables_data["income_account"] = "510001"
											vat_adult_stables_data["customer_reference"] = customer_reference
											vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_adult_stables_data["vat"] = "V"
											vat_adult_stables_data["dc_marker"] = "C"
											vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_adult_stables_data["journal_type"] = "VI"
											vat_adult_stables_data["account_description"] = single_booking.ticket
											vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_vat"], 2)

											data.append(vat_adult_stables_data)

										if single_facility["facility_booking_extra_children"] > 0:
											## Income (Facility (Stables) Extra Children Bookings) ##
											income_child_stables_data = {}
											income_child_stables_data["income_account"] = single_facility["facility_income_code"]
											income_child_stables_data["customer_reference"] = customer_reference
											income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_child_stables_data["vat"] = "V"
											income_child_stables_data["dc_marker"] = "C"
											income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_stables_data["journal_type"] = "VI"
											income_child_stables_data["account_description"] = single_booking.ticket
											income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_vat"], 2)

											data.append(income_child_stables_data)

											## VAT (Facility (Stables) Extra Children Bookings) ##
											vat_child_stables_data = {}
											vat_child_stables_data["income_account"] = "510001"
											vat_child_stables_data["customer_reference"] = customer_reference
											vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_child_stables_data["vat"] = "V"
											vat_child_stables_data["dc_marker"] = "C"
											vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_child_stables_data["journal_type"] = "VI"
											vat_child_stables_data["account_description"] = single_booking.ticket
											vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_child_stables_data["base_amount"] = round(single_facility["base_extra_child_vat"], 2)

											data.append(vat_child_stables_data)
									else:
										if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
											## Income (Facility (Stables) Fixed Rate) ##
											income_fixed_stables_data = {}
											income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
											income_fixed_stables_data["customer_reference"] = customer_reference
											income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_fixed_stables_data["vat"] = "V"
											income_fixed_stables_data["dc_marker"] = "C"
											income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_fixed_stables_data["journal_type"] = "VI"
											income_fixed_stables_data["account_description"] = single_booking.ticket
											income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed"], 2)

											data.append(income_fixed_stables_data)
										
										# if single_facility["facility_booking_adults"] > 0:
										# 	## Income (Facility (Stables) Adult Bookings) ##
										# 	income_adult_stables_data = {}
										# 	income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										# 	income_adult_stables_data["customer_reference"] = customer_reference
										# 	income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# 	income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	income_adult_stables_data["vat"] = "V"
										# 	income_adult_stables_data["dc_marker"] = "C"
										# 	income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	income_adult_stables_data["journal_type"] = "VI"
										# 	income_adult_stables_data["account_description"] = single_booking.ticket
										# 	income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	income_adult_stables_data["base_amount"] = round(single_facility["base_adult"], 2)

										# 	data.append(income_adult_stables_data)

										# if single_facility["facility_booking_children"] > 0:
										# 	## Income (Facility (Stables) Children Bookings) ##
										# 	income_child_stables_data = {}
										# 	income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										# 	income_child_stables_data["customer_reference"] = customer_reference
										# 	income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	income_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# 	income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	income_child_stables_data["vat"] = "V"
										# 	income_child_stables_data["dc_marker"] = "C"
										# 	income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	income_child_stables_data["journal_type"] = "VI"
										# 	income_child_stables_data["account_description"] = single_booking.ticket
										# 	income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	income_child_stables_data["base_amount"] = round(single_facility["base_child"], 2)

										# 	data.append(income_child_stables_data)

										if single_facility["facility_booking_extra_adults"] > 0:
											## Income (Facility (Stables) Adult Bookings) ##
											income_adult_stables_data = {}
											income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
											income_adult_stables_data["customer_reference"] = customer_reference
											income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_adult_stables_data["vat"] = "V"
											income_adult_stables_data["dc_marker"] = "C"
											income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_stables_data["journal_type"] = "VI"
											income_adult_stables_data["account_description"] = single_booking.ticket
											income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult"], 2)

											data.append(income_adult_stables_data)

										if single_facility["facility_booking_extra_children"] > 0:
											## Income (Facility (Stables) Children Bookings) ##
											income_child_stables_data = {}
											income_child_stables_data["income_account"] = single_facility["facility_income_code"]
											income_child_stables_data["customer_reference"] = customer_reference
											income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_child_stables_data["vat"] = "V"
											income_child_stables_data["dc_marker"] = "C"
											income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_stables_data["journal_type"] = "VI"
											income_child_stables_data["account_description"] = single_booking.ticket
											income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_stables_data["base_amount"] = round(single_facility["base_extra_child"], 2)

											data.append(income_child_stables_data)

									if single_facility["catering_levy"]:
										if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
											## Income (Facility (Stables) Fixed Rate) ##
											income_fixed_stables_data = {}
											income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
											income_fixed_stables_data["customer_reference"] = customer_reference
											income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_fixed_stables_data["vat"] = "V"
											income_fixed_stables_data["dc_marker"] = "C"
											income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_fixed_stables_data["journal_type"] = "VI"
											income_fixed_stables_data["account_description"] = single_booking.ticket
											income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_cater"], 2)

											data.append(income_fixed_stables_data)

											## Catering Levy (Facility (Stables) Fixed Rate) ##
											cater_fixed_stables_data = {}
											cater_fixed_stables_data["income_account"] = "511006"
											cater_fixed_stables_data["customer_reference"] = customer_reference
											cater_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											cater_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											cater_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_fixed_stables_data["vat"] = "V"
											cater_fixed_stables_data["dc_marker"] = "C"
											cater_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_fixed_stables_data["journal_type"] = "VI"
											cater_fixed_stables_data["account_description"] = single_booking.ticket
											cater_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											cater_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_cater"], 2)

											data.append(cater_fixed_stables_data)
										
										# if single_facility["facility_booking_adults"] > 0:
										# 	## Income (Facility (Stables) Adult Bookings) ##
										# 	income_adult_stables_data = {}
										# 	income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										# 	income_adult_stables_data["customer_reference"] = customer_reference
										# 	income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# 	income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	income_adult_stables_data["vat"] = "V"
										# 	income_adult_stables_data["dc_marker"] = "C"
										# 	income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	income_adult_stables_data["journal_type"] = "VI"
										# 	income_adult_stables_data["account_description"] = single_booking.ticket
										# 	income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	income_adult_stables_data["base_amount"] = round(single_facility["base_adult_minus_cater"], 2)

										# 	data.append(income_adult_stables_data)

										# 	## Catering Levy (Facility (Stables) Adult Bookings) ##
										# 	cater_adult_stables_data = {}
										# 	cater_adult_stables_data["income_account"] = "511006"
										# 	cater_adult_stables_data["customer_reference"] = customer_reference
										# 	cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# 	cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	cater_adult_stables_data["vat"] = "V"
										# 	cater_adult_stables_data["dc_marker"] = "C"
										# 	cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	cater_adult_stables_data["journal_type"] = "VI"
										# 	cater_adult_stables_data["account_description"] = single_booking.ticket
										# 	cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	cater_adult_stables_data["base_amount"] = round(single_facility["base_adult_cater"], 2)

										# 	data.append(cater_adult_stables_data)

										# if single_facility["facility_booking_children"] > 0:
										# 	## Income (Facility (Stables) Children Bookings) ##
										# 	income_child_stables_data = {}
										# 	income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										# 	income_child_stables_data["customer_reference"] = customer_reference
										# 	income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	income_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# 	income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	income_child_stables_data["vat"] = "V"
										# 	income_child_stables_data["dc_marker"] = "C"
										# 	income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	income_child_stables_data["journal_type"] = "VI"
										# 	income_child_stables_data["account_description"] = single_booking.ticket
										# 	income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	income_child_stables_data["base_amount"] = round(single_facility["base_child_minus_cater"], 2)

										# 	data.append(income_child_stables_data)

										# 	## Catering Levy (Facility (Stables) Children Bookings) ##
										# 	cater_child_stables_data = {}
										# 	cater_child_stables_data["income_account"] = "511006"
										# 	cater_child_stables_data["customer_reference"] = customer_reference
										# 	cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										# 	cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										# 	cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# 	cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										# 	cater_child_stables_data["vat"] = "V"
										# 	cater_child_stables_data["dc_marker"] = "C"
										# 	cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										# 	cater_child_stables_data["journal_type"] = "VI"
										# 	cater_child_stables_data["account_description"] = single_booking.ticket
										# 	cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										# 	cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										# 	cater_child_stables_data["base_amount"] = round(single_facility["base_child_cater"], 2)

										# 	data.append(cater_child_stables_data)

										if single_facility["facility_booking_extra_adults"] > 0:
											## Income (Facility (Stables) Extra Adult Bookings) ##
											income_adult_stables_data = {}
											income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
											income_adult_stables_data["customer_reference"] = customer_reference
											income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_adult_stables_data["vat"] = "V"
											income_adult_stables_data["dc_marker"] = "C"
											income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_stables_data["journal_type"] = "VI"
											income_adult_stables_data["account_description"] = single_booking.ticket
											income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_cater"], 2)

											data.append(income_adult_stables_data)

											## Catering Levy (Facility (Stables) Extra Adult Bookings) ##
											cater_adult_stables_data = {}
											cater_adult_stables_data["income_account"] = "511006"
											cater_adult_stables_data["customer_reference"] = customer_reference
											cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_adult_stables_data["vat"] = "V"
											cater_adult_stables_data["dc_marker"] = "C"
											cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_adult_stables_data["journal_type"] = "VI"
											cater_adult_stables_data["account_description"] = single_booking.ticket
											cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_cater"], 2)

											data.append(cater_adult_stables_data)

										if single_facility["facility_booking_extra_children"] > 0:
											## Income (Facility (Stables) Extra Children Bookings) ##
											income_child_stables_data = {}
											income_child_stables_data["income_account"] = single_facility["facility_income_code"]
											income_child_stables_data["customer_reference"] = customer_reference
											income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_child_stables_data["vat"] = "V"
											income_child_stables_data["dc_marker"] = "C"
											income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_stables_data["journal_type"] = "VI"
											income_child_stables_data["account_description"] = single_booking.ticket
											income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_cater"], 2)

											data.append(income_child_stables_data)

											## Catering Levy (Facility (Stables) Extra Children Bookings) ##
											cater_child_stables_data = {}
											cater_child_stables_data["income_account"] = "511006"
											cater_child_stables_data["customer_reference"] = customer_reference
											cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_child_stables_data["vat"] = "V"
											cater_child_stables_data["dc_marker"] = "C"
											cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_child_stables_data["journal_type"] = "VI"
											cater_child_stables_data["account_description"] = single_booking.ticket
											cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_child_stables_data["base_amount"] = round(single_facility["base_extra_child_cater"], 2)

											data.append(cater_child_stables_data)

							elif single_facility["accomodation_type"] == "Pelican":
								if single_facility["vat"] & single_facility["catering_levy"]:
									if single_facility["facility_booking_adults"] > 0:
										## Income (Facility (Pelican) Adult Bookings) ##
										income_adult_pelican_data = {}
										income_adult_pelican_data["income_account"] = single_facility["facility_income_code"]
										income_adult_pelican_data["customer_reference"] = customer_reference
										income_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# income_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_adult_pelican_data["vat"] = "V"
										income_adult_pelican_data["dc_marker"] = "C"
										income_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_pelican_data["journal_type"] = "VI"
										income_adult_pelican_data["account_description"] = single_booking.ticket
										income_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_pelican_data["base_amount"] = round(single_facility["base_adult_minus_taxes"], 2)

										data.append(income_adult_pelican_data)

										## VAT (Facility (Pelican) Adult Bookings) ##
										vat_adult_pelican_data = {}
										vat_adult_pelican_data["income_account"] = "510001"
										vat_adult_pelican_data["customer_reference"] = customer_reference
										vat_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
										vat_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# vat_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_adult_pelican_data["vat"] = "V"
										vat_adult_pelican_data["dc_marker"] = "C"
										vat_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_adult_pelican_data["journal_type"] = "VI"
										vat_adult_pelican_data["account_description"] = single_booking.ticket
										vat_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
										vat_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_adult_pelican_data["base_amount"] = round(single_facility["base_adult_vat"], 2)

										data.append(vat_adult_pelican_data)

										## Catering Levy (Facility (Pelican) Adult Bookings) ##
										cater_adult_pelican_data = {}
										cater_adult_pelican_data["income_account"] = "511006"
										cater_adult_pelican_data["customer_reference"] = customer_reference
										cater_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
										cater_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
										# cater_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_adult_pelican_data["vat"] = "V"
										cater_adult_pelican_data["dc_marker"] = "C"
										cater_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_adult_pelican_data["journal_type"] = "VI"
										cater_adult_pelican_data["account_description"] = single_booking.ticket
										cater_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
										cater_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_adult_pelican_data["base_amount"] = round(single_facility["base_adult_cater"], 2)

										data.append(cater_adult_pelican_data)

									if single_facility["facility_booking_children"] > 0:
										## Income (Facility (Pelican) Children Bookings) ##
										income_child_pelican_data = {}
										income_child_pelican_data["income_account"] = single_facility["facility_income_code"]
										income_child_pelican_data["customer_reference"] = customer_reference
										income_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
										income_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# income_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_child_pelican_data["vat"] = "V"
										income_child_pelican_data["dc_marker"] = "C"
										income_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_pelican_data["journal_type"] = "VI"
										income_child_pelican_data["account_description"] = single_booking.ticket
										income_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_pelican_data["base_amount"] = round(single_facility["base_child_minus_taxes"], 2)

										data.append(income_child_pelican_data)

										## VAT (Facility (Pelican) Children Bookings) ##
										vat_child_pelican_data = {}
										vat_child_pelican_data["income_account"] = "510001"
										vat_child_pelican_data["customer_reference"] = customer_reference
										vat_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
										vat_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# vat_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_child_pelican_data["vat"] = "V"
										vat_child_pelican_data["dc_marker"] = "C"
										vat_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_child_pelican_data["journal_type"] = "VI"
										vat_child_pelican_data["account_description"] = single_booking.ticket
										vat_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
										vat_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_child_pelican_data["base_amount"] = round(single_facility["base_child_vat"], 2)

										data.append(vat_child_pelican_data)

										## Catering Levy (Facility (Pelican) Children Bookings) ##
										cater_child_pelican_data = {}
										cater_child_pelican_data["income_account"] = "511006"
										cater_child_pelican_data["customer_reference"] = customer_reference
										cater_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
										cater_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
										# cater_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_child_pelican_data["vat"] = "V"
										cater_child_pelican_data["dc_marker"] = "C"
										cater_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_child_pelican_data["journal_type"] = "VI"
										cater_child_pelican_data["account_description"] = single_booking.ticket
										cater_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
										cater_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_child_pelican_data["base_amount"] = round(single_facility["base_child_cater"], 2)

										data.append(cater_child_pelican_data)
								
								else:
									## TODO: Handle
									if single_facility["vat"]:
										pass
									else:
										pass

									## TODO: Handle
									if single_facility["catering_levy"]:
										pass

						elif single_facility["facility_type"] == "Camping Sites":
							## TODO: Handle
							if single_facility["vat"] & single_facility["catering_levy"]:
								pass
							
							else:
								if single_facility["vat"]:
									## Income (Facility (Campsites) Campsite Fee) ##
									income_camp_fee_data = {}
									income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
									income_camp_fee_data["customer_reference"] = customer_reference
									income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									income_camp_fee_data["vat"] = "V"
									income_camp_fee_data["dc_marker"] = "C"
									income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_camp_fee_data["journal_type"] = "VI"
									income_camp_fee_data["account_description"] = single_booking.ticket
									income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_minus_vat"], 2)

									data.append(income_camp_fee_data)

									## VAT (Facility (Campsites) Campsite Fee) ##
									vat_camp_fee_data = {}
									vat_camp_fee_data["income_account"] = "510001"
									vat_camp_fee_data["customer_reference"] = customer_reference
									vat_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									vat_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									vat_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									vat_camp_fee_data["vat"] = "V"
									vat_camp_fee_data["dc_marker"] = "C"
									vat_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_camp_fee_data["journal_type"] = "VI"
									vat_camp_fee_data["account_description"] = single_booking.ticket
									vat_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									vat_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_vat"], 2)

									data.append(vat_camp_fee_data)

									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
										## Income (Facility (Campsites) Adult Bookings) ##
										income_adult_campsite_data = {}
										income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
										income_adult_campsite_data["customer_reference"] = customer_reference
										income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
										income_adult_campsite_data["vat"] = "V"
										income_adult_campsite_data["dc_marker"] = "C"
										income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_campsite_data["journal_type"] = "VI"
										income_adult_campsite_data["account_description"] = single_booking.ticket
										income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_minus_vat"], 2)

										data.append(income_adult_campsite_data)

										## VAT (Facility (Campsites) Adult Bookings) ##
										vat_adult_campsite_data = {}
										vat_adult_campsite_data["income_account"] = "510001"
										vat_adult_campsite_data["customer_reference"] = customer_reference
										vat_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
										vat_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										vat_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
										vat_adult_campsite_data["vat"] = "V"
										vat_adult_campsite_data["dc_marker"] = "C"
										vat_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_adult_campsite_data["journal_type"] = "VI"
										vat_adult_campsite_data["account_description"] = single_booking.ticket
										vat_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										vat_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_vat"], 2)

										data.append(vat_adult_campsite_data)

									if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
										## Income (Facility (Campsites) Children Bookings) ##
										income_child_campsite_data = {}
										income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
										income_child_campsite_data["customer_reference"] = customer_reference
										income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
										income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
										income_child_campsite_data["vat"] = "V"
										income_child_campsite_data["dc_marker"] = "C"
										income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_campsite_data["journal_type"] = "VI"
										income_child_campsite_data["account_description"] = single_booking.ticket
										income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_minus_vat"], 2)

										data.append(income_child_campsite_data)

										## VAT (Facility (Campsites) Children Bookings) ##
										vat_child_campsite_data = {}
										vat_child_campsite_data["income_account"] = "510001"
										vat_child_campsite_data["customer_reference"] = customer_reference
										vat_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
										vat_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										vat_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
										vat_child_campsite_data["vat"] = "V"
										vat_child_campsite_data["dc_marker"] = "C"
										vat_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_child_campsite_data["journal_type"] = "VI"
										vat_child_campsite_data["account_description"] = single_booking.ticket
										vat_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										vat_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_vat"], 2)

										data.append(vat_child_campsite_data)
								
								else:
									## Income (Facility (Campsites) Campsite Fee) ##
									income_camp_fee_data = {}
									income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
									income_camp_fee_data["customer_reference"] = customer_reference
									income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									income_camp_fee_data["vat"] = "V"
									income_camp_fee_data["dc_marker"] = "C"
									income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_camp_fee_data["journal_type"] = "VI"
									income_camp_fee_data["account_description"] = single_booking.ticket
									income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost"], 2)

									data.append(income_camp_fee_data)

									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
										## Income (Facility (Campsites) Adult Bookings) ##
										income_adult_campsite_data = {}
										income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
										income_adult_campsite_data["customer_reference"] = customer_reference
										income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
										income_adult_campsite_data["vat"] = "V"
										income_adult_campsite_data["dc_marker"] = "C"
										income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_campsite_data["journal_type"] = "VI"
										income_adult_campsite_data["account_description"] = single_booking.ticket
										income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost"], 2)

										data.append(income_adult_campsite_data)

									if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
										## Income (Facility (Campsites) Children Bookings) ##
										income_child_campsite_data = {}
										income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
										income_child_campsite_data["customer_reference"] = customer_reference
										income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
										income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
										income_child_campsite_data["vat"] = "V"
										income_child_campsite_data["dc_marker"] = "C"
										income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_campsite_data["journal_type"] = "VI"
										income_child_campsite_data["account_description"] = single_booking.ticket
										income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost"], 2)

										data.append(income_child_campsite_data)

								## TODO: Handle
								if single_facility["catering_levy"]:
									pass
				
				booking_data = {}
				# try:
				# 	booking_data["income_account"] = return_data["code_kes"]
				# except Exception:
				# 	booking_data["income_account"] = "406GA001"
				
				booking_data["income_account"] = return_data["customer_code"]
				booking_data["customer_reference"] = customer_reference
				
				# booking_data["currency_code"] = single_booking.currency
				booking_data["currency"] = booking_currency.json()["data"][0]["currency_name"]
				
				booking_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
				booking_data["tran_quantity"] = 1
				booking_data["unit_price"] = round(return_data["total_cost"], 2)
				booking_data["dc_marker"] = "D"
				booking_data["booking_type"] = return_data["booking_type"]
				booking_data["partner_booking"] = return_data["partner_booking"]
				booking_data["guest_booking"] = return_data["guest_booking"]
				booking_data["gate_booking"] = return_data["gate_booking"]
				booking_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])

				booking_data["journal_type"] = "VI"
				booking_data["account_description"] = single_booking.ticket
				booking_data["outlet"] = "#"
				booking_data["department"] = "#"
				booking_data["base_amount"] = round(return_data["total_cost"], 2)

				data.append(booking_data)
				existing_bookings.append(single_booking.booking_public_id)

	select_transactions = db.session.query(Transaction)\
									.join(Booking, Transaction.booking_id == Booking.booking_public_id)\
									.join(Gatepass, Transaction.booking_id ==  Gatepass.booking_id)\
									.add_columns(Transaction.booking_id, Transaction.transaction_total, Transaction.transaction_total_currency,\
												 Transaction.transaction_payment_currency, Transaction.transaction_payment_method, Transaction.transaction_booking_public_id,\
												 Transaction.payment_currency_buying_rate_at_time, Transaction.payment_currency_selling_rate_at_time,\
												 Transaction.transaction_payment_gateway,\
												 Booking.booking_public_id, Booking.booking_ref_code, Booking.ticket,\
												 Booking.currency, Booking.booking_done_by, Booking.created_at,\
												 Gatepass.gatepass_public_id, Gatepass.gatepass_date)\
									.filter(Transaction.deletion_marker == None)\
									.filter(Booking.status != get_booking_status_id("Cancelled"))\
									.all()

	existing_transactions = []

	transaction_data = []
	for single_transaction in select_transactions:
		# if single_transaction.created_at.strftime("%Y-%m-%d") == datetime.now().strftime("%Y-%m-%d"):
		if single_transaction.transaction_booking_public_id in existing_transactions:
			pass
		else:
			if single_transaction.created_at.strftime("%Y-%m-%d") == "2019-04-03":
				if single_transaction.transaction_payment_method == "746bb582-89ed-4c83-ac3c-f25d9d60b266":
					pass
				else:
					general_booking_data = {}
					
					## Customer data
					customer_data = {}

					transaction_currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
					bookingTotal(general_booking_data, single_transaction.booking_id, True)

					customer_ref = single_transaction.booking_ref_code + " " + single_transaction.booking_done_by

					if len(customer_ref) >= 30:
						customer_reference = customer_ref[:29]
					else:
						customer_reference = customer_ref

					customer_data["booking_public_id"] = single_transaction.booking_id
					customer_data["sales_date"] = str(single_transaction.created_at.strftime("%d%m%Y"))
					customer_data["customer_reference"] = customer_reference
					customer_data["dc_marker"] = "C"
					customer_data["account_description"] = single_transaction.ticket
					customer_data["currency_code"] = single_transaction.currency
					customer_data["currency"] = transaction_currency.json()["data"][0]["currency_name"]
					customer_data["tran_quantity"] = 1
					customer_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
					customer_data["journal_type"] = "VI"
					customer_data["outlet"] = "#"
					customer_data["department"] = "#"
					customer_data["conversion_rate"] = float(single_transaction.payment_currency_selling_rate_at_time)
					
					# try:
					# 	customer_data["income_account"] = general_booking_data["code_kes"]
					# except Exception:
					# 	customer_data["income_account"] = "406GA001"

					customer_data["income_account"] = general_booking_data["customer_code"]
					
					transaction_data.append(customer_data)

					## Bank data
					bank_data = {}

					transaction_currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
					bookingTotal(general_booking_data, single_transaction.booking_id, True)

					bank_data["booking_public_id"] = single_transaction.booking_id
					bank_data["sales_date"] = str(single_transaction.created_at.strftime("%d%m%Y"))
					bank_data["customer_reference"] = customer_reference
					bank_data["dc_marker"] = "D"
					bank_data["account_description"] = single_transaction.ticket
					bank_data["currency_code"] = single_transaction.currency
					bank_data["currency"] = transaction_currency.json()["data"][0]["currency_name"]
					bank_data["tran_quantity"] = 1
					bank_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
					bank_data["journal_type"] = "VI"
					bank_data["outlet"] = "#"
					bank_data["department"] = "#"
					bank_data["conversion_rate"] = float(single_transaction.payment_currency_selling_rate_at_time)
					
					## TODO: Save payment gateways
					# Paybill
					if single_transaction.transaction_payment_method == "5550ff2f-bc48-4f0e-8944-d08f7f2b93fd":
						bank_data["income_account"] = "407003"
					# Mpesa
					elif single_transaction.transaction_payment_method == "cbff45f3-4f12-41da-8b4a-bf308112f032":
						bank_data["income_account"] = "407003"
					# Paypal
					elif single_transaction.transaction_payment_method == "653e10f6-168c-493e-bcbb-0219537c24f6":
						bank_data["income_account"] = "407009"
					# iVeri
					elif single_transaction.transaction_payment_method == "33ad20be-3a89-4deb-ab71-7510ba51677e":
						if transaction_currency.json()["data"][0]["currency_name"] == "KES":
							bank_data["income_account"] = "407016"
						elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
							bank_data["income_account"] = "407017"
					# Mpesa Online
					elif single_transaction.transaction_payment_method == "1c9d01cc-ef31-4757-a390-10c765fcecab":
						bank_data["income_account"] = "407003"
					else:
						if single_transaction.transaction_payment_gateway:
							get_gateway = db.session.query(PaymentGateway)\
													.filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
													.first()

							bank_data["income_account"] = get_gateway.payment_gateway_code
						else:
							if transaction_currency.json()["data"][0]["currency_name"] == "KES":
								bank_data["income_account"] = "407004"
							elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
								bank_data["income_account"] = "407005"

					transaction_data.append(bank_data)
					existing_transactions.append(single_transaction.transaction_booking_public_id)
	
	return jsonify({"data": data, "transation_data": transaction_data}), 200


## URL
@app.route("/batchfile/generate/data")
def generate_full_batchfile_data():
	start_date = request.json["start_date"]
	# end_date = datetime.now().strftime("%Y-%m-%d")
	end_date = request.json["end_date"]

	start = GenerateDateFromString.generateDate(start_date)
	end = GenerateDateFromString.generateDate(end_date)

	date_array = []

	while start <= end:
		date_array.append(start.strftime("%Y-%m-%d"))

		start = start + timedelta(days = 1)
	
	select_bookings = db.session.query(Booking)\
								.join(Gatepass, Booking.booking_public_id ==  Gatepass.booking_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_ref_code, Booking.ticket,\
											 Booking.currency, Booking.booking_done_by, Booking.created_at,\
											 Booking.booking_check_in_date,\
											 Gatepass.gatepass_public_id, Gatepass.gatepass_date)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.created_at != None)\
								.filter(Booking.status != get_booking_status_id("Deleted"))\
								.filter(Booking.status != get_booking_status_id("Cancelled"))\
								.filter(Booking.status != get_booking_status_id("Abandoned"))\
								.filter(Gatepass.deletion_marker == None)\
								.all()

	yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
	
	existing_bookings = []
	
	data = []
	for single_booking in select_bookings:
		for single_date in date_array:
			if single_booking.booking_public_id in existing_bookings:
				pass
			else:
				if single_booking.booking_check_in_date.strftime("%Y-%m-%d") == single_date:
					return_data = {}

					booking_currency = requests.get(get_currency.format(single_booking.currency))
					bookingTotal(return_data, single_booking.booking_public_id, True)

					check_school_booking = db.session.query(SchoolBooking)\
													 .join(School, SchoolBooking.school_id == School.school_public_id)\
													 .add_columns(School.school_name)\
													 .filter(SchoolBooking.deletion_marker == None)\
													 .filter(SchoolBooking.booking_id == single_booking.booking_public_id)\
													 .first()
					
					if check_school_booking:
						try:
							booking_done_by = check_school_booking.school_name
						except Exception:
							booking_done_by = single_booking.booking_done_by
					else:
						booking_done_by = single_booking.booking_done_by
					
					customer_ref = single_booking.booking_ref_code + " " + booking_done_by

					if len(customer_ref) >= 30:
						customer_reference = customer_ref[:29]
					else:
						customer_reference = customer_ref

					for single_gatepass_guest in return_data["guests"]:
						if single_gatepass_guest["payment_guests"] > 0:
							## Income (Guests) ##
							income_gatepass_data = {}
							income_gatepass_data["income_account"] = single_gatepass_guest["payment_person_income_code"]
							income_gatepass_data["customer_reference"] = customer_reference
							
							# income_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
							# income_gatepass_data["currency"] = single_gatepass_guest["currency"]
							income_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
							
							income_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							# income_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
							income_gatepass_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of people is already done
							income_gatepass_data["unit_price"] = round(single_gatepass_guest["price_minus_vat"], 2)
							income_gatepass_data["vat"] = "V"
							income_gatepass_data["dc_marker"] = "C"
							# income_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
							income_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							income_gatepass_data["journal_type"] = "VI"
							income_gatepass_data["account_description"] = single_booking.ticket
							income_gatepass_data["department"] = single_gatepass_guest["payment_person_dept_code"]
							income_gatepass_data["outlet"] = single_gatepass_guest["destination_outlet_code"]
							income_gatepass_data["base_amount"] = round(single_gatepass_guest["base_amount_minus_vat"], 2)

							data.append(income_gatepass_data)

							## VAT (Guests) ##
							vat_gatepass_data = {}
							vat_gatepass_data["income_account"] = "510001"
							vat_gatepass_data["customer_reference"] = customer_reference
							
							# vat_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
							# vat_gatepass_data["currency"] = single_gatepass_guest["currency"]
							vat_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
							
							vat_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							# vat_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
							vat_gatepass_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of people is already done
							vat_gatepass_data["unit_price"] = round(single_gatepass_guest["vat"], 2)
							vat_gatepass_data["vat"] = "V"
							vat_gatepass_data["dc_marker"] = "C"
							# vat_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
							vat_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							vat_gatepass_data["journal_type"] = "VI"
							vat_gatepass_data["account_description"] = single_booking.ticket
							vat_gatepass_data["outlet"] = "#"
							vat_gatepass_data["department"] = "#"
							vat_gatepass_data["base_amount"] = round(single_gatepass_guest["base_vat"], 2)

							data.append(vat_gatepass_data)

					for single_gatepass_vehicle in return_data["vehicles"]:
						if single_gatepass_vehicle["vehicles"] > 0:
							## Income (Vehicles) ##
							income_gatepass_vehicle_data = {}
							income_gatepass_vehicle_data["income_account"] = single_gatepass_vehicle["vehicle_charge_income_code"]
							income_gatepass_vehicle_data["customer_reference"] = customer_reference
							
							# income_gatepass_vehicle_data["currency_code"] = single_gatepass_vehicle["gatepass_vehicle_currency_at_time"]
							# income_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["currency"]
							income_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["vehicle_charge_cost_currency"]
							
							income_gatepass_vehicle_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							# income_gatepass_vehicle_data["tran_quantity"] = single_gatepass_vehicle["vehicles"]
							income_gatepass_vehicle_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of vehicles is already done
							income_gatepass_vehicle_data["unit_price"] = round(single_gatepass_vehicle["price_minus_vat"], 2)
							income_gatepass_vehicle_data["vat"] = "V"
							income_gatepass_vehicle_data["dc_marker"] = "C"
							# income_gatepass_vehicle_data["conversion_rate"] = single_gatepass_vehicle["gatepass_vehicle_rate_at_time"]
							income_gatepass_vehicle_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							income_gatepass_vehicle_data["journal_type"] = "VI"
							income_gatepass_vehicle_data["account_description"] = single_booking.ticket
							income_gatepass_vehicle_data["outlet"] = single_gatepass_vehicle["vehicle_charge_outlet_code"]
							income_gatepass_vehicle_data["department"] = single_gatepass_vehicle["vehicle_charge_dept_code"]
							income_gatepass_vehicle_data["base_amount"] = round(single_gatepass_vehicle["base_amount_minus_vat"], 2)

							data.append(income_gatepass_vehicle_data)

							## VAT (Vehicles) ##
							vat_gatepass_vehicle_data = {}
							vat_gatepass_vehicle_data["income_account"] = "510001"
							vat_gatepass_vehicle_data["customer_reference"] = customer_reference
							
							# vat_gatepass_vehicle_data["currency_code"] = single_gatepass_vehicle["gatepass_vehicle_currency_at_time"]
							# vat_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["currency"]
							vat_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["vehicle_charge_cost_currency"]
							
							vat_gatepass_vehicle_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							# vat_gatepass_vehicle_data["tran_quantity"] = single_gatepass_vehicle["vehicles"]
							vat_gatepass_vehicle_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of vehicles is already done
							vat_gatepass_vehicle_data["unit_price"] = round(single_gatepass_vehicle["vat"], 2)
							vat_gatepass_vehicle_data["vat"] = "V"
							vat_gatepass_vehicle_data["dc_marker"] = "C"
							# vat_gatepass_vehicle_data["conversion_rate"] = single_gatepass_vehicle["gatepass_vehicle_rate_at_time"]
							vat_gatepass_vehicle_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							vat_gatepass_vehicle_data["journal_type"] = "VI"
							vat_gatepass_vehicle_data["account_description"] = single_booking.ticket
							vat_gatepass_vehicle_data["outlet"] = "#"
							vat_gatepass_vehicle_data["department"] = "#"
							vat_gatepass_vehicle_data["base_amount"] = round(single_gatepass_vehicle["base_vat"], 2)

							data.append(vat_gatepass_vehicle_data)

					# data.append(return_data["inventory_bookings"])
					
					if return_data["inventory_bookings"]:
						for single_inventory in return_data["inventory_bookings"]:
							if single_inventory["inventory_booking_adults"] > 0:
								if single_inventory["vat"] & single_inventory["catering_levy"]:
									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_minus_taxes"], 2)
									income_adult_inventory_data["vat"] = "V"
									income_adult_inventory_data["dc_marker"] = "C"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_taxes"], 2)

									data.append(income_adult_inventory_data)

									## VAT (Inventory Adult Bookings) ##
									vat_adult_inventory_data = {}
									vat_adult_inventory_data["income_account"] = "510001"
									vat_adult_inventory_data["customer_reference"] = customer_reference
									
									# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_adult_inventory_data["currency"] = single_inventory["currency"]
									vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
									vat_adult_inventory_data["vat"] = "V"
									vat_adult_inventory_data["dc_marker"] = "C"
									# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_inventory_data["journal_type"] = "VI"
									vat_adult_inventory_data["account_description"] = single_booking.ticket
									vat_adult_inventory_data["outlet"] = "#"
									vat_adult_inventory_data["department"] = "#"
									vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

									data.append(vat_adult_inventory_data)

									## Catering Levy (Inventory Adult Bookings) ##
									cater_adult_inventory_data = {}
									cater_adult_inventory_data["income_account"] = "511006"
									cater_adult_inventory_data["customer_reference"] = customer_reference
									
									# cater_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# cater_adult_inventory_data["currency"] = single_inventory["currency"]
									cater_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									cater_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									cater_adult_inventory_data["unit_price"] = round(single_inventory["adult_cater"], 2)
									cater_adult_inventory_data["vat"] = None
									cater_adult_inventory_data["dc_marker"] = "C"
									# cater_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									cater_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_adult_inventory_data["journal_type"] = "VI"
									cater_adult_inventory_data["account_description"] = single_booking.ticket
									cater_adult_inventory_data["outlet"] = "#"
									cater_adult_inventory_data["department"] = "#"
									cater_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_cater"], 2)

									data.append(cater_adult_inventory_data)

								else:						
									if single_inventory["vat"]:
										## Income (Inventory Adult Bookings) ##
										income_adult_inventory_data = {}
										income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
										income_adult_inventory_data["customer_reference"] = customer_reference
										
										# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# income_adult_inventory_data["currency"] = single_inventory["currency"]
										income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
										income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_vat"], 2)
										income_adult_inventory_data["vat"] = "V"
										income_adult_inventory_data["dc_marker"] = "C"
										# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_inventory_data["journal_type"] = "VI"
										income_adult_inventory_data["account_description"] = single_booking.ticket
										income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
										income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
										income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_vat"], 2)

										data.append(income_adult_inventory_data)

										## VAT (Inventory Adult Bookings) ##
										vat_adult_inventory_data = {}
										vat_adult_inventory_data["income_account"] = "510001"
										vat_adult_inventory_data["customer_reference"] = customer_reference
										
										# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# vat_adult_inventory_data["currency"] = single_inventory["currency"]
										vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
										vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
										vat_adult_inventory_data["vat"] = "V"
										vat_adult_inventory_data["dc_marker"] = "C"
										# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_adult_inventory_data["journal_type"] = "VI"
										vat_adult_inventory_data["account_description"] = single_booking.ticket
										vat_adult_inventory_data["outlet"] = "#"
										vat_adult_inventory_data["department"] = "#"
										vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

										data.append(vat_adult_inventory_data)

									else:
										## Income (Inventory Adult Bookings) ##
										income_adult_inventory_data = {}
										income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
										income_adult_inventory_data["customer_reference"] = customer_reference
										
										# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# income_adult_inventory_data["currency"] = single_inventory["currency"]
										income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
										income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price"], 2)
										income_adult_inventory_data["vat"] = "V"
										income_adult_inventory_data["dc_marker"] = "C"
										# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_inventory_data["journal_type"] = "VI"
										income_adult_inventory_data["account_description"] = single_booking.ticket
										income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
										income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
										income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price"], 2)

										data.append(income_adult_inventory_data)

									if single_inventory["catering_levy"]:
										## Income (Inventory Adult Bookings) ##
										income_adult_inventory_data = {}
										income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
										income_adult_inventory_data["customer_reference"] = customer_reference
										
										# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# income_adult_inventory_data["currency"] = single_inventory["currency"]
										income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
										income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_cater"], 2)
										income_adult_inventory_data["vat"] = None
										income_adult_inventory_data["dc_marker"] = "C"
										# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_inventory_data["journal_type"] = "VI"
										income_adult_inventory_data["account_description"] = single_booking.ticket
										income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
										income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
										income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_cater"], 2)

										data.append(income_adult_inventory_data)

										## Catering Levy (Inventory Adult Bookings) ##
										cater_adult_inventory_data = {}
										cater_adult_inventory_data["income_account"] = "511006"
										cater_adult_inventory_data["customer_reference"] = customer_reference
										
										# cater_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# cater_adult_inventory_data["currency"] = single_inventory["currency"]
										cater_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										cater_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
										cater_adult_inventory_data["unit_price"] = round(single_inventory["adult_cater"], 2)
										cater_adult_inventory_data["vat"] = None
										cater_adult_inventory_data["dc_marker"] = "C"
										# cater_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										cater_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_adult_inventory_data["journal_type"] = "VI"
										cater_adult_inventory_data["account_description"] = single_booking.ticket
										cater_adult_inventory_data["outlet"] = "#"
										cater_adult_inventory_data["department"] = "#"
										cater_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_cater"], 2)

										data.append(cater_adult_inventory_data)
					
							if single_inventory["inventory_booking_children"] > 0:
								if single_inventory["vat"] & single_inventory["catering_levy"]:
									## Income (Inventory Child Bookings) ##
									income_child_inventory_data = {}
									income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_child_inventory_data["customer_reference"] = customer_reference
									
									# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_child_inventory_data["currency"] = single_inventory["currency"]
									income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									income_child_inventory_data["unit_price"] = round(single_inventory["child_minus_taxes"], 2)
									income_child_inventory_data["vat"] = "V"
									income_child_inventory_data["dc_marker"] = "C"
									# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_inventory_data["journal_type"] = "VI"
									income_child_inventory_data["account_description"] = single_booking.ticket
									income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_taxes"], 2)

									data.append(income_child_inventory_data)

									## VAT (VAT Child Bookings) ##
									vat_child_inventory_data = {}
									vat_child_inventory_data["income_account"] = "510001"
									vat_child_inventory_data["customer_reference"] = customer_reference
									
									# vat_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_child_inventory_data["currency"] = single_inventory["currency"]
									vat_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									vat_child_inventory_data["unit_price"] = round(single_inventory["child_vat"], 2)
									vat_child_inventory_data["vat"] = "V"
									vat_child_inventory_data["dc_marker"] = "C"
									# vat_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_child_inventory_data["journal_type"] = "VI"
									vat_child_inventory_data["account_description"] = single_booking.ticket
									vat_child_inventory_data["outlet"] = "#"
									vat_child_inventory_data["department"] = "#"
									vat_child_inventory_data["base_amount"] = round(single_inventory["base_child_vat"], 2)

									data.append(vat_child_inventory_data)

									## Catering Levy (Inventory Child Bookings) ##
									cater_child_inventory_data = {}
									cater_child_inventory_data["income_account"] = "511006"
									cater_child_inventory_data["customer_reference"] = customer_reference
									
									# cater_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# cater_child_inventory_data["currency"] = single_inventory["currency"]
									cater_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									cater_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
									cater_child_inventory_data["unit_price"] = round(single_inventory["child_cater"], 2)
									cater_child_inventory_data["vat"] = None
									cater_child_inventory_data["dc_marker"] = "C"
									# cater_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									cater_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_child_inventory_data["journal_type"] = "VI"
									cater_child_inventory_data["account_description"] = single_booking.ticket
									cater_child_inventory_data["outlet"] = "#"
									cater_child_inventory_data["department"] = "#"
									cater_child_inventory_data["base_amount"] = round(single_inventory["base_child_cater"], 2)

									data.append(cater_child_inventory_data)
								
								else:
									if single_inventory["vat"]:
										## Income (Inventory Child Bookings) ##
										income_child_inventory_data = {}
										income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
										income_child_inventory_data["customer_reference"] = customer_reference
										
										# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# income_child_inventory_data["currency"] = single_inventory["currency"]
										income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
										income_child_inventory_data["unit_price"] = round(single_inventory["child_price_minus_vat"], 2)
										income_child_inventory_data["vat"] = "V"
										income_child_inventory_data["dc_marker"] = "C"
										# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_inventory_data["journal_type"] = "VI"
										income_child_inventory_data["account_description"] = single_booking.ticket
										income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
										income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
										income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_vat"], 2)

										data.append(income_child_inventory_data)

										## VAT (VAT Child Bookings) ##
										vat_child_inventory_data = {}
										vat_child_inventory_data["income_account"] = "510001"
										vat_child_inventory_data["customer_reference"] = customer_reference
										
										# vat_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# vat_child_inventory_data["currency"] = single_inventory["currency"]
										vat_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										vat_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
										vat_child_inventory_data["unit_price"] = round(single_inventory["child_vat"], 2)
										vat_child_inventory_data["vat"] = "V"
										vat_child_inventory_data["dc_marker"] = "C"
										# vat_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										vat_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_child_inventory_data["journal_type"] = "VI"
										vat_child_inventory_data["account_description"] = single_booking.ticket
										vat_child_inventory_data["outlet"] = "#"
										vat_child_inventory_data["department"] = "#"
										vat_child_inventory_data["base_amount"] = round(single_inventory["base_child_vat"], 2)

										data.append(vat_child_inventory_data)

									else:
										## Income (Inventory Child Bookings) ##
										income_child_inventory_data = {}
										income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
										income_child_inventory_data["customer_reference"] = customer_reference
										
										# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# income_child_inventory_data["currency"] = single_inventory["currency"]
										income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
										income_child_inventory_data["unit_price"] = round(single_inventory["child_price"], 2)
										income_child_inventory_data["vat"] = "V"
										income_child_inventory_data["dc_marker"] = "C"
										# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_inventory_data["journal_type"] = "VI"
										income_child_inventory_data["account_description"] = single_booking.ticket
										income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
										income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
										income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price"], 2)

										data.append(income_child_inventory_data)
									
									if single_inventory["catering_levy"]:
										## Income (Inventory Child Bookings) ##
										income_child_inventory_data = {}
										income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
										income_child_inventory_data["customer_reference"] = customer_reference
										
										# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# income_child_inventory_data["currency"] = single_inventory["currency"]
										income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
										income_child_inventory_data["unit_price"] = round(single_inventory["child_price_minus_cater"], 2)
										income_child_inventory_data["vat"] = None
										income_child_inventory_data["dc_marker"] = "C"
										# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_inventory_data["journal_type"] = "VI"
										income_child_inventory_data["account_description"] = single_booking.ticket
										income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
										income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
										income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_cater"], 2)

										data.append(income_child_inventory_data)

										## Catering Levy (Inventory Child Bookings) ##
										cater_child_inventory_data = {}
										cater_child_inventory_data["income_account"] = "511006"
										cater_child_inventory_data["customer_reference"] = customer_reference
										
										# cater_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
										# cater_child_inventory_data["currency"] = single_inventory["currency"]
										cater_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
										
										cater_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
										cater_child_inventory_data["unit_price"] = round(single_inventory["child_cater"], 2)
										cater_child_inventory_data["vat"] = None
										cater_child_inventory_data["dc_marker"] = "C"
										# cater_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
										cater_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_child_inventory_data["journal_type"] = "VI"
										cater_child_inventory_data["account_description"] = single_booking.ticket
										cater_child_inventory_data["outlet"] = "#"
										cater_child_inventory_data["department"] = "#"
										cater_child_inventory_data["base_amount"] = round(single_inventory["base_child_cater"], 2)

										data.append(cater_child_inventory_data)
					
					if return_data["facility_bookings"]:
						for single_facility in return_data["facility_bookings"]:
							if single_facility["facility_type"] == "Accomodation":
								if single_facility["accomodation_type"] == "Stables":
									if single_facility["vat"] & single_facility["catering_levy"]:
										if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
											## Income (Facility (Stables) Fixed Rate) ##
											income_fixed_stables_data = {}
											income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
											income_fixed_stables_data["customer_reference"] = customer_reference
											income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_fixed_stables_data["vat"] = "V"
											income_fixed_stables_data["dc_marker"] = "C"
											income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_fixed_stables_data["journal_type"] = "VI"
											income_fixed_stables_data["account_description"] = single_booking.ticket
											income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_taxes"], 2)

											data.append(income_fixed_stables_data)

											## VAT (Facility (Stables) Fixed Rate) ##
											vat_fixed_stables_data = {}
											vat_fixed_stables_data["income_account"] = "510001"
											vat_fixed_stables_data["customer_reference"] = customer_reference
											vat_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											vat_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											vat_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_fixed_stables_data["vat"] = "V"
											vat_fixed_stables_data["dc_marker"] = "C"
											vat_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_fixed_stables_data["journal_type"] = "VI"
											vat_fixed_stables_data["account_description"] = single_booking.ticket
											vat_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											vat_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_vat"], 2)

											data.append(vat_fixed_stables_data)

											## Catering Levy (Facility (Stables) Fixed Rate) ##
											cater_fixed_stables_data = {}
											cater_fixed_stables_data["income_account"] = "511006"
											cater_fixed_stables_data["customer_reference"] = customer_reference
											cater_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
											cater_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
											cater_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_fixed_stables_data["vat"] = "V"
											cater_fixed_stables_data["dc_marker"] = "C"
											cater_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_fixed_stables_data["journal_type"] = "VI"
											cater_fixed_stables_data["account_description"] = single_booking.ticket
											cater_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
											cater_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_cater"], 2)

											data.append(cater_fixed_stables_data)
										
										if single_facility["facility_booking_extra_adults"] > 0:
											## Income (Facility (Stables) Extra Adult Bookings) ##
											income_adult_stables_data = {}
											income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
											income_adult_stables_data["customer_reference"] = customer_reference
											income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_adult_stables_data["vat"] = "V"
											income_adult_stables_data["dc_marker"] = "C"
											income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_stables_data["journal_type"] = "VI"
											income_adult_stables_data["account_description"] = single_booking.ticket
											income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_taxes"], 2)

											data.append(income_adult_stables_data)

											## VAT (Facility (Stables) Extra Adult Bookings) ##
											vat_adult_stables_data = {}
											vat_adult_stables_data["income_account"] = "510001"
											vat_adult_stables_data["customer_reference"] = customer_reference
											vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_adult_stables_data["vat"] = "V"
											vat_adult_stables_data["dc_marker"] = "C"
											vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_adult_stables_data["journal_type"] = "VI"
											vat_adult_stables_data["account_description"] = single_booking.ticket
											vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_vat"], 2)

											data.append(vat_adult_stables_data)

											## Catering Levy (Facility (Stables) Extra Adult Bookings) ##
											cater_adult_stables_data = {}
											cater_adult_stables_data["income_account"] = "511006"
											cater_adult_stables_data["customer_reference"] = customer_reference
											cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
											cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
											cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_adult_stables_data["vat"] = "V"
											cater_adult_stables_data["dc_marker"] = "C"
											cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_adult_stables_data["journal_type"] = "VI"
											cater_adult_stables_data["account_description"] = single_booking.ticket
											cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
											cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_cater"], 2)

											data.append(cater_adult_stables_data)

										if single_facility["facility_booking_extra_children"] > 0:
											## Income (Facility (Stables) Extra Children Bookings) ##
											income_child_stables_data = {}
											income_child_stables_data["income_account"] = single_facility["facility_income_code"]
											income_child_stables_data["customer_reference"] = customer_reference
											income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_child_stables_data["vat"] = "V"
											income_child_stables_data["dc_marker"] = "C"
											income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_stables_data["journal_type"] = "VI"
											income_child_stables_data["account_description"] = single_booking.ticket
											income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_taxes"], 2)

											data.append(income_child_stables_data)

											## VAT (Facility (Stables) Extra Children Bookings) ##
											vat_child_stables_data = {}
											vat_child_stables_data["income_account"] = "510001"
											vat_child_stables_data["customer_reference"] = customer_reference
											vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_child_stables_data["vat"] = "V"
											vat_child_stables_data["dc_marker"] = "C"
											vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_child_stables_data["journal_type"] = "VI"
											vat_child_stables_data["account_description"] = single_booking.ticket
											vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_child_stables_data["base_amount"] = round(single_facility["base_extra_child_vat"], 2)

											data.append(vat_child_stables_data)

											## Catering Levy (Facility (Stables) Extra Children Bookings) ##
											cater_child_stables_data = {}
											cater_child_stables_data["income_account"] = "511006"
											cater_child_stables_data["customer_reference"] = customer_reference
											cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
											cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
											cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_child_stables_data["vat"] = "V"
											cater_child_stables_data["dc_marker"] = "C"
											cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_child_stables_data["journal_type"] = "VI"
											cater_child_stables_data["account_description"] = single_booking.ticket
											cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
											cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_child_stables_data["base_amount"] = round(single_facility["base_extra_child_cater"], 2)

											data.append(cater_child_stables_data)
									
									else:
										if single_facility["vat"]:
											if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
												## Income (Facility (Stables) Fixed Rate) ##
												income_fixed_stables_data = {}
												income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
												income_fixed_stables_data["customer_reference"] = customer_reference
												income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
												income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_fixed_stables_data["vat"] = "V"
												income_fixed_stables_data["dc_marker"] = "C"
												income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_fixed_stables_data["journal_type"] = "VI"
												income_fixed_stables_data["account_description"] = single_booking.ticket
												income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_vat"], 2)

												data.append(income_fixed_stables_data)

												## VAT (Facility (Stables) Fixed Rate) ##
												vat_fixed_stables_data = {}
												vat_fixed_stables_data["income_account"] = "510001"
												vat_fixed_stables_data["customer_reference"] = customer_reference
												vat_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
												vat_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												vat_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
												vat_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												vat_fixed_stables_data["vat"] = "V"
												vat_fixed_stables_data["dc_marker"] = "C"
												vat_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												vat_fixed_stables_data["journal_type"] = "VI"
												vat_fixed_stables_data["account_description"] = single_booking.ticket
												vat_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
												vat_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												vat_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_vat"], 2)

												data.append(vat_fixed_stables_data)
											
											if single_facility["facility_booking_extra_adults"] > 0:
												## Income (Facility (Stables) Extra Adult Bookings) ##
												income_adult_stables_data = {}
												income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
												income_adult_stables_data["customer_reference"] = customer_reference
												income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
												income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_adult_stables_data["vat"] = "V"
												income_adult_stables_data["dc_marker"] = "C"
												income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_adult_stables_data["journal_type"] = "VI"
												income_adult_stables_data["account_description"] = single_booking.ticket
												income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_vat"], 2)

												data.append(income_adult_stables_data)

												## VAT (Facility (Stables) Extra Adult Bookings) ##
												vat_adult_stables_data = {}
												vat_adult_stables_data["income_account"] = "510001"
												vat_adult_stables_data["customer_reference"] = customer_reference
												vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
												vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
												vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												vat_adult_stables_data["vat"] = "V"
												vat_adult_stables_data["dc_marker"] = "C"
												vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												vat_adult_stables_data["journal_type"] = "VI"
												vat_adult_stables_data["account_description"] = single_booking.ticket
												vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
												vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												vat_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_vat"], 2)

												data.append(vat_adult_stables_data)

											if single_facility["facility_booking_extra_children"] > 0:
												## Income (Facility (Stables) Extra Children Bookings) ##
												income_child_stables_data = {}
												income_child_stables_data["income_account"] = single_facility["facility_income_code"]
												income_child_stables_data["customer_reference"] = customer_reference
												income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
												income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_child_stables_data["vat"] = "V"
												income_child_stables_data["dc_marker"] = "C"
												income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_child_stables_data["journal_type"] = "VI"
												income_child_stables_data["account_description"] = single_booking.ticket
												income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_vat"], 2)

												data.append(income_child_stables_data)

												## VAT (Facility (Stables) Extra Children Bookings) ##
												vat_child_stables_data = {}
												vat_child_stables_data["income_account"] = "510001"
												vat_child_stables_data["customer_reference"] = customer_reference
												vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
												vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
												vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												vat_child_stables_data["vat"] = "V"
												vat_child_stables_data["dc_marker"] = "C"
												vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												vat_child_stables_data["journal_type"] = "VI"
												vat_child_stables_data["account_description"] = single_booking.ticket
												vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
												vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												vat_child_stables_data["base_amount"] = round(single_facility["base_extra_child_vat"], 2)

												data.append(vat_child_stables_data)
										else:
											if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
												## Income (Facility (Stables) Fixed Rate) ##
												income_fixed_stables_data = {}
												income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
												income_fixed_stables_data["customer_reference"] = customer_reference
												income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
												income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_fixed_stables_data["vat"] = "V"
												income_fixed_stables_data["dc_marker"] = "C"
												income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_fixed_stables_data["journal_type"] = "VI"
												income_fixed_stables_data["account_description"] = single_booking.ticket
												income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed"], 2)

												data.append(income_fixed_stables_data)
											
											if single_facility["facility_booking_extra_adults"] > 0:
												## Income (Facility (Stables) Adult Bookings) ##
												income_adult_stables_data = {}
												income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
												income_adult_stables_data["customer_reference"] = customer_reference
												income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
												income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_adult_stables_data["vat"] = "V"
												income_adult_stables_data["dc_marker"] = "C"
												income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_adult_stables_data["journal_type"] = "VI"
												income_adult_stables_data["account_description"] = single_booking.ticket
												income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult"], 2)

												data.append(income_adult_stables_data)

											if single_facility["facility_booking_extra_children"] > 0:
												## Income (Facility (Stables) Children Bookings) ##
												income_child_stables_data = {}
												income_child_stables_data["income_account"] = single_facility["facility_income_code"]
												income_child_stables_data["customer_reference"] = customer_reference
												income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
												income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_child_stables_data["vat"] = "V"
												income_child_stables_data["dc_marker"] = "C"
												income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_child_stables_data["journal_type"] = "VI"
												income_child_stables_data["account_description"] = single_booking.ticket
												income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_child_stables_data["base_amount"] = round(single_facility["base_extra_child"], 2)

												data.append(income_child_stables_data)

										if single_facility["catering_levy"]:
											if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
												## Income (Facility (Stables) Fixed Rate) ##
												income_fixed_stables_data = {}
												income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
												income_fixed_stables_data["customer_reference"] = customer_reference
												income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
												income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_fixed_stables_data["vat"] = "V"
												income_fixed_stables_data["dc_marker"] = "C"
												income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_fixed_stables_data["journal_type"] = "VI"
												income_fixed_stables_data["account_description"] = single_booking.ticket
												income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_cater"], 2)

												data.append(income_fixed_stables_data)

												## Catering Levy (Facility (Stables) Fixed Rate) ##
												cater_fixed_stables_data = {}
												cater_fixed_stables_data["income_account"] = "511006"
												cater_fixed_stables_data["customer_reference"] = customer_reference
												cater_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
												cater_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												cater_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
												cater_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												cater_fixed_stables_data["vat"] = "V"
												cater_fixed_stables_data["dc_marker"] = "C"
												cater_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												cater_fixed_stables_data["journal_type"] = "VI"
												cater_fixed_stables_data["account_description"] = single_booking.ticket
												cater_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
												cater_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												cater_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_cater"], 2)

												data.append(cater_fixed_stables_data)
											
											if single_facility["facility_booking_extra_adults"] > 0:
												## Income (Facility (Stables) Extra Adult Bookings) ##
												income_adult_stables_data = {}
												income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
												income_adult_stables_data["customer_reference"] = customer_reference
												income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
												income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_adult_stables_data["vat"] = "V"
												income_adult_stables_data["dc_marker"] = "C"
												income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_adult_stables_data["journal_type"] = "VI"
												income_adult_stables_data["account_description"] = single_booking.ticket
												income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_cater"], 2)

												data.append(income_adult_stables_data)

												## Catering Levy (Facility (Stables) Extra Adult Bookings) ##
												cater_adult_stables_data = {}
												cater_adult_stables_data["income_account"] = "511006"
												cater_adult_stables_data["customer_reference"] = customer_reference
												cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
												cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
												cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												cater_adult_stables_data["vat"] = "V"
												cater_adult_stables_data["dc_marker"] = "C"
												cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												cater_adult_stables_data["journal_type"] = "VI"
												cater_adult_stables_data["account_description"] = single_booking.ticket
												cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
												cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												cater_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_cater"], 2)

												data.append(cater_adult_stables_data)

											if single_facility["facility_booking_extra_children"] > 0:
												## Income (Facility (Stables) Extra Children Bookings) ##
												income_child_stables_data = {}
												income_child_stables_data["income_account"] = single_facility["facility_income_code"]
												income_child_stables_data["customer_reference"] = customer_reference
												income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
												income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
												income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												income_child_stables_data["vat"] = "V"
												income_child_stables_data["dc_marker"] = "C"
												income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												income_child_stables_data["journal_type"] = "VI"
												income_child_stables_data["account_description"] = single_booking.ticket
												income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
												income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_cater"], 2)

												data.append(income_child_stables_data)

												## Catering Levy (Facility (Stables) Extra Children Bookings) ##
												cater_child_stables_data = {}
												cater_child_stables_data["income_account"] = "511006"
												cater_child_stables_data["customer_reference"] = customer_reference
												cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
												cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
												cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
												cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
												cater_child_stables_data["vat"] = "V"
												cater_child_stables_data["dc_marker"] = "C"
												cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
												cater_child_stables_data["journal_type"] = "VI"
												cater_child_stables_data["account_description"] = single_booking.ticket
												cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
												cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
												cater_child_stables_data["base_amount"] = round(single_facility["base_extra_child_cater"], 2)

												data.append(cater_child_stables_data)

								elif single_facility["accomodation_type"] == "Pelican":
									if single_facility["vat"] & single_facility["catering_levy"]:
										if single_facility["facility_booking_adults"] > 0:
											## Income (Facility (Pelican) Adult Bookings) ##
											income_adult_pelican_data = {}
											income_adult_pelican_data["income_account"] = single_facility["facility_income_code"]
											income_adult_pelican_data["customer_reference"] = customer_reference
											income_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
											# income_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_adult_pelican_data["vat"] = "V"
											income_adult_pelican_data["dc_marker"] = "C"
											income_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_pelican_data["journal_type"] = "VI"
											income_adult_pelican_data["account_description"] = single_booking.ticket
											income_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_pelican_data["base_amount"] = round(single_facility["base_adult_minus_taxes"], 2)

											data.append(income_adult_pelican_data)

											## VAT (Facility (Pelican) Adult Bookings) ##
											vat_adult_pelican_data = {}
											vat_adult_pelican_data["income_account"] = "510001"
											vat_adult_pelican_data["customer_reference"] = customer_reference
											vat_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
											vat_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
											# vat_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_adult_pelican_data["vat"] = "V"
											vat_adult_pelican_data["dc_marker"] = "C"
											vat_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_adult_pelican_data["journal_type"] = "VI"
											vat_adult_pelican_data["account_description"] = single_booking.ticket
											vat_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
											vat_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_adult_pelican_data["base_amount"] = round(single_facility["base_adult_vat"], 2)

											data.append(vat_adult_pelican_data)

											## Catering Levy (Facility (Pelican) Adult Bookings) ##
											cater_adult_pelican_data = {}
											cater_adult_pelican_data["income_account"] = "511006"
											cater_adult_pelican_data["customer_reference"] = customer_reference
											cater_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
											cater_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
											# cater_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_adult_pelican_data["vat"] = "V"
											cater_adult_pelican_data["dc_marker"] = "C"
											cater_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_adult_pelican_data["journal_type"] = "VI"
											cater_adult_pelican_data["account_description"] = single_booking.ticket
											cater_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
											cater_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_adult_pelican_data["base_amount"] = round(single_facility["base_adult_cater"], 2)

											data.append(cater_adult_pelican_data)

										if single_facility["facility_booking_children"] > 0:
											## Income (Facility (Pelican) Children Bookings) ##
											income_child_pelican_data = {}
											income_child_pelican_data["income_account"] = single_facility["facility_income_code"]
											income_child_pelican_data["customer_reference"] = customer_reference
											income_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
											income_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
											# income_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											income_child_pelican_data["vat"] = "V"
											income_child_pelican_data["dc_marker"] = "C"
											income_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_pelican_data["journal_type"] = "VI"
											income_child_pelican_data["account_description"] = single_booking.ticket
											income_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_pelican_data["base_amount"] = round(single_facility["base_child_minus_taxes"], 2)

											data.append(income_child_pelican_data)

											## VAT (Facility (Pelican) Children Bookings) ##
											vat_child_pelican_data = {}
											vat_child_pelican_data["income_account"] = "510001"
											vat_child_pelican_data["customer_reference"] = customer_reference
											vat_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
											vat_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
											# vat_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											vat_child_pelican_data["vat"] = "V"
											vat_child_pelican_data["dc_marker"] = "C"
											vat_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_child_pelican_data["journal_type"] = "VI"
											vat_child_pelican_data["account_description"] = single_booking.ticket
											vat_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
											vat_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_child_pelican_data["base_amount"] = round(single_facility["base_child_vat"], 2)

											data.append(vat_child_pelican_data)

											## Catering Levy (Facility (Pelican) Children Bookings) ##
											cater_child_pelican_data = {}
											cater_child_pelican_data["income_account"] = "511006"
											cater_child_pelican_data["customer_reference"] = customer_reference
											cater_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
											cater_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											cater_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
											# cater_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
											cater_child_pelican_data["vat"] = "V"
											cater_child_pelican_data["dc_marker"] = "C"
											cater_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											cater_child_pelican_data["journal_type"] = "VI"
											cater_child_pelican_data["account_description"] = single_booking.ticket
											cater_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
											cater_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
											cater_child_pelican_data["base_amount"] = round(single_facility["base_child_cater"], 2)

											data.append(cater_child_pelican_data)
									
									else:
										## TODO: Handle
										if single_facility["vat"]:
											pass
										else:
											pass

										## TODO: Handle
										if single_facility["catering_levy"]:
											pass

							elif single_facility["facility_type"] == "Camping Sites":
								if single_facility["vat"] & single_facility["catering_levy"]:
									## Income (Facility (Campsites) Campsite Fee) ##
									income_camp_fee_data = {}
									income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
									income_camp_fee_data["customer_reference"] = customer_reference
									income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									income_camp_fee_data["vat"] = "V"
									income_camp_fee_data["dc_marker"] = "C"
									income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_camp_fee_data["journal_type"] = "VI"
									income_camp_fee_data["account_description"] = single_booking.ticket
									income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_minus_taxes"], 2)

									data.append(income_camp_fee_data)

									## VAT (Facility (Campsites) Campsite Fee) ##
									vat_camp_fee_data = {}
									vat_camp_fee_data["income_account"] = "510001"
									vat_camp_fee_data["customer_reference"] = customer_reference
									vat_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									vat_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									vat_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									vat_camp_fee_data["vat"] = "V"
									vat_camp_fee_data["dc_marker"] = "C"
									vat_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_camp_fee_data["journal_type"] = "VI"
									vat_camp_fee_data["account_description"] = single_booking.ticket
									vat_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									vat_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_vat"], 2)

									data.append(vat_camp_fee_data)

									## Catering Levy (Facility (Campsites) Campsite Fee) ##
									cater_camp_fee_data = {}
									cater_camp_fee_data["income_account"] = "511006"
									cater_camp_fee_data["customer_reference"] = customer_reference
									cater_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									cater_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									cater_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									cater_camp_fee_data["vat"] = "V"
									cater_camp_fee_data["dc_marker"] = "C"
									cater_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_camp_fee_data["journal_type"] = "VI"
									cater_camp_fee_data["account_description"] = single_booking.ticket
									cater_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									cater_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									cater_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_cater"], 2)

									data.append(cater_camp_fee_data)

									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
										## Income (Facility (Campsites) Adult Bookings) ##
										income_adult_campsite_data = {}
										income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
										income_adult_campsite_data["customer_reference"] = customer_reference
										income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
										income_adult_campsite_data["vat"] = "V"
										income_adult_campsite_data["dc_marker"] = "C"
										income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_campsite_data["journal_type"] = "VI"
										income_adult_campsite_data["account_description"] = single_booking.ticket
										income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_minus_taxes"], 2)

										data.append(income_adult_campsite_data)

										## VAT (Facility (Campsites) Adult Bookings) ##
										vat_adult_campsite_data = {}
										vat_adult_campsite_data["income_account"] = "510001"
										vat_adult_campsite_data["customer_reference"] = customer_reference
										vat_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
										vat_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										vat_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
										vat_adult_campsite_data["vat"] = "V"
										vat_adult_campsite_data["dc_marker"] = "C"
										vat_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_adult_campsite_data["journal_type"] = "VI"
										vat_adult_campsite_data["account_description"] = single_booking.ticket
										vat_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										vat_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_vat"], 2)

										data.append(vat_adult_campsite_data)

										## Catering Levy (Facility (Campsites) Adult Bookings) ##
										cater_adult_campsite_data = {}
										cater_adult_campsite_data["income_account"] = "511006"
										cater_adult_campsite_data["customer_reference"] = customer_reference
										cater_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
										cater_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										cater_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
										cater_adult_campsite_data["vat"] = "V"
										cater_adult_campsite_data["dc_marker"] = "C"
										cater_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_adult_campsite_data["journal_type"] = "VI"
										cater_adult_campsite_data["account_description"] = single_booking.ticket
										cater_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										cater_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_cater"], 2)

										data.append(cater_adult_campsite_data)

									if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
										## Income (Facility (Campsites) Children Bookings) ##
										income_child_campsite_data = {}
										income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
										income_child_campsite_data["customer_reference"] = customer_reference
										income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
										income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
										income_child_campsite_data["vat"] = "V"
										income_child_campsite_data["dc_marker"] = "C"
										income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_campsite_data["journal_type"] = "VI"
										income_child_campsite_data["account_description"] = single_booking.ticket
										income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_minus_taxes"], 2)

										data.append(income_child_campsite_data)

										## VAT (Facility (Campsites) Children Bookings) ##
										vat_child_campsite_data = {}
										vat_child_campsite_data["income_account"] = "510001"
										vat_child_campsite_data["customer_reference"] = customer_reference
										vat_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
										vat_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										vat_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
										vat_child_campsite_data["vat"] = "V"
										vat_child_campsite_data["dc_marker"] = "C"
										vat_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_child_campsite_data["journal_type"] = "VI"
										vat_child_campsite_data["account_description"] = single_booking.ticket
										vat_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										vat_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_vat"], 2)

										data.append(vat_child_campsite_data)

										## Catering Levy (Facility (Campsites) Children Bookings) ##
										cater_child_campsite_data = {}
										cater_child_campsite_data["income_account"] = "511006"
										cater_child_campsite_data["customer_reference"] = customer_reference
										cater_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
										cater_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
										cater_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
										cater_child_campsite_data["vat"] = "V"
										cater_child_campsite_data["dc_marker"] = "C"
										cater_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_child_campsite_data["journal_type"] = "VI"
										cater_child_campsite_data["account_description"] = single_booking.ticket
										cater_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
										cater_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_cater"], 2)

										data.append(cater_child_campsite_data)
								
								else:
									if single_facility["vat"]:
										## Income (Facility (Campsites) Campsite Fee) ##
										income_camp_fee_data = {}
										income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
										income_camp_fee_data["customer_reference"] = customer_reference
										income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
										income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
										income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
										income_camp_fee_data["vat"] = "V"
										income_camp_fee_data["dc_marker"] = "C"
										income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_camp_fee_data["journal_type"] = "VI"
										income_camp_fee_data["account_description"] = single_booking.ticket
										income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
										income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
										income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_minus_vat"], 2)

										data.append(income_camp_fee_data)

										## VAT (Facility (Campsites) Campsite Fee) ##
										vat_camp_fee_data = {}
										vat_camp_fee_data["income_account"] = "510001"
										vat_camp_fee_data["customer_reference"] = customer_reference
										vat_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
										vat_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
										vat_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
										vat_camp_fee_data["vat"] = "V"
										vat_camp_fee_data["dc_marker"] = "C"
										vat_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_camp_fee_data["journal_type"] = "VI"
										vat_camp_fee_data["account_description"] = single_booking.ticket
										vat_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
										vat_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_vat"], 2)

										data.append(vat_camp_fee_data)

										if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
											## Income (Facility (Campsites) Adult Bookings) ##
											income_adult_campsite_data = {}
											income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
											income_adult_campsite_data["customer_reference"] = customer_reference
											income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
											income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
											income_adult_campsite_data["vat"] = "V"
											income_adult_campsite_data["dc_marker"] = "C"
											income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_campsite_data["journal_type"] = "VI"
											income_adult_campsite_data["account_description"] = single_booking.ticket
											income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_minus_vat"], 2)

											data.append(income_adult_campsite_data)

											## VAT (Facility (Campsites) Adult Bookings) ##
											vat_adult_campsite_data = {}
											vat_adult_campsite_data["income_account"] = "510001"
											vat_adult_campsite_data["customer_reference"] = customer_reference
											vat_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
											vat_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
											vat_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
											vat_adult_campsite_data["vat"] = "V"
											vat_adult_campsite_data["dc_marker"] = "C"
											vat_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_adult_campsite_data["journal_type"] = "VI"
											vat_adult_campsite_data["account_description"] = single_booking.ticket
											vat_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
											vat_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_vat"], 2)

											data.append(vat_adult_campsite_data)

										if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
											## Income (Facility (Campsites) Children Bookings) ##
											income_child_campsite_data = {}
											income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
											income_child_campsite_data["customer_reference"] = customer_reference
											income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
											income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
											income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
											income_child_campsite_data["vat"] = "V"
											income_child_campsite_data["dc_marker"] = "C"
											income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_campsite_data["journal_type"] = "VI"
											income_child_campsite_data["account_description"] = single_booking.ticket
											income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_minus_vat"], 2)

											data.append(income_child_campsite_data)

											## VAT (Facility (Campsites) Children Bookings) ##
											vat_child_campsite_data = {}
											vat_child_campsite_data["income_account"] = "510001"
											vat_child_campsite_data["customer_reference"] = customer_reference
											vat_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
											vat_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											vat_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
											vat_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
											vat_child_campsite_data["vat"] = "V"
											vat_child_campsite_data["dc_marker"] = "C"
											vat_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											vat_child_campsite_data["journal_type"] = "VI"
											vat_child_campsite_data["account_description"] = single_booking.ticket
											vat_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
											vat_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
											vat_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_vat"], 2)

											data.append(vat_child_campsite_data)
									
									else:
										## Income (Facility (Campsites) Campsite Fee) ##
										income_camp_fee_data = {}
										income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
										income_camp_fee_data["customer_reference"] = customer_reference
										income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
										income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
										income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
										income_camp_fee_data["vat"] = "V"
										income_camp_fee_data["dc_marker"] = "C"
										income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_camp_fee_data["journal_type"] = "VI"
										income_camp_fee_data["account_description"] = single_booking.ticket
										income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
										income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
										income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost"], 2)

										data.append(income_camp_fee_data)

										if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
											## Income (Facility (Campsites) Adult Bookings) ##
											income_adult_campsite_data = {}
											income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
											income_adult_campsite_data["customer_reference"] = customer_reference
											income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
											income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
											income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
											income_adult_campsite_data["vat"] = "V"
											income_adult_campsite_data["dc_marker"] = "C"
											income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_adult_campsite_data["journal_type"] = "VI"
											income_adult_campsite_data["account_description"] = single_booking.ticket
											income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
											income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
											income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost"], 2)

											data.append(income_adult_campsite_data)

										if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
											## Income (Facility (Campsites) Children Bookings) ##
											income_child_campsite_data = {}
											income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
											income_child_campsite_data["customer_reference"] = customer_reference
											income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
											income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
											income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
											income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
											income_child_campsite_data["vat"] = "V"
											income_child_campsite_data["dc_marker"] = "C"
											income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
											income_child_campsite_data["journal_type"] = "VI"
											income_child_campsite_data["account_description"] = single_booking.ticket
											income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
											income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
											income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost"], 2)

											data.append(income_child_campsite_data)

									## TODO: Handle
									if single_facility["catering_levy"]:
										pass
					
					booking_data = {}
					# try:
					# 	booking_data["income_account"] = return_data["code_kes"]
					# except Exception:
					# 	booking_data["income_account"] = "406GA001"
					
					booking_data["income_account"] = return_data["customer_code"]
					booking_data["customer_reference"] = customer_reference
					
					# booking_data["currency_code"] = single_booking.currency
					booking_data["currency"] = booking_currency.json()["data"][0]["currency_name"]
					
					booking_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
					booking_data["tran_quantity"] = 1
					booking_data["unit_price"] = round(return_data["total_cost"], 2)
					booking_data["dc_marker"] = "D"
					booking_data["booking_type"] = return_data["booking_type"]
					booking_data["partner_booking"] = return_data["partner_booking"]
					booking_data["guest_booking"] = return_data["guest_booking"]
					booking_data["gate_booking"] = return_data["gate_booking"]
					booking_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])

					booking_data["journal_type"] = "VI"
					booking_data["account_description"] = single_booking.ticket
					booking_data["outlet"] = "#"
					booking_data["department"] = "#"
					booking_data["base_amount"] = round(return_data["total_cost"], 2)

					data.append(booking_data)
					existing_bookings.append(single_booking.booking_public_id)

	select_transactions = db.session.query(Transaction)\
									.join(Booking, Transaction.booking_id == Booking.booking_public_id)\
									.add_columns(Transaction.booking_id, Transaction.transaction_total, Transaction.transaction_total_currency,\
												 Transaction.transaction_payment_currency, Transaction.transaction_payment_method, Transaction.transaction_booking_public_id,\
												 Transaction.payment_currency_buying_rate_at_time, Transaction.payment_currency_selling_rate_at_time,\
												 Transaction.transaction_payment_gateway, Transaction.transaction_date,\
												 Booking.booking_public_id, Booking.booking_ref_code, Booking.ticket,\
												 Booking.currency, Booking.booking_done_by, Booking.created_at,\
												 Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time)\
									.filter(Transaction.deletion_marker == None)\
									.filter(Booking.deletion_marker == None)\
									.filter(Booking.status != get_booking_status_id("Cancelled"))\
									.all()

	existing_transactions = []

	transaction_data = []
	for single_transaction in select_transactions:
		for single_date in date_array:
			if single_transaction.transaction_booking_public_id in existing_transactions:
				pass
			else:
				if single_transaction.transaction_date.strftime("%Y-%m-%d") == single_date:
					if single_transaction.transaction_payment_method == "746bb582-89ed-4c83-ac3c-f25d9d60b266":
						pass
					elif single_transaction.transaction_payment_method == "37da2c36-59d2-4bf5-916e-f1c7a1596fc0":
						pass
					else:
						general_booking_data = {}
						
						## Customer data
						customer_data = {}

						booking_currency = requests.get(get_currency.format(single_transaction.currency))
						transaction_currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
						bookingTotal(general_booking_data, single_transaction.booking_id, True)

						check_school_booking = db.session.query(SchoolBooking)\
														 .join(School, SchoolBooking.school_id == School.school_public_id)\
														 .add_columns(School.school_name)\
														 .filter(SchoolBooking.deletion_marker == None)\
														 .filter(SchoolBooking.booking_id == single_transaction.booking_public_id)\
														 .first()
						
						if check_school_booking:
							try:
								booking_done_by = check_school_booking.school_name
							except Exception:
								booking_done_by = single_transaction.booking_done_by
						else:
							booking_done_by = single_transaction.booking_done_by
						
						customer_ref = single_transaction.booking_ref_code + " " + booking_done_by

						if len(customer_ref) >= 30:
							customer_reference = customer_ref[:29]
						else:
							customer_reference = customer_ref

						get_booking_payment = db.session.query(BookingPayment)\
														.filter(BookingPayment.deletion_marker == None)\
														.filter(BookingPayment.transaction_id == single_transaction.transaction_booking_public_id)\
														.first()

						if get_booking_payment.card_first_four:
							bank_reference = get_booking_payment.card_first_four + "..." + get_booking_payment.card_last_four
						else:
							bank_reference = get_booking_payment.mpesa_reference

						customer_data["booking_public_id"] = single_transaction.booking_id
						customer_data["sales_date"] = str(single_transaction.transaction_date.strftime("%d%m%Y"))
						customer_data["customer_reference"] = customer_reference
						customer_data["dc_marker"] = "C"
						customer_data["account_description"] = single_transaction.ticket
						# customer_data["currency_code"] = single_transaction.transaction_payment_currency
						customer_data["currency_code"] = single_transaction.currency
						customer_data["currency"] = transaction_currency.json()["data"][0]["currency_name"]
						# customer_data["currency"] = booking_currency.json()["data"][0]["currency_name"]
						customer_data["tran_quantity"] = 1
						customer_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
						# customer_data["unit_price"] = round(float(convertAmount(single_transaction.currency, single_transaction.transaction_payment_currency, single_transaction.transaction_total,\
						# 														single_transaction.payment_currency_buying_rate_at_time, single_transaction.payment_currency_selling_rate_at_time, single_transaction.transaction_date.strftime("%Y-%m-%d"))), 2)
						customer_data["journal_type"] = "VI"
						customer_data["outlet"] = "#"
						customer_data["department"] = "#"
						try:
							customer_data["conversion_rate"] = float(single_transaction.payment_currency_selling_rate_at_time)
						except Exception:
							raise Exception(single_transaction.booking_public_id)
						
						# try:
						# 	customer_data["income_account"] = general_booking_data["code_kes"]
						# except Exception:
						# 	customer_data["income_account"] = "406GA001"

						customer_data["income_account"] = general_booking_data["customer_code"]
						
						transaction_data.append(customer_data)

						## Bank data
						bank_data = {}

						transaction_currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
						bookingTotal(general_booking_data, single_transaction.booking_id, True)

						bank_data["booking_public_id"] = single_transaction.booking_id
						bank_data["sales_date"] = str(single_transaction.transaction_date.strftime("%d%m%Y"))
						bank_data["customer_reference"] = customer_reference
						bank_data["dc_marker"] = "D"
						# bank_data["account_description"] = single_transaction.ticket
						# bank_data["account_description"] = customer_reference
						bank_data["account_description"] = bank_reference
						bank_data["currency_code"] = single_transaction.currency
						bank_data["currency"] = transaction_currency.json()["data"][0]["currency_name"]
						bank_data["tran_quantity"] = 1
						bank_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
						bank_data["journal_type"] = "VI"
						bank_data["outlet"] = "#"
						bank_data["department"] = "#"
						bank_data["conversion_rate"] = float(single_transaction.payment_currency_selling_rate_at_time)
						
						## TODO: Save payment gateways
						# Paybill
						if single_transaction.transaction_payment_method == "5550ff2f-bc48-4f0e-8944-d08f7f2b93fd":
							bank_data["income_account"] = "407003"
						# Mpesa
						elif single_transaction.transaction_payment_method == "cbff45f3-4f12-41da-8b4a-bf308112f032":
							bank_data["income_account"] = "407003"
						# Paypal
						elif single_transaction.transaction_payment_method == "653e10f6-168c-493e-bcbb-0219537c24f6":
							bank_data["income_account"] = "407009"
						# iVeri
						elif single_transaction.transaction_payment_method == "33ad20be-3a89-4deb-ab71-7510ba51677e":
							if transaction_currency.json()["data"][0]["currency_name"] == "KES":
								bank_data["income_account"] = "407016"
							elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
								bank_data["income_account"] = "407017"
						# Mpesa Online
						elif single_transaction.transaction_payment_method == "1c9d01cc-ef31-4757-a390-10c765fcecab":
							bank_data["income_account"] = "407003"
						else:
							if single_transaction.transaction_payment_gateway:
								get_gateway = db.session.query(PaymentGateway)\
														.filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
														.first()

								bank_data["income_account"] = get_gateway.payment_gateway_code
							else:
								if transaction_currency.json()["data"][0]["currency_name"] == "KES":
									bank_data["income_account"] = "407004"
								elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
									bank_data["income_account"] = "407005"

						transaction_data.append(bank_data)
						existing_transactions.append(single_transaction.transaction_booking_public_id)
		
	return jsonify({"data": data, "transation_data": transaction_data}), 200


@app.route("/credit-notes/data", methods = ["POST"])
def credit_note_function():
	start_date = request.json["start_date"]
	end_date = request.json["end_date"]

	start = GenerateDateFromString.generateDate(start_date)
	end = GenerateDateFromString.generateDate(end_date)

	date_array = []

	while start <= end:
		date_array.append(start.strftime("%Y-%m-%d"))

		start = start + timedelta(days = 1)

	get_credit_note_info = db.session.query(CreditNote)\
									 .filter(CreditNote.deletion_marker == None)\
									 .all()

	data = []
	for single_credit_note in get_credit_note_info:
		for single_date in date_array:
			if single_credit_note.created_at.strftime("%Y-%m-%d") == single_date:
				get_booking = db.session.query(Booking)\
										.filter(Booking.booking_public_id == single_credit_note.booking_id)\
										.first()
				
				check_school_booking = db.session.query(SchoolBooking)\
												 .join(School, SchoolBooking.school_id == School.school_public_id)\
												 .add_columns(School.school_name)\
												 .filter(SchoolBooking.deletion_marker == None)\
												 .filter(SchoolBooking.booking_id == get_booking.booking_public_id)\
												 .first()
				
				credit_note_details = {}
				creditNoteTotal(single_credit_note.credit_note_public_id, single_credit_note.booking_id, credit_note_details, batchfile = True)

				if check_school_booking:
					try:
						booking_done_by = check_school_booking.school_name
					except Exception:
						booking_done_by = get_booking.booking_done_by
				else:
					booking_done_by = get_booking.booking_done_by
				
				customer_ref = get_booking.booking_ref_code + " " + booking_done_by

				if len(customer_ref) >= 30:
					customer_reference = customer_ref[:29]
				else:
					customer_reference = customer_ref
				
				credit_note_currency = get_details_currency(single_credit_note.credit_note_currency)
				
				for single_guest in credit_note_details["guests"]:
					# Credit Note Income (Guests)
					income_guest_data = {}
					income_guest_data["income_account"] = single_guest["payment_person_income_code"]
					income_guest_data["customer_reference"] = customer_reference
					income_guest_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
					income_guest_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
					income_guest_data["tran_quantity"] = 1
					income_guest_data["unit_price"] = single_guest["total_minus_vat"]
					income_guest_data["vat"] = "V"
					income_guest_data["dc_marker"] = "D"
					income_guest_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
					income_guest_data["journal_type"] = "VI"
					income_guest_data["account_description"] = get_booking.ticket
					income_guest_data["department"] = single_guest["payment_person_dept_code"]
					income_guest_data["outlet"] = single_guest["destination_outlet_code"]
					income_guest_data["base_amount"] = single_guest["total_minus_vat"]
					
					data.append(income_guest_data)

					# Credit Note VAT (Guests)
					vat_guest_data = {}
					vat_guest_data["income_account"] = "510001"
					vat_guest_data["customer_reference"] = customer_reference
					vat_guest_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
					vat_guest_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
					vat_guest_data["tran_quantity"] = 1
					vat_guest_data["unit_price"] = single_guest["vat"]
					vat_guest_data["vat"] = "V"
					vat_guest_data["dc_marker"] = "D"
					vat_guest_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
					vat_guest_data["journal_type"] = "VI"
					vat_guest_data["account_description"] = get_booking.ticket
					vat_guest_data["department"] = "#"
					vat_guest_data["outlet"] = "#"
					vat_guest_data["base_amount"] = single_guest["vat"]
					
					data.append(vat_guest_data)

				for single_vehicle in credit_note_details["vehicles"]:
					# Credit Note Income (Vehicles)
					income_vehicle_data = {}
					income_vehicle_data["income_account"] = single_vehicle["vehicle_charge_income_code"]
					income_vehicle_data["customer_reference"] = customer_reference
					income_vehicle_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
					income_vehicle_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
					income_vehicle_data["tran_quantity"] = 1
					income_vehicle_data["unit_price"] = single_vehicle["total_minus_vat"]
					income_vehicle_data["vat"] = "V"
					income_vehicle_data["dc_marker"] = "D"
					income_vehicle_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
					income_vehicle_data["journal_type"] = "VI"
					income_vehicle_data["account_description"] = get_booking.ticket
					income_vehicle_data["department"] = single_vehicle["vehicle_charge_dept_code"]
					income_vehicle_data["outlet"] = single_vehicle["vehicle_charge_outlet_code"]
					income_vehicle_data["base_amount"] = single_vehicle["total_minus_vat"]
					
					data.append(income_vehicle_data)

					# Credit Note VAT (Vehicles)
					vat_vehicle_data = {}
					vat_vehicle_data["income_account"] = "510001"
					vat_vehicle_data["customer_reference"] = customer_reference
					vat_vehicle_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
					vat_vehicle_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
					vat_vehicle_data["tran_quantity"] = 1
					vat_vehicle_data["unit_price"] = single_vehicle["vat"]
					vat_vehicle_data["vat"] = "V"
					vat_vehicle_data["dc_marker"] = "D"
					vat_vehicle_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
					vat_vehicle_data["journal_type"] = "VI"
					vat_vehicle_data["account_description"] = get_booking.ticket
					vat_vehicle_data["department"] = "#"
					vat_vehicle_data["outlet"] = "#"
					vat_vehicle_data["base_amount"] = single_vehicle["vat"]
					
					data.append(vat_vehicle_data)

				for single_activity in credit_note_details["inventory"]:
					if single_activity["credit_no_of_adults"] > 0:
						if single_activity["vat"] & single_activity["catering_levy"]:
							# Credit Note Income (Inventory Adults)
							income_inventory_adult_data = {}
							income_inventory_adult_data["income_account"] = single_activity["inventory_income_code"]
							income_inventory_adult_data["customer_reference"] = customer_reference
							income_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							income_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							income_inventory_adult_data["tran_quantity"] = 1
							income_inventory_adult_data["unit_price"] = single_activity["adult_minus_taxes"]
							income_inventory_adult_data["vat"] = "V"
							income_inventory_adult_data["dc_marker"] = "D"
							income_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							income_inventory_adult_data["journal_type"] = "VI"
							income_inventory_adult_data["account_description"] = get_booking.ticket
							income_inventory_adult_data["department"] = single_activity["inventory_dept_analysis_code"]
							income_inventory_adult_data["outlet"] = single_activity["inventory_outlet_code"]
							income_inventory_adult_data["base_amount"] = single_activity["adult_minus_taxes"]
							
							data.append(income_inventory_adult_data)

							# Credit Note VAT (Inventory Adults)
							vat_inventory_adult_data = {}
							vat_inventory_adult_data["income_account"] = "510001"
							vat_inventory_adult_data["customer_reference"] = customer_reference
							vat_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							vat_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							vat_inventory_adult_data["tran_quantity"] = 1
							vat_inventory_adult_data["unit_price"] = single_activity["adult_vat"]
							vat_inventory_adult_data["vat"] = "V"
							vat_inventory_adult_data["dc_marker"] = "D"
							vat_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							vat_inventory_adult_data["journal_type"] = "VI"
							vat_inventory_adult_data["account_description"] = get_booking.ticket
							vat_inventory_adult_data["department"] = "#"
							vat_inventory_adult_data["outlet"] = "#"
							vat_inventory_adult_data["base_amount"] = single_activity["adult_vat"]
							
							data.append(vat_inventory_adult_data)

							# Credit Note Catering Levy (Inventory Adults)
							cater_inventory_adult_data = {}
							cater_inventory_adult_data["income_account"] = "511006"
							cater_inventory_adult_data["customer_reference"] = customer_reference
							cater_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							cater_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							cater_inventory_adult_data["tran_quantity"] = 1
							cater_inventory_adult_data["unit_price"] = single_activity["adult_cater"]
							cater_inventory_adult_data["vat"] = "V"
							cater_inventory_adult_data["dc_marker"] = "D"
							cater_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							cater_inventory_adult_data["journal_type"] = "VI"
							cater_inventory_adult_data["account_description"] = get_booking.ticket
							cater_inventory_adult_data["department"] = "#"
							cater_inventory_adult_data["outlet"] = "#"
							cater_inventory_adult_data["base_amount"] = single_activity["adult_cater"]
							
							data.append(cater_inventory_adult_data)
						
						else:
							if single_activity["vat"]:
								# Credit Note Income (Inventory Adults)
								income_inventory_adult_data = {}
								income_inventory_adult_data["income_account"] = single_activity["inventory_income_code"]
								income_inventory_adult_data["customer_reference"] = customer_reference
								income_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_inventory_adult_data["tran_quantity"] = 1
								income_inventory_adult_data["unit_price"] = single_activity["adult_total_minus_vat"]
								income_inventory_adult_data["vat"] = "V"
								income_inventory_adult_data["dc_marker"] = "D"
								income_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_inventory_adult_data["journal_type"] = "VI"
								income_inventory_adult_data["account_description"] = get_booking.ticket
								income_inventory_adult_data["department"] = single_activity["inventory_dept_analysis_code"]
								income_inventory_adult_data["outlet"] = single_activity["inventory_outlet_code"]
								income_inventory_adult_data["base_amount"] = single_activity["adult_total_minus_vat"]
								
								data.append(income_inventory_adult_data)

								# Credit Note VAT (Inventory Adults)
								vat_inventory_adult_data = {}
								vat_inventory_adult_data["income_account"] = "510001"
								vat_inventory_adult_data["customer_reference"] = customer_reference
								vat_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								vat_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								vat_inventory_adult_data["tran_quantity"] = 1
								vat_inventory_adult_data["unit_price"] = single_activity["adult_vat"]
								vat_inventory_adult_data["vat"] = "V"
								vat_inventory_adult_data["dc_marker"] = "D"
								vat_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								vat_inventory_adult_data["journal_type"] = "VI"
								vat_inventory_adult_data["account_description"] = get_booking.ticket
								vat_inventory_adult_data["department"] = "#"
								vat_inventory_adult_data["outlet"] = "#"
								vat_inventory_adult_data["base_amount"] = single_activity["adult_vat"]
								
								data.append(vat_inventory_adult_data)
							
							elif single_activity["catering_levy"]:
								# Credit Note Income (Inventory Adults)
								income_inventory_adult_data = {}
								income_inventory_adult_data["income_account"] = single_activity["inventory_income_code"]
								income_inventory_adult_data["customer_reference"] = customer_reference
								income_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_inventory_adult_data["tran_quantity"] = 1
								income_inventory_adult_data["unit_price"] = single_activity["adult_total_minus_cater"]
								income_inventory_adult_data["vat"] = "V"
								income_inventory_adult_data["dc_marker"] = "D"
								income_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_inventory_adult_data["journal_type"] = "VI"
								income_inventory_adult_data["account_description"] = get_booking.ticket
								income_inventory_adult_data["department"] = single_activity["inventory_dept_analysis_code"]
								income_inventory_adult_data["outlet"] = single_activity["inventory_outlet_code"]
								income_inventory_adult_data["base_amount"] = single_activity["adult_total_minus_cater"]
								
								data.append(income_inventory_adult_data)

								# Credit Note Catering Levy (Inventory Adults)
								cater_inventory_adult_data = {}
								cater_inventory_adult_data["income_account"] = "511006"
								cater_inventory_adult_data["customer_reference"] = customer_reference
								cater_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								cater_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								cater_inventory_adult_data["tran_quantity"] = 1
								cater_inventory_adult_data["unit_price"] = single_activity["adult_cater"]
								cater_inventory_adult_data["vat"] = "V"
								cater_inventory_adult_data["dc_marker"] = "D"
								cater_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								cater_inventory_adult_data["journal_type"] = "VI"
								cater_inventory_adult_data["account_description"] = get_booking.ticket
								cater_inventory_adult_data["department"] = "#"
								cater_inventory_adult_data["outlet"] = "#"
								cater_inventory_adult_data["base_amount"] = single_activity["adult_cater"]
								
								data.append(cater_inventory_adult_data)

							else:
								# Credit Note Income (Inventory Adults)
								income_inventory_adult_data = {}
								income_inventory_adult_data["income_account"] = single_activity["inventory_income_code"]
								income_inventory_adult_data["customer_reference"] = customer_reference
								income_inventory_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_inventory_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_inventory_adult_data["tran_quantity"] = 1
								income_inventory_adult_data["unit_price"] = single_activity["adult_total"]
								income_inventory_adult_data["vat"] = "V"
								income_inventory_adult_data["dc_marker"] = "D"
								income_inventory_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_inventory_adult_data["journal_type"] = "VI"
								income_inventory_adult_data["account_description"] = get_booking.ticket
								income_inventory_adult_data["department"] = single_activity["inventory_dept_analysis_code"]
								income_inventory_adult_data["outlet"] = single_activity["inventory_outlet_code"]
								income_inventory_adult_data["base_amount"] = single_activity["adult_total"]
								
								data.append(income_inventory_adult_data)

					if single_activity["credit_no_of_children"] > 0:
						if single_activity["vat"] & single_activity["catering_levy"]:
							# Credit Note Income (Inventory Children)
							income_inventory_child_data = {}
							income_inventory_child_data["income_account"] = single_activity["inventory_income_code"]
							income_inventory_child_data["customer_reference"] = customer_reference
							income_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							income_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							income_inventory_child_data["tran_quantity"] = 1
							income_inventory_child_data["unit_price"] = single_activity["child_minus_taxes"]
							income_inventory_child_data["vat"] = "V"
							income_inventory_child_data["dc_marker"] = "D"
							income_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							income_inventory_child_data["journal_type"] = "VI"
							income_inventory_child_data["account_description"] = get_booking.ticket
							income_inventory_child_data["department"] = single_activity["inventory_dept_analysis_code"]
							income_inventory_child_data["outlet"] = single_activity["inventory_outlet_code"]
							income_inventory_child_data["base_amount"] = single_activity["child_minus_taxes"]
							
							data.append(income_inventory_child_data)

							# Credit Note VAT (Inventory Children)
							vat_inventory_child_data = {}
							vat_inventory_child_data["income_account"] = "510001"
							vat_inventory_child_data["customer_reference"] = customer_reference
							vat_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							vat_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							vat_inventory_child_data["tran_quantity"] = 1
							vat_inventory_child_data["unit_price"] = single_activity["child_vat"]
							vat_inventory_child_data["vat"] = "V"
							vat_inventory_child_data["dc_marker"] = "D"
							vat_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							vat_inventory_child_data["journal_type"] = "VI"
							vat_inventory_child_data["account_description"] = get_booking.ticket
							vat_inventory_child_data["department"] = "#"
							vat_inventory_child_data["outlet"] = "#"
							vat_inventory_child_data["base_amount"] = single_activity["child_vat"]
							
							data.append(vat_inventory_child_data)

							# Credit Note Catering Levy (Inventory Children)
							cater_inventory_child_data = {}
							cater_inventory_child_data["income_account"] = "511006"
							cater_inventory_child_data["customer_reference"] = customer_reference
							cater_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							cater_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							cater_inventory_child_data["tran_quantity"] = 1
							cater_inventory_child_data["unit_price"] = single_activity["child_cater"]
							cater_inventory_child_data["vat"] = "V"
							cater_inventory_child_data["dc_marker"] = "D"
							cater_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							cater_inventory_child_data["journal_type"] = "VI"
							cater_inventory_child_data["account_description"] = get_booking.ticket
							cater_inventory_child_data["department"] = "#"
							cater_inventory_child_data["outlet"] = "#"
							cater_inventory_child_data["base_amount"] = single_activity["child_cater"]
							
							data.append(cater_inventory_child_data)
						
						else:
							if single_activity["vat"]:
								# Credit Note Income (Inventory Children)
								income_inventory_child_data = {}
								income_inventory_child_data["income_account"] = single_activity["inventory_income_code"]
								income_inventory_child_data["customer_reference"] = customer_reference
								income_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_inventory_child_data["tran_quantity"] = 1
								income_inventory_child_data["unit_price"] = single_activity["child_total_minus_vat"]
								income_inventory_child_data["vat"] = "V"
								income_inventory_child_data["dc_marker"] = "D"
								income_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_inventory_child_data["journal_type"] = "VI"
								income_inventory_child_data["account_description"] = get_booking.ticket
								income_inventory_child_data["department"] = single_activity["inventory_dept_analysis_code"]
								income_inventory_child_data["outlet"] = single_activity["inventory_outlet_code"]
								income_inventory_child_data["base_amount"] = single_activity["child_total_minus_vat"]
								
								data.append(income_inventory_child_data)

								# Credit Note VAT (Inventory Children)
								vat_inventory_child_data = {}
								vat_inventory_child_data["income_account"] = "510001"
								vat_inventory_child_data["customer_reference"] = customer_reference
								vat_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								vat_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								vat_inventory_child_data["tran_quantity"] = 1
								vat_inventory_child_data["unit_price"] = single_activity["child_vat"]
								vat_inventory_child_data["vat"] = "V"
								vat_inventory_child_data["dc_marker"] = "D"
								vat_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								vat_inventory_child_data["journal_type"] = "VI"
								vat_inventory_child_data["account_description"] = get_booking.ticket
								vat_inventory_child_data["department"] = "#"
								vat_inventory_child_data["outlet"] = "#"
								vat_inventory_child_data["base_amount"] = single_activity["child_vat"]
								
								data.append(vat_inventory_child_data)
							
							elif single_activity["catering_levy"]:
								# Credit Note Income (Inventory Children)
								income_inventory_child_data = {}
								income_inventory_child_data["income_account"] = single_activity["inventory_income_code"]
								income_inventory_child_data["customer_reference"] = customer_reference
								income_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_inventory_child_data["tran_quantity"] = 1
								income_inventory_child_data["unit_price"] = single_activity["child_total_minus_cater"]
								income_inventory_child_data["vat"] = "V"
								income_inventory_child_data["dc_marker"] = "D"
								income_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_inventory_child_data["journal_type"] = "VI"
								income_inventory_child_data["account_description"] = get_booking.ticket
								income_inventory_child_data["department"] = single_activity["inventory_dept_analysis_code"]
								income_inventory_child_data["outlet"] = single_activity["inventory_outlet_code"]
								income_inventory_child_data["base_amount"] = single_activity["child_total_minus_cater"]
								
								data.append(income_inventory_child_data)

								# Credit Note Catering Levy (Inventory Children)
								cater_inventory_child_data = {}
								cater_inventory_child_data["income_account"] = "511006"
								cater_inventory_child_data["customer_reference"] = customer_reference
								cater_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								cater_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								cater_inventory_child_data["tran_quantity"] = 1
								cater_inventory_child_data["unit_price"] = single_activity["child_cater"]
								cater_inventory_child_data["vat"] = "V"
								cater_inventory_child_data["dc_marker"] = "D"
								cater_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								cater_inventory_child_data["journal_type"] = "VI"
								cater_inventory_child_data["account_description"] = get_booking.ticket
								cater_inventory_child_data["department"] = "#"
								cater_inventory_child_data["outlet"] = "#"
								cater_inventory_child_data["base_amount"] = single_activity["child_cater"]
								
								data.append(cater_inventory_child_data)

							else:
								# Credit Note Income (Inventory Children)
								income_inventory_child_data = {}
								income_inventory_child_data["income_account"] = single_activity["inventory_income_code"]
								income_inventory_child_data["customer_reference"] = customer_reference
								income_inventory_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_inventory_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_inventory_child_data["tran_quantity"] = 1
								income_inventory_child_data["unit_price"] = single_activity["child_total"]
								income_inventory_child_data["vat"] = "V"
								income_inventory_child_data["dc_marker"] = "D"
								income_inventory_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_inventory_child_data["journal_type"] = "VI"
								income_inventory_child_data["account_description"] = get_booking.ticket
								income_inventory_child_data["department"] = single_activity["inventory_dept_analysis_code"]
								income_inventory_child_data["outlet"] = single_activity["inventory_outlet_code"]
								income_inventory_child_data["base_amount"] = single_activity["child_total"]
								
								data.append(income_inventory_child_data)

				for single_facility in credit_note_details["facilities"]:
					if single_facility["credit_no_of_adults"] > 0:
						if single_facility["vat"] & single_facility["catering_levy"]:
							# Credit Note Income (Facility Adults)
							income_facility_adult_data = {}
							income_facility_adult_data["income_account"] = single_facility["facility_income_code"]
							income_facility_adult_data["customer_reference"] = customer_reference
							income_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							income_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							income_facility_adult_data["tran_quantity"] = 1
							income_facility_adult_data["unit_price"] = single_facility["adult_minus_taxes"]
							income_facility_adult_data["vat"] = "V"
							income_facility_adult_data["dc_marker"] = "D"
							income_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							income_facility_adult_data["journal_type"] = "VI"
							income_facility_adult_data["account_description"] = get_booking.ticket
							income_facility_adult_data["department"] = single_facility["facility_analysis_code"]
							income_facility_adult_data["outlet"] = single_facility["facility_outlet_code"]
							income_facility_adult_data["base_amount"] = single_facility["adult_minus_taxes"]
							
							data.append(income_facility_adult_data)

							# Credit Note VAT (Facility Adults)
							vat_facility_adult_data = {}
							vat_facility_adult_data["income_account"] = "510001"
							vat_facility_adult_data["customer_reference"] = customer_reference
							vat_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							vat_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							vat_facility_adult_data["tran_quantity"] = 1
							vat_facility_adult_data["unit_price"] = single_facility["adult_vat"]
							vat_facility_adult_data["vat"] = "V"
							vat_facility_adult_data["dc_marker"] = "D"
							vat_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							vat_facility_adult_data["journal_type"] = "VI"
							vat_facility_adult_data["account_description"] = get_booking.ticket
							vat_facility_adult_data["department"] = "#"
							vat_facility_adult_data["outlet"] = "#"
							vat_facility_adult_data["base_amount"] = single_facility["adult_vat"]
							
							data.append(vat_facility_adult_data)

							# Credit Note Catering Levy (Facility Adults)
							cater_facility_adult_data = {}
							cater_facility_adult_data["income_account"] = "511006"
							cater_facility_adult_data["customer_reference"] = customer_reference
							cater_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							cater_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							cater_facility_adult_data["tran_quantity"] = 1
							cater_facility_adult_data["unit_price"] = single_facility["adult_cater"]
							cater_facility_adult_data["vat"] = "V"
							cater_facility_adult_data["dc_marker"] = "D"
							cater_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							cater_facility_adult_data["journal_type"] = "VI"
							cater_facility_adult_data["account_description"] = get_booking.ticket
							cater_facility_adult_data["department"] = "#"
							cater_facility_adult_data["outlet"] = "#"
							cater_facility_adult_data["base_amount"] = single_facility["adult_cater"]
							
							data.append(cater_facility_adult_data)

						else:
							if single_facility["vat"]:
								# Credit Note Income (Facility Adults)
								income_facility_adult_data = {}
								income_facility_adult_data["income_account"] = single_facility["facility_income_code"]
								income_facility_adult_data["customer_reference"] = customer_reference
								income_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_adult_data["tran_quantity"] = 1
								income_facility_adult_data["unit_price"] = single_facility["adult_minus_vat"]
								income_facility_adult_data["vat"] = "V"
								income_facility_adult_data["dc_marker"] = "D"
								income_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_adult_data["journal_type"] = "VI"
								income_facility_adult_data["account_description"] = get_booking.ticket
								income_facility_adult_data["department"] = single_facility["facility_analysis_code"]
								income_facility_adult_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_adult_data["base_amount"] = single_facility["adult_minus_vat"]
								
								data.append(income_facility_adult_data)

								# Credit Note VAT (Facility Adults)
								vat_facility_adult_data = {}
								vat_facility_adult_data["income_account"] = "510001"
								vat_facility_adult_data["customer_reference"] = customer_reference
								vat_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								vat_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								vat_facility_adult_data["tran_quantity"] = 1
								vat_facility_adult_data["unit_price"] = single_facility["adult_vat"]
								vat_facility_adult_data["vat"] = "V"
								vat_facility_adult_data["dc_marker"] = "D"
								vat_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								vat_facility_adult_data["journal_type"] = "VI"
								vat_facility_adult_data["account_description"] = get_booking.ticket
								vat_facility_adult_data["department"] = "#"
								vat_facility_adult_data["outlet"] = "#"
								vat_facility_adult_data["base_amount"] = single_facility["adult_vat"]
								
								data.append(vat_facility_adult_data)

							elif single_facility["catering_levy"]:
								# Credit Note Income (Facility Adults)
								income_facility_adult_data = {}
								income_facility_adult_data["income_account"] = single_facility["facility_income_code"]
								income_facility_adult_data["customer_reference"] = customer_reference
								income_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_adult_data["tran_quantity"] = 1
								income_facility_adult_data["unit_price"] = single_facility["adult_minus_cater"]
								income_facility_adult_data["vat"] = "V"
								income_facility_adult_data["dc_marker"] = "D"
								income_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_adult_data["journal_type"] = "VI"
								income_facility_adult_data["account_description"] = get_booking.ticket
								income_facility_adult_data["department"] = single_facility["facility_analysis_code"]
								income_facility_adult_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_adult_data["base_amount"] = single_facility["adult_minus_cater"]
								
								data.append(income_facility_adult_data)

								# Credit Note Catering Levy (Facility Adults)
								cater_facility_adult_data = {}
								cater_facility_adult_data["income_account"] = "511006"
								cater_facility_adult_data["customer_reference"] = customer_reference
								cater_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								cater_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								cater_facility_adult_data["tran_quantity"] = 1
								cater_facility_adult_data["unit_price"] = single_facility["adult_cater"]
								cater_facility_adult_data["vat"] = "V"
								cater_facility_adult_data["dc_marker"] = "D"
								cater_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								cater_facility_adult_data["journal_type"] = "VI"
								cater_facility_adult_data["account_description"] = get_booking.ticket
								cater_facility_adult_data["department"] = "#"
								cater_facility_adult_data["outlet"] = "#"
								cater_facility_adult_data["base_amount"] = single_facility["adult_cater"]
								
								data.append(cater_facility_adult_data)

							else:
								# Credit Note Income (Facility Adults)
								income_facility_adult_data = {}
								income_facility_adult_data["income_account"] = single_facility["facility_income_code"]
								income_facility_adult_data["customer_reference"] = customer_reference
								income_facility_adult_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_adult_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_adult_data["tran_quantity"] = 1
								income_facility_adult_data["unit_price"] = single_facility["adult_total"]
								income_facility_adult_data["vat"] = "V"
								income_facility_adult_data["dc_marker"] = "D"
								income_facility_adult_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_adult_data["journal_type"] = "VI"
								income_facility_adult_data["account_description"] = get_booking.ticket
								income_facility_adult_data["department"] = single_facility["facility_analysis_code"]
								income_facility_adult_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_adult_data["base_amount"] = single_facility["adult_total"]
								
								data.append(income_facility_adult_data)
				
					if single_facility["credit_no_of_children"] > 0:
						if single_facility["vat"] & single_facility["catering_levy"]:
							# Credit Note Income (Facility Children)
							income_facility_child_data = {}
							income_facility_child_data["income_account"] = single_facility["facility_income_code"]
							income_facility_child_data["customer_reference"] = customer_reference
							income_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							income_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							income_facility_child_data["tran_quantity"] = 1
							income_facility_child_data["unit_price"] = single_facility["child_minus_taxes"]
							income_facility_child_data["vat"] = "V"
							income_facility_child_data["dc_marker"] = "D"
							income_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							income_facility_child_data["journal_type"] = "VI"
							income_facility_child_data["account_description"] = get_booking.ticket
							income_facility_child_data["department"] = single_facility["facility_analysis_code"]
							income_facility_child_data["outlet"] = single_facility["facility_outlet_code"]
							income_facility_child_data["base_amount"] = single_facility["child_minus_taxes"]
							
							data.append(income_facility_child_data)

							# Credit Note VAT (Facility Children)
							vat_facility_child_data = {}
							vat_facility_child_data["income_account"] = "510001"
							vat_facility_child_data["customer_reference"] = customer_reference
							vat_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							vat_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							vat_facility_child_data["tran_quantity"] = 1
							vat_facility_child_data["unit_price"] = single_facility["child_vat"]
							vat_facility_child_data["vat"] = "V"
							vat_facility_child_data["dc_marker"] = "D"
							vat_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							vat_facility_child_data["journal_type"] = "VI"
							vat_facility_child_data["account_description"] = get_booking.ticket
							vat_facility_child_data["department"] = "#"
							vat_facility_child_data["outlet"] = "#"
							vat_facility_child_data["base_amount"] = single_facility["child_vat"]
							
							data.append(vat_facility_child_data)

							# Credit Note Catering Levy (Facility Children)
							cater_facility_child_data = {}
							cater_facility_child_data["income_account"] = "511006"
							cater_facility_child_data["customer_reference"] = customer_reference
							cater_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							cater_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							cater_facility_child_data["tran_quantity"] = 1
							cater_facility_child_data["unit_price"] = single_facility["child_cater"]
							cater_facility_child_data["vat"] = "V"
							cater_facility_child_data["dc_marker"] = "D"
							cater_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							cater_facility_child_data["journal_type"] = "VI"
							cater_facility_child_data["account_description"] = get_booking.ticket
							cater_facility_child_data["department"] = "#"
							cater_facility_child_data["outlet"] = "#"
							cater_facility_child_data["base_amount"] = single_facility["child_cater"]
							
							data.append(cater_facility_child_data)

						else:
							if single_facility["vat"]:
								# Credit Note Income (Facility Children)
								income_facility_child_data = {}
								income_facility_child_data["income_account"] = single_facility["facility_income_code"]
								income_facility_child_data["customer_reference"] = customer_reference
								income_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_child_data["tran_quantity"] = 1
								income_facility_child_data["unit_price"] = single_facility["child_minus_vat"]
								income_facility_child_data["vat"] = "V"
								income_facility_child_data["dc_marker"] = "D"
								income_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_child_data["journal_type"] = "VI"
								income_facility_child_data["account_description"] = get_booking.ticket
								income_facility_child_data["department"] = single_facility["facility_analysis_code"]
								income_facility_child_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_child_data["base_amount"] = single_facility["child_minus_vat"]
								
								data.append(income_facility_child_data)

								# Credit Note VAT (Facility Children)
								vat_facility_child_data = {}
								vat_facility_child_data["income_account"] = "510001"
								vat_facility_child_data["customer_reference"] = customer_reference
								vat_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								vat_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								vat_facility_child_data["tran_quantity"] = 1
								vat_facility_child_data["unit_price"] = single_facility["child_vat"]
								vat_facility_child_data["vat"] = "V"
								vat_facility_child_data["dc_marker"] = "D"
								vat_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								vat_facility_child_data["journal_type"] = "VI"
								vat_facility_child_data["account_description"] = get_booking.ticket
								vat_facility_child_data["department"] = "#"
								vat_facility_child_data["outlet"] = "#"
								vat_facility_child_data["base_amount"] = single_facility["child_vat"]
								
								data.append(vat_facility_child_data)

							elif single_facility["catering_levy"]:
								# Credit Note Income (Facility Children)
								income_facility_child_data = {}
								income_facility_child_data["income_account"] = single_facility["facility_income_code"]
								income_facility_child_data["customer_reference"] = customer_reference
								income_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_child_data["tran_quantity"] = 1
								income_facility_child_data["unit_price"] = single_facility["child_minus_cater"]
								income_facility_child_data["vat"] = "V"
								income_facility_child_data["dc_marker"] = "D"
								income_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_child_data["journal_type"] = "VI"
								income_facility_child_data["account_description"] = get_booking.ticket
								income_facility_child_data["department"] = single_facility["facility_analysis_code"]
								income_facility_child_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_child_data["base_amount"] = single_facility["child_minus_cater"]
								
								data.append(income_facility_child_data)

								# Credit Note Catering Levy (Facility Children)
								cater_facility_child_data = {}
								cater_facility_child_data["income_account"] = "511006"
								cater_facility_child_data["customer_reference"] = customer_reference
								cater_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								cater_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								cater_facility_child_data["tran_quantity"] = 1
								cater_facility_child_data["unit_price"] = single_facility["child_cater"]
								cater_facility_child_data["vat"] = "V"
								cater_facility_child_data["dc_marker"] = "D"
								cater_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								cater_facility_child_data["journal_type"] = "VI"
								cater_facility_child_data["account_description"] = get_booking.ticket
								cater_facility_child_data["department"] = "#"
								cater_facility_child_data["outlet"] = "#"
								cater_facility_child_data["base_amount"] = single_facility["child_cater"]
								
								data.append(cater_facility_child_data)

							else:
								# Credit Note Income (Facility Children)
								income_facility_child_data = {}
								income_facility_child_data["income_account"] = single_facility["facility_income_code"]
								income_facility_child_data["customer_reference"] = customer_reference
								income_facility_child_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_child_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_child_data["tran_quantity"] = 1
								income_facility_child_data["unit_price"] = single_facility["child_total"]
								income_facility_child_data["vat"] = "V"
								income_facility_child_data["dc_marker"] = "D"
								income_facility_child_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_child_data["journal_type"] = "VI"
								income_facility_child_data["account_description"] = get_booking.ticket
								income_facility_child_data["department"] = single_facility["facility_analysis_code"]
								income_facility_child_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_child_data["base_amount"] = single_facility["child_total"]
								
								data.append(income_facility_child_data)
				
					if single_facility["vat"] & single_facility["catering_levy"]:
						if single_facility["fixed_minus_taxes"]:
							# Credit Note Income (Facility Fixed Charges)
							income_facility_fixed_data = {}
							income_facility_fixed_data["income_account"] = single_facility["facility_income_code"]
							income_facility_fixed_data["customer_reference"] = customer_reference
							income_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							income_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							income_facility_fixed_data["tran_quantity"] = 1
							income_facility_fixed_data["unit_price"] = single_facility["fixed_minus_taxes"]
							income_facility_fixed_data["vat"] = "V"
							income_facility_fixed_data["dc_marker"] = "D"
							income_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							income_facility_fixed_data["journal_type"] = "VI"
							income_facility_fixed_data["account_description"] = get_booking.ticket
							income_facility_fixed_data["department"] = single_facility["facility_analysis_code"]
							income_facility_fixed_data["outlet"] = single_facility["facility_outlet_code"]
							income_facility_fixed_data["base_amount"] = single_facility["fixed_minus_taxes"]
							
							data.append(income_facility_fixed_data)

							# Credit Note VAT (Facility Fixed Charges)
							vat_facility_fixed_data = {}
							vat_facility_fixed_data["income_account"] = "510001"
							vat_facility_fixed_data["customer_reference"] = customer_reference
							vat_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							vat_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							vat_facility_fixed_data["tran_quantity"] = 1
							vat_facility_fixed_data["unit_price"] = single_facility["fixed_vat"]
							vat_facility_fixed_data["vat"] = "V"
							vat_facility_fixed_data["dc_marker"] = "D"
							vat_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							vat_facility_fixed_data["journal_type"] = "VI"
							vat_facility_fixed_data["account_description"] = get_booking.ticket
							vat_facility_fixed_data["department"] = "#"
							vat_facility_fixed_data["outlet"] = "#"
							vat_facility_fixed_data["base_amount"] = single_facility["fixed_vat"]
							
							data.append(vat_facility_fixed_data)

							# Credit Note Catering Levy (Facility Fixed Charges)
							cater_facility_fixed_data = {}
							cater_facility_fixed_data["income_account"] = "511006"
							cater_facility_fixed_data["customer_reference"] = customer_reference
							cater_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
							cater_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
							cater_facility_fixed_data["tran_quantity"] = 1
							cater_facility_fixed_data["unit_price"] = single_facility["fixed_cater"]
							cater_facility_fixed_data["vat"] = "V"
							cater_facility_fixed_data["dc_marker"] = "D"
							cater_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
							cater_facility_fixed_data["journal_type"] = "VI"
							cater_facility_fixed_data["account_description"] = get_booking.ticket
							cater_facility_fixed_data["department"] = "#"
							cater_facility_fixed_data["outlet"] = "#"
							cater_facility_fixed_data["base_amount"] = single_facility["fixed_cater"]
							
							data.append(cater_facility_fixed_data)

					else:
						if single_facility["vat"]:
							if single_facility["fixed_minus_vat"]:
								# Credit Note Income (Facility Fixed Charges)
								income_facility_fixed_data = {}
								income_facility_fixed_data["income_account"] = single_facility["facility_income_code"]
								income_facility_fixed_data["customer_reference"] = customer_reference
								income_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_fixed_data["tran_quantity"] = 1
								income_facility_fixed_data["unit_price"] = single_facility["fixed_minus_vat"]
								income_facility_fixed_data["vat"] = "V"
								income_facility_fixed_data["dc_marker"] = "D"
								income_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_fixed_data["journal_type"] = "VI"
								income_facility_fixed_data["account_description"] = get_booking.ticket
								income_facility_fixed_data["department"] = single_facility["facility_analysis_code"]
								income_facility_fixed_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_fixed_data["base_amount"] = single_facility["fixed_minus_vat"]
								
								data.append(income_facility_fixed_data)

								# Credit Note VAT (Facility Fixed Charges)
								vat_facility_fixed_data = {}
								vat_facility_fixed_data["income_account"] = "510001"
								vat_facility_fixed_data["customer_reference"] = customer_reference
								vat_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								vat_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								vat_facility_fixed_data["tran_quantity"] = 1
								vat_facility_fixed_data["unit_price"] = single_facility["fixed_vat"]
								vat_facility_fixed_data["vat"] = "V"
								vat_facility_fixed_data["dc_marker"] = "D"
								vat_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								vat_facility_fixed_data["journal_type"] = "VI"
								vat_facility_fixed_data["account_description"] = get_booking.ticket
								vat_facility_fixed_data["department"] = "#"
								vat_facility_fixed_data["outlet"] = "#"
								vat_facility_fixed_data["base_amount"] = single_facility["fixed_vat"]
								
								data.append(vat_facility_fixed_data)

						elif single_facility["catering_levy"]:
							if single_facility["fixed_minus_cater"]:
								# Credit Note Income (Facility Fixed Charges)
								income_facility_fixed_data = {}
								income_facility_fixed_data["income_account"] = single_facility["facility_income_code"]
								income_facility_fixed_data["customer_reference"] = customer_reference
								income_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_fixed_data["tran_quantity"] = 1
								income_facility_fixed_data["unit_price"] = single_facility["fixed_minus_cater"]
								income_facility_fixed_data["vat"] = "V"
								income_facility_fixed_data["dc_marker"] = "D"
								income_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_fixed_data["journal_type"] = "VI"
								income_facility_fixed_data["account_description"] = get_booking.ticket
								income_facility_fixed_data["department"] = single_facility["facility_analysis_code"]
								income_facility_fixed_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_fixed_data["base_amount"] = single_facility["fixed_minus_cater"]
								
								data.append(income_facility_fixed_data)

								# Credit Note Catering Levy (Facility Fixed Charges)
								cater_facility_fixed_data = {}
								cater_facility_fixed_data["income_account"] = "511006"
								cater_facility_fixed_data["customer_reference"] = customer_reference
								cater_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								cater_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								cater_facility_fixed_data["tran_quantity"] = 1
								cater_facility_fixed_data["unit_price"] = single_facility["fixed_cater"]
								cater_facility_fixed_data["vat"] = "V"
								cater_facility_fixed_data["dc_marker"] = "D"
								cater_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								cater_facility_fixed_data["journal_type"] = "VI"
								cater_facility_fixed_data["account_description"] = get_booking.ticket
								cater_facility_fixed_data["department"] = "#"
								cater_facility_fixed_data["outlet"] = "#"
								cater_facility_fixed_data["base_amount"] = single_facility["fixed_cater"]
								
								data.append(cater_facility_fixed_data)

						else:
							if single_facility["fixed_total"]:
								# Credit Note Income (Facility Fixed Charges)
								income_facility_fixed_data = {}
								income_facility_fixed_data["income_account"] = single_facility["facility_income_code"]
								income_facility_fixed_data["customer_reference"] = customer_reference
								income_facility_fixed_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
								income_facility_fixed_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
								income_facility_fixed_data["tran_quantity"] = 1
								income_facility_fixed_data["unit_price"] = single_facility["fixed_total"]
								income_facility_fixed_data["vat"] = "V"
								income_facility_fixed_data["dc_marker"] = "D"
								income_facility_fixed_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
								income_facility_fixed_data["journal_type"] = "VI"
								income_facility_fixed_data["account_description"] = get_booking.ticket
								income_facility_fixed_data["department"] = single_facility["facility_analysis_code"]
								income_facility_fixed_data["outlet"] = single_facility["facility_outlet_code"]
								income_facility_fixed_data["base_amount"] = single_facility["fixed_total"]
								
								data.append(income_facility_fixed_data)
				
				booking_data = {}
				booking_data["income_account"] = credit_note_details["customer_code"]
				booking_data["customer_reference"] = customer_reference
				booking_data["currency"] = credit_note_currency.json()["data"][0]["currency_name"]
				booking_data["sales_date"] = str(single_credit_note.created_at.strftime("%d%m%Y"))
				booking_data["tran_quantity"] = 1
				booking_data["unit_price"] = credit_note_details["total"]
				booking_data["vat"] = "V"
				booking_data["dc_marker"] = "C"
				booking_data["conversion_rate"] = float(get_booking.currency_selling_rate_at_time)
				booking_data["journal_type"] = "VI"
				booking_data["account_description"] = get_booking.ticket
				booking_data["department"] = "#"
				booking_data["outlet"] = "#"
				booking_data["base_amount"] = credit_note_details["total"]
				
				data.append(booking_data)
	
	return jsonify({"data": data}), 200


@app.route("/credit/batchfile")
def generate_credit_note_batchfile():
	bookings_array = [
		"017a8b56-a",
		"c12a3178-d",
		"adbb105d-c",
		"eb716599-9",
		"9d8b00ce-e",
		"96505aae-3",
		"649d3ffd-4",
		"4dd15a27-2",
		"e7a87637-1",
		"1779f0be-4",
		"55c2fcc9-2"
	]
	
	select_bookings = db.session.query(Booking)\
								.join(Gatepass, Booking.booking_public_id ==  Gatepass.booking_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_ref_code, Booking.ticket,\
											 Booking.currency, Booking.booking_done_by, Booking.created_at,\
											 Booking.booking_check_in_date,\
											 Gatepass.gatepass_public_id, Gatepass.gatepass_date)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.created_at != None)\
								.filter(Booking.booking_ref_code.in_(bookings_array))\
								.filter(Booking.status != get_booking_status_id("Deleted"))\
								.filter(Booking.status != get_booking_status_id("Cancelled"))\
								.filter(Booking.status != get_booking_status_id("Abandoned"))\
								.filter(Gatepass.deletion_marker == None)\
								.all()

	yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
	
	existing_bookings = []
	
	data = []
	
	for single_booking in select_bookings:
		if single_booking.booking_public_id in existing_bookings:
			pass
		else:
			return_data = {}

			try:
				booking_currency = requests.get(get_currency.format(single_booking.currency))
				bookingTotal(return_data, single_booking.booking_public_id, True)
			except Exception:
				break

			check_school_booking = db.session.query(SchoolBooking)\
												.join(School, SchoolBooking.school_id == School.school_public_id)\
												.add_columns(School.school_name)\
												.filter(SchoolBooking.deletion_marker == None)\
												.filter(SchoolBooking.booking_id == single_booking.booking_public_id)\
												.first()
			
			if check_school_booking:
				try:
					booking_done_by = check_school_booking.school_name
				except Exception:
					booking_done_by = single_booking.booking_done_by
			else:
				booking_done_by = single_booking.booking_done_by
			
			customer_ref = single_booking.booking_ref_code + " " + booking_done_by

			if len(customer_ref) >= 30:
				customer_reference = customer_ref[:29]
			else:
				customer_reference = customer_ref

			for single_gatepass_guest in return_data["guests"]:
				if single_gatepass_guest["payment_guests"] > 0:
					if single_booking.booking_ref_code == "491a14d1-4":
						## Income (Guests) ##
						income_gatepass_data = {}
						income_gatepass_data["income_account"] = single_gatepass_guest["payment_person_income_code"]
						income_gatepass_data["customer_reference"] = customer_reference
						
						# income_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
						# income_gatepass_data["currency"] = single_gatepass_guest["currency"]
						income_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
						
						income_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# income_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
						income_gatepass_data["tran_quantity"] = 2
						income_gatepass_data["unit_price"] = round(single_gatepass_guest["price_minus_vat"], 2)
						income_gatepass_data["vat"] = "V"
						income_gatepass_data["dc_marker"] = "D"
						# income_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
						income_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						income_gatepass_data["journal_type"] = "VI"
						income_gatepass_data["account_description"] = single_booking.ticket
						income_gatepass_data["department"] = single_gatepass_guest["payment_person_dept_code"]
						income_gatepass_data["outlet"] = single_gatepass_guest["destination_outlet_code"]
						income_gatepass_data["base_amount"] = round(single_gatepass_guest["price_minus_vat"], 2)

						data.append(income_gatepass_data)

						## VAT (Guests) ##
						vat_gatepass_data = {}
						vat_gatepass_data["income_account"] = "510001"
						vat_gatepass_data["customer_reference"] = customer_reference
						
						# vat_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
						# vat_gatepass_data["currency"] = single_gatepass_guest["currency"]
						vat_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
						
						vat_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# vat_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
						vat_gatepass_data["tran_quantity"] = 2
						vat_gatepass_data["unit_price"] = round(single_gatepass_guest["vat"], 2)
						vat_gatepass_data["vat"] = "V"
						vat_gatepass_data["dc_marker"] = "D"
						# vat_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
						vat_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						vat_gatepass_data["journal_type"] = "VI"
						vat_gatepass_data["account_description"] = single_booking.ticket
						vat_gatepass_data["outlet"] = "#"
						vat_gatepass_data["department"] = "#"
						vat_gatepass_data["base_amount"] = round(single_gatepass_guest["vat"], 2)

						data.append(vat_gatepass_data)

					else:
							## Income (Guests) ##
						income_gatepass_data = {}
						income_gatepass_data["income_account"] = single_gatepass_guest["payment_person_income_code"]
						income_gatepass_data["customer_reference"] = customer_reference
						
						# income_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
						# income_gatepass_data["currency"] = single_gatepass_guest["currency"]
						income_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
						
						income_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# income_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
						income_gatepass_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of people is already done
						income_gatepass_data["unit_price"] = round(single_gatepass_guest["price_minus_vat"], 2)
						income_gatepass_data["vat"] = "V"
						income_gatepass_data["dc_marker"] = "D"
						# income_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
						income_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						income_gatepass_data["journal_type"] = "VI"
						income_gatepass_data["account_description"] = single_booking.ticket
						income_gatepass_data["department"] = single_gatepass_guest["payment_person_dept_code"]
						income_gatepass_data["outlet"] = single_gatepass_guest["destination_outlet_code"]
						income_gatepass_data["base_amount"] = round(single_gatepass_guest["base_amount_minus_vat"], 2)

						data.append(income_gatepass_data)

						## VAT (Guests) ##
						vat_gatepass_data = {}
						vat_gatepass_data["income_account"] = "510001"
						vat_gatepass_data["customer_reference"] = customer_reference
						
						# vat_gatepass_data["currency_code"] = single_gatepass_guest["gatepass_guest_currency_at_time"]
						# vat_gatepass_data["currency"] = single_gatepass_guest["currency"]
						vat_gatepass_data["currency"] = single_gatepass_guest["payment_person_currency"]
						
						vat_gatepass_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
						# vat_gatepass_data["tran_quantity"] = single_gatepass_guest["payment_guests"]
						vat_gatepass_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of people is already done
						vat_gatepass_data["unit_price"] = round(single_gatepass_guest["vat"], 2)
						vat_gatepass_data["vat"] = "V"
						vat_gatepass_data["dc_marker"] = "D"
						# vat_gatepass_data["conversion_rate"] = single_gatepass_guest["gatepass_guest_rate_at_time"]
						vat_gatepass_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
						vat_gatepass_data["journal_type"] = "VI"
						vat_gatepass_data["account_description"] = single_booking.ticket
						vat_gatepass_data["outlet"] = "#"
						vat_gatepass_data["department"] = "#"
						vat_gatepass_data["base_amount"] = round(single_gatepass_guest["base_vat"], 2)

						data.append(vat_gatepass_data)

			for single_gatepass_vehicle in return_data["vehicles"]:
				if single_gatepass_vehicle["vehicles"] > 0:
					## Income (Vehicles) ##
					income_gatepass_vehicle_data = {}
					income_gatepass_vehicle_data["income_account"] = single_gatepass_vehicle["vehicle_charge_income_code"]
					income_gatepass_vehicle_data["customer_reference"] = customer_reference
					
					# income_gatepass_vehicle_data["currency_code"] = single_gatepass_vehicle["gatepass_vehicle_currency_at_time"]
					# income_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["currency"]
					income_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["vehicle_charge_cost_currency"]
					
					income_gatepass_vehicle_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
					# income_gatepass_vehicle_data["tran_quantity"] = single_gatepass_vehicle["vehicles"]
					income_gatepass_vehicle_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of vehicles is already done
					income_gatepass_vehicle_data["unit_price"] = round(single_gatepass_vehicle["price_minus_vat"], 2)
					income_gatepass_vehicle_data["vat"] = "V"
					income_gatepass_vehicle_data["dc_marker"] = "D"
					# income_gatepass_vehicle_data["conversion_rate"] = single_gatepass_vehicle["gatepass_vehicle_rate_at_time"]
					income_gatepass_vehicle_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
					income_gatepass_vehicle_data["journal_type"] = "VI"
					income_gatepass_vehicle_data["account_description"] = single_booking.ticket
					income_gatepass_vehicle_data["outlet"] = single_gatepass_vehicle["vehicle_charge_outlet_code"]
					income_gatepass_vehicle_data["department"] = single_gatepass_vehicle["vehicle_charge_dept_code"]
					income_gatepass_vehicle_data["base_amount"] = round(single_gatepass_vehicle["base_amount_minus_vat"], 2)

					data.append(income_gatepass_vehicle_data)

					## VAT (Vehicles) ##
					vat_gatepass_vehicle_data = {}
					vat_gatepass_vehicle_data["income_account"] = "510001"
					vat_gatepass_vehicle_data["customer_reference"] = customer_reference
					
					# vat_gatepass_vehicle_data["currency_code"] = single_gatepass_vehicle["gatepass_vehicle_currency_at_time"]
					# vat_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["currency"]
					vat_gatepass_vehicle_data["currency"] = single_gatepass_vehicle["vehicle_charge_cost_currency"]
					
					vat_gatepass_vehicle_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
					# vat_gatepass_vehicle_data["tran_quantity"] = single_gatepass_vehicle["vehicles"]
					vat_gatepass_vehicle_data["tran_quantity"] = 1 ## This is set to 1 since the multiplication by number of vehicles is already done
					vat_gatepass_vehicle_data["unit_price"] = round(single_gatepass_vehicle["vat"], 2)
					vat_gatepass_vehicle_data["vat"] = "V"
					vat_gatepass_vehicle_data["dc_marker"] = "D"
					# vat_gatepass_vehicle_data["conversion_rate"] = single_gatepass_vehicle["gatepass_vehicle_rate_at_time"]
					vat_gatepass_vehicle_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
					vat_gatepass_vehicle_data["journal_type"] = "VI"
					vat_gatepass_vehicle_data["account_description"] = single_booking.ticket
					vat_gatepass_vehicle_data["outlet"] = "#"
					vat_gatepass_vehicle_data["department"] = "#"
					vat_gatepass_vehicle_data["base_amount"] = round(single_gatepass_vehicle["base_vat"], 2)

					data.append(vat_gatepass_vehicle_data)

			# data.append(return_data["inventory_bookings"])
			
			if return_data["inventory_bookings"]:
				for single_inventory in return_data["inventory_bookings"]:
					if single_inventory["inventory_booking_adults"] > 0:
						if single_inventory["vat"] & single_inventory["catering_levy"]:
							## Income (Inventory Adult Bookings) ##
							income_adult_inventory_data = {}
							income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
							income_adult_inventory_data["customer_reference"] = customer_reference
							
							# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
							# income_adult_inventory_data["currency"] = single_inventory["currency"]
							income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
							
							income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
							income_adult_inventory_data["unit_price"] = round(single_inventory["adult_minus_taxes"], 2)
							income_adult_inventory_data["vat"] = "V"
							income_adult_inventory_data["dc_marker"] = "D"
							# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
							income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							income_adult_inventory_data["journal_type"] = "VI"
							income_adult_inventory_data["account_description"] = single_booking.ticket
							income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
							income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
							income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_taxes"], 2)

							data.append(income_adult_inventory_data)

							## VAT (Inventory Adult Bookings) ##
							vat_adult_inventory_data = {}
							vat_adult_inventory_data["income_account"] = "510001"
							vat_adult_inventory_data["customer_reference"] = customer_reference
							
							# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
							# vat_adult_inventory_data["currency"] = single_inventory["currency"]
							vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
							
							vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
							vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
							vat_adult_inventory_data["vat"] = "V"
							vat_adult_inventory_data["dc_marker"] = "D"
							# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
							vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							vat_adult_inventory_data["journal_type"] = "VI"
							vat_adult_inventory_data["account_description"] = single_booking.ticket
							vat_adult_inventory_data["outlet"] = "#"
							vat_adult_inventory_data["department"] = "#"
							vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

							data.append(vat_adult_inventory_data)

							## Catering Levy (Inventory Adult Bookings) ##
							cater_adult_inventory_data = {}
							cater_adult_inventory_data["income_account"] = "511006"
							cater_adult_inventory_data["customer_reference"] = customer_reference
							
							# cater_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
							# cater_adult_inventory_data["currency"] = single_inventory["currency"]
							cater_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
							
							cater_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							cater_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
							cater_adult_inventory_data["unit_price"] = round(single_inventory["adult_cater"], 2)
							cater_adult_inventory_data["vat"] = None
							cater_adult_inventory_data["dc_marker"] = "D"
							# cater_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
							cater_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							cater_adult_inventory_data["journal_type"] = "VI"
							cater_adult_inventory_data["account_description"] = single_booking.ticket
							cater_adult_inventory_data["outlet"] = "#"
							cater_adult_inventory_data["department"] = "#"
							cater_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_cater"], 2)

							data.append(cater_adult_inventory_data)

						else:						
							if single_inventory["vat"]:
								if single_booking.booking_ref_code == "5787740c-8":
									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_vat"], 2)
									income_adult_inventory_data["vat"] = "V"
									income_adult_inventory_data["dc_marker"] = "D"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_vat"], 2)

									data.append(income_adult_inventory_data)

									## VAT (Inventory Adult Bookings) ##
									vat_adult_inventory_data = {}
									vat_adult_inventory_data["income_account"] = "510001"
									vat_adult_inventory_data["customer_reference"] = customer_reference
									
									# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_adult_inventory_data["currency"] = single_inventory["currency"]
									vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
									vat_adult_inventory_data["vat"] = "V"
									vat_adult_inventory_data["dc_marker"] = "D"
									# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_inventory_data["journal_type"] = "VI"
									vat_adult_inventory_data["account_description"] = single_booking.ticket
									vat_adult_inventory_data["outlet"] = "#"
									vat_adult_inventory_data["department"] = "#"
									vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

									data.append(vat_adult_inventory_data)

									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = "406GA002"
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_vat"], 2)
									income_adult_inventory_data["vat"] = "V"
									income_adult_inventory_data["dc_marker"] = "C"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = 100.262
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_vat"], 2)

									data.append(income_adult_inventory_data)

									## VAT (Inventory Adult Bookings) ##
									vat_adult_inventory_data = {}
									vat_adult_inventory_data["income_account"] = "510001"
									vat_adult_inventory_data["customer_reference"] = customer_reference
									
									# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_adult_inventory_data["currency"] = single_inventory["currency"]
									vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
									vat_adult_inventory_data["vat"] = "V"
									vat_adult_inventory_data["dc_marker"] = "C"
									# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_adult_inventory_data["conversion_rate"] = 100.262
									vat_adult_inventory_data["journal_type"] = "VI"
									vat_adult_inventory_data["account_description"] = single_booking.ticket
									vat_adult_inventory_data["outlet"] = "#"
									vat_adult_inventory_data["department"] = "#"
									vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

									data.append(vat_adult_inventory_data)

								else:
									## Income (Inventory Adult Bookings) ##
									income_adult_inventory_data = {}
									income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
									income_adult_inventory_data["customer_reference"] = customer_reference
									
									# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# income_adult_inventory_data["currency"] = single_inventory["currency"]
									income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_vat"], 2)
									income_adult_inventory_data["vat"] = "V"
									income_adult_inventory_data["dc_marker"] = "D"
									# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_inventory_data["journal_type"] = "VI"
									income_adult_inventory_data["account_description"] = single_booking.ticket
									income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
									income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
									income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_vat"], 2)

									data.append(income_adult_inventory_data)

									## VAT (Inventory Adult Bookings) ##
									vat_adult_inventory_data = {}
									vat_adult_inventory_data["income_account"] = "510001"
									vat_adult_inventory_data["customer_reference"] = customer_reference
									
									# vat_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
									# vat_adult_inventory_data["currency"] = single_inventory["currency"]
									vat_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
									
									vat_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
									vat_adult_inventory_data["unit_price"] = round(single_inventory["adult_vat"], 2)
									vat_adult_inventory_data["vat"] = "V"
									vat_adult_inventory_data["dc_marker"] = "D"
									# vat_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
									vat_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_inventory_data["journal_type"] = "VI"
									vat_adult_inventory_data["account_description"] = single_booking.ticket
									vat_adult_inventory_data["outlet"] = "#"
									vat_adult_inventory_data["department"] = "#"
									vat_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_vat"], 2)

									data.append(vat_adult_inventory_data)

							else:
								## Income (Inventory Adult Bookings) ##
								income_adult_inventory_data = {}
								income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_adult_inventory_data["customer_reference"] = customer_reference
								
								# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_adult_inventory_data["currency"] = single_inventory["currency"]
								income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
								income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price"], 2)
								income_adult_inventory_data["vat"] = "V"
								income_adult_inventory_data["dc_marker"] = "D"
								# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_adult_inventory_data["journal_type"] = "VI"
								income_adult_inventory_data["account_description"] = single_booking.ticket
								income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price"], 2)

								data.append(income_adult_inventory_data)

							if single_inventory["catering_levy"]:
								## Income (Inventory Adult Bookings) ##
								income_adult_inventory_data = {}
								income_adult_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_adult_inventory_data["customer_reference"] = customer_reference
								
								# income_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_adult_inventory_data["currency"] = single_inventory["currency"]
								income_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
								income_adult_inventory_data["unit_price"] = round(single_inventory["adult_price_minus_cater"], 2)
								income_adult_inventory_data["vat"] = None
								income_adult_inventory_data["dc_marker"] = "D"
								# income_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_adult_inventory_data["journal_type"] = "VI"
								income_adult_inventory_data["account_description"] = single_booking.ticket
								income_adult_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_adult_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_price_minus_cater"], 2)

								data.append(income_adult_inventory_data)

								## Catering Levy (Inventory Adult Bookings) ##
								cater_adult_inventory_data = {}
								cater_adult_inventory_data["income_account"] = "511006"
								cater_adult_inventory_data["customer_reference"] = customer_reference
								
								# cater_adult_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# cater_adult_inventory_data["currency"] = single_inventory["currency"]
								cater_adult_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								cater_adult_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_adult_inventory_data["tran_quantity"] = single_inventory["inventory_booking_adults"]
								cater_adult_inventory_data["unit_price"] = round(single_inventory["adult_cater"], 2)
								cater_adult_inventory_data["vat"] = None
								cater_adult_inventory_data["dc_marker"] = "D"
								# cater_adult_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								cater_adult_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_adult_inventory_data["journal_type"] = "VI"
								cater_adult_inventory_data["account_description"] = single_booking.ticket
								cater_adult_inventory_data["outlet"] = "#"
								cater_adult_inventory_data["department"] = "#"
								cater_adult_inventory_data["base_amount"] = round(single_inventory["base_adult_cater"], 2)

								data.append(cater_adult_inventory_data)
			
					if single_inventory["inventory_booking_children"] > 0:
						if single_inventory["vat"] & single_inventory["catering_levy"]:
							## Income (Inventory Child Bookings) ##
							income_child_inventory_data = {}
							income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
							income_child_inventory_data["customer_reference"] = customer_reference
							
							# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
							# income_child_inventory_data["currency"] = single_inventory["currency"]
							income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
							
							income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
							income_child_inventory_data["unit_price"] = round(single_inventory["child_minus_taxes"], 2)
							income_child_inventory_data["vat"] = "V"
							income_child_inventory_data["dc_marker"] = "D"
							# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
							income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							income_child_inventory_data["journal_type"] = "VI"
							income_child_inventory_data["account_description"] = single_booking.ticket
							income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
							income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
							income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_taxes"], 2)

							data.append(income_child_inventory_data)

							## VAT (VAT Child Bookings) ##
							vat_child_inventory_data = {}
							vat_child_inventory_data["income_account"] = "510001"
							vat_child_inventory_data["customer_reference"] = customer_reference
							
							# vat_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
							# vat_child_inventory_data["currency"] = single_inventory["currency"]
							vat_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
							
							vat_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							vat_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
							vat_child_inventory_data["unit_price"] = round(single_inventory["child_vat"], 2)
							vat_child_inventory_data["vat"] = "V"
							vat_child_inventory_data["dc_marker"] = "D"
							# vat_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
							vat_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							vat_child_inventory_data["journal_type"] = "VI"
							vat_child_inventory_data["account_description"] = single_booking.ticket
							vat_child_inventory_data["outlet"] = "#"
							vat_child_inventory_data["department"] = "#"
							vat_child_inventory_data["base_amount"] = round(single_inventory["base_child_vat"], 2)

							data.append(vat_child_inventory_data)

							## Catering Levy (Inventory Child Bookings) ##
							cater_child_inventory_data = {}
							cater_child_inventory_data["income_account"] = "511006"
							cater_child_inventory_data["customer_reference"] = customer_reference
							
							# cater_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
							# cater_child_inventory_data["currency"] = single_inventory["currency"]
							cater_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
							
							cater_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
							cater_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
							cater_child_inventory_data["unit_price"] = round(single_inventory["child_cater"], 2)
							cater_child_inventory_data["vat"] = None
							cater_child_inventory_data["dc_marker"] = "D"
							# cater_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
							cater_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
							cater_child_inventory_data["journal_type"] = "VI"
							cater_child_inventory_data["account_description"] = single_booking.ticket
							cater_child_inventory_data["outlet"] = "#"
							cater_child_inventory_data["department"] = "#"
							cater_child_inventory_data["base_amount"] = round(single_inventory["base_child_cater"], 2)

							data.append(cater_child_inventory_data)
						
						else:
							if single_inventory["vat"]:
								## Income (Inventory Child Bookings) ##
								income_child_inventory_data = {}
								income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_child_inventory_data["customer_reference"] = customer_reference
								
								# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_child_inventory_data["currency"] = single_inventory["currency"]
								income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								income_child_inventory_data["unit_price"] = round(single_inventory["child_price_minus_vat"], 2)
								income_child_inventory_data["vat"] = "V"
								income_child_inventory_data["dc_marker"] = "D"
								# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_child_inventory_data["journal_type"] = "VI"
								income_child_inventory_data["account_description"] = single_booking.ticket
								income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_vat"], 2)

								data.append(income_child_inventory_data)

								## VAT (VAT Child Bookings) ##
								vat_child_inventory_data = {}
								vat_child_inventory_data["income_account"] = "510001"
								vat_child_inventory_data["customer_reference"] = customer_reference
								
								# vat_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# vat_child_inventory_data["currency"] = single_inventory["currency"]
								vat_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								vat_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								vat_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								vat_child_inventory_data["unit_price"] = round(single_inventory["child_vat"], 2)
								vat_child_inventory_data["vat"] = "V"
								vat_child_inventory_data["dc_marker"] = "D"
								# vat_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								vat_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								vat_child_inventory_data["journal_type"] = "VI"
								vat_child_inventory_data["account_description"] = single_booking.ticket
								vat_child_inventory_data["outlet"] = "#"
								vat_child_inventory_data["department"] = "#"
								vat_child_inventory_data["base_amount"] = round(single_inventory["base_child_vat"], 2)

								data.append(vat_child_inventory_data)

							else:
								## Income (Inventory Child Bookings) ##
								income_child_inventory_data = {}
								income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_child_inventory_data["customer_reference"] = customer_reference
								
								# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_child_inventory_data["currency"] = single_inventory["currency"]
								income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								income_child_inventory_data["unit_price"] = round(single_inventory["child_price"], 2)
								income_child_inventory_data["vat"] = "V"
								income_child_inventory_data["dc_marker"] = "D"
								# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_child_inventory_data["journal_type"] = "VI"
								income_child_inventory_data["account_description"] = single_booking.ticket
								income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price"], 2)

								data.append(income_child_inventory_data)
							
							if single_inventory["catering_levy"]:
								## Income (Inventory Child Bookings) ##
								income_child_inventory_data = {}
								income_child_inventory_data["income_account"] = single_inventory["inventory_income_code"]
								income_child_inventory_data["customer_reference"] = customer_reference
								
								# income_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# income_child_inventory_data["currency"] = single_inventory["currency"]
								income_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								income_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								income_child_inventory_data["unit_price"] = round(single_inventory["child_price_minus_cater"], 2)
								income_child_inventory_data["vat"] = None
								income_child_inventory_data["dc_marker"] = "D"
								# income_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								income_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_child_inventory_data["journal_type"] = "VI"
								income_child_inventory_data["account_description"] = single_booking.ticket
								income_child_inventory_data["outlet"] = single_inventory["inventory_outlet_code"]
								income_child_inventory_data["department"] = single_inventory["inventory_dept_analysis_code"]
								income_child_inventory_data["base_amount"] = round(single_inventory["base_child_price_minus_cater"], 2)

								data.append(income_child_inventory_data)

								## Catering Levy (Inventory Child Bookings) ##
								cater_child_inventory_data = {}
								cater_child_inventory_data["income_account"] = "511006"
								cater_child_inventory_data["customer_reference"] = customer_reference
								
								# cater_child_inventory_data["currency_code"] = single_inventory["inventory_currency_at_time"]
								# cater_child_inventory_data["currency"] = single_inventory["currency"]
								cater_child_inventory_data["currency"] = single_inventory["inventory_booking_currency"]
								
								cater_child_inventory_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_child_inventory_data["tran_quantity"] = single_inventory["inventory_booking_children"]
								cater_child_inventory_data["unit_price"] = round(single_inventory["child_cater"], 2)
								cater_child_inventory_data["vat"] = None
								cater_child_inventory_data["dc_marker"] = "D"
								# cater_child_inventory_data["conversion_rate"] = single_inventory["inventory_rate_at_time"]
								cater_child_inventory_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_child_inventory_data["journal_type"] = "VI"
								cater_child_inventory_data["account_description"] = single_booking.ticket
								cater_child_inventory_data["outlet"] = "#"
								cater_child_inventory_data["department"] = "#"
								cater_child_inventory_data["base_amount"] = round(single_inventory["base_child_cater"], 2)

								data.append(cater_child_inventory_data)
			
			if return_data["facility_bookings"]:
				for single_facility in return_data["facility_bookings"]:
					if single_facility["facility_type"] == "Accomodation":
						if single_facility["accomodation_type"] == "Stables":
							if single_facility["vat"] & single_facility["catering_levy"]:
								if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
									## Income (Facility (Stables) Fixed Rate) ##
									income_fixed_stables_data = {}
									income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
									income_fixed_stables_data["customer_reference"] = customer_reference
									income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
									income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
									income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									income_fixed_stables_data["vat"] = "V"
									income_fixed_stables_data["dc_marker"] = "D"
									income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_fixed_stables_data["journal_type"] = "VI"
									income_fixed_stables_data["account_description"] = single_booking.ticket
									income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
									income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_taxes"], 2)

									data.append(income_fixed_stables_data)

									## VAT (Facility (Stables) Fixed Rate) ##
									vat_fixed_stables_data = {}
									vat_fixed_stables_data["income_account"] = "510001"
									vat_fixed_stables_data["customer_reference"] = customer_reference
									vat_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
									vat_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
									vat_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									vat_fixed_stables_data["vat"] = "V"
									vat_fixed_stables_data["dc_marker"] = "D"
									vat_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_fixed_stables_data["journal_type"] = "VI"
									vat_fixed_stables_data["account_description"] = single_booking.ticket
									vat_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
									vat_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_vat"], 2)

									data.append(vat_fixed_stables_data)

									## Catering Levy (Facility (Stables) Fixed Rate) ##
									cater_fixed_stables_data = {}
									cater_fixed_stables_data["income_account"] = "511006"
									cater_fixed_stables_data["customer_reference"] = customer_reference
									cater_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
									cater_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
									cater_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									cater_fixed_stables_data["vat"] = "V"
									cater_fixed_stables_data["dc_marker"] = "D"
									cater_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_fixed_stables_data["journal_type"] = "VI"
									cater_fixed_stables_data["account_description"] = single_booking.ticket
									cater_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
									cater_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									cater_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_cater"], 2)

									data.append(cater_fixed_stables_data)
								
								if single_facility["facility_booking_extra_adults"] > 0:
									## Income (Facility (Stables) Extra Adult Bookings) ##
									income_adult_stables_data = {}
									income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
									income_adult_stables_data["customer_reference"] = customer_reference
									income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
									income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
									income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									income_adult_stables_data["vat"] = "V"
									income_adult_stables_data["dc_marker"] = "D"
									income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_stables_data["journal_type"] = "VI"
									income_adult_stables_data["account_description"] = single_booking.ticket
									income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
									income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_taxes"], 2)

									data.append(income_adult_stables_data)

									## VAT (Facility (Stables) Extra Adult Bookings) ##
									vat_adult_stables_data = {}
									vat_adult_stables_data["income_account"] = "510001"
									vat_adult_stables_data["customer_reference"] = customer_reference
									vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
									vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
									vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									vat_adult_stables_data["vat"] = "V"
									vat_adult_stables_data["dc_marker"] = "D"
									vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_stables_data["journal_type"] = "VI"
									vat_adult_stables_data["account_description"] = single_booking.ticket
									vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
									vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_vat"], 2)

									data.append(vat_adult_stables_data)

									## Catering Levy (Facility (Stables) Extra Adult Bookings) ##
									cater_adult_stables_data = {}
									cater_adult_stables_data["income_account"] = "511006"
									cater_adult_stables_data["customer_reference"] = customer_reference
									cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
									cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
									cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									cater_adult_stables_data["vat"] = "V"
									cater_adult_stables_data["dc_marker"] = "D"
									cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_adult_stables_data["journal_type"] = "VI"
									cater_adult_stables_data["account_description"] = single_booking.ticket
									cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
									cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									cater_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_cater"], 2)

									data.append(cater_adult_stables_data)

								if single_facility["facility_booking_extra_children"] > 0:
									## Income (Facility (Stables) Extra Children Bookings) ##
									income_child_stables_data = {}
									income_child_stables_data["income_account"] = single_facility["facility_income_code"]
									income_child_stables_data["customer_reference"] = customer_reference
									income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
									income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
									income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									income_child_stables_data["vat"] = "V"
									income_child_stables_data["dc_marker"] = "D"
									income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_stables_data["journal_type"] = "VI"
									income_child_stables_data["account_description"] = single_booking.ticket
									income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
									income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_taxes"], 2)

									data.append(income_child_stables_data)

									## VAT (Facility (Stables) Extra Children Bookings) ##
									vat_child_stables_data = {}
									vat_child_stables_data["income_account"] = "510001"
									vat_child_stables_data["customer_reference"] = customer_reference
									vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
									vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
									vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									vat_child_stables_data["vat"] = "V"
									vat_child_stables_data["dc_marker"] = "D"
									vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_child_stables_data["journal_type"] = "VI"
									vat_child_stables_data["account_description"] = single_booking.ticket
									vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
									vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_child_stables_data["base_amount"] = round(single_facility["base_extra_child_vat"], 2)

									data.append(vat_child_stables_data)

									## Catering Levy (Facility (Stables) Extra Children Bookings) ##
									cater_child_stables_data = {}
									cater_child_stables_data["income_account"] = "511006"
									cater_child_stables_data["customer_reference"] = customer_reference
									cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
									cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
									cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									cater_child_stables_data["vat"] = "V"
									cater_child_stables_data["dc_marker"] = "D"
									cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_child_stables_data["journal_type"] = "VI"
									cater_child_stables_data["account_description"] = single_booking.ticket
									cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
									cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
									cater_child_stables_data["base_amount"] = round(single_facility["base_extra_child_cater"], 2)

									data.append(cater_child_stables_data)
							
							else:
								if single_facility["vat"]:
									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
										## Income (Facility (Stables) Fixed Rate) ##
										income_fixed_stables_data = {}
										income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
										income_fixed_stables_data["customer_reference"] = customer_reference
										income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_fixed_stables_data["vat"] = "V"
										income_fixed_stables_data["dc_marker"] = "D"
										income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_fixed_stables_data["journal_type"] = "VI"
										income_fixed_stables_data["account_description"] = single_booking.ticket
										income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_vat"], 2)

										data.append(income_fixed_stables_data)

										## VAT (Facility (Stables) Fixed Rate) ##
										vat_fixed_stables_data = {}
										vat_fixed_stables_data["income_account"] = "510001"
										vat_fixed_stables_data["customer_reference"] = customer_reference
										vat_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										vat_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										vat_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_fixed_stables_data["vat"] = "V"
										vat_fixed_stables_data["dc_marker"] = "D"
										vat_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_fixed_stables_data["journal_type"] = "VI"
										vat_fixed_stables_data["account_description"] = single_booking.ticket
										vat_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										vat_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_vat"], 2)

										data.append(vat_fixed_stables_data)
									
									if single_facility["facility_booking_extra_adults"] > 0:
										## Income (Facility (Stables) Extra Adult Bookings) ##
										income_adult_stables_data = {}
										income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										income_adult_stables_data["customer_reference"] = customer_reference
										income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_adult_stables_data["vat"] = "V"
										income_adult_stables_data["dc_marker"] = "D"
										income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_stables_data["journal_type"] = "VI"
										income_adult_stables_data["account_description"] = single_booking.ticket
										income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_vat"], 2)

										data.append(income_adult_stables_data)

										## VAT (Facility (Stables) Extra Adult Bookings) ##
										vat_adult_stables_data = {}
										vat_adult_stables_data["income_account"] = "510001"
										vat_adult_stables_data["customer_reference"] = customer_reference
										vat_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										vat_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										vat_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_adult_stables_data["vat"] = "V"
										vat_adult_stables_data["dc_marker"] = "D"
										vat_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_adult_stables_data["journal_type"] = "VI"
										vat_adult_stables_data["account_description"] = single_booking.ticket
										vat_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										vat_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_vat"], 2)

										data.append(vat_adult_stables_data)

									if single_facility["facility_booking_extra_children"] > 0:
										## Income (Facility (Stables) Extra Children Bookings) ##
										income_child_stables_data = {}
										income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										income_child_stables_data["customer_reference"] = customer_reference
										income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_child_stables_data["vat"] = "V"
										income_child_stables_data["dc_marker"] = "D"
										income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_stables_data["journal_type"] = "VI"
										income_child_stables_data["account_description"] = single_booking.ticket
										income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_vat"], 2)

										data.append(income_child_stables_data)

										## VAT (Facility (Stables) Extra Children Bookings) ##
										vat_child_stables_data = {}
										vat_child_stables_data["income_account"] = "510001"
										vat_child_stables_data["customer_reference"] = customer_reference
										vat_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										vat_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										vat_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										vat_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										vat_child_stables_data["vat"] = "V"
										vat_child_stables_data["dc_marker"] = "D"
										vat_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										vat_child_stables_data["journal_type"] = "VI"
										vat_child_stables_data["account_description"] = single_booking.ticket
										vat_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										vat_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										vat_child_stables_data["base_amount"] = round(single_facility["base_extra_child_vat"], 2)

										data.append(vat_child_stables_data)
								else:
									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
										## Income (Facility (Stables) Fixed Rate) ##
										income_fixed_stables_data = {}
										income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
										income_fixed_stables_data["customer_reference"] = customer_reference
										income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_fixed_stables_data["vat"] = "V"
										income_fixed_stables_data["dc_marker"] = "D"
										income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_fixed_stables_data["journal_type"] = "VI"
										income_fixed_stables_data["account_description"] = single_booking.ticket
										income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed"], 2)

										data.append(income_fixed_stables_data)
									
									if single_facility["facility_booking_extra_adults"] > 0:
										## Income (Facility (Stables) Adult Bookings) ##
										income_adult_stables_data = {}
										income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										income_adult_stables_data["customer_reference"] = customer_reference
										income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_adult_stables_data["vat"] = "V"
										income_adult_stables_data["dc_marker"] = "D"
										income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_stables_data["journal_type"] = "VI"
										income_adult_stables_data["account_description"] = single_booking.ticket
										income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult"], 2)

										data.append(income_adult_stables_data)

									if single_facility["facility_booking_extra_children"] > 0:
										## Income (Facility (Stables) Children Bookings) ##
										income_child_stables_data = {}
										income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										income_child_stables_data["customer_reference"] = customer_reference
										income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_child_stables_data["vat"] = "V"
										income_child_stables_data["dc_marker"] = "D"
										income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_stables_data["journal_type"] = "VI"
										income_child_stables_data["account_description"] = single_booking.ticket
										income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_stables_data["base_amount"] = round(single_facility["base_extra_child"], 2)

										data.append(income_child_stables_data)

								if single_facility["catering_levy"]:
									if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
										## Income (Facility (Stables) Fixed Rate) ##
										income_fixed_stables_data = {}
										income_fixed_stables_data["income_account"] = single_facility["facility_income_code"]
										income_fixed_stables_data["customer_reference"] = customer_reference
										income_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										income_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_fixed_stables_data["vat"] = "V"
										income_fixed_stables_data["dc_marker"] = "D"
										income_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_fixed_stables_data["journal_type"] = "VI"
										income_fixed_stables_data["account_description"] = single_booking.ticket
										income_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_minus_cater"], 2)

										data.append(income_fixed_stables_data)

										## Catering Levy (Facility (Stables) Fixed Rate) ##
										cater_fixed_stables_data = {}
										cater_fixed_stables_data["income_account"] = "511006"
										cater_fixed_stables_data["customer_reference"] = customer_reference
										cater_fixed_stables_data["currency"] = single_facility["facility_booking_currency"]
										cater_fixed_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_fixed_stables_data["tran_quantity"] = 1 ## Multiplication by number of guests done
										cater_fixed_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_fixed_stables_data["vat"] = "V"
										cater_fixed_stables_data["dc_marker"] = "D"
										cater_fixed_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_fixed_stables_data["journal_type"] = "VI"
										cater_fixed_stables_data["account_description"] = single_booking.ticket
										cater_fixed_stables_data["outlet"] = single_facility["facility_outlet_code"]
										cater_fixed_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_fixed_stables_data["base_amount"] = round(single_facility["base_fixed_cater"], 2)

										data.append(cater_fixed_stables_data)
									
									if single_facility["facility_booking_extra_adults"] > 0:
										## Income (Facility (Stables) Extra Adult Bookings) ##
										income_adult_stables_data = {}
										income_adult_stables_data["income_account"] = single_facility["facility_income_code"]
										income_adult_stables_data["customer_reference"] = customer_reference
										income_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										income_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_adult_stables_data["vat"] = "V"
										income_adult_stables_data["dc_marker"] = "D"
										income_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_adult_stables_data["journal_type"] = "VI"
										income_adult_stables_data["account_description"] = single_booking.ticket
										income_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_minus_cater"], 2)

										data.append(income_adult_stables_data)

										## Catering Levy (Facility (Stables) Extra Adult Bookings) ##
										cater_adult_stables_data = {}
										cater_adult_stables_data["income_account"] = "511006"
										cater_adult_stables_data["customer_reference"] = customer_reference
										cater_adult_stables_data["currency"] = single_facility["facility_booking_currency"]
										cater_adult_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_adult_stables_data["tran_quantity"] = 1 ## Multiplication by extra adults done
										cater_adult_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_adult_stables_data["vat"] = "V"
										cater_adult_stables_data["dc_marker"] = "D"
										cater_adult_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_adult_stables_data["journal_type"] = "VI"
										cater_adult_stables_data["account_description"] = single_booking.ticket
										cater_adult_stables_data["outlet"] = single_facility["facility_outlet_code"]
										cater_adult_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_adult_stables_data["base_amount"] = round(single_facility["base_extra_adult_cater"], 2)

										data.append(cater_adult_stables_data)

									if single_facility["facility_booking_extra_children"] > 0:
										## Income (Facility (Stables) Extra Children Bookings) ##
										income_child_stables_data = {}
										income_child_stables_data["income_account"] = single_facility["facility_income_code"]
										income_child_stables_data["customer_reference"] = customer_reference
										income_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										income_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										income_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										income_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										income_child_stables_data["vat"] = "V"
										income_child_stables_data["dc_marker"] = "D"
										income_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										income_child_stables_data["journal_type"] = "VI"
										income_child_stables_data["account_description"] = single_booking.ticket
										income_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										income_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										income_child_stables_data["base_amount"] = round(single_facility["base_extra_child_minus_cater"], 2)

										data.append(income_child_stables_data)

										## Catering Levy (Facility (Stables) Extra Children Bookings) ##
										cater_child_stables_data = {}
										cater_child_stables_data["income_account"] = "511006"
										cater_child_stables_data["customer_reference"] = customer_reference
										cater_child_stables_data["currency"] = single_facility["facility_booking_currency"]
										cater_child_stables_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
										cater_child_stables_data["tran_quantity"] = 1 ## Multiplication by extra children done
										cater_child_stables_data["unit_price"] = round(single_facility["cost_per_room"], 2)
										cater_child_stables_data["vat"] = "V"
										cater_child_stables_data["dc_marker"] = "D"
										cater_child_stables_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
										cater_child_stables_data["journal_type"] = "VI"
										cater_child_stables_data["account_description"] = single_booking.ticket
										cater_child_stables_data["outlet"] = single_facility["facility_outlet_code"]
										cater_child_stables_data["department"] = single_facility["facility_dept_analysis_code"]
										cater_child_stables_data["base_amount"] = round(single_facility["base_extra_child_cater"], 2)

										data.append(cater_child_stables_data)

						elif single_facility["accomodation_type"] == "Pelican":
							if single_facility["vat"] & single_facility["catering_levy"]:
								if single_facility["facility_booking_adults"] > 0:
									## Income (Facility (Pelican) Adult Bookings) ##
									income_adult_pelican_data = {}
									income_adult_pelican_data["income_account"] = single_facility["facility_income_code"]
									income_adult_pelican_data["customer_reference"] = customer_reference
									income_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
									income_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
									# income_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									income_adult_pelican_data["vat"] = "V"
									income_adult_pelican_data["dc_marker"] = "D"
									income_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_pelican_data["journal_type"] = "VI"
									income_adult_pelican_data["account_description"] = single_booking.ticket
									income_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
									income_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
									income_adult_pelican_data["base_amount"] = round(single_facility["base_adult_minus_taxes"], 2)

									data.append(income_adult_pelican_data)

									## VAT (Facility (Pelican) Adult Bookings) ##
									vat_adult_pelican_data = {}
									vat_adult_pelican_data["income_account"] = "510001"
									vat_adult_pelican_data["customer_reference"] = customer_reference
									vat_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
									vat_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
									# vat_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									vat_adult_pelican_data["vat"] = "V"
									vat_adult_pelican_data["dc_marker"] = "D"
									vat_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_pelican_data["journal_type"] = "VI"
									vat_adult_pelican_data["account_description"] = single_booking.ticket
									vat_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
									vat_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_adult_pelican_data["base_amount"] = round(single_facility["base_adult_vat"], 2)

									data.append(vat_adult_pelican_data)

									## Catering Levy (Facility (Pelican) Adult Bookings) ##
									cater_adult_pelican_data = {}
									cater_adult_pelican_data["income_account"] = "511006"
									cater_adult_pelican_data["customer_reference"] = customer_reference
									cater_adult_pelican_data["currency"] = single_facility["facility_booking_currency"]
									cater_adult_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_adult_pelican_data["tran_quantity"] = 1 ## Multiplication by number of adults done
									# cater_adult_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									cater_adult_pelican_data["vat"] = "V"
									cater_adult_pelican_data["dc_marker"] = "D"
									cater_adult_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_adult_pelican_data["journal_type"] = "VI"
									cater_adult_pelican_data["account_description"] = single_booking.ticket
									cater_adult_pelican_data["outlet"] = single_facility["facility_outlet_code"]
									cater_adult_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
									cater_adult_pelican_data["base_amount"] = round(single_facility["base_adult_cater"], 2)

									data.append(cater_adult_pelican_data)

								if single_facility["facility_booking_children"] > 0:
									## Income (Facility (Pelican) Children Bookings) ##
									income_child_pelican_data = {}
									income_child_pelican_data["income_account"] = single_facility["facility_income_code"]
									income_child_pelican_data["customer_reference"] = customer_reference
									income_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
									income_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
									# income_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									income_child_pelican_data["vat"] = "V"
									income_child_pelican_data["dc_marker"] = "D"
									income_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_pelican_data["journal_type"] = "VI"
									income_child_pelican_data["account_description"] = single_booking.ticket
									income_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
									income_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
									income_child_pelican_data["base_amount"] = round(single_facility["base_child_minus_taxes"], 2)

									data.append(income_child_pelican_data)

									## VAT (Facility (Pelican) Children Bookings) ##
									vat_child_pelican_data = {}
									vat_child_pelican_data["income_account"] = "510001"
									vat_child_pelican_data["customer_reference"] = customer_reference
									vat_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
									vat_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
									# vat_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									vat_child_pelican_data["vat"] = "V"
									vat_child_pelican_data["dc_marker"] = "D"
									vat_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_child_pelican_data["journal_type"] = "VI"
									vat_child_pelican_data["account_description"] = single_booking.ticket
									vat_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
									vat_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_child_pelican_data["base_amount"] = round(single_facility["base_child_vat"], 2)

									data.append(vat_child_pelican_data)

									## Catering Levy (Facility (Pelican) Children Bookings) ##
									cater_child_pelican_data = {}
									cater_child_pelican_data["income_account"] = "511006"
									cater_child_pelican_data["customer_reference"] = customer_reference
									cater_child_pelican_data["currency"] = single_facility["facility_booking_currency"]
									cater_child_pelican_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									cater_child_pelican_data["tran_quantity"] = 1 ## Multiplication by number of children already done
									# cater_child_pelican_data["unit_price"] = round(single_facility["cost_per_room"], 2)
									cater_child_pelican_data["vat"] = "V"
									cater_child_pelican_data["dc_marker"] = "D"
									cater_child_pelican_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									cater_child_pelican_data["journal_type"] = "VI"
									cater_child_pelican_data["account_description"] = single_booking.ticket
									cater_child_pelican_data["outlet"] = single_facility["facility_outlet_code"]
									cater_child_pelican_data["department"] = single_facility["facility_dept_analysis_code"]
									cater_child_pelican_data["base_amount"] = round(single_facility["base_child_cater"], 2)

									data.append(cater_child_pelican_data)
							
							else:
								## TODO: Handle
								if single_facility["vat"]:
									pass
								else:
									pass

								## TODO: Handle
								if single_facility["catering_levy"]:
									pass

					elif single_facility["facility_type"] == "Camping Sites":
						if single_facility["vat"] & single_facility["catering_levy"]:
							if single_booking.booking_ref_code == "5c9388fe-0":
								pass
							else:
								## Income (Facility (Campsites) Campsite Fee) ##
								income_camp_fee_data = {}
								income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
								income_camp_fee_data["customer_reference"] = customer_reference
								income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
								income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
								income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
								income_camp_fee_data["vat"] = "V"
								income_camp_fee_data["dc_marker"] = "D"
								income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_camp_fee_data["journal_type"] = "VI"
								income_camp_fee_data["account_description"] = single_booking.ticket
								income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
								income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
								income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_minus_taxes"], 2)

								data.append(income_camp_fee_data)

								## VAT (Facility (Campsites) Campsite Fee) ##
								vat_camp_fee_data = {}
								vat_camp_fee_data["income_account"] = "510001"
								vat_camp_fee_data["customer_reference"] = customer_reference
								vat_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
								vat_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								vat_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
								vat_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
								vat_camp_fee_data["vat"] = "V"
								vat_camp_fee_data["dc_marker"] = "D"
								vat_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								vat_camp_fee_data["journal_type"] = "VI"
								vat_camp_fee_data["account_description"] = single_booking.ticket
								vat_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
								vat_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
								vat_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_vat"], 2)

								data.append(vat_camp_fee_data)

								## Catering Levy (Facility (Campsites) Campsite Fee) ##
								cater_camp_fee_data = {}
								cater_camp_fee_data["income_account"] = "511006"
								cater_camp_fee_data["customer_reference"] = customer_reference
								cater_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
								cater_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
								cater_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
								cater_camp_fee_data["vat"] = "V"
								cater_camp_fee_data["dc_marker"] = "D"
								cater_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_camp_fee_data["journal_type"] = "VI"
								cater_camp_fee_data["account_description"] = single_booking.ticket
								cater_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
								cater_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
								cater_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_cater"], 2)

								data.append(cater_camp_fee_data)

							if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
								## Income (Facility (Campsites) Adult Bookings) ##
								income_adult_campsite_data = {}
								income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
								income_adult_campsite_data["customer_reference"] = customer_reference
								income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
								income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
								income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
								income_adult_campsite_data["vat"] = "V"
								income_adult_campsite_data["dc_marker"] = "D"
								income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_adult_campsite_data["journal_type"] = "VI"
								income_adult_campsite_data["account_description"] = single_booking.ticket
								income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
								income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
								income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_minus_taxes"], 2)

								data.append(income_adult_campsite_data)

								## VAT (Facility (Campsites) Adult Bookings) ##
								vat_adult_campsite_data = {}
								vat_adult_campsite_data["income_account"] = "510001"
								vat_adult_campsite_data["customer_reference"] = customer_reference
								vat_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
								vat_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								vat_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
								vat_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
								vat_adult_campsite_data["vat"] = "V"
								vat_adult_campsite_data["dc_marker"] = "D"
								vat_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								vat_adult_campsite_data["journal_type"] = "VI"
								vat_adult_campsite_data["account_description"] = single_booking.ticket
								vat_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
								vat_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
								vat_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_vat"], 2)

								data.append(vat_adult_campsite_data)

								## Catering Levy (Facility (Campsites) Adult Bookings) ##
								cater_adult_campsite_data = {}
								cater_adult_campsite_data["income_account"] = "511006"
								cater_adult_campsite_data["customer_reference"] = customer_reference
								cater_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
								cater_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
								cater_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
								cater_adult_campsite_data["vat"] = "V"
								cater_adult_campsite_data["dc_marker"] = "D"
								cater_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_adult_campsite_data["journal_type"] = "VI"
								cater_adult_campsite_data["account_description"] = single_booking.ticket
								cater_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
								cater_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
								cater_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_cater"], 2)

								data.append(cater_adult_campsite_data)

							if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
								## Income (Facility (Campsites) Children Bookings) ##
								income_child_campsite_data = {}
								income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
								income_child_campsite_data["customer_reference"] = customer_reference
								income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
								income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
								income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
								income_child_campsite_data["vat"] = "V"
								income_child_campsite_data["dc_marker"] = "D"
								income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								income_child_campsite_data["journal_type"] = "VI"
								income_child_campsite_data["account_description"] = single_booking.ticket
								income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
								income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
								income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_minus_taxes"], 2)

								data.append(income_child_campsite_data)

								## VAT (Facility (Campsites) Children Bookings) ##
								vat_child_campsite_data = {}
								vat_child_campsite_data["income_account"] = "510001"
								vat_child_campsite_data["customer_reference"] = customer_reference
								vat_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
								vat_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								vat_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
								vat_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
								vat_child_campsite_data["vat"] = "V"
								vat_child_campsite_data["dc_marker"] = "D"
								vat_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								vat_child_campsite_data["journal_type"] = "VI"
								vat_child_campsite_data["account_description"] = single_booking.ticket
								vat_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
								vat_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
								vat_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_vat"], 2)

								data.append(vat_child_campsite_data)

								## Catering Levy (Facility (Campsites) Children Bookings) ##
								cater_child_campsite_data = {}
								cater_child_campsite_data["income_account"] = "511006"
								cater_child_campsite_data["customer_reference"] = customer_reference
								cater_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
								cater_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
								cater_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
								cater_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
								cater_child_campsite_data["vat"] = "V"
								cater_child_campsite_data["dc_marker"] = "D"
								cater_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
								cater_child_campsite_data["journal_type"] = "VI"
								cater_child_campsite_data["account_description"] = single_booking.ticket
								cater_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
								cater_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
								cater_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_cater"], 2)

								data.append(cater_child_campsite_data)
						
						else:
							if single_facility["vat"]:
								if single_booking.booking_ref_code == "5c9388fe-0":
									pass
								else:
									## Income (Facility (Campsites) Campsite Fee) ##
									income_camp_fee_data = {}
									income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
									income_camp_fee_data["customer_reference"] = customer_reference
									income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									income_camp_fee_data["vat"] = "V"
									income_camp_fee_data["dc_marker"] = "D"
									income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_camp_fee_data["journal_type"] = "VI"
									income_camp_fee_data["account_description"] = single_booking.ticket
									income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_minus_vat"], 2)

									data.append(income_camp_fee_data)

									## VAT (Facility (Campsites) Campsite Fee) ##
									vat_camp_fee_data = {}
									vat_camp_fee_data["income_account"] = "510001"
									vat_camp_fee_data["customer_reference"] = customer_reference
									vat_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									vat_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									vat_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									vat_camp_fee_data["vat"] = "V"
									vat_camp_fee_data["dc_marker"] = "D"
									vat_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_camp_fee_data["journal_type"] = "VI"
									vat_camp_fee_data["account_description"] = single_booking.ticket
									vat_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									vat_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost_vat"], 2)

									data.append(vat_camp_fee_data)

								if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
									## Income (Facility (Campsites) Adult Bookings) ##
									income_adult_campsite_data = {}
									income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
									income_adult_campsite_data["customer_reference"] = customer_reference
									income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
									income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
									income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
									income_adult_campsite_data["vat"] = "V"
									income_adult_campsite_data["dc_marker"] = "D"
									income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_campsite_data["journal_type"] = "VI"
									income_adult_campsite_data["account_description"] = single_booking.ticket
									income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
									income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
									income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_minus_vat"], 2)

									data.append(income_adult_campsite_data)

									## VAT (Facility (Campsites) Adult Bookings) ##
									vat_adult_campsite_data = {}
									vat_adult_campsite_data["income_account"] = "510001"
									vat_adult_campsite_data["customer_reference"] = customer_reference
									vat_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
									vat_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
									vat_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
									vat_adult_campsite_data["vat"] = "V"
									vat_adult_campsite_data["dc_marker"] = "D"
									vat_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_adult_campsite_data["journal_type"] = "VI"
									vat_adult_campsite_data["account_description"] = single_booking.ticket
									vat_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
									vat_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost_vat"], 2)

									data.append(vat_adult_campsite_data)

								if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
									## Income (Facility (Campsites) Children Bookings) ##
									income_child_campsite_data = {}
									income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
									income_child_campsite_data["customer_reference"] = customer_reference
									income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
									income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
									income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
									income_child_campsite_data["vat"] = "V"
									income_child_campsite_data["dc_marker"] = "D"
									income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_campsite_data["journal_type"] = "VI"
									income_child_campsite_data["account_description"] = single_booking.ticket
									income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
									income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
									income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_minus_vat"], 2)

									data.append(income_child_campsite_data)

									## VAT (Facility (Campsites) Children Bookings) ##
									vat_child_campsite_data = {}
									vat_child_campsite_data["income_account"] = "510001"
									vat_child_campsite_data["customer_reference"] = customer_reference
									vat_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
									vat_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									vat_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
									vat_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
									vat_child_campsite_data["vat"] = "V"
									vat_child_campsite_data["dc_marker"] = "D"
									vat_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									vat_child_campsite_data["journal_type"] = "VI"
									vat_child_campsite_data["account_description"] = single_booking.ticket
									vat_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
									vat_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
									vat_child_campsite_data["base_amount"] = round(single_facility["base_child_cost_vat"], 2)

									data.append(vat_child_campsite_data)
							
							else:
								if single_booking.booking_ref_code == "5c9388fe-0":
									pass
								else:
									## Income (Facility (Campsites) Campsite Fee) ##
									income_camp_fee_data = {}
									income_camp_fee_data["income_account"] = single_facility["facility_income_code"]
									income_camp_fee_data["customer_reference"] = customer_reference
									income_camp_fee_data["currency"] = single_facility["facility_booking_currency"]
									income_camp_fee_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_camp_fee_data["tran_quantity"] = 1 ## Multiplication by number of weeks already done
									income_camp_fee_data["unit_price"] = round(single_facility["facility_booking_fixed_cost"], 2)
									income_camp_fee_data["vat"] = "V"
									income_camp_fee_data["dc_marker"] = "D"
									income_camp_fee_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_camp_fee_data["journal_type"] = "VI"
									income_camp_fee_data["account_description"] = single_booking.ticket
									income_camp_fee_data["outlet"] = single_facility["facility_outlet_code"]
									income_camp_fee_data["department"] = single_facility["facility_dept_analysis_code"]
									income_camp_fee_data["base_amount"] = round(single_facility["base_fixed_cost"], 2)

									data.append(income_camp_fee_data)

								if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
									## Income (Facility (Campsites) Adult Bookings) ##
									income_adult_campsite_data = {}
									income_adult_campsite_data["income_account"] = single_facility["facility_income_code"]
									income_adult_campsite_data["customer_reference"] = customer_reference
									income_adult_campsite_data["currency"] = single_facility["facility_booking_currency"]
									income_adult_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_adult_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
									income_adult_campsite_data["unit_price"] = round(single_facility["facility_booking_adult_cost"], 2)
									income_adult_campsite_data["vat"] = "V"
									income_adult_campsite_data["dc_marker"] = "D"
									income_adult_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_adult_campsite_data["journal_type"] = "VI"
									income_adult_campsite_data["account_description"] = single_booking.ticket
									income_adult_campsite_data["outlet"] = single_facility["facility_outlet_code"]
									income_adult_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
									income_adult_campsite_data["base_amount"] = round(single_facility["base_adult_cost"], 2)

									data.append(income_adult_campsite_data)

								if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
									## Income (Facility (Campsites) Children Bookings) ##
									income_child_campsite_data = {}
									income_child_campsite_data["income_account"] = single_facility["facility_income_code"]
									income_child_campsite_data["customer_reference"] = customer_reference
									income_child_campsite_data["currency"] = single_facility["facility_booking_currency"]
									income_child_campsite_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
									income_child_campsite_data["tran_quantity"] = 1 ## Multiplication by number of adults already done
									income_child_campsite_data["unit_price"] = round(single_facility["facility_booking_child_cost"], 2)
									income_child_campsite_data["vat"] = "V"
									income_child_campsite_data["dc_marker"] = "D"
									income_child_campsite_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])
									income_child_campsite_data["journal_type"] = "VI"
									income_child_campsite_data["account_description"] = single_booking.ticket
									income_child_campsite_data["outlet"] = single_facility["facility_outlet_code"]
									income_child_campsite_data["department"] = single_facility["facility_dept_analysis_code"]
									income_child_campsite_data["base_amount"] = round(single_facility["base_child_cost"], 2)

									data.append(income_child_campsite_data)

							## TODO: Handle
							if single_facility["catering_levy"]:
								pass
			
			if single_booking.booking_ref_code == "491a14d1-4":
				booking_data = {}
				# try:
				# 	booking_data["income_account"] = return_data["code_kes"]
				# except Exception:
				# 	booking_data["income_account"] = "406GA001"
				
				booking_data["income_account"] = return_data["customer_code"]
				booking_data["customer_reference"] = customer_reference
				
				# booking_data["currency_code"] = single_booking.currency
				booking_data["currency"] = booking_currency.json()["data"][0]["currency_name"]
				
				booking_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
				booking_data["tran_quantity"] = 2
				booking_data["unit_price"] = round(90, 2)
				booking_data["dc_marker"] = "C"
				booking_data["booking_type"] = return_data["booking_type"]
				booking_data["partner_booking"] = return_data["partner_booking"]
				booking_data["guest_booking"] = return_data["guest_booking"]
				booking_data["gate_booking"] = return_data["gate_booking"]
				booking_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])

				booking_data["journal_type"] = "VI"
				booking_data["account_description"] = single_booking.ticket
				booking_data["outlet"] = "#"
				booking_data["department"] = "#"
				booking_data["base_amount"] = round(90, 2)

				data.append(booking_data)
				existing_bookings.append(single_booking.booking_public_id)

			elif single_booking.booking_ref_code == "5787740c-8":
				booking_data = {}
				# try:
				# 	booking_data["income_account"] = return_data["code_kes"]
				# except Exception:
				# 	booking_data["income_account"] = "406GA001"
				
				booking_data["income_account"] = return_data["customer_code"]
				booking_data["customer_reference"] = customer_reference
				
				# booking_data["currency_code"] = single_booking.currency
				booking_data["currency"] = booking_currency.json()["data"][0]["currency_name"]
				
				booking_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
				booking_data["tran_quantity"] = 1
				booking_data["unit_price"] = round(return_data["total_cost"], 2)
				booking_data["dc_marker"] = "C"
				booking_data["booking_type"] = return_data["booking_type"]
				booking_data["partner_booking"] = return_data["partner_booking"]
				booking_data["guest_booking"] = return_data["guest_booking"]
				booking_data["gate_booking"] = return_data["gate_booking"]
				booking_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])

				booking_data["journal_type"] = "VI"
				booking_data["account_description"] = single_booking.ticket
				booking_data["outlet"] = "#"
				booking_data["department"] = "#"
				booking_data["base_amount"] = round(return_data["total_cost"], 2)

				data.append(booking_data)

				booking_data = {}
				# try:
				# 	booking_data["income_account"] = return_data["code_kes"]
				# except Exception:
				# 	booking_data["income_account"] = "406GA001"
				
				booking_data["income_account"] = "406GA002"
				booking_data["customer_reference"] = customer_reference
				
				# booking_data["currency_code"] = single_booking.currency
				booking_data["currency"] = "USD"
				
				booking_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
				booking_data["tran_quantity"] = 1
				booking_data["unit_price"] = round(return_data["total_cost"], 2)
				booking_data["dc_marker"] = "D"
				booking_data["booking_type"] = return_data["booking_type"]
				booking_data["partner_booking"] = return_data["partner_booking"]
				booking_data["guest_booking"] = return_data["guest_booking"]
				booking_data["gate_booking"] = return_data["gate_booking"]
				booking_data["conversion_rate"] = 100.262

				booking_data["journal_type"] = "VI"
				booking_data["account_description"] = single_booking.ticket
				booking_data["outlet"] = "#"
				booking_data["department"] = "#"
				booking_data["base_amount"] = round(return_data["total_cost"], 2)

				data.append(booking_data)

				existing_bookings.append(single_booking.booking_public_id)
			
			else:
				booking_data = {}
				# try:
				# 	booking_data["income_account"] = return_data["code_kes"]
				# except Exception:
				# 	booking_data["income_account"] = "406GA001"
				
				booking_data["income_account"] = return_data["customer_code"]
				booking_data["customer_reference"] = customer_reference
				
				# booking_data["currency_code"] = single_booking.currency
				booking_data["currency"] = booking_currency.json()["data"][0]["currency_name"]
				
				booking_data["sales_date"] = str(single_booking.booking_check_in_date.strftime("%d%m%Y"))
				booking_data["tran_quantity"] = 1
				booking_data["unit_price"] = round(return_data["total_cost"], 2)
				booking_data["dc_marker"] = "C"
				booking_data["booking_type"] = return_data["booking_type"]
				booking_data["partner_booking"] = return_data["partner_booking"]
				booking_data["guest_booking"] = return_data["guest_booking"]
				booking_data["gate_booking"] = return_data["gate_booking"]
				booking_data["conversion_rate"] = float(return_data["currency_selling_rate_at_time"])

				booking_data["journal_type"] = "VI"
				booking_data["account_description"] = single_booking.ticket
				booking_data["outlet"] = "#"
				booking_data["department"] = "#"
				booking_data["base_amount"] = round(return_data["total_cost"], 2)

				data.append(booking_data)
				existing_bookings.append(single_booking.booking_public_id)

		
	return jsonify({"data": data}), 200


@app.route("/payments/credit/batchfile")
def issue_payments_credit_note():
	bookings_array = [
		"5ef2a6cd-9455-4968-affe-33454736f03f"
	]

	## We'll see if this is needed
	# "41371937-724e-493e-bc52-e1aa2f339da8",
	
	select_transactions = db.session.query(Transaction)\
									.join(Booking, Transaction.booking_id == Booking.booking_public_id)\
									.add_columns(Transaction.booking_id, Transaction.transaction_total, Transaction.transaction_total_currency,\
												 Transaction.transaction_payment_currency, Transaction.transaction_payment_method, Transaction.transaction_booking_public_id,\
												 Transaction.payment_currency_buying_rate_at_time, Transaction.payment_currency_selling_rate_at_time,\
												 Transaction.transaction_payment_gateway, Transaction.transaction_date,\
												 Booking.booking_public_id, Booking.booking_ref_code, Booking.ticket,\
												 Booking.currency, Booking.booking_done_by, Booking.created_at,\
												 Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time)\
									.filter(Transaction.deletion_marker == None)\
									.filter(Transaction.booking_id.in_(bookings_array))\
									.filter(Booking.deletion_marker == None)\
									.filter(Booking.status != get_booking_status_id("Cancelled"))\
									.all()

	existing_transactions = []

	transaction_data = []
	for single_transaction in select_transactions:
		if single_transaction.transaction_booking_public_id in existing_transactions:
			pass
		else:
			if single_transaction.transaction_payment_method == "746bb582-89ed-4c83-ac3c-f25d9d60b266":
				pass
			else:
				general_booking_data = {}
				
				booking_currency = requests.get(get_currency.format(single_transaction.currency))
				transaction_currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
				bookingTotal(general_booking_data, single_transaction.booking_id, True)

				check_school_booking = db.session.query(SchoolBooking)\
													.join(School, SchoolBooking.school_id == School.school_public_id)\
													.add_columns(School.school_name)\
													.filter(SchoolBooking.deletion_marker == None)\
													.filter(SchoolBooking.booking_id == single_transaction.booking_public_id)\
													.first()
				
				if check_school_booking:
					try:
						booking_done_by = check_school_booking.school_name
					except Exception:
						booking_done_by = single_transaction.booking_done_by
				else:
					booking_done_by = single_transaction.booking_done_by
				
				customer_ref = single_transaction.booking_ref_code + " " + booking_done_by

				if len(customer_ref) >= 30:
					customer_reference = customer_ref[:29]
				else:
					customer_reference = customer_ref

				get_booking_payment = db.session.query(BookingPayment)\
												.filter(BookingPayment.deletion_marker == None)\
												.filter(BookingPayment.transaction_id == single_transaction.transaction_booking_public_id)\
												.first()

				if get_booking_payment.card_first_four:
					bank_reference = get_booking_payment.card_first_four + "..." + get_booking_payment.card_last_four
				else:
					bank_reference = get_booking_payment.mpesa_reference

				## Credit as KES
				## Customer data
				customer_data = {}
				customer_data["booking_public_id"] = single_transaction.booking_id
				customer_data["sales_date"] = str(single_transaction.transaction_date.strftime("%d%m%Y"))
				customer_data["customer_reference"] = customer_reference
				customer_data["dc_marker"] = "C"
				customer_data["account_description"] = single_transaction.ticket
				customer_data["currency_code"] = single_transaction.currency
				customer_data["currency"] = "KES"
				customer_data["tran_quantity"] = 1
				customer_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
				customer_data["journal_type"] = "VI"
				customer_data["outlet"] = "#"
				customer_data["department"] = "#"
				customer_data["conversion_rate"] = float(1)
				customer_data["income_account"] = general_booking_data["customer_code"]
				
				transaction_data.append(customer_data)

				## Debit the previous amount, which had been received as USD
				## Debit data
				debit_data = {}
				debit_data["booking_public_id"] = single_transaction.booking_id
				debit_data["sales_date"] = str(single_transaction.transaction_date.strftime("%d%m%Y"))
				debit_data["customer_reference"] = customer_reference
				debit_data["dc_marker"] = "D"
				debit_data["account_description"] = single_transaction.ticket
				debit_data["currency_code"] = single_transaction.currency
				debit_data["currency"] = transaction_currency.json()["data"][0]["currency_name"]
				debit_data["tran_quantity"] = 1
				debit_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
				debit_data["journal_type"] = "VI"
				debit_data["outlet"] = "#"
				debit_data["department"] = "#"
				debit_data["conversion_rate"] = float(single_transaction.currency_selling_rate_at_time)
				debit_data["income_account"] = general_booking_data["customer_code"]
				
				transaction_data.append(debit_data)

				
				transaction_currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
				bookingTotal(general_booking_data, single_transaction.booking_id, True)

				## Debit as KES
				## Bank data
				bank_data = {}
				bank_data["booking_public_id"] = single_transaction.booking_id
				bank_data["sales_date"] = str(single_transaction.transaction_date.strftime("%d%m%Y"))
				bank_data["customer_reference"] = customer_reference
				bank_data["dc_marker"] = "D"
				bank_data["account_description"] = bank_reference
				bank_data["currency_code"] = single_transaction.currency
				bank_data["currency"] = "KES"
				bank_data["tran_quantity"] = 1
				bank_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
				bank_data["journal_type"] = "VI"
				bank_data["outlet"] = "#"
				bank_data["department"] = "#"
				bank_data["conversion_rate"] = float(1)
				
				## TODO: Save payment gateways
				# Paybill
				if single_transaction.transaction_payment_method == "5550ff2f-bc48-4f0e-8944-d08f7f2b93fd":
					bank_data["income_account"] = "407003"
				# Mpesa
				elif single_transaction.transaction_payment_method == "cbff45f3-4f12-41da-8b4a-bf308112f032":
					bank_data["income_account"] = "407003"
				# Paypal
				elif single_transaction.transaction_payment_method == "653e10f6-168c-493e-bcbb-0219537c24f6":
					bank_data["income_account"] = "407009"
				# iVeri
				elif single_transaction.transaction_payment_method == "33ad20be-3a89-4deb-ab71-7510ba51677e":
					if transaction_currency.json()["data"][0]["currency_name"] == "KES":
						bank_data["income_account"] = "407016"
					elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
						bank_data["income_account"] = "407017"
				# Mpesa Online
				elif single_transaction.transaction_payment_method == "1c9d01cc-ef31-4757-a390-10c765fcecab":
					bank_data["income_account"] = "407003"
				else:
					if single_transaction.transaction_payment_gateway:
						get_gateway = db.session.query(PaymentGateway)\
												.filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
												.first()

						bank_data["income_account"] = get_gateway.payment_gateway_code
					else:
						if transaction_currency.json()["data"][0]["currency_name"] == "KES":
							bank_data["income_account"] = "407004"
						elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
							bank_data["income_account"] = "407005"

				transaction_data.append(bank_data)

				## Credit the previous amount, which had been received as USD
				credit_data = {}
				credit_data["booking_public_id"] = single_transaction.booking_id
				credit_data["sales_date"] = str(single_transaction.transaction_date.strftime("%d%m%Y"))
				credit_data["customer_reference"] = customer_reference
				credit_data["dc_marker"] = "C"
				credit_data["account_description"] = bank_reference
				credit_data["currency_code"] = single_transaction.currency
				credit_data["currency"] = transaction_currency.json()["data"][0]["currency_name"]
				credit_data["tran_quantity"] = 1
				credit_data["unit_price"] = round(float(single_transaction.transaction_total), 2)
				credit_data["journal_type"] = "VI"
				credit_data["outlet"] = "#"
				credit_data["department"] = "#"
				credit_data["conversion_rate"] = float(single_transaction.payment_currency_selling_rate_at_time)
				
				## TODO: Save payment gateways
				# Paybill
				if single_transaction.transaction_payment_method == "5550ff2f-bc48-4f0e-8944-d08f7f2b93fd":
					credit_data["income_account"] = "407003"
				# Mpesa
				elif single_transaction.transaction_payment_method == "cbff45f3-4f12-41da-8b4a-bf308112f032":
					credit_data["income_account"] = "407003"
				# Paypal
				elif single_transaction.transaction_payment_method == "653e10f6-168c-493e-bcbb-0219537c24f6":
					credit_data["income_account"] = "407009"
				# iVeri
				elif single_transaction.transaction_payment_method == "33ad20be-3a89-4deb-ab71-7510ba51677e":
					if transaction_currency.json()["data"][0]["currency_name"] == "KES":
						credit_data["income_account"] = "407016"
					elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
						credit_data["income_account"] = "407017"
				# Mpesa Online
				elif single_transaction.transaction_payment_method == "1c9d01cc-ef31-4757-a390-10c765fcecab":
					credit_data["income_account"] = "407003"
				else:
					if single_transaction.transaction_payment_gateway:
						get_gateway = db.session.query(PaymentGateway)\
												.filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
												.first()

						credit_data["income_account"] = get_gateway.payment_gateway_code
					else:
						if transaction_currency.json()["data"][0]["currency_name"] == "KES":
							credit_data["income_account"] = "407004"
						elif transaction_currency.json()["data"][0]["currency_name"] == "USD":
							credit_data["income_account"] = "407005"

				transaction_data.append(credit_data)

				existing_transactions.append(single_transaction.transaction_booking_public_id)

	return jsonify({"data": transaction_data})