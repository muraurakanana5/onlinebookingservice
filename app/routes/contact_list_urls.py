from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


from routes import app, bookings_logger
from database.contact_list import ContactList
from routes import db


@app.route("/contact/new", methods = ["POST"])
def addNewContactEmail():
	try:
		request.json["email_address"].strip()
		if not request.json["email_address"]:
			messages.append("Email address is empty.")
	except KeyError as e:
		messages.append("Email address is missing.")

	get_contact = db.session.query(ContactList)\
							.filter(ContactList.deletion_marker == None)\
							.filter(ContactList.contact_email == request.json["email_address"])\
							.first()

	if get_contact:
		message = []
		message.append("The email address provided already exists in the system.")
		return jsonify({"message": message}), 412

	else:
		contact = ContactList(
			contact_public_id = str(uuid.uuid4()),
			contact_email = request.json["email_address"],
			created_at = datetime.now(),
			updated_at = datetime.now()
		)

		db.session.add(contact)

		try:
			db.session.commit()
			db.session.close()

			message = []
			message.append("The contact email address has been added.")
			return jsonify({"message": message}), 200

		except Exception:
			db.session.rollback()
			db.session.close()

			message = []
			message.append("There was an error adding the contact email address.")
			return jsonify({"message": message}), 422


@app.route("/contact/view", methods = ["GET"])
def viewContactEmails():
	get_contacts = db.session.query(ContactList)\
							 .filter(ContactList.deletion_marker == None)\
							 .all()

	if not get_contacts:
		message = []
		message.append("There are no contact email addresses yet.")
		return jsonify({"message": message}), 412

	else:
		data = []

		for single in get_contacts:
			data.append(single.return_json())

		return jsonify({"data": data}), 200


@app.route("/contact/delete", methods = ["PATCH"])
def deleteContactEmail():
	try:
		request.json["contact_id"].strip()
		if not request.json["contact_id"]:
			messages.append("Contact ID is empty.")
	except KeyError as e:
		messages.append("Contact ID is missing.")

	get_contact = db.session.query(ContactList)\
							.filter(ContactList.deletion_marker == None)\
							.filter(ContactList.contact_public_id == request.json["contact_id"])\
							.first()

	if not get_contact:
		message = []
		message.append("The selected contact email is not in the system.")
		return jsonify({"message": message}), 412

	else:
		get_contact.deletion_marker = 1
		get_contact.updated_at = datetime.now()

		try:
			db.session.commit()
			db.session.close()

			message = []
			message.append("The contact email address has been deleted.")
			return jsonify({"message": message}), 200

		except Exception:
			db.session.rollback()
			db.session.close()

			message = []
			message.append("There was an error deleting the contact email address.")
			return jsonify({"message": message}), 422