from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed
from routes.bookings_urls import get_booking_status_id
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.credit_note import CreditNote
from database.booking_details import Detail
from database.facility import Facility
from database.booking_guest_types import GuestType
from database.mandatory_payments import Mandatory
from database.invoice import Invoice
from database.inventory import Inventory
from database.gatepass_guests import GatepassGuest
from database.gatepass import Gatepass
from database.partner import Partner
from database.check_in_vehicles import CheckInVehicle
from database.school import School
from database.school_booking import SchoolBooking
from database.group import Group
from database.member import Member
from functions.booking_snippets import get_details_member, get_details_partner, bookingTotal
from functions.validation import fieldValidation
from variables import get_user_from_aumra


def close(self):
	self.session.close()


##########################
#### Booking Searches ####
##########################
@app.route("/bookings/search/by_key", methods = ["POST"])
def global_booking_search():
	validation_list = [
	 {"field": "key"}
	]
	
	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	bookings_array = []
	search_term = request.json["key"].strip()
	return_bookings_public_id = db.session.query(Booking)\
		 .filter(Booking.booking_public_id == search_term\
		 or Booking.booking_public_id.contains(search_term))\
		 .all()

	for single_public_id in return_bookings_public_id:
		bookings_array.append(single_public_id.booking_public_id)

	return_bookings_ref_code = db.session.query(Booking)\
		.filter(Booking.booking_ref_code == search_term.lower()\
	   or Booking.booking_ref_code.contains(search_term.lower()))\
		.all()

	for single_ref_code in return_bookings_ref_code:
		bookings_array.append(single_ref_code.booking_public_id)

	return_bookings_done_by = db.session.query(Booking)\
	   .filter(Booking.booking_done_by == search_term\
		or Booking.booking_done_by.contains(search_term))\
	   .all()

	for single_done_by in return_bookings_done_by:
		bookings_array.append(single_done_by.booking_public_id)

	return_bookings_tickets = db.session.query(Booking)\
	   .filter(Booking.ticket == search_term\
		or Booking.ticket.contains(search_term))\
	   .all()

	for single_ticket in return_bookings_tickets:
		bookings_array.append(single_ticket.booking_public_id)
	
	
	return_bookings_email = db.session.query(Detail)\
		.filter(Detail.email_address == search_term\
	   or Detail.email_address.contains(search_term))\
		.all()
	 
	for single_email in return_bookings_email:
		bookings_array.append(single_email.booking_id)

	return_bookings_first_name = db.session.query(Detail)\
	   .filter(Detail.first_name.ilike(search_term))\
	   .all()

	for single_first_name in return_bookings_first_name:
		bookings_array.append(single_first_name.booking_id)

	return_bookings_last_name = db.session.query(Detail)\
		 .filter(Detail.last_name.ilike(search_term))\
		 .all()

	for single_last_name in return_bookings_last_name:
		bookings_array.append(single_last_name.booking_id)

	return_bookings_numbers = db.session.query(Detail)\
	   .filter(Detail.phone_number == search_term\
		or Detail.phone_number.contains(search_term))\
	   .all()

	for single_number in return_bookings_numbers:
		bookings_array.append(single_number.booking_id)

	return_bookings_reg = db.session.query(CheckInVehicle)\
	  .filter(CheckInVehicle.check_vehicle_reg == search_term\
		or CheckInVehicle.check_vehicle_reg.contains(search_term))\
	  .all()

	for single_reg in return_bookings_reg:
		bookings_array.append(single_reg.booking_id)
	
	
	return_bookings_school = db.session.query(SchoolBooking)\
		 .join(School, SchoolBooking.school_id == School.school_public_id)\
		 .add_columns(SchoolBooking.booking_id)\
		 .filter(School.school_name == search_term.title()\
		or School.school_name.contains(search_term.title()))\
		 .all()

	for single_school in return_bookings_school:
		bookings_array.append(single_school.booking_id)

	return_bookings_organisations = db.session.query(Group)\
	   .filter(Group.organisation_name == search_term\
		 or Group.organisation_name.contains(search_term))\
	   .all()

	for single_organisation in return_bookings_organisations:
		bookings_array.append(single_organisation.booking_id)

	return_bookings_members = db.session.query(Member)\
	   .filter(Member.member_id == search_term\
		 or Member.member_id.contains(search_term))\
	   .all()

	for single_member in return_bookings_members:
		bookings_array.append(single_member.booking_id)

	if not set(bookings_array):
		message = []
		message.append("The search for "+ search_term +" did not return any results.")
		return jsonify({"message": message}), 412

	return_bookings = db.session.query(Booking)\
		.filter(Booking.booking_public_id.in_(set(bookings_array)))\
		.order_by(Booking.booking_check_in_date.asc())\
		.all()

	id_array = []

	for each_id in return_bookings:
		id_array.append(each_id.session_id)

	request_session = requests.Session()

	try:
		return_user = request_session.post(get_user_from_aumra,\
			 json = {"users_ids": id_array})
	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		pass

	data = []
	for single in return_bookings:
		return_data = {}
		return_data["booking_public_id"] = single.booking_public_id
		return_data["booking_type"] = single.b_type.booking_type_name
		return_data["booking_type_id"] = single.booking_type
		return_data["booking_check_in_date"] = single.booking_check_in_date
		return_data["booking_check_out_date"] = single.booking_check_out_date
		return_data["booking_done_by"] = single.booking_done_by
		return_data["booking_ref_code"] = single.booking_ref_code

		return_data["facility_bookings"] = single.return_facilities(request_session)
		return_data["inventory_bookings"] = single.return_inventory(request_session)

		check_school_booking = db.session.query(SchoolBooking)\
		   .join(School, SchoolBooking.school_id == School.school_public_id)\
		   .add_columns(School.school_name)\
		   .filter(SchoolBooking.booking_id == single.booking_public_id)\
		   .first()

		check_org_booking = db.session.query(Group)\
		   .filter(Group.booking_id == single.booking_public_id)\
		   .first()

		try:
			if check_school_booking:
				return_data["booking_done_by"] = check_school_booking.school_name
			elif check_org_booking:
				return_data["booking_done_by"] = check_org_booking.organisation_name
			else:
				return_data["booking_done_by"] = single.booking_done_by

		except Exception:
			return_data["booking_done_by"] = single.booking_done_by

		booking_info = {}
		bookingTotal(booking_info, single.booking_public_id)

		return_data["booking_currency"] = booking_info["booking_currency_name"]
		return_data["booking_total"] = booking_info["total_cost"]

		return_data["guests"] = booking_info["guests"]
		return_data["guest_total"] = booking_info["guest_total"]

		return_data["vehicles"] = booking_info["vehicles"]
		return_data["vehicle_total"] = booking_info["vehicle_total"]

		return_data["accommodations"] = booking_info["facility_bookings"]
		return_data["activities"] = booking_info["inventory_bookings"]

		return_data["vat"] = booking_info["tax_breakdown"]["vat"]
		return_data["catering_levy"] = booking_info["tax_breakdown"]["catering"]
		return_data["after_tax"] = booking_info["tax_breakdown"]["after_tax"]


		try:
			get_booking_details = db.session.query(Detail)\
			  .filter(Detail.booking_id == single.booking_public_id)\
			  .filter(Detail.status != get_booking_status_id("Updated"))\
			  .first()

			return_data["email_address"] = get_booking_details.email_address
			return_data["phone_number"] = get_booking_details.phone_number
		except AttributeError:
			return_data["email_address"] = ""
			return_data["phone_number"] = ""

		return_data["booking_ticket"] = single.ticket

		check_to_invoice = db.session.query(Invoice)\
			.filter(Invoice.deletion_marker == None)\
			.filter(Invoice.booking_id == single.booking_public_id)\
			.first()

		if check_to_invoice:
			if single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if single.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif single.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if single.b_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif single.b_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif single.b_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif single.b_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif single.b_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif single.b_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif single.b_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				elif single.b_status.booking_status_name == "Postponed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["POSTPONED"])
					
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = single.b_status.booking_status_name
		return_data["booking_status_id"] = single.status

		if single.session_id:
			if single.session_id == "guest":
				return_data["session_user"] = "Public"
				return_data["session_id"] = single.session_id
			else:
				try:
					for user in return_user.json()["data"]:
						if user["public_id"] == single.session_id:
							return_data["session_user"] = user["full_name"]
							return_data["session_id"] = single.session_id

				except (IndexError, KeyError) as user_error:
					return_data["session_user"] = "N/A"
					return_data["session_id"] = single.session_id

				except (AttributeError, UnboundLocalError) as network_related_errors:
					return_data["session_user"] = "Network Error"
					return_data["session_id"] = single.session_id
		else:
			return_data["session_user"] = "N/A"
			return_data["session_id"] = single.session_id

		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		get_partner_booking = db.session.query(Partner)\
		  .filter(Partner.deletion_marker == None)\
		  .filter(Partner.booking_id == single.booking_public_id)\
		  .first()

		if get_partner_booking:
			return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref

			partner_info = get_details_partner(get_partner_booking.partner_id)
			try:
				return_data["partner_type"] = partner_info["partner_type"][0]
				return_data["partner_name"] = partner_info["name"]
			except Exception:
				return_data["partner_type"] = None
				return_data["partner_name"] = None

		else:
			return_data["partner_booking_ref"] = None
			return_data["partner_type"] = None
			return_data["partner_name"] = None

		if single.member_details:
			booking_details = single.member_details.first()

			if booking_details:
				member_info = get_details_member(booking_details.member_id)
				return_data["member_name"] = member_info["full_name"]
				return_data["membership_level"] = member_info["level"]
			else:
				return_data["member_name"] = None
				return_data["membership_level"] = None

		else:
			return_data["member_name"] = None
			return_data["membership_level"] = None

		get_credit_notes = db.session.query(CreditNote)\
		  .filter(CreditNote.deletion_marker == None)\
		  .filter(CreditNote.booking_id == single.booking_public_id)\
		  .all()

		if get_credit_notes:
			return_data["credit_note_issued"] = True
		else:
			return_data["credit_note_issued"] = False

		data.append(return_data)

	return jsonify({"data": data}), 200



## This is what is used by the UI
@app.route("/bookings/search/by_date/range", methods = ["POST"])
@app.route("/bookings/search/by_date/range/check_in", methods = ["POST"])
def search_bookings_by_date_check_in_range():
	messages = []
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	if request.json["end_date"] < request.json["start_date"]:
		output = []
		output.append("The start date cannot come after the end date.")
		return jsonify({"message": output}), 422

	# 0 -> created_at
	# 1 -> check_in
	# 2 -> check_out
	# 3 -> activity
	# 4 -> accommodation_arrival
	# 5 -> accommodation_departure
	# 6 -> in_house (or between check_in and check_out)
	
	bookings_array = []
	
	if request.json["date_search"] == "0":
		return_bookings_book_check_in_range = db.session.query(Booking)\
														.filter(Booking.created_at >= request.json["start_date"])\
														.filter(Booking.created_at <= request.json["end_date"])\
														.all()

		for single_check_in in return_bookings_book_check_in_range:
			bookings_array.append(single_check_in.booking_public_id)
	elif request.json["date_search"] == "1":
		return_bookings_book_check_in_range = db.session.query(Booking)\
														.filter(Booking.booking_check_in_date >= request.json["start_date"])\
														.filter(Booking.booking_check_in_date <= request.json["end_date"])\
														.all()

		for single_check_in in return_bookings_book_check_in_range:
			bookings_array.append(single_check_in.booking_public_id)
	elif request.json["date_search"] == "2":
		return_bookings_book_check_out_range = db.session.query(Booking)\
														.filter(Booking.booking_check_out_date >= request.json["start_date"])\
														.filter(Booking.booking_check_out_date <= request.json["end_date"])\
														.all()

		for single_check_out in return_bookings_book_check_out_range:
			bookings_array.append(single_check_out.booking_public_id)
	elif request.json["date_search"] == "3":
		return_bookings_inventory = db.session.query(Inventory)\
											.filter(Inventory.inventory_booking_date >= request.json["start_date"])\
											.filter(Inventory.inventory_booking_date <= request.json["end_date"])\
											.all()

		for single_inventory in return_bookings_inventory:
			bookings_array.append(single_inventory.booking_id)
	elif request.json["date_search"] == "4":
		return_bookings_facility_check_in = db.session.query(Facility)\
													.filter(Facility.facility_booking_check_in_date >= request.json["start_date"])\
													.filter(Facility.facility_booking_check_in_date <= request.json["end_date"])\
													.all()

		for single_facility_check_in in return_bookings_facility_check_in:
			bookings_array.append(single_facility_check_in.booking_id)
	elif request.json["date_search"] == "5":
		return_bookings_facility_check_out = db.session.query(Facility)\
													.filter(Facility.facility_booking_check_out_date >= request.json["start_date"])\
													.filter(Facility.facility_booking_check_out_date <= request.json["end_date"])\
													.all()

		for single_facility_check_out in return_bookings_facility_check_out:
			bookings_array.append(single_facility_check_out.booking_id)
	elif request.json["date_search"] == "6":
		return_bookings_in_house_range = db.session.query(Booking)\
													.filter(Booking.booking_check_in_date <= request.json["start_date"])\
													.filter(Booking.booking_check_out_date >= request.json["end_date"])\
													.all()

		for single_in_house in return_bookings_in_house_range:
			bookings_array.append(single_in_house.booking_public_id)

	if not set(bookings_array):
		message = []
		message.append("The search did not return any results.")
		return jsonify({"message": message}), 412
	
	return_bookings = db.session.query(Booking)\
								.filter(Booking.booking_public_id.in_(set(bookings_array)))\
								.order_by(Booking.booking_check_in_date.asc())\
								.all()
	
	id_array = []

	for each_id in return_bookings:
		id_array.append(each_id.session_id)

	request_session = requests.Session()

	try:
		return_user = request_session.post(get_user_from_aumra,\
										   json = {"users_ids": id_array})
	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		pass
	
	data = []
	for single in return_bookings:
		return_data = {}
		return_data["booking_public_id"] = single.booking_public_id
		return_data["booking_type"] = single.b_type.booking_type_name
		return_data["booking_type_id"] = single.booking_type
		return_data["booking_check_in_date"] = single.booking_check_in_date
		return_data["booking_check_out_date"] = single.booking_check_out_date
		return_data["booking_done_by"] = single.booking_done_by
		return_data["booking_ref_code"] = single.booking_ref_code

		return_data["facility_bookings"] = single.return_facilities(request_session)
		return_data["inventory_bookings"] = single.return_inventory(request_session)

		check_school_booking = db.session.query(SchoolBooking)\
										 .join(School, SchoolBooking.school_id == School.school_public_id)\
										 .add_columns(School.school_name)\
										 .filter(SchoolBooking.booking_id == single.booking_public_id)\
										 .first()

		check_org_booking = db.session.query(Group)\
									  .filter(Group.booking_id == single.booking_public_id)\
									  .first()

		try:
			if check_school_booking:
				return_data["booking_done_by"] = check_school_booking.school_name
			elif check_org_booking:
				return_data["booking_done_by"] = check_org_booking.organisation_name
			else:
				return_data["booking_done_by"] = single.booking_done_by

		except Exception:
			return_data["booking_done_by"] = single.booking_done_by

		booking_info = {}
		bookingTotal(booking_info, single.booking_public_id)

		return_data["booking_currency"] = booking_info["booking_currency_name"]
		return_data["booking_total"] = booking_info["total_cost"]

		return_data["guests"] = booking_info["guests"]
		return_data["guest_total"] = booking_info["guest_total"]

		return_data["vehicles"] = booking_info["vehicles"]
		return_data["vehicle_total"] = booking_info["vehicle_total"]

		return_data["accommodations"] = booking_info["facility_bookings"]
		return_data["activities"] = booking_info["inventory_bookings"]

		return_data["vat"] = booking_info["tax_breakdown"]["vat"]
		return_data["catering_levy"] = booking_info["tax_breakdown"]["catering"]
		return_data["after_tax"] = booking_info["tax_breakdown"]["after_tax"]
		
		# guest_array = []
		# guest_sum = []

		# get_all_guests = db.session.query(GatepassGuest)\
		# 						.join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
		# 						.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
		# 						.add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
		# 										GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
		# 										GatepassGuest.gatepass_no_of_nights)\
		# 						.filter(GatepassGuest.deletion_marker == None)\
		# 						.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
		# 						.filter(Gatepass.booking_id == single.booking_public_id)\
		# 						.all()

		# for each_guest in get_all_guests:
		# 	guest_data = {}
		# 	guest_data["guest_type"] = each_guest.payment_person
		# 	guest_data["no_of_guests"] = each_guest.gatepass_guest_count

		# 	guest_array.append(guest_data)
		# 	guest_sum.append(int(each_guest.gatepass_guest_count))

		# return_data["guests"] = guest_array
		# return_data["guest_total"] = sum(guest_sum)

		try:
			get_booking_details = db.session.query(Detail)\
											.filter(Detail.booking_id == single.booking_public_id)\
											.filter(Detail.status != get_booking_status_id("Updated"))\
											.first()

			return_data["email_address"] = get_booking_details.email_address
			return_data["phone_number"] = get_booking_details.phone_number
		except AttributeError:
			return_data["email_address"] = ""
			return_data["phone_number"] = ""

		return_data["booking_ticket"] = single.ticket

		check_to_invoice = db.session.query(Invoice)\
									.filter(Invoice.deletion_marker == None)\
									.filter(Invoice.booking_id == single.booking_public_id)\
									.first()

		if check_to_invoice:
			if single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if single.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif single.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if single.b_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif single.b_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif single.b_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif single.b_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif single.b_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif single.b_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif single.b_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = single.b_status.booking_status_name
		return_data["booking_status_id"] = single.status

		if single.session_id:
			if single.session_id == "guest":
				return_data["session_user"] = "Public"
				return_data["session_id"] = single.session_id
			else:
				try:
					for user in return_user.json()["data"]:
						if user["public_id"] == single.session_id:
							return_data["session_user"] = user["full_name"]
							return_data["session_id"] = single.session_id
												
				except (IndexError, KeyError) as user_error:
					return_data["session_user"] = "N/A"
					return_data["session_id"] = single.session_id

				except (AttributeError, UnboundLocalError) as network_related_errors:
					return_data["session_user"] = "Network Error"
					return_data["session_id"] = single.session_id
		else:
			return_data["session_user"] = "N/A"
			return_data["session_id"] = single.session_id

		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		get_partner_booking = db.session.query(Partner)\
										.filter(Partner.deletion_marker == None)\
										.filter(Partner.booking_id == single.booking_public_id)\
										.first()

		if get_partner_booking:
			return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
			
			partner_info = get_details_partner(get_partner_booking.partner_id)
			try:
				return_data["partner_type"] = partner_info["partner_type"][0]
				return_data["partner_name"] = partner_info["name"]
			except Exception:
				return_data["partner_type"] = None
				return_data["partner_name"] = None

		else:
			return_data["partner_booking_ref"] = None
			return_data["partner_type"] = None
			return_data["partner_name"] = None

		if single.member_details:
			booking_details = single.member_details.first()
			
			if booking_details:
				member_info = get_details_member(booking_details.member_id)
				try:
					return_data["member_name"] = member_info["full_name"]
					return_data["membership_level"] = member_info["level"]
				except Exception:
					
					return_data["member_name"] = member_info["company_name"]
					if member_info["corporate"]:
						return_data["membership_level"] = "Corporate"
					else:
						return_data["membership_level"] = "N/A"
			else:
				return_data["member_name"] = None
				return_data["membership_level"] = None
			
		
		return_data["member_name"] = None
		return_data["membership_level"] = None

		get_credit_notes = db.session.query(CreditNote)\
									 .filter(CreditNote.deletion_marker == None)\
									 .filter(CreditNote.booking_id == single.booking_public_id)\
									 .all()

		if get_credit_notes:
			return_data["credit_note_issued"] = True
		else:
			return_data["credit_note_issued"] = False

		data.append(return_data)

	return jsonify({"data": data}), 200


# def search_bookings_by_date_check_in_range():
# 	messages = []
	
# 	try:
# 		request.json["start_date"].strip()
# 		if not request.json["start_date"]:
# 			messages.append("Start date is empty.")
# 	except KeyError as e:
# 		messages.append("Start date is missing.")

# 	try:
# 		request.json["end_date"].strip()
# 		if not request.json["end_date"]:
# 			messages.append("End date is empty.")
# 	except KeyError as e:
# 		messages.append("End date is missing.")

# 	if messages:
# 		message = []
# 		message.append("You appear to be missing some data. Please try again.")
# 		return jsonify({"messages": messages}), 422

# 	if request.json["end_date"] < request.json["start_date"]:
# 		output = []
# 		output.append("The start date cannot come after the end date.")
# 		return jsonify({"message": output}), 422

# 	# 0 -> created_at
# 	# 1 -> check_in
# 	# 2 -> check_out
# 	# 3 -> activity
# 	# 4 -> accommodation_arrival
# 	# 5 -> accommodation_departure
# 	# 6 -> in_house (or between check_in and check_out)
	
# 	bookings_array = []
	
# 	if request.json["date_search"] == "0":
# 		return_bookings_book_check_in_range = db.session.query(Booking)\
# 														.filter(Booking.created_at >= request.json["start_date"])\
# 														.filter(Booking.created_at <= request.json["end_date"])\
# 														.all()

# 		for single_check_in in return_bookings_book_check_in_range:
# 			bookings_array.append(single_check_in.booking_public_id)
# 	elif request.json["date_search"] == "1":
# 		return_bookings_book_check_in_range = db.session.query(Booking)\
# 														.filter(Booking.booking_check_in_date >= request.json["start_date"])\
# 														.filter(Booking.booking_check_in_date <= request.json["end_date"])\
# 														.all()

# 		for single_check_in in return_bookings_book_check_in_range:
# 			bookings_array.append(single_check_in.booking_public_id)
# 	elif request.json["date_search"] == "2":
# 		return_bookings_book_check_out_range = db.session.query(Booking)\
# 														.filter(Booking.booking_check_out_date >= request.json["start_date"])\
# 														.filter(Booking.booking_check_out_date <= request.json["end_date"])\
# 														.all()

# 		for single_check_out in return_bookings_book_check_out_range:
# 			bookings_array.append(single_check_out.booking_public_id)
# 	elif request.json["date_search"] == "3":
# 		return_bookings_inventory = db.session.query(Inventory)\
# 											.filter(Inventory.inventory_booking_date >= request.json["start_date"])\
# 											.filter(Inventory.inventory_booking_date <= request.json["end_date"])\
# 											.all()

# 		for single_inventory in return_bookings_inventory:
# 			bookings_array.append(single_inventory.booking_id)
# 	elif request.json["date_search"] == "4":
# 		return_bookings_facility_check_in = db.session.query(Facility)\
# 													.filter(Facility.facility_booking_check_in_date >= request.json["start_date"])\
# 													.filter(Facility.facility_booking_check_in_date <= request.json["end_date"])\
# 													.all()

# 		for single_facility_check_in in return_bookings_facility_check_in:
# 			bookings_array.append(single_facility_check_in.booking_id)
# 	elif request.json["date_search"] == "5":
# 		return_bookings_facility_check_out = db.session.query(Facility)\
# 													.filter(Facility.facility_booking_check_out_date >= request.json["start_date"])\
# 													.filter(Facility.facility_booking_check_out_date <= request.json["end_date"])\
# 													.all()

# 		for single_facility_check_out in return_bookings_facility_check_out:
# 			bookings_array.append(single_facility_check_out.booking_id)
# 	elif request.json["date_search"] == "6":
# 		return_bookings_in_house_range = db.session.query(Booking)\
# 													.filter(Booking.booking_check_in_date <= request.json["start_date"])\
# 													.filter(Booking.booking_check_out_date >= request.json["end_date"])\
# 													.all()

# 		for single_in_house in return_bookings_in_house_range:
# 			bookings_array.append(single_in_house.booking_public_id)

# 	if not set(bookings_array):
# 		message = []
# 		message.append("The search did not return any results.")
# 		return jsonify({"message": message}), 412
	
# 	return_bookings = db.session.query(Booking)\
# 								.filter(Booking.booking_public_id.in_(set(bookings_array)))\
# 								.order_by(Booking.booking_check_in_date.asc())\
# 								.all()
	
# 	id_array = []

# 	for each_id in return_bookings:
# 		id_array.append(each_id.session_id)

# 	request_session = requests.Session()

# 	try:
# 		return_user = request_session.post(get_user_from_aumra,\
# 										   json = {"users_ids": id_array})
# 	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
# 		pass
	
# 	data = []
# 	for single in return_bookings:
# 		return_data = {}
# 		return_data["booking_public_id"] = single.booking_public_id
# 		return_data["booking_type"] = single.b_type.booking_type_name
# 		return_data["booking_type_id"] = single.booking_type
# 		return_data["booking_check_in_date"] = single.booking_check_in_date
# 		return_data["booking_check_out_date"] = single.booking_check_out_date
# 		return_data["booking_done_by"] = single.booking_done_by
# 		return_data["booking_ref_code"] = single.booking_ref_code

# 		return_data["facility_bookings"] = single.return_facilities(request_session)
# 		return_data["inventory_bookings"] = single.return_inventory(request_session)

# 		check_school_booking = db.session.query(SchoolBooking)\
# 										 .join(School, SchoolBooking.school_id == School.school_public_id)\
# 										 .add_columns(School.school_name)\
# 										 .filter(SchoolBooking.booking_id == single.booking_public_id)\
# 										 .first()

# 		check_org_booking = db.session.query(Group)\
# 									  .filter(Group.booking_id == single.booking_public_id)\
# 									  .first()

# 		try:
# 			if check_school_booking:
# 				return_data["booking_done_by"] = check_school_booking.school_name
# 			elif check_org_booking:
# 				return_data["booking_done_by"] = check_org_booking.organisation_name
# 			else:
# 				return_data["booking_done_by"] = single.booking_done_by

# 		except Exception:
# 			return_data["booking_done_by"] = single.booking_done_by

# 		booking_info = {}
# 		bookingTotal(booking_info, single.booking_public_id)

# 		return_data["booking_currency"] = booking_info["booking_currency_name"]
# 		return_data["booking_total"] = booking_info["total_cost"]

# 		return_data["guests"] = booking_info["guests"]
# 		return_data["guest_total"] = booking_info["guest_total"]

# 		return_data["vehicles"] = booking_info["vehicles"]
# 		return_data["vehicle_total"] = booking_info["vehicle_total"]

# 		return_data["accommodations"] = booking_info["facility_bookings"]
# 		return_data["activities"] = booking_info["inventory_bookings"]

# 		return_data["vat"] = booking_info["tax_breakdown"]["vat"]
# 		return_data["catering_levy"] = booking_info["tax_breakdown"]["catering"]
# 		return_data["after_tax"] = booking_info["tax_breakdown"]["after_tax"]
		
# 		# guest_array = []
# 		# guest_sum = []

# 		# get_all_guests = db.session.query(GatepassGuest)\
# 		# 						.join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
# 		# 						.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
# 		# 						.add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
# 		# 										GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
# 		# 										GatepassGuest.gatepass_no_of_nights)\
# 		# 						.filter(GatepassGuest.deletion_marker == None)\
# 		# 						.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
# 		# 						.filter(Gatepass.booking_id == single.booking_public_id)\
# 		# 						.all()

# 		# for each_guest in get_all_guests:
# 		# 	guest_data = {}
# 		# 	guest_data["guest_type"] = each_guest.payment_person
# 		# 	guest_data["no_of_guests"] = each_guest.gatepass_guest_count

# 		# 	guest_array.append(guest_data)
# 		# 	guest_sum.append(int(each_guest.gatepass_guest_count))

# 		# return_data["guests"] = guest_array
# 		# return_data["guest_total"] = sum(guest_sum)

# 		try:
# 			get_booking_details = db.session.query(Detail)\
# 											.filter(Detail.booking_id == single.booking_public_id)\
# 											.filter(Detail.status != get_booking_status_id("Updated"))\
# 											.first()

# 			return_data["email_address"] = get_booking_details.email_address
# 			return_data["phone_number"] = get_booking_details.phone_number
# 		except AttributeError:
# 			return_data["email_address"] = ""
# 			return_data["phone_number"] = ""

# 		return_data["booking_ticket"] = single.ticket

# 		check_to_invoice = db.session.query(Invoice)\
# 									.filter(Invoice.deletion_marker == None)\
# 									.filter(Invoice.booking_id == single.booking_public_id)\
# 									.first()

# 		if check_to_invoice:
# 			if single.deletion_marker == 1:
# 				return_data["booking_status"] = "Cancelled"
# 				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
# 			else:
# 				return_data["booking_status"] = "To Invoice"
# 				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

# 		else:
# 			if single.checked_out == 1:
# 				return_data["booking_status"] = "Checked Out"
# 				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
# 			elif single.checked_in == 1:
# 				return_data["booking_status"] = "Checked In"
# 				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
# 			elif single.deletion_marker == 1:
# 				return_data["booking_status"] = "Cancelled"
# 				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
# 			else:
# 				if single.b_status.booking_status_name == "Unconfirmed":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
# 				elif single.b_status.booking_status_name == "Confirmed":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
# 				elif single.b_status.booking_status_name == "No-Show":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
# 				elif single.b_status.booking_status_name == "Abandoned":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
# 				elif single.b_status.booking_status_name == "Updated":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
# 				elif single.b_status.booking_status_name == "Deposit":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
# 				elif single.b_status.booking_status_name == "Complimentary":
# 					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
# 				else:
# 					return_data["booking_color"] = "background-color: #fff; color: #000000"
# 				return_data["booking_status"] = single.b_status.booking_status_name
# 		return_data["booking_status_id"] = single.status

# 		if single.session_id:
# 			if single.session_id == "guest":
# 				return_data["session_user"] = "Public"
# 				return_data["session_id"] = single.session_id
# 			else:
# 				try:
# 					for user in return_user.json()["data"]:
# 						if user["public_id"] == single.session_id:
# 							return_data["session_user"] = user["full_name"]
# 							return_data["session_id"] = single.session_id
												
# 				except (IndexError, KeyError) as user_error:
# 					return_data["session_user"] = "N/A"
# 					return_data["session_id"] = single.session_id

# 				except (AttributeError, UnboundLocalError) as network_related_errors:
# 					return_data["session_user"] = "Network Error"
# 					return_data["session_id"] = single.session_id
# 		else:
# 			return_data["session_user"] = "N/A"
# 			return_data["session_id"] = single.session_id

# 		return_data["created_at"] = single.created_at
# 		return_data["updated_at"] = single.updated_at

# 		get_partner_booking = db.session.query(Partner)\
# 										.filter(Partner.deletion_marker == None)\
# 										.filter(Partner.booking_id == single.booking_public_id)\
# 										.first()

# 		if get_partner_booking:
# 			return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
			
# 			partner_info = get_details_partner(get_partner_booking.partner_id)
# 			try:
# 				return_data["partner_type"] = partner_info["partner_type"][0]
# 				return_data["partner_name"] = partner_info["name"]
# 			except Exception:
# 				return_data["partner_type"] = None
# 				return_data["partner_name"] = None

# 		else:
# 			return_data["partner_booking_ref"] = None
# 			return_data["partner_type"] = None
# 			return_data["partner_name"] = None

# 		if single.member_details:
# 			booking_details = single.member_details.first()
			
# 			if booking_details:
# 				member_info = get_details_member(booking_details.member_id)
# 				try:
# 					return_data["member_name"] = member_info["full_name"]
# 					return_data["membership_level"] = member_info["level"]
# 				except Exception:
# 					return_data["member_name"] = member_info["company_name"]
# 					if member_info["corporate"]:
# 						return_data["membership_level"] = "Corporate"
# 					else:
# 						return_data["membership_level"] = "N/A"
# 			else:
# 				return_data["member_name"] = None
# 				return_data["membership_level"] = None
			
# 		else:
# 			return_data["member_name"] = None
# 			return_data["membership_level"] = None

# 		get_credit_notes = db.session.query(CreditNote)\
# 									 .filter(CreditNote.deletion_marker == None)\
# 									 .filter(CreditNote.booking_id == single.booking_public_id)\
# 									 .all()

# 		if get_credit_notes:
# 			return_data["credit_note_issued"] = True
# 		else:
# 			return_data["credit_note_issued"] = False

# 		data.append(return_data)

# 	return jsonify({"data": data}), 200


@app.route("/bookings/search/partner/by_key", methods = ["POST"])
def partner_booking_search():
	validation_list = [
		{"field": "key"},
		{"field": "partner_id", "alias": "Partner ID"}
	]
	
	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	bookings_array = []

	return_bookings_public_id = db.session.query(Booking)\
										  .join(Partner, Booking.booking_public_id == Partner.booking_id)\
										  .add_columns(Booking.booking_public_id)\
										  .filter(Booking.booking_public_id == request.json["key"]\
												or Booking.booking_public_id.contains(request.json["key"]))\
										  .filter(Partner.partner_id == request.json["partner_id"])\
										  .all()

	for single_public_id in return_bookings_public_id:
		bookings_array.append(single_public_id.booking_public_id)

	return_bookings_ref_code = db.session.query(Booking)\
										 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
										 .add_columns(Booking.booking_public_id)\
										 .filter(Booking.booking_ref_code == request.json["key"].lower()\
												 or Booking.booking_ref_code.contains(request.json["key"].lower()))\
										 .filter(Partner.partner_id == request.json["partner_id"])\
										 .all()

	for single_ref_code in return_bookings_ref_code:
		bookings_array.append(single_ref_code.booking_public_id)

	return_bookings_partner_ref_code = db.session.query(Partner)\
												 .filter(Partner.partner_booking_ref == request.json["key"]\
														 or Partner.partner_booking_ref.contains(request.json["key"]))\
												 .filter(Partner.partner_id == request.json["partner_id"])\
												 .all()

	for single_partner_ref_code in return_bookings_partner_ref_code:
		bookings_array.append(single_partner_ref_code.booking_id)

	return_bookings_done_by = db.session.query(Booking)\
										.join(Partner, Booking.booking_public_id == Partner.booking_id)\
										.add_columns(Booking.booking_public_id)\
										.filter(Booking.booking_done_by == request.json["key"]\
											or Booking.booking_done_by.contains(request.json["key"]))\
										.filter(Partner.partner_id == request.json["partner_id"])\
										.all()

	for single_done_by in return_bookings_done_by:
		bookings_array.append(single_done_by.booking_public_id)

	return_bookings_tickets = db.session.query(Booking)\
										.join(Partner, Booking.booking_public_id == Partner.booking_id)\
										.add_columns(Booking.booking_public_id)\
										.filter(Booking.ticket == request.json["key"]\
											or Booking.ticket.contains(request.json["key"]))\
										.filter(Partner.partner_id == request.json["partner_id"])\
										.all()

	for single_ticket in return_bookings_tickets:
		bookings_array.append(single_ticket.booking_public_id)

	return_bookings_email = db.session.query(Detail)\
									  .join(Partner, Detail.booking_id == Partner.booking_id)\
									  .add_columns(Detail.booking_id)\
									  .filter(Detail.email_address == request.json["key"]\
											  or Detail.email_address.contains(request.json["key"]))\
									  .filter(Partner.partner_id == request.json["partner_id"])\
									  .all()

	for single_email in return_bookings_email:
		bookings_array.append(single_email.booking_id)

	return_bookings_first_name = db.session.query(Detail)\
										   .join(Partner, Detail.booking_id == Partner.booking_id)\
										   .add_columns(Detail.booking_id)\
										   .filter(Detail.first_name.ilike(request.json["key"]))\
										   .filter(Partner.partner_id == request.json["partner_id"])\
										   .all()

	for single_first_name in return_bookings_first_name:
		bookings_array.append(single_first_name.booking_id)

	return_bookings_last_name = db.session.query(Detail)\
										  .join(Partner, Detail.booking_id == Partner.booking_id)\
										  .add_columns(Detail.booking_id)\
										  .filter(Detail.last_name.ilike(request.json["key"]))\
										  .filter(Partner.partner_id == request.json["partner_id"])\
									  	  .all()

	for single_last_name in return_bookings_last_name:
		bookings_array.append(single_last_name.booking_id)

	return_bookings_numbers = db.session.query(Detail)\
										.join(Partner, Detail.booking_id == Partner.booking_id)\
										.add_columns(Detail.booking_id)\
										.filter(Detail.phone_number == request.json["key"]\
											  	or Detail.phone_number.contains(request.json["key"]))\
										.filter(Partner.partner_id == request.json["partner_id"])\
									  	.all()

	for single_number in return_bookings_numbers:
		bookings_array.append(single_number.booking_id)

	return_bookings_reg = db.session.query(CheckInVehicle)\
									.join(Partner, CheckInVehicle.booking_id == Partner.booking_id)\
									.add_columns(CheckInVehicle.booking_id)\
									.filter(CheckInVehicle.check_vehicle_reg == request.json["key"]\
											or CheckInVehicle.check_vehicle_reg.contains(request.json["key"]))\
									.filter(Partner.partner_id == request.json["partner_id"])\
									.all()

	for single_reg in return_bookings_reg:
		bookings_array.append(single_reg.booking_id)

	if not set(bookings_array):
		message = []
		message.append("The search did not return any results.")
		return jsonify({"message": message}), 412
	
	return_bookings = db.session.query(Booking)\
								.filter(Booking.booking_public_id.in_(set(bookings_array)))\
								.order_by(Booking.booking_check_in_date.asc())\
								.all()

	id_array = []

	for each_id in return_bookings:
		id_array.append(each_id.session_id)

	request_session = requests.Session()

	try:
		return_user = request_session.post(get_user_from_aumra,\
										   json = {"users_ids": id_array})
	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		pass
	
	data = []
	for single in return_bookings:
		return_data = {}
		return_data["booking_public_id"] = single.booking_public_id
		return_data["booking_type"] = single.b_type.booking_type_name
		return_data["booking_type_id"] = single.booking_type
		return_data["booking_check_in_date"] = single.booking_check_in_date
		return_data["booking_check_out_date"] = single.booking_check_out_date
		return_data["booking_done_by"] = single.booking_done_by
		return_data["booking_ref_code"] = single.booking_ref_code
		return_data["booking_done_by"] = single.booking_done_by

		get_partner_booking_details = db.session.query(Partner)\
												.filter(Partner.deletion_marker == None)\
												.filter(Partner.booking_id == single.booking_public_id)\
												.first()

		return_data["partner_booking_ref"] = get_partner_booking_details.partner_booking_ref
		
		booking_info = {}
		bookingTotal(booking_info, single.booking_public_id)

		return_data["booking_currency"] = booking_info["booking_currency_name"]
		return_data["booking_total"] = booking_info["total_cost"]

		return_data["facility_bookings"] = booking_info["facility_bookings"]
		return_data["inventory_bookings"] = booking_info["inventory_bookings"]
		
		guest_array = []
		guest_sum = []

		get_all_guests = db.session.query(GatepassGuest)\
								   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
								   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
												GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
												GatepassGuest.gatepass_no_of_nights)\
								   .filter(GatepassGuest.deletion_marker == None)\
								   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								   .filter(Gatepass.booking_id == single.booking_public_id)\
								   .all()

		for each_guest in get_all_guests:
			guest_data = {}
			guest_data["guest_type"] = each_guest.payment_person
			guest_data["no_of_guests"] = each_guest.gatepass_guest_count

			guest_array.append(guest_data)
			guest_sum.append(int(each_guest.gatepass_guest_count))

		return_data["guests"] = guest_array
		return_data["guest_total"] = sum(guest_sum)

		check_to_invoice = db.session.query(Invoice)\
									.filter(Invoice.deletion_marker == None)\
									.filter(Invoice.booking_id == single.booking_public_id)\
									.first()

		if check_to_invoice:
			if single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if single.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif single.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if single.b_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif single.b_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif single.b_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif single.b_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif single.b_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif single.b_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif single.b_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = single.b_status.booking_status_name
		return_data["booking_status_id"] = single.status

		if single.session_id:
			if single.session_id == "guest":
				return_data["session_user"] = "Public"
				return_data["session_id"] = single.session_id
			else:
				try:
					for user in return_user.json()["data"]:
						if user["public_id"] == single.session_id:
							return_data["session_user"] = user["full_name"]
							return_data["session_id"] = single.session_id
												
				except (IndexError, KeyError) as user_error:
					return_data["session_user"] = "N/A"
					return_data["session_id"] = single.session_id

				except (AttributeError, UnboundLocalError) as network_related_errors:
					return_data["session_user"] = "Network Error"
					return_data["session_id"] = single.session_id
		else:
			return_data["session_user"] = "N/A"
			return_data["session_id"] = single.session_id

		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		data.append(return_data)

	return jsonify({"data": data}), 200


@app.route("/bookings/search/by_date/partner/range", methods = ["POST"])
def search_partner_bookings_by_date_range():
	messages = []
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	try:
		request.json["partner_id"].strip()
		if not request.json["partner_id"]:
			messages.append("Partner ID is empty.")
	except KeyError as e:
		messages.append("Partner ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	if request.json["end_date"] < request.json["start_date"]:
		message = []
		message.append("The start date cannot come after the end date.")
		return jsonify({"message": message}), 422

	bookings_array = []
	
	return_bookings_book_check_in_range = db.session.query(Booking)\
													.join(Partner, Booking.booking_public_id == Partner.booking_id)\
													.add_columns(Booking.booking_public_id)\
													.filter(Booking.booking_check_in_date >= request.json["start_date"])\
													.filter(Booking.booking_check_in_date <= request.json["end_date"])\
													.filter(Partner.partner_id == request.json["partner_id"])\
													.all()

	for single_check_in in return_bookings_book_check_in_range:
		bookings_array.append(single_check_in.booking_public_id)

	return_bookings_book_check_out_range = db.session.query(Booking)\
													 .join(Partner, Booking.booking_public_id == Partner.booking_id)\
													 .add_columns(Booking.booking_public_id)\
													 .filter(Booking.booking_check_out_date >= request.json["start_date"])\
													 .filter(Booking.booking_check_out_date <= request.json["end_date"])\
													 .filter(Partner.partner_id == request.json["partner_id"])\
													 .all()

	for single_check_out in return_bookings_book_check_out_range:
		bookings_array.append(single_check_out.booking_public_id)

	return_bookings_inventory = db.session.query(Inventory)\
										  .join(Partner, Inventory.booking_id == Partner.booking_id)\
										  .add_columns(Inventory.booking_id)\
										  .filter(Inventory.inventory_booking_date >= request.json["start_date"])\
										  .filter(Inventory.inventory_booking_date <= request.json["end_date"])\
										  .filter(Partner.partner_id == request.json["partner_id"])\
										  .all()

	for single_inventory in return_bookings_inventory:
		bookings_array.append(single_inventory.booking_id)

	return_bookings_facility_check_in = db.session.query(Facility)\
												  .join(Partner, Facility.booking_id == Partner.booking_id)\
												  .add_columns(Facility.booking_id)\
												  .filter(Facility.facility_booking_check_in_date >= request.json["start_date"])\
												  .filter(Facility.facility_booking_check_in_date <= request.json["end_date"])\
												  .filter(Partner.partner_id == request.json["partner_id"])\
												  .all()

	for single_facility_check_in in return_bookings_facility_check_in:
		bookings_array.append(single_facility_check_in.booking_id)

	return_bookings_facility_check_out = db.session.query(Facility)\
												   .join(Partner, Facility.booking_id == Partner.booking_id)\
												   .add_columns(Facility.booking_id)\
												   .filter(Facility.facility_booking_check_out_date >= request.json["start_date"])\
												   .filter(Facility.facility_booking_check_out_date <= request.json["end_date"])\
												   .filter(Partner.partner_id == request.json["partner_id"])\
												   .all()

	for single_facility_check_out in return_bookings_facility_check_out:
		bookings_array.append(single_facility_check_out.booking_id)

	if not set(bookings_array):
		message = []
		message.append("The search did not return any results.")
		return jsonify({"message": message}), 412
	
	return_bookings = db.session.query(Booking)\
								.filter(Booking.booking_public_id.in_(set(bookings_array)))\
								.order_by(Booking.booking_check_in_date.asc())\
								.all()
	
	id_array = []

	for each_id in return_bookings:
		id_array.append(each_id.session_id)

	request_session = requests.Session()

	try:
		return_user = request_session.post(get_user_from_aumra,\
										   json = {"users_ids": id_array})
	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		pass
	
	data = []
	for single in return_bookings:
		return_data = {}
		return_data["booking_public_id"] = single.booking_public_id
		return_data["booking_type"] = single.b_type.booking_type_name
		return_data["booking_type_id"] = single.booking_type
		return_data["booking_check_in_date"] = single.booking_check_in_date
		return_data["booking_check_out_date"] = single.booking_check_out_date
		return_data["booking_done_by"] = single.booking_done_by
		return_data["booking_ref_code"] = single.booking_ref_code
		return_data["booking_done_by"] = single.booking_done_by

		get_partner_booking_details = db.session.query(Partner)\
												.filter(Partner.deletion_marker == None)\
												.filter(Partner.booking_id == single.booking_public_id)\
												.first()

		return_data["partner_booking_ref"] = get_partner_booking_details.partner_booking_ref

		booking_info = {}
		bookingTotal(booking_info, single.booking_public_id)

		return_data["booking_currency"] = booking_info["booking_currency_name"]
		return_data["booking_total"] = booking_info["total_cost"]

		return_data["facility_bookings"] = booking_info["facility_bookings"]
		return_data["inventory_bookings"] = booking_info["inventory_bookings"]
		
		guest_array = []
		guest_sum = []

		get_all_guests = db.session.query(GatepassGuest)\
								.join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
								.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								.add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
												GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
												GatepassGuest.gatepass_no_of_nights)\
								.filter(GatepassGuest.deletion_marker == None)\
								.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.booking_id == single.booking_public_id)\
								.all()

		for each_guest in get_all_guests:
			guest_data = {}
			guest_data["guest_type"] = each_guest.payment_person
			guest_data["no_of_guests"] = each_guest.gatepass_guest_count

			guest_array.append(guest_data)
			guest_sum.append(int(each_guest.gatepass_guest_count))

		return_data["guests"] = guest_array
		return_data["guest_total"] = sum(guest_sum)

		check_to_invoice = db.session.query(Invoice)\
									.filter(Invoice.deletion_marker == None)\
									.filter(Invoice.booking_id == single.booking_public_id)\
									.first()

		if check_to_invoice:
			if single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if single.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif single.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif single.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if single.b_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif single.b_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif single.b_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif single.b_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif single.b_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif single.b_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif single.b_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = single.b_status.booking_status_name
		return_data["booking_status_id"] = single.status

		if single.session_id:
			if single.session_id == "guest":
				return_data["session_user"] = "Public"
				return_data["session_id"] = single.session_id
			else:
				try:
					for user in return_user.json()["data"]:
						if user["public_id"] == single.session_id:
							return_data["session_user"] = user["full_name"]
							return_data["session_id"] = single.session_id
												
				except (IndexError, KeyError) as user_error:
					return_data["session_user"] = "N/A"
					return_data["session_id"] = single.session_id

				except (AttributeError, UnboundLocalError) as network_related_errors:
					return_data["session_user"] = "Network Error"
					return_data["session_id"] = single.session_id
		else:
			return_data["session_user"] = "N/A"
			return_data["session_id"] = single.session_id

		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		data.append(return_data)

	return jsonify({"data": data}), 200
