from flask import jsonify
from datetime import datetime, timedelta

import os
import glob
import time

# File imports
from routes import app
from functions.date_operators import GenerateDateFromString


@app.route("/tickets/view", methods = ["GET"])
def view_tickets():
	# documents = os.listdir("./tickets")
	documents_alt = glob.glob("./tickets/receipt-*")

	data = []
	
	for single in documents_alt:
		return_data = {}
		return_data["name"] = single.strip("./tickets/")
		return_data["url"] = """/docs/tickets/" + single.strip("./tickets/")

		file_details = os.stat(single)

		return_data["date_created"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(file_details.st_mtime))

		data.append(return_data)

		data = sorted(data, key = lambda order: order["date_created"], reverse = True)

	# for single in documents:
	# 	return_data = {}
	# 	return_data["name"] = single
	# 	return_data["url"] = """/docs/tickets/" + single

	# 	file_details = os.stat(single)

	# 	data.append(return_data)

	return jsonify({"data": data}), 200


@app.route("/passes/view", methods = ["GET"])
def view_guest_passes():
	documents_alt = glob.glob("./tickets/auth-visitor-*")

	data = []
	
	for single in documents_alt:
		return_data = {}
		return_data["name"] = single.strip("./tickets/")
		return_data["url"] = """/docs/tickets/" + single.strip("./tickets/")

		file_details = os.stat(single)

		return_data["date_created"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(file_details.st_mtime))

		data.append(return_data)

		data = sorted(data, key = lambda order: order["date_created"], reverse = True)

	return jsonify({"data": data}), 200


@app.route("/batchfiles/view", methods = ["GET"])
def view_batchfiles():
	documents_alt = glob.glob("./batchfiles/*.csv")

	data = []
	
	for single in documents_alt:
		return_data = {}
		return_data["name"] = single.strip("./batchfiles/")
		return_data["url"] = """/docs/batchfiles/" + single.strip("./batchfiles/")

		file_details = os.stat(single)

		return_data["date_created"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(file_details.st_mtime))

		data.append(return_data)

		data = sorted(data, key = lambda order: order["date_created"], reverse = True)

	return jsonify({"data": data}), 200


@app.route("/invoices/view", methods = ["GET"])
def view_invoices():
	documents_alt = glob.glob("./invoices/*.pdf")

	data = []
	
	for single in documents_alt:
		return_data = {}
		return_data["name"] = "invoice" + single.strip("./invoices/")
		return_data["url"] = """/docs/invoices/" + "invoice" + single.strip("./invoices/")

		file_details = os.stat(single)

		return_data["date_created"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(file_details.st_mtime))

		data.append(return_data)

		data = sorted(data, key = lambda order: order["date_created"], reverse = True)

	return jsonify({"data": data}), 200