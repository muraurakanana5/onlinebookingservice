import math
import os
import uuid
from datetime import date, datetime, timedelta

import pymysql
import requests
from flask import (Flask, json, jsonify, redirect, render_template, request,
				   url_for)

from database.booking_activity_log import BookingActivity
from database.booking_details import Detail
from database.booking_guest_types import GuestType
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_types import BookingType
from database.bookings import Booking
from database.bookings_notes import Note
from database.facility import Facility
from database.gatepass import Gatepass
from database.gatepass_details import GatepassDetail
from database.gatepass_guests import GatepassGuest
from database.gatepass_vehicles import GatepassVehicle
from database.inventory import Inventory
from database.invoice import Invoice
from database.mandatory_payment_prices import MandatoryPaymentPrices
from database.mandatory_payments import Mandatory
from database.partner import Partner
from database.transaction import Transaction
from database.vehicle import Vehicle
from functions.async_functions import AsyncRequests
from functions.booking_snippets import *
from functions.currency_operators import *
from functions.date_operators import *
from functions.validation import *
# File imports
from routes import FromCache, app, bookings_logger, db, db_cache
from routes.bookings_urls import currencyHandler, send_booking_email, send_booking_update_email
from routes.seed_functions import (BookingGuestTypesSeed, BookingStatusSeed,
								   BookingTypeSeed, MandatoryPaymentsSeed)
from variables import *
from routes.salesforce_v2 import send_booking_info_to_salesforce


def close(self):
	self.session.close()


##########################
#### Partner Bookings ####
##########################
@app.route("/partners/bookings/new", methods = ["POST"])
def add_new_partner_booking():
		
	today = datetime.now().strftime("%Y-%m-%d")


	if db.session.query(BookingStatus)\
	 .filter(BookingStatus.deletion_marker==None)\
	 .filter(BookingStatus.booking_status_public_id == "626f13f2-aab0-4488-a774-44969b3062a3")\
	 .count() == 0:
		BookingStatusSeed.seed_default_booking_status()

	if db.session.query(GuestType)\
	 .filter(GuestType.deletion_marker==None)\
	 .filter(GuestType.booking_guest_type_public_id == "2df20a96")\
	 .count() == 0:
		BookingGuestTypesSeed.seed_default_booking_guest_types_methods()

	# Field validation
	validation_list = [{
		"field": "type",
		"alias": "Booking type"
	}, {
		"field": "first_name",
		"alias": "First name"
	}, {
		"field": "last_name",
		"alias": "Last name"
	}, {
		"field": "check_in",
		"alias": "Check-in date"
	}, {
		"field": "check_out",
		"alias": "Check-out date"
	}, {
		"field": "currency"
	}, {
		"field": "partner",
		"alias": "Partner details"
	}, {
		"field": "session_id",
		"alias": "Session ID"
	}]

	messages = fieldValidation(request.json, validation_list)

	partner_temp = requests.get(
		get_partner_details.format(request.json["partner"]))
	partner_details = partner_temp.json()
	partner_type = partner_details["type_public_id"]

	if messages:
		return jsonify({"messages": messages}), 422

	seven_days_ago = (datetime.now() - timedelta(days=7)).strftime("%Y-%m-%d")

	# Date validation
	if request.json["check_in"] < today:
		message = []
		message.append("Your check in date cannot be in the past.")
		return jsonify({"message": message}), 422

	# if request.json["check_in"] < seven_days_ago:
	# 	message = []
	# 	message.append("There is an error with the set check-in date. Try again.")
	# 	return jsonify({"message": message}), 422

	if request.json["check_out"] < today:
		message = []
		message.append("Your check out date cannot be in the past.")
		return jsonify({"message": message}), 422

	# if request.json["check_out"] < seven_days_ago:
	# 	message = []
	# 	message.append("There is an error with the set check-out date. Try again.")
	# 	return jsonify({"message": message}), 422

	if request.json["check_out"] < request.json["check_in"]:
		message = []
		message.append(
			"Your check-out date cannot come before your check-in date.")
		return jsonify({"message": message}), 422

	try:
		promo_code_json = request.json["promo_code"]

	except Exception:
		promo_code_json = None

	if promo_code_json:
		try:
			post_data = {
				"promo_code": promo_code_json,
				"check_in_date": request.json["check_in"],
				"check_out_date": request.json["check_out"]
			}

			check_promo_validity = requests.post(promo_code_search,
												 json=post_data)

			if check_promo_validity.status_code != 200:
				return jsonify(
					{"message": check_promo_validity.json()["message"]}), 422

			elif check_promo_validity.status_code == 200:
				promo_code = promo_code_json
				promo_discount = int(
					check_promo_validity.json()["data"][0]["percentage_off"])
				promo_code_data = check_promo_validity.json()["data"][0]
				promo_code_public_id = check_promo_validity.json(
				)["data"][0]["public_id"]

		except Exception:
			message = []
			message.append(
				"There was an issue getting the promo code details. Please try again or leave the promo code field blank."
			)
			return jsonify({"message": message}), 422

	else:
		promo_code_data = None

		promo_discount = None
		promo_code = None
		promo_code_public_id = None
	
	
	# Validating the number of guests
	guest_sum = []

	for each_guest in request.json["guests"]:
		try:
			guest_numbers = int(each_guest["payment_guests"])
		except Exception:
			guest_numbers = 0
		guest_sum.append(guest_numbers)

	# Generating date from string using a simple, custom function
	check_in_date = GenerateDateFromString.generateDate(
		request.json["check_in"])
	check_out_date = GenerateDateFromString.generateDate(
		request.json["check_out"])

	# Getting date difference using a simple, custom function
	temp_date_diff = DateOperations.returnDateDifferenceInDays(
		check_out_date, check_in_date)

	if temp_date_diff == 0:
		date_diff = 1
	elif temp_date_diff > 0:
		date_diff = temp_date_diff

	try:
		exchange_rate_data = requests.get(get_latest_exchange_rate)
	except requests.exceptions.ConnectionError as connection_error:
		message = []
		message.append(
			"The currency exchange rate is not available at the moment. Please try again later."
		)
		return jsonify({
			"message": message,
			"error": str(connection_error)
		}), 422

	try:
		partner_temp = requests.get(
			get_partner_details.format(request.json["partner"]))
		partner_details = partner_temp.json()
	except requests.exceptions.ConnectionError as connection_error:
		message = []
		message.append(
			"The partner details are not available at the moment. Please try again later."
		)
		return jsonify({
			"message": message,
			"error": str(connection_error)
		}), 422

	try:
		gatepass_discount = float(partner_details["conservation_fee_discount"])
	except (KeyError) as no_value:
		gatepass_discount = 0

		# message = []
		# message.append("The partner conservation fee discount are not available at the moment. Please try again later.")
		# return jsonify({"message": message, "error": str(no_value)}), 422

	try:
		phone = partner_details["tel"]
	except KeyError:
		phone = None

	phone_validation = validatePhoneNumber(phone)

	first_name = request.json["first_name"].title()
	last_name = request.json["last_name"].title()

	if phone_validation[0]:
		phone = phone_validation[1]

	else:
		message = []
		message.append(phone_validation[1])
		return jsonify({"message": message}), 422

	## TODO: Undo this
	## This is for disabling emails to Tour Operators
	# try:
	# 	partner_type = partner_details["type_public_id"]

	# 	if partner_type == "6bac8c28":
	# 		partner_email = True
	# 	else:
	# 		partner_email = None
	# except Exception:
	# 	partner_email = True
	partner_type = partner_details["type_public_id"]
	partner_email = None

	email = partner_details["email"].lower()

	valid_email = validateEmail(email)

	if not valid_email:
		message = []
		message.append("The partner email address used is invalid.")
		return jsonify({"message": message}), 422

	email_array = []

	try:
		if request.json["agent_email"]:
			validate_agent_email = validateEmail(
				request.json["agent_email"].lower())

			if not validate_agent_email:
				message = []
				message.append("The agent email provided is not valid.")
				return jsonify({"message": message}), 422

			email_array.append(request.json["agent_email"].lower())

			# agent_email = request.json["agent_email"]
	except Exception:
		pass

	if request.json["payment_period"] == 3 and partner_details[
			"can_be_invoiced"]:
		booking_status = get_booking_status_id("To Invoice")
		payment_status = 4
	else:
		booking_status = get_booking_status_id("Unconfirmed")
		payment_status = None

	booking_id = str(uuid.uuid4())
	gatepass_id = str(uuid.uuid4())
	booking_ref = str(uuid.uuid4())[:10]

	get_booking_exchange_rate = requests.get(
		get_buy_sell_rate.format(request.json["currency"]))
	booking_buying_rate = get_booking_exchange_rate.json(
	)["data"][0]["currency_buy_amount"]
	booking_selling_rate = get_booking_exchange_rate.json(
	)["data"][0]["currency_sell_amount"]

	booking = Booking(booking_public_id=booking_id,
					  booking_type=request.json["type"],
					  booking_check_in_date=request.json["check_in"],
					  booking_check_out_date=request.json["check_out"],
					  booking_done_by=request.json["first_name"].title() +
					  " " + request.json["last_name"].title(),
					  booking_ref_code=booking_ref,
					  status=booking_status,
					  payment_status=payment_status,
					  currency=request.json["currency"],
					  currency_buying_rate_at_time=booking_buying_rate,
					  currency_selling_rate_at_time=booking_selling_rate,
					  session_id=request.json["session_id"],
					  created_at=datetime.now(),
					  updated_at=datetime.now())

	db.session.add(booking)

	partner = Partner(partner_booking_public_id=str(uuid.uuid4()),
					  booking_id=booking_id,
					  partner_id=request.json["partner"],
					  partner_booking_ref=request.json["partner_booking_ref"],
					  agent_email=request.json["agent_email"].lower(),
					  session_id=request.json["session_id"],
					  created_at=datetime.now(),
					  updated_at=datetime.now())

	db.session.add(partner)

	booking_activity = BookingActivity(
		booking_activity_public_id=str(uuid.uuid4()),
		booking_id=booking_id,
		booking_activity_description="Booking created",
		session_id=request.json["session_id"],
		created_at=datetime.now())

	db.session.add(booking_activity)

	booking_info = {}
	booking_info["booking_id"] = booking_id
	booking_info["booking_ref"] = booking_ref
	booking_info["first_name"] = first_name
	booking_info["last_name"] = last_name

	facility_email_data = []

	try:
		facilityBooking(request.json,
						booking_info,
						facility_email_data,
						partner_details=partner_details,
						promo_code_details=promo_code_data)
	except Exception as e:
		return jsonify({"message": str(e)}), 422

	inventory_email_data = []

	try:
		inventoryBooking(request.json,
						 booking_info,
						 inventory_email_data,
						 partner_details=partner_details,
						 promo_code_details=promo_code_data)
	except Exception as e:
		return jsonify({"message": str(e)}), 422

	try:
		for gatepass_guest in request.json["guests"]:
			get_guest_type = db.session.query(Mandatory)\
			   .filter(Mandatory.deletion_marker == None)\
			   .filter(Mandatory.payment_public_id == gatepass_guest["payment_public_id"])\
			   .first()

			try:
				guest_count = int(gatepass_guest["payment_guests"])
			except Exception:
				guest_count = 0

			guest = BookingGuest(booking_guest_public_id=str(uuid.uuid4()),
								 booking_id=booking_id,
								 guest_type=get_booking_guest_type_id(
									 get_guest_type.payment_person),
								 guest_count=guest_count)

			db.session.add(guest)

	except KeyError as guest_error:
		print("There was an error getting: " + str(guest_error))

	try:
		additional_note = request.json["additional_note"]
	except KeyError as guest_key_error:
		additional_note = None

	try:
		address = request.json["address"]
	except KeyError as address_key_error:
		address = None

	try:
		country = request.json["country"]
	except KeyError as country_key_error:
		country = None

	try:
		city = request.json["city"]
	except KeyError as city_key_error:
		city = None

	# try:
	# 	first_name,last_name = partner_details["name"].split(" ")
	# except Exception as e:
	# 	first_name = None
	# 	last_name = None

	detail = Detail(booking_details_public_id=str(uuid.uuid4()),
					booking_id=booking_id,
					first_name=first_name,
					last_name=last_name,
					email_address=partner_details["email"].lower(),
					phone_number=phone,
					address=None,
					additional_note=additional_note,
					created_at=datetime.now(),
					updated_at=datetime.now())

	db.session.add(detail)

	try:
		note = request.json["note"]
	except Exception:
		note = None

	if note:
		new_note = Note(booking_notes_public_id=str(uuid.uuid4()),
						booking_id=booking_id,
						note=note,
						session_id=request.json["session_id"],
						created_at=datetime.now())

		db.session.add(new_note)

	try:
		destination = request.json["destination"]

	except (KeyError) as destination_error:
		destination = None

	gatepass = Gatepass(gatepass_public_id=gatepass_id,
						gatepass_date=today,
						gatepass_done_by=request.json["first_name"].title() +
						" " + request.json["last_name"].title(),
						destination=destination,
						gatepass_phone_number=phone,
						gatepass_ref_code=str(uuid.uuid4())[:10],
						booking_id=booking_id,
						booking=1,
						partner_id=request.json["partner"],
						start_date=request.json["check_in"],
						end_date=request.json["check_out"],
						gatepass_currency=request.json["currency"],
						gatepass_payment_status=1,
						created_at=datetime.now(),
						updated_at=datetime.now())

	db.session.add(gatepass)
	guest_info_data = []

	try:
		for each_gatepass_guest in request.json["guests"]:
			get_gatepass_fee = db.session.query(MandatoryPaymentPrices)\
			  .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
			  .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
			   MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
			   Mandatory.payment_person, Mandatory.payment_public_id)\
			  .filter(MandatoryPaymentPrices.deletion_marker == None)\
			  .filter(MandatoryPaymentPrices.payment_schedule == each_gatepass_guest["payment_schedule"])\
			  .filter(MandatoryPaymentPrices.payment_category == each_gatepass_guest["payment_public_id"])\
			  .first()

			try:
				gatepass = get_gatepass_fee.payment_price
			except (ValueError, TypeError) as no_value:
				gatepass = 0

			gatepass_amount = currencyHandler(
				request.json["currency"], get_gatepass_fee.payment_currency,
				gatepass)

			get_ex_rate = requests.get(
				get_buy_sell_rate.format(get_gatepass_fee.payment_currency))

			try:
				buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

			except Exception:
				buying = 1
				selling = 1

			if promo_code_data:
				if promo_code_data["conservancy_fee"]:
					if each_gatepass_guest[
							"payment_public_id"] in promo_code_data[
								"conservancy_fee_array"]:
						# if
						promo_person_discount = float(
							promo_code_data["percentage_off"])
						
						
						person_discount_reason = promo_code_data["code"]
					else:
						promo_person_discount = 0
						person_discount_reason = None
				else:
					promo_person_discount = 0
					person_discount_reason = None

			else:
				promo_person_discount = 0
				person_discount_reason = None
				
			try:
				person_discount = float(
					each_gatepass_guest["payment_person_discount"])
			except Exception:
				person_discount = 0

			if promo_discount:
				person_discount_reason = promo_code
			else:
				try:
					person_discount_reason = each_gatepass_guest["reason"]
				except Exception:
					person_discount_reason = None

			try:
				guests = int(each_gatepass_guest["payment_guests"])
			except Exception:
				guests = 0

			## Only for TPS partners
			# check_residence = "non resident"
			check_residence = [
				"cdf8982a-3252-40ec-a49b-d752420eaa9b",
				"6ba37724-04df-4a1d-b475-c2915ec52a81",
				"9ab7da60-6813-411a-9a00-f2d8a413c5bb"
			]
			if partner_type == "4abc0d64":
				if each_gatepass_guest["payment_public_id"] in check_residence:
					if person_discount:
						gatepass_discount = person_discount + promo_person_discount
					else:
						gatepass_discount = gatepass_discount + promo_person_discount

					gatepass_guest = GatepassGuest(
						gatepass_guest_public_id=str(uuid.uuid4()),
						gatepass_id=gatepass_id,
						gatepass_guest_type=each_gatepass_guest[
							"payment_public_id"],
						gatepass_guest_count=guests,
						gatepass_cost_per_pp=math.ceil(
							round(gatepass_amount, 2)),
						gatepass_payment_schedule=each_gatepass_guest[
							"payment_schedule"],
						gatepass_no_of_nights=date_diff,
						gatepass_discount_rate=gatepass_discount,
						gatepass_discount_reason=None,
						gatepass_currency=request.json["currency"],
						gatepass_guest_cost_at_time=round(gatepass),
						gatepass_guest_currency_at_time=get_gatepass_fee.
						payment_currency,
						gatepass_guest_rate_at_time=buying,
						created_at=datetime.now(),
						updated_at=datetime.now())

					db.session.add(gatepass_guest)

				else:

					gatepass_guest = GatepassGuest(
						gatepass_guest_public_id=str(uuid.uuid4()),
						gatepass_id=gatepass_id,
						gatepass_guest_type=each_gatepass_guest[
							"payment_public_id"],
						gatepass_guest_count=guests,
						gatepass_cost_per_pp=math.ceil(
							round(gatepass_amount, 2)),
						gatepass_payment_schedule=each_gatepass_guest[
							"payment_schedule"],
						gatepass_no_of_nights=date_diff,
						gatepass_discount_rate=person_discount,
						gatepass_discount_reason=None,
						gatepass_currency=request.json["currency"],
						gatepass_guest_cost_at_time=round(gatepass),
						gatepass_guest_currency_at_time=get_gatepass_fee.
						payment_currency,
						gatepass_guest_rate_at_time=buying,
						created_at=datetime.now(),
						updated_at=datetime.now())

					db.session.add(gatepass_guest)

			else:
				# try:
				# 	person_discount = float(each_gatepass_guest["payment_person_discount"])
				# except Exception:
				# 	person_discount = 0

				# ## TODO: Ask about this.
				# if person_discount:
				# 	gatepass_discount = person_discount
				# else:
				# 	gatepass_discount = gatepass_discount

				try:
					gatepass_discount = float(
						partner_details["conservation_fee_discount"])
				except (KeyError) as no_value:
					gatepass_discount = 0

				if gatepass_discount:
					gatepass_discount = gatepass_discount
				else:
					try:
						gatepass_discount = float(
							each_gatepass_guest["payment_person_discount"])
					except Exception:
						gatepass_discount = 0

				# person_discount = gatepass_discount + person_discount
				person_discount = gatepass_discount + promo_person_discount

				## TODO: What is up with discounts?
				gatepass_guest = GatepassGuest(
					gatepass_guest_public_id=str(uuid.uuid4()),
					gatepass_id=gatepass_id,
					gatepass_guest_type=each_gatepass_guest[
						"payment_public_id"],
					gatepass_guest_count=guests,
					gatepass_cost_per_pp=math.ceil(round(gatepass_amount, 2)),
					gatepass_payment_schedule=each_gatepass_guest[
						"payment_schedule"],
					gatepass_no_of_nights=date_diff,
					gatepass_discount_rate=person_discount,
					gatepass_discount_reason=person_discount_reason,
					gatepass_currency=request.json["currency"],
					gatepass_guest_cost_at_time=round(gatepass),
					gatepass_guest_currency_at_time=get_gatepass_fee.
					payment_currency,
					gatepass_guest_rate_at_time=buying,
					created_at=datetime.now(),
					updated_at=datetime.now())

				db.session.add(gatepass_guest)

			guest_info = {}

			try:
				guest_numbers = int(each_gatepass_guest["payment_guests"])
			except Exception:
				guest_numbers = 0

			if guest_numbers != 0:
				guest_info["guest_category"] = get_gatepass_fee.payment_person
				guest_info["guest_number"] = each_gatepass_guest[
					"payment_guests"]

				guest_info_data.append(guest_info)

	except KeyError as gatepass_guest_error:
		print("There was an error getting: " + str(gatepass_guest_error))

	gatepass_detail = GatepassDetail(
		gatepass_details_public_id=str(uuid.uuid4()),
		gatepass_id=gatepass_id,
		first_name=first_name,
		last_name=last_name,
		email_address=partner_details["email"].lower(),
		phone_number=phone,
		created_at=datetime.now(),
		updated_at=datetime.now())

	db.session.add(gatepass_detail)

	vehicle_sum = []
	vehicle_info_data = []

	try:
		for each_vehicle in request.json["vehicles"]:
			get_cost = db.session.query(Vehicle)\
				 .filter(Vehicle.deletion_marker == None)\
				 .filter(Vehicle.vehicle_charge_public_id == each_vehicle["vehicle_charge_public_id"])\
				 .first()

			try:
				vehicle = get_cost.vehicle_charge_category_cost
			except (ValueError, TypeError) as no_value:
				vehicle = 0

			vehicle_fee = currencyHandler(
				request.json["currency_id"],
				get_cost.vehicle_charge_cost_currency, vehicle)

			get_ex_rate = requests.get(
				get_buy_sell_rate.format(
					get_cost.vehicle_charge_cost_currency))

			try:
				buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

			except Exception:
				buying = 1
				selling = 1

			if promo_discount:
				vehicle_discount = promo_discount
			else:
				try:
					vehicle_discount = float(each_vehicle["discount"])
				except Exception:
					vehicle_discount = 0

			if promo_code:
				vehicle_discount_reason = promo_code
			else:
				try:
					vehicle_discount_reason = each_vehicle["reason"]
				except Exception:
					vehicle_discount_reason = None

			try:
				vehicles = int(each_vehicle["vehicles"])
			except Exception:
				vehicles = 0

			gatepass_vehicle = GatepassVehicle(
				gatepass_vehicle_public_id=str(uuid.uuid4()),
				gatepass_id=gatepass_id,
				gatepass_vehicle_type=each_vehicle["vehicle_charge_public_id"],
				gatepass_vehicle_count=vehicles,
				gatepass_cost_per_vehicle=round(vehicle_fee),
				gatepass_vehicle_currency=request.json["currency_id"],
				gatepass_vehicle_no_of_nights=date_diff,
				gatepass_vehicle_discount_rate=vehicle_discount,
				gatepass_vehicle_discount_reason=vehicle_discount_reason,
				gatepass_vehicle_cost_at_time=round(vehicle),
				gatepass_vehicle_currency_at_time=get_cost.
				vehicle_charge_cost_currency,
				gatepass_vehicle_rate_at_time=buying,
				created_at=datetime.now(),
				updated_at=datetime.now())

			vehicle_info = {}
			try:
				vehicle_numbers = int(each_vehicle["vehicles"])
			except Exception:
				vehicle_numbers = 0

			if vehicle_numbers != 0:
				vehicle_info[
					"vehicle_category"] = get_cost.vehicle_charge_category
				vehicle_info["vehicle_number"] = each_vehicle["vehicles"]

				vehicle_info_data.append(vehicle_info)

				vehicle_sum.append(int(each_vehicle["vehicles"]))

			db.session.add(gatepass_vehicle)

	except KeyError as vehicle_key_error:
		print("There was an error getting the following key: " +
			  str(vehicle_key_error))

	if payment_status == 4:
		invoice = Invoice(invoice_public_id=str(uuid.uuid4()),
						  booking_id=booking_id,
						  session_id=request.json["session_id"],
						  created_at=datetime.now())

		db.session.add(invoice)

	try:
		db.session.commit()
		close(db)

		# email_array = [email]
		email_array.append(email)

		email_data = {}
		# email_data["recipient"] = email
		email_data["recipient"] = email_array
		email_data["sender"] = "reservations@olpejetaconservancy.org"
		email_data["subject"] = "Booking Notification (#" + booking_ref + ")"

		email_data["today"] = datetime.now().strftime("%B %Y")
		email_data["booking_ref_code"] = booking_ref
		email_data["first_name"] = first_name
		email_data["last_name"] = last_name
		email_data["check_in_date"] = check_in_date.strftime("%A, %d %b %Y")
		email_data["check_out_date"] = check_out_date.strftime("%A, %d %b %Y")
		email_data["num_of_guests"] = sum(guest_sum)
		email_data["num_of_vehicles"] = sum(vehicle_sum)
		email_data["client"] = first_name + " " + last_name
		email_data["booking_id"] = booking_id
		email_data["facility_info"] = facility_email_data
		email_data["inventory_info"] = inventory_email_data
		email_data["guest_info"] = guest_info_data
		email_data["vehicle_info"] = vehicle_info_data

		try:
			# print("i would have sent an emiail")
			send_booking_email(email_data, partner=partner_email)
		except Exception:
			pass

		if promo_code_data:
			requests.post(use_promo_code.format(promo_code_data["public_id"]),
						  json={
							  "session_id": request.json["session_id"],
							  "number": "1",
							  "booking": booking_id
						  })
		send_booking_info_to_salesforce(booking_id)
		output = []
		output.append(
			"The booking has been made. Please proceed to make your payment.")

		return_data = {}
		return_data["booking_public_id"] = booking_id
		return_data["email"] = partner_details["email"]

		return jsonify({"message": output, "data": return_data}), 201

	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while creating the booking")
		return jsonify({"message": output, "error": str(e)}), 422


@app.route("/partners/bookings/view")
def view_all_partner_bookings():
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency, Booking.deletion_marker,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name)\
							   .filter(Booking.deletion_marker == None)\
							   .options(FromCache(db_cache))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		# try:
		# 	return_user = requests.post(get_user_from_aumra,\
		# 								json = {"users_ids": id_array})
		# except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		# 	pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						return_data["session_user"] = user["first_name"] + " " + user["last_name"]
						return_data["session_id"] = single.session_id
						
			except (IndexError) as user_error:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id
			
			except (AttributeError, UnboundLocalError) as network_related_errors:
				return_data["session_user"] = "Network Error"
				return_data["session_id"] = single.session_id
			
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/view/partner/<partner_id>")
def view_all_partner_bookings_by_partner(partner_id):
	## Handling instances where the UI hasn't been updated.
	try:
		request_args_page = request.args["page"]
	except Exception:
		request_args_page = None
	
	if not request_args_page:
		page = 0
	else:
		try:
			page = int(request.args["page"])
		except Exception:
			page = 1

	try:
		items = int(request.args["items"])
	except Exception:
		items = 20
	
	if page == 0:
		return_bookings = db.session.query(Booking)\
									.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
									.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
									.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
									.join(Partner, Booking.booking_public_id == Partner.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
												 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
												 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
												 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
												 Booking.currency, Booking.deletion_marker,\
												 BookingType.booking_type_name,\
												 BookingStatus.booking_status_name,\
												 Partner.partner_booking_ref)\
									.filter(Partner.partner_id == partner_id)\
									.options(FromCache(db_cache))\
									.order_by(Booking.booking_id.desc())\
									.all()

	else:
		return_bookings = db.session.query(Partner)\
									.join(Booking, Partner.booking_id == Booking.booking_public_id)\
									.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
									.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
												 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
												 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
												 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
												 Booking.currency, Booking.deletion_marker,\
												 BookingType.booking_type_name,\
												 BookingStatus.booking_status_name,\
												 Partner.partner_booking_ref)\
									.filter(Partner.partner_id == partner_id)\
									.options(FromCache(db_cache))\
									.order_by(Partner.partner_booking_id.desc())\
									.paginate(page, items, False)\
									.items
		
	if not return_bookings:
		output = []
		output.append("There are currently no bookings for the selected partner in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code
			return_data["partner_booking_ref"] = single.partner_booking_ref

			booking_info = {}
			bookingTotal(booking_info, single.booking_public_id)

			return_data["booking_currency"] = booking_info["booking_currency_name"]
			return_data["booking_total"] = booking_info["total_cost"]

			return_data["facility_bookings"] = booking_info["facility_bookings"]
			return_data["inventory_bookings"] = booking_info["inventory_bookings"]

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			if single.session_id:
				if single.session_id == "guest":
					return_data["session_user"] = "Public"
					return_data["session_id"] = single.session_id
				else:
					try:
						for user in return_user.json()["data"]:
							if user["public_id"] == single.session_id:
								return_data["session_user"] = user["full_name"]
								return_data["session_id"] = single.session_id
													
					except (IndexError, KeyError) as user_error:
						return_data["session_user"] = "N/A"
						return_data["session_id"] = single.session_id

					except (AttributeError, UnboundLocalError) as network_related_errors:
						return_data["session_user"] = "Network Error"
						return_data["session_id"] = single.session_id

			else:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id
			
			# return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/view/partner/test/<partner_id>")
def view_all_partner_bookings_by_partner_test(partner_id):
	try:
		request_args_page = request.args["page"]
	except Exception:
		request_args_page = None
	
	if not request_args_page:
		page = 0
	else:
		try:
			page = int(request.args["page"])
		except Exception:
			page = 1

	try:
		items = int(request.args["items"])
	except Exception:
		items = 20
	
	if page == 0:
		return_bookings = db.session.query(Booking)\
									.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
									.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
									.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
									.join(Partner, Booking.booking_public_id == Partner.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
												 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
												 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
												 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
												 Booking.currency, Booking.deletion_marker,\
												 BookingType.booking_type_name,\
												 BookingStatus.booking_status_name,\
												 Partner.partner_booking_ref)\
									.filter(Booking.deletion_marker == None)\
									.filter(Partner.partner_id == partner_id)\
									.options(FromCache(db_cache))\
									.order_by(Booking.booking_id.desc())\
									.all()

	else:
		return_bookings = db.session.query(Partner)\
									.join(Booking, Partner.booking_id == Booking.booking_public_id)\
									.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
									.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
												 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
												 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
												 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
												 Booking.currency, Booking.deletion_marker,\
												 BookingType.booking_type_name,\
												 BookingStatus.booking_status_name,\
												 Partner.partner_booking_ref)\
									.filter(Partner.partner_id == partner_id)\
									.options(FromCache(db_cache))\
									.order_by(Partner.partner_booking_id.desc())\
									.paginate(page, items, False)\
									.items
		
		# return_bookings = db.session.query(Booking)\
		# 							.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
		# 							.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
		# 							.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
		# 							.join(Partner, Booking.booking_public_id == Partner.booking_id)\
		# 							.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
		# 										 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
		# 										 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
		# 										 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
		# 										 Booking.currency, Booking.deletion_marker,\
		# 										 BookingType.booking_type_name,\
		# 										 BookingStatus.booking_status_name,\
		# 										 Partner.partner_booking_ref)\
		# 							.filter(Booking.deletion_marker == None)\
		# 							.filter(Partner.partner_id == partner_id)\
		# 							.options(FromCache(db_cache))\
		# 							.order_by(Booking.booking_id.desc())\
		# 							.paginate(page, items, False)\
		# 							.items

	# return_bookings = db.session.query(Booking)\
	# 							.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
	# 							.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
	# 							.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
	# 							.join(Partner, Booking.booking_public_id == Partner.booking_id)\
	# 							.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
	# 										 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
	# 										 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
	# 										 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
	# 										 Booking.currency, Booking.deletion_marker,\
	# 										 BookingType.booking_type_name,\
	# 										 BookingStatus.booking_status_name,\
	# 										 Partner.partner_booking_ref)\
	# 							.filter(Booking.deletion_marker == None)\
	# 							.filter(Partner.partner_id == partner_id)\
	# 							.options(FromCache(db_cache))\
	# 							.order_by(Booking.booking_id.desc())\
	# 							.all()
	
	if not return_bookings:
		output = []
		output.append("There are currently no bookings for the selected partner in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code
			return_data["partner_booking_ref"] = single.partner_booking_ref

			# booking_info = {}
			# bookingTotal(booking_info, single.booking_public_id)

			# return_data["booking_currency"] = booking_info["booking_currency_name"]
			# return_data["booking_total"] = booking_info["total_cost"]

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			# try:
			# 	for user in return_user.json()["data"]:
			# 		if user["public_id"] == single.session_id:
			# 			return_data["session_user"] = user["first_name"] + " " + user["last_name"]
			# 			return_data["session_id"] = single.session_id
						
			# except (IndexError) as user_error:
			# 	return_data["session_user"] = "N/A"
			# 	return_data["session_id"] = single.session_id
			
			# except (AttributeError, UnboundLocalError) as network_related_errors:
			# 	return_data["session_user"] = "Network Error"
			# 	return_data["session_id"] = single.session_id
			
			return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/view/partner/<partner_id>/check_in/today")
def view_all_partner_bookings_by_partner_check_in_today(partner_id):
	today = datetime.now().strftime("%Y-%m-%d")
	
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency, Booking.deletion_marker,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name,\
											Partner.partner_booking_ref)\
							   .filter(Booking.deletion_marker == None)\
							   .filter(Booking.booking_check_in_date == today)\
							   .filter(Partner.partner_id == partner_id)\
							   .options(FromCache(db_cache))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings to be checked in today.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			asyncRequests = AsyncRequests()
			currency = asyncRequests.launchGetRequest(url = get_currency, request_id = single.currency)
			
			try:
				return_data["currency"] = currency["data"][0]["currency_name"]
			except Exception:
				return_data["currency"] = ""
			return_data["currency_id"] = single.currency
			
			bookingTotal(return_data, single.booking_public_id)

			return_data["partner_booking_ref"] = single.partner_booking_ref

			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			# try:
			# 	for user in return_user.json()["data"]:
			# 		if user["public_id"] == single.session_id:
			# 			return_data["session_user"] = user["first_name"] + " " + user["last_name"]
			# 			return_data["session_id"] = single.session_id
						
			# except (IndexError) as user_error:
			# 	return_data["session_user"] = "N/A"
			# 	return_data["session_id"] = single.session_id
			
			# except (AttributeError, UnboundLocalError) as network_related_errors:
			# 	return_data["session_user"] = "Network Error"
			# 	return_data["session_id"] = single.session_id
			
			return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/view/partner/<partner_id>/check_out/today")
def view_all_partner_bookings_by_partner_check_out_today(partner_id):
	today = datetime.now().strftime("%Y-%m-%d")
	
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency, Booking.deletion_marker,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name,\
											Partner.partner_booking_ref)\
							   .filter(Booking.deletion_marker == None)\
							   .filter(Booking.booking_check_out_date == today)\
							   .filter(Partner.partner_id == partner_id)\
							   .options(FromCache(db_cache))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings to be checked out today.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			asyncRequests = AsyncRequests()
			currency = asyncRequests.launchGetRequest(url = get_currency, request_id = single.currency)
			
			try:
				return_data["currency"] = currency["data"][0]["currency_name"]
			except Exception:
				return_data["currency"] = ""
			return_data["currency_id"] = single.currency

			bookingTotal(return_data, single.booking_public_id)

			return_data["partner_booking_ref"] = single.partner_booking_ref
			
			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			# try:
			# 	for user in return_user.json()["data"]:
			# 		if user["public_id"] == single.session_id:
			# 			return_data["session_user"] = user["first_name"] + " " + user["last_name"]
			# 			return_data["session_id"] = single.session_id
						
			# except (IndexError) as user_error:
			# 	return_data["session_user"] = "N/A"
			# 	return_data["session_id"] = single.session_id
			
			# except (AttributeError, UnboundLocalError) as network_related_errors:
			# 	return_data["session_user"] = "Network Error"
			# 	return_data["session_id"] = single.session_id
			
			return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/view/<booking_id>")
def view_single_partner_booking(booking_id):
	return_bookings = db.session.query(Booking)\
								.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								.join(Detail, Booking.booking_public_id == Detail.booking_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
											 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status,\
											 Booking.currency, Booking.deletion_marker,\
											 BookingType.booking_type_name,\
											 BookingStatus.booking_status_name,\
											 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
											 Detail.address, Detail.additional_note)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.booking_public_id == booking_id)\
								.options(FromCache(db_cache))\
								.all()

	if not return_bookings:
		output = []
		output.append("That booking does not appear to exist in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []
		inventory_total_cost_array = []
		total_cost_array = []

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["actual_booking_check_in_date"] = single.actual_booking_check_in_date
			return_data["actual_booking_check_out_date"] = single.actual_booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code
			return_data["currency"] = single.currency

			detail_data = {}
			detail_data["first_name"] = single.first_name
			detail_data["last_name"] = single.last_name
			detail_data["email_address"] = single.email_address
			detail_data["phone_number"] = single.phone_number
			detail_data["address"] = single.address
			detail_data["additional_note"] = single.additional_note

			return_data["booking_details"] = detail_data

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			facility_array = []
			facility_total_cost_array = []
			
			facility_bookings = db.session.query(Facility)\
										  .filter(Facility.deletion_marker == None)\
										  .filter(Facility.booking_id == single.booking_public_id)\
										  .all()

			for single_facility in facility_bookings:
				facility_data = {}
				return_facility = requests.get(get_facility_details.format(single_facility.facility_id))

				try:
					facility_data["facility_name"] = return_facility.json()["data"][0]["name"]
				except (KeyError, ValueError, TypeError) as no_data:
					facility_data["facility_name"] = "The record cannot be found."
				except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
					facility_data["facility_name"] = "The information cannot be retrieved at this time."

				try:
					facility_data["facility_type"] = return_facility.json()["data"][0]["facility_type_id"]
				except (KeyError, ValueError, TypeError) as no_data:
					facility_data["facility_type"] = "The record cannot be found."
				except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
					facility_data["facility_type"] = "The information cannot be retrieved at this time."

				fac_adult_discount = float(single_facility.facility_cost_per_adult) * (100 - float(single_facility.facility_discount_rate))/100
				fac_adult_spec_discount = fac_adult_discount * (100 - float(single_facility.facility_commission_rate))/100

				fac_child_discount = float(single_facility.facility_cost_per_child) * (100 - float(single_facility.facility_discount_rate))/100
				fac_child_spec_discount = fac_child_discount * (100 - float(single_facility.facility_commission_rate))/100
				
				pre_facility_adult_cost = float(single_facility.facility_booking_adults) * float(single_facility.facility_cost_per_adult) * \
									  float(single_facility.facility_no_of_nights)
				pre_facility_child_cost = float(single_facility.facility_booking_children) * float(single_facility.facility_cost_per_child) * \
									  float(single_facility.facility_no_of_nights)

				facility_adult_cost = float(single_facility.facility_booking_adults) * float(fac_adult_spec_discount) * \
									  float(single_facility.facility_no_of_nights)
				facility_child_cost = float(single_facility.facility_booking_children) * float(fac_child_spec_discount) * \
									  float(single_facility.facility_no_of_nights)

				# if single_facility.facility_no_of_nights%7 != 0:
				# 	weeks_count = round(single_facility.facility_no_of_nights/7) + 1
					
				# 	facility_fixed_cost = float(single_facility.facility_fixed_cost) * weeks_count

				# elif single_facility.facility_no_of_nights%7 == 0:
				# 	weeks_count = round(single_facility.facility_no_of_nights/7)
					
				# 	facility_fixed_cost = float(single_facility.facility_fixed_cost) * weeks_count

				# elif single_facility.facility_no_of_nights/7 <= 1:
				# 	facility_fixed_cost = float(single_facility.facility_fixed_cost)

				if single_facility.facility_cost_per_adult == 0 and single_facility.facility_cost_per_child == 0:
					fixed_cost_discount = float(single_facility.facility_fixed_cost) * (100 - float(single_facility.facility_discount_rate))/100
					fixed_cost_spec_discount= fixed_cost_discount * (100 - float(single_facility.facility_commission_rate))/100

					if single_facility.facility_booking_children > 0 and single_facility.facility_booking_adults > 0:
						facility_fixed_cost = fixed_cost_spec_discount * (single_facility.facility_booking_children\
											  + single_facility.facility_booking_adults) * single_facility.facility_no_of_nights
					elif single_facility.facility_booking_children == 0:
						facility_fixed_cost = fixed_cost_spec_discount * single_facility.facility_booking_adults  * single_facility.facility_no_of_nights
					elif single_facility.facility_booking_adults == 0:
						facility_fixed_cost = fixed_cost_spec_discount * single_facility.facility_booking_children * single_facility.facility_no_of_nights
					
				
				else:
					if single_facility.facility_no_of_nights/7 <= 1:
						facility_fixed_cost = float(single_facility.facility_fixed_cost)
					
					elif single_facility.facility_no_of_nights/7 > 1:
						weeks_count = round(single_facility.facility_no_of_nights/7) + 1
						
						facility_fixed_cost = float(single_facility.facility_fixed_cost) * weeks_count
				
				facility_data["facility_id"] = single_facility.facility_id
				facility_data["facility_booking_public_id"] = single_facility.facility_booking_public_id
				facility_data["facility_booking_check_in_date"] = single_facility.facility_booking_check_in_date
				facility_data["facility_booking_check_out_date"] = single_facility.facility_booking_check_out_date
				facility_data["facility_booking_guests"] = single_facility.facility_booking_adults + single_facility.facility_booking_children
				facility_data["facility_booking_adults"] = single_facility.facility_booking_adults
				facility_data["facility_booking_children"] = single_facility.facility_booking_children
				facility_data["facility_booking_currency"] = single_facility.facility_cost_currency
				facility_data["facility_no_of_nights"] = single_facility.facility_no_of_nights
				facility_data["facility_booking_adult_cost"] = round(facility_adult_cost, 2)
				facility_data["facility_booking_child_cost"] = round(facility_child_cost, 2)
				facility_data["facility_booking_fixed_cost"] = round(facility_fixed_cost, 2)
				facility_data["facility_booking_total_cost"] = round((facility_adult_cost + facility_child_cost + facility_fixed_cost), 2)
				facility_data["facility_booking_total_cost_before_discounts"] = round((pre_facility_adult_cost + pre_facility_child_cost + facility_fixed_cost), 2)
				facility_data["facility_adult_cost_before_discounts"] = round(float(single_facility.facility_cost_per_adult), 2)
				facility_data["facility_adult_cost_after_discounts"] = round(fac_adult_spec_discount, 2)
				facility_data["facility_child_cost_before_discounts"] = round(float(single_facility.facility_cost_per_child), 2)
				facility_data["facility_child_cost_after_discounts"] = round(fac_child_spec_discount, 2)

				facility_total_cost_array.append(facility_adult_cost + facility_child_cost + facility_fixed_cost)
				total_cost_array.append(facility_adult_cost + facility_child_cost + facility_fixed_cost)

				facility_array.append(facility_data)

			return_data["facility_bookings"] = facility_array
			return_data["facility_total_cost"] = round(sum(facility_total_cost_array), 2)
			
			inventory_array = []
			inventory_sub_total = []
			inventory_id_array = []
			
			inventory_bookings = db.session.query(Inventory)\
										   .filter(Inventory.deletion_marker == None)\
										   .filter(Inventory.booking_id == single.booking_public_id)\
										   .all()

			for single_inventory in inventory_bookings:
				inventory_data = {}
				# return_inventory = requests.get(get_inventory_details.format(single_inventory.inventory_id))
				# return_inventory_pricing = requests.get(get_inventory_current_pricing.format(single_inventory.inventory_id))

				try:
					return_inventory = requests.get(get_inventory_details.format(single_inventory.inventory_id))
					return_inventory_pricing = requests.get(get_inventory_current_pricing.format(single_inventory.inventory_id))
					
					inventory_data["inventory_name"] = return_inventory.json()["data"][0]["name"]
				except (KeyError, ValueError, TypeError) as no_data:
					inventory_data["inventory_name"] = "The record cannot be found."
				except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
					inventory_data["inventory_name"] = "Network error."

				adult_discount = float(single_inventory.inventory_cost_per_adult) * (100 - float(single_inventory.inventory_discount_rate))/100
				adult_spec_discount = adult_discount * (100 - float(single_inventory.inventory_commission_rate))/100

				child_discount = float(single_inventory.inventory_cost_per_child) * (100 - float(single_inventory.inventory_discount_rate))/100
				child_spec_discount = child_discount * (100 - float(single_inventory.inventory_commission_rate))/100
				
				pre_adult_cost = float(single_inventory.inventory_booking_adults) * float(single_inventory.inventory_cost_per_adult)
				pre_child_cost = float(single_inventory.inventory_booking_children) * float(single_inventory.inventory_cost_per_child)

				adult_cost = float(single_inventory.inventory_booking_adults) * adult_spec_discount
				child_cost = float(single_inventory.inventory_booking_children) * child_spec_discount
				
				inventory_data["inventory_id"] = single_inventory.inventory_id
				inventory_data["inventory_booking_public_id"] = single_inventory.inventory_booking_public_id
				inventory_data["inventory_booking_date"] = single_inventory.inventory_booking_date
				inventory_data["inventory_booking_guests"] = single_inventory.inventory_booking_adults + single_inventory.inventory_booking_children
				inventory_data["inventory_booking_adults"] = single_inventory.inventory_booking_adults
				inventory_data["inventory_booking_children"] = single_inventory.inventory_booking_children
				inventory_data["inventory_booking_adult_cost"] = round(adult_cost, 2)
				inventory_data["inventory_booking_child_cost"] = round(child_cost, 2)
				inventory_data["inventory_booking_total_cost"] = round((adult_cost + child_cost), 2)
				inventory_data["inventory_booking_total_cost_before_discounts"] = round((pre_adult_cost + pre_child_cost), 2)
				inventory_data["inventory_booking_currency"] = single_inventory.inventory_cost_currency
				inventory_data["inventory_adult_cost_before_discounts"] = round(float(single_inventory.inventory_cost_per_adult), 2)
				inventory_data["inventory_adult_cost_after_discounts"] = round(adult_spec_discount, 2)
				inventory_data["inventory_child_cost_before_discounts"] = round(float(single_inventory.inventory_cost_per_child), 2)
				inventory_data["inventory_child_cost_after_discounts"] = round(child_spec_discount, 2)
				
				inventory_total_cost_array.append(adult_cost + child_cost)
				total_cost_array.append(adult_cost + child_cost)
				
				inventory_array.append(inventory_data)

			return_data["inventory_bookings"] = inventory_array
			return_data["inventory_total_cost"] = round(sum(inventory_total_cost_array), 2)

			gatepass_total_cost_array = []

			gatepass_details = db.session.query(GatepassGuest)\
										 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
										 .add_columns(GatepassGuest.gatepass_guest_type, GatepassGuest.gatepass_guest_count,\
										 			  GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													  GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_currency)\
										 .filter(GatepassGuest.deletion_marker == None)\
										 .filter(Gatepass.deletion_marker == None)\
										 .filter(Gatepass.booking_id == single.booking_public_id)\
										 .all()

			for each_pass in gatepass_details:
				try:
					gatepass_sub_cost = round(float(each_pass.gatepass_guest_count) * ((100 - float(each_pass.gatepass_discount_rate))/100) * \
											  float(each_pass.gatepass_cost_per_pp) * float(each_pass.gatepass_no_of_nights), 2)
					
					gatepass_total_cost_array.append(gatepass_sub_cost)
					total_cost_array.append(gatepass_sub_cost)
				except (NameError, ValueError, TypeError) as no_value:
					pass

			return_data["gate_entry_total_cost"] = round(sum(gatepass_total_cost_array), 2)

			vehicle_total_cost_array = []
			vehicle_count_array = []
			
			vehicle_charges = db.session.query(GatepassVehicle)\
										.join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
										.add_columns(GatepassVehicle.gatepass_vehicle_count, GatepassVehicle.gatepass_cost_per_vehicle)\
										.filter(GatepassVehicle.deletion_marker == None)\
										.filter(Gatepass.deletion_marker == None)\
										.filter(Gatepass.booking_id == single.booking_public_id)\
										.all()

			for each_vehicle in vehicle_charges:
				try:
					vehicle_sub_cost = round(float(each_vehicle.gatepass_vehicle_count) * float(each_vehicle.gatepass_cost_per_vehicle))

					vehicle_count_array.append(each_vehicle.gatepass_vehicle_count)
					vehicle_total_cost_array.append(vehicle_sub_cost)
					total_cost_array.append(vehicle_sub_cost)
				except (NameError, ValueError, TypeError) as no_value:
					pass

			return_data["vehicle_count"] = sum(vehicle_count_array)
			return_data["vehicle_total_cost"] = round(sum(vehicle_total_cost_array), 2)
			
			return_data["total_cost"] = round(sum(total_cost_array), 2)

			if single.payment_status:
				return_data["booking_payment_status"] = "Paid"
			elif not single.payment_status:
				return_data["booking_payment_status"] = "Not Paid"

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/view/pending")
def view_all_pending_partner_bookings():
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency, Booking.deletion_marker,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name)\
							   .filter(Booking.deletion_marker == None)\
							   .filter(Booking.status == get_booking_status_id("Unconfirmed"))\
							   .options(FromCache(db_cache))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no pending partner bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			# try:
			# 	for user in return_user.json()["data"]:
			# 		if user["public_id"] == single.session_id:
			# 			return_data["session_user"] = user["first_name"] + " " + user["last_name"]
			# 			return_data["session_id"] = single.session_id
						
			# except (IndexError) as user_error:
			# 	return_data["session_user"] = "N/A"
			# 	return_data["session_id"] = single.session_id
			
			# except (AttributeError, UnboundLocalError) as network_related_errors:
			# 	return_data["session_user"] = "Network Error"
			# 	return_data["session_id"] = single.session_id
			
			return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200

	
@app.route("/partners/bookings/view/approved")
def view_all_approved_partner_bookings():
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Partner, Booking.booking_public_id == Partner.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency, Booking.deletion_marker,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name)\
							   .filter(Booking.deletion_marker == None)\
							   .filter(Booking.status == get_booking_status_id("Confirmed"))\
							   .options(FromCache(db_cache))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		# try:
		# 	return_user = requests.post(get_user_from_aumra,\
		# 								json = {"users_ids": id_array})
		# except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		# 	pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						return_data["session_user"] = user["first_name"] + " " + user["last_name"]
						return_data["session_id"] = single.session_id
						
			except (IndexError) as user_error:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id
			
			except (AttributeError, UnboundLocalError) as network_related_errors:
				return_data["session_user"] = "Network Error"
				return_data["session_id"] = single.session_id
			
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/partners/bookings/modify", methods = ["PATCH"])
def update_partner_booking():
	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == request.json["booking_id"])\
							   .first()

	if not return_booking:
		message = []
		message.append("The selected booking is not in the system.")
		return jsonify({"message": message}), 412

	if return_booking.currency != request.json["currency_id"]:
		message = []
		message.append("The updated booking currency does not match the original booking currency.")
		return jsonify({"message": message}), 422
	
	today = datetime.now().strftime("%Y-%m-%d")
	check_in_date = GenerateDateFromString.generateDate(request.json["check_in"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out"])
	
	temp_date_diff = DateOperations.returnDateDifferenceInDays(check_out_date, check_in_date)

	if temp_date_diff == 0:
		date_diff = 1
	elif temp_date_diff > 0:
		date_diff = temp_date_diff

	exchange_rate_data = requests.get(get_latest_exchange_rate)
	
	# From return_booking query
	return_booking.booking_check_in_date = request.json["check_in"],
	return_booking.booking_check_out_date = request.json["check_out"],
	return_booking.status = get_booking_status_id("Updated")
	return_booking.updated_at = datetime.now()
	
	booking_id = return_booking.booking_public_id
	booking_ref = return_booking.booking_ref_code
	booking_done_by = return_booking.booking_done_by
	
	# From JSON sent
	first_name = request.json["first_name"]
	last_name = request.json["last_name"]
	email = request.json["email"].lower()
	phone = request.json["phone"]

	recipient_email = email

	try:
		send_email = request.json["send_mail"]
	except Exception:
		send_email = None

	phone_validation = validatePhoneNumber(phone)

	if phone_validation[0]:
		phone = phone_validation[1]
	
	else:
		message = []
		message.append(phone_validation[1])
		return jsonify({"message": message}), 422

	valid_email = validateEmail(email)

	if not valid_email:
		message = []
		message.append("The email address provided is invalid.")
		return jsonify({"message": message}), 422

	try:
		promo_code_json = request.json["promo_code"]

	except Exception:
		promo_code_json = None

	if promo_code_json:
		try:
			check_promo_validity = requests.post(promo_code_search, json = {"promo_code": promo_code_json})

			if check_promo_validity.status_code != 200:
				return jsonify({"message": check_promo_validity.json()["message"]}), 422
				
			elif check_promo_validity.status_code == 200:
				promo_code = promo_code_json
				promo_discount = int(check_promo_validity.json()["data"][0]["percentage_off"])
				promo_code_public_id = check_promo_validity.json()["data"][0]["public_id"]
		
		except Exception:
			message = []
			message.append("There was an issue getting the promo code details. Please try again or leave the promo code field blank.")
			return jsonify({"message": message}), 422

	else:
		promo_code = None
		promo_discount = None
		promo_code_public_id = None
	
	get_gatepass = db.session.query(Gatepass)\
							 .filter(Gatepass.status != get_booking_status_id("Updated"))\
							 .filter(Gatepass.booking_id == booking_id)\
							 .first()

	# From get_gatepass query
	old_gatepass_id = get_gatepass.gatepass_public_id
	
	get_gatepass_vehicles = db.session.query(GatepassVehicle)\
									  .filter(GatepassVehicle.gatepass_id == old_gatepass_id)\
									  .filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
									  .all()

	for single_vehicle in get_gatepass_vehicles:
		single_vehicle.status = get_booking_status_id("Updated")
		single_vehicle.updated_at = datetime.now()

	get_gatepass_guests = db.session.query(GatepassGuest)\
									.filter(GatepassGuest.gatepass_id == old_gatepass_id)\
									.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									.all()

	for single_guest in get_gatepass_guests:
		single_guest.status = get_booking_status_id("Updated")
		single_guest.updated_at = datetime.now()

	get_gatepass_details = db.session.query(GatepassDetail)\
									 .filter(GatepassDetail.gatepass_id == old_gatepass_id)\
									 .filter(GatepassDetail.status != get_booking_status_id("Updated"))\
									 .all()

	for single_gate_detail in get_gatepass_details:
		single_gate_detail.status = get_booking_status_id("Updated")
		single_gate_detail.updated_at = datetime.now()

	get_booking_details = db.session.query(Detail)\
									.filter(Detail.booking_id == booking_id)\
									.filter(Detail.status != get_booking_status_id("Updated"))\
									.all()

	for single_book_detail in get_booking_details:
		single_book_detail.status = get_booking_status_id("Updated")
		single_book_detail.updated_at = datetime.now()

	get_booking_guests = db.session.query(BookingGuest)\
								   .filter(BookingGuest.booking_id == booking_id)\
								   .filter(BookingGuest.status != get_booking_status_id("Updated"))\
								   .all()

	for single_book_guest in get_booking_guests:
		single_book_guest.status = get_booking_status_id("Updated")
		single_book_guest.updated_at = datetime.now()

	old_facility_list_ui = []
	old_facility_list_db = []

	try:
		for facility_old_single in request.json["old_facilities"]:
			old_facility_list_ui.append(facility_old_single["facility_booking_public_id"])
		
		get_booking_accommodation = db.session.query(Facility)\
											.filter(Facility.booking_id == booking_id)\
											.filter(Facility.deletion_marker == None)\
											.filter(Facility.status != get_booking_status_id("Updated"))\
											.filter(Facility.status != get_booking_status_id("Cancelled"))\
											.all()

		for facility_old_db_single in get_booking_accommodation:
			old_facility_list_db.append(facility_old_db_single.facility_booking_public_id)

		for single_old_facility in old_facility_list_db:
			if single_old_facility in old_facility_list_ui:
				pass
			else:
				get_accommodation = db.session.query(Facility)\
											.filter(Facility.booking_id == booking_id)\
											.first()

				get_accommodation.status = get_booking_status_id("Updated")
				get_accommodation.updated_at = datetime.now()
	
	except Exception:
		pass
	
	old_inventory_list_ui = []
	old_inventory_list_db = []

	for inventory_old_single in request.json["old_inventory"]:
		old_inventory_list_ui.append(inventory_old_single["inventory_booking_public_id"])
	
	get_booking_activity = db.session.query(Inventory)\
									 .filter(Inventory.booking_id == booking_id)\
									 .filter(Inventory.deletion_marker == None)\
									 .filter(Inventory.status != get_booking_status_id("Updated"))\
									 .filter(Inventory.status != get_booking_status_id("Cancelled"))\
									 .all()

	for inventory_old_db_single in get_booking_activity:
		old_inventory_list_db.append(inventory_old_db_single.inventory_booking_public_id)

	for single_old_inventory in old_inventory_list_db:
		if single_old_inventory in old_inventory_list_ui:
			pass
		else:
			get_activity = db.session.query(Inventory)\
									 .filter(Inventory.inventory_booking_public_id == single_old_inventory)\
									 .first()

			get_activity.status = get_booking_status_id("Updated")
			get_activity.updated_at = datetime.now()

	# From get_gatepass query
	get_gatepass.status = get_booking_status_id("Updated")
	get_gatepass.updated_at = datetime.now()

	for gatepass_guest in request.json["guests"]:
		get_guest_type = db.session.query(Mandatory)\
									.filter(Mandatory.deletion_marker == None)\
									.filter(Mandatory.payment_public_id == gatepass_guest["payment_public_id"])\
									.first()
		
		try:
			guest_count = int(gatepass_guest["payment_guests"])
		except Exception:
			guest_count = 0
		
		guest = BookingGuest(
			booking_guest_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			guest_type = get_booking_guest_type_id(get_guest_type.payment_person.lower()),
			guest_count = guest_count,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(guest)
	
	detail = Detail(
		booking_details_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		first_name = first_name,
		last_name = last_name,
		email_address = email,
		phone_number = phone,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(detail)

	try:
		note = request.json["note"]
	except Exception:
		note = None

	if note:
		new_note = Note(
			booking_notes_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			note = note,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(new_note)

	try:
		destination = request.json["destination"]
	except (KeyError) as destination_error:
		destination = None
	
	# New gatepass ID
	gatepass_id = str(uuid.uuid4())

	gatepass = Gatepass(
		gatepass_public_id = gatepass_id,
		gatepass_date = today,
		gatepass_done_by = first_name + " " + last_name,
		destination = destination,
		gatepass_phone_number = phone,
		gatepass_ref_code = str(uuid.uuid4())[:10],
		booking_id = booking_id,
		booking = 1,
		start_date = request.json["check_in"],
		end_date = request.json["check_out"],
		gatepass_currency = request.json["currency_id"],
		gatepass_payment_status = 1,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(gatepass)
	
	for each_gatepass_guest in request.json["guests"]:
		# get_gatepass_fee = db.session.query(MandatoryPaymentPrices)\
		# 							 .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
		# 							 .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
		# 										  MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
		# 										  Mandatory.payment_person)\
		# 							 .filter(MandatoryPaymentPrices.deletion_marker == None)\
		# 							 .filter(MandatoryPaymentPrices.payment_schedule == each_gatepass_guest["gatepass_payment_schedule"])\
		# 							 .filter(MandatoryPaymentPrices.payment_category == each_gatepass_guest["payment_public_id"])\
		# 							 .first()

		get_gatepass_fee = db.session.query(MandatoryPaymentPrices)\
									 .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
									 .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
												  MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
												  Mandatory.payment_person)\
									 .filter(MandatoryPaymentPrices.payment_schedule == each_gatepass_guest["gatepass_payment_schedule"])\
									 .filter(MandatoryPaymentPrices.payment_category == each_gatepass_guest["payment_public_id"])\
									 .first()

		try:
			gatepass = get_gatepass_fee.payment_price
		except (ValueError, TypeError) as no_value:
			gatepass = 0
		
		gatepass_amount = currencyHandler(request.json["currency_id"], get_gatepass_fee.payment_currency, gatepass)

		get_ex_rate = requests.get(get_buy_sell_rate.format(get_gatepass_fee.payment_currency))

		try:
			buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
			selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

		except Exception:
			buying = 1
			selling = 1
		
		if promo_discount:
			person_discount = promo_discount
		else:
			try:
				person_discount = float(each_gatepass_guest["payment_person_discount"])
			except Exception:
				person_discount = 0

		if promo_discount:
			person_discount_reason = promo_code
		else:
			try:
				person_discount_reason = each_gatepass_guest["reason"]
			except Exception:
				person_discount_reason = None
		
		try:
			guests = int(each_gatepass_guest["payment_guests"])
		except Exception:
			guests = 0
		
		gatepass_guest = GatepassGuest(
			gatepass_guest_public_id = str(uuid.uuid4()),
			gatepass_id = gatepass_id,
			gatepass_guest_type = each_gatepass_guest["payment_public_id"],
			gatepass_guest_count = guests,
			gatepass_discount_rate = person_discount,
			gatepass_discount_reason = each_gatepass_guest["payment_person_discount_reason"],
			gatepass_cost_per_pp = math.ceil(round(gatepass_amount, 2)),
			gatepass_payment_schedule = each_gatepass_guest["gatepass_payment_schedule"],
			gatepass_no_of_nights = date_diff,
			gatepass_currency = request.json["currency_id"],
			gatepass_guest_cost_at_time = round(gatepass),
			gatepass_guest_currency_at_time = get_gatepass_fee.payment_currency,
			gatepass_guest_rate_at_time = buying,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(gatepass_guest)

	gatepass_detail = GatepassDetail(
		gatepass_details_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		first_name = first_name,
		last_name = last_name,
		email_address = email,
		phone_number = phone,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)
	
	db.session.add(gatepass_detail)

	for each_vehicle in request.json["vehicles"]:
		get_cost = db.session.query(Vehicle)\
							 .filter(Vehicle.deletion_marker == None)\
							 .filter(Vehicle.vehicle_charge_public_id == each_vehicle["vehicle_charge_public_id"])\
							 .first()

		try:
			vehicle = get_cost.vehicle_charge_category_cost
		except (ValueError, TypeError) as no_value:
			vehicle = 0
		
		vehicle_fee = currencyHandler(request.json["currency_id"], get_cost.vehicle_charge_cost_currency, vehicle)

		get_ex_rate = requests.get(get_buy_sell_rate.format(get_cost.vehicle_charge_cost_currency))

		try:
			buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
			selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

		except Exception:
			buying = 1
			selling = 1

		if promo_discount:
			vehicle_discount = promo_discount
		else:
			try:
				vehicle_discount = float(each_vehicle["discount"])
			except Exception:
				vehicle_discount = 0

		if promo_code:
			vehicle_discount_reason = promo_code
		else:
			try:
				vehicle_discount_reason = each_vehicle["discount_reason"]
			except Exception:
				vehicle_discount_reason = None
		
		try:
			vehicles = int(each_vehicle["vehicles"])
		except Exception:
			vehicles = 0
		
		gatepass_vehicle = GatepassVehicle(
			gatepass_vehicle_public_id = str(uuid.uuid4()),
			gatepass_id = gatepass_id,
			gatepass_vehicle_type = each_vehicle["vehicle_charge_public_id"],
			gatepass_vehicle_count = vehicles,
			gatepass_cost_per_vehicle = round(vehicle_fee),
			gatepass_vehicle_currency = request.json["currency_id"],
			gatepass_vehicle_no_of_nights = date_diff,
			gatepass_vehicle_discount_rate = vehicle_discount,
			gatepass_vehicle_discount_reason = vehicle_discount_reason,
			gatepass_vehicle_cost_at_time = round(vehicle),
			gatepass_vehicle_currency_at_time = get_cost.vehicle_charge_cost_currency,
			gatepass_vehicle_rate_at_time = buying,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(gatepass_vehicle)
	
	booking_info = {}
	booking_info["booking_id"] = booking_id
	booking_info["booking_ref"] = booking_ref
	booking_info["first_name"] = first_name
	booking_info["last_name"] = last_name
	
	## Facility booking handler
	facility_email_data = []
	try:
		facilityBooking(request.json, booking_info, facility_email_data, currency_id = True)
	except Exception as e:
		return jsonify({"message": str(e)}), 422

	## Activity booking handler
	inventory_email_data = []

	try:
		inventoryBooking(request.json, booking_info, inventory_email_data, currency_id = True)
	except Exception as e:
		return jsonify({"message": str(e)}), 422

	get_booking_exchange_rate = requests.get(get_buy_sell_rate.format(request.json["currency_id"]))
	booking_buying_rate = get_booking_exchange_rate.json()["data"][0]["currency_buy_amount"]
	booking_selling_rate = get_booking_exchange_rate.json()["data"][0]["currency_sell_amount"]
	
	return_booking.booking_done_by = first_name + " " + last_name
	return_booking.currency = request.json["currency_id"]
	return_booking.currency_buying_rate_at_time = booking_buying_rate
	return_booking.currency_selling_rate_at_time = booking_selling_rate
	return_booking.booking_check_in_date = request.json["check_in"]
	return_booking.booking_check_out_date = request.json["check_out"]
	return_booking.status = get_booking_status_id("Updated")
	return_booking.updated_at = datetime.now()
	
	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		booking_activity_description = "Booking updated",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		if send_email:
			email_data = {}
			email_data["subject"] = "Booking ID #" + booking_ref + " Update"
			email_data["recipient"] = [recipient_email]
			email_data["sender"] = "reservations@olpejetaconservancy.org"
			email_data["booking_ref_code"] = booking_ref
			email_data["done_by"] = booking_done_by
			email_data["booking_id"] = booking_id
			
			send_booking_update_email(email_data)

		message = []
		message.append("Successfully updated the booking.")
		return jsonify({"message": message}), 200

	except Exception:
		db.session.rollback()
		close(db)

		message = []
		message.append("There was an error updating the booking. Please try again later.")
		return jsonify({"message": message}), 422


#############
#### Ref ####
#############
def get_booking_status_id(status_name):
	status_id = db.session.query(BookingStatus)\
						  .filter(BookingStatus.deletion_marker == None)\
						  .filter(BookingStatus.booking_status_name == status_name)\
						  .all()

	for single in status_id:
		booking_status_id = single.booking_status_public_id

		return booking_status_id


def get_booking_guest_type_id(guest_type_name):
	type_id = db.session.query(GuestType)\
						.filter(GuestType.deletion_marker == None)\
						.filter(GuestType.booking_guest_type_name == guest_type_name)\
						.all()

	for single in type_id:
		booking_guest_type_id = single.booking_guest_type_public_id

		return booking_guest_type_id
