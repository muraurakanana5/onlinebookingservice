from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta
from flask_mail import Mail, Message
from sendgrid.helpers.mail import *

import pymysql, os, math, requests, uuid, sendgrid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.bookings_urls import get_booking_status_id, currencyPostProcessor, convertAmount
from database.booking_activity_log import BookingActivity
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_payments import BookingPayment
from database.donation import Donation
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.salesforce import SalesforceData
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.payment_methods import PaymentMethod
from database.invoice import Invoice
from database.booking_activity_log import BookingActivity
from database.partner import Partner
from functions.booking_snippets import bookingTotal
from functions.validation import fieldValidation
from variables import *


def close(self):
	self.session.close()


def currencyHandler(currencyTo, currencyFrom, amount):
	if currencyTo == currencyFrom:
		return amount
	elif currencyTo != currencyFrom:
		if currencyFrom == "162fface-f5f1-41de-913b-d2bb784dda3a":
			get_rate = requests.get(get_buy_sell_rate.format(currencyTo))

			try:
				buying = get_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_rate.json()["data"][0]["currency_sell_amount"]

				value = float(amount) / float(buying)
				return float(value)
			except Exception:
				value = 0
				return float(value)

		elif currencyTo == "162fface-f5f1-41de-913b-d2bb784dda3a":
			get_rate = requests.get(get_buy_sell_rate.format(currencyFrom))

			try:
				buying = get_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_rate.json()["data"][0]["currency_sell_amount"]

				value = float(amount) * float(selling)
				return float(value)
			except Exception:
				value = 0
				return float(value)
		
		else:
			get_from_rate = requests.get(get_buy_sell_rate.format(currencyFrom))
			get_to_rate = requests.get(get_buy_sell_rate.format(currencyTo))

			try:
				from_buying = get_from_rate.json()["data"][0]["currency_buy_amount"]
				from_selling = get_from_rate.json()["data"][0]["currency_sell_amount"]

				to_buying = get_to_rate.json()["data"][0]["currency_buy_amount"]
				to_selling = get_to_rate.json()["data"][0]["currency_sell_amount"]

				temp_from = float(amount) * float(from_selling)
				temp_to = float(temp_from) / float(to_buying)
				value = temp_to
				return float(value)
			except Exception:
				value = 0
				return float(value)


###############
#### iVeri ####
###############
@app.route("/iveri/bookings/payments/<booking_id>")
def get_iveri_posted_data(booking_id):
	get_booking = db.session.query(Booking)\
							.join(Detail, Booking.booking_public_id == Detail.booking_id)\
							.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
										 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
										 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
										 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
										 Booking.payment_status, Booking.updated_at, Booking.currency,\
										 BookingStatus.booking_status_name,\
										 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
										 Detail.address, Detail.additional_note)\
							.filter(Booking.deletion_marker == None)\
							.filter(Booking.booking_public_id == booking_id)\
							.first()

	booking_details = {}
	bookingTotal(booking_details, booking_id)
	
	# guest_count_array = []
	
	# get_all_guests = db.session.query(BookingGuest)\
	# 							.join(GuestType, BookingGuest.guest_type == GuestType.booking_guest_type_public_id)\
	# 							.add_columns(GuestType.booking_guest_type_name,\
	# 										BookingGuest.guest_count)\
	# 							.filter(BookingGuest.deletion_marker == None)\
	# 							.filter(BookingGuest.booking_id == booking_id)\
	# 							.all()

	# for each_guest in get_all_guests:
	# 	guest_count_array.append(each_guest.guest_count)

	get_booking2 = db.session.query(Booking)\
							 .filter(Booking.deletion_marker == None)\
							 .filter(Booking.booking_public_id == booking_id)\
							 .first()
	
	if not get_booking:
		output = []
		output.append("The selected booking does not appear to exist.")
		return jsonify({"message": output}), 200

	# return_bookings = db.session.query(Booking)\
	# 						    .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
	# 						    .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
	# 						    .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
	# 						    .join(Detail, Booking.booking_public_id == Detail.booking_id)\
	# 						    .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
	# 						   				 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
	# 										 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
	# 										 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status,\
	# 										 Booking.currency,\
	# 										 BookingType.booking_type_name,\
	# 										 BookingStatus.booking_status_name,\
	# 										 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
	# 										 Detail.address, Detail.additional_note)\
	# 						    .filter(Booking.deletion_marker == None)\
	# 						    .filter(Booking.booking_public_id == booking_id)\
	# 						    .all()

	# if not return_bookings:
	# 	pass
	# else:
	# 	output = []
	# 	id_array = []
	# 	inventory_total_cost_array = []
	# 	total_cost_array = []

		# for single in return_bookings:
		# 	guest_array = []
		# 	guest_sum = []

		# 	mpesa_transaction_ref = single.booking_ref_code

		# 	booking_currency = single.currency

			# get_all_guests = db.session.query(GatepassGuest)\
			# 						   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
			# 						   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
			# 						   .add_columns(Mandatory.payment_person,\
			# 						   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
			# 										GatepassGuest.gatepass_no_of_nights)\
			# 						   .filter(GatepassGuest.deletion_marker == None)\
			# 						   .filter(Gatepass.booking_id == single.booking_public_id)\
			# 						   .all()

			# for each_guest in get_all_guests:
			# 	guest_sum.append(int(each_guest.gatepass_guest_count))

			# facility_array = []
			# facility_total_cost_array = []
			
			# facility_bookings = db.session.query(Facility)\
			# 							  .filter(Facility.deletion_marker == None)\
			# 							  .filter(Facility.booking_id == single.booking_public_id)\
			# 							  .all()

			# for single_facility in facility_bookings:
			# 	fac_adult_discount = float(single_facility.facility_cost_per_adult) * (100 - float(single_facility.facility_discount_rate))/100
			# 	fac_adult_spec_discount = fac_adult_discount * (100 - float(single_facility.facility_commission_rate))/100

			# 	fac_child_discount = float(single_facility.facility_cost_per_child) * (100 - float(single_facility.facility_discount_rate))/100
			# 	fac_child_spec_discount = fac_child_discount * (100 - float(single_facility.facility_commission_rate))/100
				
			# 	pre_facility_adult_cost = float(single_facility.facility_booking_adults) * float(single_facility.facility_cost_per_adult) * \
			# 						  float(single_facility.facility_no_of_nights)
			# 	pre_facility_child_cost = float(single_facility.facility_booking_children) * float(single_facility.facility_cost_per_child) * \
			# 						  float(single_facility.facility_no_of_nights)

			# 	facility_adult_cost = float(single_facility.facility_booking_adults) * float(fac_adult_spec_discount) * \
			# 						  float(single_facility.facility_no_of_nights)
			# 	facility_child_cost = float(single_facility.facility_booking_children) * float(fac_child_spec_discount) * \
			# 						  float(single_facility.facility_no_of_nights)

			# 	if single_facility.facility_cost_per_adult == 0 and single_facility.facility_cost_per_child == 0:
			# 		fixed_cost_discount = float(single_facility.facility_fixed_cost) * (100 - float(single_facility.facility_discount_rate))/100
			# 		fixed_cost_spec_discount= fixed_cost_discount * (100 - float(single_facility.facility_commission_rate))/100

			# 		if single_facility.facility_booking_children > 0 and single_facility.facility_booking_adults > 0:
			# 			facility_fixed_cost = fixed_cost_spec_discount * (single_facility.facility_booking_children\
			# 								  + single_facility.facility_booking_adults) * single_facility.facility_no_of_nights
			# 		elif single_facility.facility_booking_children == 0:
			# 			facility_fixed_cost = fixed_cost_spec_discount * single_facility.facility_booking_adults  * single_facility.facility_no_of_nights
			# 		elif single_facility.facility_booking_adults == 0:
			# 			facility_fixed_cost = fixed_cost_spec_discount * single_facility.facility_booking_children * single_facility.facility_no_of_nights
					
				
			# 	else:
			# 		if single_facility.facility_no_of_nights/7 <= 1:
			# 			facility_fixed_cost = float(single_facility.facility_fixed_cost)
					
			# 		elif single_facility.facility_no_of_nights/7 > 1:
			# 			weeks_count = round(single_facility.facility_no_of_nights/7) + 1
						
			# 			facility_fixed_cost = float(single_facility.facility_fixed_cost) * weeks_count
				
				
			# 	facility_total_cost_array.append(facility_adult_cost + facility_child_cost + facility_fixed_cost)
			# 	total_cost_array.append(facility_adult_cost + facility_child_cost + facility_fixed_cost)

				
			# inventory_array = []
			# inventory_sub_total = []
			# inventory_id_array = []
			
			# inventory_bookings = db.session.query(Inventory)\
			# 							   .filter(Inventory.deletion_marker == None)\
			# 							   .filter(Inventory.booking_id == single.booking_public_id)\
			# 							   .all()

			# for single_inventory in inventory_bookings:
			# 	adult_discount = float(single_inventory.inventory_cost_per_adult) * (100 - float(single_inventory.inventory_discount_rate))/100
			# 	adult_spec_discount = adult_discount * (100 - float(single_inventory.inventory_commission_rate))/100

			# 	child_discount = float(single_inventory.inventory_cost_per_child) * (100 - float(single_inventory.inventory_discount_rate))/100
			# 	child_spec_discount = child_discount * (100 - float(single_inventory.inventory_commission_rate))/100
				
			# 	pre_adult_cost = float(single_inventory.inventory_booking_adults) * float(single_inventory.inventory_cost_per_adult)
			# 	pre_child_cost = float(single_inventory.inventory_booking_children) * float(single_inventory.inventory_cost_per_child)

			# 	adult_cost = float(single_inventory.inventory_booking_adults) * adult_spec_discount
			# 	child_cost = float(single_inventory.inventory_booking_children) * child_spec_discount
				
			# 	inventory_total_cost_array.append(adult_cost + child_cost)
			# 	total_cost_array.append(adult_cost + child_cost)
				
				
			# gatepass_total_cost_array = []

			# gatepass_details = db.session.query(GatepassGuest)\
			# 							 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
			# 							 .add_columns(GatepassGuest.gatepass_guest_type, GatepassGuest.gatepass_guest_count,\
			# 							 			  GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
			# 										  GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_currency)\
			# 							 .filter(GatepassGuest.deletion_marker == None)\
			# 							 .filter(Gatepass.deletion_marker == None)\
			# 							 .filter(Gatepass.booking_id == single.booking_public_id)\
			# 							 .all()

			# for each_pass in gatepass_details:
			# 	try:
			# 		gatepass_sub_cost = round(float(each_pass.gatepass_guest_count) * ((100 - float(each_pass.gatepass_discount_rate))/100) * \
			# 								  float(each_pass.gatepass_cost_per_pp) * float(each_pass.gatepass_no_of_nights), 2)
					
			# 		gatepass_total_cost_array.append(gatepass_sub_cost)
			# 		total_cost_array.append(gatepass_sub_cost)
			# 	except (NameError, ValueError, TypeError) as no_value:
			# 		pass

			# vehicle_total_cost_array = []
			# vehicle_count_array = []
			# vehicle_array = []
			
			# vehicle_charges = db.session.query(GatepassVehicle)\
			# 							.join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
			# 							.join(Vehicle, GatepassVehicle.gatepass_vehicle_type == Vehicle.vehicle_charge_public_id)\
			# 							.add_columns(GatepassVehicle.gatepass_vehicle_count, GatepassVehicle.gatepass_cost_per_vehicle,\
			# 										 GatepassVehicle.gatepass_vehicle_no_of_nights, GatepassVehicle.gatepass_vehicle_discount_rate,\
			# 										 Vehicle.vehicle_charge_category)\
			# 							.filter(GatepassVehicle.deletion_marker == None)\
			# 							.filter(Gatepass.deletion_marker == None)\
			# 							.filter(Gatepass.booking_id == single.booking_public_id)\
			# 							.all()

			# for each_vehicle in vehicle_charges:
			# 	try:
			# 		vehicle_sub_cost = round(float(each_vehicle.gatepass_vehicle_count) * float(each_vehicle.gatepass_cost_per_vehicle)\
			# 								 * ((100 - float(each_vehicle.gatepass_vehicle_discount_rate))/100) * float(each_vehicle.gatepass_vehicle_no_of_nights))

			# 		vehicle_count_array.append(each_vehicle.gatepass_vehicle_count)
			# 		vehicle_total_cost_array.append(vehicle_sub_cost)
			# 		total_cost_array.append(vehicle_sub_cost)
					
			# 		vehicle_array.append(return_vehicle_data)

			# 	except (NameError, ValueError, TypeError) as no_value:
			# 		pass

	# total = round(sum(total_cost_array), 2)
	total = booking_details["total_cost"]

	email_data = {}
	email_data["recipient"] = get_booking.email_address
	email_data["sender"] = "reservations@olpejetaconservancy.org"
	email_data["subject"] = "Booking Payment (#" + get_booking.booking_ref_code + ")"

	email_data["today"] = datetime.now().strftime("%B %Y")
	email_data["booking_ref_code"] = get_booking.booking_ref_code
	email_data["first_name"] = get_booking.first_name
	email_data["last_name"] = get_booking.last_name
	email_data["check_in_date"] = get_booking.booking_check_in_date.strftime("%A, %d %b %Y")
	email_data["check_out_date"] = get_booking.booking_check_out_date.strftime("%A, %d %b %Y")
	email_data["booking_ref_code"] = get_booking.booking_ref_code
	email_data["client"] = get_booking.first_name + " " + get_booking.last_name

	get_booking2.payment_status = 1
	get_booking2.updated_at = datetime.now()
	get_booking2.status = get_booking_status_id("Confirmed")
	
	try:
		amount = request.json["amount"] / 100
	except Exception:
		amount = total

	try:
		card = request.json["card_number"]

		card_first = card[:4]
		card_last = card[-4:]
	except Exception:
		card_first = None
		card_last = None

	get_exchange_rate = requests.get(get_buy_sell_rate.format(get_booking.currency))
	buying_rate = get_exchange_rate.json()["data"][0]["currency_buy_amount"]
	selling_rate = get_exchange_rate.json()["data"][0]["currency_sell_amount"]
	
	get_past_payments = db.session.query(Transaction)\
								  .filter(Transaction.deletion_marker == None)\
								  .filter(Transaction.booking_id == booking_id)\
								  .all()

	if len(get_past_payments) == 0:
		converted_transaction_amount = currencyHandler(get_booking.currency, get_booking.currency, amount)
			
		balance = float(total) - float(converted_transaction_amount)

	elif len(get_past_payments) > 0:
		total_payments = []
		for each_payment in get_past_payments:
			if each_payment.transaction_payment_currency:
				converted_transaction_total = currencyHandler(get_booking.currency, each_payment.transaction_payment_currency, each_payment.transaction_total)
			else:
				converted_transaction_total = currencyHandler(get_booking.currency, get_booking.currency, each_payment.transaction_total)
			
			total_payments.append(float(converted_transaction_total))

		past_total = sum(total_payments)

		if past_total == total:
			message = []
			message.append("The booking has already been paid for")
			return jsonify({"message": message}), 200

		converted_transaction_amount = currencyHandler(get_booking.currency, get_booking.currency, amount)
		
		pending = float(total) - float(past_total)
		balance = pending - float(converted_transaction_amount)
	
	transaction_id = str(uuid.uuid4())
	
	payment = BookingPayment(
		booking_payment_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		transaction_id = transaction_id,
		payment_method = "73414fc2-5bf7-42d1-9cdf-08fee8a0b08e",
		phone_number = None,
		mpesa_reference = None,
		card_first_four = card_first,
		card_last_four = card_last,
		booking_amount = total,
		amount_paid = amount,
		session_id = None
	)

	db.session.add(payment)

	transaction = Transaction(
		transaction_booking_public_id = transaction_id,
		booking_id = booking_id,
		transaction_original_cost = total,
		transaction_total = amount,
		transaction_total_currency = get_booking.currency,
		transaction_balance = balance,
		transaction_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e",
		transaction_payment_currency = get_booking.currency,
		payment_currency_buying_rate_at_time = buying_rate,
		payment_currency_selling_rate_at_time = selling_rate,
		transaction_date = datetime.now(),
		session_id = "iVeri",
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(transaction)

	get_to_invoice = db.session.query(Invoice)\
							   .filter(Invoice.deletion_marker == None)\
							   .filter(Invoice.booking_id == booking_id)\
							   .first()

	if get_to_invoice:
		get_to_invoice.deletion_marker = 1
		get_to_invoice.updated_at = datetime.now()

	get_all_inventory = db.session.query(Inventory)\
								  .filter(Inventory.deletion_marker == None)\
								  .filter(Inventory.booking_id == booking_id)\
								  .all()

	if get_all_inventory:
		for single_inventory in get_all_inventory:
			single_inventory.inventory_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_inventory.updated_at = datetime.now()

	else:
		pass

	get_all_facilities = db.session.query(Facility)\
								   .filter(Facility.deletion_marker == None)\
								   .filter(Facility.booking_id == booking_id)\
								   .all()

	if get_all_facilities:
		for single_facility in get_all_facilities:
			single_facility.facility_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_facility.updated_at = datetime.now()

	else:
		pass

	gatepass = db.session.query(Gatepass)\
						 .filter(Gatepass.deletion_marker == None)\
						 .filter(Gatepass.booking_id == booking_id)\
						 .first()

	gatepass_id = gatepass.gatepass_public_id

	get_all_guests = db.session.query(GatepassGuest)\
							   .filter(GatepassGuest.deletion_marker == None)\
							   .filter(GatepassGuest.gatepass_id == gatepass_id)\
							   .all()

	if get_all_guests:
		for single_guest in get_all_guests:
			single_guest.gatepass_guest_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_guest.updated_at = datetime.now()

	else:
		pass

	get_all_vehicles = db.session.query(GatepassVehicle)\
								 .filter(GatepassVehicle.deletion_marker == None)\
								 .filter(GatepassVehicle.gatepass_id == gatepass_id)\
								 .all()

	if get_all_vehicles:
		for single_vehicle in get_all_vehicles:
			single_vehicle.gatepass_vehicle_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_vehicle.updated_at = datetime.now()

	else:
		pass

	try:
		db.session.commit()
		close(db)

		try:
			send_email(email_data)
		except Exception:
			pass

		output = []
		output.append("Payment for the booking has been successfully carried out.")
		return jsonify({"message": output}), 200
	
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was a slight issue paying for the booking.")
		return jsonify({"message": output}), 422


@app.route("/ui-iveri/booking-payment", methods = ["POST"])
def ui_iveri_payment():
	validation_list = [
		{"field": "booking_id"},
		{"field": "amount"},
		{"field": "card_number"},
		{"field": "session_id"}
	]

	messages = fieldValidation(request.json, validation_list)
	if messages:
		return jsonify({
			"messages": messages
		}), 422

	booking_id = request.json["booking_id"]
	
	get_booking = db.session.query(Booking)\
							.join(Detail, Booking.booking_public_id == Detail.booking_id)\
							.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
										 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
										 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
										 Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
										 Booking.payment_status, Booking.updated_at, Booking.currency,\
										 BookingStatus.booking_status_name,\
										 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
										 Detail.address, Detail.additional_note)\
							.filter(Booking.deletion_marker == None)\
							.filter(Booking.booking_public_id == booking_id)\
							.first()

	booking_details = {}
	bookingTotal(booking_details, booking_id)

	get_booking2 = db.session.query(Booking)\
							 .filter(Booking.deletion_marker == None)\
							 .filter(Booking.booking_public_id == booking_id)\
							 .first()
	
	if not get_booking:
		output = []
		output.append("The selected booking does not appear to exist.")
		return jsonify({"message": output}), 200

	total = booking_details["total_cost"]

	email_data = {}
	email_data["recipient"] = get_booking.email_address
	email_data["sender"] = "reservations@olpejetaconservancy.org"
	email_data["subject"] = "Booking Payment (#" + get_booking.booking_ref_code + ")"

	email_data["today"] = datetime.now().strftime("%B %Y")
	email_data["booking_ref_code"] = get_booking.booking_ref_code
	email_data["first_name"] = get_booking.first_name
	email_data["last_name"] = get_booking.last_name
	email_data["check_in_date"] = get_booking.booking_check_in_date.strftime("%A, %d %b %Y")
	email_data["check_out_date"] = get_booking.booking_check_out_date.strftime("%A, %d %b %Y")
	email_data["booking_ref_code"] = get_booking.booking_ref_code
	email_data["client"] = get_booking.first_name + " " + get_booking.last_name

	get_booking2.payment_status = 1
	get_booking2.updated_at = datetime.now()
	get_booking2.status = get_booking_status_id("Confirmed")
	
	# This SHOULD NOT be posted in cents
	amount = request.json["amount"]

	card = request.json["card_number"]

	card_first = card[:4]
	card_last = card[-4:]

	get_exchange_rate = requests.get(get_buy_sell_rate.format(get_booking.currency))
	buying_rate = get_exchange_rate.json()["data"][0]["currency_buy_amount"]
	selling_rate = get_exchange_rate.json()["data"][0]["currency_sell_amount"]
	
	get_past_payments = db.session.query(Transaction)\
								  .filter(Transaction.deletion_marker == None)\
								  .filter(Transaction.booking_id == booking_id)\
								  .all()

	if len(get_past_payments) == 0:
		converted_transaction_amount = currencyHandler(get_booking.currency, get_booking.currency, amount)
			
		balance = float(total) - float(converted_transaction_amount)

	elif len(get_past_payments) > 0:
		total_payments = []
		for each_payment in get_past_payments:
			if each_payment.transaction_payment_currency:
				converted_transaction_total = currencyHandler(get_booking.currency, each_payment.transaction_payment_currency, each_payment.transaction_total)
			else:
				converted_transaction_total = currencyHandler(get_booking.currency, get_booking.currency, each_payment.transaction_total)
			
			total_payments.append(float(converted_transaction_total))

		past_total = sum(total_payments)

		if past_total == total:
			message = []
			message.append("The booking has already been paid for")
			return jsonify({"message": message}), 200

		converted_transaction_amount = currencyHandler(get_booking.currency, get_booking.currency, amount)
		
		pending = float(total) - float(past_total)
		balance = pending - float(converted_transaction_amount)
	
	transaction_id = str(uuid.uuid4())
	
	payment = BookingPayment(
		booking_payment_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		transaction_id = transaction_id,
		payment_method = "73414fc2-5bf7-42d1-9cdf-08fee8a0b08e",
		phone_number = None,
		mpesa_reference = None,
		card_first_four = card_first,
		card_last_four = card_last,
		booking_amount = total,
		amount_paid = amount,
		session_id = request.json["session_id"]
	)

	db.session.add(payment)

	transaction = Transaction(
		transaction_booking_public_id = transaction_id,
		booking_id = booking_id,
		transaction_original_cost = total,
		transaction_total = amount,
		transaction_total_currency = get_booking.currency,
		transaction_balance = balance,
		transaction_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e",
		transaction_payment_currency = get_booking.currency,
		payment_currency_buying_rate_at_time = buying_rate,
		payment_currency_selling_rate_at_time = selling_rate,
		transaction_date = datetime.now(),
		session_id = request.json["session_id"],
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(transaction)

	get_to_invoice = db.session.query(Invoice)\
							   .filter(Invoice.deletion_marker == None)\
							   .filter(Invoice.booking_id == booking_id)\
							   .first()

	if get_to_invoice:
		get_to_invoice.deletion_marker = 1
		get_to_invoice.updated_at = datetime.now()

	get_all_inventory = db.session.query(Inventory)\
								  .filter(Inventory.deletion_marker == None)\
								  .filter(Inventory.booking_id == booking_id)\
								  .all()

	if get_all_inventory:
		for single_inventory in get_all_inventory:
			single_inventory.inventory_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_inventory.updated_at = datetime.now()

	else:
		pass

	get_all_facilities = db.session.query(Facility)\
								   .filter(Facility.deletion_marker == None)\
								   .filter(Facility.booking_id == booking_id)\
								   .all()

	if get_all_facilities:
		for single_facility in get_all_facilities:
			single_facility.facility_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_facility.updated_at = datetime.now()

	else:
		pass

	gatepass = db.session.query(Gatepass)\
						 .filter(Gatepass.deletion_marker == None)\
						 .filter(Gatepass.booking_id == booking_id)\
						 .first()

	gatepass_id = gatepass.gatepass_public_id

	get_all_guests = db.session.query(GatepassGuest)\
							   .filter(GatepassGuest.deletion_marker == None)\
							   .filter(GatepassGuest.gatepass_id == gatepass_id)\
							   .all()

	if get_all_guests:
		for single_guest in get_all_guests:
			single_guest.gatepass_guest_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_guest.updated_at = datetime.now()

	else:
		pass

	get_all_vehicles = db.session.query(GatepassVehicle)\
								 .filter(GatepassVehicle.deletion_marker == None)\
								 .filter(GatepassVehicle.gatepass_id == gatepass_id)\
								 .all()

	if get_all_vehicles:
		for single_vehicle in get_all_vehicles:
			single_vehicle.gatepass_vehicle_payment_method = "33ad20be-3a89-4deb-ab71-7510ba51677e"
			single_vehicle.updated_at = datetime.now()

	else:
		pass

	try:
		db.session.commit()
		close(db)

		try:
			send_email(email_data)
		except Exception:
			pass

		output = []
		output.append("Payment for the booking has been successfully carried out.")
		return jsonify({"message": output}), 200
	
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was a slight issue paying for the booking.")
		return jsonify({"message": output}), 422


@app.route("/bookings/payment/new", methods = ["POST"])
def pay_for_booking():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	if request.json["session_id"] == "Mpesa":
		get_booking_id = db.session.query(Booking)\
								   .filter(Booking.deletion_marker == None)\
								   .filter(Booking.booking_ref_code == request.json["booking_id"])\
								   .first()
		
		booking_id = get_booking_id.booking_public_id

	else:
		booking_id = request.json["booking_id"]

	try:
		request.json["payment_method"].strip()
		if not request.json["payment_method"]:
			messages.append("Payment method is empty.")
	except KeyError as e:
		messages.append("Payment method is missing.")

	try:
		str(request.json["booking_amount"]).strip()
		if not request.json["booking_amount"]:
			messages.append("Booking amount is empty.")
	except KeyError as e:
		messages.append("Booking amount is missing.")

	try:
		str(request.json["payment_amount"]).strip()
		if not request.json["payment_amount"]:
			messages.append("Payment amount is empty.")
	except KeyError as e:
		messages.append("Payment amount is missing.")
	
	try:
		request.json["session_id"].strip()
		# if not request.json["session_id"]:
		# 	messages.append("Session ID is empty.")
		session_id = request.json["session_id"]
	except KeyError as e:
		# messages.append("Session ID is missing.")
		session_id = None
	
	if messages:
		return jsonify({"messages": messages}), 422

	if float(request.json["payment_amount"]) <= 0:
		message = []
		message.append("The payment amount cannot be less than or equal to 0.")
		return jsonify({"message": message}), 422
	
	try:
		phone_number = request.json["phone"]
	except (KeyError) as phone_error:
		print(str(phone_error))
		phone_number = None

	try:
		mpesa_reference = request.json["mpesa_ref"]
	except (KeyError) as mpesa_error:
		print(str(mpesa_error))
		mpesa_reference = None

	try:
		card_first_four = request.json["first_four"]
		card_last_four = request.json["last_four"]
	except (KeyError) as card_error:
		print(str(card_error))
		card_first_four = None
		card_last_four = None

	try:
		payment_gateway = request.json["payment_gateway"]["payment_gateway_public_id"]
	except Exception:
		payment_gateway = None

	payment_method = db.session.query(PaymentMethod)\
							   .filter(PaymentMethod.deletion_marker == None)\
							   .filter(PaymentMethod.payment_method_public_id == request.json["payment_method"])\
							   .first()

	if payment_method:
		pass
	else:
		message = []
		message.append("The selected payment method does not exist in the system.")
		return jsonify({"message": request.json["payment_method"]}), 422

	return_bookings = db.session.query(Booking)\
							    .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							    .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							    .join(Detail, Booking.booking_public_id == Detail.booking_id)\
							    .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
											 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status,\
											 Booking.currency,\
											 BookingType.booking_type_name,\
											 BookingStatus.booking_status_name,\
											 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
											 Detail.address, Detail.additional_note)\
							    .filter(Booking.deletion_marker == None)\
							    .filter(Booking.booking_public_id == booking_id)\
							    .all()

	if not return_bookings:
		output = []
		output.append("The selected booking does not appear to exist in the system.")
		return jsonify({"message": output}), 200
	return_data = {}
	bookingTotal(return_data, booking_id)

	total = round(return_data["total_cost"], 2)
	
	##### Handling requests from the Online checkout service
	if request.json["session_id"] == "Mpesa":
		check_payment = db.session.query(BookingPayment)\
								  .filter(BookingPayment.mpesa_reference == mpesa_reference)\
								  .first()

		if check_payment:
			message = []
			message.append("The booking has already been paid for.")
			return jsonify({"message": message}), 200
		
		else:		
			transaction_id = str(uuid.uuid4())
			
			## 1c9d01cc-ef31-4757-a390-10c765fcecab
			# payment_method = request.json["payment_method"],
			payment = BookingPayment(
				booking_payment_public_id = str(uuid.uuid4()),
				booking_id = booking_id,
				transaction_id = transaction_id,
				payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab",
				payment_currency = "162fface-f5f1-41de-913b-d2bb784dda3a",
				phone_number = phone_number,
				mpesa_reference = mpesa_reference,
				card_first_four = card_first_four,
				card_last_four = card_last_four,
				booking_amount = total,
				amount_paid = request.json["payment_amount"],
				session_id = session_id
			)

			db.session.add(payment)

			return_booking = db.session.query(Booking)\
										.filter(Booking.deletion_marker == None)\
										.filter(Booking.booking_public_id == booking_id)\
										.first()

			get_past_payments = db.session.query(Transaction)\
										.filter(Transaction.deletion_marker == None)\
										.filter(Transaction.booking_id == booking_id)\
										.all()
			
			if len(get_past_payments) == 0:
				balance = float(total) - float(request.json["payment_amount"])
				
				## Buying and selling rate are set to 1 since Mpesa payment is assumed to be in KES
				transaction = Transaction(
					transaction_booking_public_id = transaction_id,
					booking_id = booking_id,
					transaction_original_cost = total,
					transaction_total = request.json["payment_amount"],
					transaction_balance = balance,
					transaction_payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab",
					transaction_payment_currency = "162fface-f5f1-41de-913b-d2bb784dda3a",
					transaction_date = datetime.now(),
					transaction_total_currency = return_booking.currency,
					payment_currency_buying_rate_at_time = 1,
					payment_currency_selling_rate_at_time = 1,
					session_id = session_id,
					created_at = datetime.now(),
					updated_at = datetime.now()
				)

				db.session.add(transaction)

				get_to_invoice = db.session.query(Invoice)\
							   			   .filter(Invoice.deletion_marker == None)\
							   			   .filter(Invoice.booking_id == booking_id)\
							   			   .first()

				if get_to_invoice:
					get_to_invoice.deletion_marker = 1
					get_to_invoice.updated_at = datetime.now()

				if float(total) <= float(request.json["payment_amount"]):
					return_booking.payment_status = 1
					return_booking.updated_at = datetime.now()
					return_booking.status = get_booking_status_id("Confirmed")
					## To prevent update of booking session_id
					# return_booking.session_id = session_id
				elif float(total) > float(request.json["payment_amount"]):
					return_booking.payment_status = 2
					return_booking.updated_at = datetime.now()
					return_booking.status = get_booking_status_id("Deposit")
					## To prevent update of booking session_id
					# return_booking.session_id = session_id

			elif len(get_past_payments) > 0:
				total_payments = []
				for each_payment in get_past_payments:
					total_payments.append(float(each_payment.transaction_total))

				past_total = sum(total_payments)

				pending = float(total) - past_total
				balance = pending - float(request.json["payment_amount"])

				## Buying and selling rate are set to 1 since Mpesa payment is assumed to be in KES
				transaction = Transaction(
					transaction_booking_public_id = transaction_id,
					booking_id = booking_id,
					transaction_original_cost = total,
					transaction_total = request.json["payment_amount"],
					transaction_balance = balance,
					transaction_payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab",
					transaction_payment_currency = "162fface-f5f1-41de-913b-d2bb784dda3a",
					transaction_date = datetime.now(),
					transaction_total_currency = return_booking.currency,
					payment_currency_buying_rate_at_time = 1,
					payment_currency_selling_rate_at_time = 1,
					session_id = session_id,
					created_at = datetime.now(),
					updated_at = datetime.now()
				)

				db.session.add(transaction)

				get_to_invoice = db.session.query(Invoice)\
							   			   .filter(Invoice.deletion_marker == None)\
							    		   .filter(Invoice.booking_id == booking_id)\
							   			   .first()

				if get_to_invoice:
					get_to_invoice.deletion_marker = 1
					get_to_invoice.updated_at = datetime.now()

				if pending <= float(request.json["payment_amount"]):
					return_booking.payment_status = 1
					return_booking.updated_at = datetime.now()
					return_booking.status = get_booking_status_id("Confirmed")
					## To prevent update of booking session_id
					# return_booking.session_id = session_id
				elif pending > float(request.json["payment_amount"]):
					return_booking.payment_status = 2
					return_booking.updated_at = datetime.now()
					return_booking.status = get_booking_status_id("Deposit")
					## To prevent update of booking session_id
					# return_booking.session_id = session_id

			get_all_inventory = db.session.query(Inventory)\
										.filter(Inventory.deletion_marker == None)\
										.filter(Inventory.booking_id == booking_id)\
										.all()

			if get_all_inventory:
				for single_inventory in get_all_inventory:
					single_inventory.inventory_payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab"
					single_inventory.updated_at = datetime.now()

			else:
				pass

			get_all_facilities = db.session.query(Facility)\
										.filter(Facility.deletion_marker == None)\
										.filter(Facility.booking_id == booking_id)\
										.all()

			if get_all_facilities:
				for single_facility in get_all_facilities:
					single_facility.facility_payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab"
					single_facility.updated_at = datetime.now()

			else:
				pass

			gatepass = db.session.query(Gatepass)\
								.filter(Gatepass.deletion_marker == None)\
								.filter(Gatepass.booking_id == booking_id)\
								.first()

			gatepass_id = gatepass.gatepass_public_id

			get_all_guests = db.session.query(GatepassGuest)\
									.filter(GatepassGuest.deletion_marker == None)\
									.filter(GatepassGuest.gatepass_id == gatepass_id)\
									.all()

			if get_all_guests:
				for single_guest in get_all_guests:
					single_guest.gatepass_guest_payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab"
					single_guest.updated_at = datetime.now()

			else:
				pass

			get_all_vehicles = db.session.query(GatepassVehicle)\
										.filter(GatepassVehicle.deletion_marker == None)\
										.filter(GatepassVehicle.gatepass_id == gatepass_id)\
										.all()

			if get_all_vehicles:
				for single_vehicle in get_all_vehicles:
					single_vehicle.gatepass_vehicle_payment_method = "1c9d01cc-ef31-4757-a390-10c765fcecab"
					single_vehicle.updated_at = datetime.now()

			else:
				pass
			
			try:
				db.session.commit()
				close(db)
				message = []
				message.append("The booking has been paid for.")
				return jsonify({"message": message}), 200

			except Exception as e:
				db.session.rollback()
				close(db)
				message = []
				message.append("There was an error paying for the booking.")
				return jsonify({"message": message, "error": str(e)}), 422

	##### Handling requests from the UI
	elif request.json["payment_method"] == "cbff45f3-4f12-41da-8b4a-bf308112f032":
			check_existing_payment = db.session.query(BookingPayment)\
											   .filter(BookingPayment.deletion_marker == None)\
											   .filter(BookingPayment.payment_method == request.json["payment_method"])\
											   .filter(BookingPayment.booking_id == request.json["booking_id"])\
											   .filter(BookingPayment.amount_paid == request.json["payment_amount"])\
											   .first()

			if not check_existing_payment:
				message = []
				message.append("The Mpesa payment was not successful. Please try again.")
				return jsonify({"message": message}), 422
			
			else:
				message = []
				message.append("Mpesa payment successful.")
				return jsonify({"message": message}), 200
		
	elif request.json["payment_method"] != "cbff45f3-4f12-41da-8b4a-bf308112f032":
		transaction_id = str(uuid.uuid4())
		
		payment = BookingPayment(
			booking_payment_public_id = str(uuid.uuid4()),
			booking_id = request.json["booking_id"],
			transaction_id = transaction_id,
			payment_method = request.json["payment_method"],
			payment_currency = request.json["currency_id"],
			phone_number = phone_number,
			mpesa_reference = mpesa_reference,
			card_first_four = card_first_four,
			card_last_four = card_last_four,
			booking_amount = total,
			amount_paid = request.json["payment_amount"],
			session_id = session_id
		)

		db.session.add(payment)

		return_booking = db.session.query(Booking)\
									.filter(Booking.deletion_marker == None)\
									.filter(Booking.booking_public_id == request.json["booking_id"])\
									.first()

		get_past_payments = db.session.query(Transaction)\
									.filter(Transaction.deletion_marker == None)\
									.filter(Transaction.booking_id == request.json["booking_id"])\
									.all()
		
		get_exchange_rate = requests.get(get_buy_sell_rate.format(request.json["currency_id"]))
		buying_rate = get_exchange_rate.json()["data"][0]["currency_buy_amount"]
		selling_rate = get_exchange_rate.json()["data"][0]["currency_sell_amount"]
		
		if len(get_past_payments) == 0:
			converted_transaction_amount = currencyHandler(return_booking.currency, request.json["currency_id"], request.json["payment_amount"])
			
			balance = float(request.json["booking_amount"]) - float(converted_transaction_amount)
			
			transaction = Transaction(
				transaction_booking_public_id = transaction_id,
				booking_id = request.json["booking_id"],
				transaction_original_cost = request.json["booking_amount"],
				transaction_total = request.json["payment_amount"],
				transaction_balance = balance,
				transaction_payment_method = request.json["payment_method"],
				transaction_payment_gateway = payment_gateway,
				transaction_payment_currency = request.json["currency_id"],
				transaction_date = datetime.now(),
				transaction_total_currency = return_booking.currency,
				payment_currency_buying_rate_at_time = buying_rate,
				payment_currency_selling_rate_at_time = selling_rate,
				session_id = session_id,
				created_at = datetime.now(),
				updated_at = datetime.now()
			)

			db.session.add(transaction)

			get_to_invoice = db.session.query(Invoice)\
							   		   .filter(Invoice.deletion_marker == None)\
							   		   .filter(Invoice.booking_id == request.json["booking_id"])\
							   		   .first()

			if get_to_invoice:
				get_to_invoice.deletion_marker = 1
				get_to_invoice.updated_at = datetime.now()

			# if float(request.json["booking_amount"]) <= float(request.json["payment_amount"]):
			if float(request.json["booking_amount"]) <= float(converted_transaction_amount):
				return_booking.payment_status = 1
				return_booking.updated_at = datetime.now()
				return_booking.status = get_booking_status_id("Confirmed")
				## To prevent update of booking session_id
				# return_booking.session_id = session_id
			elif float(request.json["booking_amount"]) > float(converted_transaction_amount):
				# Prevents negligible amounts like 0.0233 from indicating that a booking has not been paid for
				if round(balance) == 0:
					return_booking.payment_status = 1
				else:
					return_booking.payment_status = 2
				return_booking.updated_at = datetime.now()
				return_booking.status = get_booking_status_id("Deposit")
				## To prevent update of booking session_id
				# return_booking.session_id = session_id

		elif len(get_past_payments) > 0:
			total_payments = []
			for each_payment in get_past_payments:
				if each_payment.transaction_payment_currency:
					converted_transaction_total = currencyHandler(return_booking.currency, each_payment.transaction_payment_currency, each_payment.transaction_total)
				else:
					converted_transaction_total = currencyHandler(return_booking.currency, return_booking.currency, each_payment.transaction_total)
				
				total_payments.append(float(converted_transaction_total))

			past_total = sum(total_payments)

			converted_transaction_amount = currencyHandler(return_booking.currency, request.json["currency_id"], request.json["payment_amount"])
			
			pending = float(request.json["booking_amount"]) - float(past_total)
			balance = pending - float(converted_transaction_amount)

			transaction = Transaction(
				transaction_booking_public_id = transaction_id,
				booking_id = request.json["booking_id"],
				transaction_original_cost = request.json["booking_amount"],
				transaction_total = request.json["payment_amount"],
				transaction_balance = balance,
				transaction_payment_method = request.json["payment_method"],
				transaction_payment_gateway = payment_gateway,
				transaction_payment_currency = request.json["currency_id"],
				transaction_date = datetime.now(),
				transaction_total_currency = return_booking.currency,
				payment_currency_buying_rate_at_time = buying_rate,
				payment_currency_selling_rate_at_time = selling_rate,
				session_id = session_id,
				created_at = datetime.now(),
				updated_at = datetime.now()
			)

			db.session.add(transaction)

			get_to_invoice = db.session.query(Invoice)\
							   		   .filter(Invoice.deletion_marker == None)\
							   		   .filter(Invoice.booking_id == request.json["booking_id"])\
							   		   .first()

			if get_to_invoice:
				get_to_invoice.deletion_marker = 1
				get_to_invoice.updated_at = datetime.now()

			if pending <= float(converted_transaction_amount):
				return_booking.payment_status = 1
				return_booking.updated_at = datetime.now()
				return_booking.status = get_booking_status_id("Confirmed")
				## To prevent update of booking session_id
				# return_booking.session_id = session_id
			elif pending > float(converted_transaction_amount):
				# return_booking.payment_status = 2
				# Prevents negligible amounts like 0.0233 from indicating that a booking has not been paid for
				if round(balance) == 0:
					return_booking.payment_status = 1
				else:
					return_booking.payment_status = 2
				return_booking.updated_at = datetime.now()
				return_booking.status = get_booking_status_id("Deposit")
				## To prevent update of booking session_id
				# return_booking.session_id = session_id

		get_all_inventory = db.session.query(Inventory)\
									.filter(Inventory.deletion_marker == None)\
									.filter(Inventory.booking_id == request.json["booking_id"])\
									.all()

		if get_all_inventory:
			for single_inventory in get_all_inventory:
				single_inventory.inventory_payment_method = request.json["payment_method"]
				single_inventory.updated_at = datetime.now()

		else:
			pass

		get_all_facilities = db.session.query(Facility)\
									.filter(Facility.deletion_marker == None)\
									.filter(Facility.booking_id == request.json["booking_id"])\
									.all()

		if get_all_facilities:
			for single_facility in get_all_facilities:
				single_facility.facility_payment_method = request.json["payment_method"]
				single_facility.updated_at = datetime.now()

		else:
			pass

		gatepass = db.session.query(Gatepass)\
							.filter(Gatepass.deletion_marker == None)\
							.filter(Gatepass.booking_id == request.json["booking_id"])\
							.first()

		gatepass_id = gatepass.gatepass_public_id

		get_all_guests = db.session.query(GatepassGuest)\
								.filter(GatepassGuest.deletion_marker == None)\
								.filter(GatepassGuest.gatepass_id == gatepass_id)\
								.all()

		if get_all_guests:
			for single_guest in get_all_guests:
				single_guest.gatepass_guest_payment_method = request.json["payment_method"]
				single_guest.updated_at = datetime.now()

		else:
			pass

		get_all_vehicles = db.session.query(GatepassVehicle)\
									.filter(GatepassVehicle.deletion_marker == None)\
									.filter(GatepassVehicle.gatepass_id == gatepass_id)\
									.all()

		if get_all_vehicles:
			for single_vehicle in get_all_vehicles:
				single_vehicle.gatepass_vehicle_payment_method = request.json["payment_method"]
				single_vehicle.updated_at = datetime.now()

		else:
			pass
		
		try:
			db.session.commit()
			close(db)
			message = []
			message.append("The booking has been paid for.")
			return jsonify({"message": message}), 200

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("There was an error paying for the booking.")

			return jsonify({"message": message, "error": str(e)}), 422


@app.route("/bookings/payment/defer", methods = ["POST"])
def defer_booking_payment():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	get_booking = db.session.query(Booking)\
							.filter(Booking.deletion_marker == None)\
							.filter(Booking.booking_public_id == request.json["booking_id"])\
							.first()

	if not get_booking:
		message = []
		message.append("The selected booking does not appear to exist.")
		return jsonify({"message": message}), 422

	check_if_partner_booking = db.session.query(Partner)\
										 .filter(Partner.deletion_marker == None)\
										 .filter(Partner.booking_id == get_booking.booking_public_id)\
										 .first()

	if not check_if_partner_booking:
		get_booking.status = get_booking_status_id("Unconfirmed")

	else:
		get_partner = requests.get(get_partner_details.format(check_if_partner_booking.partner_id))
		
		if get_partner.json()["type_public_id"] == "90b00b2b":
			get_booking.status = get_booking_status_id("To Invoice")
			## To Invoice
			get_booking.payment_status = 4

			invoice = Invoice(
				invoice_public_id = str(uuid.uuid4()),
				booking_id = get_booking.booking_public_id,
				session_id = request.json["session_id"],
				created_at = datetime.now()
			)

			db.session.add(invoice)

		else:
			get_booking.status = get_booking_status_id("Unconfirmed")

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Booking payment deferred.")
		return jsonify({"message": message}), 200

	except Exception:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to defer the booking payment. Try again later.")
		return jsonify({"message": message}), 422


@app.route("/bookings/paid", methods = ["POST"])
def mark_booking_as_paid():
	messages = []

	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	get_booking = db.session.query(Booking)\
							.filter(Booking.deletion_marker == None)\
							.filter(Booking.booking_public_id == request.json["booking_id"])\
							.filter(Booking.status != get_booking_status_id("Cancelled"))\
							.first()

	if not get_booking:
		message = []
		message.append("The selected booking either does not exist or has been cancelled.")
		return jsonify({"message": message}), 412

	get_booking.status = get_booking_status_id("Confirmed")
	get_booking.payment_status = 1
	get_booking.updated_at = datetime.now()

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		booking_activity_description = "Booking marked as paid",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Booking marked as paid.")
		return jsonify({"message": message}), 200

	except Exception:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to mark the booking as paid. Try again later.")
		return jsonify({"message": message}), 422


@app.route("/bookings/compliments", methods = ["POST"])
def mark_as_complimentary():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	get_booking = db.session.query(Booking)\
							.filter(Booking.deletion_marker == None)\
							.filter(Booking.booking_public_id == request.json["booking_id"])\
							.filter(Booking.status != get_booking_status_id("Cancelled"))\
							.first()

	if not get_booking:
		message = []
		message.append("The selected booking either does not exist or has been cancelled.")
		return jsonify({"message": message}), 412

	get_booking_gatepass = db.session.query(Gatepass)\
									 .filter(Gatepass.deletion_marker == None)\
									 .filter(Gatepass.booking_id == request.json["booking_id"])\
									 .filter(Gatepass.status != get_booking_status_id("Updated"))\
									 .first()

	get_gatepass_guests = db.session.query(GatepassGuest)\
									.filter(GatepassGuest.deletion_marker == None)\
									.filter(GatepassGuest.gatepass_id == get_booking_gatepass.gatepass_public_id)\
									.all()

	for single_gatepass_guest in get_gatepass_guests:
		single_gatepass_guest.gatepass_discount_rate = 100
		single_gatepass_guest.updated_at = datetime.now()

	get_gatepass_vehicles = db.session.query(GatepassVehicle)\
									  .filter(GatepassVehicle.deletion_marker == None)\
									  .filter(GatepassVehicle.gatepass_id == get_booking_gatepass.gatepass_public_id)\
									  .all()

	for single_gatepass_vehicle in get_gatepass_vehicles:
		single_gatepass_vehicle.gatepass_vehicle_discount_rate = 100
		single_gatepass_vehicle.updated_at = datetime.now()

	get_booking_inventory = db.session.query(Inventory)\
									  .filter(Inventory.deletion_marker == None)\
									  .filter(Inventory.booking_id == request.json["booking_id"])\
									  .filter(Inventory.status != get_booking_status_id("Updated"))\
									  .all()

	for single_inventory in get_booking_inventory:
		single_inventory.inventory_discount_rate = 100
		single_inventory.updated_at = datetime.now()

	get_booking_facilities = db.session.query(Facility)\
									   .filter(Facility.deletion_marker == None)\
									   .filter(Facility.booking_id == request.json["booking_id"])\
									   .filter(Facility.status != get_booking_status_id("Updated"))\
									   .all()

	for single_facility in get_booking_facilities:
		single_facility.facility_discount_rate = 100
		single_facility.updated_at = datetime.now()

	get_booking.status = get_booking_status_id("Complimentary")
	## Complimentary
	get_booking.payment_status = 3
	get_booking.updated_at = datetime.now()

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		booking_activity_description = "Booking marked as free-of-charge",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Booking marked as free-of-charge.")
		return jsonify({"message": message}), 200

	except Exception:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to mark the booking as free-of-charge. Try again later.")
		return jsonify({"message": message}), 422


@app.route("/bookings/payment/mpesa/new", methods = ["POST"])
def pay_for_booking_mpesa():
	messages = []
	
	try:
		request.json["phone"].strip()
		if not request.json["phone"]:
			messages.append("Phone number is empty.")
	except KeyError as e:
		messages.append("Phone number is missing.")
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		# str(request.json["payment_amount"]).strip()
		# if not request.json["payment_amount"]:
		# 	messages.append("Payment amount is empty.")
		data_payment_amount = request.json["payment_amount"]
	except KeyError as e:
		# messages.append("Payment amount is missing.")
		data_payment_amount = None

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")
	
	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	return_bookings = db.session.query(Booking)\
							    .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							    .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							    .join(Detail, Booking.booking_public_id == Detail.booking_id)\
							    .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
											 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status,\
											 Booking.currency, Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time,\
											 BookingType.booking_type_name,\
											 BookingStatus.booking_status_name,\
											 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number,\
											 Detail.address, Detail.additional_note)\
							    .filter(Booking.deletion_marker == None)\
							    .filter(Booking.booking_public_id == request.json["booking_id"])\
							    .first()

	if not return_bookings:
		output = []
		output.append("The selected booking does not appear to exist in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		# id_array = []
		# inventory_total_cost_array = []
		# total_cost_array = []

		# for single in return_bookings:
			# guest_array = []
			# guest_sum = []

		mpesa_transaction_ref = return_bookings.booking_ref_code

		booking_currency = return_bookings.currency
		buying_rate = return_bookings.currency_buying_rate_at_time
		selling_rate = return_bookings.currency_selling_rate_at_time

		booking_details = {}
		bookingTotal(booking_details, return_bookings.booking_public_id)

	## Handling UI requests
	if data_payment_amount:
		json_payment_amount = request.json["payment_amount"]

		# payment_amount = currencyHandler("162fface-f5f1-41de-913b-d2bb784dda3a", booking_currency, json_payment_amount)
		payment_amount = currencyHandler("162fface-f5f1-41de-913b-d2bb784dda3a", "162fface-f5f1-41de-913b-d2bb784dda3a", json_payment_amount)
	
	## Public portal
	else:
		get_all_booking_donations = db.session.query(Donation)\
											  .filter(Donation.deletion_marker == None)\
											  .filter(Donation.booking_id == request.json["booking_id"])\
											  .all()

		donations = []
		if get_all_booking_donations:
			for single_donation in get_all_booking_donations:
				donation_amount = currencyPostProcessor("162fface-f5f1-41de-913b-d2bb784dda3a", single_donation.donation_currency, single_donation.donation_amount,\
														single_donation.currency_buying_amount, single_donation.currency_selling_amount)

				donations.append(donation_amount)

		donation_total = sum(donations)
		
		# payment_amount = round(sum(total_cost_array), 2)
		converted_amount = currencyPostProcessor("162fface-f5f1-41de-913b-d2bb784dda3a", booking_currency, booking_details["total_cost"], buying_rate, selling_rate)
		payment_amount = math.ceil(round((converted_amount + donation_total), 2))
	
	if int(payment_amount) <= 70000:
		try:
			pay = requests.post(mpesa_pay, json = {
						"phone_number": request.json["phone"],
						"payment_to": "Booking",
						"amount": payment_amount,
						"reference": mpesa_transaction_ref,
						"booking_id": request.json["booking_id"],
						"transaction_reference": "B-" + mpesa_transaction_ref
					})
			
			try:
				if pay.status_code == 200:
					message = []
					message.append("Request successful. Please check mobile device.")
					return jsonify({"message": message}), 200

				elif pay.status_code == 404:
					message = []
					message.append(pay.json()["errorMessage"])
					return jsonify({"message": message}), 422
				
				else:
					message = []
					message.append("The Mpesa payment service is currently unavailable. Try again later.")
					return jsonify({"message": message, "status": str(pay.status_code)}), 422

			except Exception as e:
				message = []
				message.append("There was a slight error. Try again.")
				return jsonify({"message": message, "error": str(e)}), 422
		except Exception:
			message = []
			message.append("There was an error connecting to the Mpesa payment service. Try again later.")
			return jsonify({"message": message}), 422
	
	elif int(payment_amount) > 70000:
		message = []
		message.append("The amount is greater than KES 70,000. Try another method.")
		return jsonify({"message": message, "amount": float(payment_amount), "currency": booking_currency}), 422



@app.route("/mail_test")
def mail_test():
	random_array = []
	error_array = []
	
	sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])

	recipient = ["mwanyoike@fluidtechglobal.com","dsituma@fluidtechglobal.com","rrandiek@fluidtechglobal.com","amwathi@fluidtechglobal.com"]
	sender = "reservations@olpejetaconservancy.org"

	for single in recipient:
		from_email = Email(sender)
		to_email = Email(single)
		subject = "Multi-receiving email"
		content = Content("text/html", "Hello. This is a test.")
		mail = Mail(from_email, subject, to_email, content)

		try:
			response = sg.client.mail.send.post(request_body=mail.get())

			# return jsonify({'message' : str(response)})
			random_array.append(str(response))

		except Exception as e:
			# return jsonify({"message": str(e)})
			error_array.append(str(e))

	if random_array:
		return jsonify({"message": random_array})
	elif error_array:
		return jsonify({"data": error_array})


##############
#### Mail ####
##############
def send_email(email_data):
	recipient = email_data["recipient"]
	sender = email_data["sender"]

	booking_details = {}
	booking_details["booking_ref_code"] = email_data["booking_ref_code"]
	booking_details["first_name"] = email_data["first_name"]
	booking_details["last_name"] = email_data["last_name"]
	booking_details["check_in_date"] = email_data["check_in_date"]
	booking_details["check_out_date"] = email_data["check_out_date"]
	booking_details["client"] = email_data["first_name"] + " " + email_data["last_name"]
	booking_details["copyright_year"] = datetime.now().strftime("%Y")

	sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])
	
	from_email = Email(sender)
	to_email = Email(recipient)
	subject = email_data["subject"]
	content = Content("text/html", render_template("confirmation_with_activities.html", data = booking_details))
	mail = Mail(from_email, subject, to_email, content)

	try:
		response = sg.client.mail.send.post(request_body=mail.get())

		return jsonify({'message' : str(response)})

	except Exception as e:
		return jsonify({"message": str(e)})


#############
#### Ref ####
#############
def get_booking_status_id(status_name):
	status_id = db.session.query(BookingStatus)\
						  .filter(BookingStatus.deletion_marker == None)\
						  .filter(BookingStatus.booking_status_name == status_name)\
						  .all()

	for single in status_id:
		booking_status_id = single.booking_status_public_id

		return booking_status_id