from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes import bookings_logger
from database.pickup_location import Location
from variables import *


def close(self):
	self.session.close()


@app.route("/pick-up/new", methods = ["POST"])
def add_new_location():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	check_name = db.session.query(Location)\
						   .filter(Location.deletion_marker == None)\
						   .all()

	for each_name in check_name:
		if each_name.pickup_location_name.lower() == request.json["name"].lower():
			message = []
			message.append("That pick-up location is already in the system.")
			return jsonify({"message": message}), 422
		else:
			pass

	location = Location(
		pickup_location_public_id = str(uuid.uuid4()),
		pickup_location_name = request.json["name"].title(),
		session_id = request.json["session_id"]
	)

	db.session.add(location)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("You have successfully created a new location.")

		return jsonify({"message": output}), 201
	except Exception as e:
		print(e)

		db.session.rollback()
		close(db)

		output = []
		output.append("Unfortunately, you cannot add a pick-up at this moment. Please try again.")

		return jsonify({"message": output}), 422


@app.route("/pick-up/view")
# @bookings_logger.logWrapper()
def get_all_locations():
	output = []
	id_array = []

	all_locations = db.session.query(Location)\
							  .filter(Location.deletion_marker == None)\
							  .order_by(Location.pickup_location_name.asc())\
							  .all()

	if not all_locations:
		output.append("There are no locations in the system at this point in time.")

		return jsonify({"message": output}), 412

	else:
		for single in all_locations:
			output.append(single.return_json())

		return jsonify({"data": output}), 200


@app.route("/pick-up/view/<location_id>")
def get_single_location(location_id):
	output = []
	id_array = []

	all_locations = db.session.query(Location)\
							  .filter(Location.deletion_marker == None)\
							  .filter(Location.pickup_location_public_id == location_id)\
							  .all()

	if not all_locations:
		output.append("The selected location is not in the system at this point in time.")

		return jsonify({"message": output}), 412

	else:
		for single in all_locations:
			output.append(single.return_json())

		return jsonify({"data": output}), 200


@app.route("/pick-up/modify", methods = ["PATCH"])
def modify_location():
	messages = []
	
	try:
		request.json["location_id"].strip()
		if not request.json["location_id"]:
			messages.append("Location ID is empty.")
	except KeyError as e:
		messages.append("Location ID is missing.")
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	check_location = db.session.query(Location)\
							   .filter(Location.deletion_marker == None)\
							   .filter(Location.pickup_location_public_id == request.json["location_id"])\
							   .first()

	if not check_location:
		message = []
		message.append("The selected pick-up location does not exist in the system.")
		return jsonify({"messages": messages}), 412
	else:
		check_location.pickup_location_name = request.json["name"].title()
		check_location.session_id = request.json["session_id"]
		check_location.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			output = []
			output.append("You have successfully modified the location.")

			return jsonify({"message": output}), 200
		except Exception as e:
			print(e)

			db.session.rollback()
			close(db)

			output = []
			output.append("Unfortunately, you modify the pick-up at this moment. Please try again.")

			return jsonify({"message": output}), 422


@app.route("/pick-up/delete", methods = ["PATCH"])
def delete_single_location():
	messages = []
	
	try:
		request.json["location_id"].strip()
		if not request.json["location_id"]:
			messages.append("Location ID is empty.")
	except KeyError as e:
		messages.append("Location ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422
	
	get_location = db.session.query(Location)\
							 .filter(Location.deletion_marker == None)\
							 .filter(Location.pickup_location_public_id == request.json["location_id"])\
							 .first()

	get_location.deletion_marker = 1
	get_location.session_id = request.json["session_id"]

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("You have successfully deleted the location.")

		return jsonify({"message": output}), 200

	except Exception as e:
		print(e)

		db.session.rollback()
		close(db)

		output = []
		output.append("Unfortunately, you cannot delete a location at this moment. Please try again.")

		return jsonify({"message": output}), 422