from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes import db_cache, FromCache
from routes.seed_functions import PaymentMethodSeed
from database.payment_methods import PaymentMethod


def close(self):
	self.session.close()


#########################
#### Payment Methods ####
#########################
@app.route("/payments/methods/new", methods = ["POST"])
def add_new_payment_method():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["description"].strip()
		if not request.json["description"]:
			messages.append("Description is empty.")
	except KeyError as e:
		messages.append("Description is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422
	
	get_existing = db.session.query(PaymentMethod)\
							 .filter(PaymentMethod.deletion_marker == None)\
							 .options(FromCache(db_cache))\
							 .all()

	for single_method in get_existing:
		if single_method.payment_method_name.lower() == request.json["name"].lower():
			message = []
			message.append("The entered payment method already exists.")
			return jsonify({"message": message}), 422
	
	payment_method = PaymentMethod(
		payment_method_public_id = str(uuid.uuid4()),
		payment_method_name = request.json["name"],
		payment_method_description = request.json["description"],
		payment_method_guest = request.json["guest"],
		payment_method_partner = request.json["partner"],
		session_id = request.json["session_id"]
	)

	db.session.add(payment_method)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The payment method has been added. Please carry on.")
		return jsonify({"message": output}), 201
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("It's us, not you. There was an error while adding the payment method. :-( Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/payments/methods/view")
def view_all_payment_methods():
	# if db.session.query(PaymentMethod)\
	# 			 .filter(PaymentMethod.deletion_marker==None)\
	# 			 .filter(PaymentMethod.payment_method_public_id == "1e62ecfc-6a3a-4112-835c-4dc3fe8e28c7")\
	# 			 .options(FromCache(db_cache))\
	# 			 .count() == 0:
	# 	PaymentMethodSeed.seed_default_payment_methods()
	
	return_methods = db.session.query(PaymentMethod)\
							   .filter(PaymentMethod.deletion_marker==None)\
							   .filter(PaymentMethod.payment_method_public_id != "33ad20be-3a89-4deb-ab71-7510ba51677e")\
							   .filter(PaymentMethod.payment_method_public_id != "1c9d01cc-ef31-4757-a390-10c765fcecab")\
							   .options(FromCache(db_cache))\
							   .order_by(PaymentMethod.payment_method_name.asc())\
							   .all()

	if not return_methods:
		message = []
		message.append("There are currently no payment methods in the system.")
		return jsonify({"message": message}), 412
	
	data = []

	for single in return_methods:
		data.append(single.return_json())

	return jsonify({"data": data}), 200


@app.route("/payments/methods/delete", methods = ["PATCH"])
def delete_single_payment_method():
	messages = []
	
	try:
		request.json["method_id"].strip()
		if not request.json["method_id"]:
			messages.append("Method ID is empty.")
	except KeyError as e:
		messages.append("Method ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422
	
	return_method = db.session.query(PaymentMethod)\
							  .filter(PaymentMethod.payment_method_public_id == request.json["method_id"])\
							  .filter(PaymentMethod.deletion_marker == None)\
							  .first()

	if not return_method:
		message = []
		message.append("That payment method does not appear to exist.")
		return jsonify({"message": message}), 200
	
	return_method.deletion_marker = "1"
	return_method.updated_at = datetime.now()

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The method of payment has been deleted successfully.")
		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was a slight issue deleting the payment method. :-( Please try again later.")
		return jsonify({"message": output}), 422