from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from variables import *


def close(self):
	self.session.close()


@app.route("/count/bookings/all")
def count_all_bookings():
	bookings_count = db.session.query(Booking)\
							   .filter(Booking.deletion_marker == None)\
							   .count()

	return jsonify({"data": bookings_count}), 200