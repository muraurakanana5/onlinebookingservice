import math
import os
import uuid
from datetime import date, datetime, timedelta

import pymysql
import requests
from flask import (Flask, json, jsonify, redirect, render_template, request,
                   url_for)

from database.booking_activity_log import BookingActivity
from database.booking_details import Detail
from database.booking_guest_types import GuestType
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_types import BookingType
from database.bookings import Booking
from database.bookings_notes import Note
from database.facility import Facility
from database.gatepass import Gatepass
from database.gatepass_details import GatepassDetail
from database.gatepass_guests import GatepassGuest
from database.gatepass_vehicles import GatepassVehicle
from database.group import Group
from database.inventory import Inventory
from database.invoice import Invoice
from database.mandatory_payment_prices import MandatoryPaymentPrices
from database.mandatory_payments import Mandatory
from database.school import School
from database.school_contact import SchoolContact
from database.school_booking import SchoolBooking
from database.transaction import Transaction
from database.vehicle import Vehicle
from functions.booking_snippets import *
from functions.currency_operators import *
from functions.date_operators import *
from functions.validation import *
# File imports
from routes import app, bookings_logger, db, schoolCodeGenerator
from routes.bookings_urls import currencyHandler, send_booking_email
from routes.seed_functions import (BookingGuestTypesSeed, BookingStatusSeed,
                                   BookingTypeSeed, MandatoryPaymentsSeed)
from variables import *


def close(self):
	self.session.close()


#######################
#### Booking Types ####
#######################
@app.route("/bookings/group/types/view")
def view_all_group_booking_types():
	if db.session.query(BookingType)\
				 .filter(BookingType.deletion_marker==None)\
				 .filter(BookingType.booking_type_public_id == "7769748C")\
				 .count() == 0:
		BookingTypeSeed.seed_default_booking_types_methods()

	if db.session.query(BookingType)\
				.filter(BookingType.deletion_marker==None)\
				.count() == 0:
		output = []
		output.append("There are currently no booking in the system.")
		return jsonify({"message": output}), 412

	return_booking_types = db.session.query(BookingType)\
									 .filter(BookingType.deletion_marker == None)\
									 .filter(BookingType.booking_type_group == 1)\
									 .all()

	output = []

	for single in return_booking_types:
		return_data = {}
		return_data["booking_type_public_id"] = single.booking_type_public_id
		return_data["booking_type_name"] = single.booking_type_name
		return_data["booking_type_description"] = single.booking_type_description
		return_data["session_id"] = single.session_id
		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		output.append(return_data)

	return jsonify({"data": output}), 200


########################
#### Group Bookings ####
########################
@app.route("/group/bookings/new", methods = ["POST"])
def add_new_group_booking():
	today = datetime.now().strftime("%Y-%m-%d")

	if db.session.query(BookingStatus)\
					.filter(BookingStatus.deletion_marker==None)\
					.filter(BookingStatus.booking_status_public_id == "3b5376e0-a4dc-476e-aebc-6280b44b756a")\
					.count() == 0:
		BookingStatusSeed.seed_default_booking_status()

	if db.session.query(GuestType)\
				 .filter(GuestType.deletion_marker==None)\
				 .filter(GuestType.booking_guest_type_public_id == "2df20a96")\
				 .count() == 0:
		BookingGuestTypesSeed.seed_default_booking_guest_types_methods()

	validation_list = [
		{"field": "type", "alias": "Booking type"},
		{"field": "check_in", "alias": "Check-in date"},
		{"field": "check_out", "alias": "Check-out date"},
		{"field": "currency"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)
	
	if request.json["school"]["primary_contact"]:
		organisation = False
		school = True

		## TODO: Remove this after v1.2.17
		try:
			if "school_id" not in request.json["school"]:
				message = []
				message.append("You are currently using an outdated version of OBTS. Please refresh the page and make the school booking again.")
				return jsonify({"message": message}), 422
			
		except Exception:
			pass

		booking_type = "8D7E6504"

		school_details_list = [
			{"field": "primary_contact", "alias": "Primary contact name"},
			{"field": "primary_contact_phone", "alias": "Primary contact phone"},
			{"field": "primary_contact_email", "alias": "Primary contact email address"}
		]

		messages = messages + fieldValidation(request.json["school"], school_details_list)

		try:
			first_name = request.json["school"]["primary_contact"].split(" ")[0].title()
		except Exception as e:
			first_name = None

		try:
			last_name = request.json["school"]["primary_contact"].split(" ")[1].title()
		except Exception as e:
			last_name = ""

		phone = request.json["school"]["primary_contact_phone"]
		email = request.json["school"]["primary_contact_email"].lower()

		get_school = db.session.query(School)\
							   .filter(School.deletion_marker == None)\
							   .filter(School.school_public_id == request.json["school"]["school_id"])\
							   .first()

		city = get_school.closest_city_town

	else:
		organisation = True
		school = False

		booking_type = "C3237489"

		org_details_list = [
			{"field": "organisation_name", "alias": "Organisation name"},
			{"field": "organisation_email", "alias": "Organisation email address"},
			{"field": "organisation_type", "alias": "Organisation type"},
			{"field": "name"},
			{"field": "email", "alias": "Email address"},
			{"field": "phone"}
		]

		messages = messages + fieldValidation(request.json, org_details_list)
		
		try:
			first_name = request.json["name"].split(" ")[0].title()
		except Exception:
			first_name = None

		try:
			last_name = request.json["name"].split(" ")[1].title()
		except Exception:
			last_name = ""

		phone = request.json["phone"]
		email = request.json["email"].lower()
		city = None

	if messages:
		return jsonify({"messages": messages}), 422
	
	seven_days_ago = (datetime.now() - timedelta(days=7)).strftime("%Y-%m-%d")
	
	# Date validation
	# if request.json["check_in"] < today:
	# 	message = []
	# 	message.append("Your check in date cannot be in the past.")
	# 	return jsonify({"message": message}), 422

	if request.json["check_in"] < seven_days_ago:
		message = []
		message.append("There is an error with the set check-in date. Try again.")
		return jsonify({"message": message}), 422

	# if request.json["check_out"] < today:
	# 	message = []
	# 	message.append("Your check out date cannot be in the past.")
	# 	return jsonify({"message": message}), 422

	if request.json["check_out"] < seven_days_ago:
		message = []
		message.append("There is an error with the set check-out date. Try again.")
		return jsonify({"message": message}), 422

	if request.json["check_out"] < request.json["check_in"]:
		message = []
		message.append("Your check-out date cannot come before your check-in date.")
		return jsonify({"message": message}), 422

	try:
		promo_code_json = request.json["promo_code"]

	except Exception:
		promo_code_json = None

	if promo_code_json:
		try:
			check_promo_validity = requests.post(promo_code_search, json = {"promo_code": promo_code_json})

			if check_promo_validity.status_code != 200:
				return jsonify({"message": check_promo_validity.json()["message"]}), 422
				
			elif check_promo_validity.status_code == 200:
				promo_code = promo_code_json
				promo_discount = int(check_promo_validity.json()["data"][0]["percentage_off"])
				promo_code_public_id = check_promo_validity.json()["data"][0]["public_id"]
		
		except Exception:
			message = []
			message.append("There was an issue getting the promo code details. Please try again or leave the promo code field blank.")
			return jsonify({"message": message}), 422

	else:
		promo_code = None
		promo_discount = None
		promo_code_public_id = None

	# Validating the number of guests
	guest_sum = []
	
	for each_guest in request.json["guests"]:
		try:
			guest_numbers = int(each_guest["payment_guests"])
		except Exception:
			guest_numbers = 0
		guest_sum.append(guest_numbers)
	
	total_guests = sum(guest_sum)

	phone_validation = validatePhoneNumber(phone)

	if phone_validation[0]:
		phone = phone_validation[1]
	
	else:
		message = []
		message.append(phone_validation[1])
		return jsonify({"message": message}), 422

	valid_email = validateEmail(email)

	if not valid_email:
		message = []
		message.append("The email address provided is invalid.")
		return jsonify({"message": message}), 422

	# Generating date from string using a simple, custom function
	check_in_date = GenerateDateFromString.generateDate(request.json["check_in"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out"])
	
	# Getting date difference using a simple, custom function
	temp_date_diff = DateOperations.returnDateDifferenceInDays(check_out_date, check_in_date)

	if temp_date_diff == 0:
		date_diff = 1
	elif temp_date_diff > 0:
		date_diff = temp_date_diff

	try:
		exchange_rate_data = requests.get(get_latest_exchange_rate)
	except requests.exceptions.ConnectionError as connection_error:
		message = []
		message.append("The currency exchange rate is not available at the moment. Please try again later.")
		return jsonify({"message": message, "error": str(connection_error)}), 422
	
	booking_id = str(uuid.uuid4())
	gatepass_id = str(uuid.uuid4())
	booking_ref = str(uuid.uuid4())[:10]

	get_booking_exchange_rate = requests.get(get_buy_sell_rate.format(request.json["currency"]))
	booking_buying_rate = get_booking_exchange_rate.json()["data"][0]["currency_buy_amount"]
	booking_selling_rate = get_booking_exchange_rate.json()["data"][0]["currency_sell_amount"]
	
	booking = Booking(
		booking_public_id = booking_id,
		booking_type = booking_type,
		booking_check_in_date = request.json["check_in"],
		booking_check_out_date = request.json["check_out"],
		booking_done_by = first_name + " " + last_name,
		booking_ref_code = booking_ref,
		status = get_booking_status_id("Unconfirmed"),
		currency = request.json["currency"],
		currency_buying_rate_at_time = booking_buying_rate,
		currency_selling_rate_at_time = booking_selling_rate,
		session_id = request.json["session_id"],
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(booking)

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		booking_activity_description = "Booking created",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	booking_info = {}
	booking_info["booking_id"] = booking_id
	booking_info["booking_ref"] = booking_ref
	booking_info["first_name"] = first_name
	booking_info["last_name"] = last_name
	
	facility_email_data = []

	try:
		facilityBooking(request.json, booking_info, facility_email_data)
	except Exception as e:
		return jsonify({"message": str(e)}), 422

	inventory_email_data = []

	try:
		inventoryBooking(request.json, booking_info, inventory_email_data)
	except Exception as e:
		return jsonify({"message": str(e)}), 422

	try:
		for gatepass_guest in request.json["guests"]:
			get_guest_type = db.session.query(Mandatory)\
									   .filter(Mandatory.deletion_marker == None)\
									   .filter(Mandatory.payment_public_id == gatepass_guest["payment_public_id"])\
									   .first()
			
			try:
				guest_count = int(gatepass_guest["payment_guests"])
			except Exception:
				guest_count = 0
			
			guest = BookingGuest(
				booking_guest_public_id = str(uuid.uuid4()),
				booking_id = booking_id,
				guest_type = get_booking_guest_type_id(get_guest_type.payment_person),
				guest_count = guest_count
			)

			db.session.add(guest)

	except KeyError as guest_error:
		print("There was an error getting: " + str(guest_error))

	try:
		additional_note = request.json["additional_note"]
	except KeyError as guest_key_error:
		print("There was an error getting the following key: " + str(guest_key_error))
		additional_note = None

	try:
		address = request.json["address"]
	except KeyError as address_key_error:
		print("There was an error getting the following key: " + str(address_key_error))
		address = None

	try:
		country = request.json["country"]
	except KeyError as country_key_error:
		print("There was an error getting the following key: " + str(country_key_error))
		country = None

	detail = Detail(
		booking_details_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		first_name = first_name,
		last_name = last_name,
		email_address = email,
		phone_number = phone,
		country = country,
		city = city,
		address = address,
		additional_note = additional_note,
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(detail)

	try:
		note = request.json["note"]
	except Exception:
		note = None

	if note:
		new_note = Note(
			booking_notes_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			note = note,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(new_note)

	try:
		destination = request.json["destination"]
	except (KeyError) as destination_error:
		destination = None

	gatepass = Gatepass(
		gatepass_public_id = gatepass_id,
		gatepass_date = today,
		gatepass_done_by = first_name + " " + last_name,
		destination = destination,
		gatepass_phone_number = request.json["phone"],
		gatepass_ref_code = str(uuid.uuid4())[:10],
		booking_id = booking_id,
		booking = 1,
		start_date = request.json["check_in"],
		end_date = request.json["check_out"],
		gatepass_currency = request.json["currency"],
		gatepass_payment_status = 1,
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(gatepass)

	guest_info_data = []
	
	try:
		for each_gatepass_guest in request.json["guests"]:
			get_gatepass_fee = db.session.query(MandatoryPaymentPrices)\
										 .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
										 .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
													  MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
													  Mandatory.payment_person)\
										 .filter(MandatoryPaymentPrices.deletion_marker == None)\
										 .filter(MandatoryPaymentPrices.payment_schedule == each_gatepass_guest["payment_schedule"])\
										 .filter(MandatoryPaymentPrices.payment_category == each_gatepass_guest["payment_public_id"])\
										 .first()

			try:
				gatepass = get_gatepass_fee.payment_price
			except (ValueError, TypeError) as no_value:
				gatepass = 0
			
			gatepass_amount = currencyHandler(request.json["currency"], get_gatepass_fee.payment_currency, gatepass)
			
			get_ex_rate = requests.get(get_buy_sell_rate.format(get_gatepass_fee.payment_currency))

			try:
				buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

			except Exception:
				buying = 1
				selling = 1

			# if promo_discount:
			# 	person_discount = promo_discount
			# else:
			# 	try:
			# 		person_discount = each_gatepass_guest["payment_person_discount"]
			# 	except Exception:
			# 		person_discount = 0

			try:
				person_discount = float(each_gatepass_guest["payment_person_discount"])
			except Exception:
				person_discount = 0

			if promo_discount:
				person_discount_reason = promo_code
			else:
				try:
					person_discount_reason = each_gatepass_guest["reason"]
				except Exception:
					person_discount_reason = None

			try:
				guests = int(each_gatepass_guest["payment_guests"])
			except Exception:
				guests = 0
			
			gatepass_guest = GatepassGuest(
				gatepass_guest_public_id = str(uuid.uuid4()),
				gatepass_id = gatepass_id,
				gatepass_guest_type = each_gatepass_guest["payment_public_id"],
				gatepass_guest_count = guests,
				gatepass_discount_rate = person_discount,
				gatepass_discount_reason = person_discount_reason,
				gatepass_cost_per_pp = math.ceil(round(gatepass_amount, 2)),
				gatepass_payment_schedule = each_gatepass_guest["payment_schedule"],
				gatepass_no_of_nights = date_diff,
				gatepass_currency = request.json["currency"],
				gatepass_guest_cost_at_time = round(gatepass),
				gatepass_guest_currency_at_time = get_gatepass_fee.payment_currency,
				gatepass_guest_rate_at_time = buying,
				created_at = datetime.now(),
				updated_at = datetime.now()
			)

			db.session.add(gatepass_guest)

			guest_info = {}
	
			try:
				guest_numbers = int(each_gatepass_guest["payment_guests"])
			except Exception:
				guest_numbers = 0
			
			if guest_numbers != 0:
				guest_info["guest_category"] = get_gatepass_fee.payment_person
				guest_info["guest_number"] = each_gatepass_guest["payment_guests"]

				guest_info_data.append(guest_info)

	except KeyError as gatepass_guest_error:
		print("There was an error getting: " + str(gatepass_guest_error))

	if organisation:
		group = Group(
			group_booking_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			organisation_name = request.json["organisation_name"].title(),
			organisation_email = request.json["organisation_email"].lower(),
			organisation_type = request.json["organisation_type"],
			session_id = request.json["session_id"],
			created_at = datetime.now(),
			updated_at = datetime.now()
		)

		db.session.add(group)

	else:
		pass

	if school:
		get_school = db.session.query(School)\
							   .filter(School.deletion_marker == None)\
							   .filter(School.school_public_id == request.json["school"]["school_id"])\
							   .first()

		get_school.primary_contact = request.json["school"]["primary_contact"]
		get_school.primary_contact_email = request.json["school"]["primary_contact_email"].lower()
		get_school.primary_contact_phone = request.json["school"]["primary_contact_phone"]
		get_school.secondary_contact = request.json["school"]["secondary_contact"]
		get_school.secondary_contact_email = request.json["school"]["secondary_contact_email"].lower()
		get_school.secondary_contact_phone = request.json["school"]["secondary_contact_phone"]
		get_school.session_id = request.json["session_id"]
		get_school.updated_at = datetime.now()

		school_contact = SchoolContact(
			school_contact_public_id = str(uuid.uuid4()),
			school_id = request.json["school"]["school_id"],
			primary_contact = request.json["school"]["primary_contact"],
			primary_contact_email = request.json["school"]["primary_contact_email"].lower(),
			primary_contact_phone = request.json["school"]["primary_contact_phone"],
			secondary_contact = request.json["school"]["secondary_contact"],
			secondary_contact_email = request.json["school"]["secondary_contact_email"].lower(),
			secondary_contact_phone = request.json["school"]["secondary_contact_phone"],
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(school_contact)

		school_booking = SchoolBooking(
			school_booking_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			school_id = request.json["school"]["school_id"],
			session_id = request.json["session_id"],
			created_at = datetime.now(),
			updated_at = datetime.now()
		)

		db.session.add(school_booking)
	else:
		pass
	
	gatepass_detail = GatepassDetail(
		gatepass_details_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		first_name = first_name,
		last_name = last_name,
		email_address = request.json["email"].lower(),
		phone_number = phone,
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(gatepass_detail)

	vehicle_sum = []
	vehicle_info_data = []

	try:
		for each_vehicle in request.json["vehicles"]:
			get_cost = db.session.query(Vehicle)\
								 .filter(Vehicle.deletion_marker == None)\
								 .filter(Vehicle.vehicle_charge_public_id == each_vehicle["vehicle_charge_public_id"])\
								 .first()

			try:
				vehicle = get_cost.vehicle_charge_category_cost
			except (ValueError, TypeError) as no_value:
				vehicle = 0
			
			vehicle_fee = currencyHandler(request.json["currency"], get_cost.vehicle_charge_cost_currency, vehicle)
			
			get_ex_rate = requests.get(get_buy_sell_rate.format(get_cost.vehicle_charge_cost_currency))

			try:
				buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

			except Exception:
				buying = 1
				selling = 1

			if promo_discount:
				vehicle_discount = promo_discount
			else:
				try:
					vehicle_discount = float(each_vehicle["discount"])
				except Exception:
					vehicle_discount = 0

			if promo_code:
				vehicle_discount_reason = promo_code
			else:
				try:
					vehicle_discount_reason = each_vehicle["reason"]
				except Exception:
					vehicle_discount_reason = None
			
			try:
				vehicles = int(each_vehicle["vehicles"])
			except Exception:
				vehicles = 0
			
			gatepass_vehicle = GatepassVehicle(
				gatepass_vehicle_public_id = str(uuid.uuid4()),
				gatepass_id = gatepass_id,
				gatepass_vehicle_type = each_vehicle["vehicle_charge_public_id"],
				gatepass_vehicle_count = vehicles,
				gatepass_cost_per_vehicle = round(vehicle_fee),
				gatepass_vehicle_currency = request.json["currency"],
				gatepass_vehicle_no_of_nights = date_diff,
				gatepass_vehicle_discount_rate = vehicle_discount,
				gatepass_vehicle_discount_reason = vehicle_discount_reason,
				gatepass_vehicle_cost_at_time = round(vehicle),
				gatepass_vehicle_currency_at_time = get_cost.vehicle_charge_cost_currency,
				gatepass_vehicle_rate_at_time = buying,
				created_at = datetime.now(),
				updated_at = datetime.now()
			)

			db.session.add(gatepass_vehicle)

			vehicle_info = {}
			try:
				vehicle_numbers = int(each_vehicle["vehicles"])
			except Exception:
				vehicle_numbers = 0
			
			if vehicle_numbers != 0:
				vehicle_info["vehicle_category"] = get_cost.vehicle_charge_category
				vehicle_info["vehicle_number"] = each_vehicle["vehicles"]

				vehicle_info_data.append(vehicle_info)

			vehicle_sum.append(int(each_vehicle["vehicles"]))
	
	except KeyError as vehicle_key_error:
		print("There was an error getting the following key: " + str(vehicle_key_error))
	
	try:
		db.session.commit()
		close(db)

		email_array = [email]
		
		email_data = {}
		# email_data["recipient"] = email
		email_data["recipient"] = email_array
		email_data["sender"] = "reservations@olpejetaconservancy.org"
		email_data["subject"] = "Booking Notification (#" + booking_ref + ")"

		email_data["today"] = datetime.now().strftime("%B %Y")
		email_data["booking_ref_code"] = booking_ref
		email_data["first_name"] = first_name
		email_data["last_name"] = last_name
		email_data["check_in_date"] = check_in_date.strftime("%A, %d %b %Y")
		email_data["check_out_date"] = check_out_date.strftime("%A, %d %b %Y")
		email_data["num_of_guests"] = sum(guest_sum)
		email_data["num_of_vehicles"] = sum(vehicle_sum)
		email_data["client"] = first_name + " " + last_name
		email_data["booking_id"] = booking_id
		email_data["facility_info"] = facility_email_data
		email_data["inventory_info"] = inventory_email_data
		email_data["guest_info"] = guest_info_data
		email_data["vehicle_info"] = vehicle_info_data

		try:
			send_booking_email(email_data)
		except Exception:
			pass

		output = []
		output.append("The booking has been made. Please proceed to make your payment.")

		return_data = {}
		return_data["booking_public_id"] = booking_id
		return_data["email"] = email
		
		return jsonify({"message": output, "data": return_data}), 201
	
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while creating the booking.Please try again later.")
		return jsonify({"message": output, "error": str(e)}), 422


@app.route("/group/bookings/view")
def view_all_group_bookings():
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Group, Booking.booking_public_id == Group.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name)\
							   .filter(Booking.deletion_marker == None)\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		# try:
		# 	return_user = requests.post(get_user_from_aumra,\
		# 								json = {"users_ids": id_array})
		# except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		# 	pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(BookingGuest)\
									   .join(GuestType, BookingGuest.guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name,\
									   				BookingGuest.guest_count)\
									   .filter(BookingGuest.deletion_marker == None)\
									   .filter(BookingGuest.booking_id == single.booking_public_id)\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.booking_guest_type_name
				guest_data["no_of_guests"] = each_guest.guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			# try:
			# 	for user in return_user.json()["data"]:
			# 		if user["public_id"] == single.session_id:
						# return_data["session_user"] = user["first_name"] + " " + user["last_name"]
						# return_data["session_user"] = user["full_name"]
			# 			return_data["session_id"] = single.session_id
						
			# except (IndexError) as user_error:
			# 	return_data["session_user"] = "N/A"
			# 	return_data["session_id"] = single.session_id
			
			# except (AttributeError, UnboundLocalError) as network_related_errors:
			# 	return_data["session_user"] = "Network Error"
			# 	return_data["session_id"] = single.session_id
			
			return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/group/bookings/view/pending")
def view_all_pending_group_bookings():
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Group, Booking.booking_public_id == Group.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency, Booking.deletion_marker,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name)\
							   .filter(Booking.deletion_marker == None)\
							   .filter(Booking.status == get_booking_status_id("Unconfirmed"))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no pending group bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_ref_code"] = single.booking_ref_code

			check_school_booking = db.session.query(SchoolBooking)\
											 .join(School, SchoolBooking.school_id == School.school_public_id)\
											 .add_columns(School.school_name)\
											 .filter(SchoolBooking.deletion_marker == None)\
											 .filter(SchoolBooking.booking_id == single.booking_public_id)\
											 .first()

			check_org_booking = db.session.query(Group)\
										  .filter(Group.deletion_marker == None)\
										  .filter(Group.booking_id == single.booking_public_id)\
										  .first()

			try:
				if check_school_booking:
					return_data["booking_done_by"] = check_school_booking.school_name
				elif check_org_booking:
					return_data["booking_done_by"] = check_org_booking.organisation_name
				else:
					return_data["booking_done_by"] = single.booking_done_by

			except Exception:
				return_data["booking_done_by"] = single.booking_done_by

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			check_to_invoice = db.session.query(Invoice)\
										.filter(Invoice.deletion_marker == None)\
										.filter(Invoice.booking_id == single.booking_public_id)\
										.first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if single.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif single.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif single.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif single.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif single.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif single.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif single.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						# return_data["session_user"] = user["first_name"] + " " + user["last_name"]
						return_data["session_user"] = user["full_name"]
						return_data["session_id"] = single.session_id
						
			except (IndexError) as user_error:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id
			
			except (AttributeError, UnboundLocalError) as network_related_errors:
				return_data["session_user"] = "Network Error"
				return_data["session_id"] = single.session_id
			
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/group/bookings/view/approved")
def view_all_approved_group_bookings():
	return_bookings = db.session.query(Booking)\
							   .join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
							   .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
							   .join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
							   .join(Group, Booking.booking_public_id == Group.booking_id)\
							   .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
							   				Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
											Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
											Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
											Booking.currency,\
											BookingType.booking_type_name,\
											BookingStatus.booking_status_name)\
							   .filter(Booking.deletion_marker == None)\
							   .filter(Booking.status == get_booking_status_id("Confirmed"))\
							   .order_by(Booking.booking_id.desc())\
							   .all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		# try:
		# 	return_user = requests.post(get_user_from_aumra,\
		# 								json = {"users_ids": id_array})
		# except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		# 	pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(BookingGuest)\
									   .join(GuestType, BookingGuest.guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name,\
									   				BookingGuest.guest_count)\
									   .filter(BookingGuest.deletion_marker == None)\
									   .filter(BookingGuest.booking_id == single.booking_public_id)\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.booking_guest_type_name
				guest_data["no_of_guests"] = each_guest.guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.guest_count))

			return_data["guests"] = guest_array
			return_data["guest_total"] = sum(guest_sum)

			return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						# return_data["session_user"] = user["first_name"] + " " + user["last_name"]
						return_data["session_user"] = user["full_name"]
						return_data["session_id"] = single.session_id
						
			except (IndexError) as user_error:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id
			
			except (AttributeError, UnboundLocalError) as network_related_errors:
				return_data["session_user"] = "Network Error"
				return_data["session_id"] = single.session_id
			
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


## TODO: Update payment
@app.route("/group/bookings/modify", methods = ["PATCH"])
def change_group_booking_details():
	validation_list = [
		{"field": "booking_id", "alias": "Booking ID"},
		{"field": "check_in", "alias": "Check-in date"},
		{"field": "check_out", "alias": "Check-out date"},
		{"field": "currency"},
		{"field": "session_id", "alias": "session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if request.json["school"]["primary_contact"]:
		organisation = False
		school = True

		school_details_list = [
			{"field": "primary_contact", "alias": "Primary contact name"},
			{"field": "primary_contact_phone", "alias": "Primary contact phone"},
			{"field": "primary_contact_email", "alias": "Primary contact email address"}
		]

		messages = messages + fieldValidation(request.json["school"], school_details_list)

		try:
			first_name = request.json["school"]["primary_contact"].split(" ")[0].title()
		except Exception as e:
			first_name = None

		try:
			last_name = request.json["school"]["primary_contact"].split(" ")[1].title()
		except Exception as e:
			last_name = ""

		phone = request.json["school"]["primary_contact_phone"]
		email = request.json["school"]["primary_contact_email"].lower()

		get_school = db.session.query(School)\
							   .filter(School.deletion_marker == None)\
							   .filter(School.school_public_id == request.json["school"]["school_id"])\
							   .first()

		city = get_school.closest_city_town

	else:
		organisation = True
		school = False

		org_details_list = [
			{"field": "organisation_name", "alias": "Organisation name"},
			{"field": "organisation_email", "alias": "Organisation email address"},
			{"field": "organisation_type", "alias": "Organisation type"},
			{"field": "name"},
			{"field": "email", "alias": "Email address"},
			{"field": "phone"}
		]

		messages = messages + fieldValidation(request.json, org_details_list)
		
		try:
			first_name = request.json["name"].split(" ")[0].title()
		except Exception:
			first_name = None

		try:
			last_name = request.json["name"].split(" ")[1].title()
		except Exception:
			last_name = ""

		phone = request.json["phone"]
		email = request.json["email"].lower()
		city = None

	if messages:
		return jsonify({"messages": messages}), 422
	
	today = datetime.now().strftime("%Y-%m-%d")
	check_in_date = GenerateDateFromString.generateDate(request.json["check_in"])
	check_out_date = GenerateDateFromString.generateDate(request.json["check_out"])
	
	temp_date_diff = DateOperations.returnDateDifferenceInDays(check_out_date, check_in_date)

	if temp_date_diff == 0:
		date_diff = 1
	elif temp_date_diff > 0:
		date_diff = temp_date_diff

	exchange_rate_data = requests.get(get_latest_exchange_rate)
	
	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == request.json["booking_id"])\
							   .first()

	if not return_booking:
		message = []
		message.append("The selected booking is not in the system.")
		return jsonify({"message": message}), 412

	if return_booking.currency != request.json["currency_id"]:
		message = []
		message.append("The updated booking currency does not match the original booking currency.")
		return jsonify({"message": message}), 422

	# From return_booking query
	booking_id = return_booking.booking_public_id
	booking_ref = return_booking.booking_ref_code
	booking_done_by = return_booking.booking_done_by
	
	recipient_email = email
	
	try:
		send_email = request.json["send_mail"]
	except Exception:
		send_email = None

	phone_validation = validatePhoneNumber(phone)

	if phone_validation[0]:
		phone = phone_validation[1]
	
	else:
		message = []
		message.append(phone_validation[1])
		return jsonify({"message": message}), 422

	valid_email = validateEmail(email)

	if not valid_email:
		message = []
		message.append("The email address provided is invalid.")
		return jsonify({"message": message}), 422

	try:
		promo_code_json = request.json["promo_code"]

	except Exception:
		promo_code_json = None

	if promo_code_json:
		try:
			check_promo_validity = requests.post(promo_code_search, json = {"promo_code": promo_code_json})

			if check_promo_validity.status_code != 200:
				return jsonify({"message": check_promo_validity.json()["message"]}), 422
				
			elif check_promo_validity.status_code == 200:
				promo_code = promo_code_json
				promo_discount = int(check_promo_validity.json()["data"][0]["percentage_off"])
				promo_code_public_id = check_promo_validity.json()["data"][0]["public_id"]
		
		except Exception:
			message = []
			message.append("There was an issue getting the promo code details. Please try again or leave the promo code field blank.")
			return jsonify({"message": message}), 422

	else:
		promo_code = None
		promo_discount = None
		promo_code_public_id = None
	
	get_gatepass = db.session.query(Gatepass)\
							 .filter(Gatepass.status != get_booking_status_id("Updated"))\
							 .filter(Gatepass.booking_id == booking_id)\
							 .first()

	# From get_gatepass query
	old_gatepass_id = get_gatepass.gatepass_public_id
	
	get_gatepass_vehicles = db.session.query(GatepassVehicle)\
									  .filter(GatepassVehicle.gatepass_id == old_gatepass_id)\
									  .filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
									  .all()

	for single_vehicle in get_gatepass_vehicles:
		single_vehicle.status = get_booking_status_id("Updated")
		single_vehicle.updated_at = datetime.now()

	get_gatepass_guests = db.session.query(GatepassGuest)\
									.filter(GatepassGuest.gatepass_id == old_gatepass_id)\
									.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									.all()

	for single_guest in get_gatepass_guests:
		single_guest.status = get_booking_status_id("Updated")
		single_guest.updated_at = datetime.now()

	get_gatepass_details = db.session.query(GatepassDetail)\
									 .filter(GatepassDetail.gatepass_id == old_gatepass_id)\
									 .filter(GatepassDetail.status != get_booking_status_id("Updated"))\
									 .all()

	for single_gate_detail in get_gatepass_details:
		single_gate_detail.status = get_booking_status_id("Updated")
		single_gate_detail.updated_at = datetime.now()

	get_booking_details = db.session.query(Detail)\
									.filter(Detail.booking_id == booking_id)\
									.filter(Detail.status != get_booking_status_id("Updated"))\
									.all()

	for single_book_detail in get_booking_details:
		single_book_detail.status = get_booking_status_id("Updated")
		single_book_detail.updated_at = datetime.now()

	get_booking_guests = db.session.query(BookingGuest)\
								   .filter(BookingGuest.booking_id == booking_id)\
								   .filter(BookingGuest.status != get_booking_status_id("Updated"))\
								   .all()

	for single_book_guest in get_booking_guests:
		single_book_guest.status = get_booking_status_id("Updated")
		single_book_guest.updated_at = datetime.now()

	old_facility_list_ui = []
	old_facility_list_db = []

	for facility_old_single in request.json["old_facilities"]:
		old_facility_list_ui.append(facility_old_single["facility_booking_public_id"])
	
	get_booking_accommodation = db.session.query(Facility)\
										  .filter(Facility.booking_id == booking_id)\
										  .filter(Facility.deletion_marker == None)\
										  .filter(Facility.status != get_booking_status_id("Updated"))\
										  .filter(Facility.status != get_booking_status_id("Cancelled"))\
										  .all()

	for facility_old_db_single in get_booking_accommodation:
		old_facility_list_db.append(facility_old_db_single.facility_booking_public_id)

	for single_old_facility in old_facility_list_db:
		if single_old_facility in old_facility_list_ui:
			pass
		else:
			get_accommodation = db.session.query(Facility)\
										  .filter(Facility.booking_id == booking_id)\
										  .first()

			get_accommodation.status = get_booking_status_id("Updated")
			get_accommodation.updated_at = datetime.now()
	
	old_inventory_list_ui = []
	old_inventory_list_db = []

	for inventory_old_single in request.json["old_inventory"]:
		old_inventory_list_ui.append(inventory_old_single["inventory_booking_public_id"])
	
	get_booking_activity = db.session.query(Inventory)\
									 .filter(Inventory.booking_id == booking_id)\
									 .filter(Inventory.deletion_marker == None)\
									 .filter(Inventory.status != get_booking_status_id("Updated"))\
									 .filter(Inventory.status != get_booking_status_id("Cancelled"))\
									 .all()

	for inventory_old_db_single in get_booking_activity:
		old_inventory_list_db.append(inventory_old_db_single.inventory_booking_public_id)

	for single_old_inventory in old_inventory_list_db:
		if single_old_inventory in old_inventory_list_ui:
			pass
		else:
			get_activity = db.session.query(Inventory)\
									 .filter(Inventory.inventory_booking_public_id == single_old_inventory)\
									 .first()

			get_activity.status = get_booking_status_id("Updated")
			get_activity.updated_at = datetime.now()

	# From get_gatepass query
	get_gatepass.status = get_booking_status_id("Updated")
	get_gatepass.updated_at = datetime.now()

	for gatepass_guest in request.json["guests"]:
		get_guest_type = db.session.query(Mandatory)\
									.filter(Mandatory.deletion_marker == None)\
									.filter(Mandatory.payment_public_id == gatepass_guest["payment_public_id"])\
									.first()
		
		try:
			guest_count = int(gatepass_guest["payment_guests"])
		except Exception:
			guest_count = 0
		
		guest = BookingGuest(
			booking_guest_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			guest_type = get_booking_guest_type_id(get_guest_type.payment_person.lower()),
			guest_count = guest_count,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(guest)
	
	detail = Detail(
		booking_details_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		first_name = first_name,
		last_name = last_name,
		email_address = email,
		phone_number = phone,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(detail)

	try:
		destination = request.json["destination"]
	except (KeyError) as destination_error:
		destination = None
	
	# New gatepass ID
	gatepass_id = str(uuid.uuid4())

	gatepass = Gatepass(
		gatepass_public_id = gatepass_id,
		gatepass_date = today,
		gatepass_done_by = first_name + " " + last_name,
		destination = destination,
		gatepass_phone_number = phone,
		gatepass_ref_code = str(uuid.uuid4())[:10],
		booking_id = booking_id,
		booking = 1,
		start_date = request.json["check_in"],
		end_date = request.json["check_out"],
		gatepass_currency = request.json["currency_id"],
		gatepass_payment_status = 1,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(gatepass)
	
	for each_gatepass_guest in request.json["guests"]:
		get_gatepass_fee = db.session.query(MandatoryPaymentPrices)\
									 .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
									 .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
												  MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
												  Mandatory.payment_person)\
									 .filter(MandatoryPaymentPrices.deletion_marker == None)\
									 .filter(MandatoryPaymentPrices.payment_schedule == each_gatepass_guest["gatepass_payment_schedule"])\
									 .filter(MandatoryPaymentPrices.payment_category == each_gatepass_guest["payment_public_id"])\
									 .first()

		try:
			gatepass = get_gatepass_fee.payment_price
		except (ValueError, TypeError) as no_value:
			gatepass = 0
		
		gatepass_amount = currencyHandler(request.json["currency_id"], get_gatepass_fee.payment_currency, gatepass)

		get_ex_rate = requests.get(get_buy_sell_rate.format(get_gatepass_fee.payment_currency))

		try:
			buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
			selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

		except Exception:
			buying = 1
			selling = 1
		
		if promo_discount:
			person_discount = promo_discount
		else:
			try:
				person_discount = float(each_gatepass_guest["payment_person_discount"])
			except Exception:
				person_discount = 0

		if promo_discount:
			person_discount_reason = promo_code
		else:
			try:
				person_discount_reason = each_gatepass_guest["reason"]
			except Exception:
				person_discount_reason = None
		
		try:
			guests = int(each_gatepass_guest["payment_guests"])
		except Exception:
			guests = 0
		
		gatepass_guest = GatepassGuest(
			gatepass_guest_public_id = str(uuid.uuid4()),
			gatepass_id = gatepass_id,
			gatepass_guest_type = each_gatepass_guest["payment_public_id"],
			gatepass_guest_count = guests,
			gatepass_discount_rate = person_discount,
			gatepass_discount_reason = each_gatepass_guest["payment_person_discount_reason"],
			gatepass_cost_per_pp = math.ceil(round(gatepass_amount, 2)),
			gatepass_payment_schedule = each_gatepass_guest["gatepass_payment_schedule"],
			gatepass_no_of_nights = date_diff,
			gatepass_currency = request.json["currency_id"],
			gatepass_guest_cost_at_time = round(gatepass),
			gatepass_guest_currency_at_time = get_gatepass_fee.payment_currency,
			gatepass_guest_rate_at_time = buying,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(gatepass_guest)

	gatepass_detail = GatepassDetail(
		gatepass_details_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		first_name = first_name,
		last_name = last_name,
		email_address = email,
		phone_number = phone,
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)
	
	db.session.add(gatepass_detail)

	for each_vehicle in request.json["vehicles"]:
		get_cost = db.session.query(Vehicle)\
							 .filter(Vehicle.deletion_marker == None)\
							 .filter(Vehicle.vehicle_charge_public_id == each_vehicle["vehicle_charge_public_id"])\
							 .first()

		try:
			vehicle = get_cost.vehicle_charge_category_cost
		except (ValueError, TypeError) as no_value:
			vehicle = 0
		
		vehicle_fee = currencyHandler(request.json["currency_id"], get_cost.vehicle_charge_cost_currency, vehicle)

		get_ex_rate = requests.get(get_buy_sell_rate.format(get_cost.vehicle_charge_cost_currency))

		try:
			buying = get_ex_rate.json()["data"][0]["currency_buy_amount"]
			selling = get_ex_rate.json()["data"][0]["currency_sell_amount"]

		except Exception:
			buying = 1
			selling = 1

		if promo_discount:
			vehicle_discount = promo_discount
		else:
			try:
				vehicle_discount = float(each_vehicle["discount"])
			except Exception:
				vehicle_discount = 0

		if promo_code:
			vehicle_discount_reason = promo_code
		else:
			try:
				vehicle_discount_reason = each_vehicle["discount_reason"]
			except Exception:
				vehicle_discount_reason = None
		
		try:
			vehicles = int(each_vehicle["vehicles"])
		except Exception:
			vehicles = 0
		
		gatepass_vehicle = GatepassVehicle(
			gatepass_vehicle_public_id = str(uuid.uuid4()),
			gatepass_id = gatepass_id,
			gatepass_vehicle_type = each_vehicle["vehicle_charge_public_id"],
			gatepass_vehicle_count = vehicles,
			gatepass_cost_per_vehicle = round(vehicle_fee),
			gatepass_vehicle_currency = request.json["currency_id"],
			gatepass_vehicle_no_of_nights = date_diff,
			gatepass_vehicle_discount_rate = vehicle_discount,
			gatepass_vehicle_discount_reason = vehicle_discount_reason,
			gatepass_vehicle_cost_at_time = round(vehicle),
			gatepass_vehicle_currency_at_time = get_cost.vehicle_charge_cost_currency,
			gatepass_vehicle_rate_at_time = buying,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(gatepass_vehicle)
	
	booking_info = {}
	booking_info["booking_id"] = booking_id
	booking_info["booking_ref"] = booking_ref
	booking_info["first_name"] = first_name
	booking_info["last_name"] = last_name
	
	## Facility booking handler
	facility_email_data = []
	try:
		facilityBooking(request.json, booking_info, facility_email_data, currency_id = True)
	except Exception as e:
		error_tuple = sys.exc_info()
		trace = traceback.format_exc()
		return jsonify({"message": str(e), "trace": trace}), 422

	## Activity booking handler
	inventory_email_data = []

	try:
		inventoryBooking(request.json, booking_info, inventory_email_data, currency_id = True)
	except Exception as e:
		error_tuple = sys.exc_info()
		trace = traceback.format_exc()
		return jsonify({"message": str(e), "trace": trace}), 422

	get_booking_exchange_rate = requests.get(get_buy_sell_rate.format(request.json["currency_id"]))
	booking_buying_rate = get_booking_exchange_rate.json()["data"][0]["currency_buy_amount"]
	booking_selling_rate = get_booking_exchange_rate.json()["data"][0]["currency_sell_amount"]
	
	return_booking.booking_done_by = first_name + " " + last_name
	return_booking.currency = request.json["currency_id"]
	return_booking.currency_buying_rate_at_time = booking_buying_rate
	return_booking.currency_selling_rate_at_time = booking_selling_rate
	return_booking.booking_check_in_date = request.json["check_in"]
	return_booking.booking_check_out_date = request.json["check_out"]
	return_booking.status = get_booking_status_id("Updated")
	return_booking.updated_at = datetime.now()
	
	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		booking_activity_description = "Booking updated",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	if request.json["note"]:
		new_note = Note(
			booking_notes_public_id = str(uuid.uuid4()),
			booking_id = booking_id,
			note = request.json["note"],
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(new_note)

	try:
		db.session.commit()
		close(db)

		if send_email:
			email_data = {}
			email_data["subject"] = "Booking ID #" + booking_ref + " Update"
			email_data["recipient"] = [recipient_email]
			email_data["sender"] = "reservations@olpejetaconservancy.org"
			email_data["booking_ref_code"] = booking_ref
			email_data["done_by"] = booking_done_by
			email_data["booking_id"] = booking_id
			
			send_booking_update_email(email_data)

		message = []
		message.append("Successfully updated the booking.")
		return jsonify({"message": message}), 200

	except Exception:
		db.session.rollback()
		close(db)

		trace = traceback.format_exc()

		message = []
		message.append("There was an error updating the booking. Please try again later.")
		return jsonify({"message": message, "trace": trace}), 422


#############
#### Ref ####
#############
def get_booking_status_id(status_name):
	status_id = db.session.query(BookingStatus)\
						  .filter(BookingStatus.deletion_marker == None)\
						  .filter(BookingStatus.booking_status_name == status_name)\
						  .all()

	for single in status_id:
		booking_status_id = single.booking_status_public_id

		return booking_status_id


def get_booking_guest_type_id(guest_type_name):
	type_id = db.session.query(GuestType)\
						.filter(GuestType.deletion_marker == None)\
						.filter(GuestType.booking_guest_type_name == guest_type_name)\
						.all()

	for single in type_id:
		booking_guest_type_id = single.booking_guest_type_public_id

		return booking_guest_type_id
