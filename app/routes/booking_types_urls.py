from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed
from database.booking_types import BookingType


def close(self):
	self.session.close()


#######################
#### Booking Types ####
#######################
@app.route("/bookings/types/new", methods = ["POST"])
def add_new_booking_type():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")
	
	try:
		request.json["description"].strip()
		if not request.json["description"]:
			messages.append("Description is empty.")
	except KeyError as e:
		messages.append("Description is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	booking_type = BookingType(
		booking_type_public_id = str(uuid.uuid4()),
		booking_type_name = request.json["name"],
		booking_type_description = request.json["description"],
		booking_type_public = "1",
		booking_type_back = "1",
		session_id = request.json["session_id"],
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(booking_type)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The booking type has been added. Please carry on.")
		return jsonify({"message": output}), 201
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("It's us, not you. There was an error while adding the booking type. :-( Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/bookings/types/view")
def view_all_booking_types():
	if db.session.query(BookingType)\
				 .filter(BookingType.deletion_marker==None)\
				 .filter(BookingType.booking_type_public_id == "7769748C")\
				 .count() == 0:
		BookingTypeSeed.seed_default_booking_types_methods()
	
	if db.session.query(BookingType)\
				.filter(BookingType.deletion_marker==None)\
				.count() == 0:
		output = []
		output.append("There are currently no booking types in the system.")
		return jsonify({"message": output}), 200

	return_booking_types = db.session.query(BookingType)\
									 .filter(BookingType.deletion_marker==None)\
									 .order_by(BookingType.booking_type_name.asc())\
									 .all()

	output = []

	for single in return_booking_types:
		return_data = {}
		return_data["booking_type_public_id"] = single.booking_type_public_id
		return_data["booking_type_name"] = single.booking_type_name
		return_data["booking_type_description"] = single.booking_type_description
		return_data["booking_type_public"] = single.booking_type_public
		return_data["booking_type_back"] = single.booking_type_back
		return_data["session_id"] = single.session_id
		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/types/view/<booking_type_id>")
def view_single_booking_type(booking_type_id):
	if db.session.query(BookingType)\
				.filter(BookingType.deletion_marker==None)\
				.filter(BookingType.booking_type_public_id==booking_type_id)\
				.count() == 0:
		output = []
		output.append("That booking type does not exist in the system.")
		return jsonify({"message": output}), 200

	return_booking_types = db.session.query(BookingType)\
									 .filter(BookingType.deletion_marker==None)\
									 .filter(BookingType.booking_type_public_id==booking_type_id)\
									 .all()

	output = []

	for single in return_booking_types:
		return_data = {}
		return_data["booking_type_public_id"] = single.booking_type_public_id
		return_data["booking_type_name"] = single.booking_type_name
		return_data["booking_type_description"] = single.booking_type_description
		return_data["booking_type_public"] = single.booking_type_public
		return_data["booking_type_back"] = single.booking_type_back
		return_data["session_id"] = single.session_id
		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/types/modify", methods = ["PATCH"])
def edit_booking_type():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")
	
	try:
		request.json["type_id"].strip()
		if not request.json["type_id"]:
			messages.append("Type ID is empty.")
	except KeyError as e:
		messages.append("Type ID is missing.")
	
	try:
		request.json["description"].strip()
		if not request.json["description"]:
			messages.append("Description is empty.")
	except KeyError as e:
		messages.append("Description is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	return_booking_type = db.session.query(BookingType)\
									.filter(BookingType.deletion_marker==None)\
									.filter(BookingType.booking_type_public_id==request.json["type_id"])\
									.first()
	
	if not return_booking_type:
		output = []
		output.append("That booking type does not exist in the system.")
		return jsonify({"message": output}), 200
	
	else:
		return_booking_type.booking_type_name = request.json["name"]
		return_booking_type.booking_type_description = request.json["description"]
		return_booking_type.session_id = request.json["session_id"]
		return_booking_type.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			output = []
			output.append("The booking type has been editied. Please carry on.")
			return jsonify({"message": output}), 201
		except Exception as e:
			print(e)
			db.session.rollback()
			close(db)

			output = []
			output.append("It's us, not you. There was an error while editing the booking type. :-( Please try again later.")
			return jsonify({"message": output}), 422


@app.route("/bookings/types/delete", methods = ["PATCH"])
def delete_single_booking_types():
	messages = []
	
	try:
		request.json["type_id"].strip()
		if not request.json["type_id"]:
			messages.append("Type ID is empty.")
	except KeyError as e:
		messages.append("Type ID is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	return_booking_type = db.session.query(BookingType)\
									.filter(BookingType.deletion_marker==None)\
									.filter(BookingType.booking_type_public_id==request.json["type_id"])\
									.first()
	
	if not return_booking_type:
		output = []
		output.append("That booking type does not exist in the system.")
		return jsonify({"message": output}), 200

	else:
		return_booking_type.session_id = request.json["session_id"]
		return_booking_type.deletion_marker = "1"
		return_booking_type.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			output = []
			output.append("The booking type has been deleted successfully.")
			return jsonify({"message": output}), 200
		except Exception as e:
			print(e)
			db.session.rollback()
			close(db)

			output = []
			output.append("There was a slight issue deleting the booking type. :-( Please try again later.")
			return jsonify({"message": output}), 422