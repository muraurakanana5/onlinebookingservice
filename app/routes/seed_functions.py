from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid

# File imports
from routes import db
from database.booking_types import BookingType
from database.payment_methods import PaymentMethod
from database.booking_status import BookingStatus
from database.mandatory_payments import Mandatory
from database.vehicle import Vehicle
from database.booking_guest_types import GuestType
from database.gate import Gate
from database.facility_pricing_type import FacilityPricing

def close(self):
	self.session.close()

############################
#### Booking Types Seed ####
############################
class BookingTypeSeed():
    def seed_default_booking_types_methods():
        output = []

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "PB001A20")\
                    .count() == 0:
            partner_booking = BookingType(
                booking_type_public_id = "PB001A20",
                booking_type_name = "Partner Booking",
                booking_type_description = "A booking made by a partner.",
                booking_type_back = "1",
                session_id = "Admin"
            )

            db.session.add(partner_booking)
            output.append("Partner Booking has been added to the database.")

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "GB601X10")\
                    .count() == 0:
        
            guest_booking = BookingType(
                booking_type_public_id = "GB601X10",
                booking_type_name = "Guest Booking",
                booking_type_description = "A guest booking.",
                booking_type_public = "1",
                booking_type_back = "1",
                session_id = "Admin"
            )

            db.session.add(guest_booking)
            output.append("Guest Booking has been added to the database.")

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "VB938L19")\
                    .count() == 0:
        
            volunteer = BookingType(
                booking_type_public_id = "VB938L19",
                booking_type_name = "Volunteer",
                booking_type_description = "Volunteer booking.",
                booking_type_public = "1",
                booking_type_back = "1",
                session_id = "Admin"
            )

            db.session.add(volunteer)
            output.append("Volunteer has been added to the database.")

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "FF9330A3")\
                    .count() == 0:
        
            volunteer = BookingType(
                booking_type_public_id = "FF9330A3",
                booking_type_name = "Front Office Booking",
                booking_type_description = "Front Office Booking booking.",
                booking_type_public = "1",
                booking_type_back = "1",
                session_id = "Admin"
            )

            db.session.add(volunteer)
            output.append("Front Office Booking has been added to the database.")

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "8D7E6504")\
                    .count() == 0:
        
            school = BookingType(
                booking_type_public_id = "8D7E6504",
                booking_type_name = "School",
                booking_type_description = "School booking.",
                booking_type_group = "1",
                session_id = "Admin"
            )

            db.session.add(school)
            output.append("School has been added to the database.")

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "C3237489")\
                    .count() == 0:
        
            corporate = BookingType(
                booking_type_public_id = "C3237489",
                booking_type_name = "Corporate",
                booking_type_description = "Corporate booking.",
                booking_type_group = "1",
                session_id = "Admin"
            )

            db.session.add(corporate)
            output.append("Corporate has been added to the database.")

        if db.session.query(BookingType)\
                    .filter(BookingType.deletion_marker==None)\
                    .filter(BookingType.booking_type_public_id == "7769748C")\
                    .count() == 0:
        
            member = BookingType(
                booking_type_public_id = "7769748C",
                booking_type_name = "Member Booking",
                booking_type_description = "Member booking.",
                booking_type_back = "1",
                session_id = "Admin"
            )

            db.session.add(member)
            output.append("Member has been added to the database.")

        try:
            db.session.commit()
            close(db)

            output.append("The booking types table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the booking types table. :-( Please try again later.")
            return jsonify({"message": output}), 422


#############################
#### Booking Status Seed ####
#############################
class BookingStatusSeed():
    def seed_default_booking_status():
        output = []

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "d0c9dc58-4bd7-4279-85e1-70b8b1632f02")\
                    .count() == 0:
            confirmed = BookingStatus(
                booking_status_public_id = "d0c9dc58-4bd7-4279-85e1-70b8b1632f02",
                booking_status_name = "Confirmed",
                booking_status_description = "A confirmed order.",
                session_id = "Admin"
            )

            db.session.add(confirmed)
            output.append("Confirmed has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "4161804a-846b-438d-b969-ae2af85fd4c8")\
                    .count() == 0:
        
            unconfirmed = BookingStatus(
                booking_status_public_id = "4161804a-846b-438d-b969-ae2af85fd4c8",
                booking_status_name = "Unconfirmed",
                booking_status_description = "An unconfirmed booking.",
                session_id = "Admin"
            )

            db.session.add(unconfirmed)
            output.append("Unconfirmed has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "17e163c2-cbcf-4323-9606-609f0015fb65")\
                    .count() == 0:
        
            abandoned = BookingStatus(
                booking_status_public_id = "17e163c2-cbcf-4323-9606-609f0015fb65",
                booking_status_name = "Abandoned",
                booking_status_description = "An abandoned booking.",
                session_id = "Admin"
            )

            db.session.add(abandoned)
            output.append("Abandoned has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "626f13f2-aab0-4488-a774-44969b3062a3")\
                    .count() == 0:
        
            cancelled = BookingStatus(
                booking_status_public_id = "626f13f2-aab0-4488-a774-44969b3062a3",
                booking_status_name = "Cancelled",
                booking_status_description = "A cancelled booking.",
                session_id = "Admin"
            )

            db.session.add(cancelled)
            output.append("Cancelled has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "d57587b6-24da-40a6-a4d1-fe41972dc936")\
                    .count() == 0:
        
            pending = BookingStatus(
                booking_status_public_id = "d57587b6-24da-40a6-a4d1-fe41972dc936",
                booking_status_name = "Pending Payment",
                booking_status_description = "A booking that is yet to be paid for.",
                session_id = "Admin"
            )

            db.session.add(pending)
            output.append("Pending Payment has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "3216010b-7c03-48b4-b82e-6c8ea5e3096b")\
                    .count() == 0:
        
            no_show = BookingStatus(
                booking_status_public_id = "3216010b-7c03-48b4-b82e-6c8ea5e3096b",
                booking_status_name = "No-Show",
                booking_status_description = "A booking that is yet to be checked in past the expected check-in date.",
                session_id = "Admin"
            )

            db.session.add(no_show)
            output.append("No-Show has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "693afa26-7429-4a79-afbf-e44e513663df")\
                    .count() == 0:
        
            updated = BookingStatus(
                booking_status_public_id = "693afa26-7429-4a79-afbf-e44e513663df",
                booking_status_name = "Updated",
                booking_status_description = "A booking that has been updated.",
                session_id = "Admin"
            )

            db.session.add(updated)
            output.append("Updated has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "3b5376e0-a4dc-476e-aebc-6280b44b756a")\
                    .count() == 0:
        
            invoice = BookingStatus(
                booking_status_public_id = "3b5376e0-a4dc-476e-aebc-6280b44b756a",
                booking_status_name = "To Invoice",
                booking_status_description = "A booking that is to be invoiced",
                session_id = "Admin"
            )

            db.session.add(invoice)
            output.append("To Invoice has been added to the database.")

        if db.session.query(BookingStatus)\
                    .filter(BookingStatus.deletion_marker==None)\
                    .filter(BookingStatus.booking_status_public_id == "180e751e-a70f-4052-8119-0dffd938c2c9")\
                    .count() == 0:
        
            deposit = BookingStatus(
                booking_status_public_id = "180e751e-a70f-4052-8119-0dffd938c2c9",
                booking_status_name = "Deposit",
                booking_status_description = "A booking that has had an incomplete payment made.",
                session_id = "Admin"
            )

            db.session.add(deposit)
            output.append("Deposit has been added to the database.")

        try:
            db.session.commit()
            close(db)

            output.append("The booking status table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the booking status table. :-( Please try again later.")
            return jsonify({"message": output}), 422


##############################
#### Payment Methods Seed ####
##############################
class PaymentMethodSeed():
    def seed_default_payment_methods():
        output = []

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "73414fc2-5bf7-42d1-9cdf-08fee8a0b08e")\
                    .count() == 0:
            card_method = PaymentMethod(
                payment_method_public_id = "73414fc2-5bf7-42d1-9cdf-08fee8a0b08e",
                payment_method_name = "Credit Card",
                payment_method_description = "Means of payment by credit card",
                payment_method_guest = "1",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(card_method)
            output.append("Credit Card has been added to the database.")

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "cbff45f3-4f12-41da-8b4a-bf308112f032")\
                    .count() == 0:
        
            mpesa_method = PaymentMethod(
                payment_method_public_id = "cbff45f3-4f12-41da-8b4a-bf308112f032",
                payment_method_name = "Mpesa",
                payment_method_description = "Mobile money payment method.",
                payment_method_guest = "1",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(mpesa_method)
            output.append("Mpesa has been added to the database.")

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "746bb582-89ed-4c83-ac3c-f25d9d60b266")\
                    .count() == 0:
        
            bank_transfer_method = PaymentMethod(
                payment_method_public_id = "746bb582-89ed-4c83-ac3c-f25d9d60b266",
                payment_method_name = "Bank Transfer",
                payment_method_description = "Means of payment by bank transfer.",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(bank_transfer_method)
            output.append("Bank Transfer has been added to the database.")

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "a4e3aeb3-0937-49d5-b3dd-87b8c2f2654b")\
                    .count() == 0:
        
            bank_transfer_method = PaymentMethod(
                payment_method_public_id = "a4e3aeb3-0937-49d5-b3dd-87b8c2f2654b",
                payment_method_name = "Other",
                payment_method_description = "Miscellaneous payment method.",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(bank_transfer_method)
            output.append("Other has been added to the database.")

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "4c6a9513-ea69-437e-9c3a-32a7afc1706a")\
                    .count() == 0:
        
            mpesa_method = PaymentMethod(
                payment_method_public_id = "4c6a9513-ea69-437e-9c3a-32a7afc1706a",
                payment_method_name = "Split (Two Different Payment Methods)",
                payment_method_description = "A mix of two payment methods.",
                payment_method_guest = "1",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(mpesa_method)
            output.append("Split (Two Different Payment Methods) has been added to the database.")

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "94a91336-3bac-4e2f-8881-2887d27e4d61")\
                    .count() == 0:
        
            bank_transfer_method = PaymentMethod(
                payment_method_public_id = "94a91336-3bac-4e2f-8881-2887d27e4d61",
                payment_method_name = "Inclusive",
                payment_method_description = "N/A.",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(bank_transfer_method)
            output.append("Inclusive has been added to the database.")

        if db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.deletion_marker==None)\
                    .filter(PaymentMethod.payment_method_public_id == "1e62ecfc-6a3a-4112-835c-4dc3fe8e28c7")\
                    .count() == 0:
        
            bank_transfer_method = PaymentMethod(
                payment_method_public_id = "1e62ecfc-6a3a-4112-835c-4dc3fe8e28c7",
                payment_method_name = "Prepaid",
                payment_method_description = "N/A.",
                payment_method_partner = "1",
                session_id = "Admin"
            )

            db.session.add(bank_transfer_method)
            output.append("Prepaid has been added to the database.")

        try:
            db.session.commit()
            close(db)

            output.append("The payment methods table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the payment methods table. :-( Please try again later.")
            return jsonify({"message": output}), 422


class MandatoryPaymentsSeed():
    def seed_default_mandatory_payments():
        output = []

        if db.session.query(Mandatory)\
                    .filter(Mandatory.deletion_marker==None)\
                    .filter(Mandatory.payment_public_id == "cf347d6b-11e8-4b85-95bf-543da14e0bb3")\
                    .count() == 0:
            
            non_resident_adult = Mandatory(
                payment_public_id = "cdf8982a-3252-40ec-a49b-d752420eaa9b",
                payment_person = "Non Resident Adult",
                payment_person_currency = "USD",
                payment_person_amount = "85",
                marker = "A",
                maximum_number = 5000,
                session_id = "Admin"
            )

            db.session.add(non_resident_adult)
            output.append("Non Resident Adult has been added to the database.")

            non_resident_child = Mandatory(
                payment_public_id = "6ba37724-04df-4a1d-b475-c2915ec52a81",
                payment_person = "Non Resident Child",
                payment_person_currency = "USD",
                payment_person_amount = "42",
                marker = "C",
                maximum_number = 1500,
                session_id = "Admin"
            )

            db.session.add(non_resident_child)
            output.append("Non Resident Child has been added to the database.")

            non_resident_student = Mandatory(
                payment_public_id = "9ab7da60-6813-411a-9a00-f2d8a413c5bb",
                payment_person = "Non Resident Student",
                payment_person_currency = "USD",
                payment_person_amount = "21",
                marker = "C",
                maximum_number = 500,
                session_id = "Admin"
            )

            db.session.add(non_resident_student)
            output.append("Non Resident Student has been added to the database.")

            ea_resident_adult = Mandatory(
                payment_public_id = "b0dc8ab1-91e2-4474-ad29-9dcda9c30fc5",
                payment_person = "EA Resident Adult",
                payment_person_currency = "KES",
                payment_person_amount = "2200",
                marker = "A",
                maximum_number = 5000,
                session_id = "Admin"
            )

            db.session.add(ea_resident_adult)
            output.append("EA Resident Adult has been added to the database.")

            ea_resident_child = Mandatory(
                payment_public_id = "a0d24649-c085-4b5e-adec-d3f93f2e22d2",
                payment_person = "EA Resident Child",
                payment_person_currency = "KES",
                payment_person_amount = "1100",
                marker = "C",
                maximum_number = 1500,
                session_id = "Admin"
            )

            db.session.add(ea_resident_child)
            output.append("EA Resident Child has been added to the database.")

            ea_resident_student = Mandatory(
                payment_public_id = "18b25633-12bb-44da-b9f8-47dc629bace4",
                payment_person = "EA Resident Student",
                payment_person_currency = "KES",
                payment_person_amount = "550",
                marker = "C",
                maximum_number = 500,
                session_id = "Admin"
            )

            db.session.add(ea_resident_student)
            output.append("EA Resident Student has been added to the database.")

            citizen_adult = Mandatory(
                payment_public_id = "a8fe2f62-1671-41ff-a059-d80a5d1d3ea7",
                payment_person = "Citizen Adult",
                payment_person_currency = "KES",
                payment_person_amount = "1100",
                marker = "A",
                maximum_number = 5000,
                session_id = "Admin"
            )

            db.session.add(citizen_adult)
            output.append("Citizen Adult has been added to the database.")

            citizen_child = Mandatory(
                payment_public_id = "c1b66cb1-6c83-42c0-86f5-0db21d1b9f60",
                payment_person = "Citizen Child",
                payment_person_currency = "KES",
                payment_person_amount = "550",
                marker = "C",
                maximum_number = 1500,
                session_id = "Admin"
            )

            db.session.add(citizen_child)
            output.append("Citizen Child has been added to the database.")

            citizen_student = Mandatory(
                payment_public_id = "c7bf17ef-5d92-4cf7-866a-881fba828de3",
                payment_person = "Citizen Student",
                payment_person_currency = "KES",
                payment_person_amount = "275",
                marker = "C",
                maximum_number = 500,
                session_id = "Admin"
            )

            db.session.add(citizen_student)
            output.append("Citizen Student has been added to the database.")
            
            infant = Mandatory(
                payment_public_id = "cf347d6b-11e8-4b85-95bf-543da14e0bb3",
                payment_person = "Infant",
                payment_person_currency = "KES",
                payment_person_amount = "0",
                marker = "I",
                maximum_number = 0,
                session_id = "Admin"
            )

            db.session.add(infant)
            output.append("Infant has been added to the database.")

        try:
            db.session.commit()
            close(db)

            output.append("The mandatory payments table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the mandatory payments table. :-( Please try again later.")
            return jsonify({"message": output}), 422


##############################
#### Vehicle Charges Seed ####
##############################
class VehicleChargesSeed():
    def seed_default_vehicle_charges():
        output = []

        if db.session.query(Vehicle)\
                    .filter(Vehicle.deletion_marker == None)\
                    .filter(Vehicle.vehicle_charge_public_id == "d1954103-53af-4411-9550-1634d255e343")\
                    .count() == 0:
            vehicle_1 = Vehicle(
                vehicle_charge_public_id = "d1954103-53af-4411-9550-1634d255e343",
                vehicle_charge_category = "1 to 6 Seats",
                vehicle_charge_category_cost = 400,
                vehicle_charge_cost_currency = "162fface-f5f1-41de-913b-d2bb784dda3a",
                maximum_number = 200,
                session_id = "Admin"
            )

            db.session.add(vehicle_1)
            output.append("The vehicle class 1 to 6 seats has been seeded.")

        if db.session.query(Vehicle)\
                    .filter(Vehicle.deletion_marker == None)\
                    .filter(Vehicle.vehicle_charge_public_id == "e6473bfe-ccc1-4f96-850a-ae022eb4e87a")\
                    .count() == 0:
            vehicle_2 = Vehicle(
                vehicle_charge_public_id = "e6473bfe-ccc1-4f96-850a-ae022eb4e87a",
                vehicle_charge_category = "7 to 14 Seats",
                vehicle_charge_category_cost = 1200,
                vehicle_charge_cost_currency = "162fface-f5f1-41de-913b-d2bb784dda3a",
                maximum_number = 200,
                session_id = "Admin"
            )

            db.session.add(vehicle_2)
            output.append("The vehicle class 7 to 14 seats has been seeded.")

        if db.session.query(Vehicle)\
                    .filter(Vehicle.deletion_marker == None)\
                    .filter(Vehicle.vehicle_charge_public_id == "99330ddf-4bd6-4c5e-8844-66b6d7ba1134")\
                    .count() == 0:
            vehicle_3 = Vehicle(
                vehicle_charge_public_id = "99330ddf-4bd6-4c5e-8844-66b6d7ba1134",
                vehicle_charge_category = "15 Seats and Above",
                vehicle_charge_category_cost = 10000,
                vehicle_charge_cost_currency = "162fface-f5f1-41de-913b-d2bb784dda3a",
                maximum_number = 200,
                session_id = "Admin"
            )

            db.session.add(vehicle_3)
            output.append("The vehicle class 15 seats and above has been seeded.")

        try:
            db.session.commit()
            close(db)

            output.append("The vehicle charges table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the vehicle charges table. :-( Please try again later.")
            return jsonify({"message": output}), 422


##################################
#### Booking Guest Types Seed ####
##################################
class BookingGuestTypesSeed():
    def seed_default_booking_guest_types_methods():
        output = []

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "bd952f21")\
                    .count() == 0:
            ea_adult_citizen = GuestType(
                booking_guest_type_public_id = "bd952f21",
                booking_guest_type_name = "Citizen Adult",
                session_id = "Admin"
            )

            db.session.add(ea_adult_citizen)
            output.append("Citizen Adult has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "534613c1")\
                    .count() == 0:
            ea_adult_resident = GuestType(
                booking_guest_type_public_id = "534613c1",
                booking_guest_type_name = "EA Resident Adult",
                session_id = "Admin"
            )

            db.session.add(ea_adult_resident)
            output.append("EA Adult Resident has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "c78b0b7a")\
                    .count() == 0:
            non_resident_adult = GuestType(
                booking_guest_type_public_id = "c78b0b7a",
                booking_guest_type_name = "Non Resident Adult",
                session_id = "Admin"
            )

            db.session.add(non_resident_adult)
            output.append("Non Resident Adult has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "5d895853")\
                    .count() == 0:
            ea_child_citizen = GuestType(
                booking_guest_type_public_id = "5d895853",
                booking_guest_type_name = "Citizen Child",
                session_id = "Admin"
            )

            db.session.add(ea_child_citizen)
            output.append("Citizen Child has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "b2c2dbf0")\
                    .count() == 0:
            ea_child_resident = GuestType(
                booking_guest_type_public_id = "b2c2dbf0",
                booking_guest_type_name = "EA Resident Child",
                session_id = "Admin"
            )

            db.session.add(ea_child_resident)
            output.append("EA Child Resident has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "71f4f313")\
                    .count() == 0:
            non_resident_child = GuestType(
                booking_guest_type_public_id = "71f4f313",
                booking_guest_type_name = "Non Resident Child",
                session_id = "Admin"
            )

            db.session.add(non_resident_child)
            output.append("Non Resident Child has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "2df20a96")\
                    .count() == 0:
            non_resident_child = GuestType(
                booking_guest_type_public_id = "2df20a96",
                booking_guest_type_name = "Infant",
                session_id = "Admin"
            )

            db.session.add(non_resident_child)
            output.append("Infant has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "172775c1")\
                    .count() == 0:
            citizen_student = GuestType(
                booking_guest_type_public_id = "172775c1",
                booking_guest_type_name = "Citizen Student",
                session_id = "Admin"
            )

            db.session.add(citizen_student)
            output.append("Citizen Student has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "429bb1f4")\
                    .count() == 0:
            ea_resident_student = GuestType(
                booking_guest_type_public_id = "429bb1f4",
                booking_guest_type_name = "EA Resident Student",
                session_id = "Admin"
            )

            db.session.add(ea_resident_student)
            output.append("EA Resident Student has been added to the database.")

        if db.session.query(GuestType)\
                    .filter(GuestType.deletion_marker==None)\
                    .filter(GuestType.booking_guest_type_public_id == "9a6e34d2")\
                    .count() == 0:
            non_resident_student = GuestType(
                booking_guest_type_public_id = "9a6e34d2",
                booking_guest_type_name = "Non Resident Student",
                session_id = "Admin"
            )

            db.session.add(non_resident_student)
            output.append("Non Resident Student has been added to the database.")


####################
#### Gates Seed ####
####################
class GatesSeed():
    def seed_default_gates():
        output = []

        if db.session.query(Gate)\
                    .filter(Gate.deletion_marker==None)\
                    .filter(Gate.gate_public_id == "05e7ee16")\
                    .count() == 0:
            gate1 = Gate(
                gate_public_id = "05e7ee16",
                gate_name = "Golf 2",
                session_id = "Admin"
            )

            db.session.add(gate1)
            output.append("Golf 2 has been added to the database.")

        if db.session.query(Gate)\
                    .filter(Gate.deletion_marker==None)\
                    .filter(Gate.gate_public_id == "9ac19eb6")\
                    .count() == 0:
            gate2 = Gate(
                gate_public_id = "9ac19eb6",
                gate_name = "Golf 3",
                session_id = "Admin"
            )

            db.session.add(gate2)
            output.append("Golf 3 has been added to the database.")

        try:
            db.session.commit()
            close(db)

            output.append("The gates table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the gates table. :-( Please try again later.")
            return jsonify({"message": output}), 422


###############################
#### Facility Pricing Seed ####
###############################
class FacilityPricingTypeSeed():
    def seed_default_facility_pricing_types():
        output = []

        if db.session.query(FacilityPricing)\
                    .filter(FacilityPricing.deletion_marker==None)\
                    .filter(FacilityPricing.facility_pricing_type_public_id == "266677c3")\
                    .count() == 0:
            fully_catered = FacilityPricing(
                facility_pricing_type_public_id = "266677c3",
                facility_pricing_name = "Fully Catered",
                facility_pricing_special = None,
                session_id = "Admin"
            )

            db.session.add(fully_catered)
            output.append("Fully Catered has been added to the database.")
        
        if db.session.query(FacilityPricing)\
                    .filter(FacilityPricing.deletion_marker==None)\
                    .filter(FacilityPricing.facility_pricing_type_public_id == "9a9a77eb")\
                    .count() == 0:
            self_catered = FacilityPricing(
                facility_pricing_type_public_id = "9a9a77eb",
                facility_pricing_name = "Self-catered",
                facility_pricing_special = None,
                session_id = "Admin"
            )

            db.session.add(self_catered)
            output.append("Self-catered has been added to the database.")

        if db.session.query(FacilityPricing)\
                    .filter(FacilityPricing.deletion_marker==None)\
                    .filter(FacilityPricing.facility_pricing_type_public_id == "377788d4")\
                    .count() == 0:
            chef = FacilityPricing(
                facility_pricing_type_public_id = "377788d4",
                facility_pricing_name = "Self-catered plus Chef",
                facility_pricing_special = None,
                session_id = "Admin"
            )

            db.session.add(chef)
            output.append("Self-catered plus Chef has been added to the database.")
        
        try:
            db.session.commit()
            close(db)

            output.append("The facility pricing types table has been seeded. Please carry on.")
            return jsonify({"message": output}), 201
        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append("It's us, not you. There was an error while seeding the facility pricing types table. :-( Please try again later.")
            return jsonify({"message": output}), 422