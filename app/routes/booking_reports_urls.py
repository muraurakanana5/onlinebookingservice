from flask import jsonify, request
from datetime import datetime, timedelta, date
from collections import OrderedDict

import os, math, requests, uuid
import pdfkit
import sys
import traceback
import pandas as panda

# File imports
from routes import app, bookings_logger, receipt_print_options, format_fee
from routes import db
from routes import sessionTracking, ticketGenerator
from routes import db_cache, FromCache
from routes.bookings_urls import get_booking_status_id, currencyHandler
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed
from routes.seed_functions import BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_payments import BookingPayment
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.group import Group
from database.vehicle import Vehicle
from database.salesforce import SalesforceData
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.destination import Destination
from database.payment_methods import PaymentMethod
from database.payment_gateways import PaymentGateway
from database.booking_tickets import Ticket
from database.member import Member
from database.check_in_vehicles import CheckInVehicle
from database.contact_list import ContactList
from database.bookings_notes import Note
from database.invoice import Invoice
from database.mandatory_payment_prices import MandatoryPaymentPrices
from database.booking_activity_log import BookingActivity
from database.partner import Partner
from database.school import School
from database.school_booking import SchoolBooking
from variables import *
from functions.validation import *
from functions.date_operators import *
from functions.booking_snippets import bookingTotalGraph, bookingTotal
from sqlalchemy import or_


@app.route("/reports/gate-entry/today", methods=["POST"])
def generate_gate_entry_report():
    validation_list = [{"field": "session_id", "alias": "Session ID"}]

    messages = fieldValidation(request.json, validation_list)

    if messages:
        return jsonify({"messages": messages, "data": []}), 422

    get_gatepass_guests = db.session.query(GatepassGuest)\
      .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
      .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
      .add_columns(Mandatory.payment_person, Mandatory.payment_public_id, Mandatory.payment_person_income_code,\
       Mandatory.payment_person_dept_code,\
       GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
       GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_discount_reason, GatepassGuest.gatepass_guest_rate_at_time,\
       GatepassGuest.gatepass_guest_cost_at_time, GatepassGuest.gatepass_guest_currency_at_time,\
       GatepassGuest.gatepass_currency,\
       Gatepass.destination, Gatepass.start_date, Gatepass.booking_id)\
      .filter(GatepassGuest.deletion_marker == None)\
      .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
      .filter(GatepassGuest.status != get_booking_status_id("Deleted"))\
      .filter(GatepassGuest.gatepass_guest_count > 0)\
      .filter(Gatepass.start_date == datetime.now().strftime("%Y-%m-%d"))\
      .all()

    if not get_gatepass_guests:
        message = []
        message.append("There is no data to display for the date " +
                       datetime.now().strftime("%d-%m-%Y"))
        return jsonify({"message": message}), 422

    guest_data = []

    for single_guest in get_gatepass_guests:
        try:
            get_booking = db.session.query(Booking)\
               .join(Detail, Booking.booking_public_id == Detail.booking_id)\
               .add_columns(Booking.booking_ref_code,\
               Detail.first_name, Detail.last_name)\
               .filter(Booking.deletion_marker == None)\
               .filter(Booking.booking_public_id == single_guest.booking_id)\
               .filter(Detail.status != get_booking_status_id("Deleted"))\
               .filter(Detail.status != get_booking_status_id("Updated"))\
               .first()

            return_guest_data = OrderedDict()
            return_guest_data["Date"] = single_guest.start_date.strftime(
                "%d-%b-%y")
            return_guest_data["Booking_Ref"] = get_booking.booking_ref_code
            return_guest_data[
                "Client_Name"] = get_booking.first_name + " " + get_booking.last_name
            return_guest_data[
                "Conservancy_Fee_Category"] = single_guest.payment_person
            return_guest_data[
                "No_of_Guests"] = single_guest.gatepass_guest_count
            return_guest_data[
                "No_of_Nights"] = single_guest.gatepass_no_of_nights
            return_guest_data[
                "Receipts"] = single_guest.gatepass_guest_count * single_guest.gatepass_no_of_nights

            guest_currency = requests.get(
                get_currency.format(single_guest.gatepass_currency))
            return_guest_data["Currency"] = guest_currency.json(
            )["data"][0]["currency_name"]
            return_guest_data["Cost_per_Person"] = round(
                single_guest.gatepass_cost_per_pp)

            total_without_discount = float(
                single_guest.gatepass_cost_per_pp) * float(
                    single_guest.gatepass_no_of_nights) * float(
                        single_guest.gatepass_guest_count)
            total_with_discount = float(
                1 - (float(single_guest.gatepass_discount_rate) /
                     100)) * total_without_discount

            return_guest_data["Total_Cost"] = round(total_with_discount)

            if single_guest.destination:
                get_destination_details = db.session.query(Destination)\
                   .filter(Destination.gatepass_destination_public_id == single_guest.destination)\
                   .first()

                return_guest_data[
                    "Destination"] = get_destination_details.gatepass_destination_name

            else:
                return_guest_data["Destination"] = None

            guest_data.append(return_guest_data)

        except Exception:
            pass

    date_generated = datetime.now().strftime("%d-%m-%Y %H:%M:%S")

    filename = "reports/gate/GateEntryData" + date_generated + ".xlsx"

    writer = panda.ExcelWriter(filename)
    guest_data_df = panda.DataFrame(guest_data)
    guest_data_df.to_excel(writer, "Breakdown", index=False)

    table = panda.pivot_table(
        guest_data_df,
        index=[
            "Date", "Booking_Ref", "Conservancy_Fee_Category", "Destination",
            "Currency"
        ],
        values=["No_of_Guests", "Receipts", "Total_Cost"],
        aggfunc="sum")

    pivot_df = table.to_excel(writer, "Pivot Table")
    # for manager in table.index.get_level_values(0).unique():
    # 	pivot_df = table.xs(manager, level=0)
    # 	pivot_df.to_excel(writer, manager)

    writer.save()

    # file_location = """"

    return jsonify({
        "url": """/docs/" + filename,
        "data": guest_data
    }), 200


@app.route("/reports/details/today", methods=["POST"])
def generate_complete_entry_report():
    validation_list = [{"field": "session_id", "alias": "Session ID"}]
    messages = fieldValidation(request.json, validation_list)

    if messages:
        return jsonify({"messages": messages}), 422
    try:
        start_date_request = request.json["start_date"]
        end_date_request = request.json["end_date"]

    except Exception:
        # yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
        start_date_request = datetime.now()
        end_date_request = datetime.now()

    try:
        start_date = GenerateDateFromString.generateDate(
            request.json["start_date"])
        end_date = GenerateDateFromString.generateDate(
            request.json["end_date"])
        booking_type = request.json["booking_type"]

    except Exception:
        # yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
        start_date = datetime.now()
        end_date = datetime.now()
        booking_type = ''

    date_range = []
    while start_date <= end_date:
        date_range.append(start_date)

        start_date = start_date + timedelta(days=1)

    # for single_date in date_range:`
    # 	get_bookings = db.session.query(Booking)\
    # 							.filter(Booking.deletion_marker == None)\
    # 							.filter(Booking.status != get_booking_status_id("Deleted"))\
    # 							.filter(Booking.status != get_booking_status_id("Cancelled"))\
    # 							.filter(Booking.status != get_booking_status_id("Abandoned"))\
    # 							.filter(Booking.booking_check_in_date == single_date.strftime("%Y-%m-%d"))\
    # 							.all()

    # if not get_bookings:
    # 	message = []
    # 	message.append("There is no data to display for the date " + datetime.now().strftime("%d-%m-%Y"))
    # 	return jsonify({"message": message, "data": []}), 422

    data = []
    if booking_type == '1':
        get_bookings = db.session.query(Booking)\
           .filter(Booking.deletion_marker == None)\
           .filter(Booking.status != get_booking_status_id("Deleted"))\
           .filter(Booking.status != get_booking_status_id("Cancelled"))\
           .filter(Booking.status != get_booking_status_id("Abandoned"))\
           .filter(Booking.status != get_booking_status_id("Complimentary"))\
           .filter(Booking.booking_type != "7769748C")\
           .filter(Booking.booking_check_in_date.between(start_date_request, end_date_request))\
           .all()
    elif booking_type == "2":
        get_bookings = db.session.query(Booking)\
           .filter(Booking.deletion_marker == None)\
           .filter(Booking.status != get_booking_status_id("Deleted"))\
           .filter(Booking.status != get_booking_status_id("Cancelled"))\
           .filter(Booking.status != get_booking_status_id("Abandoned"))\
           .filter(Booking.status == get_booking_status_id("Complimentary"))\
           .filter(Booking.booking_check_in_date.between(start_date_request, end_date_request))\
           .all()

    else:
        get_bookings = db.session.query(Booking)\
            .filter(Booking.deletion_marker == None)\
            .filter(Booking.status != get_booking_status_id("Deleted"))\
            .filter(Booking.status != get_booking_status_id("Cancelled"))\
            .filter(Booking.status != get_booking_status_id("Abandoned"))\
            .filter(Booking.booking_check_in_date.between(start_date_request, end_date_request))\
            .all()

    for single_booking in get_bookings:
        booking_details = {}
        bookingTotal(booking_details, single_booking.booking_public_id, True)

        check_school_booking = db.session.query(SchoolBooking)\
           .join(School, SchoolBooking.school_id == School.school_public_id)\
           .add_columns(School.school_name)\
           .filter(SchoolBooking.deletion_marker == None)\
           .filter(SchoolBooking.booking_id == single_booking.booking_public_id)\
           .first()

        get_booking_gatepass_detail = db.session.query(Gatepass)\
            .join(Destination, Gatepass.destination == Destination.gatepass_destination_public_id)\
            .add_columns(Destination.gatepass_destination_name)\
            .filter(Gatepass.deletion_marker == None)\
            .filter(Gatepass.status != get_booking_status_id("Cancelled"))\
            .filter(Gatepass.status != get_booking_status_id("Updated"))\
            .filter(Gatepass.booking_id == single_booking.booking_public_id)\
            .first()

        if not get_booking_gatepass_detail:
            # destination_name = None
            destination_name = "N/A"
        else:
            destination_name = get_booking_gatepass_detail.gatepass_destination_name

        if check_school_booking:
            try:
                booking_done_by = check_school_booking.school_name
            except Exception:
                booking_done_by = single_booking.booking_done_by
        else:
            booking_done_by = single_booking.booking_done_by

        for single_guest in booking_details["guests"]:
            if single_guest["payment_guests"] > 0:
                if destination_name:
                    if destination_name == "Day Trip":
                        no_of_nights = 0
                    else:
                        no_of_nights = single_guest["no_of_nights"]
                else:
                    no_of_nights = single_guest["no_of_nights"]

                return_data = OrderedDict()
                return_data[
                    "Date"] = single_booking.booking_check_in_date.strftime(
                        "%d-%b-%y")
                return_data["Booking_Ref"] = single_booking.booking_ref_code
                return_data[
                    "Booking_Type"] = single_booking.return_booking_type()
                return_data["Client_Name"] = booking_done_by
                return_data["Item_Name"] = single_guest["payment_person"]
                return_data["Destination"] = destination_name
                return_data["Number"] = single_guest["payment_guests"]
                return_data["No_of_Nights"] = no_of_nights
                return_data["Bed Nights"] = single_guest[
                    "payment_guests"] * single_guest["no_of_nights"]
                return_data["Currency"] = booking_details[
                    "booking_currency_name"]
                # return_data["Cost_per_Person"] = single_guest["payment_person_amount"]
                return_data["Gross_Amount"] = single_guest[
                    "payment_person_cost_after_discount"]
                return_data["VAT"] = single_guest["base_vat"]
                return_data["Catering_Levy"] = 0
                return_data["Net_Amount"] = single_guest[
                    "base_amount_minus_vat"]
                try:
                    return_data["Gross_Base(KES)"] = round(
                        (single_guest["payment_person_cost_after_discount"] *
                         float(single_booking.currency_selling_rate_at_time)),
                        2)
                    return_data["Net_Base(KES)"] = round(
                        (single_guest["base_amount_minus_vat"] *
                         float(single_booking.currency_selling_rate_at_time)),
                        2)
                except Exception:
                    raise Exception(single_booking.booking_public_id)

                data.append(return_data)

        for single_vehicle in booking_details["vehicles"]:
            if single_vehicle["vehicles"] > 0:
                if destination_name:
                    if destination_name == "Day Trip":
                        no_of_nights = 0
                    else:
                        no_of_nights = single_vehicle["vehicle_no_of_nights"]
                else:
                    no_of_nights = single_vehicle["vehicle_no_of_nights"]

                return_data = OrderedDict()
                return_data[
                    "Date"] = single_booking.booking_check_in_date.strftime(
                        "%d-%b-%y")
                return_data["Booking_Ref"] = single_booking.booking_ref_code
                return_data[
                    "Booking_Type"] = single_booking.return_booking_type()
                return_data["Client_Name"] = booking_done_by
                return_data["Item_Name"] = single_vehicle[
                    "vehicle_charge_category"]
                return_data["Destination"] = destination_name
                return_data["Number"] = single_vehicle["vehicles"]
                return_data["No_of_Nights"] = no_of_nights
                return_data["Bed Nights"] = single_vehicle[
                    "vehicles"] * single_vehicle["vehicle_no_of_nights"]
                return_data["Currency"] = booking_details[
                    "booking_currency_name"]
                # return_data["Cost_per_Person"] = single_vehicle["vehicle_charge_category_cost"]
                return_data["Gross_Amount"] = single_vehicle[
                    "vehicle_cost_after_discount"]
                return_data["VAT"] = single_vehicle["base_vat"]
                return_data["Catering_Levy"] = 0
                return_data["Net_Amount"] = single_vehicle[
                    "base_amount_minus_vat"]
                return_data["Gross_Base(KES)"] = round(
                    (single_vehicle["vehicle_cost_after_discount"] *
                     float(single_booking.currency_selling_rate_at_time)), 2)
                return_data["Net_Base(KES)"] = round(
                    (single_vehicle["base_amount_minus_vat"] *
                     float(single_booking.currency_selling_rate_at_time)), 2)

                data.append(return_data)

        for single_inventory in booking_details["inventory_bookings"]:
            if (single_inventory["inventory_booking_adults"] +
                    single_inventory["inventory_booking_extra_adults"]) > 0:
                if destination_name:
                    if destination_name == "Day Trip":
                        no_of_nights = 0
                    else:
                        no_of_nights = 1
                else:
                    no_of_nights = 1

                return_data = OrderedDict()
                return_data[
                    "Date"] = single_booking.booking_check_in_date.strftime(
                        "%d-%b-%y")
                return_data["Booking_Ref"] = single_booking.booking_ref_code
                return_data[
                    "Booking_Type"] = single_booking.return_booking_type()
                return_data["Client_Name"] = booking_done_by
                return_data["Item_Name"] = single_inventory["inventory_name"]
                return_data["Destination"] = destination_name
                # removed due to report differences
                # return_data["Number"] = single_inventory["inventory_booking_adults"] + single_inventory["inventory_booking_extra_adults"]
                return_data["Number"] = single_inventory[
                    "inventory_booking_adults"]
                return_data["No_of_Nights"] = no_of_nights
                # removed due to report differences
                # return_data["Bed Nights"] = (single_inventory["inventory_booking_adults"] + single_inventory["inventory_booking_extra_adults"]) * 1
                return_data["Bed Nights"] = (
                    single_inventory["inventory_booking_adults"]) * 1
                return_data["Currency"] = booking_details[
                    "booking_currency_name"]
                # return_data["Cost_per_Person"] = single_inventory["inventory_booking_cost_per_adult"]
                return_data["Gross_Amount"] = single_inventory[
                    "inventory_booking_adult_cost_after_discounts"]

                if single_inventory["vat"]:
                    return_data["VAT"] = single_inventory["report_adult_vat"]
                else:
                    return_data["VAT"] = 0

                return_data["Catering_Levy"] = 0

                if single_inventory["vat"]:
                    return_data["Net_Amount"] = single_inventory[
                        "report_adult_price_minus_vat"]
                    try:
                        return_data["Gross_Base(KES)"] = round((
                            single_inventory[
                                "inventory_booking_adult_cost_after_discounts"]
                            * float(
                                single_booking.currency_selling_rate_at_time)),
                                                               2)
                        return_data["Net_Base(KES)"] = round((
                            single_inventory["report_adult_price_minus_vat"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                             2)
                    except Exception:
                        raise Exception(single_booking.booking_public_id)
                else:
                    return_data["Net_Amount"] = single_inventory[
                        "inventory_booking_adult_cost"]
                    try:
                        return_data["Gross_Base(KES)"] = round((
                            single_inventory["inventory_booking_adult_cost"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                               2)
                        return_data["Net_Base(KES)"] = round((
                            single_inventory["inventory_booking_adult_cost"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                             2)
                    except Exception:
                        raise Exception(single_booking.booking_public_id)

                data.append(return_data)

            if (single_inventory["inventory_booking_children"] +
                    single_inventory["inventory_booking_extra_children"]) > 0:
                if destination_name:
                    if destination_name == "Day Trip":
                        no_of_nights = 0
                    else:
                        no_of_nights = 1
                else:
                    no_of_nights = 1

                return_data = OrderedDict()
                return_data[
                    "Date"] = single_booking.booking_check_in_date.strftime(
                        "%d-%b-%y")
                return_data["Booking_Ref"] = single_booking.booking_ref_code
                return_data[
                    "Booking_Type"] = single_booking.return_booking_type()
                return_data["Client_Name"] = booking_done_by
                return_data["Item_Name"] = single_inventory["inventory_name"]
                return_data["Destination"] = destination_name
                return_data["Number"] = single_inventory[
                    "inventory_booking_children"] + single_inventory[
                        "inventory_booking_extra_children"]
                return_data["No_of_Nights"] = no_of_nights
                return_data["Bed Nights"] = (
                    single_inventory["inventory_booking_children"] +
                    single_inventory["inventory_booking_extra_children"]) * 1
                return_data["Currency"] = booking_details[
                    "booking_currency_name"]
                # return_data["Cost_per_Person"] = single_inventory["inventory_booking_cost_per_adult"]
                return_data["Gross_Amount"] = single_inventory[
                    "inventory_booking_child_cost_after_discounts"]

                if single_inventory["vat"]:
                    return_data["VAT"] = single_inventory["report_child_vat"]
                else:
                    return_data["VAT"] = 0

                return_data["Catering_Levy"] = 0

                if single_inventory["vat"]:
                    return_data["Net_Amount"] = single_inventory[
                        "report_child_price_minus_vat"]
                    try:
                        return_data["Gross_Base(KES)"] = round((
                            single_inventory[
                                "inventory_booking_child_cost_after_discounts"]
                            * float(
                                single_booking.currency_selling_rate_at_time)),
                                                               2)
                        return_data["Net_Base(KES)"] = round((
                            single_inventory["report_child_price_minus_vat"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                             2)
                    except Exception:
                        raise Exception(single_booking.booking_public_id)
                else:
                    return_data["Net_Amount"] = single_inventory[
                        "inventory_booking_child_cost"]
                    try:
                        return_data["Gross_Base(KES)"] = round((
                            single_inventory["inventory_booking_child_cost"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                               2)
                        return_data["Net_Base(KES)"] = round((
                            single_inventory["inventory_booking_child_cost"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                             2)
                    except Exception:
                        raise Exception(single_booking.booking_public_id)

                data.append(return_data)

        for single_facility in booking_details["facility_bookings"]:
            if single_facility["facility_type"] == "Accomodation":
                if single_facility["accomodation_type"] == "Stables":
                    if (single_facility["facility_booking_adults"] +
                            single_facility["facility_booking_children"]) > 0:
                        # net_amount = round((single_facility["facility_booking_fixed_cost"] - (single_facility["base_fixed_vat"] + single_facility["base_fixed_cater"])), 2)
                        gross_amount = round(
                            (single_facility["base_fixed_minus_taxes"] +
                             single_facility["base_fixed_vat"] +
                             single_facility["base_fixed_cater"]), 2)

                        return_data = OrderedDict()
                        return_data[
                            "Date"] = single_booking.booking_check_in_date.strftime(
                                "%d-%b-%y")
                        return_data[
                            "Booking_Ref"] = single_booking.booking_ref_code
                        return_data[
                            "Booking_Type"] = single_booking.return_booking_type(
                            )
                        return_data["Client_Name"] = booking_done_by
                        return_data["Item_Name"] = single_facility[
                            "facility_name"]
                        return_data["Destination"] = destination_name
                        return_data["Number"] = single_facility["no_of_rooms"]
                        return_data["No_of_Nights"] = single_facility[
                            "facility_no_of_nights"]
                        return_data["Bed Nights"] = single_facility[
                            "no_of_rooms"] * single_facility[
                                "facility_no_of_nights"]
                        return_data["Currency"] = booking_details[
                            "booking_currency_name"]
                        # return_data["Gross_Amount"] = single_facility["facility_booking_fixed_cost"]
                        # return_data["VAT"] = single_facility["base_fixed_vat"]
                        # return_data["Catering_Levy"] = single_facility["base_fixed_cater"]
                        return_data["Gross_Amount"] = gross_amount
                        return_data["VAT"] = round(
                            single_facility["base_fixed_vat"], 2)
                        return_data["Catering_Levy"] = round(
                            single_facility["base_fixed_cater"], 2)
                        # return_data["Net_Amount"] = net_amount
                        return_data["Net_Amount"] = round(
                            single_facility["base_fixed_minus_taxes"], 2)
                        try:
                            return_data["Gross_Base(KES)"] = round(
                                (gross_amount *
                                 float(single_booking.
                                       currency_selling_rate_at_time)), 2)
                            # return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
                            return_data["Net_Base(KES)"] = round((round(
                                single_facility["base_fixed_minus_taxes"],
                                2) * float(single_booking.
                                           currency_selling_rate_at_time)), 2)
                        except Exception:
                            raise Exception(single_booking.booking_public_id)

                        data.append(return_data)

                    if single_facility["facility_booking_extra_adults"] > 0:
                        # net_amount = round((single_facility["facility_booking_extra_adult_cost"] - (single_facility["base_extra_adult_vat"] + single_facility["base_extra_adult_cater"])), 2)
                        gross_amount = round(
                            (single_facility["base_extra_adult_minus_taxes"] +
                             single_facility["base_extra_adult_vat"] +
                             single_facility["base_extra_adult_cater"]), 2)

                        return_data = OrderedDict()
                        return_data[
                            "Date"] = single_booking.booking_check_in_date.strftime(
                                "%d-%b-%y")
                        return_data[
                            "Booking_Ref"] = single_booking.booking_ref_code
                        return_data[
                            "Booking_Type"] = single_booking.return_booking_type(
                            )
                        return_data["Client_Name"] = booking_done_by
                        return_data["Item_Name"] = single_facility[
                            "facility_name"]
                        return_data["Destination"] = destination_name
                        return_data["Number"] = single_facility[
                            "facility_booking_extra_adults"]
                        return_data["No_of_Nights"] = single_facility[
                            "facility_no_of_nights"]
                        return_data["Bed Nights"] = single_facility[
                            "facility_booking_extra_adults"] * single_facility[
                                "facility_no_of_nights"]
                        return_data["Currency"] = booking_details[
                            "booking_currency_name"]
                        # return_data["Gross_Amount"] = single_facility["facility_booking_extra_adult_cost"]
                        # return_data["VAT"] = single_facility["base_extra_adult_vat"]
                        # return_data["Catering_Levy"] = single_facility["base_extra_adult_cater"]
                        # return_data["Net_Amount"] = net_amount
                        return_data["Gross_Amount"] = gross_amount
                        return_data["VAT"] = round(
                            single_facility["base_extra_adult_vat"], 2)
                        return_data["Catering_Levy"] = round(
                            single_facility["base_extra_adult_cater"], 2)
                        return_data["Net_Amount"] = round(
                            single_facility["base_extra_adult_minus_taxes"], 2)
                        # return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_extra_adult_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
                        # return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
                        return_data["Gross_Base(KES)"] = round(
                            (gross_amount * float(
                                single_booking.currency_selling_rate_at_time)),
                            2)
                        return_data["Net_Base(KES)"] = round((
                            single_facility["base_extra_adult_minus_taxes"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                             2)

                        data.append(return_data)

                    if single_facility["facility_booking_extra_children"] > 0:
                        # net_amount = round((single_facility["facility_booking_extra_child_cost"] - (single_facility["base_extra_child_vat"] + single_facility["base_extra_child_cater"])), 2)
                        gross_amount = round(
                            (single_facility["base_extra_child_minus_taxes"] +
                             single_facility["base_extra_child_vat"] +
                             single_facility["base_extra_child_cater"]), 2)

                        return_data = OrderedDict()
                        return_data[
                            "Date"] = single_booking.booking_check_in_date.strftime(
                                "%d-%b-%y")
                        return_data[
                            "Booking_Ref"] = single_booking.booking_ref_code
                        return_data[
                            "Booking_Type"] = single_booking.return_booking_type(
                            )
                        return_data["Client_Name"] = booking_done_by
                        return_data["Item_Name"] = single_facility[
                            "facility_name"]
                        return_data["Destination"] = destination_name
                        return_data["Number"] = single_facility[
                            "facility_booking_extra_children"]
                        return_data["No_of_Nights"] = single_facility[
                            "facility_no_of_nights"]
                        return_data["Bed Nights"] = single_facility[
                            "facility_booking_extra_children"] * single_facility[
                                "facility_no_of_nights"]
                        return_data["Currency"] = booking_details[
                            "booking_currency_name"]
                        # return_data["Gross_Amount"] = single_facility["facility_booking_extra_child_cost"]
                        # return_data["VAT"] = single_facility["base_extra_child_vat"]
                        # return_data["Catering_Levy"] = single_facility["base_extra_child_cater"]
                        # return_data["Net_Amount"] = net_amount
                        return_data["Gross_Amount"] = gross_amount
                        return_data["VAT"] = round(
                            single_facility["base_extra_child_vat"], 2)
                        return_data["Catering_Levy"] = round(
                            single_facility["base_extra_child_cater"], 2)
                        return_data["Net_Amount"] = round(
                            single_facility["base_extra_child_minus_taxes"], 2)
                        # return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_extra_child_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
                        # return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
                        return_data["Gross_Base(KES)"] = round(
                            (gross_amount * float(
                                single_booking.currency_selling_rate_at_time)),
                            2)
                        return_data["Net_Base(KES)"] = round((round(
                            single_facility["base_extra_child_minus_taxes"],
                            2) * float(
                                single_booking.currency_selling_rate_at_time)),
                                                             2)

                        data.append(return_data)

                if single_facility["accomodation_type"] == "Pelican":
                    if single_facility["facility_booking_adults"] > 0:
                        net_amount = round(
                            (single_facility["facility_booking_adult_cost"] -
                             (round(single_facility["base_adult_vat"], 2) +
                              round(single_facility["base_adult_cater"], 2))),
                            2)

                        return_data = OrderedDict()
                        return_data[
                            "Date"] = single_booking.booking_check_in_date.strftime(
                                "%d-%b-%y")
                        return_data[
                            "Booking_Ref"] = single_booking.booking_ref_code
                        return_data[
                            "Booking_Type"] = single_booking.return_booking_type(
                            )
                        return_data["Client_Name"] = booking_done_by
                        return_data["Item_Name"] = single_facility[
                            "facility_name"]
                        return_data["Destination"] = destination_name
                        return_data["Number"] = single_facility[
                            "facility_booking_adults"]
                        return_data["No_of_Nights"] = single_facility[
                            "facility_no_of_nights"]
                        return_data["Bed Nights"] = single_facility[
                            "facility_booking_adults"] * single_facility[
                                "facility_no_of_nights"]
                        return_data["Currency"] = booking_details[
                            "booking_currency_name"]
                        return_data["Gross_Amount"] = single_facility[
                            "facility_booking_adult_cost"]
                        return_data["VAT"] = round(
                            single_facility["base_adult_vat"], 2)
                        return_data["Catering_Levy"] = round(
                            single_facility["base_adult_cater"], 2)
                        return_data["Net_Amount"] = net_amount
                        try:
                            return_data["Gross_Base(KES)"] = round(
                                (single_facility["facility_booking_adult_cost"]
                                 * float(single_booking.
                                         currency_selling_rate_at_time)), 2)
                            return_data["Net_Base(KES)"] = round(
                                (net_amount *
                                 float(single_booking.
                                       currency_selling_rate_at_time)), 2)
                        except Exception:
                            raise Exception(single_booking.booking_public_id)

                        data.append(return_data)

                    if single_facility["facility_booking_children"] > 0:
                        net_amount = round(
                            (single_facility["facility_booking_child_cost"] -
                             (round(single_facility["base_child_vat"], 2) +
                              round(single_facility["base_child_cater"], 2))),
                            2)

                        return_data = OrderedDict()
                        return_data[
                            "Date"] = single_booking.booking_check_in_date.strftime(
                                "%d-%b-%y")
                        return_data[
                            "Booking_Ref"] = single_booking.booking_ref_code
                        return_data[
                            "Booking_Type"] = single_booking.return_booking_type(
                            )
                        return_data["Client_Name"] = booking_done_by
                        return_data["Item_Name"] = single_facility[
                            "facility_name"]
                        return_data["Destination"] = destination_name
                        return_data["Number"] = single_facility[
                            "facility_booking_children"]
                        return_data["No_of_Nights"] = single_facility[
                            "facility_no_of_nights"]
                        return_data["Bed Nights"] = single_facility[
                            "facility_booking_children"] * single_facility[
                                "facility_no_of_nights"]
                        return_data["Currency"] = booking_details[
                            "booking_currency_name"]
                        return_data["Gross_Amount"] = single_facility[
                            "facility_booking_child_cost"]
                        return_data["VAT"] = round(
                            single_facility["base_child_vat"], 2)
                        return_data["Catering_Levy"] = round(
                            single_facility["base_child_cater"], 2)
                        return_data["Net_Amount"] = net_amount
                        return_data["Gross_Base(KES)"] = round((
                            single_facility["facility_booking_child_cost"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                               2)
                        return_data["Net_Base(KES)"] = round(
                            (net_amount * float(
                                single_booking.currency_selling_rate_at_time)),
                            2)

                        data.append(return_data)

            elif single_facility["facility_type"] == "Camping Sites":
                if (single_facility["facility_booking_adults"] +
                        single_facility["facility_booking_extra_adults"]) > 0:
                    try:
                        net_amount = round(
                            (single_facility["facility_booking_adult_cost"] -
                             (single_facility["base_adult_cost_vat"] + 0)), 2)
                    except KeyError:
                        net_amount = round(
                            (single_facility["facility_booking_adult_cost"] -
                             (0 + 0)), 2)
                    except Exception:
                        raise Exception(single_booking.booking_public_id)

                    return_data = OrderedDict()
                    return_data[
                        "Date"] = single_booking.booking_check_in_date.strftime(
                            "%d-%b-%y")
                    return_data[
                        "Booking_Ref"] = single_booking.booking_ref_code
                    return_data[
                        "Booking_Type"] = single_booking.return_booking_type()
                    return_data["Client_Name"] = booking_done_by
                    return_data["Item_Name"] = single_facility["facility_name"]
                    return_data["Destination"] = destination_name
                    return_data["Number"] = single_facility[
                        "facility_booking_adults"] + single_facility[
                            "facility_booking_extra_adults"]
                    return_data["No_of_Nights"] = single_facility[
                        "facility_no_of_nights"]
                    return_data["Bed Nights"] = single_facility[
                        "facility_booking_adults"] + single_facility[
                            "facility_booking_extra_adults"] * single_facility[
                                "facility_no_of_nights"]
                    return_data["Currency"] = booking_details[
                        "booking_currency_name"]
                    return_data["Gross_Amount"] = single_facility[
                        "facility_booking_adult_cost"]
                    try:
                        return_data["VAT"] = single_facility[
                            "base_adult_cost_vat"]
                    except KeyError:
                        return_data["VAT"] = 0
                    return_data["Catering_Levy"] = 0
                    return_data["Net_Amount"] = net_amount
                    try:
                        return_data["Gross_Base(KES)"] = round((
                            single_facility["facility_booking_adult_cost"] *
                            float(
                                single_booking.currency_selling_rate_at_time)),
                                                               2)
                        return_data["Net_Base(KES)"] = round(
                            (net_amount * float(
                                single_booking.currency_selling_rate_at_time)),
                            2)
                    except Exception:
                        raise Exception(single_booking.booking_public_id)

                    data.append(return_data)

                if (single_facility["facility_booking_children"] +
                        single_facility["facility_booking_extra_children"]
                    ) > 0:
                    try:
                        net_amount = round(
                            (single_facility["facility_booking_child_cost"] -
                             (single_facility["base_child_cost_vat"] + 0)), 2)
                    except KeyError:
                        net_amount = round(
                            (single_facility["facility_booking_child_cost"] -
                             (0 + 0)), 2)

                    return_data = OrderedDict()
                    return_data[
                        "Date"] = single_booking.booking_check_in_date.strftime(
                            "%d-%b-%y")
                    return_data[
                        "Booking_Ref"] = single_booking.booking_ref_code
                    return_data[
                        "Booking_Type"] = single_booking.return_booking_type()
                    return_data["Client_Name"] = booking_done_by
                    return_data["Item_Name"] = single_facility["facility_name"]
                    return_data["Destination"] = destination_name
                    return_data["Number"] = single_facility[
                        "facility_booking_children"] + single_facility[
                            "facility_booking_extra_children"]
                    return_data["No_of_Nights"] = single_facility[
                        "facility_no_of_nights"]
                    return_data["Bed Nights"] = single_facility[
                        "facility_booking_children"] + single_facility[
                            "facility_booking_extra_children"] * single_facility[
                                "facility_no_of_nights"]
                    return_data["Currency"] = booking_details[
                        "booking_currency_name"]
                    return_data["Gross_Amount"] = single_facility[
                        "facility_booking_child_cost"]
                    try:
                        return_data["VAT"] = single_facility[
                            "base_child_cost_vat"]
                    except KeyError:
                        return_data["VAT"] = 0
                    return_data["Catering_Levy"] = 0
                    return_data["Net_Amount"] = net_amount
                    return_data["Gross_Base(KES)"] = round(
                        (single_facility["facility_booking_child_cost"] *
                         float(single_booking.currency_selling_rate_at_time)),
                        2)
                    return_data["Net_Base(KES)"] = round(
                        (net_amount *
                         float(single_booking.currency_selling_rate_at_time)),
                        2)

                    data.append(return_data)

                if single_facility["facility_booking_fixed_cost"] > 0:
                    if single_facility["facility_no_of_nights"] / 7 <= 1:
                        weeks = 1
                    elif single_facility["facility_no_of_nights"] / 7 > 1:
                        weeks = round(
                            single_facility["facility_no_of_nights"] / 7) + 1

                    try:
                        net_amount = round(
                            (single_facility["facility_booking_fixed_cost"] -
                             (single_facility["base_fixed_cost_vat"] + 0)), 2)
                    except KeyError:
                        net_amount = round(
                            (single_facility["facility_booking_fixed_cost"] -
                             (0 + 0)), 2)

                    return_data = OrderedDict()
                    return_data[
                        "Date"] = single_booking.booking_check_in_date.strftime(
                            "%d-%b-%y")
                    return_data[
                        "Booking_Ref"] = single_booking.booking_ref_code
                    return_data[
                        "Booking_Type"] = single_booking.return_booking_type()
                    return_data["Client_Name"] = booking_done_by
                    return_data["Item_Name"] = single_facility["facility_name"]
                    return_data["Destination"] = destination_name
                    return_data["Number"] = 1
                    return_data["No_of_Nights"] = weeks
                    return_data["Bed Nights"] = 1 * weeks
                    return_data["Currency"] = booking_details[
                        "booking_currency_name"]
                    return_data["Gross_Amount"] = single_facility[
                        "facility_booking_fixed_cost"]
                    try:
                        return_data["VAT"] = single_facility[
                            "base_fixed_cost_vat"]
                    except KeyError:
                        return_data["VAT"] = 0
                    return_data["Catering_Levy"] = 0
                    return_data["Net_Amount"] = net_amount
                    return_data["Gross_Base(KES)"] = round(
                        (single_facility["facility_booking_fixed_cost"] *
                         float(single_booking.currency_selling_rate_at_time)),
                        2)
                    return_data["Net_Base(KES)"] = round(
                        (net_amount *
                         float(single_booking.currency_selling_rate_at_time)),
                        2)

                    data.append(return_data)

    get_days_transactions = db.session.query(Transaction)\
        .join(Booking, Transaction.booking_id == Booking.booking_public_id)\
        .add_columns(Transaction.transaction_date, Transaction.booking_id, Transaction.transaction_total,\
        Transaction.transaction_payment_currency, Transaction.transaction_payment_method, Transaction.transaction_booking_public_id,\
         Transaction.transaction_payment_gateway)\
        .filter(Transaction.deletion_marker == None)\
        .filter(Booking.deletion_marker == None)\
        .filter(Booking.status != get_booking_status_id("Cancelled"))\
        .all()

    transaction_data = []
    for single_transaction in get_days_transactions:
        for single_date in date_range:
            if single_transaction.transaction_date.strftime(
                    "%Y-%m-%d") == single_date.strftime("%Y-%m-%d"):
                return_transaction = OrderedDict()
                return_transaction[
                    "Date"] = single_transaction.transaction_date.strftime(
                        "%d-%b-%y")

                get_booking_details = db.session.query(Booking)\
                  .filter(Booking.deletion_marker == None)\
                  .filter(Booking.booking_public_id == single_transaction.booking_id)\
                  .first()

                check_school_booking = db.session.query(SchoolBooking)\
                   .join(School, SchoolBooking.school_id == School.school_public_id)\
                   .add_columns(School.school_name)\
                   .filter(SchoolBooking.deletion_marker == None)\
                   .filter(SchoolBooking.booking_id == single_transaction.booking_id)\
                   .first()

                if check_school_booking:
                    try:
                        transaction_by = check_school_booking.school_name
                    except Exception:
                        transaction_by = get_booking_details.booking_done_by
                else:
                    transaction_by = get_booking_details.booking_done_by

                return_transaction[
                    "Booking_Ref"] = get_booking_details.booking_ref_code
                return_transaction["Client_Name"] = transaction_by
                return_transaction[
                    "Start_Date"] = get_booking_details.booking_check_in_date.strftime(
                        "%d-%b-%y")

                get_booking_payment = db.session.query(BookingPayment)\
                  .filter(BookingPayment.deletion_marker == None)\
                  .filter(BookingPayment.transaction_id == single_transaction.transaction_booking_public_id)\
                  .first()

                return_transaction[
                    "Reference"] = get_booking_payment.mpesa_reference
                if get_booking_payment.card_first_four:
                    return_transaction[
                        "Card_Details"] = get_booking_payment.card_first_four + "..." + get_booking_payment.card_last_four
                else:
                    return_transaction["Card_Details"] = ""

                get_payment_method = db.session.query(PaymentMethod)\
                    .filter(PaymentMethod.payment_method_public_id == single_transaction.transaction_payment_method)\
                    .first()

                if single_transaction.transaction_payment_gateway:
                    get_gateway = db.session.query(PaymentGateway)\
                       .filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
                       .first()
                    return_transaction[
                        "Payment_Method"] = get_gateway.payment_gateway_name

                else:
                    return_transaction[
                        "Payment_Method"] = get_payment_method.payment_method_name

                currency = requests.get(
                    get_currency.format(
                        single_transaction.transaction_payment_currency))
                try:
                    return_transaction["Payment_Currency"] = currency.json(
                    )["data"][0]["currency_name"]
                except Exception:
                    raise Exception(
                        single_transaction.transaction_payment_currency +
                        ": " + str(currency.json()))

                return_transaction["Payment_Amount"] = float(
                    single_transaction.transaction_total)

                transaction_data.append(return_transaction)

    date_generated = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
    filename = "reports/gate/DailyReport_" + date_generated + ".xlsx"

    writer = panda.ExcelWriter(filename)
    data_df = panda.DataFrame(data)
    data_df.to_excel(writer, "Sales", index=False)

    if len(data) > 0:
        booking_ref_pivot = panda.pivot_table(
            data_df,
            index=["Date", "Booking_Ref", "Item_Name", "Destination"],
            values=[
                "Number", "Bed Nights", "Gross_Base(KES)", "Net_Base(KES)"
            ],
            aggfunc="sum")
        booking_ref_pivot_df = booking_ref_pivot.to_excel(
            writer, "Sales Pivot(Book_Ref)")

        booking_hospitality_pivot = panda.pivot_table(
            data_df,
            index=["Date", "Item_Name", "Destination"],
            values=[
                "Number", "Bed Nights", "Gross_Base(KES)", "Net_Base(KES)"
            ],
            aggfunc="sum")
        booking_hospitality_pivot_df = booking_hospitality_pivot.to_excel(
            writer, "Sales Pivot(Item_Name)")

    transaction_data = sorted(transaction_data,
                              key=lambda order: order["Payment_Method"])

    transaction_df = panda.DataFrame(transaction_data)
    transaction_df.to_excel(writer, "Payments", index=False)

    if len(transaction_data) > 0:
        transactions_pivot = panda.pivot_table(
            transaction_df,
            index=["Date", "Payment_Method", "Payment_Currency"],
            values=["Payment_Amount"],
            aggfunc="sum")
        transactions_pivot_df = transactions_pivot.to_excel(
            writer, "Payments Pivot Table")

    writer.save()

    return jsonify(
        {"url":
         """/docs/" + filename}), 200


# before filtering foc on 8/jan/21
# def generate_complete_entry_report():
# 	validation_list = [
# 		{"field": "session_id", "alias": "Session ID"}
# 	]

# 	messages = fieldValidation(request.json, validation_list)

# 	if messages:
# 		return jsonify({"messages": messages}), 422

# 	try:
# 		start_date = GenerateDateFromString.generateDate(request.json["start_date"])
# 		end_date = GenerateDateFromString.generateDate(request.json["end_date"])

# 	except Exception:
# 		# yesterday = (datetime.now() - timedelta(days = 1)).strftime("%Y-%m-%d")
# 		start_date = datetime.now()
# 		end_date = datetime.now()

# 	date_range = []
# 	while start_date <= end_date:
# 		date_range.append(start_date)

# 		start_date = start_date + timedelta(days = 1)

# 	# for single_date in date_range:
# 	# 	get_bookings = db.session.query(Booking)\
# 	# 							.filter(Booking.deletion_marker == None)\
# 	# 							.filter(Booking.status != get_booking_status_id("Deleted"))\
# 	# 							.filter(Booking.status != get_booking_status_id("Cancelled"))\
# 	# 							.filter(Booking.status != get_booking_status_id("Abandoned"))\
# 	# 							.filter(Booking.booking_check_in_date == single_date.strftime("%Y-%m-%d"))\
# 	# 							.all()

# 		# if not get_bookings:
# 		# 	message = []
# 		# 	message.append("There is no data to display for the date " + datetime.now().strftime("%d-%m-%Y"))
# 		# 	return jsonify({"message": message, "data": []}), 422
# 	first_date = request.json["start_date"]
# 	data = []
# 	get_bookings = db.session.query(Booking)\
# 							.filter(Booking.deletion_marker == None)\
# 							.filter(Booking.status != get_booking_status_id("Deleted"))\
# 							.filter(Booking.status != get_booking_status_id("Cancelled"))\
# 							.filter(Booking.status != get_booking_status_id("Abandoned"))\
# 							.filter(Booking.booking_check_in_date.between(first_date, request.json["end_date"]))\
# 							.all()

# 	for single_booking in get_bookings:
# 		booking_details = {}
# 		bookingTotal(booking_details, single_booking.booking_public_id, True)

# 		check_school_booking = db.session.query(SchoolBooking)\
# 											.join(School, SchoolBooking.school_id == School.school_public_id)\
# 											.add_columns(School.school_name)\
# 											.filter(SchoolBooking.deletion_marker == None)\
# 											.filter(SchoolBooking.booking_id == single_booking.booking_public_id)\
# 											.first()

# 		get_booking_gatepass_detail = db.session.query(Gatepass)\
# 												.join(Destination, Gatepass.destination == Destination.gatepass_destination_public_id)\
# 												.add_columns(Destination.gatepass_destination_name)\
# 												.filter(Gatepass.deletion_marker == None)\
# 												.filter(Gatepass.status != get_booking_status_id("Cancelled"))\
# 												.filter(Gatepass.status != get_booking_status_id("Updated"))\
# 												.filter(Gatepass.booking_id == single_booking.booking_public_id)\
# 												.first()

# 		if not get_booking_gatepass_detail:
# 			# destination_name = None
# 			destination_name = "N/A"
# 		else:
# 			destination_name = get_booking_gatepass_detail.gatepass_destination_name

# 		if check_school_booking:
# 			try:
# 				booking_done_by = check_school_booking.school_name
# 			except Exception:
# 				booking_done_by = single_booking.booking_done_by
# 		else:
# 			booking_done_by = single_booking.booking_done_by

# 		for single_guest in booking_details["guests"]:
# 			if single_guest["payment_guests"] > 0:
# 				if destination_name:
# 					if destination_name == "Day Trip":
# 						no_of_nights = 0
# 					else:
# 						no_of_nights = single_guest["no_of_nights"]
# 				else:
# 					no_of_nights = single_guest["no_of_nights"]

# 				return_data = OrderedDict()
# 				return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 				return_data["Booking_Ref"] = single_booking.booking_ref_code
# 				return_data["Booking_Type"] = single_booking.return_booking_type()
# 				return_data["Client_Name"] = booking_done_by
# 				return_data["Item_Name"] = single_guest["payment_person"]
# 				return_data["Destination"] = destination_name
# 				return_data["Number"] = single_guest["payment_guests"]
# 				return_data["No_of_Nights"] = no_of_nights
# 				return_data["Bed Nights"] = single_guest["payment_guests"] * single_guest["no_of_nights"]
# 				return_data["Currency"] = booking_details["booking_currency_name"]
# 				# return_data["Cost_per_Person"] = single_guest["payment_person_amount"]
# 				return_data["Gross_Amount"] = single_guest["payment_person_cost_after_discount"]
# 				return_data["VAT"] = single_guest["base_vat"]
# 				return_data["Catering_Levy"] = 0
# 				return_data["Net_Amount"] = single_guest["base_amount_minus_vat"]
# 				try:
# 					return_data["Gross_Base(KES)"] = round((single_guest["payment_person_cost_after_discount"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					return_data["Net_Base(KES)"] = round((single_guest["base_amount_minus_vat"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 				except Exception:
# 					raise Exception(single_booking.booking_public_id)

# 				data.append(return_data)

# 		for single_vehicle in booking_details["vehicles"]:
# 			if single_vehicle["vehicles"] > 0:
# 				if destination_name:
# 					if destination_name == "Day Trip":
# 						no_of_nights = 0
# 					else:
# 						no_of_nights = single_vehicle["vehicle_no_of_nights"]
# 				else:
# 					no_of_nights = single_vehicle["vehicle_no_of_nights"]

# 				return_data = OrderedDict()
# 				return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 				return_data["Booking_Ref"] = single_booking.booking_ref_code
# 				return_data["Booking_Type"] = single_booking.return_booking_type()
# 				return_data["Client_Name"] = booking_done_by
# 				return_data["Item_Name"] = single_vehicle["vehicle_charge_category"]
# 				return_data["Destination"] = destination_name
# 				return_data["Number"] = single_vehicle["vehicles"]
# 				return_data["No_of_Nights"] = no_of_nights
# 				return_data["Bed Nights"] = single_vehicle["vehicles"] * single_vehicle["vehicle_no_of_nights"]
# 				return_data["Currency"] = booking_details["booking_currency_name"]
# 				# return_data["Cost_per_Person"] = single_vehicle["vehicle_charge_category_cost"]
# 				return_data["Gross_Amount"] = single_vehicle["vehicle_cost_after_discount"]
# 				return_data["VAT"] = single_vehicle["base_vat"]
# 				return_data["Catering_Levy"] = 0
# 				return_data["Net_Amount"] = single_vehicle["base_amount_minus_vat"]
# 				return_data["Gross_Base(KES)"] = round((single_vehicle["vehicle_cost_after_discount"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 				return_data["Net_Base(KES)"] = round((single_vehicle["base_amount_minus_vat"] * float(single_booking.currency_selling_rate_at_time)), 2)

# 				data.append(return_data)

# 		for single_inventory in booking_details["inventory_bookings"]:
# 			if (single_inventory["inventory_booking_adults"] + single_inventory["inventory_booking_extra_adults"]) > 0:
# 				if destination_name:
# 					if destination_name == "Day Trip":
# 						no_of_nights = 0
# 					else:
# 						no_of_nights = 1
# 				else:
# 					no_of_nights = 1

# 				return_data = OrderedDict()
# 				return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 				return_data["Booking_Ref"] = single_booking.booking_ref_code
# 				return_data["Booking_Type"] = single_booking.return_booking_type()
# 				return_data["Client_Name"] = booking_done_by
# 				return_data["Item_Name"] = single_inventory["inventory_name"]
# 				return_data["Destination"] = destination_name
# 				# removed due to report differences
# 				# return_data["Number"] = single_inventory["inventory_booking_adults"] + single_inventory["inventory_booking_extra_adults"]
# 				return_data["Number"] = single_inventory["inventory_booking_adults"]
# 				return_data["No_of_Nights"] = no_of_nights
# 				# removed due to report differences
# 				# return_data["Bed Nights"] = (single_inventory["inventory_booking_adults"] + single_inventory["inventory_booking_extra_adults"]) * 1
# 				return_data["Bed Nights"] = (single_inventory["inventory_booking_adults"]) * 1
# 				return_data["Currency"] = booking_details["booking_currency_name"]
# 				# return_data["Cost_per_Person"] = single_inventory["inventory_booking_cost_per_adult"]
# 				return_data["Gross_Amount"] = single_inventory["inventory_booking_adult_cost_after_discounts"]

# 				if single_inventory["vat"]:
# 					return_data["VAT"] = single_inventory["report_adult_vat"]
# 				else:
# 					return_data["VAT"] = 0

# 				return_data["Catering_Levy"] = 0

# 				if single_inventory["vat"]:
# 					return_data["Net_Amount"] = single_inventory["report_adult_price_minus_vat"]
# 					try:
# 						return_data["Gross_Base(KES)"] = round((single_inventory["inventory_booking_adult_cost_after_discounts"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((single_inventory["report_adult_price_minus_vat"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					except Exception:
# 						raise Exception(single_booking.booking_public_id)
# 				else:
# 					return_data["Net_Amount"] = single_inventory["inventory_booking_adult_cost"]
# 					try:
# 						return_data["Gross_Base(KES)"] = round((single_inventory["inventory_booking_adult_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((single_inventory["inventory_booking_adult_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					except Exception:
# 						raise Exception(single_booking.booking_public_id)

# 				data.append(return_data)

# 			if (single_inventory["inventory_booking_children"] + single_inventory["inventory_booking_extra_children"]) > 0:
# 				if destination_name:
# 					if destination_name == "Day Trip":
# 						no_of_nights = 0
# 					else:
# 						no_of_nights = 1
# 				else:
# 					no_of_nights = 1

# 				return_data = OrderedDict()
# 				return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 				return_data["Booking_Ref"] = single_booking.booking_ref_code
# 				return_data["Booking_Type"] = single_booking.return_booking_type()
# 				return_data["Client_Name"] = booking_done_by
# 				return_data["Item_Name"] = single_inventory["inventory_name"]
# 				return_data["Destination"] = destination_name
# 				return_data["Number"] = single_inventory["inventory_booking_children"] + single_inventory["inventory_booking_extra_children"]
# 				return_data["No_of_Nights"] = no_of_nights
# 				return_data["Bed Nights"] = (single_inventory["inventory_booking_children"] + single_inventory["inventory_booking_extra_children"]) * 1
# 				return_data["Currency"] = booking_details["booking_currency_name"]
# 				# return_data["Cost_per_Person"] = single_inventory["inventory_booking_cost_per_adult"]
# 				return_data["Gross_Amount"] = single_inventory["inventory_booking_child_cost_after_discounts"]

# 				if single_inventory["vat"]:
# 					return_data["VAT"] = single_inventory["report_child_vat"]
# 				else:
# 					return_data["VAT"] = 0

# 				return_data["Catering_Levy"] = 0

# 				if single_inventory["vat"]:
# 					return_data["Net_Amount"] = single_inventory["report_child_price_minus_vat"]
# 					try:
# 						return_data["Gross_Base(KES)"] = round((single_inventory["inventory_booking_child_cost_after_discounts"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((single_inventory["report_child_price_minus_vat"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					except Exception:
# 						raise Exception(single_booking.booking_public_id)
# 				else:
# 					return_data["Net_Amount"] = single_inventory["inventory_booking_child_cost"]
# 					try:
# 						return_data["Gross_Base(KES)"] = round((single_inventory["inventory_booking_child_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((single_inventory["inventory_booking_child_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					except Exception:
# 						raise Exception(single_booking.booking_public_id)

# 				data.append(return_data)

# 		for single_facility in booking_details["facility_bookings"]:
# 			if single_facility["facility_type"] == "Accomodation":
# 				if single_facility["accomodation_type"] == "Stables":
# 					if (single_facility["facility_booking_adults"] + single_facility["facility_booking_children"]) > 0:
# 						# net_amount = round((single_facility["facility_booking_fixed_cost"] - (single_facility["base_fixed_vat"] + single_facility["base_fixed_cater"])), 2)
# 						gross_amount = round((single_facility["base_fixed_minus_taxes"] + single_facility["base_fixed_vat"] + single_facility["base_fixed_cater"]), 2)

# 						return_data = OrderedDict()
# 						return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 						return_data["Booking_Ref"] = single_booking.booking_ref_code
# 						return_data["Booking_Type"] = single_booking.return_booking_type()
# 						return_data["Client_Name"] = booking_done_by
# 						return_data["Item_Name"] = single_facility["facility_name"]
# 						return_data["Destination"] = destination_name
# 						return_data["Number"] = single_facility["no_of_rooms"]
# 						return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 						return_data["Bed Nights"] = single_facility["no_of_rooms"] * single_facility["facility_no_of_nights"]
# 						return_data["Currency"] = booking_details["booking_currency_name"]
# 						# return_data["Gross_Amount"] = single_facility["facility_booking_fixed_cost"]
# 						# return_data["VAT"] = single_facility["base_fixed_vat"]
# 						# return_data["Catering_Levy"] = single_facility["base_fixed_cater"]
# 						return_data["Gross_Amount"] = gross_amount
# 						return_data["VAT"] = round(single_facility["base_fixed_vat"], 2)
# 						return_data["Catering_Levy"] = round(single_facility["base_fixed_cater"], 2)
# 						# return_data["Net_Amount"] = net_amount
# 						return_data["Net_Amount"] = round(single_facility["base_fixed_minus_taxes"], 2)
# 						try:
# 							return_data["Gross_Base(KES)"] = round((gross_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 							# return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 							return_data["Net_Base(KES)"] = round((round(single_facility["base_fixed_minus_taxes"], 2) * float(single_booking.currency_selling_rate_at_time)), 2)
# 						except Exception:
# 							raise Exception(single_booking.booking_public_id)

# 						data.append(return_data)

# 					if single_facility["facility_booking_extra_adults"] > 0:
# 						# net_amount = round((single_facility["facility_booking_extra_adult_cost"] - (single_facility["base_extra_adult_vat"] + single_facility["base_extra_adult_cater"])), 2)
# 						gross_amount = round((single_facility["base_extra_adult_minus_taxes"] + single_facility["base_extra_adult_vat"] + single_facility["base_extra_adult_cater"]), 2)

# 						return_data = OrderedDict()
# 						return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 						return_data["Booking_Ref"] = single_booking.booking_ref_code
# 						return_data["Booking_Type"] = single_booking.return_booking_type()
# 						return_data["Client_Name"] = booking_done_by
# 						return_data["Item_Name"] = single_facility["facility_name"]
# 						return_data["Destination"] = destination_name
# 						return_data["Number"] = single_facility["facility_booking_extra_adults"]
# 						return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 						return_data["Bed Nights"] = single_facility["facility_booking_extra_adults"] * single_facility["facility_no_of_nights"]
# 						return_data["Currency"] = booking_details["booking_currency_name"]
# 						# return_data["Gross_Amount"] = single_facility["facility_booking_extra_adult_cost"]
# 						# return_data["VAT"] = single_facility["base_extra_adult_vat"]
# 						# return_data["Catering_Levy"] = single_facility["base_extra_adult_cater"]
# 						# return_data["Net_Amount"] = net_amount
# 						return_data["Gross_Amount"] = gross_amount
# 						return_data["VAT"] = round(single_facility["base_extra_adult_vat"], 2)
# 						return_data["Catering_Levy"] = round(single_facility["base_extra_adult_cater"], 2)
# 						return_data["Net_Amount"] = round(single_facility["base_extra_adult_minus_taxes"], 2)
# 						# return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_extra_adult_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						# return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Gross_Base(KES)"] = round((gross_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((single_facility["base_extra_adult_minus_taxes"] * float(single_booking.currency_selling_rate_at_time)), 2)

# 						data.append(return_data)

# 					if single_facility["facility_booking_extra_children"] > 0:
# 						# net_amount = round((single_facility["facility_booking_extra_child_cost"] - (single_facility["base_extra_child_vat"] + single_facility["base_extra_child_cater"])), 2)
# 						gross_amount = round((single_facility["base_extra_child_minus_taxes"] + single_facility["base_extra_child_vat"] + single_facility["base_extra_child_cater"]), 2)

# 						return_data = OrderedDict()
# 						return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 						return_data["Booking_Ref"] = single_booking.booking_ref_code
# 						return_data["Booking_Type"] = single_booking.return_booking_type()
# 						return_data["Client_Name"] = booking_done_by
# 						return_data["Item_Name"] = single_facility["facility_name"]
# 						return_data["Destination"] = destination_name
# 						return_data["Number"] = single_facility["facility_booking_extra_children"]
# 						return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 						return_data["Bed Nights"] = single_facility["facility_booking_extra_children"] * single_facility["facility_no_of_nights"]
# 						return_data["Currency"] = booking_details["booking_currency_name"]
# 						# return_data["Gross_Amount"] = single_facility["facility_booking_extra_child_cost"]
# 						# return_data["VAT"] = single_facility["base_extra_child_vat"]
# 						# return_data["Catering_Levy"] = single_facility["base_extra_child_cater"]
# 						# return_data["Net_Amount"] = net_amount
# 						return_data["Gross_Amount"] = gross_amount
# 						return_data["VAT"] = round(single_facility["base_extra_child_vat"], 2)
# 						return_data["Catering_Levy"] = round(single_facility["base_extra_child_cater"], 2)
# 						return_data["Net_Amount"] = round(single_facility["base_extra_child_minus_taxes"], 2)
# 						# return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_extra_child_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						# return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Gross_Base(KES)"] = round((gross_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((round(single_facility["base_extra_child_minus_taxes"], 2) * float(single_booking.currency_selling_rate_at_time)), 2)

# 						data.append(return_data)

# 				if single_facility["accomodation_type"] == "Pelican":
# 					if single_facility["facility_booking_adults"] > 0:
# 						net_amount = round((single_facility["facility_booking_adult_cost"] - (round(single_facility["base_adult_vat"], 2) + round(single_facility["base_adult_cater"], 2))), 2)

# 						return_data = OrderedDict()
# 						return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 						return_data["Booking_Ref"] = single_booking.booking_ref_code
# 						return_data["Booking_Type"] = single_booking.return_booking_type()
# 						return_data["Client_Name"] = booking_done_by
# 						return_data["Item_Name"] = single_facility["facility_name"]
# 						return_data["Destination"] = destination_name
# 						return_data["Number"] = single_facility["facility_booking_adults"]
# 						return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 						return_data["Bed Nights"] = single_facility["facility_booking_adults"] * single_facility["facility_no_of_nights"]
# 						return_data["Currency"] = booking_details["booking_currency_name"]
# 						return_data["Gross_Amount"] = single_facility["facility_booking_adult_cost"]
# 						return_data["VAT"] = round(single_facility["base_adult_vat"], 2)
# 						return_data["Catering_Levy"] = round(single_facility["base_adult_cater"], 2)
# 						return_data["Net_Amount"] = net_amount
# 						try:
# 							return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_adult_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 							return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 						except Exception:
# 							raise Exception(single_booking.booking_public_id)

# 						data.append(return_data)

# 					if single_facility["facility_booking_children"] > 0:
# 						net_amount = round((single_facility["facility_booking_child_cost"] - (round(single_facility["base_child_vat"], 2) + round(single_facility["base_child_cater"], 2))), 2)

# 						return_data = OrderedDict()
# 						return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 						return_data["Booking_Ref"] = single_booking.booking_ref_code
# 						return_data["Booking_Type"] = single_booking.return_booking_type()
# 						return_data["Client_Name"] = booking_done_by
# 						return_data["Item_Name"] = single_facility["facility_name"]
# 						return_data["Destination"] = destination_name
# 						return_data["Number"] = single_facility["facility_booking_children"]
# 						return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 						return_data["Bed Nights"] = single_facility["facility_booking_children"] * single_facility["facility_no_of_nights"]
# 						return_data["Currency"] = booking_details["booking_currency_name"]
# 						return_data["Gross_Amount"] = single_facility["facility_booking_child_cost"]
# 						return_data["VAT"] = round(single_facility["base_child_vat"], 2)
# 						return_data["Catering_Levy"] = round(single_facility["base_child_cater"], 2)
# 						return_data["Net_Amount"] = net_amount
# 						return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_child_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)

# 						data.append(return_data)

# 			elif single_facility["facility_type"] == "Camping Sites":
# 				if (single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]) > 0:
# 					try:
# 						net_amount = round((single_facility["facility_booking_adult_cost"] - (single_facility["base_adult_cost_vat"] + 0)), 2)
# 					except KeyError:
# 						net_amount = round((single_facility["facility_booking_adult_cost"] - (0 + 0)), 2)
# 					except Exception:
# 						raise Exception(single_booking.booking_public_id)

# 					return_data = OrderedDict()
# 					return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 					return_data["Booking_Ref"] = single_booking.booking_ref_code
# 					return_data["Booking_Type"] = single_booking.return_booking_type()
# 					return_data["Client_Name"] = booking_done_by
# 					return_data["Item_Name"] = single_facility["facility_name"]
# 					return_data["Destination"] = destination_name
# 					return_data["Number"] = single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"]
# 					return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 					return_data["Bed Nights"] = single_facility["facility_booking_adults"] + single_facility["facility_booking_extra_adults"] * single_facility["facility_no_of_nights"]
# 					return_data["Currency"] = booking_details["booking_currency_name"]
# 					return_data["Gross_Amount"] = single_facility["facility_booking_adult_cost"]
# 					try:
# 						return_data["VAT"] = single_facility["base_adult_cost_vat"]
# 					except KeyError:
# 						return_data["VAT"] = 0
# 					return_data["Catering_Levy"] = 0
# 					return_data["Net_Amount"] = net_amount
# 					try:
# 						return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_adult_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 						return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)
# 					except Exception:
# 						raise Exception(single_booking.booking_public_id)

# 					data.append(return_data)

# 				if (single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]) > 0:
# 					try:
# 						net_amount = round((single_facility["facility_booking_child_cost"] - (single_facility["base_child_cost_vat"] + 0)), 2)
# 					except KeyError:
# 						net_amount = round((single_facility["facility_booking_child_cost"] - (0 + 0)), 2)

# 					return_data = OrderedDict()
# 					return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 					return_data["Booking_Ref"] = single_booking.booking_ref_code
# 					return_data["Booking_Type"] = single_booking.return_booking_type()
# 					return_data["Client_Name"] = booking_done_by
# 					return_data["Item_Name"] = single_facility["facility_name"]
# 					return_data["Destination"] = destination_name
# 					return_data["Number"] = single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"]
# 					return_data["No_of_Nights"] = single_facility["facility_no_of_nights"]
# 					return_data["Bed Nights"] = single_facility["facility_booking_children"] + single_facility["facility_booking_extra_children"] * single_facility["facility_no_of_nights"]
# 					return_data["Currency"] = booking_details["booking_currency_name"]
# 					return_data["Gross_Amount"] = single_facility["facility_booking_child_cost"]
# 					try:
# 						return_data["VAT"] = single_facility["base_child_cost_vat"]
# 					except KeyError:
# 						return_data["VAT"] = 0
# 					return_data["Catering_Levy"] = 0
# 					return_data["Net_Amount"] = net_amount
# 					return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_child_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)

# 					data.append(return_data)

# 				if single_facility["facility_booking_fixed_cost"] > 0:
# 					if single_facility["facility_no_of_nights"]/7 <= 1:
# 						weeks = 1
# 					elif single_facility["facility_no_of_nights"]/7 > 1:
# 						weeks = round(single_facility["facility_no_of_nights"]/7) + 1

# 					try:
# 						net_amount = round((single_facility["facility_booking_fixed_cost"] - (single_facility["base_fixed_cost_vat"] + 0)), 2)
# 					except KeyError:
# 						net_amount = round((single_facility["facility_booking_fixed_cost"] - (0 + 0)), 2)

# 					return_data = OrderedDict()
# 					return_data["Date"] = single_booking.booking_check_in_date.strftime("%d-%b-%y")
# 					return_data["Booking_Ref"] = single_booking.booking_ref_code
# 					return_data["Booking_Type"] = single_booking.return_booking_type()
# 					return_data["Client_Name"] = booking_done_by
# 					return_data["Item_Name"] = single_facility["facility_name"]
# 					return_data["Destination"] = destination_name
# 					return_data["Number"] = 1
# 					return_data["No_of_Nights"] = weeks
# 					return_data["Bed Nights"] = 1 * weeks
# 					return_data["Currency"] = booking_details["booking_currency_name"]
# 					return_data["Gross_Amount"] = single_facility["facility_booking_fixed_cost"]
# 					try:
# 						return_data["VAT"] = single_facility["base_fixed_cost_vat"]
# 					except KeyError:
# 						return_data["VAT"] = 0
# 					return_data["Catering_Levy"] = 0
# 					return_data["Net_Amount"] = net_amount
# 					return_data["Gross_Base(KES)"] = round((single_facility["facility_booking_fixed_cost"] * float(single_booking.currency_selling_rate_at_time)), 2)
# 					return_data["Net_Base(KES)"] = round((net_amount * float(single_booking.currency_selling_rate_at_time)), 2)

# 					data.append(return_data)

# 	get_days_transactions = db.session.query(Transaction)\
# 									  .join(Booking, Transaction.booking_id == Booking.booking_public_id)\
# 									  .add_columns(Transaction.transaction_date, Transaction.booking_id, Transaction.transaction_total,\
# 										  		   Transaction.transaction_payment_currency, Transaction.transaction_payment_method, Transaction.transaction_booking_public_id,\
# 												   Transaction.transaction_payment_gateway)\
# 									  .filter(Transaction.deletion_marker == None)\
# 									  .filter(Booking.deletion_marker == None)\
# 									  .filter(Booking.status != get_booking_status_id("Cancelled"))\
# 									  .all()

# 	transaction_data = []
# 	for single_transaction in get_days_transactions:
# 		for single_date in date_range:
# 			if single_transaction.transaction_date.strftime("%Y-%m-%d") == single_date.strftime("%Y-%m-%d"):
# 				return_transaction = OrderedDict()
# 				return_transaction["Date"] = single_transaction.transaction_date.strftime("%d-%b-%y")

# 				get_booking_details = db.session.query(Booking)\
# 												.filter(Booking.deletion_marker == None)\
# 												.filter(Booking.booking_public_id == single_transaction.booking_id)\
# 												.first()

# 				check_school_booking = db.session.query(SchoolBooking)\
# 												 .join(School, SchoolBooking.school_id == School.school_public_id)\
# 												 .add_columns(School.school_name)\
# 												 .filter(SchoolBooking.deletion_marker == None)\
# 												 .filter(SchoolBooking.booking_id == single_transaction.booking_id)\
# 												 .first()

# 				if check_school_booking:
# 					try:
# 						transaction_by = check_school_booking.school_name
# 					except Exception:
# 						transaction_by = get_booking_details.booking_done_by
# 				else:
# 					transaction_by = get_booking_details.booking_done_by

# 				return_transaction["Booking_Ref"] = get_booking_details.booking_ref_code
# 				return_transaction["Client_Name"] = transaction_by
# 				return_transaction["Start_Date"] = get_booking_details.booking_check_in_date.strftime("%d-%b-%y")

# 				get_booking_payment = db.session.query(BookingPayment)\
# 												.filter(BookingPayment.deletion_marker == None)\
# 												.filter(BookingPayment.transaction_id == single_transaction.transaction_booking_public_id)\
# 												.first()

# 				return_transaction["Reference"] = get_booking_payment.mpesa_reference
# 				if get_booking_payment.card_first_four:
# 					return_transaction["Card_Details"] = get_booking_payment.card_first_four + "..." + get_booking_payment.card_last_four
# 				else:
# 					return_transaction["Card_Details"] = ""

# 				get_payment_method = db.session.query(PaymentMethod)\
# 											.filter(PaymentMethod.payment_method_public_id == single_transaction.transaction_payment_method)\
# 											.first()

# 				if single_transaction.transaction_payment_gateway:
# 					get_gateway = db.session.query(PaymentGateway)\
# 											.filter(PaymentGateway.payment_gateway_public_id == single_transaction.transaction_payment_gateway)\
# 											.first()
# 					return_transaction["Payment_Method"] = get_gateway.payment_gateway_name

# 				else:
# 					return_transaction["Payment_Method"] = get_payment_method.payment_method_name

# 				currency = requests.get(get_currency.format(single_transaction.transaction_payment_currency))
# 				try:
# 					return_transaction["Payment_Currency"] = currency.json()["data"][0]["currency_name"]
# 				except Exception:
# 					raise Exception(single_transaction.transaction_payment_currency + ": " + str(currency.json()))

# 				return_transaction["Payment_Amount"] = float(single_transaction.transaction_total)

# 				transaction_data.append(return_transaction)

# 	date_generated = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
# 	filename = "reports/gate/DailyReport_"  + date_generated + ".xlsx"

# 	writer = panda.ExcelWriter(filename)
# 	data_df = panda.DataFrame(data)
# 	data_df.to_excel(writer, "Sales", index = False)

# 	if len(data) > 0:
# 		booking_ref_pivot = panda.pivot_table(data_df, index = ["Date", "Booking_Ref", "Item_Name", "Destination"], values = ["Number", "Bed Nights", "Gross_Base(KES)", "Net_Base(KES)"], aggfunc = "sum")
# 		booking_ref_pivot_df = booking_ref_pivot.to_excel(writer, "Sales Pivot(Book_Ref)")

# 		booking_hospitality_pivot = panda.pivot_table(data_df, index = ["Date", "Item_Name", "Destination"], values = ["Number", "Bed Nights", "Gross_Base(KES)", "Net_Base(KES)"], aggfunc = "sum")
# 		booking_hospitality_pivot_df = booking_hospitality_pivot.to_excel(writer, "Sales Pivot(Item_Name)")

# 	transaction_data = sorted(transaction_data, key=lambda order: order["Payment_Method"])

# 	transaction_df = panda.DataFrame(transaction_data)
# 	transaction_df.to_excel(writer, "Payments", index = False)

# 	if len(transaction_data) > 0:
# 		transactions_pivot = panda.pivot_table(transaction_df, index = ["Date", "Payment_Method", "Payment_Currency"], values = ["Payment_Amount"], aggfunc = "sum")
# 		transactions_pivot_df = transactions_pivot.to_excel(writer, "Payments Pivot Table")

# 	writer.save()

# 	return jsonify({"url": """/docs/" + filename}), 200


@app.route("/reports/general", methods=["POST"])
def generate_generic_report():
    validation_list = [{
        "field": "start_date",
        "alias": "Start date"
    }, {
        "field": "end_date",
        "alias": "End date"
    }, {
        "field": "session_id",
        "alias": "Session ID"
    }]

    messages = fieldValidation(request.json, validation_list)

    if messages:
        return jsonify({"messages": messages}), 422

    try:
        if request.json["booking_type"]:
            booking_type = request.json["booking_type"]
        else:
            booking_type = None

    except Exception:
        booking_type = None

    if not booking_type:
        get_bookings = db.session.query(Booking)\
            .filter(Booking.booking_check_in_date >= request.json["start_date"])\
            .filter(Booking.booking_check_out_date <= request.json["end_date"])\
            .all()

    else:
        get_bookings = db.session.query(Booking)\
            .filter(Booking.booking_type == booking_type)\
            .filter(Booking.booking_check_in_date >= request.json["start_date"])\
            .filter(Booking.booking_check_out_date <= request.json["end_date"])\
            .all()

    if not get_bookings:
        message = []
        message.append("There are no records to display.")
        return jsonify({"message": message}), 412

    data = []
    for single in get_bookings:
        return_data = {}

        bookingTotal(return_data, single.booking_public_id)

        return_data["Booking_Type"] = single.b_type.booking_type_name
        return_data["Check_In_Date"] = single.booking_check_in_date
        return_data["Check_Out_Date"] = single.booking_check_out_date
        return_data["Done_By"] = single.booking_done_by
        return_data["Ref_Code"] = single.booking_ref_code

        check_to_invoice = db.session.query(Invoice)\
          .filter(Invoice.deletion_marker == None)\
          .filter(Invoice.booking_id == single.booking_public_id)\
          .first()

        if check_to_invoice:
            if single.deletion_marker == 1:
                return_data["Status"] = "Cancelled"
            else:
                return_data["Status"] = "To Invoice"

        else:
            if single.checked_out == 1:
                return_data["Status"] = "Checked Out"
            elif single.checked_in == 1:
                return_data["Status"] = "Checked In"
            else:
                return_data["Status"] = single.b_status.booking_status_name

        return_data["Currency"] = return_data["booking_currency_name"]
        return_data["Booking_Total"] = return_data["total_cost"]

        data.append(return_data)

    return jsonify({"data": data}), 200


@app.route("/reports/summary", methods=["POST"])
def generate_summary():
    validation_list = [{
        "field": "start_date",
        "alias": "Start date"
    }, {
        "field": "end_date",
        "alias": "End date"
    }, {
        "field": "session_id",
        "alias": "Session ID"
    }]

    messages = fieldValidation(request.json, validation_list)

    if messages:
        return jsonify({"messages": messages}), 422

    # Generating date from string using a simple, custom function
    start_date = GenerateDateFromString.generateDate(
        request.json["start_date"])
    end_date = GenerateDateFromString.generateDate(request.json["end_date"])

    date_list = []

    while start_date <= end_date:
        date_list.append(start_date)
        start_date = start_date + timedelta(days=1)

    data = []
    grand_total = []
    for single_date in date_list:
        return_data = {}

        get_all_bookings = db.session.query(Booking)\
            .filter(Booking.deletion_marker == None)\
            .filter(Booking.booking_check_in_date == GenerateDateFromString.generateDateTime(single_date.strftime("%Y-%m-%d")))\
            .filter(Booking.status != get_booking_status_id("Cancelled"))\
            .filter(Booking.status != get_booking_status_id("No-Show"))\
            .order_by(Booking.booking_check_in_date.desc())\
            .all()

        return_data["date"] = single_date.strftime("%Y-%m-%d")
        return_data["bookings_count"] = len(get_all_bookings)

        guest_total = []
        booking_total = []
        for single in get_all_bookings:
            booking_data = {}
            bookingTotalGraph(booking_data, single.booking_public_id)

            guest_total.append(booking_data["guest_total"])

            if booking_data["facility_bookings"]:
                for each_facility in booking_data["facility_bookings"]:
                    guest_total.append(
                        each_facility["facility_booking_guests"])

            if booking_data["inventory_bookings"]:
                for each_inventory in booking_data["inventory_bookings"]:
                    guest_total.append(
                        each_inventory["inventory_booking_guests"])

            booking_total.append(
                currencyHandler("162fface-f5f1-41de-913b-d2bb784dda3a",
                                single.currency, booking_data["total_cost"]))

        return_data["guests"] = sum(guest_total)
        return_data["booking_total"] = sum(booking_total)
        return_data["currency"] = "KES"

        grand_total.append(sum(booking_total))

        data.append(return_data)

    return jsonify({"data": data, "grand_total": sum(grand_total)}), 200
