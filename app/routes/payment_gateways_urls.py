from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes import db_cache, FromCache
from routes.seed_functions import PaymentMethodSeed
from database.payment_methods import PaymentMethod
from database.payment_gateways import PaymentGateway
from functions.validation import *
from functions.async_functions import AsyncRequests
from variables import get_currency


def close(self):
	self.session.close()


@app.route("/payments/gateways/new", methods = ["POST"])
def add_new_payment_gateway():
	validation_list = [
		{"field": "method", "alias": "Payment method"},
		{"field": "name", "alias": "Payment name"},
		{"field": "code"},
		{"field": "currency"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422
	
	gateway = PaymentGateway(
		payment_gateway_public_id = str(uuid.uuid4()),
		payment_method = request.json["method"],
		payment_gateway_name = request.json["name"],
		payment_gateway_code = request.json["code"],
		payment_gateway_currency = request.json["currency"],
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(gateway)

	try:
		db.session.commit()
		close(db)
		
		message = []
		message.append("The payment gateway has been saved.")
		return jsonify({"message": message}), 201

	except Exception:
		db.session.rollback()
		close(db)

		message = []
		message.append("The payment gateway has not been saved. Please try again later.")
		return jsonify({"message": message}), 422


@app.route("/payments/gateways/view", methods = ["GET"])
def view_payment_gateways():
	get_gateways = db.session.query(PaymentGateway)\
							 .filter(PaymentGateway.deletion_marker == None)\
							 .order_by(PaymentGateway.payment_gateway_id.asc())\
							 .options(FromCache(db_cache))\
							 .all()

	if not get_gateways:
		messages = []
		messages.append("There are no payment gateways set.")
		return jsonify({"messages": messages}), 412

	else:
		data = []

		for single in get_gateways:
			data.append(single.return_json())

		return jsonify({"data": data}), 200


@app.route("/payments/gateways/modify", methods = ["PATCH"])
def update_payment_gateway():
	validation_list = [
		{"field": "payment_gateway_id", "alias": "Payment gateway ID"},
		{"field": "method", "alias": "Payment method"},
		{"field": "name", "alias": "Payment name"},
		{"field": "code"},
		{"field": "currency"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	get_payment_gateway = db.session.query(PaymentGateway)\
									.filter(PaymentGateway.deletion_marker == None)\
									.filter(PaymentGateway.payment_gateway_public_id == request.json["payment_gateway_id"])\
									.first()

	if not get_payment_gateway:
		message = []
		message.append("The selected payment gateway does not appear to exist.")
		return jsonify({"message": message}), 412

	get_payment_gateway.payment_method = request.json["method"]
	get_payment_gateway.payment_gateway_name = request.json["name"]
	get_payment_gateway.payment_gateway_code = request.json["code"]
	get_payment_gateway.payment_gateway_currency = request.json["currency"]
	get_payment_gateway.session_id = request.json["session_id"]
	get_payment_gateway.updated_at = datetime.now()

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Updated the payment gateway details.")
		return jsonify({"message": message}), 200
	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to update the payment gateway details.")
		return jsonify({"message": message, "exception": str(e)}), 422