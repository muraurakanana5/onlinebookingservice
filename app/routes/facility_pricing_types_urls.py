from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from database.facility_pricing_type import FacilityPricing
from routes.seed_functions import FacilityPricingTypeSeed

from variables import *


def close(self):
	self.session.close()


##########################
#### Facility Pricing ####
##########################
@app.route("/facility/pricing/new", methods = ["POST"])
def add_new_pricing():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Facility pricing name is empty.")
	except KeyError as e:
		messages.append("Facility pricing name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	try:
		special = request.json["special"]
	except KeyError:
		special = None

	if messages:
		return jsonify({"messages": messages}), 422

	pricing = FacilityPricing(
		facility_pricing_type_public_id = str(uuid.uuid4()),
		facility_pricing_name = request.json["name"],
		facility_pricing_special = special,
		session_id = request.json["session_id"],
		created_at = datetime.now(),
		updated_at = datetime.now()
	)

	db.session.add(pricing)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully added a facility pricing type.")
		return jsonify({"message": message}), 200

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("There was an error adding that facility pricing type. Try again later.")
		return jsonify({"message": message, "error": str(e)}), 422


@app.route("/facility/pricing/view")
def view_facility_pricing():
	if db.session.query(FacilityPricing)\
					.filter(FacilityPricing.deletion_marker == None)\
					.filter(FacilityPricing.facility_pricing_type_public_id == "9a9a77eb")\
					.count() == 0:
		FacilityPricingTypeSeed.seed_default_facility_pricing_types()
	
	get_facility_pricing = db.session.query(FacilityPricing)\
									 .filter(FacilityPricing.deletion_marker == None)\
									 .all()

	if not get_facility_pricing:
		message = []
		message.append("There are no facility pricing types present in the system at the moment.")
		return jsonify({"message": message}), 412
	
	else:
		data = []

		for single in get_facility_pricing:
			data.append(single.return_json())

		return jsonify({"data": data}), 200


@app.route("/facility/pricing/view/<facility_price_id>")
def view_single_facility_pricing(facility_price_id):
	get_facility_pricing = db.session.query(FacilityPricing)\
									 .filter(FacilityPricing.deletion_marker == None)\
									 .filter(FacilityPricing.facility_pricing_type_public_id == facility_price_id)\
									 .all()

	if not get_facility_pricing:
		message = []
		message.append("That facility pricing type is not present in the system at the moment.")
		return jsonify({"message": message}), 412
	
	else:
		data = []

		for single in get_facility_pricing:
			data.append(single.return_json())

		return jsonify({"data": data}), 200


@app.route("/facility/pricing/modify", methods = ["PATCH"])
def modify_facility_pricing():
	messages = []
	
	try:
		request.json["facility_price_id"].strip()
		if not request.json["facility_price_id"]:
			messages.append("Facility ID is empty.")
	except KeyError as e:
		messages.append("Facility ID is missing.")
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Facility name is empty.")
	except KeyError as e:
		messages.append("Facility name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	try:
		special = request.json["special"]
	except KeyError:
		special = None

	get_facility_pricing = db.session.query(FacilityPricing)\
									 .filter(FacilityPricing.deletion_marker == None)\
									 .filter(FacilityPricing.facility_pricing_type_public_id == request.json["facility_price_id"])\
									 .first()
	
	if not get_facility_pricing:
		message = []
		message.append("That facility pricing type is not present in the system at the moment.")
		return jsonify({"message": message}), 412

	else:
		get_facility_pricing.facility_pricing_name = request.json["name"]
		get_facility_pricing.facility_pricing_special = special
		get_facility_pricing.session_id = request.json["session_id"]
		get_facility_pricing.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("Successfully added a facility pricing type.")
			return jsonify({"message": message}), 200

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("There was an error modifying that facility pricing type. Try again later.")
			return jsonify({"message": message, "error": str(e)}), 422


@app.route("/facility/pricing/delete", methods = ["PATCH"])
def delete_single_facility_pricing():
	get_facility_pricing = db.session.query(FacilityPricing)\
									 .filter(FacilityPricing.deletion_marker == None)\
									 .filter(FacilityPricing.facility_pricing_type_public_id == request.json["facility_price_id"])\
									 .first()

	if not get_facility_pricing:
		message = []
		message.append("That facility pricing type is not present in the system at the moment.")
		return jsonify({"message": message}), 412

	else:
		get_facility_pricing.deletion_marker = 1
		get_facility_pricing.session_id = request.json["session_id"]
		get_facility_pricing.updated_at = datetime.now()

		try:
			db.session.commit()
			close(db)

			message = []
			message.append("Deletion successful. Please proceed.")
			return jsonify({"message": message}), 200

		except Exception as e:
			db.session.rollback()
			close(db)

			message = []
			message.append("There was an error deleting that facility pricing. Try again later.")
			return jsonify({"message": message, "error": str(e)}), 422