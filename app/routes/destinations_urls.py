from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes import db_cache, FromCache
from database.destination import Destination
from database.partner_destination import PartnerDestination
from variables import *
from functions.validation import fieldValidation


def close(self):
	self.session.close()


@app.route("/destination/new", methods = ["POST"])
def add_new_destination():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	try:
		request.json["code"].strip()
		if not request.json["code"]:
			messages.append("Destination Code is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	destination = Destination(
		gatepass_destination_public_id = str(uuid.uuid4()),
		gatepass_destination_name = request.json["name"],
		code = request.json["code"],
		session_id = request.json["session_id"]
	)

	db.session.add(destination)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("You have successfully created a new destination.")

		return jsonify({"message": output}), 201
	except Exception as e:
		print(e)

		db.session.rollback()
		close(db)

		output = []
		output.append("Unfortunately, you cannot add a destination at this moment. Please try again.")

		return jsonify({"message": output}), 422


@app.route("/destination/view")
def get_all_destinations():
	output = []
	id_array = []

	all_destinations = db.session.query(Destination)\
								 .filter(Destination.deletion_marker == None)\
								 .options(FromCache(db_cache))\
								 .order_by(Destination.gatepass_destination_name.asc())\
								 .all()

	if not all_destinations:
		output.append("There are no destinations in the system at this point in time.")

		return jsonify({"message": output}), 200

	else:
		for single in all_destinations:
			output.append(single.return_json())

		return jsonify({"data": output}), 200


@app.route("/destination/view/<destination_id>")
def get_single_destination(destination_id):
	output = []

	all_destinations = db.session.query(Destination)\
								 .filter(Destination.deletion_marker == None)\
								 .filter(Destination.gatepass_destination_public_id == destination_id)\
								 .options(FromCache(db_cache))\
								 .all()

	if not all_destinations:
		output.append("That destination does not seem to exist in the system at this point in time.")

		return jsonify({"message": output}), 200

	else:
		for single in all_destinations:
			output.append(single.return_json())

		return jsonify({"data": output}), 200


@app.route('/destination/modify', methods=['PATCH'])
def modify_destination():
	messages = []
	
	try:
		request.json["name"].strip()
		if not request.json["name"]:
			messages.append("Name is empty.")
	except KeyError as e:
		messages.append("Name is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	try:
		request.json["code"].strip()
		if not request.json["code"]:
			messages.append("Destination Code is empty.")
	except KeyError as e:
		messages.append("Destination Code is missing.")
	
	try:
		request.json["destination_id"].strip()
		if not request.json["destination_id"]:
			messages.append("Destination ID is empty.")
	except KeyError as e:
		messages.append("destination ID is missing.")
	
	# get the destination
	destination = db.session.query(Destination)\
									.filter(Destination.deletion_marker == None)\
									.filter(Destination.gatepass_destination_public_id == request.json["destination_id"])\
									.first()
	
	if not destination:
		destination = {
			'message' : 'No such desination could be found'
		}
		return make_response(jsonify(destination)), 422
	
	destination.code = request.json["code"]
	destination.gatepass_destination_name = request.json["name"]
	destination.session_id = request.json["session_id"]
	
	try:
		db.session.commit()
		close(db)

		reponseObject = {
			'message' : 'Succesfully altered'
		}
		return make_response(jsonify(reponseObject)), 200
	except Exception as error:
		reponseObject = {
			'message' : str(error)
		}
		return make_response(jsonify(reponseObject)), 500


@app.route("/destination/delete", methods = ["POST"])
def delete_single_destination():
	messages = []
	
	try:
		request.json["destination_id"].strip()
		if not request.json["destination_id"]:
			messages.append("Destination ID is empty.")
	except KeyError as e:
		messages.append("Destination ID is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	get_destination = db.session.query(Destination)\
								.filter(Destination.deletion_marker == None)\
								.filter(Destination.gatepass_destination_public_id == request.json["destination_id"])\
								.first()

	get_destination.deletion_marker = 1
	get_destination.session_id = request.json["session_id"]

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("You have successfully deleted the destination.")

		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)

		db.session.rollback()
		close(db)

		output = []
		output.append("Unfortunately, you cannot delete a destination at this moment. Please try again.")

		return jsonify({"message": output}), 422



##########################
## Partner Destinations ##
##########################
@app.route("/destination/partner/new", methods = ["POST"])
def add_partner_destinations():
	validation_list = [
		{"field": "partner_id", "alias": "Partner ID"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": mesages}), 422

	for single_destination in request.json["destinations"]:
		p_destination = PartnerDestination(
			partner_destination_public_id = str(uuid.uuid4()),
			partner_id = request.json["partner_id"],
			destination_id = single_destination,
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(p_destination)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully saved the partner destinations.")
		return jsonify({"message": message}), 201

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to save the partner destinations. Try again later.")
		return jsonify({"message": message, "error": str(e)}), 422


@app.route("/destination/partner/<partner_id>/view")
def view_single_partner_destinations(partner_id):
	get_partner_destinations = db.session.query(PartnerDestination)\
										 .filter(PartnerDestination.deletion_marker == None)\
										 .filter(PartnerDestination.partner_id == partner_id)\
										 .all()

	if not get_partner_destinations:
		# message = []
		# message.append("There are no destinations set for the selected partner.")
		# return jsonify({"message": message}), 412

		all_destinations = db.session.query(Destination)\
									 .filter(Destination.deletion_marker == None)\
									 .options(FromCache(db_cache))\
									 .order_by(Destination.gatepass_destination_name.asc())\
									 .all()

		data = []
		for single in all_destinations:
			return_data = {}
			return_data["partner_destination_public_id"] = ""
			return_data["partner_id"] = partner_id
			return_data["destination_id"] = single.gatepass_destination_public_id
			return_data["destination_public_id"] = single.gatepass_destination_public_id
			return_data["destination_name"] = single.gatepass_destination_name
			return_data["session_id"] = single.session_id
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			data.append(return_data)

		return jsonify({"data": data}), 200
	
	data = []
	for single in get_partner_destinations:
		data.append(single.return_json())

	sorted_data = sorted(data, key=lambda order: order["destination_name"])
	
	return jsonify({"data": sorted_data}), 200


@app.route("/destination/partner/modify", methods = ["PATCH"])
def modify_partner_destinations():
	validation_list = [
		{"field": "partner_id", "alias": "Partner ID"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": mesages}), 422

	destinations_array_db = []
	
	get_existing_destinations = db.session.query(PartnerDestination)\
										  .filter(PartnerDestination.deletion_marker == None)\
										  .filter(PartnerDestination.partner_id == request.json["partner_id"])\
										  .all()

	for single_existing in get_existing_destinations:
		destinations_array_db.append(single_existing.destination_id)

	for each_existing in destinations_array_db:
		if each_existing in set(request.json["destinations"]):
			pass
		else:
			get_destination = db.session.query(PartnerDestination)\
										.filter(PartnerDestination.partner_id == request.json["partner_id"])\
										.filter(PartnerDestination.destination_id == each_existing)\
										.first()

			get_destination.deletion_marker = 1
			get_destination.session_id = request.json["session_id"]
			get_destination.updated_at = datetime.now()

	for sent_destination in set(request.json["destinations"]):
		if sent_destination in destinations_array_db:
			pass
		else:
			p_destination = PartnerDestination(
				partner_destination_public_id = str(uuid.uuid4()),
				partner_id = request.json["partner_id"],
				destination_id = sent_destination,
				session_id = request.json["session_id"],
				created_at = datetime.now()
			)

			db.session.add(p_destination)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully saved the partner destinations.")
		return jsonify({"message": message}), 200

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to save the partner destinations. Try again later.")
		return jsonify({"message": message, "error": str(e)}), 422