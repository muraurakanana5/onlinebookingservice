from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_details import Detail
from database.booking_guest_types import GuestType
from variables import *


def close(self):
	self.session.close()


#################
#### Express ####
#################
@app.route("/bookings/express/new", methods = ["POST"])
def add_new_express_booking():
	if db.session.query(BookingStatus)\
					.filter(BookingStatus.deletion_marker==None)\
					.filter(BookingStatus.booking_status_public_id == "626f13f2-aab0-4488-a774-44969b3062a3")\
					.count() == 0:
		BookingStatusSeed.seed_default_booking_status()

	if not request.json["type"].strip()\
	or not request.json["check_in"].strip()\
	or not request.json["check_out"].strip()\
	or not str(request.json["ea_adult_citizen"]).strip()\
	or not str(request.json["ea_adult_resident"]).strip()\
	or not str(request.json["non_resident_adult"]).strip()\
	or not str(request.json["ea_child_citizen"]).strip()\
	or not str(request.json["ea_child_resident"]).strip()\
	or not str(request.json["non_resident_child"]).strip()\
	or not str(request.json["infant"]).strip()\
	or not request.json["first_name"].strip()\
	or not request.json["last_name"].strip()\
	or not request.json["email"].strip()\
	or not request.json["phone"].strip()\
	or not request.json["address"].strip():
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": output}), 422

	today = datetime.now().strftime("%Y-%m-%d")
	booking_id = str(uuid.uuid4())

	issues = []

	if request.json["check_in"] < today:
		output = []
		output.append("Your check in date cannot be in the past.")
		return jsonify({"message": output}), 422

	if request.json["check_out"] < today:
		output = []
		output.append("Your check out date cannot be in the past.")
		return jsonify({"message": output}), 422

	if request.json["check_out"] < request.json["check_in"]:
		output = []
		output.append("Your check out date cannot come before your check in date.")
		return jsonify({"message": output}), 422

	total_guests = int(request.json["ea_adult_citizen"]) + int(request.json["ea_adult_resident"]) + int(request.json["non_resident_adult"])\
					+ int(request.json["ea_child_citizen"]) + int(request.json["ea_child_resident"]) + int(request.json["non_resident_child"])\
					+ int(request.json["infant"])

	if total_guests <= 0:
		output = []
		output.append("A booking must have at least one guest. Please try again.")
		return jsonify({"message": output})

	booking = Booking(
		booking_public_id = booking_id,
		booking_type = request.json["type"],
		booking_check_in_date = request.json["check_in"],
		booking_check_out_date = request.json["check_out"],
		booking_done_by = request.json["first_name"] + " " + request.json["last_name"],
		booking_ref_code = str(uuid.uuid4())[:10],
		express_checkout = 1,
		status = get_booking_status_id("Abandoned")
	)

	db.session.add(booking)

	guest1 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Adult Citizen"),
		guest_count = request.json["ea_adult_citizen"]
	)

	db.session.add(guest1)

	guest2 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Adult Resident"),
		guest_count = request.json["ea_adult_resident"]
	)

	db.session.add(guest2)

	guest3 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Non Resident Adult"),
		guest_count = request.json["non_resident_adult"]
	)

	db.session.add(guest3)

	guest4 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Child Citizen"),
		guest_count = request.json["ea_child_citizen"]
	)

	db.session.add(guest4)

	guest5 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Child Resident"),
		guest_count = request.json["ea_child_resident"]
	)

	db.session.add(guest5)

	guest6 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Non Resident Child"),
		guest_count = request.json["non_resident_child"]
	)

	db.session.add(guest6)

	guest7 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Infant"),
		guest_count = request.json["infant"]
	)

	db.session.add(guest7)

	try:
		additional_note = request.json["additional_note"]
	except KeyError as guest_key_error:
		print("There was an error getting the following key: " + str(guest_key_error))
		additional_note = None

	detail = Detail(
		booking_details_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		first_name = request.json["first_name"],
		last_name = request.json["last_name"],
		email_address = request.json["email"],
		phone_number = request.json["phone"],
		address = request.json["address"],
		additional_note = additional_note
	)

	db.session.add(detail)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The booking has been made. Please carry on.")

		return_data = {}
		return_data["booking_public_id"] = booking_id
		return_data["email"] = request.json["email"]

		return jsonify({"message": output, "data": return_data}), 201

	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("It's us, not you. There was an error while creating the express booking. :-( Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/bookings/express/view")
def view_all_express_bookings():
	return_bookings = db.session.query(Booking)\
								.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
								.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
								.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
												Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
												Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.session_id,\
												Booking.created_at, Booking.updated_at, Booking.status, Booking.booking_ref_code,\
												BookingType.booking_type_name,\
												BookingStatus.booking_status_name)\
								.filter(Booking.deletion_marker == None)\
								.filter(Booking.express_checkout == 1)\
								.order_by(Booking.booking_id.desc())\
								.all()

	if not return_bookings:
		output = []
		output.append("There are currently no bookings in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []
		id_array = []

		for each_id in return_bookings:
			id_array.append(each_id.session_id)

		try:
			return_user = requests.post(get_user_from_aumra,\
										json = {"users_ids": id_array})
		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			pass

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			return_data["booking_type"] = single.booking_type_name
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			guest_array = []

			get_all_guests = db.session.query(BookingGuest)\
									   .join(GuestType, BookingGuest.guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name,\
									   				BookingGuest.guest_count)\
									   .filter(BookingGuest.deletion_marker == None)\
									   .filter(BookingGuest.booking_id == single.booking_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.guest_count
				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.guest_count
				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.guest_count
				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.guest_count
				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.guest_count
				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.guest_count
				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.guest_count
					
			guest_data = {}
			guest_data["ea_adult_citizen"] = ea_adult_citizen
			guest_data["ea_adult_resident"] = ea_adult_resident
			guest_data["non_resident_adult"] = non_resident_adult
			guest_data["ea_child_citizen"] = ea_child_citizen
			guest_data["ea_child_resident"] = ea_child_resident
			guest_data["non_resident_child"] = non_resident_child
			guest_data["infant"] = infant

			guest_array.append(guest_data)

			return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
								 		+ ea_child_resident + non_resident_child + infant

			return_data["guests"] = guest_array
			return_data["booking_status"] = single.booking_status_name
			return_data["booking_status_id"] = single.status

			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						return_data["session_user"] = user["first_name"] + " " + user["last_name"]
						return_data["session_id"] = single.session_id
					
			except (IndexError) as user_error:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id
				
			except (AttributeError, UnboundLocalError) as network_related_errors:
				return_data["session_user"] = "Network Error"
				return_data["session_id"] = single.session_id
				
			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			output.append(return_data)

		return jsonify({"data": output}), 200


#############
#### Ref ####
#############
def get_booking_status_id(status_name):
	status_id = db.session.query(BookingStatus)\
						  .filter(BookingStatus.deletion_marker == None)\
						  .filter(BookingStatus.booking_status_name == status_name)\
						  .all()

	for single in status_id:
		booking_status_id = single.booking_status_public_id

		return booking_status_id


def get_booking_guest_type_id(guest_type_name):
	type_id = db.session.query(GuestType)\
						.filter(GuestType.deletion_marker == None)\
						.filter(GuestType.booking_guest_type_name == guest_type_name)\
						.all()

	for single in type_id:
		booking_guest_type_id = single.booking_guest_type_public_id

		return booking_guest_type_id