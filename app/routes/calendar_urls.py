import math
import os
import uuid
from datetime import date, datetime, timedelta

import pymysql
import requests
from flask import (Flask, json, jsonify, redirect, render_template, request,
                   url_for)

from database.booking_guest_types import GuestType
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_types import BookingType
from database.bookings import Booking
from database.facility import Facility
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_vehicles import GatepassVehicle
from database.inventory import Inventory
from database.invoice import Invoice
from database.mandatory_payments import Mandatory
from database.partner import Partner
from database.vehicle import Vehicle
from functions.async_functions import AsyncRequests
from functions.date_operators import *
# File imports
from routes import FromCache, app, db, db_cache
from routes.bookings_urls import get_booking_status_id
from routes.seed_functions import BookingStatusSeed, BookingTypeSeed
from variables import *


def close(self):
	self.session.close()


##################
#### Calendar ####
##################
@app.route("/calendar/view", methods = ["POST"])
def calendar_view():
	messages = []
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422
	
	facility_output = []
	inventory_output = []
	campsite_output = []
	accomodation_output = []
	summary_array = []
	date_array = []
	stuff_array = []

	starting = GenerateDateFromString.generateDate(request.json["start_date"])
	ending = GenerateDateFromString.generateDate(request.json["end_date"])
	add_day = timedelta(days=1)

	while starting <= ending:
		date_array.append(starting)

		starting += add_day

	###### Inventory Section ######
	try:
		# asyncRequests = AsyncRequests()
		
		# get_all_active_inventory = asyncRequests.launchGetRequest(url = get_active_inventory)
		
		get_inventory = requests.get(get_active_inventory)
		get_all_active_inventory = get_inventory.json()

		try:
			for single_inventory in get_all_active_inventory["data"]:
				return_inventory_info = {}
				return_inventory_info["inventory_id"] = single_inventory["public_id"]
				return_inventory_info["inventory_name"] = single_inventory["name"].title()
				
				max_guests = single_inventory["maximum_guests"]
				return_inventory_info["maximum_guests"] = max_guests

				inventory_details_array = []

				for each_date in date_array:
					get_all_bookings = db.session.query(Inventory)\
												   .join(Booking, Inventory.booking_id == Booking.booking_public_id)\
												   .add_columns(Inventory.inventory_booking_public_id, Inventory.inventory_booking_adults, Inventory.inventory_booking_children,\
																Inventory.booking_id)\
												   .filter(Inventory.deletion_marker == None)\
												   .filter(Inventory.inventory_id == single_inventory["public_id"])\
												   .filter(Inventory.inventory_booking_date == each_date)\
												   .filter(Inventory.status != get_booking_status_id("Updated"))\
												   .filter(Inventory.status != get_booking_status_id("Cancelled"))\
												   .filter(Booking.deletion_marker == None)\
												   .filter(Booking.status != get_booking_status_id("Cancelled"))\
												   .options(FromCache(db_cache))\
												   .all()

					return_inventory_data = {}

					active_unavailability = {}

					if single_inventory["unavailability_schedule_set"]:
						for each_schedule in single_inventory["unavailability_schedule"]:
							if (each_schedule["start_date_formatted"] <= each_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= each_date.strftime("%Y-%m-%d")):
								active_unavailability = each_schedule
							else:
								pass
					
					if not get_all_bookings:
						if active_unavailability:
							return_inventory_data["date"] = each_date
							return_inventory_data["available_slots"] = max_guests - active_unavailability["unavailability_number"]
							return_inventory_data["number_of_people"] = 0
							return_inventory_data["booking_details"] = []

							inventory_details_array.append(return_inventory_data)
							
						else:
							return_inventory_data["date"] = each_date
							return_inventory_data["available_slots"] = max_guests
							return_inventory_data["number_of_people"] = 0
							return_inventory_data["booking_details"] = []

							inventory_details_array.append(return_inventory_data)
					
					else:
						number_of_total_inventory_people = []
								
						for one_booking in get_all_bookings:
							number_of_total_inventory_people.append(one_booking.inventory_booking_adults + one_booking.inventory_booking_children)

						if active_unavailability:
							number_of_total_inventory_people.append(active_unavailability["unavailability_number"])
						
						return_inventory_data["date"] = each_date
						return_inventory_data["available_slots"] = max_guests - sum(number_of_total_inventory_people)
						return_inventory_data["number_of_people"] = sum(number_of_total_inventory_people)
						return_inventory_data["booking_details"] = []

						inventory_details_array.append(return_inventory_data)

				return_inventory_info["inventory_details"] = inventory_details_array

				inventory_output.append(return_inventory_info)
			
		except (KeyError):
			pass

	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		return jsonify({"message": "Error getting inventory details."})


	# get_all_active_accomodation = asyncRequests.launchGetRequest(url = get_active_facilities)
	
	get_accomodation = requests.get(get_active_facilities)
	get_all_active_accomodation = get_accomodation.json()
	
	###### Accomodation Section ######
	all_facility_bookings = []

	for single_accomodation in get_all_active_accomodation["data"]:
		if single_accomodation["facility_type_id"] == "Accomodation":
			all_facility_bookings.append(single_accomodation["public_id"])
	
	for facility_booking in set(all_facility_bookings):
		return_facility_info = {}

		try:
			return_facility = requests.get(get_facility_details.format(facility_booking))

			facility_type = return_facility.json()["data"][0]["facility_type_id"]

			if facility_type == "Accomodation":
				return_facility_info["accomodation_id"] = facility_booking
				return_facility_info["accomodation_name"] = return_facility.json()["data"][0]["name"].title()

				max_accomodation_guests = int(return_facility.json()["data"][0]["maximum_guests"])
				quantity = int(return_facility.json()["data"][0]["quantity"])
				return_facility_info["maximum_guests"] = quantity

				accomodation_details_array = []
				
				for each_date in date_array:
					get_all_bookings = db.session.query(Facility)\
												   .join(Booking, Facility.booking_id == Booking.booking_public_id)\
												   .add_columns(Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id)\
												   .filter(Facility.deletion_marker == None)\
												   .filter(Facility.facility_id == facility_booking)\
												   .filter(Facility.facility_booking_check_in_date <= each_date)\
												   .filter(Facility.facility_booking_check_out_date > each_date)\
												   .filter(Facility.status != get_booking_status_id("Updated"))\
												   .filter(Facility.status != get_booking_status_id("Cancelled"))\
												   .filter(Booking.deletion_marker == None)\
												   .filter(Booking.status != get_booking_status_id("Cancelled"))\
												   .options(FromCache(db_cache))\
												   .all()

					return_accomodation_data = {}

					accommodation_active_unavailability = {}

					if return_facility.json()["data"][0]["unavailability_schedule_set"]:
						for each_schedule in return_facility.json()["data"][0]["unavailability_schedule"]:
							if (each_schedule["start_date_formatted"] <= each_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= each_date.strftime("%Y-%m-%d")):
								accommodation_active_unavailability = each_schedule
							else:
								pass
					
					if not get_all_bookings:
						if accommodation_active_unavailability:
							return_accomodation_data["date"] = each_date
							return_accomodation_data["available_slots"] = quantity - accommodation_active_unavailability["unavailability_number"]
							return_accomodation_data["number_of_people"] = 0

							accomodation_details_array.append(return_accomodation_data)
						
						else:
							return_accomodation_data["date"] = each_date
							return_accomodation_data["available_slots"] = quantity
							return_accomodation_data["number_of_people"] = 0

							accomodation_details_array.append(return_accomodation_data)

					else:
						number_of_accomodation_people = []
						bookings_id = []

						for one_acc_booking in get_all_bookings:
							number_of_accomodation_people.append(one_acc_booking.facility_booking_adults + one_acc_booking.facility_booking_children)
							# bookings_id.append(one_acc_booking.booking_id)

						return_accomodation_data["date"] = each_date
						return_accomodation_data["bookings"] = bookings_id
						
						if return_facility.json()["data"][0]["accomodation_type_id"] == "Pelican":
							return_accomodation_data["available_slots"] = quantity - 1
							return_accomodation_data["number_of_people"] = sum(number_of_accomodation_people)
						elif return_facility.json()["data"][0]["accomodation_type_id"] == "Stables":
							space_available = max_accomodation_guests * quantity
							
							# guest_modulus = (space_available - sum(number_of_accomodation_people)) % max_accomodation_guests

							# if guest_modulus > 0:
							# 	pass

							if (sum(number_of_accomodation_people) == 1) & (sum(number_of_accomodation_people) < max_accomodation_guests):
								space_taken = sum(number_of_accomodation_people) + (max_accomodation_guests - sum(number_of_accomodation_people))
							else:
								space_taken = sum(number_of_accomodation_people)
							
							if accommodation_active_unavailability:
								return_accomodation_data["available_slots"] = round((float(space_available) - float(space_taken)) / float(max_accomodation_guests)) - accommodation_active_unavailability["unavailability_number"]
								return_accomodation_data["number_of_people"] = sum(number_of_accomodation_people)
							else:
								return_accomodation_data["available_slots"] = round((float(space_available) - float(space_taken)) / float(max_accomodation_guests))
								return_accomodation_data["number_of_people"] = sum(number_of_accomodation_people)

						accomodation_details_array.append(return_accomodation_data)

				return_facility_info["accomodation_details"] = accomodation_details_array
			else:
				pass

		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			return_facility_info["accomodation_id"] = facility_booking
			return_facility_info["accomodation_name"] = "Network error."
			return_facility_info["maximum_guests"] = "Network error."
		except (KeyError) as no_data:
			# return_facility_info["accomodation_id"] = facility_booking
			# return_facility_info["accomodation_name"] = return_facility.json()
			# return_facility_info["maximum_guests"] = return_facility.json()
			pass

		if return_facility_info:
			accomodation_output.append(return_facility_info)
		else:
			pass


	###### Camping Section ######
	all_camping_bookings = []

	for single_camping in get_all_active_accomodation["data"]:
		if single_camping["facility_type_id"] == "Camping Sites":
			all_camping_bookings.append(single_camping["public_id"])
	
	for camping_booking in set(all_camping_bookings):
		return_camping_info = {}

		try:
			return_facility = requests.get(get_facility_details.format(camping_booking))

			facility_type = return_facility.json()["data"][0]["facility_type_id"]

			if facility_type == "Camping Sites":
				return_camping_info["accomodation_id"] = camping_booking
				return_camping_info["accomodation_name"] = return_facility.json()["data"][0]["name"].title()

				# Since camping is exclusive and no quantity is set, it is necessary to set the quantity to 1
				quantity = 1
				max_accomodation_guests = return_facility.json()["data"][0]["maximum_guests"]
				return_camping_info["maximum_guests"] = max_accomodation_guests

				camping_details_array = []
				
				for each_date in date_array:
					get_all_bookings = db.session.query(Facility)\
												   .join(Booking, Facility.booking_id == Booking.booking_public_id)\
												   .add_columns(Facility.facility_booking_adults, Facility.facility_booking_children)\
												   .filter(Facility.deletion_marker == None)\
												   .filter(Facility.facility_id == camping_booking)\
												   .filter(Facility.facility_booking_check_in_date <= each_date)\
												   .filter(Facility.facility_booking_check_out_date > each_date)\
												   .filter(Facility.status != get_booking_status_id("Updated"))\
												   .filter(Facility.status != get_booking_status_id("Cancelled"))\
												   .filter(Booking.deletion_marker == None)\
												   .filter(Booking.status != get_booking_status_id("Cancelled"))\
												   .options(FromCache(db_cache))\
												   .all()

					return_camping_data = {}

					campsite_active_unavailability = {}

					if return_facility.json()["data"][0]["unavailability_schedule_set"]:
						for each_schedule in return_facility.json()["data"][0]["unavailability_schedule"]:
							if (each_schedule["start_date_formatted"] <= each_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= each_date.strftime("%Y-%m-%d")):
								campsite_active_unavailability = each_schedule
							else:
								pass
					
					if not get_all_bookings:
						if campsite_active_unavailability:
							return_camping_data["date"] = each_date
							return_camping_data["available_slots"] = 0
							return_camping_data["number_of_people"] = 0

							camping_details_array.append(return_camping_data)
						
						else:
							return_camping_data["date"] = each_date
							return_camping_data["available_slots"] = quantity
							return_camping_data["number_of_people"] = 0

							camping_details_array.append(return_camping_data)

					else:
						number_of_camping_people = []

						for one_camp_booking in get_all_bookings:
							number_of_camping_people.append(one_camp_booking.facility_booking_adults + one_camp_booking.facility_booking_children)

						return_camping_data["date"] = each_date
						return_camping_data["available_slots"] = quantity - quantity
						return_camping_data["number_of_people"] = sum(number_of_camping_people)

						camping_details_array.append(return_camping_data)

				return_camping_info["accomodation_details"] = camping_details_array
			else:
				pass

		except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
			return_camping_info["accomodation_id"] = camping_booking
			return_camping_info["accomodation_name"] = "Network error."
			return_camping_info["maximum_guests"] = "Network error."
		except (KeyError) as no_data:
			# return_camping_info["accomodation_id"] = camping_booking
			# return_camping_info["accomodation_name"] = return_facility.json()
			# return_camping_info["maximum_guests"] = return_facility.json()
			pass

		if return_camping_info:
			campsite_output.append(return_camping_info)
		else:
			pass

	try:
		inventory_output = sorted(inventory_output, key=lambda order: order["inventory_name"])
		accomodation_output = sorted(accomodation_output, key=lambda order: order["accomodation_name"])
		campsite_output = sorted(campsite_output, key=lambda order: order["accomodation_name"])
		
		return jsonify({"inventory": inventory_output, "accomodation": accomodation_output, "camping": campsite_output})
		# return jsonify({"inventory": inventory_output})
	except Exception:
		return jsonify({"message": "There are no details to display."}), 200


# def calendar_view():
#     messages = []

#     try:
#         request.json["start_date"].strip()
#         if not request.json["start_date"]:
#             messages.append("Start date is empty.")
#     except KeyError as e:
#         messages.append("Start date is missing.")

#     try:
#         request.json["end_date"].strip()
#         if not request.json["end_date"]:
#             messages.append("End date is empty.")
#     except KeyError as e:
#         messages.append("End date is missing.")

#     if messages:
#         return jsonify({"messages": messages}), 422

#     facility_output = []
#     inventory_output = []
#     campsite_output = []
#     accomodation_output = []
#     summary_array = []
#     date_array = []
#     stuff_array = []

#     starting = GenerateDateFromString.generateDate(request.json["start_date"])
#     ending = GenerateDateFromString.generateDate(request.json["end_date"])
#     add_day = timedelta(days=1)

#     while starting <= ending:
#         date_array.append(starting)

#         starting += add_day

#     ###### Inventory Section ######
#     try:
#         # asyncRequests = AsyncRequests()

#         # get_all_active_inventory = asyncRequests.launchGetRequest(url = get_active_inventory)

#         get_inventory = requests.get(get_active_inventory)
#         get_all_active_inventory = get_inventory.json()

#         try:
#             for single_inventory in get_all_active_inventory["data"]:
#                 return_inventory_info = {}
#                 return_inventory_info["inventory_id"] = single_inventory["public_id"]
#                 return_inventory_info["inventory_name"] = single_inventory["name"].title()

#                 max_guests = single_inventory["maximum_guests"]
#                 return_inventory_info["maximum_guests"] = max_guests

#                 inventory_details_array = []

#                 for each_date in date_array:
#                     get_all_bookings = db.session.query(Inventory)\
#                               .join(Booking, Inventory.booking_id == Booking.booking_public_id)\
#                               .add_columns(Inventory.inventory_booking_public_id, Inventory.inventory_booking_adults, Inventory.inventory_booking_children,\
#                                Inventory.booking_id)\
#                               .filter(Inventory.deletion_marker == None)\
#                               .filter(Inventory.inventory_id == single_inventory["public_id"])\
#                               .filter(Inventory.inventory_booking_date == each_date)\
#                               .filter(Inventory.status != get_booking_status_id("Updated"))\
#                               .filter(Inventory.status != get_booking_status_id("Cancelled"))\
#                               .filter(Booking.deletion_marker == None)\
#                               .filter(Booking.status != get_booking_status_id("Cancelled"))\
#                               .options(FromCache(db_cache))\
#                               .all()

#                     return_inventory_data = {}

#                     active_unavailability = {}

#                     if single_inventory["unavailability_schedule_set"]:
#                         for each_schedule in single_inventory["unavailability_schedule"]:
#                             if (each_schedule["start_date_formatted"] <= each_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= each_date.strftime("%Y-%m-%d")):
#                                 active_unavailability = each_schedule
#                             else:
#                                 pass

#                     if not get_all_bookings:
#                         if active_unavailability:
#                             return_inventory_data["date"] = each_date
#                             return_inventory_data["available_slots"] = max_guests - active_unavailability["unavailability_number"]
#                             return_inventory_data["number_of_people"] = 0
#                             return_inventory_data["booking_details"] = []

#                             inventory_details_array.append(return_inventory_data)

#                         else:
#                             return_inventory_data["date"] = each_date
#                             return_inventory_data["available_slots"] = max_guests
#                             return_inventory_data["number_of_people"] = 0
#                             return_inventory_data["booking_details"] = []

#                             inventory_details_array.append(return_inventory_data)

#                     else:
#                         number_of_total_inventory_people = []

#                         for one_booking in get_all_bookings:
#                             number_of_total_inventory_people.append(one_booking.inventory_booking_adults + one_booking.inventory_booking_children)

#                         if active_unavailability:
#                             number_of_total_inventory_people.append(active_unavailability["unavailability_number"])

#                         return_inventory_data["date"] = each_date
#                         return_inventory_data["available_slots"] = max_guests - sum(number_of_total_inventory_people)
#                         return_inventory_data["number_of_people"] = sum(number_of_total_inventory_people)
#                         return_inventory_data["booking_details"] = []

#                         inventory_details_array.append(return_inventory_data)

#                 return_inventory_info["inventory_details"] = inventory_details_array

#                 inventory_output.append(return_inventory_info)

#         except (KeyError):
#             pass

#     except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
#         return jsonify({"message": "Error getting inventory details."})


#     # get_all_active_accomodation = asyncRequests.launchGetRequest(url = get_active_facilities)

#     get_accomodation = requests.get(get_active_facilities)
#     get_all_active_accomodation = get_accomodation.json()

#     ###### Accomodation Section ######
#     all_facility_bookings = []

#     for single_accomodation in get_all_active_accomodation["data"]:
#         if single_accomodation["facility_type_id"] == "Accomodation":
#             all_facility_bookings.append(single_accomodation["public_id"])

#     # print("all_facility_bookings ")
#     # print(all_facility_bookings)

#     for facility_booking in set(all_facility_bookings):
#         return_facility_info = {}

#         try:
#             return_facility = requests.get(get_facility_details.format(facility_booking))
#             facility_type = return_facility.json()["data"][0]["facility_type_id"]

#             if facility_type == "Accomodation":
#                 return_facility_info["accomodation_id"] = facility_booking
#                 return_facility_info["accomodation_name"] = return_facility.json()["data"][0]["name"].title()

#                 max_accomodation_guests = int(return_facility.json()["data"][0]["maximum_guests"])

#                 quantity = int(return_facility.json()["data"][0]["quantity"])

#                 return_facility_info["maximum_guests"] = quantity

#                 accomodation_details_array = []

#                 for each_date in date_array:
#                     get_all_bookings = db.session.query(Facility)\
#                               .join(Booking, Facility.booking_id == Booking.booking_public_id)\
#                               .add_columns(Facility.facility_id,Facility.facility_booking_adults, Facility.facility_booking_children, Facility.booking_id,Facility.facility_booked_rooms)\
#                               .filter(Facility.deletion_marker == None)\
#                               .filter(Facility.facility_id == facility_booking)\
#                               .filter(Facility.facility_booking_check_in_date <= each_date)\
#                               .filter(Facility.facility_booking_check_out_date > each_date)\
#                               .filter(Facility.status != get_booking_status_id("Updated"))\
#                               .filter(Facility.status != get_booking_status_id("Cancelled"))\
#                               .filter(Booking.deletion_marker == None)\
#                               .filter(Booking.status != get_booking_status_id("Cancelled"))\
#                               .options(FromCache(db_cache))\
#                               .all()



#                     return_accomodation_data = {}
#                     accommodation_active_unavailability = {}

#                     if return_facility.json()["data"][0]["unavailability_schedule_set"]:
#                         for each_schedule in return_facility.json()["data"][0]["unavailability_schedule"]:
#                             if (each_schedule["start_date_formatted"] <= each_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= each_date.strftime("%Y-%m-%d")):
#                                 accommodation_active_unavailability = each_schedule
#                             else:
#                                 pass

#                     if not get_all_bookings:
#                         if accommodation_active_unavailability:
#                             return_accomodation_data["date"] = each_date
#                             return_accomodation_data["available_slots"] = quantity - accommodation_active_unavailability["unavailability_number"]
#                             return_accomodation_data["number_of_people"] = 0
#                             accomodation_details_array.append(return_accomodation_data)

#                         else:
#                             return_accomodation_data["date"] = each_date
#                             return_accomodation_data["available_slots"] = quantity
#                             return_accomodation_data["number_of_people"] = 0
#                             accomodation_details_array.append(return_accomodation_data)

#                     else:
#                         number_of_accomodation_people = []
#                         bookings_id = []
#                         number_of_occupied_rooms = []

#                         for one_acc_booking in get_all_bookings:
#                             number_of_accomodation_people.append(one_acc_booking.facility_booking_adults + one_acc_booking.facility_booking_children)
#                             total_guest_number = one_acc_booking.facility_booking_adults + one_acc_booking.facility_booking_children

#                             if(one_acc_booking.facility_id =="9221ced6-4704-4a9c-a327-d5771f3434d8"):

#                                 if one_acc_booking.facility_booked_rooms == None:
#                                     if(total_guest_number > max_accomodation_guests):

#                                         number_of_occupied_rooms.append(2)
#                                     else:
#                                         number_of_occupied_rooms.append(1)

#                                 else:
#                                     number_of_occupied_rooms.append(
#                                         one_acc_booking.facility_booked_rooms)

#                             elif( one_acc_booking.facility_id =="35b57e2b-ee6c-4c84-8bb3-183f3c8b92a2"):

#                                 if one_acc_booking.facility_booked_rooms == None:
#                                     if(total_guest_number > max_accomodation_guests):

#                                         number_of_occupied_rooms.append(2)
#                                     else:
#                                         number_of_occupied_rooms.append(1)

#                                 else:
#                                     number_of_occupied_rooms.append(
#                                         one_acc_booking.facility_booked_rooms)

#                         # bookings_id.append(one_acc_booking.booking_id)

#                         return_accomodation_data["date"] = each_date
#                         return_accomodation_data["bookings"] = bookings_id

#                         if return_facility.json()["data"][0]["accomodation_type_id"] == "Pelican":
#                             return_accomodation_data["available_slots"] = quantity - 1
#                             return_accomodation_data["number_of_people"] = sum(number_of_accomodation_people)

#                         elif return_facility.json()["data"][0]["accomodation_type_id"] == "Stables":
#                             space_available = max_accomodation_guests * quantity

#                             if(facility_booking == "9221ced6-4704-4a9c-a327-d5771f3434d8"):
#                                 number_of_rooms_available =  quantity
#                                 space_taken = sum(number_of_occupied_rooms)

#                             elif(facility_booking == "35b57e2b-ee6c-4c84-8bb3-183f3c8b92a2"):
#                                 number_of_rooms_available =  quantity
#                                 space_taken = sum(number_of_occupied_rooms)

#                             else:
#                                 if (sum(number_of_accomodation_people) == 1) & (sum(number_of_accomodation_people) < max_accomodation_guests):
#                                     space_taken = sum(number_of_accomodation_people) + (max_accomodation_guests - sum(number_of_accomodation_people))
#                                 else:
#                                     space_taken = sum(number_of_accomodation_people)

#                             if accommodation_active_unavailability:
#                                 if(facility_booking == "9221ced6-4704-4a9c-a327-d5771f3434d8" or facility_booking == "35b57e2b-ee6c-4c84-8bb3-183f3c8b92a2"):
#                                     return_accomodation_data["available_slots"] = round((float(number_of_rooms_available) - float(space_taken))) - accommodation_active_unavailability["unavailability_number"]
#                                 else:
#                                     return_accomodation_data["available_slots"] = round((float(space_available) - float(space_taken)) / float(max_accomodation_guests)) - accommodation_active_unavailability["unavailability_number"]
#                                 return_accomodation_data["number_of_people"] = sum(number_of_accomodation_people)
#                             else:
#                                 if(facility_booking == "9221ced6-4704-4a9c-a327-d5771f3434d8"):
#                                     return_accomodation_data["available_slots"] = round((float(number_of_rooms_available) - float(space_taken)))
#                                 elif (facility_booking ==
#                                         "35b57e2b-ee6c-4c84-8bb3-183f3c8b92a2"
#                                     ):

#                                     return_accomodation_data[
#                                         "available_slots"] = round(
#                                             (float(number_of_rooms_available) -
#                                              float(space_taken)))
#                                 else:
#                                     return_accomodation_data["available_slots"] = round((float(space_available) - float(space_taken)) / float(max_accomodation_guests))
#                                 return_accomodation_data["number_of_people"] = sum(number_of_accomodation_people)

#                         accomodation_details_array.append(return_accomodation_data)

#                 return_facility_info["accomodation_details"] = accomodation_details_array
#             else:
#                 pass

#         except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
#             return_facility_info["accomodation_id"] = facility_booking
#             return_facility_info["accomodation_name"] = "Network error."
#             return_facility_info["maximum_guests"] = "Network error."
#         except (KeyError) as no_data:
#             # return_facility_info["accomodation_id"] = facility_booking
#             # return_facility_info["accomodation_name"] = return_facility.json()
#             # return_facility_info["maximum_guests"] = return_facility.json()
#             pass

#         if return_facility_info:
#             accomodation_output.append(return_facility_info)
#         else:
#             pass


#     ###### Camping Section ######
#     all_camping_bookings = []

#     for single_camping in get_all_active_accomodation["data"]:
#         if single_camping["facility_type_id"] == "Camping Sites":
#             all_camping_bookings.append(single_camping["public_id"])

#     for camping_booking in set(all_camping_bookings):
#         return_camping_info = {}

#         try:
#             return_facility = requests.get(get_facility_details.format(camping_booking))

#             facility_type = return_facility.json()["data"][0]["facility_type_id"]

#             if facility_type == "Camping Sites":
#                 return_camping_info["accomodation_id"] = camping_booking
#                 return_camping_info["accomodation_name"] = return_facility.json()["data"][0]["name"].title()

#                 # Since camping is exclusive and no quantity is set, it is necessary to set the quantity to 1
#                 quantity = 1
#                 max_accomodation_guests = return_facility.json()["data"][0]["maximum_guests"]
#                 return_camping_info["maximum_guests"] = max_accomodation_guests

#                 camping_details_array = []

#                 for each_date in date_array:
#                     get_all_bookings = db.session.query(Facility)\
#                               .join(Booking, Facility.booking_id == Booking.booking_public_id)\
#                               .add_columns(Facility.facility_booking_adults, Facility.facility_booking_children)\
#                               .filter(Facility.deletion_marker == None)\
#                               .filter(Facility.facility_id == camping_booking)\
#                               .filter(Facility.facility_booking_check_in_date <= each_date)\
#                               .filter(Facility.facility_booking_check_out_date > each_date)\
#                               .filter(Facility.status != get_booking_status_id("Updated"))\
#                               .filter(Facility.status != get_booking_status_id("Cancelled"))\
#                               .filter(Booking.deletion_marker == None)\
#                               .filter(Booking.status != get_booking_status_id("Cancelled"))\
#                               .options(FromCache(db_cache))\
#                               .all()

#                     return_camping_data = {}

#                     campsite_active_unavailability = {}

#                     if return_facility.json()["data"][0]["unavailability_schedule_set"]:
#                         for each_schedule in return_facility.json()["data"][0]["unavailability_schedule"]:
#                             if (each_schedule["start_date_formatted"] <= each_date.strftime("%Y-%m-%d")) & (each_schedule["end_date_formatted"] >= each_date.strftime("%Y-%m-%d")):
#                                 campsite_active_unavailability = each_schedule
#                             else:
#                                 pass

#                     if not get_all_bookings:
#                         if campsite_active_unavailability:
#                             return_camping_data["date"] = each_date
#                             return_camping_data["available_slots"] = 0
#                             return_camping_data["number_of_people"] = 0

#                             camping_details_array.append(return_camping_data)

#                         else:
#                             return_camping_data["date"] = each_date
#                             return_camping_data["available_slots"] = quantity
#                             return_camping_data["number_of_people"] = 0

#                             camping_details_array.append(return_camping_data)

#                     else:
#                         number_of_camping_people = []

#                         for one_camp_booking in get_all_bookings:
#                             number_of_camping_people.append(one_camp_booking.facility_booking_adults + one_camp_booking.facility_booking_children)

#                         return_camping_data["date"] = each_date
#                         return_camping_data["available_slots"] = quantity - quantity
#                         return_camping_data["number_of_people"] = sum(number_of_camping_people)

#                         camping_details_array.append(return_camping_data)

#                 return_camping_info["accomodation_details"] = camping_details_array
#             else:
#                 pass

#         except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
#             return_camping_info["accomodation_id"] = camping_booking
#             return_camping_info["accomodation_name"] = "Network error."
#             return_camping_info["maximum_guests"] = "Network error."
#         except (KeyError) as no_data:
#             # return_camping_info["accomodation_id"] = camping_booking
#             # return_camping_info["accomodation_name"] = return_facility.json()
#             # return_camping_info["maximum_guests"] = return_facility.json()
#             pass

#         if return_camping_info:
#             campsite_output.append(return_camping_info)
#         else:
#             pass

#     try:
#         inventory_output = sorted(inventory_output, key=lambda order: order["inventory_name"])
#         accomodation_output = sorted(accomodation_output, key=lambda order: order["accomodation_name"])
#         campsite_output = sorted(campsite_output, key=lambda order: order["accomodation_name"])

#         return jsonify({"inventory": inventory_output, "accomodation": accomodation_output, "camping": campsite_output})
#         # return jsonify({"inventory": inventory_output})
#     except Exception:
#         return jsonify({"message": "There are no details to display."}), 200



@app.route("/calendar/entry/view", methods = ["POST"])
def view_entry_fees():
	messages = []
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	date_array = []
	starting = GenerateDateFromString.generateDate(request.json["start_date"])
	ending = GenerateDateFromString.generateDate(request.json["end_date"])
	add_day = timedelta(days=1)

	while starting <= ending:
		date_array.append(starting)

		starting += add_day

	get_vehicle_entry_fees = db.session.query(Vehicle)\
									   .filter(Vehicle.deletion_marker == None)\
									   .order_by(Vehicle.vehicle_charge_id.asc())\
									   .options(FromCache(db_cache))\
									   .all()

	vehicle_data = []

	for single_vehicle in get_vehicle_entry_fees:
		return_vehicle_data = {}
		return_vehicle_data["vehicle_charge_public_id"] = single_vehicle.vehicle_charge_public_id
		return_vehicle_data["vehicle_charge_category"] = single_vehicle.vehicle_charge_category
		return_vehicle_data["capacity"] = "∞"
		
		vehicle_details_array = []
		
		for each_date in date_array:
			get_booking_vehicles = db.session.query(GatepassVehicle)\
											 .join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
											 .add_columns(GatepassVehicle.gatepass_vehicle_count,\
												 		  Gatepass.booking_id)\
											 .filter(GatepassVehicle.deletion_marker == None)\
											 .filter(GatepassVehicle.gatepass_vehicle_type == single_vehicle.vehicle_charge_public_id)\
											 .filter(GatepassVehicle.gatepass_vehicle_count > 0)\
											 .filter(GatepassVehicle.status != get_booking_status_id("Cancelled"))\
											 .filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
											 .filter(Gatepass.start_date <= each_date)\
											 .filter(Gatepass.end_date >= each_date)\
											 .filter(Gatepass.deletion_marker == None)\
											 .filter(Gatepass.status != get_booking_status_id("Cancelled"))\
											 .filter(Gatepass.status != get_booking_status_id("Updated"))\
											 .options(FromCache(db_cache))\
											 .all()

			vehicle_details = {}
			vehicle_details["date"] = each_date

			if not get_booking_vehicles:
				vehicle_details["vehicle_count"] = 0
				vehicle_details["booking_details"] = []

			else:
				count_sum = []
				bookings = []
				for single_booking_vehicle in get_booking_vehicles:
					count_sum.append(float(single_booking_vehicle.gatepass_vehicle_count))
					# bookings.append(single_booking_vehicle.booking_id)

				vehicle_details["vehicle_count"] = sum(count_sum)
				vehicle_details["booking_details"] = bookings

			vehicle_details_array.append(vehicle_details)

		return_vehicle_data["vehicle_details"] = vehicle_details_array

		vehicle_data.append(return_vehicle_data)
		
	get_conservancy_fees = db.session.query(Mandatory)\
									 .filter(Mandatory.deletion_marker == None)\
									 .order_by(Mandatory.payment_person.asc())\
									 .options(FromCache(db_cache))\
									 .all()

	conservancy_fee_data = []

	for single in get_conservancy_fees:
		return_conservancy_data = {}
		return_conservancy_data["payment_public_id"] = single.payment_public_id
		return_conservancy_data["payment_person"] = single.payment_person
		return_conservancy_data["capacity"] = "∞"

		conservancy_fee_details_array = []

		for each_date in date_array:
			get_conservancy_bookings = db.session.query(GatepassGuest)\
												 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
												 .add_columns(GatepassGuest.gatepass_guest_count,\
												 		  	  Gatepass.booking_id)\
												 .filter(GatepassGuest.deletion_marker == None)\
												 .filter(GatepassGuest.gatepass_guest_type == single.payment_public_id)\
												 .filter(GatepassGuest.gatepass_guest_count > 0)\
												 .filter(GatepassGuest.status != get_booking_status_id("Cancelled"))\
												 .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
												 .filter(Gatepass.start_date <= each_date)\
												 .filter(Gatepass.end_date >= each_date)\
												 .filter(Gatepass.deletion_marker == None)\
												 .filter(Gatepass.status != get_booking_status_id("Cancelled"))\
												 .filter(Gatepass.status != get_booking_status_id("Updated"))\
												 .options(FromCache(db_cache))\
												 .all()

			conservancy_fee_details = {}
			conservancy_fee_details["date"] = each_date

			if not get_conservancy_bookings:
				conservancy_fee_details["conservancy_fee_count"] = 0
				conservancy_fee_details["booking_details"] = []

			else:
				count_sum = []
				bookings = []
				for single_booking_fee in get_conservancy_bookings:
					count_sum.append(float(single_booking_fee.gatepass_guest_count))
					# bookings.append(single_booking_fee.booking_id)

				conservancy_fee_details["conservancy_fee_count"] = sum(count_sum)
				conservancy_fee_details["booking_details"] = bookings

			conservancy_fee_details_array.append(conservancy_fee_details)

		return_conservancy_data["conservancy_fee_details"] = conservancy_fee_details_array

		conservancy_fee_data.append(return_conservancy_data)

	return jsonify({"vehicles": vehicle_data, "conservancy_fees": conservancy_fee_data}), 200


@app.route("/bookings/vehicles/list", methods = ["POST"])
def list_vehicle_bookings():
	messages = []

	try:
		request.json["vehicle_id"].strip()
		if not request.json["vehicle_id"]:
			messages.append("Vehicle ID is empty.")
	except KeyError as e:
		messages.append("Vehicle ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	requested_date = GenerateDateFromString.generateDate(request.json["date"])

	return_bookings = db.session.query(GatepassVehicle)\
								.join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
								.add_columns(GatepassVehicle.gatepass_vehicle_public_id, GatepassVehicle.gatepass_vehicle_type, GatepassVehicle.gatepass_vehicle_count,\
											 GatepassVehicle.gatepass_vehicle_no_of_nights,\
											 Gatepass.booking_id)\
								.filter(GatepassVehicle.deletion_marker == None)\
								.filter(GatepassVehicle.gatepass_vehicle_count > 0)\
								.filter(GatepassVehicle.gatepass_vehicle_type == request.json["vehicle_id"])\
								.filter(GatepassVehicle.status != get_booking_status_id("Cancelled"))\
								.filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.status != get_booking_status_id("Cancelled"))\
								.filter(Gatepass.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.start_date <= requested_date)\
								.filter(Gatepass.end_date >= requested_date)\
								.all()

	if not return_bookings:
		message = []
		message.append("There are no vehicles arriving on the selected date: " + requested_date.strftime("%d %b %Y"))
		return jsonify({"message": message}), 412

	data = []

	for single in return_bookings:
		return_data = {}
		return_data["gatepass_vehicle_count"] = int(single.gatepass_vehicle_count)

		get_booking = db.session.query(Booking)\
								.filter(Booking.booking_public_id == single.booking_id)\
								.first()

		return_data["booking_public_id"] = get_booking.booking_public_id
			
		booking_type = db.session.query(BookingType)\
								 .filter(BookingType.deletion_marker == None)\
								 .filter(BookingType.booking_type_public_id == get_booking.booking_type)\
								 .first()

		return_data["booking_type"] = booking_type.booking_type_name

		return_data["booking_type_id"] = get_booking.booking_type
		return_data["booking_check_in_date"] = get_booking.booking_check_in_date
		return_data["booking_check_out_date"] = get_booking.booking_check_out_date
		return_data["booking_done_by"] = get_booking.booking_done_by
		return_data["booking_ref_code"] = get_booking.booking_ref_code

		guest_array = []
		
		get_all_guests = db.session.query(GatepassGuest)\
								   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
								   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
												GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
												GatepassGuest.gatepass_no_of_nights)\
								   .filter(GatepassGuest.deletion_marker == None)\
								   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								   .filter(Gatepass.booking_id == single.booking_id)\
								   .options(FromCache(db_cache))\
								   .all()

		for each_guest in get_all_guests:
			guest_data = {}
			guest_data["guest_type"] = each_guest.payment_person
			guest_data["no_of_guests"] = each_guest.gatepass_guest_count

			guest_array.append(guest_data)

		return_data["guests"] = guest_array
		## The naming of this key is simply for convenience when working with the UI
		return_data["guest_total"] = int(single.gatepass_vehicle_count)

		check_to_invoice = db.session.query(Invoice)\
									 .filter(Invoice.deletion_marker == None)\
									 .filter(Invoice.booking_id == single.booking_id)\
									 .first()
		
		booking_status = db.session.query(BookingStatus)\
									   .filter(BookingStatus.deletion_marker == None)\
									   .filter(BookingStatus.booking_status_public_id == get_booking.status)\
									   .first()

		if check_to_invoice:
			if get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if get_booking.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif get_booking.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if booking_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif booking_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif booking_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif booking_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif booking_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif booking_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif booking_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = booking_status.booking_status_name
		return_data["booking_status_id"] = get_booking.status
		
		return_data["created_at"] = get_booking.created_at
		return_data["updated_at"] = get_booking.updated_at
		
		if get_booking.session_id:
			get_user = requests.get(get_single_user.format(get_booking.session_id))

			return_data["session_user"] = get_user.json()["first_name"] + " " + get_user.json()["last_name"]
			return_data["session_id"] = get_booking.session_id

		else:
			return_data["session_user"] = None
			return_data["session_id"] = None
		
		data.append(return_data)

	return jsonify({"data": data}), 200


@app.route("/bookings/guests/list", methods = ["POST"])
def list_guest_bookings():
	messages = []

	try:
		request.json["guest_id"].strip()
		if not request.json["guest_id"]:
			messages.append("Conservancy category ID is empty.")
	except KeyError as e:
		messages.append("Conservancy category ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	requested_date = GenerateDateFromString.generateDate(request.json["date"])

	return_bookings = db.session.query(GatepassGuest)\
								.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								.add_columns(GatepassGuest.gatepass_guest_public_id, GatepassGuest.gatepass_guest_type, GatepassGuest.gatepass_guest_count,\
											 GatepassGuest.gatepass_no_of_nights,\
											 Gatepass.booking_id)\
								.filter(GatepassGuest.deletion_marker == None)\
								.filter(GatepassGuest.gatepass_guest_count > 0)\
								.filter(GatepassGuest.gatepass_guest_type == request.json["guest_id"])\
								.filter(GatepassGuest.status != get_booking_status_id("Cancelled"))\
								.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.status != get_booking_status_id("Cancelled"))\
								.filter(Gatepass.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.start_date <= requested_date)\
								.filter(Gatepass.end_date >= requested_date)\
								.all()

	if not return_bookings:
		message = []
		message.append("There are no guests arriving on the selected date: " + requested_date.strftime("%d %b %Y"))
		return jsonify({"message": message}), 412

	data = []

	for single in return_bookings:
		return_data = {}
		return_data["gatepass_guest_count"] = int(single.gatepass_guest_count)

		get_booking = db.session.query(Booking)\
								.filter(Booking.booking_public_id == single.booking_id)\
								.first()

		return_data["booking_public_id"] = get_booking.booking_public_id
			
		booking_type = db.session.query(BookingType)\
								 .filter(BookingType.deletion_marker == None)\
								 .filter(BookingType.booking_type_public_id == get_booking.booking_type)\
								 .first()

		return_data["booking_type"] = booking_type.booking_type_name

		return_data["booking_type_id"] = get_booking.booking_type
		return_data["booking_check_in_date"] = get_booking.booking_check_in_date
		return_data["booking_check_out_date"] = get_booking.booking_check_out_date
		return_data["booking_done_by"] = get_booking.booking_done_by
		return_data["booking_ref_code"] = get_booking.booking_ref_code

		guest_array = []
		
		get_all_guests = db.session.query(GatepassGuest)\
								   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
								   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
												GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
												GatepassGuest.gatepass_no_of_nights)\
								   .filter(GatepassGuest.deletion_marker == None)\
								   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								   .filter(Gatepass.booking_id == single.booking_id)\
								   .options(FromCache(db_cache))\
								   .all()

		for each_guest in get_all_guests:
			guest_data = {}
			guest_data["guest_type"] = each_guest.payment_person
			guest_data["no_of_guests"] = each_guest.gatepass_guest_count

			guest_array.append(guest_data)

		return_data["guests"] = guest_array
		## The naming of this key is simply for convenience when working with the UI
		return_data["guest_total"] = int(single.gatepass_guest_count)

		check_to_invoice = db.session.query(Invoice)\
									 .filter(Invoice.deletion_marker == None)\
									 .filter(Invoice.booking_id == single.booking_id)\
									 .first()
		
		booking_status = db.session.query(BookingStatus)\
									   .filter(BookingStatus.deletion_marker == None)\
									   .filter(BookingStatus.booking_status_public_id == get_booking.status)\
									   .first()

		if check_to_invoice:
			if get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if get_booking.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif get_booking.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if booking_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif booking_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif booking_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif booking_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif booking_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif booking_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif booking_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = booking_status.booking_status_name
		return_data["booking_status_id"] = get_booking.status

		return_data["created_at"] = get_booking.created_at
		return_data["updated_at"] = get_booking.updated_at

		if get_booking.session_id:
			get_user = requests.get(get_single_user.format(get_booking.session_id))

			return_data["session_user"] = get_user.json()["first_name"] + " " + get_user.json()["last_name"]
			return_data["session_id"] = get_booking.session_id

		else:
			return_data["session_user"] = None
			return_data["session_id"] = None
		
		data.append(return_data)

	return jsonify({"data": data}), 200


@app.route("/calendar/view/partner", methods = ["POST"])
def view_partner_calendar():
	messages = []
	
	try:
		request.json["partner_id"].strip()
		if not request.json["partner_id"]:
			messages.append("Partner ID is empty.")
	except KeyError as e:
		messages.append("Partner ID is missing.")
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422
	
	facility_output = []
	inventory_output = []
	campsite_output = []
	accomodation_output = []
	summary_array = []
	date_array = []
	stuff_array = []

	starting = GenerateDateFromString.generateDate(request.json["start_date"])
	ending = GenerateDateFromString.generateDate(request.json["end_date"])
	add_day = timedelta(days=1)

	while starting <= ending:
		date_array.append(starting)

		starting += add_day

	###### Inventory Section ######
	try:
		asyncRequests = AsyncRequests()
		
		get_all_active_inventory = asyncRequests.launchGetRequest(url = get_active_inventory)
		
		try:
			for single_inventory in get_all_active_inventory["data"]:
				return_inventory_info = {}
				return_inventory_info["inventory_id"] = single_inventory["public_id"]
				return_inventory_info["inventory_name"] = single_inventory["name"].title()
				
				inventory_details_array = []

				for each_date in date_array:
					get_all_bookings = db.session.query(Inventory)\
												 .join(Booking, Inventory.booking_id == Booking.booking_public_id)\
												 .join(Partner, Inventory.booking_id == Partner.booking_id)\
												 .add_columns(Inventory.inventory_booking_public_id, Inventory.inventory_booking_adults, Inventory.inventory_booking_children,\
															  Inventory.inventory_booking_extra_adults, Inventory.inventory_booking_extra_children, Inventory.booking_id)\
												 .filter(Inventory.deletion_marker == None)\
												 .filter(Inventory.inventory_id == single_inventory["public_id"])\
												 .filter(Inventory.inventory_booking_date == each_date)\
												 .filter(Inventory.status != get_booking_status_id("Updated"))\
												 .filter(Inventory.status != get_booking_status_id("Cancelled"))\
												 .filter(Booking.deletion_marker == None)\
												 .filter(Booking.status != get_booking_status_id("Cancelled"))\
												 .filter(Partner.partner_id == request.json["partner_id"])\
												 .options(FromCache(db_cache))\
												 .all()

					return_inventory_data = {}

					if not get_all_bookings:
						return_inventory_data["date"] = each_date
						return_inventory_data["number_of_people"] = 0
						
						inventory_details_array.append(return_inventory_data)
					
					else:
						number_of_total_inventory_people = []
						
						for one_booking in get_all_bookings:
							number_of_total_inventory_people.append(one_booking.inventory_booking_adults + one_booking.inventory_booking_children + one_booking.inventory_booking_extra_adults + one_booking.inventory_booking_extra_children)

						return_inventory_data["date"] = each_date
						return_inventory_data["number_of_people"] = sum(number_of_total_inventory_people)
						
						inventory_details_array.append(return_inventory_data)

				return_inventory_info["inventory_details"] = inventory_details_array

				inventory_output.append(return_inventory_info)
			
		except (KeyError):
			pass

	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		return jsonify({"message": "Error getting inventory details."})


	try:
		inventory_output = sorted(inventory_output, key=lambda order: order["inventory_name"])
		
		return jsonify({"inventory": inventory_output})

	except Exception:
		return jsonify({"message": "There are no details to display."}), 200


@app.route("/calendar/entry/view/partner", methods = ["POST"])
def view_partner_entry_fees():
	messages = []
	
	try:
		request.json["partner_id"].strip()
		if not request.json["partner_id"]:
			messages.append("Partner ID is empty.")
	except KeyError as e:
		messages.append("Partner ID is missing.")
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	date_array = []
	starting = GenerateDateFromString.generateDate(request.json["start_date"])
	ending = GenerateDateFromString.generateDate(request.json["end_date"])
	add_day = timedelta(days=1)

	while starting <= ending:
		date_array.append(starting)

		starting += add_day

	get_vehicle_entry_fees = db.session.query(Vehicle)\
									   .filter(Vehicle.deletion_marker == None)\
									   .order_by(Vehicle.vehicle_charge_id.asc())\
									   .options(FromCache(db_cache))\
									   .all()

	vehicle_data = []

	for single_vehicle in get_vehicle_entry_fees:
		return_vehicle_data = {}
		return_vehicle_data["vehicle_charge_public_id"] = single_vehicle.vehicle_charge_public_id
		return_vehicle_data["vehicle_charge_category"] = single_vehicle.vehicle_charge_category
		
		vehicle_details_array = []
		
		for each_date in date_array:
			get_booking_vehicles = db.session.query(GatepassVehicle)\
											 .join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
											 .join(Partner, Gatepass.booking_id == Partner.booking_id)\
											 .add_columns(GatepassVehicle.gatepass_vehicle_count,\
												 		  Gatepass.booking_id)\
											 .filter(GatepassVehicle.deletion_marker == None)\
											 .filter(GatepassVehicle.gatepass_vehicle_type == single_vehicle.vehicle_charge_public_id)\
											 .filter(GatepassVehicle.gatepass_vehicle_count > 0)\
											 .filter(GatepassVehicle.status != get_booking_status_id("Cancelled"))\
											 .filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
											 .filter(Gatepass.start_date <= each_date)\
											 .filter(Gatepass.end_date >= each_date)\
											 .filter(Gatepass.deletion_marker == None)\
											 .filter(Gatepass.status != get_booking_status_id("Cancelled"))\
											 .filter(Gatepass.status != get_booking_status_id("Updated"))\
											 .filter(Partner.partner_id == request.json["partner_id"])\
											 .options(FromCache(db_cache))\
											 .all()

			vehicle_details = {}
			vehicle_details["date"] = each_date

			if not get_booking_vehicles:
				vehicle_details["vehicle_count"] = 0

			else:
				count_sum = []
				for single_booking_vehicle in get_booking_vehicles:
					count_sum.append(float(single_booking_vehicle.gatepass_vehicle_count))

				vehicle_details["vehicle_count"] = sum(count_sum)

			vehicle_details_array.append(vehicle_details)

		return_vehicle_data["vehicle_details"] = vehicle_details_array

		vehicle_data.append(return_vehicle_data)
		
	get_conservancy_fees = db.session.query(Mandatory)\
									 .filter(Mandatory.deletion_marker == None)\
									 .order_by(Mandatory.payment_person.asc())\
									 .options(FromCache(db_cache))\
									 .all()

	conservancy_fee_data = []

	for single in get_conservancy_fees:
		return_conservancy_data = {}
		return_conservancy_data["payment_public_id"] = single.payment_public_id
		return_conservancy_data["payment_person"] = single.payment_person

		conservancy_fee_details_array = []

		for each_date in date_array:
			get_conservancy_bookings = db.session.query(GatepassGuest)\
												 .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
												 .join(Partner, Gatepass.booking_id == Partner.booking_id)\
												 .add_columns(GatepassGuest.gatepass_guest_count,\
												 		  	  Gatepass.booking_id)\
												 .filter(GatepassGuest.deletion_marker == None)\
												 .filter(GatepassGuest.gatepass_guest_type == single.payment_public_id)\
												 .filter(GatepassGuest.gatepass_guest_count > 0)\
												 .filter(GatepassGuest.status != get_booking_status_id("Cancelled"))\
												 .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
												 .filter(Gatepass.start_date <= each_date)\
												 .filter(Gatepass.end_date >= each_date)\
												 .filter(Gatepass.deletion_marker == None)\
												 .filter(Gatepass.status != get_booking_status_id("Cancelled"))\
												 .filter(Gatepass.status != get_booking_status_id("Updated"))\
												 .filter(Partner.partner_id == request.json["partner_id"])\
												 .options(FromCache(db_cache))\
												 .all()

			conservancy_fee_details = {}
			conservancy_fee_details["date"] = each_date

			if not get_conservancy_bookings:
				conservancy_fee_details["conservancy_fee_count"] = 0

			else:
				count_sum = []
				for single_booking_fee in get_conservancy_bookings:
					count_sum.append(float(single_booking_fee.gatepass_guest_count))

				conservancy_fee_details["conservancy_fee_count"] = sum(count_sum)

			conservancy_fee_details_array.append(conservancy_fee_details)

		return_conservancy_data["conservancy_fee_details"] = conservancy_fee_details_array

		conservancy_fee_data.append(return_conservancy_data)

	return jsonify({"vehicles": vehicle_data, "conservancy_fees": conservancy_fee_data}), 200


@app.route("/bookings/guests/partner/list", methods = ["POST"])
def list_guest_bookings_for_partner():
	messages = []

	try:
		request.json["guest_id"].strip()
		if not request.json["guest_id"]:
			messages.append("Conservancy category ID is empty.")
	except KeyError as e:
		messages.append("Conservancy category ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	try:
		request.json["partner_id"].strip()
		if not request.json["partner_id"]:
			messages.append("Partner ID is empty.")
	except KeyError as e:
		messages.append("Partner ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	requested_date = GenerateDateFromString.generateDate(request.json["date"])

	return_bookings = db.session.query(GatepassGuest)\
								.join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								.join(Partner, Gatepass.booking_id == Partner.booking_id)\
								.add_columns(GatepassGuest.gatepass_guest_public_id, GatepassGuest.gatepass_guest_type, GatepassGuest.gatepass_guest_count,\
											 GatepassGuest.gatepass_no_of_nights,\
											 Gatepass.booking_id)\
								.filter(GatepassGuest.deletion_marker == None)\
								.filter(GatepassGuest.gatepass_guest_count > 0)\
								.filter(GatepassGuest.gatepass_guest_type == request.json["guest_id"])\
								.filter(GatepassGuest.status != get_booking_status_id("Cancelled"))\
								.filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.status != get_booking_status_id("Cancelled"))\
								.filter(Gatepass.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.start_date <= requested_date)\
								.filter(Gatepass.end_date >= requested_date)\
								.filter(Partner.partner_id == request.json["partner_id"])\
								.all()

	if not return_bookings:
		message = []
		message.append("There are no guests arriving on the selected date: " + requested_date.strftime("%d %b %Y"))
		return jsonify({"message": message}), 412

	data = []

	for single in return_bookings:
		return_data = {}
		return_data["gatepass_guest_count"] = int(single.gatepass_guest_count)

		get_booking = db.session.query(Booking)\
								.filter(Booking.booking_public_id == single.booking_id)\
								.first()

		booking_type = db.session.query(BookingType)\
								 .filter(BookingType.deletion_marker == None)\
								 .filter(BookingType.booking_type_public_id == get_booking.booking_type)\
								 .first()

		return_data["booking_type"] = booking_type.booking_type_name

		return_data["booking_public_id"] = get_booking.booking_public_id
		return_data["booking_type_id"] = get_booking.booking_type
		return_data["booking_check_in_date"] = get_booking.booking_check_in_date
		return_data["booking_check_out_date"] = get_booking.booking_check_out_date
		return_data["booking_done_by"] = get_booking.booking_done_by
		return_data["booking_ref_code"] = get_booking.booking_ref_code

		guest_array = []
		
		get_all_guests = db.session.query(GatepassGuest)\
								   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
								   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
												GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
												GatepassGuest.gatepass_no_of_nights)\
								   .filter(GatepassGuest.deletion_marker == None)\
								   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								   .filter(Gatepass.booking_id == single.booking_id)\
								   .options(FromCache(db_cache))\
								   .all()

		for each_guest in get_all_guests:
			guest_data = {}
			guest_data["guest_type"] = each_guest.payment_person
			guest_data["no_of_guests"] = each_guest.gatepass_guest_count

			guest_array.append(guest_data)

		return_data["guests"] = guest_array
		## The naming of this key is simply for convenience when working with the UI
		return_data["guest_total"] = int(single.gatepass_guest_count)

		check_to_invoice = db.session.query(Invoice)\
									 .filter(Invoice.deletion_marker == None)\
									 .filter(Invoice.booking_id == single.booking_id)\
									 .first()
		
		booking_status = db.session.query(BookingStatus)\
									   .filter(BookingStatus.deletion_marker == None)\
									   .filter(BookingStatus.booking_status_public_id == get_booking.status)\
									   .first()

		if check_to_invoice:
			if get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if get_booking.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif get_booking.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if booking_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif booking_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif booking_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif booking_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif booking_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif booking_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif booking_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = booking_status.booking_status_name
		return_data["booking_status_id"] = get_booking.status

		return_data["created_at"] = get_booking.created_at
		return_data["updated_at"] = get_booking.updated_at

		if get_booking.session_id:
			get_user = requests.get(get_single_user.format(get_booking.session_id))

			return_data["session_user"] = get_user.json()["first_name"] + " " + get_user.json()["last_name"]
			return_data["session_id"] = get_booking.session_id

		else:
			return_data["session_user"] = None
			return_data["session_id"] = None
		
		data.append(return_data)

	return jsonify({"data": data}), 200


@app.route("/bookings/vehicles/partner/list", methods = ["POST"])
def list_vehicle_bookings_for_partner():
	messages = []

	try:
		request.json["vehicle_id"].strip()
		if not request.json["vehicle_id"]:
			messages.append("Vehicle ID is empty.")
	except KeyError as e:
		messages.append("Vehicle ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	try:
		request.json["partner_id"].strip()
		if not request.json["partner_id"]:
			messages.append("Partner ID is empty.")
	except KeyError as e:
		messages.append("Partner ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	requested_date = GenerateDateFromString.generateDate(request.json["date"])

	return_bookings = db.session.query(GatepassVehicle)\
								.join(Gatepass, GatepassVehicle.gatepass_id == Gatepass.gatepass_public_id)\
								.join(Partner, Gatepass.booking_id == Partner.booking_id)\
								.add_columns(GatepassVehicle.gatepass_vehicle_public_id, GatepassVehicle.gatepass_vehicle_type, GatepassVehicle.gatepass_vehicle_count,\
											 GatepassVehicle.gatepass_vehicle_no_of_nights,\
											 Gatepass.booking_id)\
								.filter(GatepassVehicle.deletion_marker == None)\
								.filter(GatepassVehicle.gatepass_vehicle_count > 0)\
								.filter(GatepassVehicle.gatepass_vehicle_type == request.json["vehicle_id"])\
								.filter(GatepassVehicle.status != get_booking_status_id("Cancelled"))\
								.filter(GatepassVehicle.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.status != get_booking_status_id("Cancelled"))\
								.filter(Gatepass.status != get_booking_status_id("Updated"))\
								.filter(Gatepass.start_date <= requested_date)\
								.filter(Gatepass.end_date >= requested_date)\
								.filter(Partner.partner_id == request.json["partner_id"])\
								.all()

	if not return_bookings:
		message = []
		message.append("There are no vehicles arriving on the selected date: " + requested_date.strftime("%d %b %Y"))
		return jsonify({"message": message}), 412

	data = []

	for single in return_bookings:
		return_data = {}
		return_data["gatepass_vehicle_count"] = int(single.gatepass_vehicle_count)

		get_booking = db.session.query(Booking)\
								.filter(Booking.booking_public_id == single.booking_id)\
								.first()

		booking_type = db.session.query(BookingType)\
								 .filter(BookingType.deletion_marker == None)\
								 .filter(BookingType.booking_type_public_id == get_booking.booking_type)\
								 .first()

		return_data["booking_type"] = booking_type.booking_type_name

		return_data["booking_public_id"] = get_booking.booking_public_id
		return_data["booking_type_id"] = get_booking.booking_type
		return_data["booking_check_in_date"] = get_booking.booking_check_in_date
		return_data["booking_check_out_date"] = get_booking.booking_check_out_date
		return_data["booking_done_by"] = get_booking.booking_done_by
		return_data["booking_ref_code"] = get_booking.booking_ref_code

		guest_array = []
		
		get_all_guests = db.session.query(GatepassGuest)\
								   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
								   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
								   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
												GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
												GatepassGuest.gatepass_no_of_nights)\
								   .filter(GatepassGuest.deletion_marker == None)\
								   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
								   .filter(Gatepass.booking_id == single.booking_id)\
								   .options(FromCache(db_cache))\
								   .all()

		for each_guest in get_all_guests:
			guest_data = {}
			guest_data["guest_type"] = each_guest.payment_person
			guest_data["no_of_guests"] = each_guest.gatepass_guest_count

			guest_array.append(guest_data)

		return_data["guests"] = guest_array
		## The naming of this key is simply for convenience when working with the UI
		return_data["guest_total"] = int(single.gatepass_vehicle_count)

		check_to_invoice = db.session.query(Invoice)\
									 .filter(Invoice.deletion_marker == None)\
									 .filter(Invoice.booking_id == single.booking_id)\
									 .first()
		
		booking_status = db.session.query(BookingStatus)\
									   .filter(BookingStatus.deletion_marker == None)\
									   .filter(BookingStatus.booking_status_public_id == get_booking.status)\
									   .first()

		if check_to_invoice:
			if get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				return_data["booking_status"] = "To Invoice"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

		else:
			if get_booking.checked_out == 1:
				return_data["booking_status"] = "Checked Out"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
			elif get_booking.checked_in == 1:
				return_data["booking_status"] = "Checked In"
				return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
			elif get_booking.deletion_marker == 1:
				return_data["booking_status"] = "Cancelled"
				return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
			else:
				if booking_status.booking_status_name == "Unconfirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
				elif booking_status.booking_status_name == "Confirmed":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
				elif booking_status.booking_status_name == "No-Show":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
				elif booking_status.booking_status_name == "Abandoned":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
				elif booking_status.booking_status_name == "Updated":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
				elif booking_status.booking_status_name == "Deposit":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
				elif booking_status.booking_status_name == "Complimentary":
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
				else:
					return_data["booking_color"] = "background-color: #fff; color: #000000"
				return_data["booking_status"] = booking_status.booking_status_name
		return_data["booking_status_id"] = get_booking.status
		
		return_data["created_at"] = get_booking.created_at
		return_data["updated_at"] = get_booking.updated_at
		
		if get_booking.session_id:
			get_user = requests.get(get_single_user.format(get_booking.session_id))

			return_data["session_user"] = get_user.json()["first_name"] + " " + get_user.json()["last_name"]
			return_data["session_id"] = get_booking.session_id

		else:
			return_data["session_user"] = None
			return_data["session_id"] = None
		
		data.append(return_data)

	return jsonify({"data": data}), 200