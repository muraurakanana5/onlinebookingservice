from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta, date
from simple_salesforce import Salesforce, SalesforceLogin
from simple_salesforce import exceptions as sf_exceptions

import pymysql, os, math, requests, uuid
import urllib.parse as urlparse
import base64
import traceback


# File imports
from routes import app
from routes import db

from database.callback_details import SalesforceDetails
from database.salesforce_code import SalesforceCode
from functions.validation import *

def close(self):
	self.session.close()


@app.route("/callback")
def salesforceCallback():
	url = request.url

	parsed = urlparse.urlparse(url)
	code = urlparse.parse_qs(parsed.query)["code"][0]

	get_code = db.session.query(SalesforceCode)\
						 .filter(SalesforceCode.deletion_marker == None)\
						 .first()

	if get_code:
		get_code.deletion_marker = 1
		get_code.updated_at = datetime.now()

	sf_code = SalesforceCode(
		salesforce_code = code,
		created_at = datetime.now()
	)

	db.session.add(sf_code)
	
	try:
		 getSalesforceToken(code)
	except Exception as e:
		return jsonify(str(e))
	
	try:
		db.session.commit()
		close(db)

	except Exception:
		db.session.rollback()
		close(db)

	return redirect("""/sales-force", code = 302)
	

@app.route("/salesforce/authenticate")
def authenticateSalesforce():
	# url = requests.get("https://test.salesforce.com/services/oauth2/authorize?client_id=3MVG9U_dUptXGpYKd4EIcpzouSQa3ezC5L0moxtCZxjIf2nDjyoWOE7y5KPwl1H5fUb4fmXrzfzBvu20POM28&client_secret=4922646630109225577&redirect_uri="":5005/callback&grant_type=authorization_code&response_type=code")
	url = requests.get("https://olpejeta.my.salesforce.com/services/oauth2/authorize?client_id={}&client_secret={}&redirect_uri={}&grant_type=authorization_code&response_type=code&scope={}".format(app.config["SF_CONSUMER_KEY"], app.config["SF_CONSUMER_SECRET"], app.config["SF_REDIRECT_URI"], "full refresh_token"))

	return url.content


# @app.route("/salesforce/token/<code>")
def getSalesforceToken(code):
	json_data = {
		"grant_type": "authorization_code",
		"redirect_uri": app.config["SF_REDIRECT_URI"],
		"code": code,
		"client_id": app.config["SF_CONSUMER_KEY"],
		"client_secret": app.config["SF_CONSUMER_SECRET"]
	}

	headers = {
		"content-type": "application/x-www-form-urlencoded"
	}

	get_sf_details = requests.post("https://olpejeta.my.salesforce.com/services/oauth2/token", data = json_data, headers = headers)
	# return jsonify({get_sf_details.json()})

	# get_sf_details = requests.post("https://olpejeta.my.salesforce.com/services/oauth2/token?grant_type=authorization_code&code={}&client_id={}&client_secret={}&redirect_uri={}&scope=refresh_token".format(code, app.config["SF_CONSUMER_KEY"], app.config["SF_CONSUMER_SECRET"], app.config["SF_REDIRECT_URI"]))
	
	get_code = db.session.query(SalesforceCode)\
						 .filter(SalesforceCode.deletion_marker == None)\
						 .filter(SalesforceCode.salesforce_code == code)\
						 .first()

	# sf_details = json.loads(json.dumps(get_sf_details.json()))

	# return get_sf_details.json()
	
	sf_details = get_sf_details.json()
	
	try:
		get_code.access_token = sf_details["access_token"]
		get_code.refresh_token = sf_details["refresh_token"]
		get_code.instance_url = sf_details["instance_url"]
	except Exception as e:
		raise Exception(str(e))

	try:
		db.session.commit()
		close(db)

		# return "Ok", 200

	except Exception as e:
		db.session.rollback()
		close(db)

		raise Exception(str(e))

		# return "Error", 422


@app.route("/salesforce/access")
def return_salesforce_access_details():
	get_details = db.session.query(SalesforceCode)\
						 	.filter(SalesforceCode.deletion_marker == None)\
						 	.first()

	access_token = get_details.access_token
	refresh_token = get_details.refresh_token
	instance_url = get_details.instance_url

	sf = Salesforce(instance_url = instance_url, session_id = access_token)

	try:
		is_record = sf.query("SELECT Id FROM Contact LIMIT 1")
		record = is_record["records"]
		expired_session = False

	except sf_exceptions.SalesforceExpiredSession:
		expired_session = True

	if expired_session:
		json_data = {
			"grant_type": "refresh_token",
			"refresh_token": refresh_token,
			"client_id": app.config["SF_CONSUMER_KEY"],
			"client_secret": app.config["SF_CONSUMER_SECRET"]
		}

		headers = {
			"content-type": "application/x-www-form-urlencoded"
		}

		get_refresh_details = requests.post("https://olpejeta.my.salesforce.com/services/oauth2/token", data = json_data, headers = headers)

		access_token = get_refresh_details.json()["access_token"]

	data = {
		"access_token": access_token,
		"instance_url": instance_url
	}

	return jsonify(data), 200


# @app.route("/salesforce/credentials", methods = ["POST"])
# def saveCredentials():
# 	validation_list = [
# 		{"field": "email", "alias": "Username"},
# 		{"field": "password"}
# 	]

# 	messages = fieldValidation(request.json, validation_list)

# 	if messages:
# 		return jsonify({"messages": messages}), 422

# 	try:
# 		# sf = Salesforce(username = request.json["email"], password = request.json["password"], security_token = 'ag5WoEQG3ZmQyova1wRtUu65', sandbox = True)
# 		sf_session_id, sf_instance = SalesforceLogin(username = request.json["email"], password = request.json["password"], domain = "olpejeta.my")
# 		sf = Salesforce(instance = sf_instance, session_id = sf_session_id)
# 		is_record = sf.query("SELECT Id, Email FROM Contact LIMIT 1")
# 		record = is_record["records"]
# 	except Exception as e:
# 		trace = traceback.format_exc()
# 		message = []
# 		message.append("Unfortunately, the Salesforce credentials provided do not appear to be valid. Please try again.")
# 		return jsonify({"message": str(e) + "Exception: " + trace}), 422
	
# 	enc = base64.b64encode(str.encode(str(request.json["password"])))

# 	password = str(enc).strip("b'")

# 	check_existing_creds = db.session.query(SalesforceDetails)\
# 									 .filter(SalesforceDetails.deletion_marker ==  None)\
# 									 .first()

# 	if check_existing_creds:
# 		check_existing_creds.deletion_marker = 1
# 		check_existing_creds.updated_at = datetime.now()
	
# 	details = SalesforceDetails(
# 		details_email = request.json["email"],
# 		details_password = password,
# 		created_at = datetime.now(),
# 		updated_at = datetime.now()
# 	)

# 	db.session.add(details)

# 	try:
# 		db.session.commit()
# 		close(db)

# 		message = []
# 		message.append("Successfully saved the Salesforce details.")
# 		return jsonify({"message": message}), 201

# 	except Exception:
# 		db.session.rollback()
# 		close(db)

# 		message = []
# 		message.append("There was an error saving the details.")
# 		return jsonify({"message": message}), 422


# @app.route("/salesforce/users")
# def getAllSalesforceDetails():
# 	get_codes = db.session.query(SalesforceDetails)\
# 						  .order_by(SalesforceDetails.details_id.desc())\
# 						  .all()

# 	if not get_codes:
# 		message = []
# 		message.append("There are no Salesforce user details that have been saved yet.")

# 		return jsonify({"message": message}), 422

# 	else:
# 		data = []

# 		for single in get_codes:
# 			return_data = {}
			
# 			start_padding = 3

# 			email_masked = single.details_email[:start_padding]
# 			w = 0

# 			while w < (len(single.details_email) - start_padding):
# 				email_masked = email_masked + "*"
# 				w = w + 1
			
# 			return_data["details_email"] = email_masked
			
# 			password_masked = ""
# 			x = 0

# 			while x < len(single.details_password):
# 				password_masked = password_masked + "*"
# 				x = x + 1

# 			return_data["details_password"] = password_masked
			
# 			if single.deletion_marker:
# 				return_data["status"] = "Inactive"
# 				return_data["valid_credentials"] = "N/A"

# 			else:
# 				return_data["status"] = "Active"

# 				try:
# 					u_name = single.details_email
# 					temp_pass = single.details_password
# 					pass_w = str(base64.b64decode(temp_pass)).strip("b'")
					
# 					# sf = Salesforce(username = u_name, password = pass_w, security_token = 'ag5WoEQG3ZmQyova1wRtUu65', sandbox = True)
# 					sf_session_id, sf_instance = SalesforceLogin(username = u_name, password = pass_w, domain = "olpejeta.my")
# 					sf = Salesforce(instance = sf_instance, session_id = sf_session_id)
					
# 					is_record = sf.query("SELECT Id, Email FROM Contact LIMIT 1")

# 					record = is_record["records"]
# 					return_data["valid_credentials"] = "Valid credentials"
# 				except Exception:
# 					return_data["valid_credentials"] = "Invalid credentials"

# 			data.append(return_data)
		
# 		return jsonify({"data": data}), 200


# @app.route("/salesforce/details")
# def getSalesforceDetails():
# 	get_code = db.session.query(SalesforceDetails)\
# 						 .filter(SalesforceDetails.deletion_marker == None)\
# 						 .first()

# 	if not get_code:
# 		status = False
# 		data = []

# 		return jsonify({"status": status, "data": data}), 422

# 	else:
# 		status = True
# 		data = []

# 		data.append(get_code.return_json())
		
# 		return jsonify({"status": status, "data": data}), 200