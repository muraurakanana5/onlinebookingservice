import requests
import uuid
from datetime import datetime
from flask import jsonify, request

from database.school import School
from database.school_contact import SchoolContact
from functions.validation import fieldValidation
from routes import (FromCache, app, bookings_logger, db, db_cache,
					receipt_print_options, schoolCodeGenerator, sessionTracking, ticketGenerator)



def close(self):
	self.session.close()


@app.route("/schools/new", methods = ["POST"])
def add_new_school():
	validation_list = [
		{"field": "school_name", "alias": "School name"},
		{"field": "level"},
		{"field": "type"},
		{"field": "gender"},
		{"field": "city_town", "alias": "City/Town"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	get_last_school = db.session.query(School)\
								.filter(School.deletion_marker == None)\
								.order_by(School.school_id.desc())\
								.first()

	if not get_last_school:
		school_code = schoolCodeGenerator(None)
	else:
		school_code = schoolCodeGenerator(get_last_school.school_code)
	
	school_id = str(uuid.uuid4())
	
	school = School(
		school_public_id = school_id,
		school_name = request.json["school_name"].title(),
		school_level = request.json["level"],
		school_type = request.json["type"],
		school_gender = request.json["gender"],
		school_code = school_code,
		closest_city_town = request.json["city_town"],
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(school)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully saved the school details.")

		return jsonify({"message": message}), 201

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to save the school details. Try again later.")

		return jsonify({"message": message, "error": str(e)}), 422


@app.route("/schools/view", methods = ["GET"])
def view_all_schools():
	get_schools = db.session.query(School)\
							.filter(School.deletion_marker == None)\
							.order_by(School.school_code.asc())\
							.all()

	if not get_schools:
		message = []
		message.append("There are no school details in the system yet.")

		return jsonify({"message": message}), 412

	data = []
	for single in get_schools:
		data.append(single.return_json())
		

	return jsonify({"data": data}), 200


@app.route("/schools/view/<school_id>", methods = ["GET"])
def view_single_school(school_id):
	get_school = db.session.query(School)\
						   .filter(School.deletion_marker == None)\
						   .filter(School.school_public_id == school_id)\
						   .first()

	if not get_school:
		message = []
		message.append("There are no school details in the system yet.")

		return jsonify({"message": message}), 412

	data = [get_school.return_json()]
	
	return jsonify({"data": data}), 200


@app.route("/schools/modify", methods = ["PATCH"])
def modify_school_details():
	validation_list = [
		{"field": "school_id", "alias": "School ID"},
		{"field": "school_name", "alias": "School name"},
		{"field": "level"},
		{"field": "type"},
		{"field": "gender"},
		{"field": "city_town", "alias": "City/Town"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	get_school = db.session.query(School)\
						   .filter(School.deletion_marker == None)\
						   .filter(School.school_public_id == request.json["school_id"])\
						   .first()

	if not get_school:
		message = []
		message.append("The selected school details do not appear to exist.")
		return jsonify({"message": message}), 412

	get_school.school_name = request.json["school_name"]
	get_school.school_level = request.json["level"]
	get_school.school_type = request.json["type"]
	get_school.school_gender = request.json["gender"]
	get_school.closest_city_town = request.json["city_town"]
	get_school.session_id = request.json["session_id"]
	get_school.updated_at = datetime.now()

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully updated the school details.")
		return jsonify({"message": message}), 200

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to update the school details. Try again later.")
		return jsonify({"message": message, "error": str(e)}), 422


@app.route("/schools/delete", methods = ["PATCH"])
def delete_school_details():
	validation_list = [
		{"field": "school_id", "alias": "School ID"},
		{"field": "session_id", "alias": "Session ID"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	get_school = db.session.query(School)\
						   .filter(School.deletion_marker == None)\
						   .filter(School.school_public_id == request.json["school_id"])\
						   .first()

	if not get_school:
		message = []
		message.append("The selected school details do not appear to exist.")
		return jsonify({"message": message}), 412

	get_school_contacts = db.session.query(SchoolContact)\
									.filter(SchoolContact.deletion_marker == 1)\
									.filter(SchoolContact.school_id == request.json["school_id"])\
									.all()

	for single_contact in get_school_contacts:
		single_contact.deletion_marker = 1
		single_contact.session_id = request.json["session_id"]
		single_contact.updated_at = datetime.now()

	get_school.deletion_marker = 1
	get_school.session_id = request.json["session_id"]
	get_school.updated_at = datetime.now()

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully updated the school details.")
		return jsonify({"message": message}), 200

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to update the school details. Try again later.")
		return jsonify({"message": message, "error": str(e)}), 422