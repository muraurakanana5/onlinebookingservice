from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta, date
from simple_salesforce import Salesforce, SalesforceLogin

import pymysql, os, math, requests, uuid
import base64


# File imports
from routes import app
from routes import db
from routes.callback_urls import return_salesforce_access_details
# from routes.bookings_urls import send_salesforce_email
# from routes.bookings_urls import get_booking_status_id

from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_payments import BookingPayment
from database.check_in_vehicles import CheckInVehicle
from database.payment_methods import PaymentMethod
from database.booking_status import BookingStatus
from database.facility import Facility
from database.inventory import Inventory
from database.booking_details import Detail
from database.mandatory_payments import Mandatory
from database.transaction import Transaction
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from database.booking_guest_types import GuestType
from database.gatepass_vehicles import GatepassVehicle
from database.vehicle import Vehicle
from database.sun import Sun
from database.modify import Modify
from database.gate import Gate
from database.vehicle_details import VehicleDetail
from database.destination import Destination
from database.salesforce import SalesforceData
from database.group import Group
from database.school_booking import SchoolBooking
from database.school import School
from database.member import Member
from database.callback_details import SalesforceDetails
from database.salesforce_code import SalesforceCode
from database.booking_activity_log import BookingActivity
from database.partner import Partner

from variables import *
from functions.date_operators import *
from functions.currency_operators import *
from functions.booking_snippets import bookingTotal, get_booking_status_id
from functions.validation import validatePhoneNumber

from simple_salesforce import Salesforce
from collections import OrderedDict

from flask_mail import Mail, Message
from sendgrid.helpers.mail import *

import sendgrid
import jinja2


country_list = [
 {"api_name": "Tanzania, United Republic of", "sf_name": "Tanzania"}
]


def send_salesforce_email(sf_data):
	recipients = [
	 "william.njoroge@olpejetaconservancy.org",
	 "patricia.kanana@olpejetaconservancy.org"
	]

	sendgrid_response = []

	for recipient in recipients:
		sender = "reservations@olpejetaconservancy.org"
		subject = "Salesforce Schedule (Bookings)"

		# template = jinja2.Template

		sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])

		with app.app_context():
			content = Content("text/html", render_template("salesforce.html", data = sf_data, date = datetime.now().strftime("%H:%M:%S %d-%m-%Y")))

		from_email = Email(sender)
		to_email = Email(recipient)
		subject = subject
		mail = Mail(from_email, subject, to_email, content)
		response = sg.client.mail.send.post(request_body=mail.get())

		sendgrid_response.append(str(response))

	return sendgrid_response


####################
#### Salesforce ####
####################
@app.route('/post/visits', methods = ["POST"])
def ui_generated_request():
	"""
	Handling requests from the UI
	"""

	messages = []

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	get_user = requests.get(get_single_user.format(request.json["session_id"]))

	if get_user.status_code != 200:
		message = []
		message.append("Not authorised to perform this operation")
		return jsonify({"message": message}), 422

	get_code = db.session.query(SalesforceDetails)\
	   .filter(SalesforceDetails.deletion_marker == None)\
	   .first()

	if not get_code:
		message = []
		message.append("There are no credentials.")
		return jsonify({"message": message}), 200

	u_name = get_code.details_email

	temp_pass = get_code.details_password
	pass_w = str(base64.b64decode(temp_pass)).strip("b'")

	# sf = Salesforce(username='oaknet@opc.org.oaknet', password='integration1', security_token='ag5WoEQG3ZmQyova1wRtUu65', sandbox=True)
	sf = Salesforce(username = u_name, password = pass_w, security_token = 'ag5WoEQG3ZmQyova1wRtUu65', sandbox = True)
	new_bookings = db.session.query(Booking)\
		.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
		.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
		.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
		.join(Detail, Booking.booking_public_id == Detail.booking_id)\
		.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
		 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
		 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
		 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status, Booking.salesforce,\
		 Booking.currency, Booking.salesforce_uuid,\
		 BookingType.booking_type_name,\
		 BookingStatus.booking_status_name,\
		 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.contact_uuid,\
		 Detail.address, Detail.additional_note, Detail.city, Detail.country, Detail.country_code)\
		.filter(Booking.deletion_marker == None)\
		.filter(Booking.salesforce == None)\
		.filter(Booking.salesforce_uuid == None)\
		.filter(Booking.payment_status != None)\
		.filter(Booking.status != get_booking_status_id("Cancelled"))\
		.filter(Detail.deletion_marker == None)\
		.filter(Detail.contact_uuid == None)\
		.all()
	if new_bookings:
		for booking in new_bookings:
			try:
				get_email = db.session.query(Detail)\
					.filter(Detail.deletion_marker == None)\
					.filter(Detail.email_address == booking.email_address)\
					.filter(Detail.contact_uuid != None)\
					.filter(Detail.status != get_booking_status_id("Cancelled"))\
					.filter(Detail.status != get_booking_status_id("Updated"))\
					.order_by(Detail.booking_details_id.desc())\
					.first()

				try:
					number_prefix,number = booking.phone_number.split("-")
					number_prefix = number_prefix.strip("+")
					number = number
				except Exception:
					number_prefix = None
					number = None

				country_details = requests.get(get_country_details.format(number_prefix))

				try:
					country = country_details.json()[0]["name"]
					demonym = country_details.json()[0]["demonym"]
				except Exception:
					country = None
					demonym = None

				if booking.city:
					city = booking.city
				else:
					try:
						city = country_details.json()[0]["capital"]
					except Exception:
						city = None

				if booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
					citizenship = "Citizen"
				else:
					citizenship = None

				is_record = sf.query("SELECT Id, Email FROM Contact WHERE Email = '{}'".format(booking.email_address))

				if is_record["totalSize"] > 0:
					contact_uuid = is_record["records"][0]["Id"]

				else:
					payload = {
					  "Title": None,
					  'LastName': booking.last_name,
					  'Email': booking.email_address,
					  'FirstName': booking.first_name,
					  'Phone': number,
					  'MobilePhone' : number,
					  "Mobile_Phone_Country_Code_T__c": number_prefix,
					  'City__c' : city,
					  "Country__c": country,
					  "xsms__Mobile_Phone_Country__c": country,
					  "Citizenship__c": citizenship
					 }
					new_contact = sf.Contact.create(payload)

					contact_uuid = new_contact['id']

				return_data = {}
				bookingTotal(return_data, booking.booking_public_id)

				currency = requests.get(get_currency.format(booking.currency))
				return_data["currency"] = currency.json()["data"][0]["currency_name"]

				inventory_id = []

				if return_data["inventory_bookings"]:
					activity_checked = "true"

					for single_inventory in return_data["inventory_bookings"]:
						inventory_details = {}
						inventory_details["id"] = single_inventory["inventory_id"]
						inventory_details["name"] = single_inventory["inventory_name"]
						inventory_details["date"] = single_inventory["inventory_booking_date"]

						inventory_id.append(inventory_details)

				else:
					activity_checked = "false"

				facility_type = []
				facility_id = []

				accomodation = []
				camping = []

				if return_data["facility_bookings"]:
					for single_facility in return_data["facility_bookings"]:
						facility_details = {}

						facility_details["name"] = single_facility["facility_name"]
						facility_details["date"] = single_facility["facility_booking_check_in_date"]

						facility_type.append(single_facility["facility_type"])

						if single_facility["facility_type"] == "Camping Sites":
							camping.append(single_facility["facility_name"])
						elif single_facility["facility_type"] == "Accomodation":
							accomodation.append(single_facility["facility_name"])

						facility_id.append(facility_details)

				if facility_type:
					if "Accomodation" in facility_type:
						accomodation_checked = "true"
						camping_checked = "false"
					elif "Camping Sites" in facility_type:
						accomodation_checked = "false"
						camping_checked = "true"
					elif "Accomodation" & "Camping Sites" in facility_type:
						accomodation_checked = "true"
						camping_checked = "true"

				else:
					accomodation_checked = "false"
					camping_checked = "false"

				get_school_booking = db.session.query(SchoolBooking)\
					.join(School, SchoolBooking.school_id == School.school_public_id)\
					.add_columns(School.school_name, School.school_level)\
					.filter(SchoolBooking.deletion_marker == None)\
					.filter(SchoolBooking.booking_id == booking.booking_public_id)\
					.filter(School.deletion_marker == None)\
					.first()

				if get_school_booking:
					school_booking = "true"
					school_name = get_school_booking.school_name
					school_level = get_school_booking.school_level

				else:
					school_booking = "false"
					school_name = None
					school_level = None

				get_payment_mode = db.session.query(Transaction)\
				  .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
				  .add_columns(PaymentMethod.payment_method_name)\
				  .filter(Transaction.deletion_marker == None)\
				  .filter(Transaction.booking_id == booking.booking_public_id)\
				  .order_by(Transaction.transaction_booking_id.desc())\
				  .first()

				if get_payment_mode:
					payment_method = get_payment_mode.payment_method_name
				else:
					payment_method = None

				new_visit = sf.Visit__c.create({
				 'Residency__c': booking.country,
				 'Booking_Amount__c' : return_data["total_cost"],
				 'Check_in_Date_Time__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Check_out_Date__c' : (booking.booking_check_out_date).strftime('%Y-%m-%d'),
				 'CurrencyIsoCode' : return_data["currency"],
				 'Booking__c' : booking.booking_ref_code,
				 'Visit_Date__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Contact__c' : contact_uuid,
				 "Mode_of_Payment__c": payment_method,
				 'Indemnity_checked__c' : "true",
				 'Activity_booked__c' : activity_checked,
				 'Camping_booked__c' : camping_checked,
				 'Accomodation_Booked__c' : accomodation_checked,
				 'RecordTypeId' : "0121D0000009eEnQAI",
				 'School_Booking__c' : school_booking,
				 'Level_of_school__c' : school_level,
				 'Name_of_school__c' : school_name
				})

				get_member_booking = db.session.query(Member)\
					.filter(Member.deletion_marker == None)\
					.filter(Member.booking_id == booking.booking_public_id)\
					.first()

				if get_member_booking:
					get_salesforce = requests.get(get_membership_data.format(get_member_booking.member_id))

					if get_salesforce.status_code == 200:
						new_member_visit = sf.Member_Visits__c.create({
						 "Date__c": (booking.booking_check_in_date).strftime('%Y-%m-%d'),
						 "Membership__c": get_salesforce.json()["member_salesforce"],
						 "Number_of_people__c": int(get_member_booking.num_of_people) + int(get_member_booking.num_of_children),
						 "Number_of_vehicles__c": int(get_member_booking.num_of_vehicles)
						})
					else:
						pass

				else:
					pass

				if inventory_id:
					for each_inventory in inventory_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Activity__c' : each_inventory["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_inventory["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				if facility_id:
					for each_facility in facility_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Accomodation__c' : each_facility["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_facility["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				get_booking = db.session.query(Booking)\
				   .filter(Booking.deletion_marker == None)\
				   .filter(Booking.booking_public_id == booking.booking_public_id)\
				   .first()

				get_detail = db.session.query(Detail)\
				  .filter(Detail.deletion_marker == None)\
				  .filter(Detail.booking_id == booking.booking_public_id)\
				  .first()

				get_booking.salesforce = 1
				get_booking.salesforce_uuid = new_visit["id"]
				get_booking.updated_at = datetime.now()

				get_detail.contact_uuid = contact_uuid
				get_detail.updated_at = datetime.now()

				salesforce_data = SalesforceData(
				  salesforce_tracking_public_id = str(uuid.uuid4()),
				  booking_id = booking.booking_public_id,
				  date_done = datetime.now(),
				  salesforce_uuid = new_visit["id"]
				 )
				db.session.add(salesforce_data)
			except Exception as identifier:
				# responseObject = {
				#     'message' : str(identifier)
				# }
				# return make_response(jsonify(responseObject)), 500
				pass
		try:
			db.session.commit()
			responseObject = {
			 'message' : 'Successfully pushed booking data to Salesforce.'
			}
			return make_response(jsonify(responseObject)), 200
		except Exception as identifier:
			responseObject = {
			 'message' : str(identifier)
			}
			return make_response(jsonify(responseObject)), 200
	else:
		responseObject = {
		 'message' : 'No new bookings to push to Salesforce.'
		}
		return make_response(jsonify(responseObject)), 200

@app.route('/send/single-visit/salesforce/<booking_id>')
def sendSingleSalesforceVisits(booking_id):
	details = requests.get(get_sf_details)

	sf = Salesforce(instance_url = details.json()["instance_url"], session_id = details.json()["access_token"])

	record_type_id = None

	## Querying the Visit object to get the recordTypeId
	get_visits_info = sf.Visit__c.describe()
	for info in get_visits_info["recordTypeInfos"]:
		if ("Booking" == info["name"]) or ("Booking" in info["name"]):
			record_type_id = str(info["recordTypeId"])

		else:
			pass

	if not record_type_id:
		message = []
		message.append("No recordTypeId present.")
		return jsonify({"message": message}), 422

	new_bookings = db.session.query(Booking)\
		.join(Detail, Booking.booking_public_id == Detail.booking_id)\
		.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
		 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
		 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
		 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status, Booking.salesforce,\
		 Booking.currency, Booking.salesforce_uuid,\
		 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.contact_uuid,\
		 Detail.address, Detail.additional_note, Detail.city, Detail.country, Detail.country_code)\
		.filter(Booking.deletion_marker == None)\
		.filter(Booking.salesforce == None)\
		.filter(Booking.salesforce_uuid == None)\
		.filter(Booking.status != get_booking_status_id("Cancelled"))\
		.filter(Booking.booking_public_id == booking_id)\
		.filter(Detail.deletion_marker == None)\
		.filter(Detail.contact_uuid == None)\
		.all()
	if new_bookings:
		for booking in new_bookings:
			try:
				get_email = db.session.query(Detail)\
					.filter(Detail.deletion_marker == None)\
					.filter(Detail.email_address == booking.email_address)\
					.filter(Detail.contact_uuid != None)\
					.filter(Detail.status != get_booking_status_id("Cancelled"))\
					.filter(Detail.status != get_booking_status_id("Updated"))\
					.order_by(Detail.booking_details_id.desc())\
					.first()

				try:
					number_prefix,number = booking.phone_number.split("-")
					number_prefix = number_prefix.strip("+")
					number = number
				except Exception:
					number_prefix = None
					number = None

				country_details = requests.get(get_country_details.format(number_prefix))

				try:
					country = country_details.json()[0]["name"]
					demonym = country_details.json()[0]["demonym"]
				except Exception:
					country = None
					demonym = None

				if booking.city:
					city = booking.city
				else:
					try:
						city = country_details.json()[0]["capital"]
					except Exception:
						city = None

				if booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
					citizenship = "Citizen"
				else:
					citizenship = None

				is_record = sf.query("SELECT Id, Email FROM Contact WHERE Email = '{}'".format(booking.email_address))

				if is_record["totalSize"] > 0:
					contact_uuid = is_record["records"][0]["Id"]

				else:
					## TODO: Apply this check to main Salesforce function
					if booking.last_name:
						last_name = booking.last_name
					else:
						last_name = "N/A"

					payload = {
					  "Title": None,
					  'LastName': last_name,
					  'Email': booking.email_address,
					  'FirstName': booking.first_name,
					  'Phone': number,
					  'MobilePhone' : number,
					  "Mobile_Phone_Country_Code_T__c": number_prefix,
					  'City__c' : city,
					  "Country__c": country,
					  "xsms__Mobile_Phone_Country__c": country,
					  "Citizenship__c": citizenship
					 }
					new_contact = sf.Contact.create(payload)

					contact_uuid = new_contact['id']

				return_data = {}
				bookingTotal(return_data, booking.booking_public_id)

				currency = requests.get(get_currency.format(booking.currency))
				return_data["currency"] = currency.json()["data"][0]["currency_name"]

				inventory_id = []

				if return_data["inventory_bookings"]:
					activity_checked = "true"

					for single_inventory in return_data["inventory_bookings"]:
						inventory_details = {}
						inventory_details["id"] = single_inventory["inventory_id"]
						inventory_details["name"] = single_inventory["inventory_name"]
						inventory_details["date"] = single_inventory["inventory_booking_date"]

						inventory_id.append(inventory_details)

				else:
					activity_checked = "false"

				facility_type = []
				facility_id = []

				accomodation = []
				camping = []

				if return_data["facility_bookings"]:
					for single_facility in return_data["facility_bookings"]:
						facility_details = {}

						facility_details["name"] = single_facility["facility_name"]
						facility_details["date"] = single_facility["facility_booking_check_in_date"]

						facility_type.append(single_facility["facility_type"])

						if single_facility["facility_type"] == "Camping Sites":
							camping.append(single_facility["facility_name"])
						elif single_facility["facility_type"] == "Accomodation":
							accomodation.append(single_facility["facility_name"])

						facility_id.append(facility_details)

				if facility_type:
					if "Accomodation" in facility_type:
						accomodation_checked = "true"
						camping_checked = "false"
					elif "Camping Sites" in facility_type:
						accomodation_checked = "false"
						camping_checked = "true"
					elif "Accomodation" & "Camping Sites" in facility_type:
						accomodation_checked = "true"
						camping_checked = "true"

				else:
					accomodation_checked = "false"
					camping_checked = "false"

				## TODO: Apply this to main Salesforce stuff
				get_school_booking = db.session.query(SchoolBooking)\
					.join(School, SchoolBooking.school_id == School.school_public_id)\
					.add_columns(School.school_name, School.school_level, School.school_code)\
					.filter(SchoolBooking.deletion_marker == None)\
					.filter(SchoolBooking.booking_id == booking.booking_public_id)\
					.filter(School.deletion_marker == None)\
					.first()

				if get_school_booking:
					school_booking = "true"
					school_name = get_school_booking.school_name
					school_level = get_school_booking.school_level
					school_code = get_school_booking.school_code

					students = []
					for single_student in return_data["guests"]:
						if ("student" in single_student["payment_person"]) or ("Student" in single_student["payment_person"]):
							students.append(single_student["payment_guests"])
						else:
							pass

					no_of_students = sum(students)

				else:
					school_booking = "false"
					school_name = None
					school_level = None
					school_code = None
					no_of_students = 0

				get_payment_mode = db.session.query(Transaction)\
				  .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
				  .add_columns(PaymentMethod.payment_method_name)\
				  .filter(Transaction.deletion_marker == None)\
				  .filter(Transaction.booking_id == booking.booking_public_id)\
				  .order_by(Transaction.transaction_booking_id.desc())\
				  .first()

				if get_payment_mode:
					payment_method = get_payment_mode.payment_method_name
				else:
					payment_method = None

				## TODO: Apply this to main Salesforce stuff
				new_visit = sf.Visit__c.create({
				 'Residency__c': booking.country,
				 'Booking_Amount__c' : return_data["total_cost"],
				 'Check_in_Date_Time__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Check_out_Date__c' : (booking.booking_check_out_date).strftime('%Y-%m-%d'),
				 'CurrencyIsoCode' : return_data["currency"],
				 'Booking__c' : booking.booking_ref_code,
				 'Visit_Date__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Contact__c' : contact_uuid,
				 "Mode_of_Payment__c": payment_method,
				 'Indemnity_checked__c' : "true",
				 'Activity_booked__c' : activity_checked,
				 'Camping_booked__c' : camping_checked,
				 'Accomodation_Booked__c' : accomodation_checked,
				 'RecordTypeId' : record_type_id,
				 'School_Booking__c' : school_booking,
				 'Level_of_school__c' : school_level,
				 'Name_of_school__c' : school_name,
				 'OBTS_School_ID__c' : school_code,
				 'OBTS_Number_of_students__c' : no_of_students
				})

				get_member_booking = db.session.query(Member)\
					.filter(Member.deletion_marker == None)\
					.filter(Member.booking_id == booking.booking_public_id)\
					.first()

				if get_member_booking:
					get_salesforce = requests.get(get_membership_data.format(get_member_booking.member_id))

					## TODO: Apply this to main Salesforce stuff
					opportunity_record = sf.query("SELECT Id FROM Opportunity WHERE Contact_Email_del__c = '{}'".format(booking.email_address))
					opp = opportunity_record["records"][0]["Id"]

					if get_salesforce.status_code == 200:
						new_member_visit = sf.Member_Visits__c.create({
						 "Date__c": (booking.booking_check_in_date).strftime('%Y-%m-%d'),
						 "Membership__c": opp,
						 "Number_of_people__c": int(get_member_booking.num_of_people) + int(get_member_booking.num_of_children),
						 "Number_of_vehicles__c": int(get_member_booking.num_of_vehicles)
						})
					else:
						pass

				else:
					pass

				if inventory_id:
					for each_inventory in inventory_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Activity__c' : each_inventory["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_inventory["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				if facility_id:
					for each_facility in facility_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Accomodation__c' : each_facility["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_facility["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				get_booking = db.session.query(Booking)\
				   .filter(Booking.deletion_marker == None)\
				   .filter(Booking.booking_public_id == booking.booking_public_id)\
				   .first()

				get_detail = db.session.query(Detail)\
				  .filter(Detail.deletion_marker == None)\
				  .filter(Detail.booking_id == booking.booking_public_id)\
				  .first()

				get_booking.salesforce = 1
				get_booking.salesforce_uuid = new_visit["id"]
				get_booking.updated_at = datetime.now()

				get_detail.contact_uuid = contact_uuid
				get_detail.updated_at = datetime.now()

				salesforce_data = SalesforceData(
				  salesforce_tracking_public_id = str(uuid.uuid4()),
				  booking_id = booking.booking_public_id,
				  date_done = datetime.now(),
				  salesforce_uuid = new_visit["id"]
				 )
				db.session.add(salesforce_data)
			except Exception as identifier:
				responseObject = {
				 'message' : str(identifier),
				 'recordTypeId': record_type_id
				}
				return make_response(jsonify(responseObject)), 422
				# pass
		try:
			db.session.commit()
			responseObject = {
			 'message' : 'Successfully pushed booking data to Salesforce.'
			}
			return make_response(jsonify(responseObject)), 200
		except Exception as identifier:
			responseObject = {
			 'message' : str(identifier)
			}
			return make_response(jsonify(responseObject)), 422
	else:
		responseObject = {
		 'message' : 'No new bookings to push to Salesforce.' + str(len(new_bookings))
		}
		return make_response(jsonify(responseObject)), 200


@app.route("/send/driver-details")
def send_driver_details():
	get_code = db.session.query(SalesforceDetails)\
	   .filter(SalesforceDetails.deletion_marker == None)\
	   .first()

	if not get_code:
		message = []
		message.append("There are no credentials.")
		return jsonify({"message": message}), 200

	u_name = get_code.details_email

	temp_pass = get_code.details_password
	pass_w = str(base64.b64decode(temp_pass)).strip("b'")

	sf = Salesforce(username = u_name, password = pass_w, security_token = 'ag5WoEQG3ZmQyova1wRtUu65', sandbox = True)

	get_driver_details = db.session.query(CheckInVehicle)\
		.filter(CheckInVehicle.deletion_marker == None)\
		.filter(CheckInVehicle.driver_salesforce_uuid == None)\
		.filter(CheckInVehicle.check_vehicle_phone_number != None)\
		.all()


	if not get_driver_details:
		message = []
		message.append("There are no driver details to push to Salesforce.")
		return jsonify({"message": message}), 412

	for single_driver in get_driver_details:
		try:
			phone_validation = validatePhoneNumber(single_driver.check_vehicle_phone_number)

			if phone_validation[0]:
				phone = phone_validation[1]

				number_prefix,number = phone.split("-")
				number_prefix = number_prefix.strip("+")

				is_record = sf.query("SELECT Id FROM Contact WHERE MobilePhone = '{}'".format(number))

				if is_record["totalSize"] > 0:
					contact_uuid = is_record["records"][0]["Id"]

				else:
					country_details = requests.get(get_country_details.format(number_prefix))

					try:
						country = country_details.json()[0]["name"]
					except Exception:
						country = None

					## TODO: Confirm about first/last name
					try:
						first_name,last_name = single_driver.check_vehicle_driver.split(" ")
					except Exception:
						first_name = single_driver.check_vehicle_driver
						last_name = single_driver.check_vehicle_driver

					contact_payload = {
					 "FirstName": first_name,
					 "LastName": last_name,
					 "Phone": number,
					 "MobilePhone" : number,
					 "Mobile_Phone_Country_Code_T__c": number_prefix,
					 "Country__c": country,
					 "xsms__Mobile_Phone_Country__c": country,
					 "Driver_Contact__c": "true"
					}

					new_contact = sf.Contact.create(contact_payload)

					contact_uuid = new_contact["id"]

				single_driver.driver_salesforce_uuid = contact_uuid

			else:
				pass

		except Exception as e:
			return jsonify({"error": str(e)}), 422

	try:
		db.session.commit()
		db.session.close()

	except Exception:
		db.session.rollback()
		db.session.close()

	message = []
	message.append("Successfully pushed driver details to Salesforce.")
	return jsonify({"message": message}), 200


@app.route("/sf/org-id")
def testing_alternate_sign_in():

	get_sf_code = db.session.query(SalesforceCode)\
	   .filter(SalesforceCode.deletion_marker == None)\
	   .first()

	if not get_sf_code:
		message = []
		message.append("There is no Salesforce code present. Please authenticate this application.")
		return jsonify({"message": message}), 422

	sf = Salesforce(instance_url = get_sf_code.instance_url, session_id = get_sf_code.access_token)

	try:
		is_record = sf.query("SELECT Id FROM Contact LIMIT 1")

		return jsonify({"records": is_record["records"][0]["Id"]}), 200

	except Exception as e:
		return jsonify({"error": str(e)}), 422


@app.route("/salesforce/uuid/check/<email_address>")
def checkIfSalesforceContact(email_address):
	check_contact = db.session.query(Detail)\
	  .join(Booking, Detail.booking_id == Booking.booking_public_id)\
	  .add_columns(Detail.contact_uuid)\
	  .filter(Booking.deletion_marker == None)\
	  .filter(Detail.deletion_marker == None)\
	  .filter(Detail.email_address == email_address)\
	  .first()

	try:
		if check_contact.contact_uuid:
			return jsonify({"exists": True, "uuid": check_contact.contact_uuid}), 200

		else:
			return jsonify({"exists": False, "uuid": None}), 200

	except Exception:
		return jsonify({"exists": False, "uuid": None}), 200


def send_single_visit_salesforce(booking_id):
	get_code = db.session.query(SalesforceDetails)\
	   .filter(SalesforceDetails.deletion_marker == None)\
	   .first()

	if not get_code:
		message = []
		message.append("There are no credentials.")
		return jsonify({"message": message}), 200

	u_name = get_code.details_email

	temp_pass = get_code.details_password
	pass_w = str(base64.b64decode(temp_pass)).strip("b'")

	# sf = Salesforce(username='oaknet@opc.org.oaknet', password='integration1', security_token='ag5WoEQG3ZmQyova1wRtUu65', sandbox=True)
	sf = Salesforce(username = u_name, password = pass_w, security_token = 'ag5WoEQG3ZmQyova1wRtUu65', sandbox = True)
	new_bookings = db.session.query(Booking)\
		.join(BookingGuest, Booking.booking_public_id == BookingGuest.booking_id)\
		.join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
		.join(BookingStatus, Booking.status == BookingStatus.booking_status_public_id)\
		.join(Detail, Booking.booking_public_id == Detail.booking_id)\
		.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
		 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
		 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
		 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status, Booking.salesforce,\
		 Booking.currency, Booking.salesforce_uuid,\
		 BookingType.booking_type_name,\
		 BookingStatus.booking_status_name,\
		 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.contact_uuid,\
		 Detail.address, Detail.additional_note, Detail.city, Detail.country, Detail.country_code)\
	   .filter(Booking.deletion_marker == None)\
	   .filter(Booking.salesforce == None)\
	   .filter(Booking.salesforce_uuid == None)\
	   .filter(Booking.payment_status != None)\
	   .filter(Booking.status != get_booking_status_id("Cancelled"))\
	   .filter(Booking.booking_public_id == booking_id)\
	   .filter(Detail.deletion_marker == None)\
	   .filter(Detail.contact_uuid == None)\
	   .all()
	if new_bookings:
		for booking in new_bookings:
			try:
				get_email = db.session.query(Detail)\
					.filter(Detail.deletion_marker == None)\
					.filter(Detail.email_address == booking.email_address)\
					.filter(Detail.contact_uuid != None)\
					.filter(Detail.status != get_booking_status_id("Cancelled"))\
					.filter(Detail.status != get_booking_status_id("Updated"))\
					.order_by(Detail.booking_details_id.desc())\
					.first()

				try:
					number_prefix,number = booking.phone_number.split("-")
					number_prefix = number_prefix.strip("+")
					number = number
				except Exception:
					number_prefix = None
					number = None

				country_details = requests.get(get_country_details.format(number_prefix))

				try:
					country = country_details.json()[0]["name"]
					demonym = country_details.json()[0]["demonym"]
				except Exception:
					country = None
					demonym = None

				if booking.city:
					city = booking.city
				else:
					try:
						city = country_details.json()[0]["capital"]
					except Exception:
						city = None

				if booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
					citizenship = "Citizen"
				else:
					citizenship = None

				is_record = sf.query("SELECT Id, Email FROM Contact WHERE Email = '{}'".format(booking.email_address))

				if is_record["totalSize"] > 0:
					contact_uuid = is_record["records"][0]["Id"]

				else:
					payload = {
					  "Title": None,
					  'LastName': booking.last_name,
					  'Email': booking.email_address,
					  'FirstName': booking.first_name,
					  'Phone': number,
					  'MobilePhone' : number,
					  "Mobile_Phone_Country_Code_T__c": number_prefix,
					  'City__c' : city,
					  "Country__c": country,
					  "xsms__Mobile_Phone_Country__c": country,
					  "Citizenship__c": citizenship
					 }
					new_contact = sf.Contact.create(payload)

					contact_uuid = new_contact['id']

				return_data = {}
				bookingTotal(return_data, booking.booking_public_id)

				currency = requests.get(get_currency.format(booking.currency))
				return_data["currency"] = currency.json()["data"][0]["currency_name"]

				inventory_id = []

				if return_data["inventory_bookings"]:
					activity_checked = "true"

					for single_inventory in return_data["inventory_bookings"]:
						inventory_details = {}
						inventory_details["id"] = single_inventory["inventory_id"]
						inventory_details["name"] = single_inventory["inventory_name"]
						inventory_details["date"] = single_inventory["inventory_booking_date"]

						inventory_id.append(inventory_details)

				else:
					activity_checked = "false"

				facility_type = []
				facility_id = []

				accomodation = []
				camping = []

				if return_data["facility_bookings"]:
					for single_facility in return_data["facility_bookings"]:
						facility_details = {}

						facility_details["name"] = single_facility["facility_name"]
						facility_details["date"] = single_facility["facility_booking_check_in_date"]

						facility_type.append(single_facility["facility_type"])

						if single_facility["facility_type"] == "Camping Sites":
							camping.append(single_facility["facility_name"])
						elif single_facility["facility_type"] == "Accomodation":
							accomodation.append(single_facility["facility_name"])

						facility_id.append(facility_details)

				if facility_type:
					if "Accomodation" in facility_type:
						accomodation_checked = "true"
						camping_checked = "false"
					elif "Camping Sites" in facility_type:
						accomodation_checked = "false"
						camping_checked = "true"
					elif "Accomodation" & "Camping Sites" in facility_type:
						accomodation_checked = "true"
						camping_checked = "true"

				else:
					accomodation_checked = "false"
					camping_checked = "false"

				get_school_booking = db.session.query(SchoolBooking)\
					.join(School, SchoolBooking.school_id == School.school_public_id)\
					.add_columns(School.school_name, School.school_level)\
					.filter(SchoolBooking.deletion_marker == None)\
					.filter(SchoolBooking.booking_id == booking.booking_public_id)\
					.filter(School.deletion_marker == None)\
					.first()

				if get_school_booking:
					school_booking = "true"
					school_name = get_school_booking.school_name
					school_level = get_school_booking.school_level

				else:
					school_booking = "false"
					school_name = None
					school_level = None

				get_payment_mode = db.session.query(Transaction)\
				  .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
				  .add_columns(PaymentMethod.payment_method_name)\
				  .filter(Transaction.deletion_marker == None)\
				  .filter(Transaction.booking_id == booking.booking_public_id)\
				  .order_by(Transaction.transaction_booking_id.desc())\
				  .first()

				if get_payment_mode:
					payment_method = get_payment_mode.payment_method_name
				else:
					payment_method = None

				new_visit = sf.Visit__c.create({
				 'Residency__c': booking.country,
				 'Booking_Amount__c' : return_data["total_cost"],
				 'Check_in_Date_Time__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Check_out_Date__c' : (booking.booking_check_out_date).strftime('%Y-%m-%d'),
				 'CurrencyIsoCode' : return_data["currency"],
				 'Booking__c' : booking.booking_ref_code,
				 'Visit_Date__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Contact__c' : contact_uuid,
				 "Mode_of_Payment__c": payment_method,
				 'Indemnity_checked__c' : "true",
				 'Activity_booked__c' : activity_checked,
				 'Camping_booked__c' : camping_checked,
				 'Accomodation_Booked__c' : accomodation_checked,
				 'RecordTypeId' : "0121D0000009eEnQAI",
				 'School_Booking__c' : school_booking,
				 'Level_of_school__c' : school_level,
				 'Name_of_school__c' : school_name
				})

				get_member_booking = db.session.query(Member)\
					.filter(Member.deletion_marker == None)\
					.filter(Member.booking_id == booking.booking_public_id)\
					.first()

				if get_member_booking:
					get_salesforce = requests.get(get_membership_data.format(get_member_booking.member_id))

					if get_salesforce.status_code == 200:
						new_member_visit = sf.Member_Visits__c.create({
						 "Date__c": (booking.booking_check_in_date).strftime('%Y-%m-%d'),
						 "Membership__c": get_salesforce.json()["member_salesforce"],
						 "Number_of_people__c": int(get_member_booking.num_of_people) + int(get_member_booking.num_of_children),
						 "Number_of_vehicles__c": int(get_member_booking.num_of_vehicles)
						})
					else:
						pass

				else:
					pass

				if inventory_id:
					for each_inventory in inventory_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Activity__c' : each_inventory["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_inventory["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				if facility_id:
					for each_facility in facility_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Accomodation__c' : each_facility["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_facility["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				get_booking = db.session.query(Booking)\
				   .filter(Booking.deletion_marker == None)\
				   .filter(Booking.booking_public_id == booking.booking_public_id)\
				   .first()

				get_detail = db.session.query(Detail)\
				  .filter(Detail.deletion_marker == None)\
				  .filter(Detail.booking_id == booking.booking_public_id)\
				  .first()

				get_booking.salesforce = 1
				get_booking.salesforce_uuid = new_visit["id"]
				get_booking.updated_at = datetime.now()

				get_detail.contact_uuid = contact_uuid
				get_detail.updated_at = datetime.now()

				salesforce_data = SalesforceData(
				  salesforce_tracking_public_id = str(uuid.uuid4()),
				  booking_id = booking_id,
				  date_done = datetime.now(),
				  salesforce_uuid = new_visit["id"]
				 )
				db.session.add(salesforce_data)

				booking_activity = BookingActivity(
				 booking_activity_public_id = str(uuid.uuid4()),
				 booking_id = booking_id,
				 booking_activity_description = "Booking data pushed to Salesforce. Vist ID: " + new_visit["id"],
				 session_id = None,
				 created_at = datetime.now()
				)

				db.session.add(booking_activity)

			except Exception as identifier:
				# responseObject = {
				#     'message' : str(identifier)
				# }
				# return make_response(jsonify(responseObject)), 500
				pass
		try:
			db.session.commit()
			db.session.close()
			responseObject = {
			 'message' : 'Successfully pushed booking data to Salesforce.'
			}
			return make_response(jsonify(responseObject)), 200
		except Exception as identifier:
			db.session.rollback()
			db.session.close()
			responseObject = {
			 'message' : str(identifier)
			}
			return make_response(jsonify(responseObject)), 200
	else:
		responseObject = {
		 'message' : 'No new booking data to push to Salesforce.'
		}
		return make_response(jsonify(responseObject)), 200


@app.route('/send/visits/salesforce', methods = ["POST", "GET"])
def send_visit_data():
	details = requests.get(get_sf_details)

	sf = Salesforce(instance_url = details.json()["instance_url"], session_id = details.json()["access_token"])

	record_type_id = None

	## Querying the Visit object to get the recordTypeId
	get_visits_info = sf.Visit__c.describe()
	for info in get_visits_info["recordTypeInfos"]:
		if ("Booking" == info["name"]) or ("Booking" in info["name"]):
			record_type_id = str(info["recordTypeId"])

		else:
			pass

	if not record_type_id:
		message = []
		message.append("No recordTypeId present.")
		return jsonify({"message": message}), 422

	yesterday = datetime.now() - timedelta(days = 1)

	new_bookings = db.session.query(Booking)\
		.join(Detail, Booking.booking_public_id == Detail.booking_id)\
		.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
		 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
		 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
		 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status, Booking.salesforce,\
		 Booking.currency, Booking.salesforce_uuid,\
		 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.contact_uuid,\
		 Detail.address, Detail.additional_note, Detail.city, Detail.country, Detail.country_code)\
		.filter(Booking.deletion_marker == None)\
		.filter(Booking.salesforce == None)\
		.filter(Booking.salesforce_uuid == None)\
		.filter(Booking.status != get_booking_status_id("Cancelled"))\
		.filter(Detail.deletion_marker == None)\
		.filter(Detail.contact_uuid == None)\
		.all()

	if not new_bookings:
		message = []
		message.append("No new visit records to post at this time.")
		return jsonify({"message": message}), 200

	bookings_array = []
	array_to_push = []

	for booking in new_bookings:
		get_member_booking = db.session.query(Member)\
			.filter(Member.deletion_marker == None)\
			.filter(Member.booking_id == booking.booking_public_id)\
			.first()

		if get_member_booking:
			pass

		# if booking.booking_check_in_date.strftime("%Y-%m-%d") == yesterday.strftime("%Y-%m-%d"):
		# 	array_to_push.append(booking.booking_public_id)

		# return jsonify({"data": array_to_push}), 200

		if booking.booking_check_in_date.strftime("%Y-%m-%d") == yesterday.strftime("%Y-%m-%d"):
			array_to_push.append(booking.booking_public_id)

			try:
				get_details = db.session.query(Detail)\
				  .filter(Detail.deletion_marker == None)\
				  .filter(Detail.booking_id == booking.booking_public_id)\
				  .filter(Detail.contact_uuid == None)\
				  .filter(Detail.status != get_booking_status_id("Cancelled"))\
				  .filter(Detail.status != get_booking_status_id("Updated"))\
				  .order_by(Detail.booking_details_id.desc())\
				  .first()

				try:
					number_prefix,number = get_details.phone_number.split("-")
					number_prefix = number_prefix.strip("+")
					number = number
				except Exception:
					number_prefix = None
					number = None

				country_details = requests.get(get_country_details.format(number_prefix))

				try:
					country = country_details.json()[0]["name"]
					demonym = country_details.json()[0]["demonym"]
				except Exception:
					country = None
					demonym = None

				if get_details.city:
					city = get_details.city
				else:
					try:
						city = country_details.json()[0]["capital"]
					except Exception:
						city = None

				if booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
					citizenship = "Citizen"
				else:
					citizenship = None

				is_record = sf.query("SELECT Id, Email FROM Contact WHERE Email = '{}'".format(get_details.email_address))

				if is_record["totalSize"] > 0:
					contact_uuid = is_record["records"][0]["Id"]

				else:
					if get_details.last_name:
						last_name = get_details.last_name
					else:
						last_name = "N/A"

					payload = {
					  "Title": None,
					  'LastName': last_name,
					  'Email': get_details.email_address,
					  'FirstName': get_details.first_name,
					  'Phone': number,
					  'MobilePhone' : number,
					  "Mobile_Phone_Country_Code_T__c": number_prefix,
					  'City__c' : city,
					  "Country__c": country,
					  "xsms__Mobile_Phone_Country__c": country,
					  "Citizenship__c": citizenship
					 }
					new_contact = sf.Contact.create(payload)

					contact_uuid = new_contact['id']

				return_data = {}
				bookingTotal(return_data, booking.booking_public_id)

				currency = requests.get(get_currency.format(booking.currency))
				return_data["currency"] = currency.json()["data"][0]["currency_name"]

				inventory_id = []

				if return_data["inventory_bookings"]:
					activity_checked = "true"

					for single_inventory in return_data["inventory_bookings"]:
						inventory_details = {}
						inventory_details["id"] = single_inventory["inventory_id"]
						inventory_details["name"] = single_inventory["inventory_name"]
						inventory_details["date"] = single_inventory["inventory_booking_date"]

						inventory_id.append(inventory_details)

				else:
					activity_checked = "false"

				facility_type = []
				facility_id = []

				accomodation = []
				camping = []

				if return_data["facility_bookings"]:
					for single_facility in return_data["facility_bookings"]:
						facility_details = {}

						facility_details["name"] = single_facility["facility_name"]
						facility_details["date"] = single_facility["facility_booking_check_in_date"]

						facility_type.append(single_facility["facility_type"])

						if single_facility["facility_type"] == "Camping Sites":
							camping.append(single_facility["facility_name"])
						elif single_facility["facility_type"] == "Accomodation":
							accomodation.append(single_facility["facility_name"])

						facility_id.append(facility_details)

				if facility_type:
					if "Accomodation" in facility_type:
						accomodation_checked = "true"
						camping_checked = "false"
					elif "Camping Sites" in facility_type:
						accomodation_checked = "false"
						camping_checked = "true"
					elif "Accomodation" & "Camping Sites" in facility_type:
						accomodation_checked = "true"
						camping_checked = "true"

				else:
					accomodation_checked = "false"
					camping_checked = "false"

				get_school_booking = db.session.query(SchoolBooking)\
					.join(School, SchoolBooking.school_id == School.school_public_id)\
					.add_columns(School.school_name, School.school_level, School.school_code)\
					.filter(SchoolBooking.deletion_marker == None)\
					.filter(SchoolBooking.booking_id == booking.booking_public_id)\
					.filter(School.deletion_marker == None)\
					.first()

				if get_school_booking:
					school_booking = "true"
					school_name = get_school_booking.school_name
					school_level = get_school_booking.school_level
					school_code = get_school_booking.school_code

					students = []
					for single_student in return_data["guests"]:
						if ("student" in single_student["payment_person"]) or ("Student" in single_student["payment_person"]):
							students.append(single_student["payment_guests"])
						else:
							pass

					no_of_students = sum(students)

				else:
					school_booking = "false"
					school_name = None
					school_level = None
					school_code = None
					no_of_students = 0

				get_payment_mode = db.session.query(Transaction)\
				  .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
				  .add_columns(PaymentMethod.payment_method_name)\
				  .filter(Transaction.deletion_marker == None)\
				  .filter(Transaction.booking_id == booking.booking_public_id)\
				  .order_by(Transaction.transaction_booking_id.desc())\
				  .first()

				if get_payment_mode:
					payment_method = get_payment_mode.payment_method_name
				else:
					payment_method = None

				get_status_name = db.session.query(BookingStatus)\
					.filter(BookingStatus.booking_status_public_id == booking.status)\
					.first()

				new_visit = sf.Visit__c.create({
				 'Residency__c': booking.country,
				 'Booking_Amount__c' : return_data["total_cost"],
				 'Check_in_Date_Time__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Check_out_Date__c' : (booking.booking_check_out_date).strftime('%Y-%m-%d'),
				 'CurrencyIsoCode' : return_data["currency"],
				 'Booking__c' : booking.booking_ref_code,
				 'Visit_Date__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
				 'Contact__c' : contact_uuid,
				 "Mode_of_Payment__c": payment_method,
				 'Indemnity_checked__c' : "true",
				 'Activity_booked__c' : activity_checked,
				 'Camping_booked__c' : camping_checked,
				 'Accomodation_Booked__c' : accomodation_checked,
				 'RecordTypeId' : record_type_id,
				 'School_Booking__c' : school_booking,
				 'Level_of_school__c' : school_level,
				 'Name_of_school__c' : school_name,
				 'OBTS_School_ID__c' : school_code,
				 'OBTS_Number_of_students__c' : no_of_students,
				 'Booking_Status__c' : get_status_name.booking_status_name
				})

				if inventory_id:
					for each_inventory in inventory_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Activity__c' : each_inventory["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_inventory["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				if facility_id:
					for each_facility in facility_id:
						new_visit_activity = sf.Visit_activity__c.create({
						 'Accomodation__c' : each_facility["name"],
						 'CurrencyIsoCode' : return_data["currency"],
						 'Activity_Date__c' : (each_facility["date"]).strftime('%Y-%m-%d'),
						 'Visit__c' : new_visit["id"]
						})

				get_booking = db.session.query(Booking)\
				   .filter(Booking.deletion_marker == None)\
				   .filter(Booking.booking_public_id == booking.booking_public_id)\
				   .first()

				get_detail = db.session.query(Detail)\
				  .filter(Detail.deletion_marker == None)\
				  .filter(Detail.booking_id == booking.booking_public_id)\
				  .first()

				get_booking.salesforce = 1
				get_booking.salesforce_uuid = new_visit["id"]
				get_booking.updated_at = datetime.now()

				get_detail.contact_uuid = contact_uuid
				get_detail.updated_at = datetime.now()

				salesforce_data = SalesforceData(
				  salesforce_tracking_public_id = str(uuid.uuid4()),
				  booking_id = booking.booking_public_id,
				  date_done = datetime.now(),
				  salesforce_uuid = new_visit["id"]
				 )
				db.session.add(salesforce_data)

				bookings_array.append(booking.booking_public_id + ": Ok")

			except Exception as identifier:
				bookings_array.append(booking.booking_public_id + ": " + str(identifier))

	try:
		db.session.commit()
		db.session.close()

		if request.method == "POST":
			message = []
			message.append("Posted records to Salesforce.")
			return jsonify({"message": message, "records": bookings_array, "array_to_push": array_to_push}), 200
		elif request.method == "GET":
			return bookings_array

	except Exception as exc:
		db.session.rollback()
		db.session.close()

		if request.method == "POST":
			message = []
			message.append("Error posting records to Salesforce.")
			return jsonify({"message": message, "error": str(exc), "records": bookings_array, "array_to_push": array_to_push}), 200
		elif request.method == "GET":
			return str(exc)


def send_visit_data_scheduled():
	details = requests.get(get_sf_details)

	sf = Salesforce(instance_url = details.json()["instance_url"], session_id = details.json()["access_token"])

	record_type_id = None

	## Querying the Visit object to get the recordTypeId
	get_visits_info = sf.Visit__c.describe()

	for info in get_visits_info["recordTypeInfos"]:
		if ("Booking" == info["name"]) or ("Booking" in info["name"]):
			record_type_id = str(info["recordTypeId"])

		else:
			pass

	if not record_type_id:
		message = []
		message.append("No recordTypeId present.")
		return jsonify({"message": message}), 422

	yesterday = datetime.now() - timedelta(days = 1)
   

	dates_to_post = []

	if yesterday.strftime("%Y-%m-%d") in dates_to_post:
		dates_to_post = dates_to_post
	else:
		new_date_array = []
		new_date_array.append(yesterday.strftime("%Y-%m-%d"))
		dates_to_post = new_date_array

	new_bookings = db.session.query(Booking)\
		.join(Detail, Booking.booking_public_id == Detail.booking_id)\
		.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
		 Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
		 Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
		 Booking.created_at, Booking.updated_at, Booking.status, Booking.payment_status, Booking.salesforce,\
		 Booking.currency, Booking.salesforce_uuid,\
		 Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.contact_uuid,\
		 Detail.address, Detail.additional_note, Detail.city, Detail.country, Detail.country_code)\
		.filter(Booking.deletion_marker == None)\
		.filter(Booking.salesforce == None)\
		.filter(Booking.salesforce_uuid == None)\
		.filter(Booking.status != get_booking_status_id("Cancelled"))\
		.filter(Detail.deletion_marker == None)\
		.filter(Detail.contact_uuid == None)\
		.all()

	if not new_bookings:
		message = []
		message.append("No new visit records to post at this time.")
		return jsonify({"message": message}), 200

	bookings_array = []
	array_to_push = []

	for booking in new_bookings:

		for one_date in dates_to_post:
			if booking.booking_check_in_date.strftime("%Y-%m-%d") == one_date:
				array_to_push.append(booking.booking_public_id)

				try:
					get_details = db.session.query(Detail)\
					   .filter(Detail.deletion_marker == None)\
					   .filter(Detail.booking_id == booking.booking_public_id)\
					   .filter(Detail.contact_uuid == None)\
					   .filter(Detail.status != get_booking_status_id("Cancelled"))\
					   .filter(Detail.status != get_booking_status_id("Updated"))\
					   .order_by(Detail.booking_details_id.desc())\
					   .first()

					## TODO: Does this make a difference in the data that's posted?
					## Specifically, handling the exception: 'NoneType' object has no attribute 'email_address'
					if not get_details:
						break

					try:
						number_prefix,number = get_details.phone_number.split("-")
						number_prefix = number_prefix.strip("+")
						number = number
					except Exception:
						number_prefix = None
						number = None

					country_details = requests.get(get_country_details.format(number_prefix))

					try:
						country = country_details.json()[0]["name"]
						# This is necessary since there are some country names that aren't accepted by the Salesforce country picklist
						for one_country in country_list:
							if country_details.json()[0]["name"] in one_country["api_name"]:
								country = one_country["sf_name"]
								break
						demonym = country_details.json()[0]["demonym"]
					except Exception:
						country = None
						demonym = None

					# Certain records will not have the city field
					try:
						if get_details.city:
							city = get_details.city
						else:
							try:
								city = country_details.json()[0]["capital"]
							except Exception:
								city = None
					except Exception:
						city = None

					if booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
						citizenship = "Citizen"
					else:
						citizenship = None

					get_partner_booking = db.session.query(Partner)\
					  .filter(Partner.deletion_marker == None)\
					  .filter(Partner.booking_id == booking.booking_public_id)\
					  .first()

					if get_partner_booking:
						partner_booking = True
						partner_details = requests.get(get_partner_details.format(get_partner_booking.partner_id))

						get_contact_types = sf.Contact.describe()
						partner_name = partner_details.json()["name"]

						## Partner details have a specific flag on the type.
						## The following queries Salesforce and the partner service to determine the correct type
						if partner_details.json()["partner_type"][0] == "Tour Partners":
							for field in get_contact_types["fields"]:
								if field["name"] == "Type__c":
									for picklist_values in field["picklistValues"]:
										if picklist_values["label"] == "Tourism Partners":
											contact_type = "Tourism Partners"
											obts_type_of_booking = "Tourism Partner Booking"
										else:
											pass
						elif partner_details.json()["partner_type"][0] == "Tour Operators & Travel Agents":
							for field in get_contact_types["fields"]:
								if field["name"] == "Type__c":
									for picklist_values in field["picklistValues"]:
										if picklist_values["label"] == "Tour Operators & Agents":
											contact_type = "Tour Operators & Agents"
											obts_type_of_booking = "Tourism Partner Booking"
										else:
											pass
						else:
							contact_type = "Tourism Partners"
							obts_type_of_booking = "Tourism Partner Booking"

					else:
						partner_booking = False
						contact_type = False
						obts_type_of_booking = "Guest Booking"

					is_record = sf.query("SELECT Id, Email FROM Contact WHERE Email = '{}'".format(get_details.email_address))

					if is_record["totalSize"] > 0:
						contact_uuid = is_record["records"][0]["Id"]

					else:
						if partner_booking:
							# LastName is a required attribute, hence the need to set a value in the case of partner bookings
							last_name = "(" + contact_type + ")"
							try:
								first_name = partner_name
							except NameError:
								first_name = get_details.first_name
						else:
							if get_details.last_name:
								last_name = get_details.last_name
							else:
								last_name = "N/A"

							first_name = get_details.first_name

						# "Mobile_Phone_Country_Code_T__c": number_prefix,

						# "Type__c": contact_type
						payload = {
						 "Title": None,
						 'LastName': last_name,
						 'Email': get_details.email_address,
						 'FirstName': first_name,
						 'Phone': number,
						 'MobilePhone' : number,
						 'City__c' : city,
						 "Country__c": country,
						 "xsms__Mobile_Phone_Country__c": country,
						 "Citizenship__c": citizenship,
						 "Type__c": contact_type
						}
						new_contact = sf.Contact.create(payload)

						contact_uuid = new_contact['id']

					return_data = {}
					bookingTotal(return_data, booking.booking_public_id)

					currency = requests.get(get_currency.format(booking.currency))
					return_data["currency"] = currency.json()["data"][0]["currency_name"]

					inventory_id = []
					inventory_name_array = []

					if return_data["inventory_bookings"]:
						activity_checked = "true"

						for single_inventory in return_data["inventory_bookings"]:
							inventory_details = {}
							inventory_details["id"] = single_inventory["inventory_id"]
							inventory_details["name"] = single_inventory["inventory_name"]
							inventory_details["date"] = single_inventory["inventory_booking_date"]

							inventory_name_array.append(
							 inventory_details["name"])
							inventory_id.append(inventory_details)

					else:
						activity_checked = "false"

					facility_type = []
					facility_id = []

					accomodation = []
					camping = []

					if return_data["facility_bookings"]:
						for single_facility in return_data["facility_bookings"]:
							facility_details = {}

							facility_details["name"] = single_facility["facility_name"]
							facility_details["date"] = single_facility["facility_booking_check_in_date"]

							facility_type.append(single_facility["facility_type"])

							if single_facility["facility_type"] == "Camping Sites":
								camping.append(single_facility["facility_name"])
							elif single_facility["facility_type"] == "Accomodation":
								accomodation.append(single_facility["facility_name"])

							facility_id.append(facility_details)

					if facility_type:
						if "Accomodation" in facility_type:
							accomodation_checked = "true"
							camping_checked = "false"
						elif "Camping Sites" in facility_type:
							accomodation_checked = "false"
							camping_checked = "true"
						elif "Accomodation" & "Camping Sites" in facility_type:
							accomodation_checked = "true"
							camping_checked = "true"

					else:
						accomodation_checked = "false"
						camping_checked = "false"

					get_school_booking = db.session.query(SchoolBooking)\
						.join(School, SchoolBooking.school_id == School.school_public_id)\
						.add_columns(School.school_name, School.school_level, School.school_code)\
						.filter(SchoolBooking.deletion_marker == None)\
						.filter(SchoolBooking.booking_id == booking.booking_public_id)\
						.filter(School.deletion_marker == None)\
						.first()

					if get_school_booking:
						school_booking = "true"
						school_name = get_school_booking.school_name
						school_level = get_school_booking.school_level
						school_code = get_school_booking.school_code

						students = []
						for single_student in return_data["guests"]:
							if ("student" in single_student["payment_person"]) or ("Student" in single_student["payment_person"]):
								students.append(single_student["payment_guests"])
							else:
								pass

						no_of_students = sum(students)

					else:
						school_booking = "false"
						school_name = None
						school_level = None
						school_code = None
						no_of_students = 0

					get_payment_mode = db.session.query(Transaction)\
						.join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
						.add_columns(PaymentMethod.payment_method_name)\
						.filter(Transaction.deletion_marker == None)\
						.filter(Transaction.booking_id == booking.booking_public_id)\
						.order_by(Transaction.transaction_booking_id.desc())\
						.first()

					if get_payment_mode:
						payment_method = get_payment_mode.payment_method_name
					else:
						payment_method = None

					get_status_name = db.session.query(BookingStatus)\
						.filter(BookingStatus.booking_status_public_id == booking.status)\
						.first()


					activity_string = ','.join(inventory_name_array)
					new_visit = sf.Visit__c.create({
					 'Residency__c': booking.country,
					 'Booking_Amount__c' : return_data["total_cost"],
					 'Check_in_Date_Time__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
					 'Check_out_Date__c' : (booking.booking_check_out_date).strftime('%Y-%m-%d'),
					 'CurrencyIsoCode' : return_data["currency"],
					 'Booking__c' : booking.booking_ref_code,
					 'Visit_Date__c' : (booking.booking_check_in_date).strftime('%Y-%m-%d'),
					 'Contact__c' : contact_uuid,
					 "Mode_of_Payment__c": payment_method,
					 'Indemnity_checked__c' : "true",
					 'Activity_booked__c' : activity_checked,
					 'Camping_booked__c' : camping_checked,
					 'Accomodation_Booked__c' : accomodation_checked,
					 'RecordTypeId' : record_type_id,
					 'School_Booking__c' : school_booking,
					 'Level_of_school__c' : school_level,
					 'Name_of_school__c' : school_name,
					 'OBTS_School_ID__c' : school_code,
					 'OBTS_Number_of_students__c' : no_of_students,
					 'Booking_Status__c' : get_status_name.booking_status_name,
					 'Type_of_OBTS_Booking__c' : obts_type_of_booking,
					  'Activity__c':
					 activity_string
					})

					get_member_booking = db.session.query(Member)\
						.filter(Member.deletion_marker == None)\
						.filter(Member.booking_id == booking.booking_public_id)\
						.first()

					if get_member_booking:
						get_salesforce = requests.get(get_membership_data.format(get_member_booking.member_id))

						opportunity_record = sf.query("SELECT Id FROM Opportunity WHERE Contact_Email_del__c = '{}'".format(booking.email_address))
						opp_array = opportunity_record["records"]

						if len(opp_array) == 0:
							pass
						else:
							opp = opportunity_record["records"][0]["Id"]

							if get_salesforce.status_code == 200:
								new_member_visit = sf.Member_Visits__c.create({
								"Date__c": (booking.booking_check_in_date).strftime('%Y-%m-%d'),
								"Membership__c": opp,
								"Number_of_people__c": int(get_member_booking.num_of_people) + int(get_member_booking.num_of_children),
								"Number_of_vehicles__c": int(get_member_booking.num_of_vehicles)
								})
							else:
								pass

					else:
						pass

					if inventory_id:
						for each_inventory in inventory_id:
							new_visit_activity = sf.Visit_activity__c.create({
							 'Activity__c' : each_inventory["name"],
							 'CurrencyIsoCode' : return_data["currency"],
							 'Activity_Date__c' : (each_inventory["date"]).strftime('%Y-%m-%d'),
							 'Visit__c' : new_visit["id"]
							})

					if facility_id:
						for each_facility in facility_id:
							new_visit_activity = sf.Visit_activity__c.create({
							 'Accomodation__c' : each_facility["name"],
							 'CurrencyIsoCode' : return_data["currency"],
							 'Activity_Date__c' : (each_facility["date"]).strftime('%Y-%m-%d'),
							 'Visit__c' : new_visit["id"]
							})

					get_booking = db.session.query(Booking)\
					   .filter(Booking.deletion_marker == None)\
					   .filter(Booking.booking_public_id == booking.booking_public_id)\
					   .first()

					get_detail = db.session.query(Detail)\
					  .filter(Detail.deletion_marker == None)\
					  .filter(Detail.booking_id == booking.booking_public_id)\
					  .first()

					get_booking.salesforce = 1
					get_booking.salesforce_uuid = new_visit["id"]
					get_booking.updated_at = datetime.now()

					get_detail.contact_uuid = contact_uuid
					get_detail.updated_at = datetime.now()

					salesforce_data = SalesforceData(
					  salesforce_tracking_public_id = str(uuid.uuid4()),
					  booking_id = booking.booking_public_id,
					  date_done = datetime.now(),
					  salesforce_uuid = new_visit["id"]
					 )
					db.session.add(salesforce_data)

					# bookings_array.append(booking.booking_public_id + ": Ok")
					salesforce_data = {}
					salesforce_data["booking"] = booking.booking_public_id
					salesforce_data["status"] = "Posted"
					salesforce_data["exception"] = ""

					bookings_array.append(salesforce_data)

				except Exception as identifier:
					# bookings_array.append(booking.booking_public_id + ": " + str(identifier))
					salesforce_data = {}
					salesforce_data["booking"] = booking.booking_public_id
					salesforce_data["status"] = "Error"
					salesforce_data["exception"] = str(identifier)

					bookings_array.append(salesforce_data)
					# raise Exception(identifier)

	try:
		db.session.commit()
		db.session.close()
		send_salesforce_email(bookings_array)

	except Exception as exc:
		db.session.rollback()
		db.session.close()
		send_salesforce_email(bookings_array)

def send_booking_info_to_salesforce(booking_id):

	details = requests.get(get_sf_details)

	sf = Salesforce(instance_url=details.json()["instance_url"],
	 session_id=details.json()["access_token"])

	new_bookings = db.session.query(Booking)\
	 .join(Detail, Booking.booking_public_id == Detail.booking_id)\
	 .join(BookingType, Booking.booking_type == BookingType.booking_type_public_id)\
	 .add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_check_in_date,\
	  Booking.booking_check_out_date, Booking.actual_booking_check_in_date, Booking.actual_booking_check_out_date,\
	  Booking.booking_done_by, Booking.checked_in, Booking.checked_out, Booking.booking_ref_code, Booking.session_id,\
	  Booking.created_at, Booking.updated_at,Booking.status, Booking.status, Booking.payment_status, Booking.salesforce,\
	  Booking.currency, Booking.salesforce_uuid,\
	  Detail.first_name, Detail.last_name, Detail.email_address, Detail.phone_number, Detail.contact_uuid,\
	  Detail.address, Detail.additional_note, Detail.city, Detail.country, Detail.country_code,\
	  BookingType.booking_type_name, BookingType.booking_type_description)\
	 .filter(Booking.deletion_marker == None)\
	 .filter(Booking.salesforce == None)\
	 .filter(Booking.salesforce_uuid == None)\
	 .filter(Booking.status != get_booking_status_id("Cancelled"))\
	 .filter(Booking.booking_public_id == booking_id)\
	 .filter(Detail.deletion_marker == None)\
	 .filter(Detail.contact_uuid == None)\
	 .filter(BookingType.deletion_marker == None)\
	 .all()


	if not new_bookings:
		message = []
		message.append("Booking to post to salesforce was not found.")
		return jsonify({"message": message}), 200

	bookings_array = []
	array_to_push = []

	for booking in new_bookings:

		booking_data={}
		booking_data["booking_date"] = booking.created_at
		booking_data["booking_ref"] = booking.booking_ref_code
		booking_data["booking_type"] = booking.booking_type_name

		try:
			get_details = db.session.query(Detail)\
			 .filter(Detail.deletion_marker == None)\
			 .filter(Detail.booking_id == booking.booking_public_id)\
			 .filter(Detail.contact_uuid == None)\
			 .filter(Detail.status != get_booking_status_id("Cancelled"))\
			 .filter(Detail.status != get_booking_status_id("Updated"))\
			 .order_by(Detail.booking_details_id.desc())\
			 .first()

			## TODO: Does this make a difference in the data that's posted?
			## Specifically, handling the exception: 'NoneType' object has no attribute 'email_address'
			if not get_details:
				break

			try:
				number_prefix, number = get_details.phone_number.split("-")
				number_prefix = number_prefix.strip("+")
				number = number
			except Exception:
				number_prefix = None
				number = None

			country_details = requests.get(
			 get_country_details.format(number_prefix))

			try:
				country = country_details.json()[0]["name"]
				# This is necessary since there are some country names that aren't accepted by the Salesforce country picklist
				for one_country in country_list:
					if country_details.json(
					)[0]["name"] in one_country["api_name"]:
						country = one_country["sf_name"]
						break
				demonym = country_details.json()[0]["demonym"]
			except Exception:
				country = None
				demonym = None


			try:
				if get_details.city:
					city = get_details.city
				else:
					try:
						city = country_details.json()[0]["capital"]
					except Exception:
						city = None
			except Exception:
				city = None

			if booking.currency == "162fface-f5f1-41de-913b-d2bb784dda3a":
				citizenship = "Citizen"
			else:
				citizenship = None

			get_partner_booking = db.session.query(Partner)\
			 .filter(Partner.deletion_marker == None)\
			 .filter(Partner.booking_id == booking.booking_public_id)\
			 .first()

			if get_partner_booking:
				partner_booking = True
				partner_details = requests.get(
				 get_partner_details.format(get_partner_booking.partner_id))

				get_contact_types = sf.Contact.describe()
				partner_name = partner_details.json()["name"]


				if partner_details.json(
				)["partner_type"][0] == "Tour Partners":
					for field in get_contact_types["fields"]:
						if field["name"] == "Type__c":
							for picklist_values in field["picklistValues"]:
								if picklist_values[
								  "label"] == "Tourism Partners":
									contact_type = "Tourism Partners"
									obts_type_of_booking = "Tourism Partner Booking"
								else:
									pass
				elif partner_details.json(
				)["partner_type"][0] == "Tour Operators & Travel Agents":
					for field in get_contact_types["fields"]:
						if field["name"] == "Type__c":
							for picklist_values in field["picklistValues"]:
								if picklist_values[
								  "label"] == "Tour Operators & Agents":
									contact_type = "Tour Operators & Agents"
									obts_type_of_booking = "Tourism Partner Booking"
								else:
									pass
				else:
					contact_type = "Tourism Partners"
					obts_type_of_booking = "Tourism Partner Booking"

			else:
				partner_booking = False
				contact_type = False
				obts_type_of_booking = "Guest Booking"

			is_record = sf.query(
			 "SELECT Id, Email FROM Contact WHERE Email = '{}'".format(
			  get_details.email_address))


			if is_record["totalSize"] > 0:
				contact_uuid = is_record["records"][0]["Id"]

			else:
				if partner_booking:

					last_name = "(" + contact_type + ")"
					try:
						first_name = partner_name
					except NameError:
						first_name = get_details.first_name
				else:
					if get_details.last_name:
						last_name = get_details.last_name
					else:
						last_name = "N/A"

					first_name = get_details.first_name


				payload = {
				 "Title": None,
				 'LastName': last_name,
				 'Email': get_details.email_address,
				 'FirstName': first_name,
				 'Phone': number,
				 'MobilePhone': number,
				 'City__c': city,
				 "Country__c": country,
				 "xsms__Mobile_Phone_Country__c": country,
				 "Citizenship__c": citizenship,
				 "Type__c": contact_type
				}

				new_contact = sf.Contact.create(payload)

				contact_uuid = new_contact['id']

			return_data = {}
			bookingTotal(return_data, booking.booking_public_id)

			currency = requests.get(get_currency.format(booking.currency))
			return_data["currency"] = currency.json(
			)["data"][0]["currency_name"]

			inventory_id = []

			if return_data["inventory_bookings"]:
				activity_checked = "true"

				for single_inventory in return_data["inventory_bookings"]:
					inventory_details = {}
					inventory_details["id"] = single_inventory["inventory_id"]
					inventory_details["name"] = single_inventory[
					 "inventory_name"]
					inventory_details["date"] = single_inventory[
					 "inventory_booking_date"]

					inventory_id.append(inventory_details)

			else:
				activity_checked = "false"

			facility_type = []
			facility_id = []

			accomodation = []
			camping = []

			if return_data["facility_bookings"]:
				for single_facility in return_data["facility_bookings"]:
					facility_details = {}

					facility_details["name"] = single_facility["facility_name"]
					facility_details["date"] = single_facility[
					 "facility_booking_check_in_date"]

					facility_type.append(single_facility["facility_type"])

					if single_facility["facility_type"] == "Camping Sites":
						camping.append(single_facility["facility_name"])

					elif single_facility["facility_type"] == "Accomodation":
						accomodation.append(single_facility["facility_name"])

					facility_id.append(facility_details)

			if facility_type:
				if "Accomodation" in facility_type:
					accomodation_checked = "true"
					camping_checked = "false"
				elif "Camping Sites" in facility_type:
					accomodation_checked = "false"
					camping_checked = "true"
				elif "Accomodation" & "Camping Sites" in facility_type:
					accomodation_checked = "true"
					camping_checked = "true"

			else:
				accomodation_checked = "false"
				camping_checked = "false"

			get_school_booking = db.session.query(SchoolBooking)\
			 .join(School, SchoolBooking.school_id == School.school_public_id)\
			 .add_columns(School.school_name, School.school_level, School.school_code)\
			 .filter(SchoolBooking.deletion_marker == None)\
			 .filter(SchoolBooking.booking_id == booking.booking_public_id)\
			 .filter(School.deletion_marker == None)\
			 .first()

			if get_school_booking:
				school_booking = "true"
				school_name = get_school_booking.school_name
				school_level = get_school_booking.school_level
				school_code = get_school_booking.school_code

				students = []
				for single_student in return_data["guests"]:
					if ("student" in single_student["payment_person"]) or (
					  "Student" in single_student["payment_person"]):
						students.append(single_student["payment_guests"])
					else:
						pass

				no_of_students = sum(students)

			else:
				school_booking = "false"
				school_name = None
				school_level = None
				school_code = None
				no_of_students = 0

			get_payment_mode = db.session.query(Transaction)\
			 .join(PaymentMethod, Transaction.transaction_payment_method == PaymentMethod.payment_method_public_id)\
			 .add_columns(PaymentMethod.payment_method_name)\
			 .filter(Transaction.deletion_marker == None)\
			 .filter(Transaction.booking_id == booking.booking_public_id)\
			 .order_by(Transaction.transaction_booking_id.desc())\
			 .first()

			if get_payment_mode:
				payment_method = get_payment_mode.payment_method_name
			else:
				payment_method = None

			get_status_name = db.session.query(BookingStatus)\
			 .filter(BookingStatus.booking_status_public_id == booking.status)\
			 .first()


			if inventory_id:
				for each_inventory in inventory_id:
					booking_data["booked_activity"] = each_inventory["name"]

			else:
				booking_data["booked_activity"] = 'None'

			if facility_id:
				for each_facility in facility_id:
					booking_data["booked_accomodation"] = each_facility["name"]
			else:
				booking_data["booked_accomodation"] = 'None'


			if camping:
				booking_data["booked_campsite"] = camping[0]
			else:
				booking_data["booked_campsite"] = 'None'


			books = {
			 'Booking_Date__c':
			 booking_data["booking_date"].strftime('%Y-%m-%d'),
			 'Booking_Ref__c':
			 booking_data["booking_ref"],
			 'Contact__c':
			 contact_uuid,
			 'Booking_Status__c':
			 get_status_name.booking_status_name,
			 'Type_of_Booking__c':
			 booking_data["booking_type"],
			 'Booked_Accomodation__c':
			 booking_data["booked_accomodation"],
			 'Campsite_Booked__c':
			 booking_data["booked_campsite"],
			 'Booked_Activity__c':
			 booking_data["booked_activity"],
			}

			new_Booking = sf.Bookings__c.create({
			 'Booking_Date__c':
			 booking_data["booking_date"].strftime('%Y-%m-%d'),
			 'Booking_Ref__c':
			 booking_data["booking_ref"],
			 'Contact__c':
			 contact_uuid,
			 'Booking_Status__c':
			 get_status_name.booking_status_name,
			 'Type_of_Booking__c':
			 booking_data["booking_type"],
			 'Booked_Accomodation__c':
			 booking_data["booked_accomodation"],
			 'Campsite_Booked__c':
			 booking_data["booked_campsite"],
			 'Booked_Activity__c':
			 booking_data["booked_activity"],
			})

			get_booking = db.session.query(Booking)\
			 .filter(Booking.deletion_marker == None)\
			 .filter(Booking.booking_public_id == booking.booking_public_id)\
			 .first()

			get_detail = db.session.query(Detail)\
			 .filter(Detail.deletion_marker == None)\
			 .filter(Detail.booking_id == booking.booking_public_id)\
			 .first()

			#get_booking.salesforce = 1
			#get_booking.salesforce_uuid = new_Booking["id"]
			#get_booking.updated_at = datetime.now()

			#get_detail.contact_uuid = contact_uuid
			#get_detail.updated_at = datetime.now()

			# salesforce_data = SalesforceData(
			#salesforce_tracking_public_id=str(uuid.uuid4()),
			# booking_id=booking.booking_public_id,
			# date_done=datetime.now(),
			#salesforce_uuid=new_Booking["id"])
			# db.session.add(salesforce_data)


			salesforce_data = {}
			salesforce_data["booking"] = booking.booking_public_id
			salesforce_data["status"] = "Posted"
			salesforce_data["exception"] = ""

			bookings_array.append(salesforce_data)

		except Exception as identifier:

			salesforce_data = {}
			salesforce_data["booking"] = booking.booking_public_id
			salesforce_data["status"] = "Error"
			salesforce_data["exception"] = str(identifier)

			bookings_array.append(salesforce_data)


	try:
		db.session.commit()
		db.session.close()


	except Exception as exc:
		db.session.rollback()
		db.session.close()
