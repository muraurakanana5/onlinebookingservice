import base64
import math
import os
import sys
import traceback
import uuid
from datetime import date, datetime, timedelta

import pdfkit
import pymysql
import requests
import sendgrid
from flask import (Flask, json, jsonify, redirect, render_template, request,
				   url_for, Response, make_response, send_file)
from sendgrid.helpers.mail import *

from database.booking_activity_log import BookingActivity
from database.booking_details import Detail
from database.booking_guest_types import GuestType
from database.booking_guests import BookingGuest
from database.booking_payments import BookingPayment
from database.booking_status import BookingStatus
from database.booking_tickets import Ticket
from database.booking_types import BookingType
from database.bookings import Booking
from database.credit_note import CreditNote
from database.credit_note_facilities import CreditNoteFacility
from database.credit_note_guests import CreditNoteGuest
from database.credit_note_inventory import CreditNoteInventory
from database.credit_note_vehicles import CreditNoteVehicle
from database.bookings_notes import Note
from database.check_in_vehicles import CheckInVehicle
from database.destination import Destination
from database.donation import Donation
from database.facility import Facility
from database.gate import Gate
from database.gatepass import Gatepass
from database.gatepass_details import GatepassDetail
from database.gatepass_guests import GatepassGuest
from database.gatepass_vehicles import GatepassVehicle
from database.inventory import Inventory
from database.mandatory_payments import Mandatory
from database.modify import Modify
from database.partner import Partner
from database.payment_methods import PaymentMethod
from database.salesforce import SalesforceData
from database.school import School
from database.school_booking import SchoolBooking
from database.sun import Sun
from database.transaction import Transaction
from database.vehicle import Vehicle
from database.vehicle_details import VehicleDetail
from functions.async_functions import AsyncRequests
from functions.booking_snippets import bookingTotal, get_details_donation_cause, get_details_currency, creditNoteTotal
from functions.currency_operators import *
from functions.date_operators import *
from functions.validation import validateEmail
# File imports
from routes import (app, bookings_logger, db,
					receipt_print_options, sessionTracking, ticketGenerator, invoice_options)
from routes.bookings_urls import currencyPostProcessor, get_booking_status_id
from routes.seed_functions import (BookingGuestTypesSeed, BookingStatusSeed,
								   BookingTypeSeed, MandatoryPaymentsSeed)
from variables import *


def close(self):
	self.session.close()


def currencyHandler(currencyTo, currencyFrom, amount):
	if currencyTo == currencyFrom:
		return amount
	elif currencyTo != currencyFrom:
		if currencyFrom == "162fface-f5f1-41de-913b-d2bb784dda3a":
			get_rate = requests.get(get_buy_sell_rate.format(currencyTo))

			try:
				buying = get_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_rate.json()["data"][0]["currency_sell_amount"]

				value = float(amount) / float(buying)
				return float(value)
			except Exception:
				value = 0
				return float(value)

		elif currencyTo == "162fface-f5f1-41de-913b-d2bb784dda3a":
			get_rate = requests.get(get_buy_sell_rate.format(currencyFrom))

			try:
				buying = get_rate.json()["data"][0]["currency_buy_amount"]
				selling = get_rate.json()["data"][0]["currency_sell_amount"]

				value = float(amount) * float(selling)
				return float(value)
			except Exception:
				value = 0
				return float(value)
		
		else:
			get_from_rate = requests.get(get_buy_sell_rate.format(currencyFrom))
			get_to_rate = requests.get(get_buy_sell_rate.format(currencyTo))

			try:
				from_buying = get_from_rate.json()["data"][0]["currency_buy_amount"]
				from_selling = get_from_rate.json()["data"][0]["currency_sell_amount"]

				to_buying = get_to_rate.json()["data"][0]["currency_buy_amount"]
				to_selling = get_to_rate.json()["data"][0]["currency_sell_amount"]

				temp_from = float(amount) * float(from_selling)
				temp_to = float(temp_from) / float(to_buying)
				value = temp_to
				return float(value)
			except Exception:
				value = 0
				return float(value)


@app.route("/print/receipt", methods = ["POST"])
def print_receipt():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")
		
	if messages:
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422
	
	today = datetime.now().strftime("%Y-%m-%d")
	
	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == request.json["booking_id"])\
							   .filter(Booking.deletion_marker == None)\
							   .first()

	if not return_booking:
		output = []
		output.append("That booking does not seem to exist.")
		return jsonify({"message": output}), 200
	else:
		if not return_booking.ticket:
			get_last_ticket = db.session.query(Ticket)\
										.filter(Ticket.deletion_marker == None)\
										.filter(Ticket.ticket_date == datetime.now().strftime("%Y-%m-%d"))\
										.first()

			if not get_last_ticket:
				booking_ticket = ticketGenerator(None)

				ticket = Ticket(
					booking_tickets_public_id = str(uuid.uuid4()),
					ticket_date = datetime.now().strftime("%Y-%m-%d"),
					first_ticket = booking_ticket,
					last_ticket = booking_ticket,
					session_id = request.json["session_id"],
					created_at = datetime.now(),
					updated_at = datetime.now()
				)

				db.session.add(ticket)
			
			else:
				booking_ticket = ticketGenerator(get_last_ticket.last_ticket)

				get_last_ticket.last_ticket = booking_ticket
				get_last_ticket.updated_at = datetime.now()

			return_booking.ticket = booking_ticket
		
		else:
			booking_ticket = return_booking.ticket
		
		# options = {
		# 	'page-width': '225',
		# 	'page-height' : '910',
		# 	'minimum-font-size' : '50',
		# 	'margin-top': '0.0in',
		# 	'margin-right': '0.0in',
		# 	'margin-bottom': '0.0in',
		# 	'margin-left': '0.0in',
		# 	'dpi': '400',
		# 	'encoding': "UTF-8",
		# 	'custom-header' : [
		# 		('Accept-Encoding', 'gzip')
		# 	],
		# 	'zoom': '3.4'
		# }

		get_booking_details = db.session.query(Booking)\
										.join(Detail, Booking.booking_public_id == Detail.booking_id)\
										.join(Gatepass, Booking.booking_public_id == Gatepass.booking_id)\
										.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_done_by,\
													Booking.gatepass_id, Booking.currency, Booking.booking_check_out_date,\
													Booking.currency, Booking.booking_check_in_date, Booking.booking_ref_code,\
													Gatepass.gatepass_public_id, Gatepass.gatepass_ref_code,\
													Detail.additional_note)\
										.filter(Booking.deletion_marker == None)\
										.filter(Detail.deletion_marker == None)\
										.filter(Gatepass.deletion_marker == None)\
										.filter(Booking.booking_public_id == request.json["booking_id"])\
										.first()

		print_data = {}
		
		bookingTotal(print_data, request.json["booking_id"])

		print_data["date"] = datetime.now().strftime("%d %B %Y %I:%M %p")
		print_data["gatepass_id"] = booking_ticket
		print_data["done_by"] = get_booking_details.booking_done_by
		print_data["additional_note"] = get_booking_details.additional_note
		print_data["booking_ref"] = get_booking_details.booking_ref_code
		
		currency = requests.get(get_currency.format(get_booking_details.currency))
		print_data["currency"] = currency.json()["data"][0]["currency_name"]

		print_data["check_out_date"] = get_booking_details.booking_check_out_date.strftime("%d %B %Y")
		print_data["check_in_date"] = get_booking_details.booking_check_in_date.strftime("%d %B %Y")

		get_teller = requests.get(get_single_user.format(request.json["session_id"]))

		try:
			print_data["teller"] = get_teller.json()["first_name"] + " " + get_teller.json()["last_name"]
		except Exception:
			print_data["teller"] = "N/A"

		get_payment = db.session.query(BookingPayment)\
								.join(PaymentMethod, BookingPayment.payment_method == PaymentMethod.payment_method_public_id)\
								.add_columns(PaymentMethod.payment_method_name)\
								.filter(BookingPayment.deletion_marker == None)\
								.filter(BookingPayment.booking_id == get_booking_details.booking_public_id)\
								.first()

		if get_payment:
			print_data["payment_method"] = get_payment.payment_method_name
		else:
			print_data["payment_method"] = "N/A"

		vehicle_data = []
		total_cost = []
		total_vehicle = []

		total = sum(total_cost)
		after_tax = total / 1.16
		tax = total - after_tax
		
		print_data["tax_amount"] = "{:,}".format(round(currencyHandler("162fface-f5f1-41de-913b-d2bb784dda3a",\
									get_booking_details.currency, tax), 2))
		print_data["amount"] = "{:,}".format(round(currencyHandler("162fface-f5f1-41de-913b-d2bb784dda3a",\
								get_booking_details.currency, after_tax), 2))
		print_data["total_cost"] = "{:,}".format(round(currencyHandler("162fface-f5f1-41de-913b-d2bb784dda3a",\
									get_booking_details.currency, total))) + ".0"

		print_data["reprint"] = True

		get_vehicles = db.session.query(CheckInVehicle)\
								 .filter(CheckInVehicle.deletion_marker == None)\
								 .filter(CheckInVehicle.booking_id == request.json["booking_id"])\
								 .all()
		
		driver_data = {}
		if get_vehicles:
			driver_data["driver_status"] = True

			vehicle_info = []
			for vehicle in get_vehicles:
				info = {}
				info["driver"] = vehicle.check_vehicle_driver.title()
				info["reg_plate"] = vehicle.check_vehicle_reg.upper()
				info["disk"] = vehicle.check_vehicle_disc

				vehicle_info.append(info)

			driver_data["vehicles"] = vehicle_info

		else:
			driver_data["driver_status"] = False

		receipt = render_template("receipt-template.html", data = print_data, drivers = driver_data)
		config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")

		pdfkit.from_string(receipt, 'tickets/receipt-' + get_booking_details.gatepass_ref_code + '-reprint.pdf', options = receipt_print_options, configuration = config)

		# get_file = open("receipt.txt", "r")
		# pdfkit.from_string(get_file, 'tickets/receipt-' + get_booking_details.gatepass_ref_code + '-reprint.pdf', options = receipt_print_options)

		return jsonify({"message": "Success.",
						"url": 'https://' + app.config['SERVER'] + '/docs/tickets/receipt-' + get_booking_details.gatepass_ref_code + '-reprint.pdf'}), 200


@app.route("/print/invoice", methods = ["POST"])
def print_invoice():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")
		
	if messages:
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == request.json["booking_id"])\
							   .filter(Booking.deletion_marker == None)\
							   .first()

	if not return_booking:
		output = []
		output.append("The selected booking does not seem to exist.")
		return jsonify({"message": output}), 412

	get_booking_details = db.session.query(Booking)\
									.join(Detail, Booking.booking_public_id == Detail.booking_id)\
									.join(Gatepass, Booking.booking_public_id == Gatepass.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_done_by,\
												 Booking.gatepass_id, Booking.currency, Booking.booking_check_out_date,\
												 Booking.currency, Booking.booking_check_in_date, Booking.booking_ref_code,\
												 Booking.payment_status, Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time,\
												 Gatepass.gatepass_public_id, Gatepass.gatepass_ref_code, Gatepass.destination,\
												 Detail.additional_note, Detail.phone_number, Detail.email_address)\
									.filter(Booking.deletion_marker == None)\
									.filter(Detail.deletion_marker == None)\
									.filter(Gatepass.deletion_marker == None)\
									.filter(Gatepass.status != get_booking_status_id("Updated"))\
									.filter(Booking.booking_public_id == request.json["booking_id"])\
									.first()
	
	return_data = {}

	bookingTotal(return_data, request.json["booking_id"])

	booking_ref_code = return_booking.booking_ref_code

	return_data["booking_ref"] = return_booking.booking_ref_code
	return_data["booking_check_in_date"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out_date"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_check_in"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_done_by"] = get_booking_details.booking_done_by
	return_data["phone_number"] = get_booking_details.phone_number
	return_data["email_address"] = get_booking_details.email_address
	return_data["buying_rate"] = get_booking_details.currency_buying_rate_at_time
	return_data["selling_rate"] = get_booking_details.currency_selling_rate_at_time
	return_data["booking_currency"] = get_booking_details.currency
	return_data["created_at"] = return_booking.created_at.strftime("%d %b %Y")

	try:
		return_data["invoice_currency"] = request.json["currency_id"]

		currency = requests.get(get_currency.format(request.json["currency_id"]))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]
	
	except Exception:
		return_data["invoice_currency"] = get_booking_details.currency

		currency = requests.get(get_currency.format(get_booking_details.currency))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]

	# vat = requests.get(get_latest_vat_rate)
	# return_data["vat_rate"] = int(vat.json()["data"][0]["vat_rate"])
	return_data["vat_rate"] = return_data["_vat_rate"]

	# catering = requests.get(get_latest_cater_levy_rate)
	# return_data["catering_levy"] = int(catering.json()["data"][0]["levy_rate"])
	return_data["catering_levy"] = return_data["_catering_levy_rate"]

	if return_booking.booking_check_in_date.strftime("%d %b %Y") == return_booking.booking_check_out_date.strftime("%d %b %Y"):
		return_data["unit"] = "Day"
	else:
		return_data["unit"] = "Night"

	# get_payment = db.session.query(BookingPayment)\
	# 						.join(PaymentMethod, BookingPayment.payment_method == PaymentMethod.payment_method_public_id)\
	# 						.add_columns(PaymentMethod.payment_method_name)\
	# 						.filter(BookingPayment.deletion_marker == None)\
	# 						.filter(BookingPayment.booking_id == get_booking_details.booking_public_id)\
	# 						.first()

	# if get_payment:
	# 	return_data["payment_method"] = get_payment.payment_method_name
	# 	return_data["payment_status"] = "Paid"
	# else:
	# 	return_data["payment_method"] = "N/A"
	# 	return_data["payment_status"] = "Not Paid"

	if get_booking_details.payment_status == 1:
		return_data["payment_status"] = "Paid"
	if get_booking_details.payment_status == 2:
		return_data["payment_status"] = "Deposit"
	if get_booking_details.payment_status == 3:
		return_data["payment_status"] = "Complimentary"
	if get_booking_details.payment_status == 4:
		return_data["payment_status"] = "To Invoice"
	if not get_booking_details.payment_status:
		return_data["payment_status"] = "Not Paid"
	
	try:
		if get_booking_details.destination:
			get_destination = db.session.query(Destination)\
										.filter(Destination.gatepass_destination_public_id == get_booking_details.destination)\
										.first()
			
			return_data["destination"] = get_destination.gatepass_destination_name

		else:
			return_data["destination"] = "N/A"
	except Exception:
		return_data["destination"] = "N/A"

	asyncRequests = AsyncRequests()
	id_array = []
	
	get_notes = db.session.query(Note)\
						  .filter(Note.deletion_marker == None)\
						  .filter(Note.show_on_invoice != None)\
						  .filter(Note.booking_id == get_booking_details.booking_public_id)\
						  .all()

	if not get_notes:
		pass
	else:
		for one_note in get_notes:
			## Append booking note session IDs to id_array
			id_array.append(one_note.session_id)

	## Query Aumra service
	post_data = {"users_ids": id_array}
	return_user = asyncRequests.launchPostRequest(url = get_user_from_aumra, json = post_data)
	
	## Adding booking notes
	notes = []
	
	if not get_notes:
		pass
	else:
		for single_note in get_notes:
			return_notes = {}
			return_notes["booking_note_public_id"] = single_note.booking_notes_public_id
			return_notes["note"] = single_note.note
			
			try:
				for user in return_user["data"]:
					if user["public_id"] == single_note.session_id:
						name = user["full_name"]

			except (IndexError, KeyError):
				name = "N/A"

			except (AttributeError, UnboundLocalError):
				name = "Network Error"

			return_notes["user"] = name
			return_notes["created_at"] = single_note.created_at
			
			notes.append(return_notes)

	get_partner_booking = db.session.query(Partner)\
									.filter(Partner.booking_id == get_booking_details.booking_public_id)\
									.first()

	if get_partner_booking:
		return_data["partner_booking"] = True
		return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
	else:
		return_data["partner_booking"] = False

	get_school_booking = db.session.query(SchoolBooking)\
								   .filter(SchoolBooking.booking_id == get_booking_details.booking_public_id)\
								   .first()

	if get_school_booking:
		return_data["school_booking"] = True
		return_data["school_name"] = get_school_booking.school_details.school_name
		return_data["primary_contact"] = get_school_booking.school_details.primary_contact
	else:
		return_data["school_booking"] = False

	get_payments = db.session.query(Transaction)\
							 .filter(Transaction.deletion_marker == None)\
							 .filter(Transaction.booking_id == get_booking_details.booking_public_id)\
							 .all()

	transaction_data = []

	for single_payment in get_payments:
		transaction_data.append(currencyPostProcessor(get_booking_details.currency, single_payment.transaction_payment_currency, single_payment.transaction_total,\
													  single_payment.payment_currency_buying_rate_at_time, single_payment.payment_currency_selling_rate_at_time,))
	
	payment_total = sum(transaction_data)

	return_data["payment_total"] = payment_total
	return_data["payment_balance"] = return_data["total_cost"] - payment_total
	
	print_data = return_data

	get_all_booking_donations = db.session.query(Donation)\
										  .filter(Donation.deletion_marker == None)\
										  .filter(Donation.booking_id == get_booking_details.booking_public_id)\
										  .all()

	donation_data = []
	if get_all_booking_donations:
		for single_donation in get_all_booking_donations:
			return_donation_data = {}
			return_donation_data["donation_public_id"] = single_donation.donation_public_id
			return_donation_data["donation_currency"] = single_donation.donation_currency
			return_donation_data["donation_amount"] = float(single_donation.donation_amount)
			return_donation_data["donation_cause"] = single_donation.donation_cause

			currency_details = get_details_currency(single_donation.donation_currency)
			return_donation_data["donation_currency_name"] = currency_details.json()["data"][0]["currency_name"]

			cause_details = get_details_donation_cause(single_donation.donation_cause)
			return_donation_data["donation_cause_name"] = cause_details.json()["name"]
			
			donation_data.append(return_donation_data)

	invoice = render_template("invoice.html", data = print_data, booking_notes = notes, donations = donation_data)
	config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
	current_date = datetime.now()

	# pdfkit.from_string(invoice, 'invoices/invoice-' + booking_ref_code + '.pdf', options = invoice_print_options, configuration = config)
	pdfkit.from_string(invoice, 'invoices/invoice-' + booking_ref_code + '.pdf', options = invoice_options(current_date), configuration = config)

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		booking_activity_description = "Booking invoice printed",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		db.session.close()
	
	except Exception:
		db.session.rollback()
		db.session.close()

	return jsonify({"message": "Success.",
					"url": 'https://' + app.config['SERVER'] + '/docs/invoices/invoice-' + booking_ref_code + '.pdf'}), 200


@app.route("/email/invoice", methods = ["POST"])
def email_invoice():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		if not request.json["email"]:
			messages.append("Email is empty.")
	except KeyError as e:
		messages.append("Email is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")
		
	if messages:
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"messages": messages}), 422

	email_errors = []

	for single_email in request.json["email"]:
		valid = validateEmail(single_email)

		if valid:
			pass
		else:
			email_errors.append(single_email + " is not a valid email address")

	if email_errors:
		return jsonify({"messages": email_errors}), 422

	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == request.json["booking_id"])\
							   .filter(Booking.deletion_marker == None)\
							   .first()

	if not return_booking:
		output = []
		output.append("The selected booking does not seem to exist.")
		return jsonify({"message": output}), 412

	get_booking_details = db.session.query(Booking)\
									.join(Detail, Booking.booking_public_id == Detail.booking_id)\
									.join(Gatepass, Booking.booking_public_id == Gatepass.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_done_by,\
												 Booking.gatepass_id, Booking.currency, Booking.booking_check_out_date,\
												 Booking.currency, Booking.booking_check_in_date, Booking.booking_ref_code,\
												 Booking.payment_status, Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time,\
												 Gatepass.gatepass_public_id, Gatepass.gatepass_ref_code, Gatepass.destination,\
												 Detail.additional_note, Detail.phone_number, Detail.email_address)\
									.filter(Booking.deletion_marker == None)\
									.filter(Detail.deletion_marker == None)\
									.filter(Gatepass.deletion_marker == None)\
									.filter(Gatepass.status != get_booking_status_id("Updated"))\
									.filter(Booking.booking_public_id == request.json["booking_id"])\
									.first()
	
	return_data = {}

	bookingTotal(return_data, request.json["booking_id"])

	booking_ref_code = return_booking.booking_ref_code
	booking_done_by = get_booking_details.booking_done_by

	return_data["booking_ref"] = return_booking.booking_ref_code
	return_data["booking_check_in_date"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out_date"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_done_by"] = get_booking_details.booking_done_by
	return_data["phone_number"] = get_booking_details.phone_number
	return_data["email_address"] = get_booking_details.email_address
	return_data["buying_rate"] = get_booking_details.currency_buying_rate_at_time
	return_data["selling_rate"] = get_booking_details.currency_selling_rate_at_time
	return_data["booking_currency"] = get_booking_details.currency

	try:
		return_data["invoice_currency"] = request.json["currency_id"]

		currency = requests.get(get_currency.format(request.json["currency_id"]))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]
	
	except Exception:
		return_data["invoice_currency"] = get_booking_details.currency

		currency = requests.get(get_currency.format(get_booking_details.currency))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]

	# vat = requests.get(get_latest_vat_rate)
	# return_data["vat_rate"] = int(vat.json()["data"][0]["vat_rate"])
	return_data["vat_rate"] = return_data["_vat_rate"]

	# catering = requests.get(get_latest_cater_levy_rate)
	# return_data["catering_levy"] = int(catering.json()["data"][0]["levy_rate"])
	return_data["catering_levy"] = return_data["_catering_levy_rate"]

	if return_booking.booking_check_in_date.strftime("%d %b %Y") == return_booking.booking_check_out_date.strftime("%d %b %Y"):
		return_data["unit"] = "Day"
	else:
		return_data["unit"] = "Night"

	# get_payment = db.session.query(BookingPayment)\
	# 						.join(PaymentMethod, BookingPayment.payment_method == PaymentMethod.payment_method_public_id)\
	# 						.add_columns(PaymentMethod.payment_method_name)\
	# 						.filter(BookingPayment.deletion_marker == None)\
	# 						.filter(BookingPayment.booking_id == get_booking_details.booking_public_id)\
	# 						.first()

	# if get_payment:
	# 	return_data["payment_method"] = get_payment.payment_method_name
	# 	return_data["payment_status"] = "Paid"
	# else:
	# 	return_data["payment_method"] = "N/A"
	# 	return_data["payment_status"] = "Not Paid"

	if get_booking_details.payment_status == 1:
		return_data["payment_status"] = "Paid"
	if get_booking_details.payment_status == 2:
		return_data["payment_status"] = "Deposit"
	if get_booking_details.payment_status == 3:
		return_data["payment_status"] = "Complimentary"
	if get_booking_details.payment_status == 4:
		return_data["payment_status"] = "To Invoice"
	if not get_booking_details.payment_status:
		return_data["payment_status"] = "Not Paid"
	
	try:
		if get_booking_details.destination:
			get_destination = db.session.query(Destination)\
										.filter(Destination.gatepass_destination_public_id == get_booking_details.destination)\
										.first()
			
			return_data["destination"] = get_destination.gatepass_destination_name

		else:
			return_data["destination"] = "N/A"
	except Exception:
		return_data["destination"] = "N/A"

	asyncRequests = AsyncRequests()
	id_array = []
	
	get_notes = db.session.query(Note)\
						  .filter(Note.deletion_marker == None)\
						  .filter(Note.show_on_invoice != None)\
						  .filter(Note.booking_id == get_booking_details.booking_public_id)\
						  .all()

	if not get_notes:
		pass
	else:
		for one_note in get_notes:
			## Append booking note session IDs to id_array
			id_array.append(one_note.session_id)

	## Query Aumra service
	post_data = {"users_ids": id_array}
	return_user = asyncRequests.launchPostRequest(url = get_user_from_aumra, json = post_data)
	
	## Adding booking notes
	notes = []
	
	if not get_notes:
		pass
	else:
		for single_note in get_notes:
			return_notes = {}
			return_notes["booking_note_public_id"] = single_note.booking_notes_public_id
			return_notes["note"] = single_note.note
			
			try:
				for user in return_user["data"]:
					if user["public_id"] == single_note.session_id:
						name = user["full_name"]

			except (IndexError, KeyError):
				name = "N/A"

			except (AttributeError, UnboundLocalError):
				name = "Network Error"

			return_notes["user"] = name
			return_notes["created_at"] = single_note.created_at
			
			notes.append(return_notes)

	get_partner_booking = db.session.query(Partner)\
									.filter(Partner.booking_id == get_booking_details.booking_public_id)\
									.first()

	if get_partner_booking:
		return_data["partner_booking"] = True
		return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
	else:
		return_data["partner_booking"] = False

	get_school_booking = db.session.query(SchoolBooking)\
								   .filter(SchoolBooking.booking_id == get_booking_details.booking_public_id)\
								   .first()

	if get_school_booking:
		return_data["school_booking"] = True
		return_data["school_name"] = get_school_booking.school_details.school_name
		return_data["primary_contact"] = get_school_booking.school_details.primary_contact
	else:
		return_data["school_booking"] = False
	
	get_payments = db.session.query(Transaction)\
							 .filter(Transaction.deletion_marker == None)\
							 .filter(Transaction.booking_id == get_booking_details.booking_public_id)\
							 .all()

	transaction_data = []

	for single_payment in get_payments:
		transaction_data.append(currencyPostProcessor(get_booking_details.currency, single_payment.transaction_payment_currency, single_payment.transaction_total,\
													  single_payment.payment_currency_buying_rate_at_time, single_payment.payment_currency_selling_rate_at_time,))
	
	payment_total = sum(transaction_data)

	return_data["payment_total"] = payment_total
	return_data["payment_balance"] = return_data["total_cost"] - payment_total
	
	print_data = return_data

	get_all_booking_donations = db.session.query(Donation)\
										  .filter(Donation.deletion_marker == None)\
										  .filter(Donation.booking_id == get_booking_details.booking_public_id)\
										  .all()

	donation_data = []
	if get_all_booking_donations:
		for single_donation in get_all_booking_donations:
			return_donation_data = {}
			return_donation_data["donation_public_id"] = single_donation.donation_public_id
			return_donation_data["donation_currency"] = single_donation.donation_currency
			return_donation_data["donation_amount"] = float(single_donation.donation_amount)
			return_donation_data["donation_cause"] = single_donation.donation_cause

			currency_details = get_details_currency(single_donation.donation_currency)
			return_donation_data["donation_currency_name"] = currency_details.json()["data"][0]["currency_name"]

			cause_details = get_details_donation_cause(single_donation.donation_cause)
			return_donation_data["donation_cause_name"] = cause_details.json()["name"]
			
			donation_data.append(return_donation_data)

	invoice = render_template("invoice.html", data = print_data, booking_notes = notes, donations = donation_data)
	config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
	current_date = datetime.now()

	# pdfkit.from_string(invoice, "invoices/invoice-" + booking_ref_code + "-email.pdf", options = invoice_print_options, configuration = config)
	pdfkit.from_string(invoice, "invoices/invoice-" + booking_ref_code + "-email.pdf", options = invoice_options(current_date), configuration = config)

	# Email stuff starts here
	sender = "reservations@olpejetaconservancy.org"
	email_response = []
	
	booking_details = {}
	booking_details["booking_ref_code"] = booking_ref_code
	booking_details["client"] = booking_done_by
	booking_details["today"] = datetime.now().strftime("%B %Y")
	booking_details["copyright_year"] = datetime.now().strftime("%Y")

	sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])
	
	file_path = "invoices/invoice-" + booking_ref_code + "-email.pdf"
	with open(file_path, "rb") as file:
		invoice_data = file.read()
		file.close()

	encoded_invoice = base64.b64encode(invoice_data).decode()
	
	for recipient in request.json["email"]:
		from_email = Email(sender)
		to_email = Email(recipient)
		subject = "Booking Invoice (#" + booking_ref_code + ")"
		content = Content("text/html", render_template("invoice_email.html", data = booking_details))
		
		invoice_attach = Attachment()
		invoice_attach.content = encoded_invoice
		invoice_attach.type = "application/pdf"
		invoice_attach.filename = "invoice-" + booking_ref_code + ".pdf"
		invoice_attach.disposition = "attachment"

		mail = Mail(from_email, subject, to_email, content)
		mail.add_attachment(invoice_attach)

		try:
			response = sg.client.mail.send.post(request_body=mail.get())

			booking_activity = BookingActivity(
				booking_activity_public_id = str(uuid.uuid4()),
				booking_id = request.json["booking_id"],
				booking_activity_description = "Booking invoice email sent to " + recipient + " from " + sender + ".",
				session_id = request.json["session_id"],
				created_at = datetime.now()
			)

			db.session.add(booking_activity)

			try:
				db.session.commit()
				db.session.close()
			
			except Exception:
				db.session.rollback()
				db.session.close()

		except Exception as e:
			error_tuple = sys.exc_info()
			trace = traceback.format_exc()
			response = trace
	
	return jsonify({"message": "Email successfuly sent.", "response": str(response)}), 200


@app.route("/invoice/generate/<booking_id>", methods = ["GET"])
def generate_invoice(booking_id):
	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == booking_id)\
							   .filter(Booking.deletion_marker == None)\
							   .first()

	if not return_booking:
		output = []
		output.append("The selected booking does not seem to exist.")
		return jsonify({"message": output}), 412

	get_booking_details = db.session.query(Booking)\
									.join(Detail, Booking.booking_public_id == Detail.booking_id)\
									.join(Gatepass, Booking.booking_public_id == Gatepass.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_done_by,\
												 Booking.gatepass_id, Booking.currency, Booking.booking_check_out_date,\
												 Booking.currency, Booking.booking_check_in_date, Booking.booking_ref_code,\
												 Booking.payment_status, Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time,\
												 Gatepass.gatepass_public_id, Gatepass.gatepass_ref_code, Gatepass.destination,\
												 Detail.additional_note, Detail.phone_number, Detail.email_address)\
									.filter(Booking.deletion_marker == None)\
									.filter(Detail.deletion_marker == None)\
									.filter(Gatepass.deletion_marker == None)\
									.filter(Gatepass.status != get_booking_status_id("Updated"))\
									.filter(Booking.booking_public_id == booking_id)\
									.first()
	
	return_data = {}

	bookingTotal(return_data, booking_id)

	booking_ref_code = return_booking.booking_ref_code

	return_data["booking_ref"] = return_booking.booking_ref_code
	return_data["booking_check_in_date"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out_date"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_check_in"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_done_by"] = get_booking_details.booking_done_by
	return_data["phone_number"] = get_booking_details.phone_number
	return_data["email_address"] = get_booking_details.email_address
	return_data["buying_rate"] = get_booking_details.currency_buying_rate_at_time
	return_data["selling_rate"] = get_booking_details.currency_selling_rate_at_time
	return_data["booking_currency"] = get_booking_details.currency

	try:
		return_data["invoice_currency"] = request.json["currency_id"]

		currency = requests.get(get_currency.format(request.json["currency_id"]))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]
	
	except Exception:
		return_data["invoice_currency"] = get_booking_details.currency

		currency = requests.get(get_currency.format(get_booking_details.currency))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]

	# vat = requests.get(get_latest_vat_rate)
	# return_data["vat_rate"] = int(vat.json()["data"][0]["vat_rate"])
	return_data["vat_rate"] = return_data["_vat_rate"]

	# catering = requests.get(get_latest_cater_levy_rate)
	# return_data["catering_levy"] = int(catering.json()["data"][0]["levy_rate"])
	return_data["catering_levy"] = return_data["_catering_levy_rate"]

	if return_booking.booking_check_in_date.strftime("%d %b %Y") == return_booking.booking_check_out_date.strftime("%d %b %Y"):
		return_data["unit"] = "Day"
	else:
		return_data["unit"] = "Night"

	if get_booking_details.payment_status == 1:
		return_data["payment_status"] = "Paid"
	if get_booking_details.payment_status == 2:
		return_data["payment_status"] = "Deposit"
	if get_booking_details.payment_status == 3:
		return_data["payment_status"] = "Complimentary"
	if get_booking_details.payment_status == 4:
		return_data["payment_status"] = "To Invoice"
	if not get_booking_details.payment_status:
		return_data["payment_status"] = "Not Paid"
	
	try:
		if get_booking_details.destination:
			get_destination = db.session.query(Destination)\
										.filter(Destination.gatepass_destination_public_id == get_booking_details.destination)\
										.first()
			
			return_data["destination"] = get_destination.gatepass_destination_name

		else:
			return_data["destination"] = "N/A"
	except Exception:
		return_data["destination"] = "N/A"

	asyncRequests = AsyncRequests()
	id_array = []
	
	get_notes = db.session.query(Note)\
						  .filter(Note.deletion_marker == None)\
						  .filter(Note.show_on_invoice != None)\
						  .filter(Note.booking_id == get_booking_details.booking_public_id)\
						  .all()

	if not get_notes:
		pass
	else:
		for one_note in get_notes:
			## Append booking note session IDs to id_array
			id_array.append(one_note.session_id)

	## Query Aumra service
	post_data = {"users_ids": id_array}
	return_user = asyncRequests.launchPostRequest(url = get_user_from_aumra, json = post_data)
	
	## Adding booking notes
	notes = []
	
	if not get_notes:
		pass
	else:
		for single_note in get_notes:
			return_notes = {}
			return_notes["booking_note_public_id"] = single_note.booking_notes_public_id
			return_notes["note"] = single_note.note
			
			try:
				for user in return_user["data"]:
					if user["public_id"] == single_note.session_id:
						name = user["full_name"]

			except (IndexError, KeyError):
				name = "N/A"

			except (AttributeError, UnboundLocalError):
				name = "Network Error"

			return_notes["user"] = name
			return_notes["created_at"] = single_note.created_at
			
			notes.append(return_notes)

	get_partner_booking = db.session.query(Partner)\
									.filter(Partner.booking_id == get_booking_details.booking_public_id)\
									.first()

	if get_partner_booking:
		return_data["partner_booking"] = True
		return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
	else:
		return_data["partner_booking"] = False

	get_school_booking = db.session.query(SchoolBooking)\
								   .filter(SchoolBooking.booking_id == get_booking_details.booking_public_id)\
								   .first()

	if get_school_booking:
		return_data["school_booking"] = True
		return_data["school_name"] = get_school_booking.school_details.school_name
		return_data["primary_contact"] = get_school_booking.school_details.primary_contact
	else:
		return_data["school_booking"] = False
	
	get_payments = db.session.query(Transaction)\
							 .filter(Transaction.deletion_marker == None)\
							 .filter(Transaction.booking_id == get_booking_details.booking_public_id)\
							 .all()

	transaction_data = []

	for single_payment in get_payments:
		transaction_data.append(currencyPostProcessor(get_booking_details.currency, single_payment.transaction_payment_currency, single_payment.transaction_total,\
													  single_payment.payment_currency_buying_rate_at_time, single_payment.payment_currency_selling_rate_at_time,))
	
	payment_total = sum(transaction_data)

	return_data["payment_total"] = payment_total
	return_data["payment_balance"] = return_data["total_cost"] - payment_total
	
	print_data = return_data

	get_all_booking_donations = db.session.query(Donation)\
										  .filter(Donation.deletion_marker == None)\
										  .filter(Donation.booking_id == get_booking_details.booking_public_id)\
										  .all()

	donation_data = []
	if get_all_booking_donations:
		for single_donation in get_all_booking_donations:
			return_donation_data = {}
			return_donation_data["donation_public_id"] = single_donation.donation_public_id
			return_donation_data["donation_currency"] = single_donation.donation_currency
			return_donation_data["donation_amount"] = float(single_donation.donation_amount)
			return_donation_data["donation_cause"] = single_donation.donation_cause

			currency_details = get_details_currency(single_donation.donation_currency)
			return_donation_data["donation_currency_name"] = currency_details.json()["data"][0]["currency_name"]

			cause_details = get_details_donation_cause(single_donation.donation_cause)
			return_donation_data["donation_cause_name"] = cause_details.json()["name"]
			
			donation_data.append(return_donation_data)

	invoice = render_template("invoice.html", data = print_data, booking_notes = notes, donations = donation_data)
	config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
	current_date = datetime.now()

	# pdfkit.from_string(invoice, 'invoices/invoice-' + booking_ref_code + '.pdf', options = invoice_print_options, configuration = config)
	pdfkit.from_string(invoice, 'invoices/invoice-' + booking_ref_code + '.pdf', options = invoice_options(current_date), configuration = config)

	## Ref: https://pythonprogramming.net/flask-send-file-tutorial/

	pdf_response = make_response(send_file("/booking_service/invoices/invoice-" + booking_ref_code + ".pdf", attachment_filename = "invoice-" + booking_ref_code + ".pdf"))
	pdf_response.headers["Content-Disposition"] = "attachment; filename = invoice-" + booking_ref_code + ".pdf"

	return pdf_response
	
	# booking_invoice = open("invoices/invoice-" + booking_ref_code + ".pdf", "r")

	# response = make_response()

	# response.headers["Content-Type"] = "application/pdf"
	# response.headers["Content-Disposition"] = "inline; filename = invoice-" + booking_ref_code + ".pdf"

	# return response

	# return str(invoice_pdf)

	# return jsonify({"message": "Success.",
	# 				"url": 'https://' + app.config['SERVER'] + '/docs/invoices/invoice-' + booking_ref_code + '.pdf'}), 200


@app.route("/print/credit_note/<booking_ref>", methods = ["GET"])
def print_credit_note(booking_ref):
	# messages = []
	
	# try:
	# 	request.json["booking_id"].strip()
	# 	if not request.json["booking_id"]:
	# 		messages.append("Booking ID is empty.")
	# except KeyError as e:
	# 	messages.append("Booking ID is missing.")
	
	# try:
	# 	request.json["session_id"].strip()
	# 	if not request.json["session_id"]:
	# 		messages.append("Session ID is empty.")
	# except KeyError as e:
	# 	messages.append("Session ID is missing.")
		
	# if messages:
	# 	output = []
	# 	output.append("You appear to be missing some data. Please try again.")
	# 	return jsonify({"messages": messages}), 422

	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_ref_code == booking_ref)\
							   .filter(Booking.deletion_marker == None)\
							   .first()

	if not return_booking:
		output = []
		output.append("The selected booking does not seem to exist.")
		return jsonify({"message": output}), 412

	get_booking_details = db.session.query(Booking)\
									.join(Detail, Booking.booking_public_id == Detail.booking_id)\
									.join(Gatepass, Booking.booking_public_id == Gatepass.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_done_by,\
												 Booking.gatepass_id, Booking.currency, Booking.booking_check_out_date,\
												 Booking.currency, Booking.booking_check_in_date, Booking.booking_ref_code,\
												 Booking.payment_status, Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time,\
												 Gatepass.gatepass_public_id, Gatepass.gatepass_ref_code, Gatepass.destination,\
												 Detail.additional_note, Detail.phone_number, Detail.email_address)\
									.filter(Booking.deletion_marker == None)\
									.filter(Detail.deletion_marker == None)\
									.filter(Gatepass.deletion_marker == None)\
									.filter(Gatepass.status != get_booking_status_id("Updated"))\
									.filter(Booking.booking_public_id == return_booking.booking_public_id)\
									.first()
	
	return_data = {}

	bookingTotal(return_data, return_booking.booking_public_id)

	booking_ref_code = return_booking.booking_ref_code

	return_data["booking_ref"] = return_booking.booking_ref_code
	return_data["booking_check_in_date"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out_date"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_check_in"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_data["booking_check_out"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_data["booking_done_by"] = get_booking_details.booking_done_by
	return_data["phone_number"] = get_booking_details.phone_number
	return_data["email_address"] = get_booking_details.email_address
	return_data["buying_rate"] = get_booking_details.currency_buying_rate_at_time
	return_data["selling_rate"] = get_booking_details.currency_selling_rate_at_time
	return_data["booking_currency"] = get_booking_details.currency

	try:
		return_data["invoice_currency"] = request.json["currency_id"]

		currency = requests.get(get_currency.format(request.json["currency_id"]))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]
	
	except Exception:
		return_data["invoice_currency"] = get_booking_details.currency

		currency = requests.get(get_currency.format(get_booking_details.currency))
		return_data["currency"] = currency.json()["data"][0]["currency_name"]

	# vat = requests.get(get_latest_vat_rate)
	# return_data["vat_rate"] = int(vat.json()["data"][0]["vat_rate"])
	return_data["vat_rate"] = return_data["_vat_rate"]

	# catering = requests.get(get_latest_cater_levy_rate)
	# return_data["catering_levy"] = int(catering.json()["data"][0]["levy_rate"])
	return_data["catering_levy"] = return_data["_catering_levy_rate"]

	if return_booking.booking_check_in_date.strftime("%d %b %Y") == return_booking.booking_check_out_date.strftime("%d %b %Y"):
		return_data["unit"] = "Day"
	else:
		return_data["unit"] = "Night"

	# get_payment = db.session.query(BookingPayment)\
	# 						.join(PaymentMethod, BookingPayment.payment_method == PaymentMethod.payment_method_public_id)\
	# 						.add_columns(PaymentMethod.payment_method_name)\
	# 						.filter(BookingPayment.deletion_marker == None)\
	# 						.filter(BookingPayment.booking_id == get_booking_details.booking_public_id)\
	# 						.first()

	# if get_payment:
	# 	return_data["payment_method"] = get_payment.payment_method_name
	# 	return_data["payment_status"] = "Paid"
	# else:
	# 	return_data["payment_method"] = "N/A"
	# 	return_data["payment_status"] = "Not Paid"

	if get_booking_details.payment_status == 1:
		return_data["payment_status"] = "Paid"
	if get_booking_details.payment_status == 2:
		return_data["payment_status"] = "Deposit"
	if get_booking_details.payment_status == 3:
		return_data["payment_status"] = "Complimentary"
	if get_booking_details.payment_status == 4:
		return_data["payment_status"] = "To Invoice"
	if not get_booking_details.payment_status:
		return_data["payment_status"] = "Not Paid"
	
	try:
		if get_booking_details.destination:
			get_destination = db.session.query(Destination)\
										.filter(Destination.gatepass_destination_public_id == get_booking_details.destination)\
										.first()
			
			return_data["destination"] = get_destination.gatepass_destination_name

		else:
			return_data["destination"] = "N/A"
	except Exception:
		return_data["destination"] = "N/A"

	get_partner_booking = db.session.query(Partner)\
									.filter(Partner.booking_id == get_booking_details.booking_public_id)\
									.first()

	if get_partner_booking:
		return_data["partner_booking"] = True
		return_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
	else:
		return_data["partner_booking"] = False

	get_school_booking = db.session.query(SchoolBooking)\
								   .filter(SchoolBooking.booking_id == get_booking_details.booking_public_id)\
								   .first()

	if get_school_booking:
		return_data["school_booking"] = True
		return_data["school_name"] = get_school_booking.school_details.school_name
		return_data["primary_contact"] = get_school_booking.school_details.primary_contact
	else:
		return_data["school_booking"] = False

	get_payments = db.session.query(Transaction)\
							 .filter(Transaction.deletion_marker == None)\
							 .filter(Transaction.booking_id == get_booking_details.booking_public_id)\
							 .all()

	transaction_data = []

	for single_payment in get_payments:
		transaction_data.append(currencyPostProcessor(get_booking_details.currency, single_payment.transaction_payment_currency, single_payment.transaction_total,\
													  single_payment.payment_currency_buying_rate_at_time, single_payment.payment_currency_selling_rate_at_time,))
	
	payment_total = sum(transaction_data)

	return_data["payment_total"] = payment_total
	return_data["payment_balance"] = return_data["total_cost"] - payment_total
	
	print_data = return_data

	invoice = render_template("credit_note.html", data = print_data)
	config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
	current_date = datetime.now()

	# pdfkit.from_string(invoice, 'invoices/credit-note-' + booking_ref_code + '.pdf', options = invoice_print_options, configuration = config)
	pdfkit.from_string(invoice, 'invoices/credit-note-' + booking_ref_code + '.pdf', options = invoice_options(current_date), configuration = config)

	pdf_response = make_response(send_file("/booking_service/invoices/credit-note-" + booking_ref_code + ".pdf", attachment_filename = "credit-note-" + booking_ref_code + ".pdf"))
	pdf_response.headers["Content-Disposition"] = "attachment; filename = credit-note-" + booking_ref_code + ".pdf"

	return pdf_response

	# booking_activity = BookingActivity(
	# 	booking_activity_public_id = str(uuid.uuid4()),
	# 	booking_id = request.json["booking_id"],
	# 	booking_activity_description = "Booking invoice printed",
	# 	session_id = request.json["session_id"],
	# 	created_at = datetime.now()
	# )

	# db.session.add(booking_activity)

	# try:
	# 	db.session.commit()
	# 	db.session.close()
	
	# except Exception:
	# 	db.session.rollback()
	# 	db.session.close()

	# return jsonify({"message": "Success.",
	# 				"url": 'https://' + app.config['SERVER'] + '/docs/invoices/credit-note-' + booking_ref_code + '.pdf'}), 200


@app.route("/print/booking_credit_note", methods = ["POST"])
def print_booking_credit_note():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")

	try:
		if not request.json["credit_note_id"]:
			messages.append("Credit note ID is empty.")
	except KeyError as e:
		messages.append("Credit note ID is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")
		
	if messages:
		return jsonify({"messages": messages}), 422

	get_credit_note = db.session.query(CreditNote)\
								.filter(CreditNote.deletion_marker == None)\
								.filter(CreditNote.credit_note_public_id == request.json["credit_note_id"])\
								.first()

	if not get_credit_note:
		message = []
		message.append("The selected credit note does not appear to exist.")
		return jsonify({"message": message}), 412

	return_booking = db.session.query(Booking)\
							   .filter(Booking.booking_public_id == request.json["booking_id"])\
							   .filter(Booking.deletion_marker == None)\
							   .first()
	
	get_booking_details = db.session.query(Booking)\
									.join(Detail, Booking.booking_public_id == Detail.booking_id)\
									.join(Gatepass, Booking.booking_public_id == Gatepass.booking_id)\
									.add_columns(Booking.booking_public_id, Booking.booking_type, Booking.booking_done_by,\
												 Booking.gatepass_id, Booking.currency, Booking.booking_check_out_date,\
												 Booking.currency, Booking.booking_check_in_date, Booking.booking_ref_code,\
												 Booking.payment_status, Booking.currency_buying_rate_at_time, Booking.currency_selling_rate_at_time,\
												 Gatepass.gatepass_public_id, Gatepass.gatepass_ref_code, Gatepass.destination,\
												 Detail.additional_note, Detail.phone_number, Detail.email_address)\
									.filter(Booking.deletion_marker == None)\
									.filter(Detail.deletion_marker == None)\
									.filter(Gatepass.deletion_marker == None)\
									.filter(Gatepass.status != get_booking_status_id("Updated"))\
									.filter(Booking.booking_public_id == request.json["booking_id"])\
									.first()

	return_credit_note_data = {}
	creditNoteTotal(request.json["credit_note_id"], request.json["booking_id"], return_credit_note_data)

	credit_note_ref = get_credit_note.credit_note_ref

	return_credit_note_data["booking_ref"] = return_booking.booking_ref_code
	return_credit_note_data["booking_check_in_date"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_credit_note_data["booking_check_out_date"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_credit_note_data["booking_check_in"] = return_booking.booking_check_in_date.strftime("%d %b %Y")
	return_credit_note_data["booking_check_out"] = return_booking.booking_check_out_date.strftime("%d %b %Y")
	return_credit_note_data["booking_done_by"] = get_booking_details.booking_done_by
	return_credit_note_data["phone_number"] = get_booking_details.phone_number
	return_credit_note_data["email_address"] = get_booking_details.email_address
	return_credit_note_data["buying_rate"] = get_booking_details.currency_buying_rate_at_time
	return_credit_note_data["selling_rate"] = get_booking_details.currency_selling_rate_at_time
	return_credit_note_data["booking_currency"] = get_booking_details.currency
	return_credit_note_data["invoice_currency"] = get_booking_details.currency
	currency = requests.get(get_currency.format(get_booking_details.currency))
	return_credit_note_data["currency"] = currency.json()["data"][0]["currency_name"]

	try:
		if get_booking_details.destination:
			get_destination = db.session.query(Destination)\
										.filter(Destination.gatepass_destination_public_id == get_booking_details.destination)\
										.first()
			
			return_credit_note_data["destination"] = get_destination.gatepass_destination_name

		else:
			return_credit_note_data["destination"] = "N/A"
	except Exception:
		return_credit_note_data["destination"] = "N/A"

	get_partner_booking = db.session.query(Partner)\
									.filter(Partner.booking_id == get_booking_details.booking_public_id)\
									.first()

	if get_partner_booking:
		return_credit_note_data["partner_booking"] = True
		return_credit_note_data["partner_booking_ref"] = get_partner_booking.partner_booking_ref
	else:
		return_credit_note_data["partner_booking"] = False

	get_school_booking = db.session.query(SchoolBooking)\
								   .filter(SchoolBooking.booking_id == get_booking_details.booking_public_id)\
								   .first()

	if get_school_booking:
		return_credit_note_data["school_booking"] = True
		return_credit_note_data["school_name"] = get_school_booking.school_details.school_name
		return_credit_note_data["primary_contact"] = get_school_booking.school_details.primary_contact
	else:
		return_credit_note_data["school_booking"] = False
	
	print_data = return_credit_note_data

	credit_note = render_template("credit_note.html", data = print_data)
	config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
	current_date = datetime.now()

	# pdfkit.from_string(credit_note, 'invoices/credit-note-' + credit_note_ref + '.pdf', options = invoice_print_options, configuration = config)
	pdfkit.from_string(credit_note, 'invoices/credit-note-' + credit_note_ref + '.pdf', options = invoice_options(current_date), configuration = config)

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		booking_activity_description = "Booking credit note ref. #" + credit_note_ref + " printed",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		db.session.close()
	
	except Exception:
		db.session.rollback()
		db.session.close()

	return jsonify({"message": "Success.",
					"url": 'https://' + app.config['SERVER'] + '/docs/invoices/credit-note-' + credit_note_ref + '.pdf'}), 200