from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta, date

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from database.gatepass import Gatepass
from database.gatepass_guests import GatepassGuest
from database.gatepass_details import GatepassDetail
from routes.seed_functions import BookingTypeSeed, BookingStatusSeed, MandatoryPaymentsSeed, BookingGuestTypesSeed
from database.booking_types import BookingType
from database.bookings import Booking
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_details import Detail
from database.booking_guest_types import GuestType
from database.mandatory_payments import Mandatory
from variables import *


def close(self):
	self.session.close()


####################
#### Gatepasses ####
####################
@app.route("/gatepass/new", methods = ["POST"])
def add_new_gatepass():
	today = datetime.now().strftime("%Y-%m-%d")

	if not str(request.json["ea_adult_citizen"]).strip()\
	or not str(request.json["ea_adult_resident"]).strip()\
	or not str(request.json["non_resident_adult"]).strip()\
	or not str(request.json["ea_child_citizen"]).strip()\
	or not str(request.json["ea_child_resident"]).strip()\
	or not str(request.json["non_resident_child"]).strip()\
	or not str(request.json["infant"]).strip()\
	or not str(request.json["citizen_student"]).strip()\
	or not str(request.json["ea_resident_student"]).strip()\
	or not str(request.json["non_resident_student"]).strip()\
	or not str(request.json["currency"]).strip()\
	or not str(request.json["start_date"]).strip()\
	or not str(request.json["end_date"]).strip()\
	or not request.json["session_id"].strip():
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": output}), 422

	gatepass_id = str(uuid.uuid4())
	booking_id = str(uuid.uuid4())

	try:
		first = request.json["first_name"]
	except KeyError as first_error:
		first = None
	
	try:
		last = request.json["last_name"]
	except KeyError as last_error:
		last = None

	try:
		email = request.json["email"]
	except KeyError as email_error:
		email = None

	try:
		phone = request.json["phone"]
	except KeyError as phone_error:
		phone = None

	try:
		partner = request.json["partner"]
	except KeyError as partner_error:
		partner = None

	stripped_start_year,stripped_start_month,stripped_start_day = request.json["start_date"].split("-")
	stripped_end_year,stripped_end_month,stripped_end_day = request.json["end_date"].split("-")

	int_start_year = int(stripped_start_year)
	int_start_month = int(stripped_start_month)
	int_start_day = int(stripped_start_day)
	int_end_year = int(stripped_end_year)
	int_end_month = int(stripped_end_month)
	int_end_day = int(stripped_end_day)

	start_date = date(int_start_year,int_start_month,int_start_day)
	end_date = date(int_end_year,int_end_month,int_end_day)
	
	if int(abs(end_date - start_date).days) == 0:
		date_diff = 1
	else:
		date_diff = int(abs(end_date - start_date).days)

	exchange_rate_data = requests.get(get_latest_exchange_rate)

	gatepass_fee = db.session.query(Mandatory)\
							 .filter(Mandatory.deletion_marker == None)\
							 .order_by(Mandatory.payment_id.desc())\
							 .first()
	
	## Non Resident Adult
	if request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_adult_currency == "USD":
		payment_non_resident_adult_amount = float(gatepass_fee.payment_non_resident_adult_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_adult_currency == "USD":
		payment_non_resident_adult_amount = \
					round(float(gatepass_fee.payment_non_resident_adult_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_adult_currency == "KES":
		payment_non_resident_adult_amount = \
					round(float(gatepass_fee.payment_non_resident_adult_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_adult_currency == "KES":
		payment_non_resident_adult_amount = float(gatepass_fee.payment_non_resident_adult_amount)

	## Non Resident Child
	if request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_child_currency == "USD":
		payment_non_resident_child_amount = float(gatepass_fee.payment_non_resident_child_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_child_currency == "USD":
		payment_non_resident_child_amount = \
					round(float(gatepass_fee.payment_non_resident_child_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_child_currency == "KES":
		payment_non_resident_child_amount = \
					round(float(gatepass_fee.payment_non_resident_child_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_child_currency == "KES":
		payment_non_resident_child_amount = float(gatepass_fee.payment_non_resident_child_amount)
	
	## Non Resident Student
	if request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_student_currency == "USD":
		payment_non_resident_student_amount = float(gatepass_fee.payment_non_resident_student_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_student_currency == "USD":
		payment_non_resident_student_amount = \
					round(float(gatepass_fee.payment_non_resident_student_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_student_currency == "KES":
		payment_non_resident_student_amount = \
					round(float(gatepass_fee.payment_non_resident_student_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_student_currency == "KES":
		payment_non_resident_student_amount = float(gatepass_fee.payment_non_resident_student_amount)

	## EA Resident Adult
	if request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_adult_currency == "USD":
		payment_ea_resident_adult_amount = float(gatepass_fee.payment_ea_resident_adult_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_adult_currency == "USD":
		payment_ea_resident_adult_amount = \
					round(float(gatepass_fee.payment_ea_resident_adult_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_adult_currency == "KES":
		payment_ea_resident_adult_amount = \
					round(float(gatepass_fee.payment_ea_resident_adult_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_adult_currency == "KES":
		payment_ea_resident_adult_amount = float(gatepass_fee.payment_ea_resident_adult_amount)

	## EA Resident Child
	if request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_child_currency == "USD":
		payment_ea_resident_child_amount = float(gatepass_fee.payment_ea_resident_child_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_child_currency == "USD":
		payment_ea_resident_child_amount = \
					round(float(gatepass_fee.payment_ea_resident_child_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_child_currency == "KES":
		payment_ea_resident_child_amount = \
					round(float(gatepass_fee.payment_ea_resident_child_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_child_currency == "KES":
		payment_ea_resident_child_amount = float(gatepass_fee.payment_ea_resident_child_amount)
	
	## EA Resident Student
	if request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_student_currency == "USD":
		payment_ea_resident_student_amount = float(gatepass_fee.payment_ea_resident_student_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_student_currency == "USD":
		payment_ea_resident_student_amount = \
					round(float(gatepass_fee.payment_ea_resident_student_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_student_currency == "KES":
		payment_ea_resident_student_amount = \
					round(float(gatepass_fee.payment_ea_resident_student_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_student_currency == "KES":
		payment_ea_resident_student_amount = float(gatepass_fee.payment_ea_resident_student_amount)

	## Citizen Adult
	if request.json["currency"] == "USD" and gatepass_fee.payment_citizen_adult_currency == "USD":
		payment_citizen_adult_amount = float(gatepass_fee.payment_citizen_adult_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_adult_currency == "USD":
		payment_citizen_adult_amount = \
					round(float(gatepass_fee.payment_citizen_adult_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_citizen_adult_currency == "KES":
		payment_citizen_adult_amount = \
					round(float(gatepass_fee.payment_citizen_adult_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_adult_currency == "KES":
		payment_citizen_adult_amount = float(gatepass_fee.payment_citizen_adult_amount)

	## Citizen Child
	if request.json["currency"] == "USD" and gatepass_fee.payment_citizen_child_currency == "USD":
		payment_citizen_child_amount = float(gatepass_fee.payment_citizen_child_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_child_currency == "USD":
		payment_citizen_child_amount = \
					round(float(gatepass_fee.payment_citizen_child_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_citizen_child_currency == "KES":
		payment_citizen_child_amount = \
					round(float(gatepass_fee.payment_citizen_child_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_child_currency == "KES":
		payment_citizen_child_amount = float(gatepass_fee.payment_citizen_child_amount)
	
	## Citizen Student
	if request.json["currency"] == "USD" and gatepass_fee.payment_citizen_student_currency == "USD":
		payment_citizen_student_amount = float(gatepass_fee.payment_citizen_student_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_student_currency == "USD":
		payment_citizen_student_amount = \
					round(float(gatepass_fee.payment_citizen_student_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_citizen_student_currency == "KES":
		payment_citizen_student_amount = \
					round(float(gatepass_fee.payment_citizen_student_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_student_currency == "KES":
		payment_citizen_student_amount = float(gatepass_fee.payment_citizen_student_amount)
	
	gatepass = Gatepass(
		gatepass_public_id = gatepass_id,
		gatepass_date = today,
		destination = request.json["destination"],
		gatepass_done_by = first + " " + last,
		gatepass_phone_number = request.json["phone"],
		gatepass_ref_code = str(uuid.uuid4())[:10],
		booking_id = booking_id,
		partner_id = partner,
		start_date = request.json["start_date"],
		end_date = request.json["end_date"],
		gatepass_currency = request.json["currency"],
		session_id = request.json["session_id"]
	)

	db.session.add(gatepass)

	booking_guest1 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Adult Citizen"),
		gatepass_guest_count = request.json["ea_adult_citizen"],
		gatepass_cost_per_pp = payment_citizen_adult_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest1)

	booking_guest2 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Adult Resident"),
		gatepass_guest_count = request.json["ea_adult_resident"],
		gatepass_cost_per_pp = payment_ea_resident_adult_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest2)

	booking_guest3 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Non Resident Adult"),
		gatepass_guest_count = request.json["non_resident_adult"],
		gatepass_cost_per_pp = payment_non_resident_adult_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest3)

	booking_guest4 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Child Citizen"),
		gatepass_guest_count = request.json["ea_child_citizen"],
		gatepass_cost_per_pp = payment_citizen_child_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest4)

	booking_guest5 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Child Resident"),
		gatepass_guest_count = request.json["ea_child_resident"],
		gatepass_cost_per_pp = payment_ea_resident_child_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest5)

	booking_guest6 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Non Resident Child"),
		gatepass_guest_count = request.json["non_resident_child"],
		gatepass_cost_per_pp = payment_non_resident_child_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest6)

	booking_guest7 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Infant"),
		gatepass_guest_count = request.json["infant"],
		gatepass_cost_per_pp = 0,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest7)

	booking_guest8 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Citizen Student"),
		gatepass_guest_count = request.json["citizen_student"],
		gatepass_cost_per_pp = payment_citizen_student_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest8)

	booking_guest9 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Resident Student"),
		gatepass_guest_count = request.json["ea_resident_student"],
		gatepass_cost_per_pp = payment_ea_resident_student_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest9)

	booking_guest10 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Non Resident Student"),
		gatepass_guest_count = request.json["non_resident_student"],
		gatepass_cost_per_pp = payment_non_resident_student_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest10)

	gatepass_detail = GatepassDetail(
		gatepass_details_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		first_name = first,
		last_name = last,
		email_address = email,
		phone_number = phone
	)

	db.session.add(gatepass_detail)

	booking = Booking(
		booking_public_id = booking_id,
		booking_done_by = first + " " + last,
		booking_ref_code = str(uuid.uuid4())[:10],
		gatepass_id = gatepass_id,
		status = get_booking_status_id("Confirmed")
	)

	guest1 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Adult Citizen"),
		guest_count = request.json["ea_adult_citizen"]
	)

	db.session.add(guest1)

	guest2 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Adult Resident"),
		guest_count = request.json["ea_adult_resident"]
	)

	db.session.add(guest2)

	guest3 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Non Resident Adult"),
		guest_count = request.json["non_resident_adult"]
	)

	db.session.add(guest3)

	guest4 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Child Citizen"),
		guest_count = request.json["ea_child_citizen"]
	)

	db.session.add(guest4)

	guest5 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Child Resident"),
		guest_count = request.json["ea_child_resident"]
	)

	db.session.add(guest5)

	guest6 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Non Resident Child"),
		guest_count = request.json["non_resident_child"]
	)

	db.session.add(guest6)

	guest7 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Infant"),
		guest_count = request.json["infant"]
	)

	db.session.add(guest7)

	guest8 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Citizen Student"),
		guest_count = request.json["citizen_student"]
	)

	db.session.add(guest8)

	guest9 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("EA Resident Student"),
		guest_count = request.json["ea_resident_student"]
	)

	db.session.add(guest9)

	guest10 = BookingGuest(
		booking_guest_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		guest_type = get_booking_guest_type_id("Non Resident Student"),
		guest_count = request.json["non_resident_student"]
	)

	db.session.add(guest10)

	booking_detail = Detail(
		booking_details_public_id = str(uuid.uuid4()),
		booking_id = booking_id,
		first_name = first,
		last_name = last,
		email_address = email,
		phone_number = phone
	)

	db.session.add(booking_detail)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The gate pass has been issued. Proceed to payment.")

		return_data = {}
		return_data["gatepass_public_id"] = gatepass_id
		return_data["phone"] = phone

		return jsonify({"message": output, "data": return_data}), 201
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("It's us, not you. There was an error while issuing the gate pass. :-( Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/gatepass/view")
def view_all_gatepasses():
	return_gatepass = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail, Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.add_columns(Gatepass.gatepass_public_id, Gatepass.destination, Gatepass.gatepass_date,\
											 Gatepass.gatepass_done_by, Gatepass.gatepass_phone_number,\
											 Gatepass.gatepass_ref_code, Gatepass.session_id, Gatepass.gatepass_payment_status,\
											 Gatepass.start_date, Gatepass.end_date,\
											 GatepassDetail.first_name, GatepassDetail.last_name, GatepassDetail.email_address,\
											 GatepassDetail.phone_number)\
								.filter(Gatepass.deletion_marker==None)\
								.filter(Gatepass.member_visit == None)\
								.filter(Gatepass.booking == None)\
								.order_by(Gatepass.gatepass_id.desc())\
								.all()

	if not return_gatepass:
		output = []
		output.append("There are currently no gatepasses in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []

		for single in return_gatepass:
			return_data = {}
			return_data["gatepass_public_id"] = single.gatepass_public_id
			return_data["destination"] = single.destination
			return_data["gatepass_date"] = single.gatepass_date
			return_data["gatepass_done_by"] = single.gatepass_done_by
			return_data["gatepass_phone_number"] = single.gatepass_phone_number
			return_data["gatepass_ref_code"] = single.gatepass_ref_code
			return_data["first_name"] = single.first_name
			return_data["last_name"] = single.last_name
			return_data["start_date"] = single.start_date
			return_data["end_date"] = single.end_date

			if single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Paid"
			elif not single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Not Paid"

			guest_array = []
			
			get_all_guests = db.session.query(GatepassGuest)\
									   .join(GuestType, GatepassGuest.gatepass_guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name, GuestType.booking_guest_type_public_id,\
									   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_guest_type)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.gatepass_id == single.gatepass_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Citizen Student":
					citizen_student = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Resident Student":
					ea_resident_student = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Student":
					non_resident_student = each_guest.gatepass_guest_count

			try:
				guest_data = {}
				guest_data["ea_adult_citizen"] = ea_adult_citizen
				guest_data["ea_adult_resident"] = ea_adult_resident
				guest_data["non_resident_adult"] = non_resident_adult
				guest_data["ea_child_citizen"] = ea_child_citizen
				guest_data["ea_child_resident"] = ea_child_resident
				guest_data["non_resident_child"] = non_resident_child
				guest_data["infant"] = infant
				guest_data["citizen_student"] = citizen_student
				guest_data["ea_resident_student"] = ea_resident_student
				guest_data["non_resident_student"] = non_resident_student

				guest_array.append(guest_data)

				return_data["guests"] = guest_array

				return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
									 		+ ea_child_resident + non_resident_child + infant + citizen_student\
											+ ea_resident_student + non_resident_student
			except UnboundLocalError as guest_data_unavailable:
				pass

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/gatepass/view/unpaid")
def view_all_unpaid_gatepasses():
	return_gatepass = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail, Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.add_columns(Gatepass.gatepass_public_id, Gatepass.destination, Gatepass.gatepass_date,\
											 Gatepass.gatepass_done_by, Gatepass.gatepass_phone_number,\
											 Gatepass.gatepass_ref_code, Gatepass.session_id, Gatepass.gatepass_payment_status,\
											 Gatepass.start_date, Gatepass.end_date,\
											 GatepassDetail.first_name, GatepassDetail.last_name, GatepassDetail.email_address,\
											 GatepassDetail.phone_number)\
								.filter(Gatepass.deletion_marker==None)\
								.filter(Gatepass.member_visit == None)\
								.filter(Gatepass.gatepass_payment_status == None)\
								.filter(Gatepass.booking == None)\
								.all()

	if not return_gatepass:
		output = []
		output.append("There are currently no unpaid gatepasses in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []

		for single in return_gatepass:
			return_data = {}
			return_data["gatepass_public_id"] = single.gatepass_public_id
			return_data["destination"] = single.destination
			return_data["gatepass_date"] = single.gatepass_date
			return_data["gatepass_done_by"] = single.gatepass_done_by
			return_data["gatepass_phone_number"] = single.gatepass_phone_number
			return_data["gatepass_ref_code"] = single.gatepass_ref_code
			return_data["first_name"] = single.first_name
			return_data["last_name"] = single.last_name
			return_data["start_date"] = single.start_date
			return_data["end_date"] = single.end_date

			if single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Paid"
			elif not single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Not Paid"

			guest_array = []
			
			get_all_guests = db.session.query(GatepassGuest)\
									   .join(GuestType, GatepassGuest.gatepass_guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name, GuestType.booking_guest_type_public_id,\
									   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_guest_type)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.gatepass_id == single.gatepass_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Citizen Student":
					citizen_student = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Resident Student":
					ea_resident_student = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Student":
					non_resident_student = each_guest.gatepass_guest_count

			try:
				guest_data = {}
				guest_data["ea_adult_citizen"] = ea_adult_citizen
				guest_data["ea_adult_resident"] = ea_adult_resident
				guest_data["non_resident_adult"] = non_resident_adult
				guest_data["ea_child_citizen"] = ea_child_citizen
				guest_data["ea_child_resident"] = ea_child_resident
				guest_data["non_resident_child"] = non_resident_child
				guest_data["infant"] = infant
				guest_data["citizen_student"] = citizen_student
				guest_data["ea_resident_student"] = ea_resident_student
				guest_data["non_resident_student"] = non_resident_student

				guest_array.append(guest_data)

				return_data["guests"] = guest_array

				return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
									 		+ ea_child_resident + non_resident_child + infant + citizen_student\
											+ ea_resident_student + non_resident_student
			except UnboundLocalError as guest_data_unavailable:
				pass

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/gatepass/view/paid")
def view_all_paid_gatepasses():
	return_gatepass = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail, Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.add_columns(Gatepass.gatepass_public_id, Gatepass.destination, Gatepass.gatepass_date,\
											 Gatepass.gatepass_done_by, Gatepass.gatepass_phone_number,\
											 Gatepass.gatepass_ref_code, Gatepass.session_id, Gatepass.gatepass_payment_status,\
											 Gatepass.start_date, Gatepass.end_date,\
											 GatepassDetail.first_name, GatepassDetail.last_name, GatepassDetail.email_address,\
											 GatepassDetail.phone_number)\
								.filter(Gatepass.deletion_marker==None)\
								.filter(Gatepass.gatepass_payment_status == 1)\
								.filter(Gatepass.booking == None)\
								.all()

	if not return_gatepass:
		output = []
		output.append("There are currently no unpaid gatepasses in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []

		for single in return_gatepass:
			return_data = {}
			return_data["gatepass_public_id"] = single.gatepass_public_id
			return_data["destination"] = single.destination
			return_data["gatepass_date"] = single.gatepass_date
			return_data["gatepass_done_by"] = single.gatepass_done_by
			return_data["gatepass_phone_number"] = single.gatepass_phone_number
			return_data["gatepass_ref_code"] = single.gatepass_ref_code
			return_data["first_name"] = single.first_name
			return_data["last_name"] = single.last_name
			return_data["start_date"] = single.start_date
			return_data["end_date"] = single.end_date

			if single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Paid"
			elif not single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Not Paid"

			guest_array = []
			
			get_all_guests = db.session.query(GatepassGuest)\
									   .join(GuestType, GatepassGuest.gatepass_guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name, GuestType.booking_guest_type_public_id,\
									   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_guest_type)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.gatepass_id == single.gatepass_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Citizen Student":
					citizen_student = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Resident Student":
					ea_resident_student = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Student":
					non_resident_student = each_guest.gatepass_guest_count

			try:
				guest_data = {}
				guest_data["ea_adult_citizen"] = ea_adult_citizen
				guest_data["ea_adult_resident"] = ea_adult_resident
				guest_data["non_resident_adult"] = non_resident_adult
				guest_data["ea_child_citizen"] = ea_child_citizen
				guest_data["ea_child_resident"] = ea_child_resident
				guest_data["non_resident_child"] = non_resident_child
				guest_data["infant"] = infant
				guest_data["citizen_student"] = citizen_student
				guest_data["ea_resident_student"] = ea_resident_student
				guest_data["non_resident_student"] = non_resident_student

				guest_array.append(guest_data)

				return_data["guests"] = guest_array

				return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
									 		+ ea_child_resident + non_resident_child + infant + citizen_student\
											+ ea_resident_student + non_resident_student
			except UnboundLocalError as guest_data_unavailable:
				pass

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/gatepass/view/<gatepass_id>")
def view_single_gatepass(gatepass_id):
	return_gatepass = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail, Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.add_columns(Gatepass.gatepass_public_id, Gatepass.destination, Gatepass.gatepass_date,\
											 Gatepass.gatepass_done_by, Gatepass.gatepass_phone_number,\
											 Gatepass.gatepass_ref_code, Gatepass.session_id, Gatepass.gatepass_payment_status,\
											 Gatepass.start_date, Gatepass.end_date, Gatepass.gatepass_currency,\
											 GatepassDetail.first_name, GatepassDetail.last_name, GatepassDetail.email_address,\
											 GatepassDetail.phone_number)\
								.filter(Gatepass.deletion_marker==None)\
								.filter(Gatepass.gatepass_public_id==gatepass_id)\
								.all()

	if not return_gatepass:
		output = []
		output.append("That gatepass does not appear to exist in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []

		for single in return_gatepass:
			return_data = {}
			return_data["gatepass_public_id"] = single.gatepass_public_id
			return_data["destination"] = single.destination
			return_data["gatepass_date"] = single.gatepass_date
			return_data["gatepass_done_by"] = single.gatepass_done_by
			return_data["gatepass_phone_number"] = single.gatepass_phone_number
			return_data["gatepass_ref_code"] = single.gatepass_ref_code
			return_data["first_name"] = single.first_name
			return_data["last_name"] = single.last_name
			return_data["start_date"] = single.start_date
			return_data["end_date"] = single.end_date
			return_data["currency"] = single.gatepass_currency

			if single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Paid"
			elif not single.gatepass_payment_status:
				return_data["gatepass_payment_status"] = "Not Paid"

			guest_array = []
			gatepass_cost_array = []
			
			get_all_guests = db.session.query(GatepassGuest)\
									   .join(GuestType, GatepassGuest.gatepass_guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name, GuestType.booking_guest_type_public_id,\
									   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_guest_type,\
													   GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													   GatepassGuest.gatepass_no_of_nights, GatepassGuest.gatepass_currency)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.gatepass_id == single.gatepass_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.gatepass_guest_count
					gatepass_ea_adult_citizen = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  		  float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_ea_adult_citizen)

				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.gatepass_guest_count
					gatepass_ea_adult_resident = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  	 	   float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_ea_adult_resident)

				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.gatepass_guest_count
					gatepass_non_resident_adult = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  			float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_non_resident_adult)

				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.gatepass_guest_count
					gatepass_ea_child_citizen = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_ea_child_citizen)

				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.gatepass_guest_count
					gatepass_ea_child_resident = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  		   float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_ea_child_resident)

				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.gatepass_guest_count
					gatepass_non_resident_child = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  			float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_non_resident_child)

				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.gatepass_guest_count
					gatepass_infant = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_infant)

				if each_guest.booking_guest_type_name == "Citizen Student":
					citizen_student = each_guest.gatepass_guest_count
					gatepass_citizen_student = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  		 float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_citizen_student)

				if each_guest.booking_guest_type_name == "EA Resident Student":
					ea_resident_student = each_guest.gatepass_guest_count
					gatepass_ea_resident_student = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  			 float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_ea_resident_student)

				if each_guest.booking_guest_type_name == "Non Resident Student":
					non_resident_student = each_guest.gatepass_guest_count
					gatepass_non_resident_student = round(float(each_guest.gatepass_guest_count) * ((100 - float(each_guest.gatepass_discount_rate))/100) * \
											  			  float(each_guest.gatepass_cost_per_pp) * float(each_guest.gatepass_no_of_nights), 2)
					gatepass_cost_array.append(gatepass_non_resident_student)

			try:
				guest_data = {}
				guest_data["ea_adult_citizen"] = ea_adult_citizen
				guest_data["ea_adult_resident"] = ea_adult_resident
				guest_data["non_resident_adult"] = non_resident_adult
				guest_data["ea_child_citizen"] = ea_child_citizen
				guest_data["ea_child_resident"] = ea_child_resident
				guest_data["non_resident_child"] = non_resident_child
				guest_data["infant"] = infant
				guest_data["citizen_student"] = citizen_student
				guest_data["ea_resident_student"] = ea_resident_student
				guest_data["non_resident_student"] = non_resident_student

				guest_array.append(guest_data)

				return_data["guests"] = guest_array

				return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
									 		+ ea_child_resident + non_resident_child + infant + citizen_student\
											+ ea_resident_student + non_resident_student

				return_data["total_cost"] = sum(gatepass_cost_array)
			except UnboundLocalError as guest_data_unavailable:
				pass

			output.append(return_data)

		return jsonify({"data": output}), 200


###########################
#### Member Gatepasses ####
###########################
@app.route("/gatepass/members/new", methods = ["POST"])
def add_new_member_gatepass():
	if not str(request.json["member"]).strip()\
	or not str(request.json["destination"]).strip()\
	or not str(request.json["currency"]).strip()\
	or not str(request.json["start_date"]).strip()\
	or not str(request.json["end_date"]).strip()\
	or not request.json["session_id"].strip():
		output = []
		output.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": output}), 422

	gatepass_id = str(uuid.uuid4())
	booking_id = str(uuid.uuid4())

	today = datetime.now().strftime("%Y-%m-%d")

	member_details = requests.get(get_member_details.format(request.json["member"]))
	
	member = request.json["member"]
	first,last = member_details.json()["full_name"].split(" ")
	email = member_details.json()["email"]
	phone = member_details.json()["tel_number"]

	try:
		ea_adult_citizen = request.json["ea_adult_citizen"]
	except KeyError as ea_adult_citizen_error:
		ea_adult_citizen = 0
	try:
		ea_adult_resident = request.json["ea_adult_resident"]
	except KeyError as ea_adult_resident_error:
		ea_adult_resident = 0
	try:
		non_resident_adult = request.json["non_resident_adult"]
	except KeyError as non_resident_adult_error:
		non_resident_adult = 0
	try:
		ea_child_citizen = request.json["ea_child_citizen"]
	except KeyError as ea_child_citizen_error:
		ea_child_citizen = 0
	try:
		ea_child_resident = request.json["ea_child_resident"]
	except KeyError as ea_child_resident_error:
		ea_child_resident = 0
	try:
		non_resident_child = request.json["non_resident_child"]
	except KeyError as non_resident_child_error:
		non_resident_child = 0
	try:
		infant = request.json["infant"]
	except KeyError as infant_error:
		infant = 0

	stripped_start_year,stripped_start_month,stripped_start_day = request.json["start_date"].split("-")
	stripped_end_year,stripped_end_month,stripped_end_day = request.json["end_date"].split("-")

	int_start_year = int(stripped_start_year)
	int_start_month = int(stripped_start_month)
	int_start_day = int(stripped_start_day)
	int_end_year = int(stripped_end_year)
	int_end_month = int(stripped_end_month)
	int_end_day = int(stripped_end_day)

	start_date = date(int_start_year,int_start_month,int_start_day)
	end_date = date(int_end_year,int_end_month,int_end_day)
	
	if int(abs(end_date - start_date).days) == 0:
		date_diff = 1
	else:
		date_diff = int(abs(end_date - start_date).days)

	exchange_rate_data = requests.get(get_latest_exchange_rate)

	gatepass_fee = db.session.query(Mandatory)\
							 .filter(Mandatory.deletion_marker == None)\
							 .order_by(Mandatory.payment_id.desc())\
							 .first()

	## Non Resident Adult
	if request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_adult_currency == "USD":
		payment_non_resident_adult_amount = float(gatepass_fee.payment_non_resident_adult_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_adult_currency == "USD":
		payment_non_resident_adult_amount = \
					round(float(gatepass_fee.payment_non_resident_adult_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_adult_currency == "KES":
		payment_non_resident_adult_amount = \
					round(float(gatepass_fee.payment_non_resident_adult_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_adult_currency == "KES":
		payment_non_resident_adult_amount = float(gatepass_fee.payment_non_resident_adult_amount)

	## Non Resident Child
	if request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_child_currency == "USD":
		payment_non_resident_child_amount = float(gatepass_fee.payment_non_resident_child_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_child_currency == "USD":
		payment_non_resident_child_amount = \
					round(float(gatepass_fee.payment_non_resident_child_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_non_resident_child_currency == "KES":
		payment_non_resident_child_amount = \
					round(float(gatepass_fee.payment_non_resident_child_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_non_resident_child_currency == "KES":
		payment_non_resident_child_amount = float(gatepass_fee.payment_non_resident_child_amount)

	## EA Resident Adult
	if request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_adult_currency == "USD":
		payment_ea_resident_adult_amount = float(gatepass_fee.payment_ea_resident_adult_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_adult_currency == "USD":
		payment_ea_resident_adult_amount = \
					round(float(gatepass_fee.payment_ea_resident_adult_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_adult_currency == "KES":
		payment_ea_resident_adult_amount = \
					round(float(gatepass_fee.payment_ea_resident_adult_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_adult_currency == "KES":
		payment_ea_resident_adult_amount = float(gatepass_fee.payment_ea_resident_adult_amount)

	## EA Resident Child
	if request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_child_currency == "USD":
		payment_ea_resident_child_amount = float(gatepass_fee.payment_ea_resident_child_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_child_currency == "USD":
		payment_ea_resident_child_amount = \
					round(float(gatepass_fee.payment_ea_resident_child_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_ea_resident_child_currency == "KES":
		payment_ea_resident_child_amount = \
					round(float(gatepass_fee.payment_ea_resident_child_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_ea_resident_child_currency == "KES":
		payment_ea_resident_child_amount = float(gatepass_fee.payment_ea_resident_child_amount)

	## Citizen Adult
	if request.json["currency"] == "USD" and gatepass_fee.payment_citizen_adult_currency == "USD":
		payment_citizen_adult_amount = float(gatepass_fee.payment_citizen_adult_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_adult_currency == "USD":
		payment_citizen_adult_amount = \
					round(float(gatepass_fee.payment_citizen_adult_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_citizen_adult_currency == "KES":
		payment_citizen_adult_amount = \
					round(float(gatepass_fee.payment_citizen_adult_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_adult_currency == "KES":
		payment_citizen_adult_amount = float(gatepass_fee.payment_citizen_adult_amount)

	## Citizen Child
	if request.json["currency"] == "USD" and gatepass_fee.payment_citizen_child_currency == "USD":
		payment_citizen_child_amount = float(gatepass_fee.payment_citizen_child_amount)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_child_currency == "USD":
		payment_citizen_child_amount = \
					round(float(gatepass_fee.payment_citizen_child_amount) * float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "USD" and gatepass_fee.payment_citizen_child_currency == "KES":
		payment_citizen_child_amount = \
					round(float(gatepass_fee.payment_citizen_child_amount)/float(exchange_rate_data.json()["data"][0]["base_currency_amount"]), 2)
	elif request.json["currency"] == "KES" and gatepass_fee.payment_citizen_child_currency == "KES":
		payment_citizen_child_amount = float(gatepass_fee.payment_citizen_child_amount)

	gatepass = Gatepass(
		gatepass_public_id = gatepass_id,
		gatepass_date = today,
		destination = request.json["destination"],
		gatepass_done_by = member_details.json()["full_name"],
		gatepass_phone_number = phone,
		gatepass_ref_code = str(uuid.uuid4())[:10],
		booking_id = booking_id,
		member_id = member,
		member_visit = 1,
		gatepass_payment_status = 1,
		start_date = request.json["start_date"],
		end_date = request.json["end_date"],
		session_id = request.json["session_id"]
	)

	db.session.add(gatepass)

	booking_guest1 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Adult Citizen"),
		gatepass_guest_count = ea_adult_citizen,
		gatepass_cost_per_pp = payment_citizen_adult_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest1)

	booking_guest2 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Adult Resident"),
		gatepass_guest_count = ea_adult_resident,
		gatepass_cost_per_pp = payment_ea_resident_adult_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest2)

	booking_guest3 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Non Resident Adult"),
		gatepass_guest_count = non_resident_adult,
		gatepass_cost_per_pp = payment_non_resident_adult_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest3)

	booking_guest4 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Child Citizen"),
		gatepass_guest_count = ea_child_citizen,
		gatepass_cost_per_pp = payment_citizen_child_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest4)

	booking_guest5 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("EA Child Resident"),
		gatepass_guest_count = ea_child_resident
	)

	db.session.add(booking_guest5)

	booking_guest6 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Non Resident Child"),
		gatepass_guest_count = non_resident_child,
		gatepass_cost_per_pp = payment_non_resident_child_amount,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest6)

	booking_guest7 = GatepassGuest(
		gatepass_guest_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		gatepass_guest_type = get_booking_guest_type_id("Infant"),
		gatepass_guest_count = infant,
		gatepass_cost_per_pp = 0,
		gatepass_no_of_nights = date_diff,
		gatepass_currency = request.json["currency"]
	)

	db.session.add(booking_guest7)

	gatepass_detail = GatepassDetail(
		gatepass_details_public_id = str(uuid.uuid4()),
		gatepass_id = gatepass_id,
		first_name = first,
		last_name = last,
		email_address = email,
		phone_number = phone
	)

	db.session.add(gatepass_detail)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The gate pass has been issued. Proceed to payment.")

		return_data = {}
		return_data["gatepass_public_id"] = gatepass_id
		return_data["phone"] = phone

		return jsonify({"message": output, "data": return_data}), 201
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("It's us, not you. There was an error while issuing the gate pass. :-( Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/gatepass/members/view")
def view_all_member_gatepasses():
	return_gatepass = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail, Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.add_columns(Gatepass.gatepass_public_id, Gatepass.destination, Gatepass.gatepass_date,\
											 Gatepass.gatepass_done_by, Gatepass.gatepass_phone_number,\
											 Gatepass.gatepass_ref_code, Gatepass.session_id, Gatepass.gatepass_payment_status,\
											 Gatepass.start_date, Gatepass.end_date,\
											 GatepassDetail.first_name, GatepassDetail.last_name, GatepassDetail.email_address,\
											 GatepassDetail.phone_number)\
								.filter(Gatepass.deletion_marker==None)\
								.filter(Gatepass.member_visit == 1)\
								.all()

	if not return_gatepass:
		output = []
		output.append("There are currently no member gatepasses in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []

		for single in return_gatepass:
			return_data = {}
			return_data["gatepass_public_id"] = single.gatepass_public_id
			return_data["destination"] = single.destination
			return_data["gatepass_date"] = single.gatepass_date
			return_data["gatepass_done_by"] = single.gatepass_done_by
			return_data["gatepass_phone_number"] = single.gatepass_phone_number
			return_data["gatepass_ref_code"] = single.gatepass_ref_code
			return_data["first_name"] = single.first_name
			return_data["last_name"] = single.last_name
			return_data["start_date"] = single.start_date
			return_data["end_date"] = single.end_date

			guest_array = []
			
			get_all_guests = db.session.query(GatepassGuest)\
									   .join(GuestType, GatepassGuest.gatepass_guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name, GuestType.booking_guest_type_public_id,\
									   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_guest_type)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.gatepass_id == single.gatepass_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.gatepass_guest_count

			try:
				guest_data = {}
				guest_data["ea_adult_citizen"] = ea_adult_citizen
				guest_data["ea_adult_resident"] = ea_adult_resident
				guest_data["non_resident_adult"] = non_resident_adult
				guest_data["ea_child_citizen"] = ea_child_citizen
				guest_data["ea_child_resident"] = ea_child_resident
				guest_data["non_resident_child"] = non_resident_child
				guest_data["infant"] = infant

				guest_array.append(guest_data)

				return_data["guests"] = guest_array

				return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
									 		+ ea_child_resident + non_resident_child + infant
			except UnboundLocalError as guest_data_unavailable:
				pass

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/gatepass/members/view/<gatepass_id>")
def view_single_member_gatepass(gatepass_id):
	return_gatepass = db.session.query(Gatepass)\
								.join(GatepassGuest, Gatepass.gatepass_public_id == GatepassGuest.gatepass_id)\
								.join(GatepassDetail, Gatepass.gatepass_public_id == GatepassDetail.gatepass_id)\
								.add_columns(Gatepass.gatepass_public_id, Gatepass.destination, Gatepass.gatepass_date,\
											 Gatepass.gatepass_done_by, Gatepass.gatepass_phone_number,\
											 Gatepass.gatepass_ref_code, Gatepass.session_id, Gatepass.gatepass_payment_status,\
											 Gatepass.start_date, Gatepass.end_date,\
											 GatepassDetail.first_name, GatepassDetail.last_name, GatepassDetail.email_address,\
											 GatepassDetail.phone_number)\
								.filter(Gatepass.deletion_marker==None)\
								.filter(Gatepass.member_visit == 1)\
								.filter(Gatepass.gatepass_public_id==gatepass_id)\
								.all()

	if not return_gatepass:
		output = []
		output.append("That member gatepass does not appear to exist in the system.")
		return jsonify({"message": output}), 200
	else:
		output = []

		for single in return_gatepass:
			return_data = {}
			return_data["gatepass_public_id"] = single.gatepass_public_id
			return_data["destination"] = single.destination
			return_data["gatepass_date"] = single.gatepass_date
			return_data["gatepass_done_by"] = single.gatepass_done_by
			return_data["gatepass_phone_number"] = single.gatepass_phone_number
			return_data["gatepass_ref_code"] = single.gatepass_ref_code
			return_data["first_name"] = single.first_name
			return_data["last_name"] = single.last_name
			return_data["start_date"] = single.start_date
			return_data["end_date"] = single.end_date

			guest_array = []
			
			get_all_guests = db.session.query(GatepassGuest)\
									   .join(GuestType, GatepassGuest.gatepass_guest_type == GuestType.booking_guest_type_public_id)\
									   .add_columns(GuestType.booking_guest_type_name, GuestType.booking_guest_type_public_id,\
									   				GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_guest_type)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.gatepass_id == single.gatepass_public_id)\
									   .all()

			for each_guest in get_all_guests:
				if each_guest.booking_guest_type_name == "EA Adult Citizen":
					ea_adult_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Adult Resident":
					ea_adult_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Adult":
					non_resident_adult = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Citizen":
					ea_child_citizen = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "EA Child Resident":
					ea_child_resident = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Non Resident Child":
					non_resident_child = each_guest.gatepass_guest_count
				if each_guest.booking_guest_type_name == "Infant":
					infant = each_guest.gatepass_guest_count

			try:
				guest_data = {}
				guest_data["ea_adult_citizen"] = ea_adult_citizen
				guest_data["ea_adult_resident"] = ea_adult_resident
				guest_data["non_resident_adult"] = non_resident_adult
				guest_data["ea_child_citizen"] = ea_child_citizen
				guest_data["ea_child_resident"] = ea_child_resident
				guest_data["non_resident_child"] = non_resident_child
				guest_data["infant"] = infant

				guest_array.append(guest_data)

				return_data["guests"] = guest_array

				return_data["guest_total"] = ea_adult_citizen + ea_adult_resident + non_resident_adult + ea_child_citizen\
									 		+ ea_child_resident + non_resident_child + infant
			except UnboundLocalError as guest_data_unavailable:
				pass

			output.append(return_data)

		return jsonify({"data": output}), 200


#############
#### Ref ####
#############
def get_booking_status_id(status_name):
	status_id = db.session.query(BookingStatus)\
						  .filter(BookingStatus.deletion_marker == None)\
						  .filter(BookingStatus.booking_status_name == status_name)\
						  .all()

	for single in status_id:
		booking_status_id = single.booking_status_public_id

		return booking_status_id


def get_booking_guest_type_id(guest_type_name):
	type_id = db.session.query(GuestType)\
						.filter(GuestType.deletion_marker == None)\
						.filter(GuestType.booking_guest_type_name == guest_type_name)\
						.all()

	for single in type_id:
		booking_guest_type_id = single.booking_guest_type_public_id

		return booking_guest_type_id