import math
import os
import sys
import threading
import traceback
import uuid
from datetime import date, datetime, timedelta

import pdfkit
import pymysql
import requests
from flask import Response, json, jsonify, request

from database.booking_activity_log import BookingActivity
from database.bookings import Booking
from routes import FromCache, app, bookings_logger, db, db_cache
from variables import get_user_from_aumra


@app.route("/activities/view")
def view_all_booking_activities():
	get_all_activities = db.session.query(BookingActivity)\
								   .add_columns(BookingActivity.booking_activity_public_id, BookingActivity.booking_activity_description, BookingActivity.session_id,\
												BookingActivity.created_at,\
												Booking.booking_ref_code)\
								   .join(Booking, BookingActivity.booking_id == Booking.booking_public_id)\
								   .filter(BookingActivity.deletion_marker == None)\
								   .all()

	if not get_all_activities:
		message = []
		message.append("There are no booking activities in the system.")
		return jsonify({"message": message}), 412

	id_array = []

	for each_id in get_all_activities:
		if each_id.session_id:
			id_array.append(each_id.session_id)

	try:
		return_user = requests.post(get_user_from_aumra,\
									json = {"users_ids": id_array})
	except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
		pass
	
	data = []
	for single in get_all_activities:
		return_data = {}
		return_data["booking_activity_public_id"] = single.booking_activity_public_id
		return_data["booking_activity_description"] = single.booking_activity_description
		return_data["created_at"] = single.created_at
		return_data["booking_ref_code"] = single.booking_ref_code
		return_data["session_id"] = single.session_id

		if not single.session_id:
			return_data["user"] = "System activity"
		else:
			try:
				for user in return_user.json()["data"]:
					if user["public_id"] == single.session_id:
						return_data["user"] = user["full_name"]
			except Exception:
				return_data["user"] = "N/A"

		data.append(return_data)

	return jsonify({"data": data}), 200
