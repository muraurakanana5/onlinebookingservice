from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes.seed_functions import PaymentMethodSeed
from database.payment_methods import PaymentMethod


def close(self):
	self.session.close()


#########################
#### Payment Methods ####
#########################
@app.route("/guests/payments/methods/view")
def view_all_guest_payment_methods():
	if db.session.query(PaymentMethod)\
				 .filter(PaymentMethod.deletion_marker==None)\
				 .filter(PaymentMethod.payment_method_public_id == "1e62ecfc-6a3a-4112-835c-4dc3fe8e28c7")\
				 .count() == 0:
		PaymentMethodSeed.seed_default_payment_methods()
	
	if db.session.query(PaymentMethod)\
				.filter(PaymentMethod.deletion_marker==None)\
				.count() == 0:
		output = []
		output.append("There are currently no payment methods in the system.")
		return jsonify({"message": output}), 200

	return_methods = db.session.query(PaymentMethod)\
							   .filter(PaymentMethod.deletion_marker==None)\
                               .filter(PaymentMethod.payment_method_guest == 1)\
							   .all()

	data = []

	for single in return_methods:
		data.append(single.return_json())

	return jsonify({"data": data}), 200