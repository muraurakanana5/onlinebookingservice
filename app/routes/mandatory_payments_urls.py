from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid

# File imports
from routes import app
from routes import db
from routes.seed_functions import MandatoryPaymentsSeed
from database.mandatory_payments import Mandatory
from database.booking_guest_types import GuestType
from database.mandatory_payment_prices import MandatoryPaymentPrices
from database.mandatory_payment_schedule import MandatoryPaymentScheduleCalendar
from database.destination import Destination

from variables import *

from functions.booking_snippets import *
from functions.date_operators import *


def close(self):
    self.session.close()


############################
#### Mandatory Payments ####
############################
@app.route("/payments/default/new", methods=["POST"])
def add_new_default_payment():
    messages = []

    try:
        request.json["income_account_code"].strip()
        if not request.json["income_account_code"]:
            messages.append("Income Account Code is empty.")
    except KeyError as e:
        messages.append("Income Account Code is missing.")

    try:
        request.json["department_analysis_code"].strip()
        if not request.json["department_analysis_code"]:
            messages.append("Department Analysis Code is empty.")
    except KeyError as e:
        messages.append("Department Analysis Code is missing.")

    try:
        request.json["person"].strip()
        if not request.json["person"]:
            messages.append("Person is empty.")
    except KeyError as e:
        messages.append("Person is missing.")

    try:
        request.json["marker"].strip()
        if not request.json["marker"]:
            messages.append("Marker is empty.")
    except KeyError as e:
        messages.append("Marker is missing.")

    try:
        request.json["description"].strip()
        if not request.json["description"]:
            messages.append("Description is empty.")
    except KeyError as e:
        messages.append("Description is missing.")

    try:
        request.json["school"].strip()
        if not request.json["school"]:
            messages.append("School conservancy fee marker is empty.")
    except KeyError as e:
        messages.append("School conservancy fee marker is missing.")

    try:
        str(request.json["max_number"]).strip()
        if not str(request.json["max_number"]):
            messages.append("Max number is empty.")
    except KeyError as e:
        messages.append("Max number is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    if messages:
        message = []
        message.append("You appear to be missing some data. Please try again.")
        return jsonify({"messages": messages}), 422

    check_name = db.session.query(Mandatory)\
            .filter(Mandatory.deletion_marker == None)\
            .all()

    for each_name in check_name:
        if each_name.payment_person.lower() == request.json["person"].lower():
            message = []
            message.append("That category already exists in the system.")
            return jsonify({"message": message}), 422

    if request.json["school"] == "Yes":
        school = "Y"
    elif request.json["school"] == "No":
        school = "N"

    payment = Mandatory(
        payment_public_id=str(uuid.uuid4()),
        payment_person_income_code=request.json["income_account_code"],
        payment_person_dept_code=request.json["department_analysis_code"],
        payment_person=request.json["person"].title(),
        marker=request.json["marker"],
        maximum_number=request.json["max_number"],
        school=school,
        session_id=request.json["session_id"],
        created_at=datetime.now(),
        updated_at=datetime.now())

    db.session.add(payment)

    guest = GuestType(booking_guest_type_public_id=str(uuid.uuid4())[:8],
                      booking_guest_type_name=request.json["person"],
                      session_id=request.json["session_id"],
                      created_at=datetime.now(),
                      updated_at=datetime.now())

    db.session.add(guest)

    try:
        db.session.commit()
        close(db)

        message = []
        message.append("You have added a new gate entry fee category.")

        return jsonify({"message": message}), 201

    except Exception as e:
        db.session.rollback()
        close(db)

        message = []
        message.append(
            "The gate entry fee category could not be added at this moment. Try later."
        )

        return jsonify({"message": message, "error": str(e)}), 422


@app.route("/payments/public/check-in-date", methods=["POST"])
def query_schedule_by_check_in_date():
    check_in = request.json["check_in_date"]

    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
           .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_destination == "")\
           .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= check_in)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= check_in)\
           .first()

    if not get_schedule:
        message = []
        message.append(
            "Entry fee pricing for the selected period has not been set. Please contact customer care."
        )
        return jsonify({"message": message}), 412
    else:
        get_prices = db.session.query(MandatoryPaymentPrices)\
                .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                 MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                 Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                 Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                 Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
                .filter(MandatoryPaymentPrices.deletion_marker == None)\
                .filter(MandatoryPaymentPrices.payment_schedule == get_schedule.schedule_public_id)\
                .filter(Mandatory.deletion_marker == None)\
                .filter(Mandatory.payment_person != "Ea Resident Student")\
                .filter(Mandatory.payment_person != "Non Resident Student")\
                .filter(Mandatory.payment_person != "Citizen Student")\
                .order_by(Mandatory.payment_person.asc())\
                .all()

        data = []

        for single in get_prices:
            return_data = {}
            return_data["payment_public_id"] = single.payment_public_id
            return_data[
                "payment_person_income_code"] = single.payment_person_income_code
            return_data[
                "payment_person_dept_code"] = single.payment_person_dept_code
            return_data["payment_person"] = single.payment_person.title()
            return_data["payment_schedule"] = single.payment_schedule

            try:
                currency = requests.get(
                    get_currency.format(single.payment_currency))
                return_data["payment_person_currency"] = currency.json(
                )["data"][0]["currency_name"]
                return_data[
                    "payment_person_currency_id"] = single.payment_currency
            except Exception:
                return_data["payment_person_currency"] = "N/A"
                return_data[
                    "payment_person_currency_id"] = single.payment_currency

            return_data[
                "payment_person_description"] = single.payment_person_description
            return_data["payment_person_amount"] = float(single.payment_price)
            return_data["payment_person_discount"] = 0
            return_data["payment_guests"] = 0
            return_data["marker"] = single.marker
            return_data["school"] = single.school
            return_data["amount"] = 0
            return_data["max_number"] = single.maximum_number
            return_data["session_id"] = single.session_id

            data.append(return_data)

        return jsonify({"data": data}), 200


@app.route("/payments/public/check-in-out-date", methods=["POST"])
def query_schedule_by_check_in_out_date():
    check_in = request.json["check_in_date"]
    check_out = request.json["check_out_date"]

    # if not check_in or check_out:
    # 	message = []
    # 	message.append("Check-in and check-out dates must be provided.")
    # 	return jsonify({"message": message}), 422

    if check_in == check_out:
        # Setting destination to day trip
        destination = "5717abc1-98e9-4928-ba35-40f2fc93b3d3"
    else:
        destination = ""

    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
           .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_destination == destination)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= check_in)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= check_out)\
           .first()

    if not get_schedule:
        get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
                .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
                .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= check_in)\
                .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= check_out)\
                .first()

    if not get_schedule:
        message = []
        message.append(
            "Entry fee pricing for the selected period has not been set. Please contact customer care."
        )
        return jsonify({"message": message}), 412
    else:
        get_prices = db.session.query(MandatoryPaymentPrices)\
                .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                 MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                 Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                 Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                 Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
                .filter(MandatoryPaymentPrices.deletion_marker == None)\
                .filter(MandatoryPaymentPrices.payment_schedule == get_schedule.schedule_public_id)\
                .filter(Mandatory.deletion_marker == None)\
                .filter(Mandatory.payment_person != "Ea Resident Student")\
                .filter(Mandatory.payment_person != "Non Resident Student")\
                .filter(Mandatory.payment_person != "Citizen Student")\
                .order_by(Mandatory.payment_person.asc())\
                .all()

        data = []

        for single in get_prices:
            return_data = {}
            return_data["payment_public_id"] = single.payment_public_id
            return_data[
                "payment_person_income_code"] = single.payment_person_income_code
            return_data[
                "payment_person_dept_code"] = single.payment_person_dept_code
            return_data["payment_person"] = single.payment_person.title()
            return_data["payment_schedule"] = single.payment_schedule

            try:
                currency = requests.get(
                    get_currency.format(single.payment_currency))
                return_data["payment_person_currency"] = currency.json(
                )["data"][0]["currency_name"]
                return_data[
                    "payment_person_currency_id"] = single.payment_currency
            except Exception:
                return_data["payment_person_currency"] = "N/A"
                return_data[
                    "payment_person_currency_id"] = single.payment_currency

            return_data[
                "payment_person_description"] = single.payment_person_description
            return_data["payment_person_amount"] = float(single.payment_price)
            return_data["payment_person_discount"] = 0
            return_data["payment_guests"] = 0
            return_data["marker"] = single.marker
            return_data["school"] = single.school
            return_data["amount"] = 0
            return_data["max_number"] = single.maximum_number
            return_data["session_id"] = single.session_id

            data.append(return_data)

        return jsonify({"data": data}), 200


@app.route("/payments/default/view", methods=["GET", "POST"])
def view_default_payments():
    if db.session.query(Mandatory)\
        .filter(Mandatory.deletion_marker==None)\
        .filter(Mandatory.payment_person=="Infant")\
        .count() == 0:
        MandatoryPaymentsSeed.seed_default_mandatory_payments()

    today = datetime.now()

    # get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
    # 							 .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
    # 							 .filter(MandatoryPaymentScheduleCalendar.schedule_destination == "")\
    # 							 .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= today.strftime("%Y-%m-%d"))\
    # 							 .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= today.strftime("%Y-%m-%d"))\
    # 							 .all()
    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
            .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
            .filter(MandatoryPaymentScheduleCalendar.schedule_destination == None)\
            .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= today.strftime("%Y-%m-%d"))\
            .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= today.strftime("%Y-%m-%d"))\
            .all()
			
    if not get_schedule:
        message = []
        message.append("There are no pricing schedules set.")
        return jsonify({"message": message}), 412

    else:
        data = []

        for single_schedule in get_schedule:
            get_prices = db.session.query(MandatoryPaymentPrices)\
                    .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                    .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                        MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                     Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                     Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                     Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
                    .filter(MandatoryPaymentPrices.deletion_marker == None)\
                    .filter(MandatoryPaymentPrices.payment_schedule == single_schedule.schedule_public_id)\
                    .filter(Mandatory.deletion_marker == None)\
                    .order_by(Mandatory.payment_person.asc())\
                    .all()

            for single in get_prices:
                return_data = {}
                return_data["payment_public_id"] = single.payment_public_id
                return_data[
                    "payment_person_income_code"] = single.payment_person_income_code
                return_data[
                    "payment_person_dept_code"] = single.payment_person_dept_code
                return_data["payment_person"] = single.payment_person.title()
                return_data["payment_schedule"] = single.payment_schedule

                try:
                    currency = requests.get(
                        get_currency.format(single.payment_currency))
                    return_data["payment_person_currency"] = currency.json(
                    )["data"][0]["currency_name"]
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency
                except Exception:
                    return_data["payment_person_currency"] = "N/A"
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency

                return_data[
                    "payment_person_description"] = single.payment_person_description
                return_data["payment_person_amount"] = float(
                    single.payment_price)
                return_data["payment_person_discount"] = 0
                return_data["payment_guests"] = 0
                return_data["marker"] = single.marker
                return_data["school"] = single.school
                return_data["amount"] = 0
                return_data["max_number"] = single.maximum_number
                return_data["session_id"] = single.session_id

                data.append(return_data)

    return jsonify({"data": data}), 200


@app.route("/payments/default/view/<payment_id>")
def view_single_default_payments(payment_id):
    if db.session.query(Mandatory)\
        .filter(Mandatory.deletion_marker==None)\
        .filter(Mandatory.payment_person=="Infant")\
        .count() == 0:
        MandatoryPaymentsSeed.seed_default_mandatory_payments()

    return_payments = db.session.query(Mandatory)\
           .filter(Mandatory.deletion_marker == None)\
           .filter(Mandatory.payment_public_id == payment_id)\
           .all()

    output = []

    for single in return_payments:
        return_data = {}
        return_data["payment_public_id"] = single.payment_public_id
        return_data[
            "payment_person_income_code"] = single.payment_person_income_code
        return_data[
            "payment_person_dept_code"] = single.payment_person_dept_code
        return_data["payment_person"] = single.payment_person.title()

        # try:
        # 	currency = requests.get(get_currency.format(single.payment_person_currency))
        # 	return_data["payment_person_currency"] = currency.json()["data"][0]["currency_name"]
        # 	return_data["payment_person_currency_id"] = single.payment_person_currency
        # except Exception:
        # 	return_data["payment_person_currency"] = "N/A"
        # 	return_data["payment_person_currency_id"] = single.payment_person_currency

        return_data["payment_person_currency"] = "KES"
        return_data["payment_person_currency_id"] = "1234567"
        return_data["payment_person_amount"] = float(0)

        return_data[
            "payment_person_description"] = single.payment_person_description
        # return_data["payment_person_amount"] = float(single.payment_person_amount)
        return_data["payment_person_discount"] = 0
        return_data["payment_person_discount_reason"] = None
        return_data["payment_guests"] = 0
        return_data["marker"] = single.marker
        return_data["school"] = single.school
        return_data["amount"] = 0
        return_data["max_number"] = single.maximum_number
        return_data["session_id"] = single.session_id

        output.append(return_data)

    return jsonify({"data": output}), 200


@app.route("/payments/default/view/public")
def view_default_public_payments():
    ## Don't think this is necessary at this point in time, especially seeing that this is an endpoint accessed by the public
    # if db.session.query(Mandatory)\
    # 				.filter(Mandatory.deletion_marker==None)\
    # 				.filter(Mandatory.payment_person=="Infant")\
    # 				.count() == 0:
    # 	MandatoryPaymentsSeed.seed_default_mandatory_payments()

    today = datetime.now()

    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
            .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= today.strftime("%Y-%m-%d"))\
            .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= today.strftime("%Y-%m-%d"))\
            .filter(Mandatory.deletion_marker==None)\
            .all()

    if not get_schedule:
        message = []
        message.append("There are no pricing schedules set.")
        return jsonify({"message": message}), 412

    else:
        data = []

        for single_schedule in get_schedule:
            get_prices = db.session.query(MandatoryPaymentPrices)\
                    .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                    .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                        MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                     Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                     Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                     Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
                    .filter(MandatoryPaymentPrices.deletion_marker == None)\
                    .filter(MandatoryPaymentPrices.payment_schedule == single_schedule.schedule_public_id)\
                    .filter(Mandatory.deletion_marker == None)\
                    .filter(Mandatory.payment_person != "Ea Resident Student")\
                    .filter(Mandatory.payment_person != "Non Resident Student")\
                    .filter(Mandatory.payment_person != "Citizen Student")\
                    .order_by(Mandatory.payment_person.asc())\
                    .all()

            for single in get_prices:
                return_data = {}
                return_data["payment_public_id"] = single.payment_public_id
                return_data[
                    "payment_person_income_code"] = single.payment_person_income_code
                return_data[
                    "payment_person_dept_code"] = single.payment_person_dept_code
                return_data["payment_person"] = single.payment_person.title()
                return_data["payment_schedule"] = single.payment_schedule

                try:
                    currency = requests.get(
                        get_currency.format(single.payment_currency))
                    return_data["payment_person_currency"] = currency.json(
                    )["data"][0]["currency_name"]
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency
                except Exception:
                    return_data["payment_person_currency"] = "N/A"
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency

                return_data[
                    "payment_person_description"] = single.payment_person_description
                return_data["payment_person_amount"] = float(
                    single.payment_price)
                return_data["payment_person_discount"] = 0
                return_data["payment_guests"] = 0
                return_data["marker"] = single.marker
                return_data["school"] = single.school
                return_data["amount"] = 0
                return_data["max_number"] = single.maximum_number
                return_data["session_id"] = single.session_id

                data.append(return_data)

    return jsonify({"data": data}), 200


@app.route("/payments/default/view/school")
def view_default_school_trip_payments():
    if db.session.query(Mandatory)\
        .filter(Mandatory.deletion_marker==None)\
        .filter(Mandatory.payment_person=="Infant")\
        .count() == 0:
        MandatoryPaymentsSeed.seed_default_mandatory_payments()

    today = datetime.now()

    # get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
    # 							 .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
    # 							 .filter(MandatoryPaymentScheduleCalendar.schedule_destination == "")\
    # 							 .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= today.strftime("%Y-%m-%d"))\
    # 							 .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= today.strftime("%Y-%m-%d"))\
    # 							 .all()
    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
            .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
            .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= today.strftime("%Y-%m-%d"))\
            .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= today.strftime("%Y-%m-%d"))\
            .all()

    if not get_schedule:
        message = []
        message.append("There are no pricing schedules set.")
        return jsonify({"message": message}), 412

    else:
        data = []

        for single_schedule in get_schedule:
            get_prices = db.session.query(MandatoryPaymentPrices)\
                    .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                    .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                        MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                     Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                     Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                     Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
                    .filter(MandatoryPaymentPrices.deletion_marker == None)\
                    .filter(MandatoryPaymentPrices.payment_schedule == single_schedule.schedule_public_id)\
                    .filter(Mandatory.deletion_marker == None)\
                    .filter(Mandatory.school == "Y")\
                    .order_by(Mandatory.payment_person.asc())\
                    .all()

            for single in get_prices:
                return_data = {}
                return_data["payment_public_id"] = single.payment_public_id
                return_data[
                    "payment_person_income_code"] = single.payment_person_income_code
                return_data[
                    "payment_person_dept_code"] = single.payment_person_dept_code
                return_data["payment_person"] = single.payment_person.title()
                return_data["payment_schedule"] = single.payment_schedule

                try:
                    currency = requests.get(
                        get_currency.format(single.payment_currency))
                    return_data["payment_person_currency"] = currency.json(
                    )["data"][0]["currency_name"]
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency
                except Exception:
                    return_data["payment_person_currency"] = "N/A"
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency

                return_data[
                    "payment_person_description"] = single.payment_person_description
                return_data["payment_person_amount"] = float(
                    single.payment_price)
                return_data["payment_person_discount"] = 0
                return_data["payment_guests"] = 0
                return_data["marker"] = single.marker
                return_data["school"] = single.school
                return_data["amount"] = 0
                return_data["max_number"] = single.maximum_number
                return_data["session_id"] = single.session_id

                data.append(return_data)

    return jsonify({"data": data}), 200


@app.route("/payments/default/modify", methods=["PATCH"])
def edit_default_payment():
    messages = []

    try:
        request.json["payment_id"].strip()
        if not request.json["payment_id"]:
            messages.append("Payment ID is empty.")
    except KeyError as e:
        messages.append("Payment ID is missing.")

    try:
        request.json["income_account_code"].strip()
        if not request.json["income_account_code"]:
            messages.append("Income Account Code is empty.")
    except KeyError as e:
        messages.append("Income Account Code is missing.")

    try:
        request.json["department_analysis_code"].strip()
        if not request.json["department_analysis_code"]:
            messages.append("Department Analysis Code is empty.")
    except KeyError as e:
        messages.append("Department Analysis Code is missing.")

    try:
        request.json["person"].strip()
        if not request.json["person"]:
            messages.append("Person is empty.")
    except KeyError as e:
        messages.append("Person is missing.")

    try:
        request.json["marker"].strip()
        if not request.json["marker"]:
            messages.append("Marker is empty.")
    except KeyError as e:
        messages.append("Marker is missing.")

    try:
        request.json["description"].strip()
        if not request.json["description"]:
            messages.append("Description is empty.")
    except KeyError as e:
        messages.append("Description is missing.")

    try:
        request.json["school"].strip()
        if not request.json["school"]:
            messages.append("School conservancy fee marker is empty.")
    except KeyError as e:
        messages.append("School conservancy fee marker is missing.")

    try:
        str(request.json["max_number"]).strip()
        if not str(request.json["max_number"]):
            messages.append("Max number is empty.")
    except KeyError as e:
        messages.append("Max number is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    if messages:
        message = []
        message.append("You appear to be missing some data. Please try again.")
        return jsonify({"messages": messages}), 422

    get_payment = db.session.query(Mandatory)\
          .filter(Mandatory.deletion_marker == None)\
          .filter(Mandatory.payment_public_id == request.json["payment_id"])\
          .first()

    if not get_payment:
        message = []
        message.append(
            "That gate entry fee does not appear to exist in the system. Please try again."
        )
        return jsonify({"message": message}), 422

    else:
        if request.json["school"] == "Yes":
            school = "Y"
        elif request.json["school"] == "No":
            school = "N"

        get_guest_type = db.session.query(GuestType)\
                 .filter(GuestType.deletion_marker == None)\
                 .all()

        for single_guest_type in get_guest_type:
            if single_guest_type.booking_guest_type_name.lower(
            ) == get_payment.payment_person.lower():
                single_guest_type.booking_guest_type_name = request.json[
                    "person"]

        get_payment.payment_person_income_code = request.json[
            "income_account_code"]
        get_payment.payment_person_dept_code = request.json[
            "department_analysis_code"]
        get_payment.payment_person = request.json["person"]
        get_payment.marker = request.json["marker"]
        get_payment.maximum_number = request.json["max_number"]
        get_payment.school = school
        get_payment.payment_person_description = request.json["description"]
        get_payment.session_id = request.json["session_id"]
        get_payment.updated_at = datetime.now()

        try:
            db.session.commit()
            close(db)

            output = []
            output.append("The gate entry fee has been modified successfully.")
            return jsonify({"message": output}), 200

        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append(
                "There was a slight issue modifying the gate entry fee. :-( Please try again later."
            )
            return jsonify({"message": output}), 422


@app.route("/payments/default/delete", methods=["PATCH"])
def delete_default_payment():
    messages = []

    try:
        request.json["payment_id"].strip()
        if not request.json["payment_id"]:
            messages.append("Payment ID is empty.")
    except KeyError as e:
        messages.append("Payment ID is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    if messages:
        message = []
        message.append("You appear to be missing some data. Please try again.")
        return jsonify({"message": message, "messages": messages}), 422

    get_payment = db.session.query(Mandatory)\
          .filter(Mandatory.deletion_marker == None)\
          .filter(Mandatory.payment_public_id == request.json["payment_id"])\
          .first()

    if not get_payment:
        message = []
        message.append(
            "That gate entry fee does not appear to exist in the system. Please try again."
        )
        return jsonify({"message": message}), 422

    else:
        get_guest_type = db.session.query(GuestType)\
                 .filter(GuestType.deletion_marker == None)\
                 .all()

        for single_guest_type in get_guest_type:
            if single_guest_type.booking_guest_type_name.lower(
            ) == get_payment.payment_person.lower():
                single_guest_type.deletion_marker = 1
                single_guest_type.session_id = request.json["session_id"]
                single_guest_type.updated_at = datetime.now()

        get_payment.deletion_marker = 1
        get_payment.session_id = request.json["session_id"]
        get_payment.updated_at = datetime.now()

        try:
            db.session.commit()
            close(db)

            output = []
            output.append("The gate entry fee has been deleted successfully.")
            return jsonify({"message": output}), 200

        except Exception as e:
            print(e)
            db.session.rollback()
            close(db)

            output = []
            output.append(
                "There was a slight issue deleting the gate entry fee. :-( Please try again later."
            )
            return jsonify({"message": output}), 422


#####################################
#### Mandatory Payment Schedules ####
#####################################
@app.route("/payments/schedule/new", methods=["POST"])
def add_new_payment_schedule():
    messages = []

    try:
        request.json["start_date"].strip()
        if not request.json["start_date"]:
            messages.append("Start date is empty.")
    except KeyError as e:
        messages.append("Start date is missing.")

    try:
        request.json["end_date"].strip()
        if not request.json["end_date"]:
            messages.append("End date is empty.")
    except KeyError as e:
        messages.append("End date is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    if messages:
        return jsonify({"messages": messages}), 422

    get_all_categories = db.session.query(Mandatory)\
              .filter(Mandatory.deletion_marker == None)\
              .all()

    categories = []
    for each_category in get_all_categories:
        categories.append(each_category.payment_public_id)

    if len(request.json["fees"]) < len(categories):
        message = []
        message.append("You have set the pricing for " +
                       str(len(request.json["fees"])) +
                       " residency types while " + str(len(categories)) +
                       " are expected. Please check on the set values.")
        return jsonify({"message": message}), 422

    elif len(request.json["fees"]) > len(categories):
        message = []
        message.append(
            "You've set the pricing for more residency types than the system can accept. Please check on the set values."
        )
        return jsonify({"message": message}), 422

    if request.json["start_date"] < datetime.now().strftime("%Y-%m-%d"):
        message = []
        message.append("The schedule start date cannot be in the past.")
        return jsonify({"message": message}), 422

    if request.json["end_date"] < datetime.now().strftime("%Y-%m-%d"):
        message = []
        message.append("The schedule end date cannot be in the past.")
        return jsonify({"message": message}), 422

    if request.json["end_date"] < request.json["start_date"]:
        message = []
        message.append(
            "The schedule end date cannot come before the start date.")
        return jsonify({"message": message}), 422

    start_date = GenerateDateFromString.generateDate(
        request.json["start_date"])
    end_date = GenerateDateFromString.generateDate(request.json["end_date"])

    try:
        destination = request.json["destination"]
    except Exception:
        destination = None

    schedule_id = str(uuid.uuid4())

    schedule = MandatoryPaymentScheduleCalendar(
        schedule_public_id=schedule_id,
        schedule_start_date=start_date,
        schedule_end_date=end_date,
        schedule_destination=destination,
        session_id=request.json["session_id"],
        created_at=datetime.now(),
        updated_at=datetime.now())

    db.session.add(schedule)

    for each_fee in request.json["fees"]:
        payment_prices = MandatoryPaymentPrices(
            price_public_id=str(uuid.uuid4()),
            payment_schedule=schedule_id,
            payment_category=each_fee["payment_person"],
            payment_currency=each_fee["currency"],
            payment_price=float(each_fee["amount"]),
            session_id=request.json["session_id"],
            created_at=datetime.now(),
            updated_at=datetime.now())

        db.session.add(payment_prices)

    try:
        db.session.commit()
        close(db)

        message = []
        message.append("Successfully saved the schedule.")
        return jsonify({"message": message}), 201

    except Exception:
        db.session.rollback()
        close(db)

        message = []
        message.append(
            "There was an error while saving the schedule. Please try again later."
        )
        return jsonify({"message": message}), 422


@app.route("/payments/schedule/view", methods=["GET"])
def view_all_payment_schedules():
    get_schedules = db.session.query(MandatoryPaymentScheduleCalendar)\
            .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
            .order_by(MandatoryPaymentScheduleCalendar.schedule_start_date.desc())\
            .all()

    if not get_schedules:
        message = []
        message.append("There do not appear to be any pricing schedules set.")
        return jsonify({"message": message}), 412

    else:
        data = []

        for single_schedule in get_schedules:
            return_schedule = {}
            return_schedule[
                "schedule_public_id"] = single_schedule.schedule_public_id
            return_schedule[
                "schedule_start_date"] = single_schedule.schedule_start_date
            return_schedule[
                "schedule_end_date"] = single_schedule.schedule_end_date

            if single_schedule.schedule_destination:
                get_destination = db.session.query(Destination)\
                       .filter(Destination.deletion_marker == None)\
                       .filter(Destination.gatepass_destination_public_id == single_schedule.schedule_destination)\
                       .first()

                return_schedule[
                    "schedule_destination"] = get_destination.gatepass_destination_name
            else:
                return_schedule["schedule_destination"] = None

            return_schedule[
                "schedule_destination_id"] = single_schedule.schedule_destination

            get_prices = db.session.query(MandatoryPaymentPrices)\
                    .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                    .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                        MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                     Mandatory.payment_person)\
                    .filter(MandatoryPaymentPrices.deletion_marker == None)\
                    .filter(MandatoryPaymentPrices.payment_schedule == single_schedule.schedule_public_id)\
                    .all()

            price_data = []

            for single_price in get_prices:
                return_price = {}
                return_price["payment_person"] = single_price.payment_person
                return_price["price_public_id"] = single_price.price_public_id

                try:
                    currency = requests.get(
                        get_currency.format(single_price.payment_currency))
                    return_price["payment_currency"] = currency.json(
                    )["data"][0]["currency_name"]
                    return_price[
                        "payment_currency_id"] = single_price.payment_currency
                except Exception:
                    return_price["payment_currency"] = "N/A"
                    return_price[
                        "payment_currency_id"] = single_price.payment_currency

                return_price["payment_price"] = float(
                    single_price.payment_price)

                price_data.append(return_price)

            return_schedule["fees"] = price_data

            data.append(return_schedule)

        return jsonify({"data": data}), 200


@app.route("/payments/schedule/modify", methods=["PATCH"])
def update_payment_schedule():
    messages = []

    try:
        request.json["schedule_id"].strip()
        if not request.json["schedule_id"]:
            messages.append("Schedule ID is empty.")
    except KeyError as e:
        messages.append("Schedule ID is missing.")

    try:
        request.json["start_date"].strip()
        if not request.json["start_date"]:
            messages.append("Start date is empty.")
    except KeyError as e:
        messages.append("Start date is missing.")

    try:
        request.json["end_date"].strip()
        if not request.json["end_date"]:
            messages.append("End date is empty.")
    except KeyError as e:
        messages.append("End date is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    if messages:
        return jsonify({"messages": messages}), 422

    if request.json["start_date"] < datetime.now().strftime("%Y-%m-%d"):
        message = []
        message.append("The schedule start date cannot be in the past.")
        return jsonify({"message": message}), 422

    if request.json["end_date"] < datetime.now().strftime("%Y-%m-%d"):
        message = []
        message.append("The schedule end date cannot be in the past.")
        return jsonify({"message": message}), 422

    if request.json["end_date"] < request.json["start_date"]:
        message = []
        message.append(
            "The schedule end date cannot come before the start date.")
        return jsonify({"message": message}), 422

    try:
        destination = request.json["destination"]
    except Exception:
        destination = None

    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
           .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_public_id == request.json["schedule_id"])\
           .first()

    if not get_schedule:
        message = []
        message.append(
            "The selected schedule does not appear to exist in the system.")
        return jsonify({"message": message}), 412

    get_schedule.schedule_start_date = request.json["start_date"]
    get_schedule.schedule_end_date = request.json["end_date"]
    get_schedule.schedule_destination = destination
    get_schedule.session_id = request.json["session_id"]

    try:
        db.session.commit()
        close(db)

        message = []
        message.append("The selected schedule has been updated.")
        return jsonify({"message": message}), 200

    except Exception:
        db.session.rollback()
        close(db)

        message = []
        message.append(
            "There was an error updating the selected schedule. Try again later."
        )
        return jsonify({"message": message}), 422


@app.route("/payments/schedule/item/modify", methods=["PATCH"])
def update_payment_schedule_item():
    messages = []

    try:
        request.json["price_id"].strip()
        if not request.json["price_id"]:
            messages.append("Price ID is empty.")
    except KeyError as e:
        messages.append("Price ID is missing.")

    try:
        request.json["currency"].strip()
        if not request.json["currency"]:
            messages.append("Currency is empty.")
    except KeyError as e:
        messages.append("Currency is missing.")

    try:
        request.json["amount"].strip()
        if not request.json["amount"]:
            messages.append("Amount is empty.")
    except KeyError as e:
        messages.append("Amount is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    get_schedule_item = db.session.query(MandatoryPaymentPrices)\
             .filter(MandatoryPaymentPrices.deletion_marker == None)\
             .filter(MandatoryPaymentPrices.price_public_id == request.json["price_id"])\
             .first()

    if not get_schedule_item:
        message = []
        message.append("The selected pricing schedule item does not exist.")
        return jsonify({"message": message}), 200

    get_schedule_item.payment_currency = request.json["currency"]
    get_schedule_item.payment_price = float(request.json["amount"])
    get_schedule_item.session_id = request.json["session_id"]

    try:
        db.session.commit()
        close(db)

        message = []
        message.append("Successfully saved the change to pricing.")
        return jsonify({"message": message}), 200

    except Exception:
        db.session.rollback()
        close(db)

        message = []
        message.append(
            "There was an error saving the change to the pricing. Try again later."
        )
        return jsonify({"message": message}), 422


@app.route("/payments/schedule/delete", methods=["PATCH"])
def delete_payment_schedule():
    try:
        request.json["schedule_id"].strip()
        if not request.json["schedule_id"]:
            messages.append("Schedule ID is empty.")
    except KeyError as e:
        messages.append("Schedule ID is missing.")

    try:
        request.json["session_id"].strip()
        if not request.json["session_id"]:
            messages.append("Session ID is empty.")
    except KeyError as e:
        messages.append("Session ID is missing.")

    get_schedules = db.session.query(MandatoryPaymentScheduleCalendar)\
            .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
            .filter(MandatoryPaymentScheduleCalendar.schedule_public_id == request.json["schedule_id"])\
            .first()

    if not get_schedules:
        message = []
        message.append(
            "The selected schedule does not appear to exist in the system.")
        return jsonify({"message": message}), 412

    else:
        get_schedules.deletion_marker = 1
        get_schedules.updated_at = datetime.now()
        get_schedules.session_id = request.json["session_id"]

        get_prices = db.session.query(MandatoryPaymentPrices)\
                .filter(MandatoryPaymentPrices.deletion_marker == None)\
                .filter(MandatoryPaymentPrices.payment_schedule == get_schedules.schedule_public_id)\
                .all()

        for single_price in get_prices:
            single_price.deletion_marker = 1
            single_price.updated_at = datetime.now()
            single_price.session_id = request.json["session_id"]

        try:
            db.session.commit()
            close(db)

            message = []
            message.append("The selected schedule has been deleted.")
            return jsonify({"message": message}), 200

        except Exception:
            db.session.rollback()
            close(db)

            message = []
            message.append(
                "There was an error deleting the selected schedule. Try again later."
            )
            return jsonify({"message": message}), 422


@app.route("/payments/schedule/clear", methods=["GET"])
def clear_expired_schedules():
    today = datetime.now()

    get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
           .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
           .filter(MandatoryPaymentScheduleCalendar.schedule_end_date < today.strftime("%Y-%m-%d"))\
           .all()

    if not get_schedule:
        message = []
        message.append("There are no expired schedules to clear.")
        return jsonify({"message": message}), 200

    for single in get_schedule:
        single_price.deletion_marker = 1
        single_price.updated_at = datetime.now()
        single_price.session_id = "Clear task"

        get_prices = db.session.query(MandatoryPaymentPrices)\
                .filter(MandatoryPaymentPrices.deletion_marker == None)\
                .filter(MandatoryPaymentPrices.payment_schedule == single.schedule_public_id)\
                .all()

        for single_price in get_prices:
            single_price.deletion_marker = 1
            single_price.updated_at = datetime.now()
            single_price.session_id = "Clear task"

    try:
        db.session.commit()
        close(db)

        message = []
        message.append("Successfully cleared expired schedules.")
        return jsonify({"message": message}), 200

    except Exception:
        message = []
        message.append("Unable to clear the expired schedules.")
        return jsonify({"message": message}), 200


@app.route("/payments/entry_fees", methods=["POST"])
def view_entry_fees_schedule():
    today = datetime.now()

    try:
        start_date = request.json["check_in_date"]
    except Exception:
        start_date = None

    try:
        end_date = request.json["check_out_date"]
    except Exception:
        end_date = None

    try:
        destination = request.json["destination"]
    except Exception:
        destination = None

    data = []
    if destination:
        get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
               .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
               .filter(MandatoryPaymentScheduleCalendar.schedule_destination == destination)\
               .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= request.json["check_in_date"])\
               .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= request.json["check_out_date"])\
               .first()
        data.append("Stage 1")

        if not get_schedule:
            get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
                   .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
                   .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= request.json["check_in_date"])\
                   .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= request.json["check_in_date"])\
                   .first()
            data.append("Stage 2")

    else:
        get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
               .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
               .filter(MandatoryPaymentScheduleCalendar.schedule_destination == "")\
               .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= request.json["check_in_date"])\
               .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= request.json["check_in_date"])\
               .first()
        data.append("Stage 3")

    if not get_schedule:
        message = []
        message.append("There are no pricing schedules set.")
        return jsonify({"message": message, "data": data}), 412

    else:
        data = []

        get_prices = db.session.query(MandatoryPaymentPrices)\
              .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
              .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                 MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                 Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                 Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                 Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
              .filter(MandatoryPaymentPrices.deletion_marker == None)\
              .filter(MandatoryPaymentPrices.payment_schedule == get_schedule.schedule_public_id)\
              .filter(Mandatory.deletion_marker == None)\
              .filter(Mandatory.payment_person != "Ea Resident Student")\
              .filter(Mandatory.payment_person != "Non Resident Student")\
              .filter(Mandatory.payment_person != "Citizen Student")\
              .order_by(Mandatory.payment_person.asc())\
              .all()

        for single in get_prices:
            return_data = {}
            return_data["payment_public_id"] = single.payment_public_id
            return_data[
                "payment_person_income_code"] = single.payment_person_income_code
            return_data[
                "payment_person_dept_code"] = single.payment_person_dept_code
            return_data["payment_person"] = single.payment_person.title()
            return_data["payment_schedule"] = single.payment_schedule

            try:
                currency = requests.get(
                    get_currency.format(single.payment_currency))
                return_data["payment_person_currency"] = currency.json(
                )["data"][0]["currency_name"]
                return_data[
                    "payment_person_currency_id"] = single.payment_currency
            except Exception:
                return_data["payment_person_currency"] = "N/A"
                return_data[
                    "payment_person_currency_id"] = single.payment_currency

            return_data[
                "payment_person_description"] = single.payment_person_description
            return_data["payment_person_amount"] = float(single.payment_price)
            return_data["payment_person_discount"] = 0
            return_data["payment_guests"] = 0
            return_data["marker"] = single.marker
            return_data["school"] = single.school
            return_data["amount"] = 0
            return_data["max_number"] = single.maximum_number
            return_data["session_id"] = single.session_id

            data.append(return_data)

        return jsonify({"data": data}), 200


@app.route("/payments/entry_fees/school", methods=["POST"])
def view_school_entry_fees_schedule():
    today = datetime.now()

    try:
        start_date = request.json["check_in_date"]
    except Exception:
        start_date = None

    try:
        end_date = request.json["check_out_date"]
    except Exception:
        end_date = None

    try:
        destination = request.json["destination"]
    except Exception:
        destination = None

    if destination:
        get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
               .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
               .filter(MandatoryPaymentScheduleCalendar.schedule_destination == destination)\
               .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= request.json["check_in_date"])\
               .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= request.json["check_out_date"])\
               .all()

        if not get_schedule:
            get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
                   .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
                   .filter(MandatoryPaymentScheduleCalendar.schedule_destination == "")\
                   .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= request.json["check_in_date"])\
                   .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= request.json["check_in_date"])\
                   .all()

    else:
        get_schedule = db.session.query(MandatoryPaymentScheduleCalendar)\
               .filter(MandatoryPaymentScheduleCalendar.deletion_marker == None)\
               .filter(MandatoryPaymentScheduleCalendar.schedule_destination == "")\
               .filter(MandatoryPaymentScheduleCalendar.schedule_start_date <= request.json["check_in_date"])\
               .filter(MandatoryPaymentScheduleCalendar.schedule_end_date >= request.json["check_in_date"])\
               .all()

    if not get_schedule:
        message = []
        message.append("There are no pricing schedules set.")
        return jsonify({"message": message}), 412

    else:
        data = []

        for single_schedule in get_schedule:
            get_prices = db.session.query(MandatoryPaymentPrices)\
                    .join(Mandatory, MandatoryPaymentPrices.payment_category == Mandatory.payment_public_id)\
                    .add_columns(MandatoryPaymentPrices.price_public_id, MandatoryPaymentPrices.payment_schedule, MandatoryPaymentPrices.payment_category,\
                        MandatoryPaymentPrices.payment_currency, MandatoryPaymentPrices.payment_price,\
                     Mandatory.payment_public_id, Mandatory.payment_person, Mandatory.payment_person_income_code,\
                     Mandatory.payment_person_dept_code, Mandatory.payment_person_description, Mandatory.marker,\
                     Mandatory.school, Mandatory.maximum_number, Mandatory.session_id)\
                    .filter(MandatoryPaymentPrices.deletion_marker == None)\
                    .filter(MandatoryPaymentPrices.payment_schedule == single_schedule.schedule_public_id)\
                    .filter(Mandatory.deletion_marker == None)\
                    .filter(Mandatory.school == "Y")\
                    .order_by(Mandatory.payment_person.asc())\
                    .all()

            for single in get_prices:
                return_data = {}
                return_data["payment_public_id"] = single.payment_public_id
                return_data[
                    "payment_person_income_code"] = single.payment_person_income_code
                return_data[
                    "payment_person_dept_code"] = single.payment_person_dept_code
                return_data["payment_person"] = single.payment_person.title()
                return_data["payment_schedule"] = single.payment_schedule

                try:
                    currency = requests.get(
                        get_currency.format(single.payment_currency))
                    return_data["payment_person_currency"] = currency.json(
                    )["data"][0]["currency_name"]
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency
                except Exception:
                    return_data["payment_person_currency"] = "N/A"
                    return_data[
                        "payment_person_currency_id"] = single.payment_currency

                return_data[
                    "payment_person_description"] = single.payment_person_description
                return_data["payment_person_amount"] = float(
                    single.payment_price)
                return_data["payment_person_discount"] = 0
                return_data["payment_guests"] = 0
                return_data["marker"] = single.marker
                return_data["school"] = single.school
                return_data["amount"] = 0
                return_data["max_number"] = single.maximum_number
                return_data["session_id"] = single.session_id

                data.append(return_data)

        return jsonify({"data": data}), 200
