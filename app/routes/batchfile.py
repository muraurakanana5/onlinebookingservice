from flask import Flask, jsonify, render_template, url_for, request, redirect, json, make_response
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid

from lxml import etree as ET

# File imports
from routes import app
from routes import db
from database.batchfile_code import BatchfileCode
from variables import get_single_user

import pandas as pd
from collections import OrderedDict

def close(self):
	self.session.close()


# rec=0
def autoIncrement():  
	get_current_code = db.session.query(BatchfileCode)\
								 .first()

	if get_current_code:
		pStart = int(get_current_code.batchfile_code)
	else:
		pStart = 10000000000000000 #adjust start value, if req'd   
	
	pInterval = 1 #adjust interval value, if req'd  
	
	rec = pStart + pInterval
	
	if not get_current_code:
		code = BatchfileCode(
			batchfile_code = rec
		)

		db.session.add(code)
	
	else:
		get_current_code.batchfile_code = rec

	db.session.commit()
	close(db)

	return rec


@app.route('/batchfile/post', methods = ["POST"])
def ui_generated_batchfile():
	"""
	Handling requests from the UI
	"""
	
	messages = []

	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	get_user = requests.get(get_single_user.format(request.json["session_id"]))

	if get_user.status_code != 200:
		message = []
		message.append("Not authorised to perform this operation")
		return jsonify({"message": message}), 422
	
	json_data = {
		"start_date": request.json["start_date"],
		"end_date": request.json["end_date"]
	}
	
	batch_data = requests.get('https://' + app.config['SERVER'] + ':5005/batchfile/generate/data', json = json_data) # production url
	incoming_data = batch_data.json()["data"]
	transaction_data_json = batch_data.json()["transation_data"]

	data = []

	batch_number = autoIncrement()
	print(batch_number)

	for content in incoming_data:
		response = OrderedDict()
		
		response['Account Code'] = content["income_account"]
		response['Accounting Period'] = ''
		# if len(content["sales_date"]) != 8:
		#     response['Transaction Date'] = "0" + str(content["sales_date"])
		# else:
		response['Transaction Date'] = str(content["sales_date"])
		
		response['Filler'] = ''
		response['Record Type'] = 'L'
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		# response['Base Amount'] = content["unit_price"]
		response['Base Amount'] = round(content["base_amount"] * content["tran_quantity"] * content["conversion_rate"], 2)
		# response['Base Amount'] = content["unit_price"] * content["tran_quantity"]
		response['D/C Marker'] = content["dc_marker"]
		response['Allocation Marker'] = ''
		response['Journal Type'] = content["journal_type"]
		response['Journal Source'] = ''
		response['Transaction Reference'] = content["customer_reference"]
		response['Description'] = content["account_description"]
		
		response['Entry Date'] = ''
		response['Entry Period'] = ''
		response['Due Date'] = ''
		response['Filler 1'] = ''
		response['Allocation Ref'] = ''
		response['Allocation Date'] = ''
		response['Allocation Period'] = ''
		response['Asset Indicator'] = ''
		response['Asset Code'] = ''
		response['Asset Subcode'] = ''

		
		response['Conversion Code'] = content["currency"]
		response['Conversion Rate'] = content["conversion_rate"]

		try:
			response['Transaction Amount'] = content["tran_quantity"] * content["base_amount"]
		except Exception:
			response['Transaction Amount'] = None

		response['Transaction Decimal Places'] = ''
		response['Cleardown Sequence Number'] = ''
		response['Filler 2'] = ''
		response['Reversal Flag'] = ''
		response['Loss Gain Flag'] = ''
		response['Rough Transaction Flag'] = ''
		response['Transaction in Use'] = ''
		
		# response['Outlet Analysis'] = content["outlet"]
		response['Analysis 1 Code'] = content["outlet"]
		response['Analysis 2 Code'] = content["department"]
		response['Analysis 3 Code'] = "#"
		response['Analysis 4 Code'] = "#"
		response['Analysis 5 Code'] = "#"
		response['Analysis 6 Code'] = "#"
		response['Analysis 7 Code'] = "#"
		response['Analysis 8 Code'] = "#"
		response['Analysis 9 Code'] = "#"
		response['Analysis 10 Code'] = "#"
		
		response['Posting Date for Rough Journals'] = ''
		response['Update Order Balance Indicator'] = ''
		response['Allocation in Progress Flag'] = ''
		response['Journal Hold Reference'] = ''
		response['Hold Operator ID'] = ''
		response['Budget Check Account'] = ''
		response['Conversion Rate from Pivot to Base'] = ''
		response['Operator for above'] = ''
		response['Operator for Transaction Rate'] = ''
		response['Conversion Rate from Pivot to Second Base'] = ''
		
		response['Operator for above 1'] = ''
		response['Report/Second Base Amount'] = ''
		response['Memo Amount'] = ''
		response['Exclude from Balancing in Revaluation'] = ''
		response['Filler 2'] = ''
		response['Ledger Extension Details Flag'] = ''
		response['Consumed Budget ID'] = ''
		response['Value 4 Currency Code'] = ''
		response['Value 4 Amount'] = ''
		response['Value 4 Conversion Rate'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Value 4 Operator 1'] = ''
		response['Value 4 Decimal Places'] = ''
		response['Value 5 Currency Code'] = ''
		response['journal_line_number'] = ''
		response['Value 5 Amount 1'] = ''
		response['Value 5 Conversion Rate 1'] = ''
		response['Value 5 Operator 2'] = ''
		response['Value 5 Decimal Places 2'] = ''
		response['Link Reference 1'] = ''
		response['Link Reference 2'] = ''
		
		response['Link Reference 3'] = ''
		response['Allocation Code 2'] = ''
		response['Allocation Statements Count'] = ''
		response['Allocation User ID 1'] = ''
		response['Original Line for this Split'] = ''
		response['Value Datetime'] = ''
		response['Signing Details'] = ''
		response['Installment Date'] = ''
		response['Principal Code'] = ''
		response['Principal Code 1'] = ''
		
		response['Principal Code 2'] = ''
		response['Principal Code 3'] = ''
		response['Principal Code 4'] = ''
		response['Principal Code 5'] = ''
		response['Principal Code 6'] = ''
		response['Principal Code 7'] = ''
		response['Principal Code 8'] = ''
		response['Principal Code 9'] = ''
		response['Principal Code 10'] = ''
		response['Principal Code 11'] = ''
		
		response['Principal Code 12'] = ''
		response['Principal Code 13'] = ''
		response['Principal Code 14'] = ''
		response['Principal Code 15'] = ''
		response['Principal Code 16'] = ''
		response['Principal Code 17'] = ''
		response['Principal Code 18'] = ''
		response['Principal Code 19'] = ''
		response['Binder Status'] = ''
		response['Agreed Status'] = ''
		
		response['Split Link Reference'] = ''
		response['Posting Reference'] = ''
		response['True Rated Flag'] = ''
		response['Date Held'] = ''
		response['Held Text'] = ''
		response['Instalment Number'] = ''
		response['Supplementary Extension Record Flag'] = ''
		response['Approvals Extension Flag'] = ''
		response['Revaluation Link Reference'] = ''
		response['Manual Payment Override Flag'] = ''
		
		response['Payment Stamp'] = ''
		response['Authorization in Progress'] = ''
		response['Split in Progress'] = ''
		response['Journal Line Number'] = ''
		response['Voucher Number / Second Reference'] = ''
		response['Journal Class Code'] = ''
		response['Originator Id'] = ''
		response['Originated Date and Time'] = ''
		response['Allocator Id'] = ''
		response['Journal Reversal Type'] = ''
		
		response['Document Date 1'] = ''
		response['Document Date 2'] = ''
		response['Document Date 3'] = ''
		response['Document Date 4'] = ''
		response['Document Prefix 1'] = ''
		response['Document Number 1'] = ''
		response['Document Prefix 2'] = ''
		response['Document Number 2'] = ''
		response['Document Prefix 3'] = ''
		response['Document Number 3'] = ''
		response['Document Prefix 4'] = ''
		response['Document Number 4'] = ''
		response['Discount 1 Date'] = ''
		response['Discount 1 Percentage'] = ''
		response['Discount 2 Date'] = ''
		response['Discount 2 Percentage'] = ''
		response['Interest Date'] = ''
		response['Interest Percentage'] = ''
		response['Late Payment Date'] = ''
		response['Late Payment Percentage'] = ''
		response['Payment Reference'] = ''
		response['Bank Code'] = ''
		response['Source Reference'] = ''
		response['Module Code'] = ''
		response['Payment Terms Code'] = ''
		
		
		response['General Description 1'] = int(batch_number)
		data.append(response)

	df = pd.DataFrame(data)
	# df.to_csv('{0}.csv'.format(batch_number), sep=",", header=False, index=False, encoding='utf-8')

	# batchfile_name = 'batchfile-{0}.csv'.format(datetime.now().strftime("%Y-%m-%d"))
	# df.to_csv(batchfile_name, sep=",", header=True, index=False, encoding='utf-8')
	df.to_csv("batchfiles/{0}.csv".format(batch_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")
	
	## Second Batchfile
	batchfile_number = int(str(int(str(batch_number)[0]) + 1) + str(batch_number)[1:]) # Generates another batch number in the format of 2xxxxxx
	transaction_data = []
	
	for transaction in transaction_data_json:
		response_data = OrderedDict()
		
		response_data['Account Code'] = transaction["income_account"]
		response_data['Accounting Period'] = ''
		response_data['Transaction Date'] = str(transaction["sales_date"])
		
		response_data['Filler'] = ''
		response_data['Record Type'] = 'L'
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Base Amount'] = round(transaction["tran_quantity"] * transaction["unit_price"] * transaction["conversion_rate"], 2)
		response_data['D/C Marker'] = transaction["dc_marker"]
		response_data['Allocation Marker'] = ''
		response_data['Journal Type'] = transaction["journal_type"]
		response_data['Journal Source'] = ''
		response_data['Transaction Reference'] = transaction["customer_reference"]
		response_data['Description'] = transaction["account_description"]
		
		response_data['Entry Date'] = ''
		response_data['Entry Period'] = ''
		response_data['Due Date'] = ''
		response_data['Filler 1'] = ''
		response_data['Allocation Ref'] = ''
		response_data['Allocation Date'] = ''
		response_data['Allocation Period'] = ''
		response_data['Asset Indicator'] = ''
		response_data['Asset Code'] = ''
		response_data['Asset Subcode'] = ''

		
		response_data['Conversion Code'] = transaction["currency"]
		response_data['Conversion Rate'] = transaction["conversion_rate"]

		try:
			response_data['Transaction Amount'] = transaction["tran_quantity"] * transaction["unit_price"]
		except Exception:
			response_data['Transaction Amount'] = None

		response_data['Transaction Decimal Places'] = ''
		response_data['Cleardown Sequence Number'] = ''
		response_data['Filler 2'] = ''
		response_data['Reversal Flag'] = ''
		response_data['Loss Gain Flag'] = ''
		response_data['Rough Transaction Flag'] = ''
		response_data['Transaction in Use'] = ''
		
		# response_data['Outlet Analysis'] = transaction["outlet"]
		# response_data['Analysis 1 Code'] = transaction["department"]
		# response_data['Outlet Analysis'] = "#"
		response_data['Analysis 1 Code'] = "#"
		response_data['Analysis 2 Code'] = "#"
		response_data['Analysis 3 Code'] = "#"
		response_data['Analysis 4 Code'] = "#"
		response_data['Analysis 5 Code'] = "#"
		response_data['Analysis 6 Code'] = "#"
		response_data['Analysis 7 Code'] = "#"
		response_data['Analysis 8 Code'] = "#"
		response_data['Analysis 9 Code'] = "#"
		response_data['Analysis 10 Code'] = "#"
		
		response_data['Posting Date for Rough Journals'] = ''
		response_data['Update Order Balance Indicator'] = ''
		response_data['Allocation in Progress Flag'] = ''
		response_data['Journal Hold Reference'] = ''
		response_data['Hold Operator ID'] = ''
		response_data['Budget Check Account'] = ''
		response_data['Conversion Rate from Pivot to Base'] = ''
		response_data['Operator for above'] = ''
		response_data['Operator for Transaction Rate'] = ''
		response_data['Conversion Rate from Pivot to Second Base'] = ''
		
		response_data['Operator for above 1'] = ''
		response_data['Report/Second Base Amount'] = ''
		response_data['Memo Amount'] = ''
		response_data['Exclude from Balancing in Revaluation'] = ''
		response_data['Filler 2'] = ''
		response_data['Ledger Extension Details Flag'] = ''
		response_data['Consumed Budget ID'] = ''
		response_data['Value 4 Currency Code'] = ''
		response_data['Value 4 Amount'] = ''
		response_data['Value 4 Conversion Rate'] = ''
		
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		
		response_data['Value 4 Operator 1'] = ''
		response_data['Value 4 Decimal Places'] = ''
		response_data['Value 5 Currency Code'] = ''
		response_data['journal_line_number'] = ''
		response_data['Value 5 Amount 1'] = ''
		response_data['Value 5 Conversion Rate 1'] = ''
		response_data['Value 5 Operator 2'] = ''
		response_data['Value 5 Decimal Places 2'] = ''
		response_data['Link Reference 1'] = ''
		response_data['Link Reference 2'] = ''
		
		response_data['Link Reference 3'] = ''
		response_data['Allocation Code 2'] = ''
		response_data['Allocation Statements Count'] = ''
		response_data['Allocation User ID 1'] = ''
		response_data['Original Line for this Split'] = ''
		response_data['Value Datetime'] = ''
		response_data['Signing Details'] = ''
		response_data['Installment Date'] = ''
		response_data['Principal Code'] = ''
		response_data['Principal Code 1'] = ''
		
		response_data['Principal Code 2'] = ''
		response_data['Principal Code 3'] = ''
		response_data['Principal Code 4'] = ''
		response_data['Principal Code 5'] = ''
		response_data['Principal Code 6'] = ''
		response_data['Principal Code 7'] = ''
		response_data['Principal Code 8'] = ''
		response_data['Principal Code 9'] = ''
		response_data['Principal Code 10'] = ''
		response_data['Principal Code 11'] = ''
		
		response_data['Principal Code 12'] = ''
		response_data['Principal Code 13'] = ''
		response_data['Principal Code 14'] = ''
		response_data['Principal Code 15'] = ''
		response_data['Principal Code 16'] = ''
		response_data['Principal Code 17'] = ''
		response_data['Principal Code 18'] = ''
		response_data['Principal Code 19'] = ''
		response_data['Binder Status'] = ''
		response_data['Agreed Status'] = ''
		
		response_data['Split Link Reference'] = ''
		response_data['Posting Reference'] = ''
		response_data['True Rated Flag'] = ''
		response_data['Date Held'] = ''
		response_data['Held Text'] = ''
		response_data['Instalment Number'] = ''
		response_data['Supplementary Extension Record Flag'] = ''
		response_data['Approvals Extension Flag'] = ''
		response_data['Revaluation Link Reference'] = ''
		response_data['Manual Payment Override Flag'] = ''
		
		response_data['Payment Stamp'] = ''
		response_data['Authorization in Progress'] = ''
		response_data['Split in Progress'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Voucher Number / Second Reference'] = ''
		response_data['Journal Class Code'] = ''
		response_data['Originator Id'] = ''
		response_data['Originated Date and Time'] = ''
		response_data['Allocator Id'] = ''
		response_data['Journal Reversal Type'] = ''
		
		response_data['Document Date 1'] = ''
		response_data['Document Date 2'] = ''
		response_data['Document Date 3'] = ''
		response_data['Document Date 4'] = ''
		response_data['Document Prefix 1'] = ''
		response_data['Document Number 1'] = ''
		response_data['Document Prefix 2'] = ''
		response_data['Document Number 2'] = ''
		response_data['Document Prefix 3'] = ''
		response_data['Document Number 3'] = ''
		response_data['Document Prefix 4'] = ''
		response_data['Document Number 4'] = ''
		response_data['Discount 1 Date'] = ''
		response_data['Discount 1 Percentage'] = ''
		response_data['Discount 2 Date'] = ''
		response_data['Discount 2 Percentage'] = ''
		response_data['Interest Date'] = ''
		response_data['Interest Percentage'] = ''
		response_data['Late Payment Date'] = ''
		response_data['Late Payment Percentage'] = ''
		response_data['Payment Reference'] = ''
		response_data['Bank Code'] = ''
		response_data['Source Reference'] = ''
		response_data['Module Code'] = ''
		response_data['Payment Terms Code'] = ''
		
		
		response_data['General Description 1'] = int(batchfile_number)
		transaction_data.append(response_data)

	transaction_df = pd.DataFrame(transaction_data)
	transaction_df.to_csv("batchfiles/{0}.csv".format(batchfile_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")

	
	responseObject = {
		"url_1" : "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batch_number),
		"url_2" : "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batchfile_number)
	}
	return make_response(jsonify(responseObject))


@app.route('/batchfile/data')
def createBatchfile():
	# batch_data = requests.get('https://' + app.config['SERVER'] + ':5005/batchfile/data/view') # production url
	batch_data = requests.get('https://' + app.config['SERVER'] + ':5005/batchfile/generate/data') # production url
	incoming_data = batch_data.json()["data"]
	transaction_data_json = batch_data.json()["transation_data"]

	# df = pd.DataFrame(incoming_data)

	data = []

	batch_number = autoIncrement()
	print(batch_number)

	for content in incoming_data:
		response = OrderedDict()
		
		response['Account Code'] = content["income_account"]
		response['Accounting Period'] = ''
		# if len(content["sales_date"]) != 8:
		#     response['Transaction Date'] = "0" + str(content["sales_date"])
		# else:
		response['Transaction Date'] = str(content["sales_date"])
		
		response['Filler'] = ''
		response['Record Type'] = 'L'
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		# response['Base Amount'] = content["unit_price"]
		response['Base Amount'] = round(content["base_amount"] * content["tran_quantity"] * content["conversion_rate"], 2)
		# response['Base Amount'] = content["unit_price"] * content["tran_quantity"]
		response['D/C Marker'] = content["dc_marker"]
		response['Allocation Marker'] = ''
		response['Journal Type'] = content["journal_type"]
		response['Journal Source'] = ''
		response['Transaction Reference'] = content["customer_reference"]
		response['Description'] = content["account_description"]
		
		response['Entry Date'] = ''
		response['Entry Period'] = ''
		response['Due Date'] = ''
		response['Filler 1'] = ''
		response['Allocation Ref'] = ''
		response['Allocation Date'] = ''
		response['Allocation Period'] = ''
		response['Asset Indicator'] = ''
		response['Asset Code'] = ''
		response['Asset Subcode'] = ''

		
		response['Conversion Code'] = content["currency"]
		response['Conversion Rate'] = content["conversion_rate"]

		try:
			response['Transaction Amount'] = content["tran_quantity"] * content["base_amount"]
		except Exception:
			response['Transaction Amount'] = None

		response['Transaction Decimal Places'] = ''
		response['Cleardown Sequence Number'] = ''
		response['Filler 2'] = ''
		response['Reversal Flag'] = ''
		response['Loss Gain Flag'] = ''
		response['Rough Transaction Flag'] = ''
		response['Transaction in Use'] = ''
		
		# response['Outlet Analysis'] = content["outlet"]
		response['Analysis 1 Code'] = content["outlet"]
		response['Analysis 2 Code'] = content["department"]
		response['Analysis 3 Code'] = "#"
		response['Analysis 4 Code'] = "#"
		response['Analysis 5 Code'] = "#"
		response['Analysis 6 Code'] = "#"
		response['Analysis 7 Code'] = "#"
		response['Analysis 8 Code'] = "#"
		response['Analysis 9 Code'] = "#"
		response['Analysis 10 Code'] = "#"
		
		response['Posting Date for Rough Journals'] = ''
		response['Update Order Balance Indicator'] = ''
		response['Allocation in Progress Flag'] = ''
		response['Journal Hold Reference'] = ''
		response['Hold Operator ID'] = ''
		response['Budget Check Account'] = ''
		response['Conversion Rate from Pivot to Base'] = ''
		response['Operator for above'] = ''
		response['Operator for Transaction Rate'] = ''
		response['Conversion Rate from Pivot to Second Base'] = ''
		
		response['Operator for above 1'] = ''
		response['Report/Second Base Amount'] = ''
		response['Memo Amount'] = ''
		response['Exclude from Balancing in Revaluation'] = ''
		response['Filler 2'] = ''
		response['Ledger Extension Details Flag'] = ''
		response['Consumed Budget ID'] = ''
		response['Value 4 Currency Code'] = ''
		response['Value 4 Amount'] = ''
		response['Value 4 Conversion Rate'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Value 4 Operator 1'] = ''
		response['Value 4 Decimal Places'] = ''
		response['Value 5 Currency Code'] = ''
		response['journal_line_number'] = ''
		response['Value 5 Amount 1'] = ''
		response['Value 5 Conversion Rate 1'] = ''
		response['Value 5 Operator 2'] = ''
		response['Value 5 Decimal Places 2'] = ''
		response['Link Reference 1'] = ''
		response['Link Reference 2'] = ''
		
		response['Link Reference 3'] = ''
		response['Allocation Code 2'] = ''
		response['Allocation Statements Count'] = ''
		response['Allocation User ID 1'] = ''
		response['Original Line for this Split'] = ''
		response['Value Datetime'] = ''
		response['Signing Details'] = ''
		response['Installment Date'] = ''
		response['Principal Code'] = ''
		response['Principal Code 1'] = ''
		
		response['Principal Code 2'] = ''
		response['Principal Code 3'] = ''
		response['Principal Code 4'] = ''
		response['Principal Code 5'] = ''
		response['Principal Code 6'] = ''
		response['Principal Code 7'] = ''
		response['Principal Code 8'] = ''
		response['Principal Code 9'] = ''
		response['Principal Code 10'] = ''
		response['Principal Code 11'] = ''
		
		response['Principal Code 12'] = ''
		response['Principal Code 13'] = ''
		response['Principal Code 14'] = ''
		response['Principal Code 15'] = ''
		response['Principal Code 16'] = ''
		response['Principal Code 17'] = ''
		response['Principal Code 18'] = ''
		response['Principal Code 19'] = ''
		response['Binder Status'] = ''
		response['Agreed Status'] = ''
		
		response['Split Link Reference'] = ''
		response['Posting Reference'] = ''
		response['True Rated Flag'] = ''
		response['Date Held'] = ''
		response['Held Text'] = ''
		response['Instalment Number'] = ''
		response['Supplementary Extension Record Flag'] = ''
		response['Approvals Extension Flag'] = ''
		response['Revaluation Link Reference'] = ''
		response['Manual Payment Override Flag'] = ''
		
		response['Payment Stamp'] = ''
		response['Authorization in Progress'] = ''
		response['Split in Progress'] = ''
		response['Journal Line Number'] = ''
		response['Voucher Number / Second Reference'] = ''
		response['Journal Class Code'] = ''
		response['Originator Id'] = ''
		response['Originated Date and Time'] = ''
		response['Allocator Id'] = ''
		response['Journal Reversal Type'] = ''
		
		response['Document Date 1'] = ''
		response['Document Date 2'] = ''
		response['Document Date 3'] = ''
		response['Document Date 4'] = ''
		response['Document Prefix 1'] = ''
		response['Document Number 1'] = ''
		response['Document Prefix 2'] = ''
		response['Document Number 2'] = ''
		response['Document Prefix 3'] = ''
		response['Document Number 3'] = ''
		response['Document Prefix 4'] = ''
		response['Document Number 4'] = ''
		response['Discount 1 Date'] = ''
		response['Discount 1 Percentage'] = ''
		response['Discount 2 Date'] = ''
		response['Discount 2 Percentage'] = ''
		response['Interest Date'] = ''
		response['Interest Percentage'] = ''
		response['Late Payment Date'] = ''
		response['Late Payment Percentage'] = ''
		response['Payment Reference'] = ''
		response['Bank Code'] = ''
		response['Source Reference'] = ''
		response['Module Code'] = ''
		response['Payment Terms Code'] = ''
		
		
		response['General Description 1'] = int(batch_number)
		data.append(response)

	df = pd.DataFrame(data)
	# df.to_csv('{0}.csv'.format(batch_number), sep=",", header=False, index=False, encoding='utf-8')

	# batchfile_name = 'batchfile-{0}.csv'.format(datetime.now().strftime("%Y-%m-%d"))
	# df.to_csv(batchfile_name, sep=",", header=True, index=False, encoding='utf-8')
	df.to_csv("batchfiles/{0}.csv".format(batch_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")
	
	## Second Batchfile
	batchfile_number = int(str(int(str(batch_number)[0]) + 1) + str(batch_number)[1:]) # Generates another batch number in the format of 2xxxxxx
	transaction_data = []
	
	for transaction in transaction_data_json:
		response_data = OrderedDict()
		
		response_data['Account Code'] = transaction["income_account"]
		response_data['Accounting Period'] = ''
		response_data['Transaction Date'] = str(transaction["sales_date"])
		
		response_data['Filler'] = ''
		response_data['Record Type'] = 'L'
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Base Amount'] = round(transaction["tran_quantity"] * transaction["unit_price"] * transaction["conversion_rate"], 2)
		response_data['D/C Marker'] = transaction["dc_marker"]
		response_data['Allocation Marker'] = ''
		response_data['Journal Type'] = transaction["journal_type"]
		response_data['Journal Source'] = ''
		response_data['Transaction Reference'] = transaction["customer_reference"]
		response_data['Description'] = transaction["account_description"]
		
		response_data['Entry Date'] = ''
		response_data['Entry Period'] = ''
		response_data['Due Date'] = ''
		response_data['Filler 1'] = ''
		response_data['Allocation Ref'] = ''
		response_data['Allocation Date'] = ''
		response_data['Allocation Period'] = ''
		response_data['Asset Indicator'] = ''
		response_data['Asset Code'] = ''
		response_data['Asset Subcode'] = ''

		
		response_data['Conversion Code'] = transaction["currency"]
		response_data['Conversion Rate'] = transaction["conversion_rate"]

		try:
			response_data['Transaction Amount'] = transaction["tran_quantity"] * transaction["unit_price"]
		except Exception:
			response_data['Transaction Amount'] = None

		response_data['Transaction Decimal Places'] = ''
		response_data['Cleardown Sequence Number'] = ''
		response_data['Filler 2'] = ''
		response_data['Reversal Flag'] = ''
		response_data['Loss Gain Flag'] = ''
		response_data['Rough Transaction Flag'] = ''
		response_data['Transaction in Use'] = ''
		
		# response_data['Outlet Analysis'] = transaction["outlet"]
		# response_data['Analysis 1 Code'] = transaction["department"]
		# response_data['Outlet Analysis'] = "#"
		response_data['Analysis 1 Code'] = "#"
		response_data['Analysis 2 Code'] = "#"
		response_data['Analysis 3 Code'] = "#"
		response_data['Analysis 4 Code'] = "#"
		response_data['Analysis 5 Code'] = "#"
		response_data['Analysis 6 Code'] = "#"
		response_data['Analysis 7 Code'] = "#"
		response_data['Analysis 8 Code'] = "#"
		response_data['Analysis 9 Code'] = "#"
		response_data['Analysis 10 Code'] = "#"
		
		response_data['Posting Date for Rough Journals'] = ''
		response_data['Update Order Balance Indicator'] = ''
		response_data['Allocation in Progress Flag'] = ''
		response_data['Journal Hold Reference'] = ''
		response_data['Hold Operator ID'] = ''
		response_data['Budget Check Account'] = ''
		response_data['Conversion Rate from Pivot to Base'] = ''
		response_data['Operator for above'] = ''
		response_data['Operator for Transaction Rate'] = ''
		response_data['Conversion Rate from Pivot to Second Base'] = ''
		
		response_data['Operator for above 1'] = ''
		response_data['Report/Second Base Amount'] = ''
		response_data['Memo Amount'] = ''
		response_data['Exclude from Balancing in Revaluation'] = ''
		response_data['Filler 2'] = ''
		response_data['Ledger Extension Details Flag'] = ''
		response_data['Consumed Budget ID'] = ''
		response_data['Value 4 Currency Code'] = ''
		response_data['Value 4 Amount'] = ''
		response_data['Value 4 Conversion Rate'] = ''
		
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		
		response_data['Value 4 Operator 1'] = ''
		response_data['Value 4 Decimal Places'] = ''
		response_data['Value 5 Currency Code'] = ''
		response_data['journal_line_number'] = ''
		response_data['Value 5 Amount 1'] = ''
		response_data['Value 5 Conversion Rate 1'] = ''
		response_data['Value 5 Operator 2'] = ''
		response_data['Value 5 Decimal Places 2'] = ''
		response_data['Link Reference 1'] = ''
		response_data['Link Reference 2'] = ''
		
		response_data['Link Reference 3'] = ''
		response_data['Allocation Code 2'] = ''
		response_data['Allocation Statements Count'] = ''
		response_data['Allocation User ID 1'] = ''
		response_data['Original Line for this Split'] = ''
		response_data['Value Datetime'] = ''
		response_data['Signing Details'] = ''
		response_data['Installment Date'] = ''
		response_data['Principal Code'] = ''
		response_data['Principal Code 1'] = ''
		
		response_data['Principal Code 2'] = ''
		response_data['Principal Code 3'] = ''
		response_data['Principal Code 4'] = ''
		response_data['Principal Code 5'] = ''
		response_data['Principal Code 6'] = ''
		response_data['Principal Code 7'] = ''
		response_data['Principal Code 8'] = ''
		response_data['Principal Code 9'] = ''
		response_data['Principal Code 10'] = ''
		response_data['Principal Code 11'] = ''
		
		response_data['Principal Code 12'] = ''
		response_data['Principal Code 13'] = ''
		response_data['Principal Code 14'] = ''
		response_data['Principal Code 15'] = ''
		response_data['Principal Code 16'] = ''
		response_data['Principal Code 17'] = ''
		response_data['Principal Code 18'] = ''
		response_data['Principal Code 19'] = ''
		response_data['Binder Status'] = ''
		response_data['Agreed Status'] = ''
		
		response_data['Split Link Reference'] = ''
		response_data['Posting Reference'] = ''
		response_data['True Rated Flag'] = ''
		response_data['Date Held'] = ''
		response_data['Held Text'] = ''
		response_data['Instalment Number'] = ''
		response_data['Supplementary Extension Record Flag'] = ''
		response_data['Approvals Extension Flag'] = ''
		response_data['Revaluation Link Reference'] = ''
		response_data['Manual Payment Override Flag'] = ''
		
		response_data['Payment Stamp'] = ''
		response_data['Authorization in Progress'] = ''
		response_data['Split in Progress'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Voucher Number / Second Reference'] = ''
		response_data['Journal Class Code'] = ''
		response_data['Originator Id'] = ''
		response_data['Originated Date and Time'] = ''
		response_data['Allocator Id'] = ''
		response_data['Journal Reversal Type'] = ''
		
		response_data['Document Date 1'] = ''
		response_data['Document Date 2'] = ''
		response_data['Document Date 3'] = ''
		response_data['Document Date 4'] = ''
		response_data['Document Prefix 1'] = ''
		response_data['Document Number 1'] = ''
		response_data['Document Prefix 2'] = ''
		response_data['Document Number 2'] = ''
		response_data['Document Prefix 3'] = ''
		response_data['Document Number 3'] = ''
		response_data['Document Prefix 4'] = ''
		response_data['Document Number 4'] = ''
		response_data['Discount 1 Date'] = ''
		response_data['Discount 1 Percentage'] = ''
		response_data['Discount 2 Date'] = ''
		response_data['Discount 2 Percentage'] = ''
		response_data['Interest Date'] = ''
		response_data['Interest Percentage'] = ''
		response_data['Late Payment Date'] = ''
		response_data['Late Payment Percentage'] = ''
		response_data['Payment Reference'] = ''
		response_data['Bank Code'] = ''
		response_data['Source Reference'] = ''
		response_data['Module Code'] = ''
		response_data['Payment Terms Code'] = ''
		
		
		response_data['General Description 1'] = int(batchfile_number)
		transaction_data.append(response_data)

	transaction_df = pd.DataFrame(transaction_data)
	transaction_df.to_csv("batchfiles/{0}.csv".format(batchfile_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")

	
	responseObject = {
		"url_1" : "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batch_number),
		"url_2" : "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batchfile_number)
	}
	return make_response(jsonify(responseObject))


@app.route('/batchfile-credit/data')
def createCreditBatchfile():
	batch_data = requests.get('https://' + app.config['SERVER'] + ':5005/credit/batchfile') # production url
	incoming_data = batch_data.json()["data"]

	# df = pd.DataFrame(incoming_data)

	data = []

	get_current_code = db.session.query(BatchfileCode)\
								 .first()

	batch_number = int(get_current_code.batchfile_code)

	batchfile_number = int(str(int(str(batch_number)[0]) + 2) + str(batch_number)[1:]) # Generates another batch number in the format of 3xxxxxx

	batch_number = batchfile_number

	for content in incoming_data:
		response = OrderedDict()
		
		response['Account Code'] = content["income_account"]
		response['Accounting Period'] = ''
		# if len(content["sales_date"]) != 8:
		#     response['Transaction Date'] = "0" + str(content["sales_date"])
		# else:
		response['Transaction Date'] = str(content["sales_date"])
		
		response['Filler'] = ''
		response['Record Type'] = 'L'
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		# response['Base Amount'] = content["unit_price"]
		response['Base Amount'] = round(content["base_amount"] * content["tran_quantity"] * content["conversion_rate"], 2)
		# response['Base Amount'] = content["unit_price"] * content["tran_quantity"]
		response['D/C Marker'] = content["dc_marker"]
		response['Allocation Marker'] = ''
		response['Journal Type'] = content["journal_type"]
		response['Journal Source'] = ''
		response['Transaction Reference'] = content["customer_reference"]
		response['Description'] = content["account_description"]
		
		response['Entry Date'] = ''
		response['Entry Period'] = ''
		response['Due Date'] = ''
		response['Filler 1'] = ''
		response['Allocation Ref'] = ''
		response['Allocation Date'] = ''
		response['Allocation Period'] = ''
		response['Asset Indicator'] = ''
		response['Asset Code'] = ''
		response['Asset Subcode'] = ''

		
		response['Conversion Code'] = content["currency"]
		response['Conversion Rate'] = content["conversion_rate"]

		try:
			response['Transaction Amount'] = content["tran_quantity"] * content["base_amount"]
		except Exception:
			response['Transaction Amount'] = None

		response['Transaction Decimal Places'] = ''
		response['Cleardown Sequence Number'] = ''
		response['Filler 2'] = ''
		response['Reversal Flag'] = ''
		response['Loss Gain Flag'] = ''
		response['Rough Transaction Flag'] = ''
		response['Transaction in Use'] = ''
		
		# response['Outlet Analysis'] = content["outlet"]
		response['Analysis 1 Code'] = content["outlet"]
		response['Analysis 2 Code'] = content["department"]
		response['Analysis 3 Code'] = "#"
		response['Analysis 4 Code'] = "#"
		response['Analysis 5 Code'] = "#"
		response['Analysis 6 Code'] = "#"
		response['Analysis 7 Code'] = "#"
		response['Analysis 8 Code'] = "#"
		response['Analysis 9 Code'] = "#"
		response['Analysis 10 Code'] = "#"
		
		response['Posting Date for Rough Journals'] = ''
		response['Update Order Balance Indicator'] = ''
		response['Allocation in Progress Flag'] = ''
		response['Journal Hold Reference'] = ''
		response['Hold Operator ID'] = ''
		response['Budget Check Account'] = ''
		response['Conversion Rate from Pivot to Base'] = ''
		response['Operator for above'] = ''
		response['Operator for Transaction Rate'] = ''
		response['Conversion Rate from Pivot to Second Base'] = ''
		
		response['Operator for above 1'] = ''
		response['Report/Second Base Amount'] = ''
		response['Memo Amount'] = ''
		response['Exclude from Balancing in Revaluation'] = ''
		response['Filler 2'] = ''
		response['Ledger Extension Details Flag'] = ''
		response['Consumed Budget ID'] = ''
		response['Value 4 Currency Code'] = ''
		response['Value 4 Amount'] = ''
		response['Value 4 Conversion Rate'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Value 4 Operator 1'] = ''
		response['Value 4 Decimal Places'] = ''
		response['Value 5 Currency Code'] = ''
		response['journal_line_number'] = ''
		response['Value 5 Amount 1'] = ''
		response['Value 5 Conversion Rate 1'] = ''
		response['Value 5 Operator 2'] = ''
		response['Value 5 Decimal Places 2'] = ''
		response['Link Reference 1'] = ''
		response['Link Reference 2'] = ''
		
		response['Link Reference 3'] = ''
		response['Allocation Code 2'] = ''
		response['Allocation Statements Count'] = ''
		response['Allocation User ID 1'] = ''
		response['Original Line for this Split'] = ''
		response['Value Datetime'] = ''
		response['Signing Details'] = ''
		response['Installment Date'] = ''
		response['Principal Code'] = ''
		response['Principal Code 1'] = ''
		
		response['Principal Code 2'] = ''
		response['Principal Code 3'] = ''
		response['Principal Code 4'] = ''
		response['Principal Code 5'] = ''
		response['Principal Code 6'] = ''
		response['Principal Code 7'] = ''
		response['Principal Code 8'] = ''
		response['Principal Code 9'] = ''
		response['Principal Code 10'] = ''
		response['Principal Code 11'] = ''
		
		response['Principal Code 12'] = ''
		response['Principal Code 13'] = ''
		response['Principal Code 14'] = ''
		response['Principal Code 15'] = ''
		response['Principal Code 16'] = ''
		response['Principal Code 17'] = ''
		response['Principal Code 18'] = ''
		response['Principal Code 19'] = ''
		response['Binder Status'] = ''
		response['Agreed Status'] = ''
		
		response['Split Link Reference'] = ''
		response['Posting Reference'] = ''
		response['True Rated Flag'] = ''
		response['Date Held'] = ''
		response['Held Text'] = ''
		response['Instalment Number'] = ''
		response['Supplementary Extension Record Flag'] = ''
		response['Approvals Extension Flag'] = ''
		response['Revaluation Link Reference'] = ''
		response['Manual Payment Override Flag'] = ''
		
		response['Payment Stamp'] = ''
		response['Authorization in Progress'] = ''
		response['Split in Progress'] = ''
		response['Journal Line Number'] = ''
		response['Voucher Number / Second Reference'] = ''
		response['Journal Class Code'] = ''
		response['Originator Id'] = ''
		response['Originated Date and Time'] = ''
		response['Allocator Id'] = ''
		response['Journal Reversal Type'] = ''
		
		response['Document Date 1'] = ''
		response['Document Date 2'] = ''
		response['Document Date 3'] = ''
		response['Document Date 4'] = ''
		response['Document Prefix 1'] = ''
		response['Document Number 1'] = ''
		response['Document Prefix 2'] = ''
		response['Document Number 2'] = ''
		response['Document Prefix 3'] = ''
		response['Document Number 3'] = ''
		response['Document Prefix 4'] = ''
		response['Document Number 4'] = ''
		response['Discount 1 Date'] = ''
		response['Discount 1 Percentage'] = ''
		response['Discount 2 Date'] = ''
		response['Discount 2 Percentage'] = ''
		response['Interest Date'] = ''
		response['Interest Percentage'] = ''
		response['Late Payment Date'] = ''
		response['Late Payment Percentage'] = ''
		response['Payment Reference'] = ''
		response['Bank Code'] = ''
		response['Source Reference'] = ''
		response['Module Code'] = ''
		response['Payment Terms Code'] = ''
		
		
		response['General Description 1'] = int(batch_number)
		data.append(response)

	df = pd.DataFrame(data)

	df.to_csv("batchfiles/{0}.csv".format(batch_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")
	
	responseObject = {
		"url_1" : "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batch_number)
	}
	return make_response(jsonify(responseObject))


@app.route('/payment-credit/batchfile/data')
def createPaymentsCreditBatchfile():
	"""
	Used to clear up certain issues that are made in payments received
	"""
	batch_data = requests.get('https://' + app.config['SERVER'] + ':5005/payments/credit/batchfile') # production url
	incoming_data = batch_data.json()["data"]

	# df = pd.DataFrame(incoming_data)

	data = []

	get_current_code = db.session.query(BatchfileCode)\
								 .first()

	batch_number = int(get_current_code.batchfile_code)

	batchfile_number = int(str(int(str(batch_number)[0]) + 3) + str(batch_number)[1:]) # Generates another batch number in the format of 4xxxxxx

	batch_number = batchfile_number

	for transaction in incoming_data:
		response_data = OrderedDict()
		
		response_data['Account Code'] = transaction["income_account"]
		response_data['Accounting Period'] = ''
		response_data['Transaction Date'] = str(transaction["sales_date"])
		
		response_data['Filler'] = ''
		response_data['Record Type'] = 'L'
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Base Amount'] = round(transaction["tran_quantity"] * transaction["unit_price"] * transaction["conversion_rate"], 2)
		response_data['D/C Marker'] = transaction["dc_marker"]
		response_data['Allocation Marker'] = ''
		response_data['Journal Type'] = transaction["journal_type"]
		response_data['Journal Source'] = ''
		response_data['Transaction Reference'] = transaction["customer_reference"]
		response_data['Description'] = transaction["account_description"]
		
		response_data['Entry Date'] = ''
		response_data['Entry Period'] = ''
		response_data['Due Date'] = ''
		response_data['Filler 1'] = ''
		response_data['Allocation Ref'] = ''
		response_data['Allocation Date'] = ''
		response_data['Allocation Period'] = ''
		response_data['Asset Indicator'] = ''
		response_data['Asset Code'] = ''
		response_data['Asset Subcode'] = ''

		
		response_data['Conversion Code'] = transaction["currency"]
		response_data['Conversion Rate'] = transaction["conversion_rate"]

		try:
			response_data['Transaction Amount'] = transaction["tran_quantity"] * transaction["unit_price"]
		except Exception:
			response_data['Transaction Amount'] = None

		response_data['Transaction Decimal Places'] = ''
		response_data['Cleardown Sequence Number'] = ''
		response_data['Filler 2'] = ''
		response_data['Reversal Flag'] = ''
		response_data['Loss Gain Flag'] = ''
		response_data['Rough Transaction Flag'] = ''
		response_data['Transaction in Use'] = ''
		
		# response_data['Outlet Analysis'] = transaction["outlet"]
		# response_data['Analysis 1 Code'] = transaction["department"]
		# response_data['Outlet Analysis'] = "#"
		response_data['Analysis 1 Code'] = "#"
		response_data['Analysis 2 Code'] = "#"
		response_data['Analysis 3 Code'] = "#"
		response_data['Analysis 4 Code'] = "#"
		response_data['Analysis 5 Code'] = "#"
		response_data['Analysis 6 Code'] = "#"
		response_data['Analysis 7 Code'] = "#"
		response_data['Analysis 8 Code'] = "#"
		response_data['Analysis 9 Code'] = "#"
		response_data['Analysis 10 Code'] = "#"
		
		response_data['Posting Date for Rough Journals'] = ''
		response_data['Update Order Balance Indicator'] = ''
		response_data['Allocation in Progress Flag'] = ''
		response_data['Journal Hold Reference'] = ''
		response_data['Hold Operator ID'] = ''
		response_data['Budget Check Account'] = ''
		response_data['Conversion Rate from Pivot to Base'] = ''
		response_data['Operator for above'] = ''
		response_data['Operator for Transaction Rate'] = ''
		response_data['Conversion Rate from Pivot to Second Base'] = ''
		
		response_data['Operator for above 1'] = ''
		response_data['Report/Second Base Amount'] = ''
		response_data['Memo Amount'] = ''
		response_data['Exclude from Balancing in Revaluation'] = ''
		response_data['Filler 2'] = ''
		response_data['Ledger Extension Details Flag'] = ''
		response_data['Consumed Budget ID'] = ''
		response_data['Value 4 Currency Code'] = ''
		response_data['Value 4 Amount'] = ''
		response_data['Value 4 Conversion Rate'] = ''
		
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Filler'] = ''
		response_data['Record Type'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Journal Number'] = ''
		response_data['Journal Line Number'] = ''
		
		response_data['Value 4 Operator 1'] = ''
		response_data['Value 4 Decimal Places'] = ''
		response_data['Value 5 Currency Code'] = ''
		response_data['journal_line_number'] = ''
		response_data['Value 5 Amount 1'] = ''
		response_data['Value 5 Conversion Rate 1'] = ''
		response_data['Value 5 Operator 2'] = ''
		response_data['Value 5 Decimal Places 2'] = ''
		response_data['Link Reference 1'] = ''
		response_data['Link Reference 2'] = ''
		
		response_data['Link Reference 3'] = ''
		response_data['Allocation Code 2'] = ''
		response_data['Allocation Statements Count'] = ''
		response_data['Allocation User ID 1'] = ''
		response_data['Original Line for this Split'] = ''
		response_data['Value Datetime'] = ''
		response_data['Signing Details'] = ''
		response_data['Installment Date'] = ''
		response_data['Principal Code'] = ''
		response_data['Principal Code 1'] = ''
		
		response_data['Principal Code 2'] = ''
		response_data['Principal Code 3'] = ''
		response_data['Principal Code 4'] = ''
		response_data['Principal Code 5'] = ''
		response_data['Principal Code 6'] = ''
		response_data['Principal Code 7'] = ''
		response_data['Principal Code 8'] = ''
		response_data['Principal Code 9'] = ''
		response_data['Principal Code 10'] = ''
		response_data['Principal Code 11'] = ''
		
		response_data['Principal Code 12'] = ''
		response_data['Principal Code 13'] = ''
		response_data['Principal Code 14'] = ''
		response_data['Principal Code 15'] = ''
		response_data['Principal Code 16'] = ''
		response_data['Principal Code 17'] = ''
		response_data['Principal Code 18'] = ''
		response_data['Principal Code 19'] = ''
		response_data['Binder Status'] = ''
		response_data['Agreed Status'] = ''
		
		response_data['Split Link Reference'] = ''
		response_data['Posting Reference'] = ''
		response_data['True Rated Flag'] = ''
		response_data['Date Held'] = ''
		response_data['Held Text'] = ''
		response_data['Instalment Number'] = ''
		response_data['Supplementary Extension Record Flag'] = ''
		response_data['Approvals Extension Flag'] = ''
		response_data['Revaluation Link Reference'] = ''
		response_data['Manual Payment Override Flag'] = ''
		
		response_data['Payment Stamp'] = ''
		response_data['Authorization in Progress'] = ''
		response_data['Split in Progress'] = ''
		response_data['Journal Line Number'] = ''
		response_data['Voucher Number / Second Reference'] = ''
		response_data['Journal Class Code'] = ''
		response_data['Originator Id'] = ''
		response_data['Originated Date and Time'] = ''
		response_data['Allocator Id'] = ''
		response_data['Journal Reversal Type'] = ''
		
		response_data['Document Date 1'] = ''
		response_data['Document Date 2'] = ''
		response_data['Document Date 3'] = ''
		response_data['Document Date 4'] = ''
		response_data['Document Prefix 1'] = ''
		response_data['Document Number 1'] = ''
		response_data['Document Prefix 2'] = ''
		response_data['Document Number 2'] = ''
		response_data['Document Prefix 3'] = ''
		response_data['Document Number 3'] = ''
		response_data['Document Prefix 4'] = ''
		response_data['Document Number 4'] = ''
		response_data['Discount 1 Date'] = ''
		response_data['Discount 1 Percentage'] = ''
		response_data['Discount 2 Date'] = ''
		response_data['Discount 2 Percentage'] = ''
		response_data['Interest Date'] = ''
		response_data['Interest Percentage'] = ''
		response_data['Late Payment Date'] = ''
		response_data['Late Payment Percentage'] = ''
		response_data['Payment Reference'] = ''
		response_data['Bank Code'] = ''
		response_data['Source Reference'] = ''
		response_data['Module Code'] = ''
		response_data['Payment Terms Code'] = ''
		
		
		response_data['General Description 1'] = int(batchfile_number)
		data.append(response_data)

	df = pd.DataFrame(data)

	df.to_csv("batchfiles/{0}.csv".format(batch_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")
	
	responseObject = {
		"url_1" : "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batch_number)
	}
	return make_response(jsonify(responseObject))


@app.route("/credit-notes/batchfile", methods = ["POST"])
def generate_credit_note_file():
	"""
	The actual credit not batchfile function
	"""
	get_current_code = db.session.query(BatchfileCode)\
								 .first()

	batch_number = int(get_current_code.batchfile_code)

	batchfile_number = int(str(int(str(batch_number)[0]) + 2) + str(batch_number)[1:]) # Generates another batch number in the format of 3xxxxxx

	batch_number = batchfile_number
	
	json_data = {
		"start_date": request.json["start_date"],
		"end_date": request.json["end_date"]
	}

	batch_data = requests.post('https://' + app.config['SERVER'] + ':5005/credit-notes/data', json = json_data)
	incoming_data = batch_data.json()["data"]

	data = []
	
	for content in incoming_data:
		response = OrderedDict()
		
		response['Account Code'] = content["income_account"]
		response['Accounting Period'] = ''
		# if len(content["sales_date"]) != 8:
		#     response['Transaction Date'] = "0" + str(content["sales_date"])
		# else:
		response['Transaction Date'] = str(content["sales_date"])
		
		response['Filler'] = ''
		response['Record Type'] = 'L'
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		# response['Base Amount'] = content["unit_price"]
		response['Base Amount'] = round(content["base_amount"] * content["tran_quantity"] * content["conversion_rate"], 2)
		# response['Base Amount'] = content["unit_price"] * content["tran_quantity"]
		response['D/C Marker'] = content["dc_marker"]
		response['Allocation Marker'] = ''
		response['Journal Type'] = content["journal_type"]
		response['Journal Source'] = ''
		response['Transaction Reference'] = content["customer_reference"]
		response['Description'] = content["account_description"]
		
		response['Entry Date'] = ''
		response['Entry Period'] = ''
		response['Due Date'] = ''
		response['Filler 1'] = ''
		response['Allocation Ref'] = ''
		response['Allocation Date'] = ''
		response['Allocation Period'] = ''
		response['Asset Indicator'] = ''
		response['Asset Code'] = ''
		response['Asset Subcode'] = ''

		
		response['Conversion Code'] = content["currency"]
		response['Conversion Rate'] = content["conversion_rate"]

		try:
			response['Transaction Amount'] = content["tran_quantity"] * content["base_amount"]
		except Exception:
			response['Transaction Amount'] = None

		response['Transaction Decimal Places'] = ''
		response['Cleardown Sequence Number'] = ''
		response['Filler 2'] = ''
		response['Reversal Flag'] = ''
		response['Loss Gain Flag'] = ''
		response['Rough Transaction Flag'] = ''
		response['Transaction in Use'] = ''
		
		# response['Outlet Analysis'] = content["outlet"]
		response['Analysis 1 Code'] = content["outlet"]
		response['Analysis 2 Code'] = content["department"]
		response['Analysis 3 Code'] = "#"
		response['Analysis 4 Code'] = "#"
		response['Analysis 5 Code'] = "#"
		response['Analysis 6 Code'] = "#"
		response['Analysis 7 Code'] = "#"
		response['Analysis 8 Code'] = "#"
		response['Analysis 9 Code'] = "#"
		response['Analysis 10 Code'] = "#"
		
		response['Posting Date for Rough Journals'] = ''
		response['Update Order Balance Indicator'] = ''
		response['Allocation in Progress Flag'] = ''
		response['Journal Hold Reference'] = ''
		response['Hold Operator ID'] = ''
		response['Budget Check Account'] = ''
		response['Conversion Rate from Pivot to Base'] = ''
		response['Operator for above'] = ''
		response['Operator for Transaction Rate'] = ''
		response['Conversion Rate from Pivot to Second Base'] = ''
		
		response['Operator for above 1'] = ''
		response['Report/Second Base Amount'] = ''
		response['Memo Amount'] = ''
		response['Exclude from Balancing in Revaluation'] = ''
		response['Filler 2'] = ''
		response['Ledger Extension Details Flag'] = ''
		response['Consumed Budget ID'] = ''
		response['Value 4 Currency Code'] = ''
		response['Value 4 Amount'] = ''
		response['Value 4 Conversion Rate'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Filler'] = ''
		response['Record Type'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		response['Journal Number'] = ''
		response['Journal Line Number'] = ''
		
		response['Value 4 Operator 1'] = ''
		response['Value 4 Decimal Places'] = ''
		response['Value 5 Currency Code'] = ''
		response['journal_line_number'] = ''
		response['Value 5 Amount 1'] = ''
		response['Value 5 Conversion Rate 1'] = ''
		response['Value 5 Operator 2'] = ''
		response['Value 5 Decimal Places 2'] = ''
		response['Link Reference 1'] = ''
		response['Link Reference 2'] = ''
		
		response['Link Reference 3'] = ''
		response['Allocation Code 2'] = ''
		response['Allocation Statements Count'] = ''
		response['Allocation User ID 1'] = ''
		response['Original Line for this Split'] = ''
		response['Value Datetime'] = ''
		response['Signing Details'] = ''
		response['Installment Date'] = ''
		response['Principal Code'] = ''
		response['Principal Code 1'] = ''
		
		response['Principal Code 2'] = ''
		response['Principal Code 3'] = ''
		response['Principal Code 4'] = ''
		response['Principal Code 5'] = ''
		response['Principal Code 6'] = ''
		response['Principal Code 7'] = ''
		response['Principal Code 8'] = ''
		response['Principal Code 9'] = ''
		response['Principal Code 10'] = ''
		response['Principal Code 11'] = ''
		
		response['Principal Code 12'] = ''
		response['Principal Code 13'] = ''
		response['Principal Code 14'] = ''
		response['Principal Code 15'] = ''
		response['Principal Code 16'] = ''
		response['Principal Code 17'] = ''
		response['Principal Code 18'] = ''
		response['Principal Code 19'] = ''
		response['Binder Status'] = ''
		response['Agreed Status'] = ''
		
		response['Split Link Reference'] = ''
		response['Posting Reference'] = ''
		response['True Rated Flag'] = ''
		response['Date Held'] = ''
		response['Held Text'] = ''
		response['Instalment Number'] = ''
		response['Supplementary Extension Record Flag'] = ''
		response['Approvals Extension Flag'] = ''
		response['Revaluation Link Reference'] = ''
		response['Manual Payment Override Flag'] = ''
		
		response['Payment Stamp'] = ''
		response['Authorization in Progress'] = ''
		response['Split in Progress'] = ''
		response['Journal Line Number'] = ''
		response['Voucher Number / Second Reference'] = ''
		response['Journal Class Code'] = ''
		response['Originator Id'] = ''
		response['Originated Date and Time'] = ''
		response['Allocator Id'] = ''
		response['Journal Reversal Type'] = ''
		
		response['Document Date 1'] = ''
		response['Document Date 2'] = ''
		response['Document Date 3'] = ''
		response['Document Date 4'] = ''
		response['Document Prefix 1'] = ''
		response['Document Number 1'] = ''
		response['Document Prefix 2'] = ''
		response['Document Number 2'] = ''
		response['Document Prefix 3'] = ''
		response['Document Number 3'] = ''
		response['Document Prefix 4'] = ''
		response['Document Number 4'] = ''
		response['Discount 1 Date'] = ''
		response['Discount 1 Percentage'] = ''
		response['Discount 2 Date'] = ''
		response['Discount 2 Percentage'] = ''
		response['Interest Date'] = ''
		response['Interest Percentage'] = ''
		response['Late Payment Date'] = ''
		response['Late Payment Percentage'] = ''
		response['Payment Reference'] = ''
		response['Bank Code'] = ''
		response['Source Reference'] = ''
		response['Module Code'] = ''
		response['Payment Terms Code'] = ''
		
		
		response['General Description 1'] = int(batch_number)
		data.append(response)

	df = pd.DataFrame(data)

	df.to_csv("batchfiles/{0}.csv".format(batch_number), sep = ",", header = False, index = False, encoding = "utf-8", line_terminator = "\r\n")
	
	return jsonify({"url": "https://" + app.config['SERVER'] + "/docs/batchfiles/{0}.csv".format(batch_number)}), 200