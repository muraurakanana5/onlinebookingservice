from flask import Flask, jsonify, render_template, url_for, request, redirect, json
from datetime import datetime, timedelta

import pymysql, os, math, requests, uuid


# File imports
from routes import app
from routes import db
from routes import db_cache
from routes import FromCache
from routes.bookings_urls import get_booking_status_id
from database.inventory import Inventory
from database.invoice import Invoice
from database.bookings import Booking
from database.booking_activity_log import BookingActivity
from database.booking_guests import BookingGuest
from database.booking_status import BookingStatus
from database.booking_types import BookingType
from database.booking_guest_types import GuestType
from database.mandatory_payments import Mandatory
from database.gatepass_guests import GatepassGuest
from database.gatepass import Gatepass
from database.partner import Partner
from functions.date_operators import GenerateDateFromString
from variables import *


def close(self):
	self.session.close()


###########################
#### Inventory Booking ####
###########################
@app.route("/bookings/inventory/new", methods = ["POST"])
def add_new_inventory_booking():
	messages = []
	
	try:
		request.json["booking_id"].strip()
		if not request.json["booking_id"]:
			messages.append("Booking ID is empty.")
	except KeyError as e:
		messages.append("Booking ID is missing.")
	
	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Activity ID is empty.")
	except KeyError as e:
		messages.append("Activity ID is missing.")
	
	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	try:
		str(request.json["guests"]).strip()
		if not request.json["guests"]:
			messages.append("Guests is empty.")
	except KeyError as e:
		messages.append("Guests is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	today = datetime.now().strftime("%Y-%m-%d")
	
	# if db.session.query(Inventory)\
	# 			 .filter(Inventory.deletion_marker == None)\
	# 			 .filter(Inventory.inventory_booking_date == request.json["date"])\
	# 			 .filter(Inventory.inventory_id == request.json["inventory_id"])\
	# 			 .count() != 0:
	# 	output = []
	# 	output.append("You cannot choose that activity on the specified day. Please try either a different day or activity.")
	# 	return jsonify({"message": output}), 422

	inventory_data = requests.get(get_inventory_details.format(request.json["inventory_id"]))
	single_inventory_code = inventory_data.json()["data"][0]["code"]
	
	inventory_booking = Inventory(
		inventory_booking_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		inventory_id = request.json["inventory_id"],
		inventory_code = single_inventory_code,
		inventory_booking_date = request.json["date"],
		inventory_booking_guests = request.json["guests"],
		session_id = request.json["session_id"]
	)

	db.session.add(inventory_booking)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The activity booking has been made. Please carry on.")
		return jsonify({"message": output}), 201
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("It's us, not you. There was an error while creating the activity booking. :-( Please try again later.")
		return jsonify({"message": output}), 422


@app.route("/bookings/inventory/unavailable/date", methods = ["POST"])
def check_unavailable_inventory_booking_date():
	messages = []
	
	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	return_unavailable = db.session.query(Inventory)\
								   .join(Booking, Inventory.booking_id == Booking.booking_public_id)\
								   .add_columns(Inventory.inventory_id)\
								   .filter(Inventory.deletion_marker == None)\
								   .filter(Inventory.inventory_booking_date == request.json["date"])\
								   .filter(Inventory.status != get_booking_status_id("Cancelled"))\
								   .filter(Inventory.status != get_booking_status_id("Updated"))\
								   .filter(Booking.deletion_marker == None)\
								   .filter(Booking.status != get_booking_status_id("Cancelled"))\
								   .all()

	output = []

	for single in return_unavailable:
		return_data = {}
		return_data["inventory_id"] = single.inventory_id

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/inventory/unavailable/inventory", methods = ["POST"])
def check_unavailable_inventory_booking_inventory():
	messages = []

	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Activity ID is empty.")
	except KeyError as e:
		messages.append("Activity ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	return_unavailable = db.session.query(Inventory)\
								   .filter(Inventory.deletion_marker == None)\
								   .filter(Inventory.inventory_id == request.json["inventory_id"])\
								   .all()

	output = []

	for single in return_unavailable:
		return_data = {}
		return_data["date"] = single.inventory_booking_date

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/inventory/view")
def get_all_inventory_bookings():
	if db.session.query(Inventory)\
				 .filter(Inventory.deletion_marker == None)\
				 .count() == 0:
		output = []
		output.append("There are no activity bookings in the system.")
		return jsonify({"message": output}), 422

	return_bookings = db.session.query(Inventory)\
								.filter(Inventory.deletion_marker == None)\
								.all()

	output = []

	for single in return_bookings:
		return_data = {}
		return_data["inventory_booking_public_id"] = single.inventory_booking_public_id
		return_data["booking_id"] = single.booking_id
		return_data["inventory_id"] = single.inventory_id
		return_data["inventory_booking_date"] = single.inventory_booking_date
		return_data["inventory_booking_guests"] = single.inventory_booking_adults + single.inventory_booking_children
		return_data["session_id"] = single.session_id

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/inventory/view/<inventory_id>")
def get_single_inventory_bookings(inventory_id):
	today = datetime.now().strftime("%Y-%m-%d")
	
	if db.session.query(Inventory)\
				 .filter(Inventory.deletion_marker == None)\
				 .filter(Inventory.inventory_booking_public_id == inventory_id)\
				 .count() == 0:
		output = []
		output.append("There are no activity bookings in the system.")
		return jsonify({"message": output}), 422

	return_bookings = db.session.query(Inventory)\
								.filter(Inventory.deletion_marker == None)\
								.filter(Inventory.inventory_booking_public_id == inventory_id)\
								.all()

	output = []

	for single in return_bookings:
		return_data = {}
		return_data["booking_id"] = single.booking_id
		return_data["inventory_id"] = single.inventory_id
		return_data["inventory_booking_date"] = single.inventory_booking_date
		return_data["inventory_booking_guests"] = single.inventory_booking_adults + single.inventory_booking_children
		return_data["session_id"] = single.session_id
		return_data["created_at"] = single.created_at
		return_data["updated_at"] = single.updated_at

		output.append(return_data)

	return jsonify({"data": output}), 200


@app.route("/bookings/inventory/search", methods=["POST"])
def search_inventory_bookings_by_date():
	messages = []
	
	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Activity ID is empty.")
	except KeyError as e:
		messages.append("Activity ID is missing.")
	
	try:
		request.json["start_date"].strip()
		if not request.json["start_date"]:
			messages.append("Start date is empty.")
	except KeyError as e:
		messages.append("Start date is missing.")

	try:
		request.json["end_date"].strip()
		if not request.json["end_date"]:
			messages.append("End date is empty.")
	except KeyError as e:
		messages.append("End date is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422

	return_booking_details = db.session.query(Inventory)\
									   .join(Booking, Inventory.booking_id == Booking.booking_public_id)\
									   .add_columns(Inventory.inventory_booking_public_id, Inventory.booking_id, Inventory.inventory_booking_date,\
									   				Inventory.inventory_id, Inventory.inventory_booking_adults, Inventory.inventory_booking_children,\
													Inventory.inventory_cost_per_adult, Inventory.inventory_cost_per_child, Inventory.inventory_cost_currency,\
													Inventory.session_id)\
									   .filter(Inventory.deletion_marker == None)\
									   .filter(Inventory.inventory_id == request.json["inventory_id"])\
									   .filter(Inventory.inventory_booking_date >= request.json["start_date"])\
									   .filter(Inventory.inventory_booking_date <= request.json["end_date"])\
									   .filter(Booking.deletion_marker == None)\
									   .filter(Booking.modified == None)\
									   .all()

	inventory_details = requests.get(get_inventory_details.format(request.json["inventory_id"]))
	max_guests = inventory_details.json()["data"][0]["maximum_guests"]
	
	if not return_booking_details:
		message = []
		message.append("There are no inventory bookings within that date range.")

		return jsonify({"data": output}), 200
	else:
		output = []
		guest_array = []
		
		for each_booking in return_booking_details:
			return_data = {}
			return_data["inventory_booking_public_id"] = each_booking.inventory_booking_public_id
			return_data["booking_id"] = each_booking.booking_id
			return_data["inventory_name"] = inventory_details.json()["data"][0]["name"]
			return_data["inventory_booking_date"] = each_booking.inventory_booking_date
			return_data["inventory_id"] = each_booking.inventory_id
			return_data["inventory_booking_guests"] = each_booking.inventory_booking_adults + each_booking.inventory_booking_children
			return_data["inventory_booking_adults"] = each_booking.inventory_booking_adults
			return_data["inventory_booking_children"] = each_booking.inventory_booking_children
			return_data["inventory_cost_per_adult"] = float(each_booking.inventory_cost_per_adult)
			return_data["inventory_cost_per_child"] = float(each_booking.inventory_cost_per_child)
			return_data["inventory_cost_currency"] = each_booking.inventory_cost_currency
			return_data["session_id"] = each_booking.session_id

			output.append(return_data)
		
		return jsonify({"data": output}), 200


@app.route("/bookings/inventory/view/by_id_date", methods=["POST"])
def get_inventory_by_id_and_date():
	messages = []
	
	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Activity ID is empty.")
	except KeyError as e:
		messages.append("Activity ID is missing.")
	
	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	get_inventory = requests.get(get_inventory_details.format(request.json["inventory_id"]))
	inventory_details = get_inventory.json()

	return_booking_details = db.session.query(Inventory)\
									   .filter(Inventory.deletion_marker == None)\
									   .filter(Inventory.inventory_id == request.json["inventory_id"])\
									   .filter(Inventory.inventory_booking_date == request.json["date"])\
									   .filter(Inventory.status != get_booking_status_id("Cancelled"))\
									   .filter(Inventory.status != get_booking_status_id("Updated"))\
									   .all()

	inventory_details = requests.get(get_inventory_details.format(request.json["inventory_id"]))
	max_guests = inventory_details.json()["data"][0]["maximum_guests"]

	active_unavailability = {}
	
	if inventory_details.json()["data"][0]["unavailability_schedule_set"]:
		for each_schedule in inventory_details.json()["data"][0]["unavailability_schedule"]:
			if (each_schedule["start_date_formatted"] <= request.json["date"]) & (each_schedule["end_date_formatted"] >= request.json["date"]):
				active_unavailability = each_schedule
			else:
				pass

	if active_unavailability:
		data = []
		
		# return_data = {}
		# return_data["available_slots"] = max_guests - active_unavailability["unavailability_number"]

		# data.append(return_data)

		# return jsonify({"data": data}), 200
		unavailability_number = active_unavailability["unavailability_number"]
	else:
		unavailability_number = 0
	
	if not return_booking_details:
		output = []
		
		return_data = {}
		return_data["available_slots"] = max_guests - unavailability_number

		output.append(return_data)
		
		return jsonify({"data": output}), 200
	else:
		output = []
		guest_array = []
		
		for each_booking in return_booking_details:
			guest_array.append(each_booking.inventory_booking_adults + each_booking.inventory_booking_children)

		current_guest_total = sum(guest_array)

		return_data = {}
		# return_data["inventory_id"] = request.json["inventory_id"]
		# return_data["inventory_name"] = inventory_details.json()["data"][0]["name"]
		# return_data["current_number_of_guests"] = current_guest_total
		# return_data["max_guest_total"] = max_guests
		return_data["available_slots"] = max_guests - (current_guest_total + unavailability_number)

		output.append(return_data)
		
		return jsonify({"data": output}), 200


@app.route("/bookings/inventory/view/schedule", methods=["POST"])
def get_inventory_schedule():
	messages = []
	
	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Activity ID is empty.")
	except KeyError as e:
		messages.append("Activity ID is missing.")
	
	if messages:
		message = []
		message.append("You appear to be missing some data. Please try again.")
		return jsonify({"message": message, "messages": messages}), 422
	
	weekday = datetime.today().weekday()
	start_of_week = datetime.now() - timedelta(days=(weekday + 1))
	end_of_week = datetime.now() + timedelta(days=(5 - weekday))

	next_weekday = datetime.today().weekday()
	start_of_next_week = start_of_week + timedelta(days=7)
	end_of_next_week = end_of_week + timedelta(days=7)
	
	today = datetime.now()
	tomorrow = datetime.now() + timedelta(days=1)
	
	count_bookings_today = db.session.query(Inventory)\
									 .filter(Inventory.deletion_marker == None)\
									 .filter(Inventory.inventory_id == request.json["inventory_id"])\
									 .filter(Inventory.inventory_booking_date == today.strftime("%Y-%m-%d"))\
									 .count()

	count_bookings_tomorrow = db.session.query(Inventory)\
										.filter(Inventory.deletion_marker == None)\
										.filter(Inventory.inventory_id == request.json["inventory_id"])\
										.filter(Inventory.inventory_booking_date == tomorrow.strftime("%Y-%m-%d"))\
										.count()

	count_bookings_this_week = db.session.query(Inventory)\
										 .filter(Inventory.deletion_marker == None)\
										 .filter(Inventory.inventory_id == request.json["inventory_id"])\
										 .filter(Inventory.inventory_booking_date >= start_of_week.strftime("%Y-%m-%d"))\
										 .filter(Inventory.inventory_booking_date <= end_of_week.strftime("%Y-%m-%d"))\
										 .count()

	count_bookings_next_week = db.session.query(Inventory)\
										 .filter(Inventory.deletion_marker == None)\
										 .filter(Inventory.inventory_id == request.json["inventory_id"])\
										 .filter(Inventory.inventory_booking_date >= start_of_next_week.strftime("%Y-%m-%d"))\
										 .filter(Inventory.inventory_booking_date <= end_of_next_week.strftime("%Y-%m-%d"))\
										 .count()


	get_bookings_today = db.session.query(Inventory)\
								   .filter(Inventory.deletion_marker == None)\
								   .filter(Inventory.inventory_id == request.json["inventory_id"])\
								   .filter(Inventory.inventory_booking_date == today.strftime("%Y-%m-%d"))\
								   .all()

	today_array = []
	for single_today in get_bookings_today:
		return_bookings_today = {}
		return_bookings_today["inventory_booking_public_id"] = single_today.inventory_booking_public_id
		return_bookings_today["booking_id"] = single_today.booking_id
		return_bookings_today["inventory_id"] = single_today.inventory_id
		return_bookings_today["inventory_code"] = single_today.inventory_code
		return_bookings_today["inventory_booking_date"] = single_today.inventory_booking_date
		return_bookings_today["inventory_booking_adults"] = single_today.inventory_booking_adults
		return_bookings_today["inventory_booking_children"] = single_today.inventory_booking_children

		today_array.append(return_bookings_today)

	get_bookings_tomorrow = db.session.query(Inventory)\
									  .filter(Inventory.deletion_marker == None)\
									  .filter(Inventory.inventory_id == request.json["inventory_id"])\
									  .filter(Inventory.inventory_booking_date == tomorrow.strftime("%Y-%m-%d"))\
									  .all()

	tomorrow_array = []
	for single_tomorrow in get_bookings_tomorrow:
		return_bookings_tomorrow = {}
		return_bookings_tomorrow["inventory_booking_public_id"] = single_tomorrow.inventory_booking_public_id
		return_bookings_tomorrow["booking_id"] = single_tomorrow.booking_id
		return_bookings_tomorrow["inventory_id"] = single_tomorrow.inventory_id
		return_bookings_tomorrow["inventory_code"] = single_tomorrow.inventory_code
		return_bookings_tomorrow["inventory_booking_date"] = single_tomorrow.inventory_booking_date
		return_bookings_tomorrow["inventory_booking_adults"] = single_tomorrow.inventory_booking_adults
		return_bookings_tomorrow["inventory_booking_children"] = single_tomorrow.inventory_booking_children

		tomorrow_array.append(return_bookings_tomorrow)

	get_bookings_this_week = db.session.query(Inventory)\
									  .filter(Inventory.deletion_marker == None)\
									  .filter(Inventory.inventory_id == request.json["inventory_id"])\
									  .filter(Inventory.inventory_booking_date >= start_of_week.strftime("%Y-%m-%d"))\
									  .filter(Inventory.inventory_booking_date <= end_of_week.strftime("%Y-%m-%d"))\
									  .all()

	this_week_array = []
	for single_this_week in get_bookings_this_week:
		return_bookings_this_week = {}
		return_bookings_this_week["inventory_booking_public_id"] = single_this_week.inventory_booking_public_id
		return_bookings_this_week["booking_id"] = single_this_week.booking_id
		return_bookings_this_week["inventory_id"] = single_this_week.inventory_id
		return_bookings_this_week["inventory_code"] = single_this_week.inventory_code
		return_bookings_this_week["inventory_booking_date"] = single_this_week.inventory_booking_date
		return_bookings_this_week["inventory_booking_adults"] = single_this_week.inventory_booking_adults
		return_bookings_this_week["inventory_booking_children"] = single_this_week.inventory_booking_children

		this_week_array.append(return_bookings_this_week)

	get_bookings_next_week = db.session.query(Inventory)\
									  .filter(Inventory.deletion_marker == None)\
									  .filter(Inventory.inventory_id == request.json["inventory_id"])\
									  .filter(Inventory.inventory_booking_date >= start_of_next_week.strftime("%Y-%m-%d"))\
									  .filter(Inventory.inventory_booking_date <= end_of_next_week.strftime("%Y-%m-%d"))\
									  .all()

	next_week_array = []
	for single_next_week in get_bookings_next_week:
		return_bookings_next_week = {}
		return_bookings_next_week["inventory_booking_public_id"] = single_next_week.inventory_booking_public_id
		return_bookings_next_week["booking_id"] = single_next_week.booking_id
		return_bookings_next_week["inventory_id"] = single_next_week.inventory_id
		return_bookings_next_week["inventory_code"] = single_next_week.inventory_code
		return_bookings_next_week["inventory_booking_date"] = single_next_week.inventory_booking_date
		return_bookings_next_week["inventory_booking_adults"] = single_next_week.inventory_booking_adults
		return_bookings_next_week["inventory_booking_children"] = single_next_week.inventory_booking_children

		next_week_array.append(return_bookings_next_week)

	data = []

	booking_schedule = {}
	booking_schedule["today_count"] = count_bookings_today
	booking_schedule["today_bookings"] = today_array

	booking_schedule["tomorrow_count"] = count_bookings_tomorrow
	booking_schedule["tomorrow_bookings"] = tomorrow_array

	booking_schedule["this_week_count"] = count_bookings_this_week
	booking_schedule["this_week_bookings"] = this_week_array

	booking_schedule["next_week_count"] = count_bookings_next_week
	booking_schedule["next_week_bookings"] = next_week_array

	booking_schedule["date_start_of_week"] = start_of_week.strftime("%y-%m-%d")
	booking_schedule["date_end_of_week"] = end_of_week.strftime("%y-%m-%d")
	booking_schedule["date_start_of_next_week"] = start_of_next_week.strftime("%y-%m-%d")
	booking_schedule["date_end_of_next_week"] = end_of_next_week.strftime("%y-%m-%d")

	data.append(booking_schedule)

	return jsonify({"data": data}), 200


@app.route("/bookings/inventory/list", methods = ["POST"])
def list_inventory_bookings():
	messages = []

	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Inventory ID is empty.")
	except KeyError as e:
		messages.append("Inventory ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	requested_date = GenerateDateFromString.generateDate(request.json["date"])
	
	get_inventory = requests.get(get_inventory_details.format(request.json["inventory_id"]))
	inventory_details = get_inventory.json()
	
	return_bookings = db.session.query(Inventory)\
								.join(Booking, Inventory.booking_id == Booking.booking_public_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_done_by, Booking.booking_check_in_date,\
											 Booking.booking_check_out_date, Booking.booking_ref_code, Booking.booking_type,\
											 Booking.status, Booking.created_at, Booking.updated_at,\
											 Booking.deletion_marker, Booking.checked_in, Booking.checked_out,\
											 Booking.session_id,\
											 Inventory.booking_id, Inventory.inventory_booking_adults, Inventory.inventory_booking_children,\
											 Inventory.inventory_booking_extra_adults, Inventory.inventory_booking_extra_children)\
								.filter(Inventory.deletion_marker == None)\
								.filter(Inventory.inventory_id == request.json["inventory_id"])\
								.filter(Inventory.inventory_booking_date == request.json["date"])\
								.filter(Inventory.status != get_booking_status_id("Updated"))\
								.filter(Inventory.status != get_booking_status_id("Cancelled"))\
								.all()

	active_unavailability = {}
	
	if inventory_details["data"][0]["unavailability_schedule_set"]:
		for each_schedule in inventory_details["data"][0]["unavailability_schedule"]:
			if (each_schedule["start_date_formatted"] <= request.json["date"]) & (each_schedule["end_date_formatted"] >= request.json["date"]):
				active_unavailability = each_schedule
			else:
				pass

	# if active_unavailability:
	# 	availability = int(inventory_details["data"][0]["maximum_guests"]) - int(active_unavailability["unavailability_number"])
		
	# 	if availability == 0:
	# 		message = []
	# 		message.append("The activity is not avaliable for the date " + requested_date.strftime("%d %b %Y") + " due to the following reason: " + active_unavailability["unavailability_reason"])
	# 		return jsonify({"message": message, "data": []}), 412
	
	if not return_bookings:
		data = []
		
		if active_unavailability:
			return_unavailability = {}
			return_unavailability["booking_public_id"] = "N/A"
			return_unavailability["booking_type"] = "N/A"
			return_unavailability["booking_type_id"] = "N/A"
			return_unavailability["booking_check_in_date"] = active_unavailability["unavailability_start_date"]
			return_unavailability["booking_check_out_date"] = active_unavailability["unavailability_end_date"]
			return_unavailability["booking_done_by"] = "Unavailability (" + active_unavailability["unavailability_reason"] + ")"
			return_unavailability["booking_ref_code"] = "N/A"
			return_unavailability["inventory_booking_guests"] = active_unavailability["unavailability_number"]
			return_unavailability["guests"] = []
			return_unavailability["guest_total"] = active_unavailability["unavailability_number"]
			return_unavailability["booking_status"] = "N/A"
			return_unavailability["booking_status_id"] = "N/A"
			return_unavailability["created_at"] = active_unavailability["created_at"]
			return_unavailability["updated_at"] = active_unavailability["created_at"]
			return_unavailability["session_user"] = "N/A"
			return_unavailability["session_id"] = "N/A"

			data.append(return_unavailability)
			return jsonify({"data": data}), 200
		
		else:
			message = []
			message.append("There are no activity bookings for the selected date: " + requested_date.strftime("%d %b %Y"))
			return jsonify({"message": message, "data": []}), 412

	else:
		output = []

		if active_unavailability:
			return_unavailability = {}
			return_unavailability["booking_public_id"] = "N/A"
			return_unavailability["booking_type"] = "N/A"
			return_unavailability["booking_type_id"] = "N/A"
			return_unavailability["booking_check_in_date"] = active_unavailability["unavailability_start_date"]
			return_unavailability["booking_check_out_date"] = active_unavailability["unavailability_end_date"]
			return_unavailability["booking_done_by"] = "Unavailability (" + active_unavailability["unavailability_reason"] + ")"
			return_unavailability["booking_ref_code"] = "N/A"
			return_unavailability["inventory_booking_guests"] = active_unavailability["unavailability_number"]
			return_unavailability["guests"] = []
			return_unavailability["guest_total"] = active_unavailability["unavailability_number"]
			return_unavailability["booking_status"] = "N/A"
			return_unavailability["booking_status_id"] = "N/A"
			return_unavailability["created_at"] = active_unavailability["created_at"]
			return_unavailability["updated_at"] = active_unavailability["created_at"]
			return_unavailability["session_user"] = "N/A"
			return_unavailability["session_id"] = "N/A"

			output.append(return_unavailability)

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			
			booking_type = db.session.query(BookingType)\
									 .filter(BookingType.deletion_marker == None)\
									 .filter(BookingType.booking_type_public_id == single.booking_type)\
									 .first()

			return_data["booking_type"] = booking_type.booking_type_name
			
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			get_guests = db.session.query(BookingGuest)\
								   .filter(BookingGuest.deletion_marker == None)\
								   .filter(BookingGuest.booking_id == single.booking_public_id)\
								   .all()

			return_data["inventory_booking_guests"] = int(single.inventory_booking_adults) + int(single.inventory_booking_children) + int(single.inventory_booking_extra_adults) + int(single.inventory_booking_extra_children)

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			# return_data["guest_total"] = sum(guest_sum)
			return_data["guest_total"] = int(single.inventory_booking_adults) + int(single.inventory_booking_children) + int(single.inventory_booking_extra_adults) + int(single.inventory_booking_extra_children)

			check_to_invoice = db.session.query(Invoice)\
										 .filter(Invoice.deletion_marker == None)\
										 .filter(Invoice.booking_id == single.booking_id)\
										 .first()
			
			booking_status = db.session.query(BookingStatus)\
									   .filter(BookingStatus.deletion_marker == None)\
									   .filter(BookingStatus.booking_status_public_id == single.status)\
									   .first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if booking_status.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif booking_status.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif booking_status.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif booking_status.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif booking_status.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif booking_status.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif booking_status.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = booking_status.booking_status_name
			return_data["booking_status_id"] = single.status

			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			id_array = []
			for each_id in return_bookings:
				id_array.append(each_id.session_id)

			try:
				return_user = requests.post(get_user_from_aumra,\
											json = {"users_ids": id_array})
			except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
				pass

			if single.session_id:
				if single.session_id == "guest":
					return_data["session_user"] = "Public"
					return_data["session_id"] = single.session_id
				else:
					try:
						for user in return_user.json()["data"]:
							if user["public_id"] == single.session_id:
								return_data["session_user"] = user["full_name"]
								return_data["session_id"] = single.session_id
													
					except (IndexError, KeyError) as user_error:
						return_data["session_user"] = "N/A"
						return_data["session_id"] = single.session_id

					except (AttributeError, UnboundLocalError) as network_related_errors:
						return_data["session_user"] = "Network Error"
						return_data["session_id"] = single.session_id
			else:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/bookings/inventory/partner/list", methods = ["POST"])
def list_inventory_bookings_for_partner():
	messages = []

	try:
		request.json["inventory_id"].strip()
		if not request.json["inventory_id"]:
			messages.append("Inventory ID is empty.")
	except KeyError as e:
		messages.append("Inventory ID is missing.")

	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	try:
		request.json["partner_id"].strip()
		if not request.json["partner_id"]:
			messages.append("Partner ID is empty.")
	except KeyError as e:
		messages.append("Partner ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422

	requested_date = GenerateDateFromString.generateDate(request.json["date"])
	
	get_inventory = requests.get(get_inventory_details.format(request.json["inventory_id"]))
	inventory_details = get_inventory.json()
	
	return_bookings = db.session.query(Inventory)\
								.join(Booking, Inventory.booking_id == Booking.booking_public_id)\
								.join(Partner, Inventory.booking_id == Partner.booking_id)\
								.add_columns(Booking.booking_public_id, Booking.booking_done_by, Booking.booking_check_in_date,\
											 Booking.booking_check_out_date, Booking.booking_ref_code, Booking.booking_type,\
											 Booking.status, Booking.created_at, Booking.updated_at,\
											 Booking.deletion_marker, Booking.checked_in, Booking.checked_out,\
											 Booking.session_id,\
											 Inventory.booking_id, Inventory.inventory_booking_adults, Inventory.inventory_booking_children,\
											 Inventory.inventory_booking_extra_adults, Inventory.inventory_booking_extra_children)\
								.filter(Inventory.deletion_marker == None)\
								.filter(Inventory.inventory_id == request.json["inventory_id"])\
								.filter(Inventory.inventory_booking_date == request.json["date"])\
								.filter(Inventory.status != get_booking_status_id("Updated"))\
								.filter(Inventory.status != get_booking_status_id("Cancelled"))\
								.filter(Partner.partner_id == request.json["partner_id"])\
								.all()

	active_unavailability = {}
	
	if inventory_details["data"][0]["unavailability_schedule_set"]:
		for each_schedule in inventory_details["data"][0]["unavailability_schedule"]:
			if (each_schedule["start_date_formatted"] <= request.json["date"]) & (each_schedule["end_date_formatted"] >= request.json["date"]):
				active_unavailability = each_schedule
			else:
				pass

	# if active_unavailability:
	# 	availability = int(inventory_details["data"][0]["maximum_guests"]) - int(active_unavailability["unavailability_number"])
		
	# 	if availability == 0:
	# 		message = []
	# 		message.append("The activity is not avaliable for the date " + requested_date.strftime("%d %b %Y") + " due to the following reason: " + active_unavailability["unavailability_reason"])
	# 		return jsonify({"message": message, "data": []}), 412
	
	if not return_bookings:
		data = []
		
		if active_unavailability:
			return_unavailability = {}
			return_unavailability["booking_public_id"] = "N/A"
			return_unavailability["booking_type"] = "N/A"
			return_unavailability["booking_type_id"] = "N/A"
			return_unavailability["booking_check_in_date"] = active_unavailability["unavailability_start_date"]
			return_unavailability["booking_check_out_date"] = active_unavailability["unavailability_end_date"]
			return_unavailability["booking_done_by"] = "Unavailability (" + active_unavailability["unavailability_reason"] + ")"
			return_unavailability["booking_ref_code"] = "N/A"
			return_unavailability["inventory_booking_guests"] = active_unavailability["unavailability_number"]
			return_unavailability["guests"] = []
			return_unavailability["guest_total"] = active_unavailability["unavailability_number"]
			return_unavailability["booking_status"] = "N/A"
			return_unavailability["booking_status_id"] = "N/A"
			return_unavailability["created_at"] = active_unavailability["unavailability_start_date"]
			return_unavailability["updated_at"] = active_unavailability["unavailability_start_date"]
			return_unavailability["session_user"] = "N/A"
			return_unavailability["session_id"] = "N/A"

			data.append(return_unavailability)
			return jsonify({"data": data}), 200
		
		else:
			message = []
			message.append("There are no activity bookings for the selected date: " + requested_date.strftime("%d %b %Y"))
			return jsonify({"message": message, "data": []}), 412

	else:
		output = []

		if active_unavailability:
			return_unavailability = {}
			return_unavailability["booking_public_id"] = "N/A"
			return_unavailability["booking_type"] = "N/A"
			return_unavailability["booking_type_id"] = "N/A"
			return_unavailability["booking_check_in_date"] = active_unavailability["unavailability_start_date"]
			return_unavailability["booking_check_out_date"] = active_unavailability["unavailability_end_date"]
			return_unavailability["booking_done_by"] = "Unavailability (" + active_unavailability["unavailability_reason"] + ")"
			return_unavailability["booking_ref_code"] = "N/A"
			return_unavailability["inventory_booking_guests"] = active_unavailability["unavailability_number"]
			return_unavailability["guests"] = []
			return_unavailability["guest_total"] = active_unavailability["unavailability_number"]
			return_unavailability["booking_status"] = "N/A"
			return_unavailability["booking_status_id"] = "N/A"
			return_unavailability["created_at"] = active_unavailability["unavailability_start_date"]
			return_unavailability["updated_at"] = active_unavailability["unavailability_start_date"]
			return_unavailability["session_user"] = "N/A"
			return_unavailability["session_id"] = "N/A"

			output.append(return_unavailability)

		for single in return_bookings:
			return_data = {}
			return_data["booking_public_id"] = single.booking_public_id
			
			booking_type = db.session.query(BookingType)\
									 .filter(BookingType.deletion_marker == None)\
									 .filter(BookingType.booking_type_public_id == single.booking_type)\
									 .first()

			return_data["booking_type"] = booking_type.booking_type_name
			
			return_data["booking_type_id"] = single.booking_type
			return_data["booking_check_in_date"] = single.booking_check_in_date
			return_data["booking_check_out_date"] = single.booking_check_out_date
			return_data["booking_done_by"] = single.booking_done_by
			return_data["booking_ref_code"] = single.booking_ref_code

			get_guests = db.session.query(BookingGuest)\
								   .filter(BookingGuest.deletion_marker == None)\
								   .filter(BookingGuest.booking_id == single.booking_public_id)\
								   .all()

			return_data["inventory_booking_guests"] = int(single.inventory_booking_adults) + int(single.inventory_booking_children) + int(single.inventory_booking_extra_adults) + int(single.inventory_booking_extra_children)

			guest_array = []
			guest_sum = []

			get_all_guests = db.session.query(GatepassGuest)\
									   .join(Mandatory, GatepassGuest.gatepass_guest_type == Mandatory.payment_public_id)\
									   .join(Gatepass, GatepassGuest.gatepass_id == Gatepass.gatepass_public_id)\
									   .add_columns(Mandatory.payment_person, Mandatory.payment_public_id,\
													GatepassGuest.gatepass_guest_count, GatepassGuest.gatepass_discount_rate, GatepassGuest.gatepass_cost_per_pp,\
													GatepassGuest.gatepass_no_of_nights)\
									   .filter(GatepassGuest.deletion_marker == None)\
									   .filter(GatepassGuest.status != get_booking_status_id("Updated"))\
									   .filter(Gatepass.booking_id == single.booking_public_id)\
									   .options(FromCache(db_cache))\
									   .all()

			for each_guest in get_all_guests:
				guest_data = {}
				guest_data["guest_type"] = each_guest.payment_person
				guest_data["no_of_guests"] = each_guest.gatepass_guest_count

				guest_array.append(guest_data)
				guest_sum.append(int(each_guest.gatepass_guest_count))

			return_data["guests"] = guest_array
			# return_data["guest_total"] = sum(guest_sum)
			return_data["guest_total"] = int(single.inventory_booking_adults) + int(single.inventory_booking_children) + int(single.inventory_booking_extra_adults) + int(single.inventory_booking_extra_children)

			check_to_invoice = db.session.query(Invoice)\
										 .filter(Invoice.deletion_marker == None)\
										 .filter(Invoice.booking_id == single.booking_id)\
										 .first()
			
			booking_status = db.session.query(BookingStatus)\
									   .filter(BookingStatus.deletion_marker == None)\
									   .filter(BookingStatus.booking_status_public_id == single.status)\
									   .first()

			if check_to_invoice:
				if single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					return_data["booking_status"] = "To Invoice"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["TO_INVOICE"])

			else:
				if single.checked_out == 1:
					return_data["booking_status"] = "Checked Out"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CHECKED_OUT"])
				elif single.checked_in == 1:
					return_data["booking_status"] = "Checked In"
					return_data["booking_color"] = "background-color:{}; color: #fff".format(app.config["CHECKED_IN"])
				elif single.deletion_marker == 1:
					return_data["booking_status"] = "Cancelled"
					return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CANCELLED"])
				else:
					if booking_status.booking_status_name == "Unconfirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UNCONFIRMED"])
					elif booking_status.booking_status_name == "Confirmed":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["CONFIRMED"])
					elif booking_status.booking_status_name == "No-Show":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["NO_SHOW"])
					elif booking_status.booking_status_name == "Abandoned":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["ABANDONED"])
					elif booking_status.booking_status_name == "Updated":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["UPDATED"])
					elif booking_status.booking_status_name == "Deposit":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["DEPOSIT"])
					elif booking_status.booking_status_name == "Complimentary":
						return_data["booking_color"] = "background-color: {}; color: #fff".format(app.config["COMPLIMENTARY"])
					else:
						return_data["booking_color"] = "background-color: #fff; color: #000000"
					return_data["booking_status"] = booking_status.booking_status_name
			return_data["booking_status_id"] = single.status

			return_data["created_at"] = single.created_at
			return_data["updated_at"] = single.updated_at

			id_array = []
			for each_id in return_bookings:
				id_array.append(each_id.session_id)

			try:
				return_user = requests.post(get_user_from_aumra,\
											json = {"users_ids": id_array})
			except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.ConnectTimeout) as connection_error:
				pass

			if single.session_id:
				if single.session_id == "guest":
					return_data["session_user"] = "Public"
					return_data["session_id"] = single.session_id
				else:
					try:
						for user in return_user.json()["data"]:
							if user["public_id"] == single.session_id:
								return_data["session_user"] = user["full_name"]
								return_data["session_id"] = single.session_id
													
					except (IndexError, KeyError) as user_error:
						return_data["session_user"] = "N/A"
						return_data["session_id"] = single.session_id

					except (AttributeError, UnboundLocalError) as network_related_errors:
						return_data["session_user"] = "Network Error"
						return_data["session_id"] = single.session_id
			else:
				return_data["session_user"] = "N/A"
				return_data["session_id"] = single.session_id

			output.append(return_data)

		return jsonify({"data": output}), 200


@app.route("/bookings/inventory/modify/date", methods = ["PATCH"])
def edit_single_inventory_booking():
	messages = []
	
	try:
		request.json["inventory_booking_id"].strip()
		if not request.json["inventory_booking_id"]:
			messages.append("Activity booking ID is empty.")
	except KeyError as e:
		messages.append("Activity booking ID is missing.")
	
	try:
		request.json["date"].strip()
		if not request.json["date"]:
			messages.append("Date is empty.")
	except KeyError as e:
		messages.append("Date is missing.")

	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")

	if messages:
		return jsonify({"messages": messages}), 422
	
	return_booking = db.session.query(Inventory)\
							   .filter(Inventory.deletion_marker == None)\
							   .filter(Inventory.inventory_booking_public_id == request.json["inventory_booking_id"])\
							   .filter(Inventory.status != get_booking_status_id("Updated"))\
							   .filter(Inventory.status != get_booking_status_id("Cancelled"))\
							   .first()

	if not return_booking:
		output = []
		output.append("The selected reservation doesn't appear to exist.")
		return jsonify({"message": output}), 422

	get_booking = db.session.query(Booking)\
							.filter(Booking.booking_public_id == return_booking.booking_id)\
							.first()

	if (request.json["date"] < get_booking.booking_check_in_date.strftime("%Y-%m-%d")) or (request.json["date"] > get_booking.booking_check_out_date.strftime("%Y-%m-%d")):
		message = []
		message.append("The activity date has to fall within the booking check-in and check-out dates.")
		return jsonify({"message": message}), 412
	
	return_booking.inventory_booking_date = request.json["date"]
	# return_booking.session_id = request.json["session_id"]
	# return_booking.updated_at = datetime.now()

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = return_booking.booking_id,
		booking_activity_description = request.json["inventory_name"] + " reservation date changed from " + request.json["old_date"] + " to " + request.json["date"] + "",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The activity booking date has been modified.")
		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while editing the activity booking.")
		return jsonify({"message": output}), 422


@app.route("/bookings/inventory/delete", methods = ["PATCH"])
def delete_single_inventory_booking():
	messages = []
	
	try:
		request.json["inventory_booking_id"].strip()
		if not request.json["inventory_booking_id"]:
			messages.append("Activity booking ID is empty.")
	except KeyError as e:
		messages.append("Activity booking ID is missing.")
	
	try:
		request.json["session_id"].strip()
		if not request.json["session_id"]:
			messages.append("Session ID is empty.")
	except KeyError as e:
		messages.append("Session ID is missing.")
	
	if messages:
		return jsonify({"messages": messages}), 422
	
	return_booking = db.session.query(Inventory)\
							   .filter(Inventory.deletion_marker == None)\
							   .filter(Inventory.status != get_booking_status_id("Updated"))\
							   .filter(Inventory.inventory_booking_public_id == request.json["inventory_booking_id"])\
							   .first()

	if not return_booking:
		message = []
		message.append("The selected activity booking doesn't appear to exist or has already been cancelled.")
		return jsonify({"message": message}), 422

	return_booking.status = get_booking_status_id("Updated")
	# return_booking.session_id = request.json["session_id"]
	return_booking.updated_at = datetime.now()

	try:
		db.session.commit()
		close(db)

		output = []
		output.append("The activity booking has been cancelled.")
		return jsonify({"message": output}), 200
	except Exception as e:
		print(e)
		db.session.rollback()
		close(db)

		output = []
		output.append("There was an error while cancelling the activity booking.")
		return jsonify({"message": output}), 422