import requests
import uuid

from datetime import date, datetime, timedelta
from flask import (json, jsonify, request)

from database.booking_activity_log import BookingActivity
from database.bookings import Booking
from database.credit_note import CreditNote
from database.credit_note_facilities import CreditNoteFacility
from database.credit_note_guests import CreditNoteGuest
from database.credit_note_inventory import CreditNoteInventory
from database.credit_note_vehicles import CreditNoteVehicle
from functions.date_operators import *
from functions.validation import *
from routes import (FromCache, app, bookings_logger, db, db_cache)
from variables import *


def close(self):
	self.session.close()


@app.route("/credit-note/new", methods = ["POST"])
def add_credit_note():
	validation_list = [
		{"field": "booking_id", "alias": "Booking ID"},
		{"field": "session_id", "alias": "Session ID"},
		{"field": "reason"},
		{"field": "currency"}
	]

	messages = fieldValidation(request.json, validation_list)

	if messages:
		return jsonify({"messages": messages}), 422

	get_booking = db.session.query(Booking)\
							.filter(Booking.deletion_marker == None)\
							.filter(Booking.booking_public_id == request.json["booking_id"])\
							.first()

	if not get_booking:
		message = []
		message.append("The selected booking does not appear to exist or has been cancelled.")
		return jsonify({"message": message}), 412

	get_exchange_rate = requests.get(get_buy_sell_rate.format(request.json["currency"]))
	buying_rate = get_exchange_rate.json()["data"][0]["currency_buy_amount"]
	selling_rate = get_exchange_rate.json()["data"][0]["currency_sell_amount"]
	
	credit_note_id = str(uuid.uuid4())
	
	credit_note = CreditNote(
		credit_note_public_id = credit_note_id,
		booking_id = request.json["booking_id"],
		credit_note_ref = str(uuid.uuid4())[:12],
		credit_note_currency = request.json["currency"],
		credit_note_amount = 0,
		credit_note_reason = request.json["reason"],
		currency_buying_rate_at_time = buying_rate,
		currency_selling_rate_at_time = selling_rate,
		created_at = datetime.now(),
		session_id = request.json["session_id"]
	)

	db.session.add(credit_note)

	for single_guest in request.json["guests"]:
		credit_note_guest = CreditNoteGuest(
			credit_note_guests_public_id = str(uuid.uuid4()),
			credit_note_id = credit_note_id,
			gatepass_guest_id = single_guest["guest"]["gatepass_guest_public_id"],
			credit_number_of_guests = int(single_guest["number_of_guests"]),
			credit_guest_discount = float(single_guest["guest_discount"]),
			credit_guest_check_in_date = single_guest["check_in_date"],
			credit_guest_check_out_date = single_guest["check_out_date"],
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(credit_note_guest)

	for single_vehicle in request.json["vehicles"]:
		credit_note_vehicle = CreditNoteVehicle(
			credit_note_vehicles_public_id = str(uuid.uuid4()),
			credit_note_id = credit_note_id,
			gatepass_vehicle_id = single_vehicle["vehicle"]["gatepass_vehicle_public_id"],
			credit_number_of_vehicles = int(single_vehicle["number_of_vehicles"]),
			credit_vehicle_discount = float(single_vehicle["vehicle_discount"]),
			credit_vehicle_check_in_date = single_vehicle["check_in_date"],
			credit_vehicle_check_out_date = single_vehicle["check_out_date"],
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(credit_note_vehicle)

	for single_facility in request.json["accommodations"]:
		credit_note_facility = CreditNoteFacility(
			credit_note_facilities_public_id = str(uuid.uuid4()),
			credit_note_id = credit_note_id,
			facility_booking_id = single_facility["accommodation"]["facility_booking_public_id"],
			credit_facility_booking_adults = int(single_facility["number_of_adults"]),
			credit_facility_booking_children = int(single_facility["number_of_children"]),
			credit_facility_booking_extra_adults = int(single_facility["number_of_extra_adults"]),
			credit_facility_booking_extra_children = int(single_facility["number_of_extra_children"]),
			credit_facility_check_in_date = single_facility["check_in_date"],
			credit_facility_check_out_date = single_facility["check_out_date"],
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(credit_note_facility)

	for single_inventory in request.json["activities"]:
		credit_note_inventory = CreditNoteInventory(
			credit_note_inventory_public_id = str(uuid.uuid4()),
			credit_note_id = credit_note_id,
			inventory_booking_id = single_inventory["activity"]["inventory_booking_public_id"],
			credit_inventory_booking_adults = int(single_inventory["number_of_adults"]),
			credit_inventory_booking_children = int(single_inventory["number_of_children"]),
			credit_inventory_booking_extra_adults = int(single_inventory["number_of_extra_adults"]),
			credit_inventory_booking_extra_children = int(single_inventory["number_of_extra_children"]),
			credit_inventory_date = single_inventory["activity_date"],
			session_id = request.json["session_id"],
			created_at = datetime.now()
		)

		db.session.add(credit_note_inventory)

	booking_activity = BookingActivity(
		booking_activity_public_id = str(uuid.uuid4()),
		booking_id = request.json["booking_id"],
		booking_activity_description = "Credit note issued.",
		session_id = request.json["session_id"],
		created_at = datetime.now()
	)

	db.session.add(booking_activity)

	try:
		db.session.commit()
		close(db)

		message = []
		message.append("Successfully issued credit note.")
		return jsonify({"message": message}), 201

	except Exception as e:
		db.session.rollback()
		close(db)

		message = []
		message.append("Unable to issue credit note. Try again later.")
		return jsonify({"message": message, "exc": str(e)}), 422