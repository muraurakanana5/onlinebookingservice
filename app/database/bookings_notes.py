from datetime import datetime

import requests

from routes import db
from variables import get_single_user


class Note(db.Model):
	__tablename__ = 'booking_notes'
	booking_notes_id = db.Column(db.Integer, primary_key=True)
	booking_notes_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	note = db.Column(db.Text)
	show_on_invoice = db.Column(db.String(20))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, booking_notes_public_id = None, booking_id = None,
				 note = None, show_on_invoice = None, session_id = None,
				 status = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.booking_notes_public_id = booking_notes_public_id
		self.booking_id = booking_id
		self.note = note
		self.show_on_invoice = show_on_invoice
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Note %r>" % (self.booking_notes_public_id)

	
	def return_json(self):
		user = requests.get(get_single_user.format(self.session_id))
		
		return {
			"booking_notes_public_id": self.booking_notes_public_id,
			"note": self.note,
			"created_at": self.created_at,
			"user": user.json()["first_name"] + " " + user.json()["last_name"]
		}