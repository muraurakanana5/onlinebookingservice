from datetime import datetime

from routes import db


class CreditNoteGuest(db.Model):
	__tablename__ = 'credit_note_guests'
	credit_note_guests_id = db.Column(db.Integer, primary_key=True)
	credit_note_guests_public_id = db.Column(db.String(255), unique=True, index=True)
	credit_note_id = db.Column(db.String(255), db.ForeignKey("credit_notes.credit_note_public_id"), index=True)
	gatepass_guest_id = db.Column(db.String(255), index=True)
	credit_number_of_guests = db.Column(db.Integer, nullable=False)
	credit_guest_discount = db.Column(db.Float(10,2), default=0)
	credit_guest_check_in_date = db.Column(db.DateTime)
	credit_guest_check_out_date = db.Column(db.DateTime)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)