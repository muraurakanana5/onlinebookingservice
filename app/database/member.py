from datetime import datetime

from routes import db


class Member(db.Model):
	__tablename__ = 'member_bookings'
	member_booking_id = db.Column(db.Integer, primary_key=True)
	member_booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	member_id = db.Column(db.String(255))
	num_of_people = db.Column(db.Integer)
	num_of_children = db.Column(db.Integer)
	num_of_vehicles = db.Column(db.Integer)
	session_id = db.Column(db.String(255), index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, member_booking_public_id = None, booking_id = None,
				 member_id = None, num_of_people = None, num_of_children = None,
				 num_of_vehicles = None, session_id = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.member_booking_public_id = member_booking_public_id
		self.booking_id = booking_id
		self.member_id = member_id
		self.num_of_people = num_of_people
		self.num_of_children = num_of_children
		self.num_of_vehicles = num_of_vehicles
		self.session_id = session_id
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Member %r>" % (self.member_booking_id)

	
	def return_json(self):
		return {
			"member_booking_public_id": self.member_booking_public_id,
			"booking_id": self.booking_id,
			"member_id": self.member_id,
			"num_of_people": self.num_of_people,
			"num_of_children": self.num_of_children,
			"num_of_vehicles": self.num_of_vehicles,
			"session_id": self.session_id,
			"created_at": self.created_at
		}