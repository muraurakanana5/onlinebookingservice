from datetime import datetime

from routes import db


class Destination(db.Model):
	__tablename__ = 'gatepass_destinations'
	gatepass_destination_id = db.Column(db.Integer, primary_key=True)
	gatepass_destination_public_id = db.Column(db.String(255), unique=True, index=True)
	gatepass_destination_name = db.Column(db.String(255))
	code = db.Column(db.String(100))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	destinations_partners = db.relationship("PartnerDestination", backref = "destination_details", lazy = "dynamic")


	def __init__(self, gatepass_destination_public_id = None, gatepass_destination_name = None,
				 code = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.gatepass_destination_public_id = gatepass_destination_public_id
		self.gatepass_destination_name = gatepass_destination_name
		self.code = code
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Destination %r>" % (self.gatepass_destination_name)

	
	def return_json(self):
		return {
			"destination_public_id": self.gatepass_destination_public_id,
			"destination_name": self.gatepass_destination_name,
			"destination_code": self.code,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}