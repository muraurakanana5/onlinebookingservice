from datetime import datetime

from routes import db


class CreditNoteVehicle(db.Model):
	__tablename__ = 'credit_note_vehicles'
	credit_note_vehicles_id = db.Column(db.Integer, primary_key=True)
	credit_note_vehicles_public_id = db.Column(db.String(255), unique=True, index=True)
	credit_note_id = db.Column(db.String(255), db.ForeignKey("credit_notes.credit_note_public_id"), index=True)
	gatepass_vehicle_id = db.Column(db.String(255), index=True)
	credit_number_of_vehicles = db.Column(db.Integer, nullable=False)
	credit_vehicle_discount = db.Column(db.Float(10,2), default=0)
	credit_vehicle_check_in_date = db.Column(db.DateTime)
	credit_vehicle_check_out_date = db.Column(db.DateTime)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)