from datetime import datetime

import requests

from routes import db
from variables import get_currency


class Vehicle(db.Model):
	__tablename__ = 'vehicle_charges'
	vehicle_charge_id = db.Column(db.Integer, primary_key=True)
	vehicle_charge_public_id = db.Column(db.String(255), unique=True, index=True)
	vehicle_charge_income_code = db.Column(db.String(255))
	vehicle_charge_outlet_code = db.Column(db.String(255))
	vehicle_charge_dept_code = db.Column(db.String(255))
	vehicle_charge_category = db.Column(db.String(255))
	vehicle_charge_category_cost = db.Column(db.Float(10,2))
	vehicle_charge_cost_currency = db.Column(db.String(255))
	vehicle_charge_description = db.Column(db.String(255))
	maximum_number = db.Column(db.Integer, nullable=True)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, vehicle_charge_public_id = None, vehicle_charge_income_code = None,
				 vehicle_charge_outlet_code = None, vehicle_charge_dept_code = None, vehicle_charge_category = None,
				 vehicle_charge_category_cost = None, vehicle_charge_cost_currency = None, vehicle_charge_description = None,
				 maximum_number = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.vehicle_charge_public_id = vehicle_charge_public_id
		self.vehicle_charge_income_code = vehicle_charge_income_code
		self.vehicle_charge_outlet_code = vehicle_charge_outlet_code
		self.vehicle_charge_dept_code = vehicle_charge_dept_code
		self.vehicle_charge_category = vehicle_charge_category
		self.vehicle_charge_category_cost = vehicle_charge_category_cost
		self.vehicle_charge_cost_currency = vehicle_charge_cost_currency
		self.vehicle_charge_description = vehicle_charge_description
		self.maximum_number = maximum_number
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Vehicle %r>" % (self.vehicle_charge_category)


	def return_json(self):
		currency = requests.get(get_currency.format(self.vehicle_charge_cost_currency))
		
		return {
			"vehicle_charge_public_id": self.vehicle_charge_public_id,
			"vehicle_charge_income_code": self.vehicle_charge_income_code,
			"vehicle_charge_outlet_code": self.vehicle_charge_outlet_code,
			"vehicle_charge_dept_code": self.vehicle_charge_dept_code,
			"vehicle_charge_category": self.vehicle_charge_category,
			"vehicle_charge_category_cost": float(self.vehicle_charge_category_cost),
			"vehicle_charge_cost_currency_id": self.vehicle_charge_cost_currency,
			"vehicle_charge_description": self.vehicle_charge_description,
			"discount": 0,
			"discount_reason": None,
			"vehicles": 0,
			"max_number": self.maximum_number,
			"vehicle_charge_cost_currency": currency.json()["data"][0]["currency_name"]
		}