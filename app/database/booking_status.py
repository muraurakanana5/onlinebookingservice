from datetime import datetime

from routes import db


class BookingStatus(db.Model):
	__tablename__ = 'booking_status'
	booking_status_id = db.Column(db.Integer, primary_key=True)
	booking_status_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_status_name = db.Column(db.String(255), nullable=False)
	booking_status_description = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	bookings = db.relationship("Booking", backref = "b_status", lazy = "dynamic")


	def __init__(self, booking_status_public_id = None, booking_status_name = None,
				 booking_status_description = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.booking_status_public_id = booking_status_public_id
		self.booking_status_name = booking_status_name
		self.booking_status_description = booking_status_description
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<BookingStatus %r>" % (self.booking_status_name)


	def return_json(self):
		return {
			"booking_status_public_id": self.booking_status_public_id,
			"booking_status_name": self.booking_status_name,
			"booking_status_description": self.booking_status_description,
			"session_id": self.session_id,
			"status": self.status,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}