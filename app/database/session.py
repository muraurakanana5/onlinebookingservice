from datetime import datetime

from routes import db


class SessionTracking(db.Model):
	__tablename__ = 'session_tracking'
	session_tracking_id = db.Column(db.Integer, primary_key=True)
	method = db.Column(db.String(255))
	url = db.Column(db.String(255))
	headers = db.Column(db.Text)
	user_agent = db.Column(db.Text)
	language = db.Column(db.Text)
	platform = db.Column(db.Text)
	browser = db.Column(db.Text)
	created_at = db.Column(db.DateTime)