from datetime import datetime

from routes import db


class Inventory(db.Model):
	__tablename__ = 'inventory_bookings'
	inventory_booking_id = db.Column(db.Integer, primary_key=True)
	inventory_booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	inventory_id = db.Column(db.String(255), nullable=False, index=True)
	inventory_code = db.Column(db.String(255), nullable=False, index=True)
	inventory_booking_date = db.Column(db.DateTime)
	inventory_booking_adults = db.Column(db.Integer, nullable=False)
	inventory_booking_children = db.Column(db.Integer, nullable=False)
	inventory_booking_extra_adults = db.Column(db.Integer)
	inventory_booking_extra_children = db.Column(db.Integer)
	inventory_cost_per_adult = db.Column(db.Float(10,2))
	inventory_cost_per_child = db.Column(db.Float(10,2))
	inventory_cost_currency = db.Column(db.String(255))
	inventory_discount_rate = db.Column(db.Float(10,2), default=0)
	inventory_commission_rate = db.Column(db.Float(10,2), default=0)
	inventory_discount_fee_currency = db.Column(db.String(255))
	inventory_discount_fee = db.Column(db.Float(10,2), default=0)
	inventory_analysis_code = db.Column(db.String(255))
	inventory_adult_cost_at_time = db.Column(db.Float(10,2))
	inventory_child_cost_at_time = db.Column(db.Float(10,2))
	inventory_currency_at_time = db.Column(db.String(255))
	inventory_rate_at_time = db.Column(db.Float(10,2))
	inventory_payment_method = db.Column(db.String(255))
	inventory_timeslot = db.Column(db.String(255))
	inventory_pickup_location = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	inventory_booking_date_before_postpone = db.Column(db.DateTime)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __repr__(self):
		return "<Inventory %r>" % (self.inventory_booking_public_id)