from datetime import datetime

from routes import db


class Gate(db.Model):
	__tablename__ = 'gates'
	gate_id = db.Column(db.Integer, primary_key=True)
	gate_public_id = db.Column(db.String(255), unique=True, index=True)
	gate_name = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, gate_public_id = None, gate_name = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.gate_public_id = gate_public_id
		self.gate_name = gate_name
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Gate %r>" % (self.gate_name)

	
	def return_json(self):
		return {
			"gate_public_id": self.gate_public_id,
			"gate_name": self.gate_name
		}