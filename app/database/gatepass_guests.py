from datetime import datetime

from routes import db


class GatepassGuest(db.Model):
	__tablename__ = 'gatepass_guests'
	gatepass_guest_id = db.Column(db.Integer, primary_key=True)
	gatepass_guest_public_id = db.Column(db.String(255), unique=True, index=True)
	gatepass_id = db.Column(db.String(255), nullable=False, index=True)
	gatepass_guest_type = db.Column(db.String(255), index=True)
	gatepass_guest_count = db.Column(db.Integer, default = 0)
	gatepass_discount_rate = db.Column(db.Float(10,2), default=0)
	gatepass_discount_reason = db.Column(db.Text)
	gatepass_cost_per_pp = db.Column(db.Float(10,2))
	gatepass_payment_schedule = db.Column(db.String(255))
	gatepass_no_of_nights = db.Column(db.Integer)
	gatepass_currency = db.Column(db.String(255))
	gatepass_guest_cost_at_time = db.Column(db.Float(10,2))
	gatepass_guest_currency_at_time = db.Column(db.String(255))
	gatepass_guest_rate_at_time = db.Column(db.Float(10,2))
	gatepass_guest_payment_method = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)