from datetime import datetime

from routes import db


class Facility(db.Model):
    __tablename__ = 'facility_bookings'
    facility_booking_id = db.Column(db.Integer, primary_key=True)
    facility_booking_public_id = db.Column(db.String(255),
                                           unique=True,
                                           index=True)
    booking_id = db.Column(db.String(255),
                           db.ForeignKey("bookings.booking_public_id"),
                           index=True)
    facility_id = db.Column(db.String(255), nullable=False, index=True)
    facility_code = db.Column(db.String(255), nullable=False, index=True)
    facility_booking_check_in_date = db.Column(db.DateTime)
    facility_booking_check_out_date = db.Column(db.DateTime)
    facility_booking_adults = db.Column(db.Integer, nullable=False)
    facility_booking_children = db.Column(db.Integer, nullable=False)
    facility_booking_extra_adults = db.Column(db.Integer)
    facility_booking_extra_children = db.Column(db.Integer)
    facility_cost_per_adult = db.Column(db.Float(10, 2))
    facility_cost_per_child = db.Column(db.Float(10, 2))
    facility_fixed_cost = db.Column(db.Float(10, 2))
    facility_special_price = db.Column(db.Float(10, 2))
    facility_special_price_unit_increase = db.Column(db.Float(10, 2))
    facility_cost_currency = db.Column(db.String(255))
    facility_no_of_nights = db.Column(db.Integer)
    facility_discount_rate = db.Column(db.Float(10, 2), default=0)
    facility_commission_rate = db.Column(db.Float(10, 2), default=0)
    facility_discount_fee_currency = db.Column(db.String(255))
    facility_discount_fee = db.Column(db.Float(10, 2), default=0)
    facility_analysis_code = db.Column(db.String(255))
    facility_adult_cost_at_time = db.Column(db.Float(10, 2))
    facility_child_cost_at_time = db.Column(db.Float(10, 2))
    facility_fixed_cost_at_time = db.Column(db.Float(10, 2))
    facility_special_price_at_time = db.Column(db.Float(10, 2))
    facility_special_price_unit_increase_at_time = db.Column(db.Float(10, 2))
    facility_rate_at_time = db.Column(db.Float(10, 2))
    facility_currency_at_time = db.Column(db.String(255))
    facility_payment_method = db.Column(db.String(255))
    facility_catering_type = db.Column(db.String(255))
    facility_booked_rooms = db.Column(db.Integer)
    session_id = db.Column(db.String(255), index=True)
    status = db.Column(db.String(255), default="1", index=True)
    deletion_marker = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    facility_booking_check_in_date_before_postpone = db.Column(db.DateTime)
    facility_booking_check_out_date_before_postpone = db.Column(db.DateTime)

    def __repr__(self):
        return "<Facility %r>" % (self.facility_booking_public_id)
