from datetime import datetime

from routes import db


class GuestType(db.Model):
	__tablename__ = 'booking_guest_types'
	booking_guest_type_id = db.Column(db.Integer, primary_key=True)
	booking_guest_type_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_guest_type_name = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)