from datetime import datetime

from routes import db
from database.payment_gateways import PaymentGateway


class PaymentMethod(db.Model):
	__tablename__ = 'payment_methods'
	payment_method_id = db.Column(db.Integer, primary_key=True)
	payment_method_public_id = db.Column(db.String(255), unique=True, index=True)
	payment_method_name = db.Column(db.String(255), nullable=False)
	payment_method_description = db.Column(db.String(255), nullable=False)
	payment_method_guest = db.Column(db.Integer)
	payment_method_partner = db.Column(db.Integer)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime, default=datetime.now())
	updated_at = db.Column(db.DateTime, default=datetime.now())
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	payment_gateways = db.relationship("PaymentGateway", backref = "method", lazy = "dynamic")


	def __init__(self, payment_method_public_id = None, payment_method_name = None,
				 payment_method_description = None, payment_method_guest = None, payment_method_partner = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.payment_method_public_id = payment_method_public_id
		self.payment_method_name = payment_method_name
		self.payment_method_description = payment_method_description
		self.payment_method_guest = payment_method_guest
		self.payment_method_partner = payment_method_partner
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<PaymentMethod %r>" % (self.payment_method_name)

	
	def return_json(self):
		get_payment_gateways = self.payment_gateways\
								   .filter(PaymentGateway.deletion_marker == None)\
								   .all()
		
		if get_payment_gateways:
			gateways_set = True
			payment_gateways = []

			for single_gateway in get_payment_gateways:
				payment_gateways.append(single_gateway.return_json())

		else:
			gateways_set = False
			payment_gateways = []
		
		return {
			"payment_method_public_id": self.payment_method_public_id,
			"payment_method_name": self.payment_method_name,
			"payment_method_description": self.payment_method_description,
			"payment_method_guest": self.payment_method_guest,
			"payment_method_partner": self.payment_method_partner,
			"payment_gateways_set": gateways_set,
			"payment_gateways": payment_gateways,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}