from datetime import datetime

from routes import db


class CreditNoteInventory(db.Model):
	__tablename__ = 'credit_note_inventory'
	credit_note_inventory_id = db.Column(db.Integer, primary_key=True)
	credit_note_inventory_public_id = db.Column(db.String(255), unique=True, index=True)
	credit_note_id = db.Column(db.String(255), db.ForeignKey("credit_notes.credit_note_public_id"), index=True)
	inventory_booking_id = db.Column(db.String(255), index=True)
	credit_inventory_booking_adults = db.Column(db.Integer, nullable=False)
	credit_inventory_booking_children = db.Column(db.Integer, nullable=False)
	credit_inventory_booking_extra_adults = db.Column(db.Integer)
	credit_inventory_booking_extra_children = db.Column(db.Integer)
	credit_inventory_date = db.Column(db.DateTime)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)