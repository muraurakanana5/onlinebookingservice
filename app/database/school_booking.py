from datetime import datetime

from routes import db

class SchoolBooking(db.Model):
	__tablename__ = 'school_bookings'
	school_booking_id = db.Column(db.Integer, primary_key=True)
	school_booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	school_id = db.Column(db.String(255), db.ForeignKey("schools.school_public_id"), index=True)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, school_booking_public_id = None, booking_id = None,
				 school_id = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.school_booking_public_id = school_booking_public_id
		self.booking_id = booking_id
		self.school_id = school_id
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<SchoolBooking %r>" % (self.school_booking_public_id)


	def return_json(self):
		return {
			"school_booking_public_id": self.school_booking_public_id,
			"booking_id": self.booking_id,
			"school_id": self.school_id,
			"session_id": self.session_id,
			"created_at": self.created_at
		}