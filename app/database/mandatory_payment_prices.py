from datetime import datetime

import requests

from routes import db
from variables import get_currency


class MandatoryPaymentPrices(db.Model):
	__tablename__ = 'mandatory_payment_prices'
	price_id = db.Column(db.Integer, primary_key=True)
	price_public_id = db.Column(db.String(255), unique=True, index=True)
	payment_schedule = db.Column(db.String(255), db.ForeignKey("payment_schedule_calendar.schedule_public_id"), index=True)
	payment_category = db.Column(db.String(255), db.ForeignKey("mandatory_payments.payment_public_id"), index=True)
	payment_currency = db.Column(db.String(255))
	payment_price = db.Column(db.Float(10,2))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, price_public_id = None, payment_schedule = None,
				 payment_category = None, payment_currency = None, payment_price = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.price_public_id = price_public_id
		self.payment_schedule = payment_schedule
		self.payment_category = payment_category
		self.payment_currency = payment_currency
		self.payment_price = payment_price
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at


	def __repr__(self):
		return "<MandatoryPaymentPrices %r>" % (self.price_public_id)

	
	def return_json(self):
		currency = requests.get(get_currency.format(self.payment_currency))
		
		return {
			"payment_schedule": self.payment_schedule,
			"payment_person_currency_id": self.payment_currency,
			"payment_person_amount": float(self.payment_price),
			"session_id": self.session_id,
			"payment_person_currency": currency.json()["data"][0]["currency_name"],
			"payment_public_id": self.fee_category.payment_public_id,
			"payment_person_income_code": self.fee_category.payment_person_income_code,
			"payment_person_dept_code": self.fee_category.payment_person_dept_code,
			"payment_person":self.fee_category.payment_person,
			"payment_person_description": self.fee_category.payment_person_description,
			"payment_person_discount": 0,
			"payment_guests": 0,
			"marker": self.fee_category.marker,
			"school": self.fee_category.school,
			"amount": 0,
			"max_number": self.fee_category.maximum_number
		}