from datetime import datetime

from routes import db


class Gatepass(db.Model):
	__tablename__ = 'gatepasses'
	gatepass_id = db.Column(db.Integer, primary_key=True)
	gatepass_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), index=True)
	gatepass_date = db.Column(db.DateTime)
	gatepass_done_by = db.Column(db.String(255))
	gatepass_phone_number = db.Column(db.String(255))
	gatepass_payment_status = db.Column(db.Integer)
	destination = db.Column(db.String(255))
	gatepass_ref_code = db.Column(db.String(255), index=True)
	booking = db.Column(db.Integer)
	member_visit = db.Column(db.Integer)
	partner_id = db.Column(db.String(255), index=True)
	member_id = db.Column(db.String(255), index=True)
	start_date = db.Column(db.Date)
	end_date = db.Column(db.Date)
	gatepass_currency = db.Column(db.String(255))
	gatepass_source = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	before_postpone_start_date = db.Column(db.Date)
	before_postpone_end_date = db.Column(db.Date)


	def __repr__(self):
		return "<Gatepass %r>" % (self.gatepass_public_id)