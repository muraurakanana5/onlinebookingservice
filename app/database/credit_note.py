from datetime import datetime

from routes import db


class CreditNote(db.Model):
	__tablename__ = 'credit_notes'
	credit_note_id = db.Column(db.Integer, primary_key=True)
	credit_note_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	credit_note_ref = db.Column(db.String(255), unique=True, index=True)
	credit_note_currency = db.Column(db.String(255))
	credit_note_amount = db.Column(db.Float(16,2))
	credit_note_reason = db.Column(db.Text)
	currency_buying_rate_at_time = db.Column(db.Float(10,2))
	currency_selling_rate_at_time = db.Column(db.Float(10,2))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)