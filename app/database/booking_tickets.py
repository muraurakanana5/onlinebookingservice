from datetime import datetime

from routes import db


class Ticket(db.Model):
	__tablename__ = 'booking_tickets'
	booking_tickets_id = db.Column(db.Integer, primary_key=True)
	booking_tickets_public_id = db.Column(db.String(255), unique=True, index=True)
	ticket_date = db.Column(db.Date)
	first_ticket = db.Column(db.String(255))
	last_ticket = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, booking_tickets_public_id = None, ticket_date = None,
				 first_ticket = None, last_ticket = None, session_id = None,
				 status = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.booking_tickets_public_id = booking_tickets_public_id
		self.ticket_date = ticket_date
		self.first_ticket = first_ticket
		self.last_ticket = last_ticket
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Ticket %r>" % (self.ticket_date)


	def return_json():
		return {
			"booking_tickets_public_id": self.booking_tickets_public_id,
			"ticket_date": self.ticket_date,
			"first_ticket": self.first_ticket,
			"last_ticket": self.last_ticket,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}