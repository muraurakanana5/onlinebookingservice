from datetime import datetime

from routes import db


class CreditNoteFacility(db.Model):
	__tablename__ = 'credit_note_facilities'
	credit_note_facilities_id = db.Column(db.Integer, primary_key=True)
	credit_note_facilities_public_id = db.Column(db.String(255), unique=True, index=True)
	credit_note_id = db.Column(db.String(255), db.ForeignKey("credit_notes.credit_note_public_id"), index=True)
	facility_booking_id = db.Column(db.String(255), index=True)
	credit_facility_booking_adults = db.Column(db.Integer, nullable=False)
	credit_facility_booking_children = db.Column(db.Integer, nullable=False)
	credit_facility_booking_extra_adults = db.Column(db.Integer)
	credit_facility_booking_extra_children = db.Column(db.Integer)
	credit_facility_check_in_date = db.Column(db.DateTime)
	credit_facility_check_out_date = db.Column(db.DateTime)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)