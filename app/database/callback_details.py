from datetime import datetime

from routes import db


class SalesforceDetails(db.Model):
	__tablename__ = 'salesforce_details'
	details_id = db.Column(db.Integer, primary_key=True)
	details_email = db.Column(db.Text)
	details_password = db.Column(db.Text)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, details_email = None, details_password = None,
				 status = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.details_email = details_email
		self.details_password = self.details_password
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<SalesforceDetails %r>" % (self.details_email)

	
	def return_json(self):
		return {
			"details_email": self.details_email,
			"details_password": self.details_password
		}