from datetime import datetime

from routes import db


class Invoice(db.Model):
	__tablename__ = 'bookings_to_invoice'
	invoice_id = db.Column(db.Integer, primary_key=True)
	invoice_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, invoice_public_id = None, booking_id = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.invoice_public_id = invoice_public_id
		self.booking_id = booking_id
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Invoice %r>" % (self.invoice_public_id)

	
	def return_json(self):
		return {
			"invoice_public_id": self.invoice_public_id,
			"booking_id": self.booking_id,
			"session_id": self.session_id,
			"created_at": self.created_at
		}