from datetime import datetime

from routes import db


class BookingPayment(db.Model):
	__tablename__ = 'booking_payments'
	booking_payment_id = db.Column(db.Integer, primary_key=True)
	booking_payment_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), index=True)
	transaction_id = db.Column(db.String(255), index=True)
	payment_method = db.Column(db.String(255), index=True)
	payment_currency = db.Column(db.String(255), index=True)
	phone_number = db.Column(db.String(255))
	mpesa_reference = db.Column(db.String(255))
	card_first_four = db.Column(db.String(10))
	card_last_four = db.Column(db.String(10))
	booking_amount = db.Column(db.Float(10,2))
	amount_paid = db.Column(db.Float(10,2))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime, default=datetime.now())
	updated_at = db.Column(db.DateTime, default=datetime.now())