from datetime import datetime

import requests

from database.facility import Facility
from database.inventory import Inventory
from routes import db
from variables import get_facility_details, get_inventory_details


class Booking(db.Model):
	__tablename__ = 'bookings'
	booking_id = db.Column(db.Integer, primary_key=True)
	booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_type = db.Column(db.String(255), db.ForeignKey("booking_types.booking_type_public_id"), index=True)
	booking_check_in_date = db.Column(db.DateTime)
	booking_check_out_date = db.Column(db.DateTime)
	actual_booking_check_in_date = db.Column(db.DateTime)
	actual_booking_check_out_date = db.Column(db.DateTime)
	booking_done_by = db.Column(db.String(255))
	checked_in = db.Column(db.Integer, nullable=True)
	checked_out = db.Column(db.Integer, nullable=True)
	payment_status = db.Column(db.Integer)
	currency = db.Column(db.String(255))
	currency_buying_rate_at_time = db.Column(db.Float(10,2))
	currency_selling_rate_at_time = db.Column(db.Float(10,2))
	gatepass_id = db.Column(db.String(255), index=True)
	booking_ref_code = db.Column(db.String(255), index=True)
	salesforce = db.Column(db.Integer)
	salesforce_uuid = db.Column(db.String(255), index=True)
	modified = db.Column(db.Integer)
	ticket = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), db.ForeignKey("booking_status.booking_status_public_id"), index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	before_postpone_booking_check_in_date = db.Column(db.DateTime)
	before_postpone_booking_check_out_date = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	facility_bookings = db.relationship("Facility", backref = "booking_facility", lazy = "dynamic")
	inventory_bookings = db.relationship("Inventory", backref = "booking_inventory", lazy = "dynamic")
	member_details = db.relationship("Member", backref = "member_booking_details", lazy = "dynamic")
	vehicle_check_in_details = db.relationship("CheckInVehicle", backref = "vehicle_booking_details", lazy = "dynamic")


	def __repr__(self):
		return "<Booking %r>" % (self.booking_public_id)


	def return_booking_type(self):
		return self.b_type.booking_type_name

	
	def return_booking_status(self):
		return self.b_status.booking_status_name

	
	def return_facilities(self, request_session):
		get_all_facilities = self.facility_bookings\
								 .filter(Facility.deletion_marker == None)\
								 .all()

		facilities = []

		for single_facility in get_all_facilities:
			facility_data = {}
			facility_data["facility_id"] = single_facility.facility_id

			facility_details = requests.get(get_facility_details.format(single_facility.facility_id))
			try:
				facility_data["facility_name"] = facility_details.json()["data"][0]["name"]
			except Exception:
				facility_data["facility_name"] = "N/A"

			facility_data["facility_booking_guests"] = single_facility.facility_booking_adults + single_facility.facility_booking_children\
													   + single_facility.facility_booking_extra_adults + single_facility.facility_booking_extra_children

			facilities.append(facility_data)

		return facilities

	
	def return_inventory(self, request_session):
		get_all_inventory = self.inventory_bookings\
								.filter(Inventory.deletion_marker == None)\
								.all()

		inventory = []

		for single_inventory in get_all_inventory:
			inventory_data = {}
			inventory_data["inventory_id"] = single_inventory.inventory_id
			
			inventory_details = requests.get(get_inventory_details.format(single_inventory.inventory_id))
			try:
				inventory_data["inventory_name"] = inventory_details.json()["data"][0]["name"]
			except Exception:
				inventory_data["inventory_name"] = "N/A"

			inventory_data["inventory_booking_guests"] = single_inventory.inventory_booking_adults + single_inventory.inventory_booking_children\
														 + single_inventory.inventory_booking_extra_adults + single_inventory.inventory_booking_extra_children

			inventory.append(inventory_data)

		return inventory