from datetime import datetime

from routes import db


class MandatoryPaymentScheduleCalendar(db.Model):
	__tablename__ = 'payment_schedule_calendar'
	schedule_id = db.Column(db.Integer, primary_key=True)
	schedule_public_id = db.Column(db.String(255), unique=True, index=True)
	schedule_start_date = db.Column(db.Date)
	schedule_end_date = db.Column(db.Date)
	schedule_destination = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	schedule_entry_fees = db.relationship("MandatoryPaymentPrices", backref = "fee_calendar", lazy = "dynamic")


	def __init__(self, schedule_public_id = None, schedule_start_date = None,
				 schedule_end_date = None, schedule_destination = None, session_id = None,
				 status = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.schedule_public_id = schedule_public_id
		self.schedule_start_date = schedule_start_date
		self.schedule_end_date = schedule_end_date
		self.schedule_destination = schedule_destination
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<MandatoryPaymentScheduleCalendar %r>" % (self.schedule_public_id)


	def __str__(self):
		return "<MandatoryPaymentScheduleCalendar {}, {}, {}>".format(self.schedule_public_id, self.schedule_start_date, self.schedule_end_date)

	
	# def return_prices_json(self):
	# 	return {

	# 	}