from datetime import datetime

from routes import db


class School(db.Model):
	__tablename__ = 'schools'
	school_id = db.Column(db.Integer, primary_key=True)
	school_public_id = db.Column(db.String(255), unique=True, index=True)
	school_name = db.Column(db.String(255))
	school_level = db.Column(db.String(255))
	school_type = db.Column(db.String(255))
	school_gender = db.Column(db.String(255))
	school_code = db.Column(db.String(255), index=True)
	closest_city_town = db.Column(db.String(255))
	primary_contact = db.Column(db.String(255))
	primary_contact_email = db.Column(db.String(255))
	primary_contact_phone = db.Column(db.String(255))
	secondary_contact = db.Column(db.String(255))
	secondary_contact_email = db.Column(db.String(255))
	secondary_contact_phone = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	school_bookings = db.relationship("SchoolBooking", backref = "school_details", lazy = "dynamic")


	def __init__(self, school_public_id = None, school_name = None,
				 school_level = None, school_type = None, school_gender = None,
				 school_code = None, closest_city_town = None, primary_contact = None,
				 primary_contact_email = None, primary_contact_phone = None, secondary_contact = None,
				 secondary_contact_email = None, secondary_contact_phone = None, session_id = None,
				 status = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.school_public_id = school_public_id
		self.school_name = school_name
		self.school_level = school_level
		self.school_type = school_type
		self.school_gender = school_gender
		self.school_code = school_code
		self.closest_city_town = closest_city_town
		self.primary_contact = primary_contact
		self.primary_contact_email = primary_contact_email
		self.primary_contact_phone = primary_contact_phone
		self.secondary_contact = secondary_contact
		self.secondary_contact_email = secondary_contact_email
		self.secondary_contact_phone = secondary_contact_phone
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<School %r>" % (self.school_name)

	
	def return_json(self):
		return {
			"school_public_id": self.school_public_id,
			"school_name": self.school_name.title(),
			"school_level": self.school_level,
			"school_type": self.school_type,
			"school_gender": self.school_gender,
			"school_code": self.school_code,
			"closest_city_town": self.closest_city_town.title(),
			"primary_contact": self.primary_contact,
			"primary_contact_email": self.primary_contact_email,
			"primary_contact_phone": self.primary_contact_phone,
			"secondary_contact": self.secondary_contact,
			"secondary_contact_email": self.secondary_contact_email,
			"secondary_contact_phone": self.secondary_contact_phone,
			"session_id": self.session_id,
			"created_at": self.created_at
		}