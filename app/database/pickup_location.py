from datetime import datetime

from routes import db


class Location(db.Model):
	__tablename__ = 'pickup_locations'
	pickup_location_id = db.Column(db.Integer, primary_key=True)
	pickup_location_public_id = db.Column(db.String(255), unique=True, index=True)
	pickup_location_name = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, pickup_location_public_id = None, pickup_location_name = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.pickup_location_public_id = pickup_location_public_id
		self.pickup_location_name = pickup_location_name
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Location %r>" % (self.pickup_location_name)

	
	def return_json(self):
		return {
			"location_public_id": self.pickup_location_public_id,
			"location_name": self.pickup_location_name,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}