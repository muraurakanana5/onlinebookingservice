from datetime import datetime

import requests

from routes import db
from variables import get_currency


class PaymentGateway(db.Model):
	__tablename__ = 'payment_gateways'
	payment_gateway_id = db.Column(db.Integer, primary_key=True)
	payment_gateway_public_id = db.Column(db.String(255), unique=True, index=True)
	payment_method = db.Column(db.String(255), db.ForeignKey("payment_methods.payment_method_public_id"), index=True)
	payment_gateway_name = db.Column(db.String(255), nullable=False)
	payment_gateway_code = db.Column(db.String(255), nullable=False)
	payment_gateway_currency = db.Column(db.String(255), nullable=False)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, payment_gateway_public_id = None, payment_method = None,
				 payment_gateway_name = None, payment_gateway_code = None, payment_gateway_currency = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.payment_gateway_public_id = payment_gateway_public_id
		self.payment_method = payment_method
		self.payment_gateway_name = payment_gateway_name
		self.payment_gateway_code = payment_gateway_code
		self.payment_gateway_currency = payment_gateway_currency
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at


	def __repr__(self):
		return "<PaymentGateway %r>" % (self.payment_gateway_name)

	
	def return_json(self):
		currency = requests.get(get_currency.format(self.payment_gateway_currency))
		
		return {
			"payment_gateway_public_id": self.payment_gateway_public_id,
			"payment_method": self.payment_method,
			"payment_gateway_name": self.payment_gateway_name,
			"payment_gateway_code": self.payment_gateway_code,
			"payment_gateway_currency": self.payment_gateway_currency,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"payment_gateway_currency_name": currency.json()["data"][0]["currency_name"],
			"payment_method_name": self.method.payment_method_name
		}