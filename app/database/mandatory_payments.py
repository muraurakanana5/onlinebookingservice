from datetime import datetime

from routes import db


class Mandatory(db.Model):
	__tablename__ = 'mandatory_payments'
	payment_id = db.Column(db.Integer, primary_key=True)
	payment_public_id = db.Column(db.String(255), unique=True, index=True)
	payment_person_income_code = db.Column(db.String(255))
	payment_person_dept_code = db.Column(db.String(255))
	payment_person = db.Column(db.String(255))
	payment_person_description = db.Column(db.String(255))
	marker = db.Column(db.String(20))
	maximum_number = db.Column(db.Integer, nullable=False)
	school = db.Column(db.String(10))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	category_entry_fees = db.relationship("MandatoryPaymentPrices", backref = "fee_category", lazy = "dynamic")


	def __init__(self, payment_public_id = None, payment_person_income_code = None,
				 payment_person_dept_code = None, payment_person = None, payment_person_description = None,
				 marker = None, maximum_number = None, school = None,
				 session_id = None, status = None, deletion_marker = None,
				 created_at = None, updated_at = None):
		self.payment_public_id = payment_public_id
		self.payment_person_income_code = payment_person_income_code
		self.payment_person_dept_code = payment_person_dept_code
		self.payment_person = payment_person
		self.payment_person_description = payment_person_description
		self.marker = marker
		self.maximum_number = maximum_number
		self.school = school
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Mandatory %r>" % (self.payment_person)

	
	# def return_json(self):
	# 	return {

	# 	}