from datetime import datetime

from routes import db


class VehicleDetail(db.Model):
	__tablename__ = 'vehicle_details'
	vehicle_details_id = db.Column(db.Integer, primary_key=True)
	vehicle_details_public_id = db.Column(db.String(255), unique=True, index=True)
	gatepass_id = db.Column(db.String(255), nullable=False, index=True)
	vehicle_driver = db.Column(db.String(255))
	vehicle_registration_plate = db.Column(db.String(255))
	vehicle_assigned_disk = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime, default=datetime.now())
	updated_at = db.Column(db.DateTime, default=datetime.now())