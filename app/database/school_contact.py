from datetime import datetime

from routes import db


class SchoolContact(db.Model):
	__tablename__ = 'school_contacts'
	school_contact_id = db.Column(db.Integer, primary_key=True)
	school_contact_public_id = db.Column(db.String(255), unique=True, index=True)
	school_id = db.Column(db.String(255), db.ForeignKey("schools.school_public_id"), index = True)
	primary_contact = db.Column(db.String(255))
	primary_contact_email = db.Column(db.String(255))
	primary_contact_phone = db.Column(db.String(255))
	secondary_contact = db.Column(db.String(255))
	secondary_contact_email = db.Column(db.String(255))
	secondary_contact_phone = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)