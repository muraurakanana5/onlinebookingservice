from datetime import datetime

from routes import db


class PromoCode(db.Model):
	__tablename__ = 'booking_promo_codes'
	booking_promo_id = db.Column(db.Integer, primary_key=True)
	booking_promo_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	booking_promo_code = db.Column(db.String(255), nullable=False)
	booking_promo_code_application = db.Column(db.String(80), nullable=False)
	booking_promo_precentage = db.Column(db.Float(10,2), default=0)
	booking_promo_fee_currency = db.Column(db.String(255))
	booking_promo_fee = db.Column(db.Float(10,2), default=0)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, booking_promo_public_id = None, booking_id = None,
				 booking_promo_code = None, booking_promo_code_application = None, booking_promo_precentage = None,
				 booking_promo_fee_currency = None, booking_promo_fee = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.booking_promo_public_id = booking_promo_public_id
		self.booking_id = booking_id
		self.booking_promo_code = booking_promo_code
		self.booking_promo_code_application = booking_promo_code_application
		self.booking_promo_precentage = booking_promo_precentage
		self.booking_promo_fee_currency = booking_promo_fee_currency
		self.booking_promo_fee = booking_promo_fee
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at


	def __repr__(self):
		return "<PromoCode %r>" % (self.booking_promo_public_id)

	
	def return_json(self):
		return {
			"booking_promo_public_id": self.booking_promo_public_id,
			"booking_id": self.booking_id,
			"booking_promo_code": self.booking_promo_code,
			"booking_promo_code_application": self.booking_promo_code_application,
			"booking_promo_precentage": self.booking_promo_precentage,
			"booking_promo_fee_currency": self.booking_promo_fee_currency,
			"booking_promo_fee": self.booking_promo_fee,
			"created_at": self.created_at
		}