from datetime import datetime

from routes import db


class SalesforceData(db.Model):
	__tablename__ = 'saleforce_tracking'
	salesforce_tracking_id = db.Column(db.Integer, primary_key=True)
	salesforce_tracking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), nullable=False, index=True)
	date_done = db.Column(db.DateTime, default=datetime.now())
	salesforce_uuid = db.Column(db.String(255), nullable=False, index=True)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime, default=datetime.now())
	updated_at = db.Column(db.DateTime, default=datetime.now())