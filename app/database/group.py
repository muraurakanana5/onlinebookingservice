from datetime import datetime

from routes import db


class Group(db.Model):
	__tablename__ = 'group_bookings'
	group_booking_id = db.Column(db.Integer, primary_key=True)
	group_booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	organisation_name = db.Column(db.String(255))
	organisation_email = db.Column(db.String(255))
	organisation_type = db.Column(db.String(255), index=True)
	session_id = db.Column(db.String(255), index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, group_booking_public_id = None, booking_id = None,
				 organisation_name = None, organisation_email = None, organisation_type = None,
				 session_id = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.group_booking_public_id = group_booking_public_id
		self.booking_id = booking_id
		self.organisation_name = organisation_name
		self.organisation_email = organisation_email
		self.organisation_type = organisation_type
		self.session_id = session_id
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at


	def __repr__(self):
		return "<Group %r>" % (self.group_booking_public_id)


	def return_json(self):
		return {
			"group_booking_public_id": self.group_booking_public_id,
			"booking_id": self.booking_id,
			"organisation_name": self.organisation_name,
			"organisation_email": self.organisation_email,
			"organisation_type": self.organisation_type,
			"session_id": self.session_id,
			"created_at": self.created_at
		}