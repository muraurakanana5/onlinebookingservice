from datetime import datetime

from routes import db


class SalesforceCode(db.Model):
	__tablename__ = 'salesforce_codes'
	salesforce_code = db.Column(db.String(255), primary_key=True)
	access_token = db.Column(db.String(255))
	refresh_token = db.Column(db.String(255))
	instance_url = db.Column(db.String(255))
	deletion_marker = db.Column(db.Integer)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	

	def __init__(self, salesforce_code = None, access_token = None,
	refresh_token = None, instance_url = None, deletion_marker = None,
	created_at = None,	updated_at = None):
		self.salesforce_code = salesforce_code
		self.access_token = access_token
		self.refresh_token = refresh_token
		self.instance_url = instance_url
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at
	
	
	def __repr__(self):
		return "<SalesforceCode %r>" % (self.salesforce_code)

	
	def return_json(self):
		return {
			"salesforce_code": self.salesforce_code,
			"access_token": self.access_token,
			"refresh_token": self.refresh_token,
			"instance_url": self.instance_url,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}