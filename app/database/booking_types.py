from datetime import datetime

from routes import db


class BookingType(db.Model):
	__tablename__ = 'booking_types'
	booking_type_id = db.Column(db.Integer, primary_key=True)
	booking_type_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_type_name = db.Column(db.String(255), nullable=False)
	booking_type_description = db.Column(db.String(255), nullable=False)
	booking_type_public = db.Column(db.Integer)
	booking_type_back = db.Column(db.Integer)
	booking_type_group = db.Column(db.Integer)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)
	## Ref
	# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database-legacy
	# https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
	bookings = db.relationship("Booking", backref = "b_type", lazy = "dynamic")


	def __init__(self, booking_type_public_id = None, booking_type_name = None,
				 booking_type_description = None, booking_type_public = None, booking_type_back = None,
				 booking_type_group = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.booking_type_public_id = booking_type_public_id
		self.booking_type_name = booking_type_name
		self.booking_type_description = booking_type_description
		self.booking_type_public = booking_type_public
		self.booking_type_back = booking_type_back
		self.booking_type_group = booking_type_group
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<BookingType %r>" % (self.booking_type_name)


	def return_json(self):
		return {
			"booking_type_public_id": self.booking_type_public_id,
			"booking_type_name": self.booking_type_name,
			"booking_type_description": self.booking_type_description,
			"booking_type_public": self.booking_type_public,
			"booking_type_back": self.booking_type_back,
			"booking_type_group": self.booking_type_group,
			"session_id": self.session_id,
			"status": self.status,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}