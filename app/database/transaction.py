from datetime import datetime

from routes import db


class Transaction(db.Model):
	__tablename__ = 'transactions'
	transaction_booking_id = db.Column(db.Integer, primary_key=True)
	transaction_booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), nullable=False, index=True)
	transaction_original_cost = db.Column(db.Float(10,2))
	transaction_total = db.Column(db.Float(10,2))
	transaction_total_currency = db.Column(db.String(255))
	transaction_balance = db.Column(db.Float(10,2))
	transaction_comment = db.Column(db.String(255))
	transaction_payment_method = db.Column(db.String(255), index=True)
	transaction_payment_gateway = db.Column(db.String(255))
	transaction_payment_currency = db.Column(db.String(255), index=True)
	payment_currency_buying_rate_at_time = db.Column(db.Float(10,2))
	payment_currency_selling_rate_at_time = db.Column(db.Float(10,2))
	transaction_date = db.Column(db.DateTime)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)