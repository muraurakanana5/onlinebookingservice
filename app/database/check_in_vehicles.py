from datetime import datetime

from routes import db


class CheckInVehicle(db.Model):
	__tablename__ = "check_in_vehicles"
	check_vehicle_id = db.Column(db.Integer, primary_key=True)
	check_vehicle_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), db.ForeignKey("bookings.booking_public_id"), index=True)
	check_vehicle_driver = db.Column(db.String(255))
	check_vehicle_phone_number = db.Column(db.String(255))
	check_vehicle_reg = db.Column(db.String(255))
	check_vehicle_disc = db.Column(db.String(255))
	driver_salesforce_uuid = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, check_vehicle_public_id = None, booking_id = None,
				 check_vehicle_driver = None, check_vehicle_phone_number = None, check_vehicle_reg = None,
				 check_vehicle_disc = None, driver_salesforce_uuid = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.check_vehicle_public_id = check_vehicle_public_id
		self.booking_id = booking_id
		self.check_vehicle_driver = check_vehicle_driver
		self.check_vehicle_phone_number = check_vehicle_phone_number
		self.check_vehicle_reg = check_vehicle_reg
		self.check_vehicle_disc = check_vehicle_disc
		self.driver_salesforce_uuid = driver_salesforce_uuid
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		"<CheckInVehicle %r>" % (self.check_vehicle_reg)


	def return_json(self):
		return {
			"check_vehicle_public_id": self.check_vehicle_public_id,
			"booking_id": self.booking_id,
			"check_vehicle_driver": self.check_vehicle_driver.title(),
			"check_vehicle_phone_number": self.check_vehicle_phone_number,
			"check_vehicle_reg": self.check_vehicle_reg.upper(),
			"check_vehicle_disc": self.check_vehicle_disc,
			"session_id": self.session_id,
			"created_at": self.created_at
		}