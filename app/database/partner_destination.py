from datetime import datetime

from routes import db


class PartnerDestination(db.Model):
	__tablename__ = 'partner_destinations'
	partner_destination_id = db.Column(db.Integer, primary_key=True)
	partner_destination_public_id = db.Column(db.String(255), unique=True, index=True)
	partner_id = db.Column(db.String(255), index=True)
	destination_id = db.Column(db.String(255), db.ForeignKey("gatepass_destinations.gatepass_destination_public_id"), index=True)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, partner_destination_public_id = None, partner_id = None,
				 destination_id = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.partner_destination_public_id = partner_destination_public_id
		self.partner_id = partner_id
		self.destination_id = destination_id
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<PartnerDestination %r>" % (self.partner_destination_public_id)

	
	def return_json(self):
		return {
			"partner_destination_public_id": self.partner_destination_public_id,
			"partner_id": self.partner_id,
			"destination_id": self.destination_id,
			"destination_name": self.destination_details.gatepass_destination_name,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}