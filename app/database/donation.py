from datetime import datetime

from routes import db


class Donation(db.Model):
	__tablename__ = 'booking_donations'
	donation_id = db.Column(db.Integer, primary_key=True)
	donation_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), index=True)
	donation_currency = db.Column(db.String(255))
	donation_cause = db.Column(db.String(255))
	donation_amount = db.Column(db.Float(18,6))
	currency_buying_amount = db.Column(db.Float(10,2))
	currency_selling_amount = db.Column(db.Float(10,2))
	session_id = db.Column(db.String(255), index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)