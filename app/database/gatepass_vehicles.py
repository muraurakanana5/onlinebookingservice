from datetime import datetime

from routes import db


class GatepassVehicle(db.Model):
	__tablename__ = 'gatepass_vehicles'
	gatepass_vehicle_id = db.Column(db.Integer, primary_key=True)
	gatepass_vehicle_public_id = db.Column(db.String(255), unique=True, index=True)
	gatepass_id = db.Column(db.String(255), nullable=False, index=True)
	gatepass_vehicle_type = db.Column(db.String(255), index=True)
	gatepass_vehicle_count = db.Column(db.Integer, default = 0)
	gatepass_cost_per_vehicle = db.Column(db.Float(10,2))
	gatepass_vehicle_currency = db.Column(db.String(255))
	gatepass_vehicle_no_of_nights = db.Column(db.Integer)
	gatepass_vehicle_discount_rate = db.Column(db.Float(10,2), default=0)
	gatepass_vehicle_discount_reason = db.Column(db.Text)
	gatepass_vehicle_cost_at_time = db.Column(db.Float(10,2))
	gatepass_vehicle_currency_at_time = db.Column(db.String(255))
	gatepass_vehicle_rate_at_time = db.Column(db.Float(10,2))
	gatepass_vehicle_payment_method = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)