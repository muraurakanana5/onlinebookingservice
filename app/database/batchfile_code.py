from datetime import datetime

from routes import db


class BatchfileCode(db.Model):
	__tablename__ = 'batchfile_codes'
	batchfile_code = db.Column(db.String(255), primary_key=True)
	

	def __init__(self, batchfile_code = None):
		self.batchfile_code = batchfile_code
	
	
	def __repr__(self):
		return "<BatchfileCode %r>" % (self.batchfile_code)

	
	def return_json(self):
		return {
			"batchfile_code": self.batchfile_code
		}