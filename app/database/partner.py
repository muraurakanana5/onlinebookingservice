from datetime import datetime

from routes import db


class Partner(db.Model):
	__tablename__ = 'partner_bookings'
	partner_booking_id = db.Column(db.Integer, primary_key=True)
	partner_booking_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), index=True)
	partner_id = db.Column(db.String(255), nullable=False, index=True)
	partner_booking_ref = db.Column(db.String(255))
	agent_email = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, partner_booking_public_id = None, booking_id = None,
				 partner_id = None, partner_booking_ref = None, agent_email = None,
				 session_id = None, deletion_marker = None, created_at = None,
				 updated_at = None):
		self.partner_booking_public_id = partner_booking_public_id
		self.booking_id = booking_id
		self.partner_id = partner_id
		self.partner_booking_ref = partner_booking_ref
		self.agent_email = agent_email
		self.session_id = session_id
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<Partner %r>" % (self.partner_booking_public_id)

	
	def return_json(self):
		return {
			"partner_booking_public_id": self.partner_booking_public_id,
			"booking_id": self.booking_id,
			"partner_id": self.partner_id,
			"partner_booking_ref": self.partner_booking_ref,
			"agent_email": self.agent_email,
			"session_id": self.session_id,
			"created_at": self.created_at
		}