from datetime import datetime

import requests

from routes import db
from variables import get_single_user


class BookingActivity(db.Model):
	__tablename__ = 'booking_activities_log'
	booking_activity_id = db.Column(db.Integer, primary_key=True)
	booking_activity_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), nullable=False, index=True)
	booking_activity_description = db.Column(db.Text)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)


	def __init__(self, booking_activity_public_id = None, booking_id = None,
				 booking_activity_description = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None):
		self.booking_activity_public_id = booking_activity_public_id
		self.booking_id = booking_id
		self.booking_activity_description = booking_activity_description
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at

	
	def __repr__(self):
		return "<BookingActivity %r>" % (self.booking_activity_public_id)


	def return_json(self):
		if self.session_id:
			user = requests.get(get_single_user.format(self.session_id))

			name = user.json()["first_name"] + " " + user.json()["last_name"]

		else:
			name = None
		
		return {
			"booking_activity_public_id": self.booking_activity_public_id,
			"booking_id": self.booking_id,
			"booking_activity_description": self.booking_activity_description,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"session_user": name
		}