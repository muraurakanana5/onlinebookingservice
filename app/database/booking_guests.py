from datetime import datetime

from routes import db


class BookingGuest(db.Model):
	__tablename__ = 'booking_guests'
	booking_guest_id = db.Column(db.Integer, primary_key=True)
	booking_guest_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), nullable=False, index=True)
	guest_type = db.Column(db.String(255), index=True)
	guest_count = db.Column(db.Integer)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime, default=datetime.now())
	updated_at = db.Column(db.DateTime, default=datetime.now())