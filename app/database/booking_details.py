from datetime import datetime

from routes import db


class Detail(db.Model):
	__tablename__ = 'booking_details'
	booking_details_id = db.Column(db.Integer, primary_key=True)
	booking_details_public_id = db.Column(db.String(255), unique=True, index=True)
	booking_id = db.Column(db.String(255), nullable=False, index=True)
	first_name = db.Column(db.String(255))
	last_name = db.Column(db.String(255))
	email_address = db.Column(db.String(255))
	phone_number = db.Column(db.String(255))
	address = db.Column(db.String(255))
	country = db.Column(db.String(255))
	country_code = db.Column(db.String(255))
	contact_uuid = db.Column(db.String(255), index=True)
	city = db.Column(db.String(255))
	additional_note = db.Column(db.String(255))
	session_id = db.Column(db.String(255), index=True)
	updated_by = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)