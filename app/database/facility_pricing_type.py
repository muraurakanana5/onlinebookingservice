from datetime import datetime

from routes import db


class FacilityPricing(db.Model):
	__tablename__ = 'facility_pricing_types'
	facility_pricing_type_id = db.Column(db.Integer, primary_key=True)
	facility_pricing_type_public_id = db.Column(db.String(255), unique=True, index=True)
	facility_pricing_name = db.Column(db.String(255))
	facility_pricing_special = db.Column(db.Integer)
	session_id = db.Column(db.String(255), index=True)
	status = db.Column(db.String(255), default="1", index=True)
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, facility_pricing_type_public_id = None, facility_pricing_name = None,
				 facility_pricing_special = None, session_id = None, status = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.facility_pricing_type_public_id = facility_pricing_type_public_id
		self.facility_pricing_name = facility_pricing_name
		self.facility_pricing_special = facility_pricing_special
		self.session_id = session_id
		self.status = status
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at


	def __repr__(self):
		return "<FacilityPricing %r>" % (self.facility_pricing_name)

	
	def return_json(self):
		return {
			"facility_pricing_type_public_id": self.facility_pricing_type_public_id,
			"facility_pricing_name": self.facility_pricing_name,
			"session_id": self.session_id,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}