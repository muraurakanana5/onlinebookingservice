from datetime import datetime

from routes import db


class ContactList(db.Model):
	__tablename__ = 'contact_list'
	contact_id = db.Column(db.Integer, primary_key=True)
	contact_public_id = db.Column(db.String(255), unique=True, index=True)
	contact_email = db.Column(db.String(255))
	deletion_marker = db.Column(db.Integer, nullable=True)
	created_at = db.Column(db.DateTime)
	updated_at = db.Column(db.DateTime)


	def __init__(self, contact_public_id = None, contact_email = None,
				 deletion_marker = None, created_at = None, updated_at = None):
		self.contact_public_id = contact_public_id
		self.contact_email = contact_email
		self.deletion_marker = deletion_marker
		self.created_at = created_at
		self.updated_at = updated_at

	
	def __repr__(self):
		return "<ContactList %r>" % (self.contact_email)

	
	def return_json(self):
		return {
			"contact_public_id": self.contact_public_id,
			"contact_email": self.contact_email,
			"created_at": self.created_at,
			"updated_at": self.updated_at
		}