FROM php:7.0-apache

RUN apt-get update && apt-get install -y python3 python-dev libmysqlclient-dev\
    build-essential python-pip libapache2-mod-wsgi nano
RUN mkdir -p /visitors_service

RUN apt-get install -y wget
RUN apt-get install -y libxrender1 libfontconfig libxext6
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN tar vxf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN cp wkhtmltox/bin/wk* /usr/local/bin/
RUN chmod +x /usr/local/bin/wkhtmltopdf

WORKDIR /visitors_service

ADD . /visitors_service/
RUN pip install -r requirements.txt

ENV APACHE_DOCUMENT_ROOT /visitors_service/visitors_service/app
ENV PYTHONPATH=/visitors_service/visitors_service/app

RUN cp apache/000-default.conf /etc/apache2/sites-available/000-default.conf