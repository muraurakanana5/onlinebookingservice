### Payment Methods URLs
These URLs are used to perform basic CRUD functions on payment methods.

    5005/payments/methods/new, methods = ["POST"]
        Fields accepted:
            1. "name" : The name of the payment method
            2. "description"
            3. "session_id"

    5005/payments/methods/view
        Fields returned in "data":
            1. "payment_method_public_id"
            2. "payment_method_name"
            3. "payment_method_description"
            4. "payment_method_guest"
            5. "payment_method_partner"
            6. "session_id"
            7. "created_at"
            8. "updated_at"

    5005/payments/methods/delete/, methods = ["PATCH"]
        Fields accepted:
            1. "method_id" : The method public id.
            2. "session_id"


### Booking Types URLs
These URLs are used to perform basic CRUD functions on booking types.

    5005/bookings/types/new, methods = ["POST"]
        Fields accepted:
            1. "name" : The name of the booking type
            2. "description" : The description
            3. "session_id"

    5005/bookings/types/view
        Fields returned in "data":
            1. "booking_type_public_id"
            2. "booking_type_name"
            3. "booking_type_description"
            4. "booking_type_public"
            5. "booking_type_back"
            6. "session_id"
            7. "created_at"
            8. "updated_at"

    5005/bookings/types/view/<booking_type_id>
        <booking_type_id> is the public id.
        Fields returned in "data":
            1. "booking_type_public_id"
            2. "booking_type_name"
            3. "booking_type_description"
            4. "booking_type_public"
            5. "booking_type_back"
            6. "session_id"
            7. "created_at"
            8. "updated_at"

    5005/bookings/types/modify, methods = ["PATCH"]
        Fields accepted:
            1. "name"
            2. "type_id" : The public id of the booking type
            3. "description"
            4. "session_id
    
    5005/bookings/types/delete, methods = ["DELETE"]
        Fields accepted:
            1. "type_id" : The public id of the booking type
            2. "session_id


### Bookings URLs
These URLs are used to perform basic CRUD functions on bookings.

    5005/bookings/back/types/view (This URL is to be used in dashboards when selecting the booking types.)
        Fields returned in "data":
            1. "booking_type_public_id"
            2. "booking_type_name"
            3. "booking_type_description"
            4. "session_id"
            5. "created_at"
            6. "updated_at"

    5005/bookings/public/types/view (This URL is to be used on the public portal when selecting the booking types.)
        Fields returned in "data":
            1. "booking_type_public_id"
            2. "booking_type_name"
            3. "booking_type_description"
            4. "session_id"
            5. "created_at"
            6. "updated_at"
    
    5005/bookings/new, methods = ["POST"]
        Fields accepted:
            1. "type" : The booking type
            2. "check_in"
            3. "check_out"
            4. "guests" : Array
                In array:
                    [{"payment_guests": <int>, "payment_person": <string>, "payment_public_id": <string>}] 
            5. "facilities" : Array
                In array:
                    [{"public_id": "<string>, "adults": <int>, "children": <int>}] 
            6. "inventory" : Array
                In array:
                    [{"public_id": "<string>, "date": "<date>, "adults": <int>, "children": <int>}]
            7. "vehicles" : Array
                In array:
                    [{"vehicle_charge_public_id": <string>, "vehicles": <int>}]
            8. "first_name"
            9. "last_name"
            10. "email"
            11. "phone"
            12. "address"
            13. "additional_note" : (Optional)
            14. "session_id"

    5005/bookings/view
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/<booking_id>
        <booking_id> is the public id.
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "actual_booking_check_in_date"
            7. "actual_booking_check_out_date"
            8. "booking_done_by"
            9. "guests"
            10. "guest_total"
            11. "booking_status"
            12. "booking_status_id"
            13. "session_id"
            14. "created_at"
            15. "updated_at"

    5005/bookings/view/checked_in
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/confirmed
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/unconfirmed
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/check_in/today
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/check_out/today
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/today
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/view/abandoned
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/bookings/confirm, methods = ["PATCH"]
        Fields accepted:
            1. "booking_id" : The booking public id
            2. "session_id"
    
    5005/bookings/check_in, methods = ["PATCH"]
        Fields accepted:
            1. "booking_id" : The booking public id
            2. "session_id"
            3. "vehicles" : Array
                In array:
                [{"reg_plate": <string>, "disk": <string>, "driver_name": <string>}]
            4. "source"

    5005/bookings/check_out, methods = ["PATCH"]
        Fields accepted:
            1. "booking_id" : The booking public id
            2. "session_id"

    5005/bookings/cancel, methods = ["PATCH"]
        Fields accepted:
            1. "booking_id" : The booking public id
            2. "session_id"
    
    5005/bookings/modify, methods = ["PATCH"]
        Fields accepted:
            1. "booking_id" : The booking public id
            2. "new_check_in"
            3. "new_check_out"
            4. "guests" : Array
                In array:
                    [{"payment_guests": <int>, "check_in": <date>, "check_out": <date> "payment_person": <string>, "payment_public_id": <string>}] 
            5. "facilities" : Array
                In array:
                    [{"public_id": <string>, "adults": <int>, "children": <int>}] 
            6. "inventory" : Array
                In array:
                    [{"public_id": <string>, "date": <date>, "adults": <int>, "children": <int>}]
            7. "vehicles" : Array
                In array:
                    [{"vehicle_charge_public_id": <string>, "vehicles": <int>}]
            8. "session_id"
    
    5005/bookings/delete, methods = ["DELETE"]
        Fields accepted:
            1. "booking_id" : The booking public id
            2. "session_id"


### Express Bookings URLs
These URLs are used to perform basic CRUD functions on express bookings.

    5005/bookings/express/new, methods = ["POST"]
        Fields accepted:
            1. "type" : The booking type
            2. "check_in"
            3. "check_out"
            4. "ea_adult_citizen" : Number (if no number is set, assign 0)
            5. "ea_adult_resident" : Number (if no number is set, assign 0)
            6. "non_resident_adult" : Number (if no number is set, assign 0)
            7. "ea_child_citizen" : Number (if no number is set, assign 0)
            8. "ea_child_resident" : Number (if no number is set, assign 0)
            9. "non_resident_child" : Number (if no number is set, assign 0)
            10. "infant" : Number (if no number is set, assign 0)
            11. "facilities" : Array
            12. "inventory" : Array
            13. "first_name"
            14. "last_name"
            15. "email"
            16. "phone"
            17. "address"
            18. "additional_note" : (Optional)
            19. "session_id"


### Inventory Booking URLs
These URLs are used to perform basic CRUD functions on inventory booking.

    5005/bookings/inventory/new, methods = ["POST"]
        Fields accepted are:
            1. "booking_id" : The booking public id
            2. "inventory_id" : The inventory public id
            3. "date"
            4. "guests" : The number of guests
            5. "session_id"

    5005/bookings/inventory/unavailable/date, methods = ["POST"]
        Returns unavailable activities based on date.
        Fields accepted are:
            1. "date"

    5005/bookings/inventory/unavailable/inventory, methods = ["POST"]
        Returns unavailable activities based on inventory.
        Fields accepted are:
            1. "inventory_id"

    5005/bookings/inventory/view/by_id_date, methods=["POST"]
        Returns available slots for an inventory item.
        Fields accepted are:
            1. "inventory_id"
            2. "date"

    5005/bookings/inventory/search, methods=["POST"]
        Returns all bookings for an inventory item.
        Fields accepted are:
            1. "inventory_id"
            2. "start_date"
            3. "end_date"
            4. "session_id"

    5005/bookings/inventory/view/schedule, methods=["POST"]
        Returns bookings(today, tomorrow, this week and next week) for an inventory item.
        Field accepted is:
            1. "inventory_id"

    5005/bookings/inventory/view
        Fields returned in "data":
            1. "inventory_booking_public_id" : The inventory booking public id
            2. "booking_id" : The booking public id
            3. "inventory_id" : The inventory public id
            4. "inventory_booking_date"
            5. "inventory_booking_guests"
            6. "session_id"

    5005/bookings/inventory/view/<inventory_id>
        <inventory_id> is the inventory booking public id.
        Fields returned in "data":
            1. "inventory_booking_public_id" : The inventory booking public id
            2. "booking_id" : The booking public id
            3. "inventory_id" : The inventory public id
            4. "inventory_booking_date"
            5. "inventory_booking_guests"
            6. "session_id"

    5005/bookings/inventory/edit, methods = ["PATCH"]
        Fields accepted are:
            1. "inventory_booking_id" : The inventory booking public id
            2. "date"
            3. "session_id"

    5005/bookings/inventory/delete, methods = ["DELETE"]
        Fields accepted are:
            1. "inventory_booking_id" : The inventory booking public id
            2. "session_id"


### Facility Booking URLs
These URLs are used to perform basic CRUD functions on facility booking.

    5005/bookings/facility/new, methods = ["POST"]
        Fields accepted are:
            1. "booking_id" : The booking public id
            2. "facility_id" : The inventory public id
            3. "check_in_date"
            4. "check_out_date"
            5. "guests" : The number of guests
            6. "session_id"

    5005/bookings/facility/available/date, methods = ["POST"]
        Returns available activities based on date.
        Fields accepted are:
            1. "check_in_date"
            2. "check_out_date"

    5005/bookings/facility/unavailable/facility, methods = ["POST"]
        Returns unavailable activities based on facility.
        Fields accepted are:
            1. "facility_id"

    5005/bookings/facility/view/schedule, methods=["POST"]
        Returns bookings(today, tomorrow, this week and next week) for an facility item.
        Field accepted is:
            1. "facility_id"

    5005/bookings/facility/view
        Fields returned in "data":
            1. "facility_booking_public_id" : The facility booking public id
            2. "booking_id" : The booking public id
            3. "facility_id" : The facility public id
            4. "facility_booking_check_in_date"
            5. "facility_booking_check_out_date"
            6. "facility_booking_guests"
            7. "session_id"

    5005/bookings/facility/view/<facility_id>
        <facility_id> is the facility booking public id.
        Fields returned in "data":
            1. "booking_id" : The booking public id
            2. "facility_id" : The facility public id
            3. "facility_booking_check_in_date"
            4. "facility_booking_check_out_date"
            5. "facility_booking_guests"
            6. "session_id"

    5005/bookings/facility/edit, methods = ["PATCH"]
        Fields accepted are:
            1. "facility_booking_id" : The facility booking public id
            2. "check_in_date"
            3. "check_out_date"
            4. "session_id"

    5005/bookings/facility/delete, methods = ["DELETE"]
        Fields accepted are:
            1. "facility_booking_id" : The facility booking public id
            2. "session_id"


### Search URLs
These are used to perform search functions. They are typically POST requests.

    5005/bookings/search/by_key, methods = ["POST"]
        Fields accepted are:
            1. "key" : This is the string or substring that is queried. It can be a name, public id etc.

    5005/bookings/search/by_date/single, methods = ["POST"]
        Fields accepted are:
            1. "date" : This is the date or sub-date (eg 2018-01) that is queried.

    5005/bookings/search/by_date/range/check_in, methods = ["POST"]
        This returns all records where booking check in dates are within a certain range.
        Fields accepted are:
            1. "start_date"
            2. "end_date

    5005/bookings/search/by_date/range/check_out, methods = ["POST"]
        This returns all records where booking check out dates are within a certain range.
        Fields accepted are:
            1. "start_date"
            2. "end_date

    5005/bookings/search/by_date/range/check_in_out, methods = ["POST"]
        This returns all records where the booking check in date is greater than/equal to the start_date and the
        booking check out date is less than/equal to the end_date.
        Fields accepted are:
            1. "start_date"
            2. "end_date


### Payments URLs
These URLs are used to perform basic CRUD functions on conservation fees.

    5005/payments/default/new" methods = ["POST"]
        Fields accepted are:
            1. "person"
            2. "currency"
            3. "amount"
            4. "marker" : Can only be one of either "A" (Adult) or "C" (Child)
            5. "max_number"
            6. "session_id"
    
    5005/payments/default/view
        Fields returned in "data":
            1. "payment_public_id"
            2. "payment_person"
            3. "payment_person_currency"
            4. "payment_person_amount"
            5. "payment_person_discount"
            6. "payment_guests"
            7. "marker"
            8. "amount"
            9. "session_id"

    5005/payments/default/view/public
        Fields returned in "data":
            1. "payment_public_id"
            2. "payment_person"
            3. "payment_person_currency"
            4. "payment_person_amount"
            5. "payment_person_discount"
            6. "payment_guests"
            7. "marker"
            8. "amount"
            9. "session_id"

    5005/payments/default/view/school
        Fields returned in "data":
            1. "payment_public_id"
            2. "payment_person"
            3. "payment_person_currency"
            4. "payment_person_amount"
            5. "payment_person_discount"
            6. "payment_guests"
            7. "marker"
            8. "amount"
            9. "session_id"

    5005/payments/default/modify, methods = ["PATCH"]
        Fields accepted are:
            1. "payment_id" : The gate entry fee public id
            2. "person"
            3. "currency"
            4. "amount"
            5. "marker" : Can only be one of either "A" (Adult) or "C" (Child)
            6. "max_number"
            7. "session_id"
    
    5005/payments/default/delete, methods = ["PATCH"]
        Fields accepted are:
            1. "payment_id" : The gate entry fee public id
            2. "session_id"


### Gatepass URLs
    5005/gatepass/new, methods = ["POST"]
        Fields:
            "first_name"
            "last_name"
            "email"
            "phone"
            "destination"
            "ea_adult_citizen"
            "ea_adult_resident"
            "non_resident_adult"
            "ea_child_citizen"
            "ea_child_resident"
            "non_resident_child"
            "citizen_student"
            "ea_resident_student"
            "non_resident_student"
            "infant"
            "session_id"
            "partner" (Optional)

    5005/gatepass/members/new, methods = ["POST"]
        Fields:
            "member"
            "destination"
            "session_id"
            "ea_adult_citizen" (Optional)
            "ea_adult_resident" (Optional)
            "non_resident_adult" (Optional)
            "ea_child_citizen" (Optional)
            "ea_child_resident" (Optional)
            "non_resident_child" (Optional)
            "infant" (Optional)

    5005/gatepass/view
    5005//gatepass/view/<gatepass_id>


### Gatepass Payments URLs
    5005/gatepass/payments/new, methods = ["POST"]
        Fields:
            "gatepass_id"
            "session_id"
            "phone" (Optional)
            "mpesa" (Optional)
            "card_first" (Optional)
            "card_last" (Optional)


### Calendar URLs
    5005/calendar/view, methods = ["POST"]
        Fields:
            "start_date"
            "end_date"


### Booking Payments URLs
    5005/iveri/bookings/payments
        This is the callback URL once an iVeri transaction has been carried out.

    5005/bookings/payment/new, methods = ["POST"]
        This is the URL used to pay for bookings.
        Fields accepted are:
            1. "booking_id"
            2. "payment_method"
            3. "booking_amount"
            4. "payment_amount"
            5. "session_id"
            6. "phone" : Optional
            7. "mpesa_ref" : Optional
            8. "first_four" : Optional
            9. "last_four" : Optional

    5005/bookings/payment/defer, methods = ["POST"]
        The URL used to defer booking payments.
        Feilds accepted are:
            1. "booking_id"
            2. "session_id"


### Vehicles URLs
These URLs are used to perform basic CRUD functions on vehicles.
    
    5005/vehicle/charges/new" methods = ["POST"]
        Fields accepted are:
            1. "category" : The category eg 1 to 6 Seats
            2. "cost"
            3. "currency" : Such as KES
            4. "max_number"
            5. "session_id"
    
    5005/vehicle/charges/view
        Returns the following fields:
            1. "vehicle_charge_public_id"
            2. "vehicle_charge_category"
            3. "vehicle_charge_category_cost"
            4. "vehicle_charge_cost_currency"
            5. "discount"
            6. "vehicles"

    5005/vehicle/charges/view/<charge_id>
        <charge_id> is the vehicle charge public id.
        Returns the following fields:
            1. "vehicle_charge_public_id"
            2. "vehicle_charge_category"
            3. "vehicle_charge_category_cost"
            4. "vehicle_charge_cost_currency"
            5. "discount"
            6. "vehicles"

    5005/vehicle/charges/edit" methods = ["PATCH"]
        Fields accepted are:
            1. "charge_id"
            2. "category" : The category eg 1 to 6 Seats
            3. "cost"
            4. "currency" : Such as KES
            5. "max_number"
            6. "session_id"

    
    5005/vehicle/charges/delete" methods = ["PATCH"]
        Fields accepted are:
            1. "charge_id"
            2. "session_id"


### Bookings Batchfile URLs
A list of URLs needed for the generation of batchfiles.

    5005/batchfile/bookings/view
        The returned data is similar to that of a single booking view.


### Destination URLs
These URLs are used to perform basic CRUD functions on gatepass destinations.

    5005/destination/new, methods = ["POST"]
        Fields accepted are:
            1. "name"
            2. "session_id"
            3. "code"

    5005/destination/view
        Fields returned are:
            1. "destination_public_id"
            2. "destination_name"
            3. "destination_code"
            4. "session_id"

    5005/destination/view/<destination_id>
        Fields returned are:
            1. "destination_public_id"
            2. "destination_name"
            3. "destination_code"
            4. "session_id"

    5005/destination/modify', methods=['PATCH']
        Fields accepted are:
            1. "destination_id"
            2. "name"
            3. "code"
            4. "session_id"


    5005/destination/delete, methods = ["PATCH"]
        Fields accepted are:
            1. "destination_id"
            2. "session_id"


### Pick-Up Location URLs
These URLs are used to perform basic CRUD functions on pick-up locations.

    5005/pick-up/new, methods = ["POST"]
        Fields accepted are:
            1. "name"
            2. "session_id"

    5005/pick-up/view
        Fields returned are:
            1. "location_public_id"
            2. "location_name"
            3. "session_id"

    5005/pick-up/view/<location_id>
        Fields returned are:
            1. "location_public_id"
            2. "location_name"
            3. "session_id"

    5005/pick-up/modify', methods=['PATCH']
        Fields accepted are:
            1. "location_id"
            2. "name"
            3. "session_id"


    5005/pick-up/delete, methods = ["DELETE"]
        Fields accepted are:
            1. "location_id"
            2. "session_id"


### Partner Bookings URLs
These URLs are used to perform basic CRUD functions on partner bookings.

    5005/partners/bookings/new, methods = ["POST"]
        Fields accepted:
            1. "type" : The booking type
            2. "check_in"
            3. "check_out"
            4. "ea_adult_citizen" : Number (if no number is set, assign 0)
            5. "ea_adult_resident" : Number (if no number is set, assign 0)
            6. "non_resident_adult" : Number (if no number is set, assign 0)
            7. "ea_child_citizen" : Number (if no number is set, assign 0)
            8. "ea_child_resident" : Number (if no number is set, assign 0)
            9. "non_resident_child" : Number (if no number is set, assign 0)
            10. "infant" : Number (if no number is set, assign 0)
            11. "facilities" : Array
                In array:
                    [{"public_id": "<string>, "adults": <int>, "children": <int>}] 
            12. "inventory" : Array
                In array:
                    [{"public_id": "<string>, "date": "<date>, "adults": <int>, "children": <int>}]
            13. "vehicles" : Array
                In array:
                    {"seater6": <int>, "seater14": <int>, "seater60": <int>}
            14. "partner" : The partner ID
            15. "additional_note" : (Optional)
            16. "session_id"

    5005/partners/bookings/view
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/partners/bookings/view/partner/<partner_id>
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/partners/bookings/view/pending
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/partners/bookings/view/approved
        Fields returned in "data":
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"


### Group Booking Types URLs
These URLs are used to perform basic CRUD functions on group booking types.

    5005/bookings/group/types/view
        Fields returned in "data":
            1. "booking_type_public_id"
            2. "booking_type_name"
            3. "booking_type_description"
            4. "booking_type_public"
            5. "booking_type_back"
            6. "session_id"
            7. "created_at"
            8. "updated_at"


### Group Bookings URLs
These URLs are used to perform basic CRUD functions on group bookings.

    5005/group/bookings/new, methods = ["POST"]
        Fields accepted:
            1. "type" : The booking type
            2. "check_in"
            3. "check_out"
            4. "currency"
            5. "organisation_name"
            6. "organisation_email"
            7. "organisation_type"
            8. "facilities" : Array
                In array:
                    [{"public_id": "<string>, "adults": <int>, "children": <int>}] 
            9. "inventory" : Array
                In array:
                    [{"public_id": "<string>, "date": "<date>, "adults": <int>, "children": <int>}]
            10. "vehicles" : Array
                In array:
                    {"seater6": <int>, "seater14": <int>, "seater60": <int>}
            11. "name"
            12. "email"
            13. "phone"
            14. "additional_note" : (Optional)
            15. "session_id"

    5005/group/bookings/view
        Fields returned:
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/group/bookings/view/pending
        Fields returned:
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"

    5005/group/bookings/view/approved
        Fields returned:
            1. "booking_public_id"
            2. "booking_type"
            3. "booking_type_id"
            4. "booking_check_in_date"
            5. "booking_check_out_date"
            6. "booking_done_by"
            7. "guests"
            8. "guest_total"
            9. "booking_status"
            10. "booking_status_id"
            11. "session_id"
            12. "created_at"
            13. "updated_at"


### Gates URLs
URLs dealing with gates.

    5005/gates/new, methods = ["POST"]
        Fields accepted are:
            1. "name"
            2. "session_id"
    
    5005/gates/view
        Fields returned:
            1. "gate_public_id"
            2. "gate_name"

    5005/gates/view/<gate_id>
        gate_id is the gate public id.
        Fields returned:
            1. "gate_public_id"
            2. "gate_name"

    5005/gates/modify, methods = ["PATCH"]
        Fields accepted are:
            1. "gate_id" : The gate public id
            2. "name"
            3. "session_id"

    5005/gates/delete, methods = ["PATCH"]
        Fields accepted are:
            1. "gate_id" : The gate public id
            2. "session_id"


### Bookings Count URLs
There is no POST or DELETE here. Only GET. These URLs return counts.

    5005/count/bookings/all
        Returns all bookings in the system.


### Bookings Graphs URLs
There is no POST or DELETE here. Only GET. These URLs return data for the purpose of graphing.

    5005/bookings/graphs/week/all
        Returns both check in and check out data for a period of 4 weeks of all bookings.

    5005/bookings/graphs/week/group
        Returns both check in and check out data for a period of 4 weeks of group bookings.

    5005/bookings/graphs/week/partner
        Returns both check in and check out data for a period of 4 weeks of partner bookings.


### CSV Urls
5005/generate/reports/csv', POST
	start_date
	end_date
    csv (Send this if you need a csv generated)


### Facility Pricing Type URLs
These URLs are used to perform basic CRUD functions on facility pricing types.

    5005/facility/pricing/new, methods = ["POST"]
        Fields accepted are:
            1. "name"
            2. "session_id"
            3. "special" (optional)

    5005/facility/pricing/view
        Fields returned are:
            1. "facility_pricing_type_public_id"
            2. "facility_pricing_name"
            3. "session_id"

    5005/facility/pricing/view/<facility_price_id>
        Fields returned are:
            1. "facility_pricing_type_public_id"
            2. "facility_pricing_name"
            3. "session_id"

    5005/facility/pricing/modify, methods=['PATCH']
        Fields accepted are:
            1. "facility_price_id"
            2. "name"
            3. "special" (optional)
            4. "session_id"


    5005/facility/pricing/delete, methods = ["PATCH"]
        Fields accepted are:
            1. "facility_price_id"
            2. "session_id"


### Booking Notes URLs
These URLs are used to perform basic CRUD functions on booking notes.

    5005/note/new, methods = ["POST"]
        Fields accepted are:
            1. "booking_id"
            2. "note"
            3. "session_id"

    5005/note/delete, methods = ["PATCH"]
        Fields accepted are:
            1. "note_id" : The note public ID
            2. "session_id"


### Salesforce Credentials URLs
These URLs are used to perform basic CRUD functions on Salesforce credentials.

    5005/salesforce/credentials, methods = ["POST"]
        Fields accepted are:
            1. "email"
            2. "password"