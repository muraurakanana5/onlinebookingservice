migrate:
	docker-compose exec booking_service /bin/bash -c "python app/manage.py db init"
	docker-compose exec booking_service /bin/bash -c "python app/manage.py db migrate"
	docker-compose exec booking_service /bin/bash -c "python app/manage.py db upgrade"
