FROM python:3.6.0-slim

RUN apt-get update && apt-get install -y python3-dev libmysqlclient-dev build-essential libssl-dev libffi-dev libpq-dev gcc
RUN mkdir -p /booking_service
RUN mkdir -p /config

RUN apt-get install -y wget
RUN apt-get install -y libxrender1 libfontconfig libxext6
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN tar vxf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN cp wkhtmltox/bin/wk* /usr/local/bin/
RUN chmod +x /usr/local/bin/wkhtmltopdf

WORKDIR /booking_service

ADD . /booking_service/
# RUN pip install -r requirements.txt --cache-dir /pip-cache
RUN pip install -r requirements.txt
RUN echo "Africa/Nairobi" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata
# RUN mkdir reports

# RUN openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365

# RUN openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj "/C=KE/ST=Nairobi/L=Nairobi/O=Ol Pejeta/OU=Org/CN=bookings.olpejetaconservancy.org"
# RUN openssl req -x509 -new -nodes -keyout privkey.pem -out cert.pem -days 365 -subj "/C=KE/ST=Nairobi/L=Nairobi/O=Ol Pejeta/OU=Org/CN=bookings.olpejetaconservancy.org"

# RUN openssl genrsa -out privkey.pem 2048
# RUN openssl req -x509 -new -nodes -key privkey.pem -out cert.pem -days 365 -subj "/C=KE/ST=Nairobi/L=Nairobi/O=Ol Pejeta/OU=Org/CN=bookings.olpejetaconservancy.org"

ENTRYPOINT ["python", "app/run.py"]
EXPOSE 5005